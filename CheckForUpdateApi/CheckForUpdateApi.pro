
TEMPLATE    = lib
TARGET      = CheckForUpdateApi
VERSION		= 1.1.3
CONFIG 		+= staticlib
CONFIG 		+= c++14
# very important to create a same directory structure when loaded in visual studio.
CONFIG 		-= flat
QT       	+= core gui xml widgets network

DEFINES += QT_DEPRECATED_WARNINGS

isEmpty(PREFIX) {
    PREFIX = CheckForUpdateApiLib
}

CONFIG(debug, debug|release) {

	MOC_DIR = CheckForUpdateApi/moc/Debug/
	RCC_DIR = CheckForUpdateApi/rcc/Debug/
	UI_DIR = CheckForUpdateApi/include/ui/
	
	contains(QT_ARCH, i386) {
		OBJECTS_DIR = obj/x86/Debug/
		DESTDIR = CheckForUpdateApi/lib/x86/Debug/
		LIB_TARGET = $$PREFIX/lib/x86/Debug/CheckForUpdateApi.lib
		target.path = $$PREFIX/lib/x86/Debug/lib/
	} else {
		OBJECTS_DIR = obj/x64/Debug/
		DESTDIR = CheckForUpdateApi/lib/x64/Debug/
		LIB_TARGET = $$PREFIX/lib/x64/Debug/CheckForUpdateApi.lib
		target.path = $$PREFIX/lib/x64/Debug/lib/
	}
	
} else {

	MOC_DIR = CheckForUpdateApi/moc/Release/
	RCC_DIR = CheckForUpdateApi/rcc/Release/
	UI_DIR = CheckForUpdateApi/include/ui/
	
	contains(QT_ARCH, i386) {
		OBJECTS_DIR = obj/x86/Release/
		DESTDIR = CheckForUpdateApi/lib/x86/Release/
		LIB_TARGET = $$PREFIX/lib/x86/Release/CheckForUpdateApi.lib
		target.path = $$PREFIX/lib/x86/Release/lib/
	} else {
		OBJECTS_DIR = obj/x64/Release/
		DESTDIR = CheckForUpdateApi/lib/x64/Release/
		LIB_TARGET = $$PREFIX/lib/x64/Release/CheckForUpdateApi.lib
		target.path = $$PREFIX/lib/x64/Release/lib/
	}
}

	
INCLUDEPATH += \
				../XrayToolKit/XrayToolKit/include/widgets/ \
				../XrayToolKit/XrayToolKit/include/filehandler/ \
				../XrayToolKit/XrayToolKit/include/network/ \
				../XrayToolKit/XrayToolKit/include/utilities/ \
				../XrayToolKit/XrayToolKit/include/globals/ \
				../../thirdparty/libs/src/spdlog-1.x/include/


SOURCES     += \
			   ../XrayToolKit/XrayToolKit/src/widgets/XrayPixmapStatusWidget.cpp \
			   ../XrayToolKit/XrayToolKit/src/widgets/XrayProgressWidget.cpp \
			   \
			   ../XrayToolKit/XrayToolKit/src/filehandler/XrayXmlSettings.cpp \
			   \
			   ../XrayToolKit/XrayToolKit/src/network/XrayQFileDownloader.cpp \
			   ../XrayToolKit/XrayToolKit/src/network/XrayCheckForUpdate.cpp \
			   ../XrayToolKit/XrayToolKit/src/network/XrayQFileDownloaderWidget.cpp \
			   \
			   ../XrayToolKit/XrayToolKit/src/utilities/XrayLogger.cpp \
			   ../XrayToolKit/XrayToolKit/src/utilities/XrayQFileUtil.cpp \
			   ../XrayToolKit/XrayToolKit/src/utilities/XrayQSimpleCrypt.cpp \
			   ../XrayToolKit/XrayToolKit/src/utilities/XrayQtUtil.cpp \
			   \
			   ../XrayToolKit/XrayToolKit/src/globals/XrayGlobal.cpp

HEADERS     += \
			   ../XrayToolKit/XrayToolKit/include/widgets/XrayPixmapStatusWidget.h \
			   ../XrayToolKit/XrayToolKit/include/widgets/XrayProgressWidget.h \
			   \
			   ../XrayToolKit/XrayToolKit/include/filehandler/XrayXmlSettings.h \
			   \
			   ../XrayToolKit/XrayToolKit/include/network/XrayQFileDownloader.h \
			   ../XrayToolKit/XrayToolKit/include/network/XrayCheckForUpdate.h \
			   ../XrayToolKit/XrayToolKit/include/network/XrayQFileDownloaderWidget.h \
			   \
			   ../XrayToolKit/XrayToolKit/include/utilities/XrayLogger.h \
			   ../XrayToolKit/XrayToolKit/include/utilities/XrayQFileUtil.h \
			   ../XrayToolKit/XrayToolKit/include/utilities/XrayQSimpleCrypt.h \
			   ../XrayToolKit/XrayToolKit/include/utilities/XrayQtUtil.h \
			   ../XrayToolKit/XrayToolKit/include/utilities/XrayQtApiExport.h \
			   \
			   ../XrayToolKit/XrayToolKit/include/globals/XrayGlobal.h

FORMS       += \


RESOURCES   += \
			   
TRANSLATIONS += \



headers.path = $$PREFIX/include
headers.files = $$HEADERS

INSTALLS += target headers