/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/04
** filename: 	XrayQFileDownloader.h
** file base:	XrayQFileDownloader
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a file downloader.

** usage example:
XrayQFileDownloader downloader(QUrl("http://www.real3d.pk/files/softwares/argbp/argbp.exe"));
//downloader.setAuthentication({ "admin", "admin" });	// if authentication is required.
QTimer::singleShot(0, &downloader, SLOT(start()));
****************************************************************************/

#pragma once
#ifndef XrayQFileDownloader_h__
#define XrayQFileDownloader_h__

#include "XrayQtApiExport.h"

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QByteArray>
#include <QString>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQFileDownloader : public QObject
{
	Q_OBJECT

public:
	explicit XrayQFileDownloader(QObject* _parent = 0);
	explicit XrayQFileDownloader(const QUrl& _url, QObject* _parent = 0);

	/*!
		Description:
		Function to get the downloaded data as sting.
		/return downloaded data as sting.
	*/
	QString getData() const;

	/*!
		Description:
		Function to set the url to be downloaded.
		/param _url - link of the file to be downloaded.
	*/
	void setUrl(const QUrl& _url);
	QUrl getUrl() const;

	/*!
		Description:
		Function to set the authentication as user name and password.
		/param _userNamePassword - user name and password
	*/
	void setAuthentication(const QPair<QString, QString>& _username_password);
	QPair<QString, QString> getAuthentication() const;

	/*!
		Description:
		Function to save to the downloaded data to the disk.
		/param _dirPath - absolute path to the directory.
		/return true on success.
	*/
	bool saveToDisk(const QString& _dirPath);

	/*!
		Description:
		Function that returns the saved file name.
	*/
	QString getSavedFileName() const;

signals:
	/*!
		Description:
		This signal will be triggered when the download is started.
	*/
	void startDownload();
	/*!
		Description:
		This signal will be triggered during downloading.
	*/
	void downloadProgress(qint64 _bytesReceived, qint64 _bytesTotal);
	/*!
		Description:
		This signal will be triggered when the download is completed.
	*/
	void downloaded();
	/*!
		Description:
		This signal will be triggered when there is an error in downloading.
	*/
	void downloadError(const QString& _error);

public slots:
	/*!
		Description:
		Function to start downloading.
	*/
	void start();

private slots:
	void finished(QNetworkReply* _reply);

private:
	QNetworkAccessManager m_networkAccessManager;
	QPair<QString, QString> m_authenticator;
	QByteArray m_data;
	QUrl m_url;
	QString m_savedFile;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQFileDownloader_h__                                                                                                                                                                                                                                                                                   
