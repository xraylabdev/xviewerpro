/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/02
** filename: 	XrayTcpClientMainWidget.h
** file base:	XrayTcpClientMainWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a client interface.
****************************************************************************/

#pragma once
#ifndef XrayTcpClientMainWidget_h__
#define XrayTcpClientMainWidget_h__

#include "XrayQtApiExport.h"
#include "XrayProgressWidget.h"
#include "XrayStatusBar.h"
#include "XrayQLed.h"
#include "XrayQListWidget.h"
#include "XrayTcpClientThread.h"

#include <QMainWindow>
#include <QPushButton>
#include <QLabel>
#include <QAction>
#include <QLineEdit>
#include <QListWidget>
#include <QSettings>
#include <QCloseEvent>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayTcpClientMainWidget : public QMainWindow
{
	Q_OBJECT

public:
	XrayTcpClientMainWidget(QWidget* parent = nullptr);
	~XrayTcpClientMainWidget();

	/*!
	   Description:
	   Function that saves the settings.
	   \param - a reference to QSettings.
	*/
	void saveSettings(QSettings& _settings);
	/*!
	   Description:
	   Function that restores the settings.
	   \param - a reference to QSettings.
	*/
	void restoreSettings(QSettings& _settings);

public slots:
	void startClient();
	void stopServer();
	void browseDir();
	void enableStartReceiving();
	/*!
		Description:
		This slot will be triggered when an error occurs.
	*/
	void displayError(int _socketError, const QString& _message);
	/*!
		Description:
		This slot will be triggered when the data files has been received.
	*/
	void receivedFiles(const QVector<QString>& _files);
	/*!
	   Description:
	   This slot is being triggered when the data has been received from the server.
	*/
	void onFinish();

private:
	void closeEvent(QCloseEvent* _event) override;

protected:
	XrayProgressWidget* p_progressWidget;
	XrayStatusBar* p_statusBar;

	QLabel* p_serverIpLabel;
	QLabel* p_serverPortLabel;
	QLineEdit* p_serverIp;
	QLineEdit* p_serverPort;

	QLabel* p_timerLabel;
	QLineEdit* p_timerValue;

	QLabel* p_lastRecTime;

	QLineEdit* p_dirPath;
	QPushButton* p_browsDir;
	QPushButton* p_startReceiving;
	XrayQLedBlink* p_blinker;

	XrayQListWidget* p_listWidget;
	XrayTcpClientThread m_thread;
	QTimer m_timer;
};

XRAYLAB_END_NAMESPACE

#endif // XrayTcpClientMainWidget_h__