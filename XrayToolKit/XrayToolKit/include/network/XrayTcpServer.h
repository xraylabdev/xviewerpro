/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/10
** filename: 	XrayTcpServer.h
** file base:	XrayTcpServer
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a threaded-based server side that
send multiple files data to the client side.
****************************************************************************/

#pragma once
#ifndef XrayTcpServer_h__
#define XrayTcpServer_h__

#include "XrayQtApiExport.h"

#include <QStringList>
#include <QTcpServer>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayTcpServer : public QTcpServer
{
    Q_OBJECT

public:
	/*!
		Description:
		Default constructor to create a new server object.
		This constructor is for a file that first loaded from the disk
		and then send to the client.
		/param _files - specify the list of files.
		/param parent - pointer to the parent object.
	*/
	explicit XrayTcpServer(const QStringList& _files = QStringList(), QObject* _parent = 0);

	/*!
		Description:
		Function to remove all files from the list.
	*/
	void clear();
	/*!
		Description:
		Function to add a file that needs to be sent to the client.
		/param _fileName - specify the absolute file path.
	*/
	void addFile(const QString& _fileName);
	/*!
		Description:
		Function to set files that need to be sent to the client.
		/param _files - list of file paths.
	*/
	void setFiles(const QStringList& _files);
	/*!
		Description:
		Function that returns a list of file paths.
		/return - list of file paths.
	*/
	QStringList getFiles() const;

signals:
	/*!
	   Description:
	   This signal is being triggered on the start of loading files.
	*/
	void onStart();
	/*!
	   Description:
	   This signal is being triggered while loading files from the directory.
	   \param _value - value for the progress bar from [0 ~ 1].
	*/
	void onProgress(double _value);
	/*!
	   Description:
	   This signal is being triggered when the data is sent to the client.
	*/
	void onFinished();

protected:
    void incomingConnection(qintptr _socketDescriptor) override;

private:
    QStringList m_projectDir;
};

XRAYLAB_END_NAMESPACE

#endif // XrayTcpServer_h__