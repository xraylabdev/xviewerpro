/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/10
** filename: 	XrayTcpServerThread.h
** file base:	XrayTcpServerThread
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a threaded-based server side that
send multiple files data to the client side.
****************************************************************************/

#pragma once
#ifndef XrayTcpServerThread_h__
#define XrayTcpServerThread_h__

#include "XrayQtApiExport.h"

#include <QThread>
#include <QTcpSocket>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayTcpServerThread : public QThread
{
	Q_OBJECT

public:
	/*!
		Description:
		Default constructor to create an a new thread object.
		This constructor is for a file that first loaded from the disk
		and then send to the client.
		/param _socketDescriptor - specify the socket descriptor.
		/param _fileName - specify the file name to be sent.
		/param parent - pointer to the parent object.
	*/
	XrayTcpServerThread(int _socketDescriptor, const QStringList& _files = QStringList(), QObject* _parent = 0);
	/*!
		Description:
		Default constructor to create an a new thread object.
		This constructor is for the given data to send to the client.
		/param _socketDescriptor - specify the socket descriptor.
		/param _data - specify the data array.
		/param parent - pointer to the parent object.
	*/
	XrayTcpServerThread(int _socketDescriptor, const QByteArray& _data, QObject* _parent);

signals:
	/*!
		Description:
		This signal will be triggered when an error occurs.
	*/
	void error(QTcpSocket::SocketError _socketError);

	/*!
	   Description:
	   This signal is being triggered on the start of loading files.
	*/
	void onStart();
	/*!
	   Description:
	   This signal is being triggered while loading files from the directory.
	   \param _value - value for the progress bar from [0 ~ 1].
	*/
	void onProgress(double _value);
	/*!
	   Description:
	   This signal is being triggered when the data is sent to the client.
	*/
	void onFinished();

private:
	/*!
		Description:
		Function to process all operations.
	*/
	void run() override;

	int m_socketDescriptor;
	QStringList m_projectDir;
	QByteArray m_data;
};

XRAYLAB_END_NAMESPACE

#endif // XrayTcpServerThread_h__