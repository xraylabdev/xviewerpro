/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/04
** filename: 	XrayCheckForUpdate.h
** file base:	XrayCheckForUpdate
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a fully loaded check for update widget
with a download manager.

** usage example:
auto p_updater = new XrayCheckForUpdate(QUrl("http://real3d.pk/files/xpaint_pad_file.xml"));
p_updater->checkOnStartupEnabled(false);
p_updater->setRemindMeLaterDays(7);
p_updater->start();
****************************************************************************/

#pragma once
#ifndef XrayCheckForUpdate_h__
#define XrayCheckForUpdate_h__

#include "XrayQFileDownloaderWidget.h"

#include <QWidget>
#include <QGroupBox>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QTextBrowser>
#include <QCloseEvent>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayCheckForUpdate : public QWidget
{
	Q_OBJECT

public:
	/*!
		Description:
		Constructor that takes the URL of the PAD file and create an object
		that check for updates from the given PAD file.
		By default check on startup is true.
		/param _url - link of the file to be checked.
	*/
	explicit XrayCheckForUpdate(const QUrl& _url, QWidget* _parent = 0);

	/*!
		Description:
		Function to set the url of the pad file.
		/param _url - link of the file to be checked.
	*/
	void setUrl(const QUrl& _url);
	/*!
		Description:
		Function to set the url of the pad file.
		/returns - file to be checked.
	*/
	QUrl getUrl() const;

	/*!
		Description:
		This class will check for update on startup if it is true.
		/param _b - true for check on startup.
	*/
	void checkOnStartupEnabled(bool _b);
	/*!
		Description:
		Function that returns true if check for update on startup is true.
	*/
	bool isCheckOnStartupEnabled() const;

	/*!
		Description:
		Function to set the number of days after which this class will check for update automatically.
	*/
	void setRemindMeLaterDays(int _days);
	/*!
		Description:
		Function that returns the remind me later days.
	*/
	int getRemindMeLaterDays() const;

signals:
	/*!
		Description:
		This signal is being triggered if the update is available.
	*/
	void updateAvailable();

public slots:
	/*!
		Description:
		Function to start checking the update and show the dialog.
	*/
	void start();

private slots:
	/*!
		Description:
		Function to start checking the update and show the dialog.
	*/
	void launchDownloadedFile();

private:
	/*!
		Description:
		Function that checks the update of the software from the server.
	*/
	void checkForUpdate(bool _atStartUp);
	/*!
		Description:
		Function that automatically checks the update after the specified days.
		Default is 7 days.
	*/
	bool remindeMeLaterCheck();

	void closeEvent(QCloseEvent* _event) Q_DECL_OVERRIDE;

	QGroupBox* p_releaseNotesGBox;
	QGridLayout* p_layout;
	QPushButton* p_installUpdateBtn;
	QPushButton* p_checkUpdateBtn;
	QLabel* p_updateAvailableLabel;
	QLabel* p_installedLabel;
	QLabel* p_availableLabel;
	QLabel* p_releaseDateLabel;
	QLabel* p_releaseStatusLabel;
	QTextBrowser* p_textBrowser;
	QGroupBox* p_downloadGBox;
	XrayQFileDownloaderWidget* p_updateDownloader;
	int m_remindMeLaterDays;

	QUrl m_url;
};

XRAYLAB_END_NAMESPACE

#endif // XrayCheckForUpdate_h__                                                                                                                                                                                                                                                                                   
