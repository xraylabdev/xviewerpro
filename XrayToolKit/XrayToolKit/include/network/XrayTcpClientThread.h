/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/10
** filename: 	XrayTcpClientThread.h
** file base:	XrayTcpClientThread
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a threaded-based client side that
receive multiple files or data from the server side and save to the disk
and emit a signal with the data. If you are only interested in the data but
don't want to save files to the disk then just make the directory path empty.
****************************************************************************/

#pragma once
#ifndef XrayTcpClientThread_h__
#define XrayTcpClientThread_h__

#include "XrayQtApiExport.h"

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QVector>
#include <QPair>
#include <QByteArray>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayTcpClientThread : public QThread
{
    Q_OBJECT

public:
	/*!
		Description:
		Default constructor to create an a new thread object.
		/param _dirPath - absolute path to the directory where the received data will be saved.
		/param parent - pointer to the parent obect.
	*/
	explicit XrayTcpClientThread(const QString& _dirPath = QString(), QObject* _parent = 0);
	/*!
		Description:
		Destructor that stops the thread and destroys the object.
	*/
	~XrayTcpClientThread();

	/*!
		Description:
		Function to start the thread by specifying the host name and the port number.
		This function can be triggered multiple times to get the file data from the
		server.
		/param _hostName - host name or the ip address
		/param _port - port number
		/param _dirPath - absolute path to the directory where the received data will be saved.
	*/
	void start(const QString& _hostName, quint16 _port, const QString& _dirPath);

signals:
	/*!
		Description:
		This signal will be triggered when an error occurs.
	*/
	void error(int _socketError, const QString& _message);

	/*!
	   Description:
	   This signal is being triggered on the start of receiving files.
	*/
	void onStart();
	/*!
	   Description:
	   This signal is being triggered while saving files to the disk.
	   \param _value - value for the progress bar from [0 ~ 1].
	*/
	void onProgress(double _value);
	/*!
	   Description:
	   This signal is being triggered when the data has been received from the server.
	*/
	void onFinished();

	/*!
		Description:
		This signal will be triggered when the data files has been received.
	*/
	void receivedFiles(const QVector<QString>& _files);

private:
	/*!
		Description:
		Function to process all operations.
	*/
	void run() override;

	/*!
		Description:
		Helper function to save the given _data to the disk.
	*/
	static bool saveToDisk(const QString& _dirName, const QString& _fileName, const QByteArray& _data);

    QString m_hostName;
    quint16 m_port;
    QMutex m_mutex;
    QWaitCondition m_cond;
    bool m_quit;
	QString m_dirPath;
};

XRAYLAB_END_NAMESPACE

#endif // XrayTcpClientThread_h__