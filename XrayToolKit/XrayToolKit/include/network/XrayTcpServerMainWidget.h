/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/02
** filename: 	XrayTcpServerMainWidget.h
** file base:	XrayTcpServerMainWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a client server interface.
****************************************************************************/

#pragma once
#ifndef XrayTcpServerMainWidget_h__
#define XrayTcpServerMainWidget_h__

#include "XrayQtApiExport.h"
#include "XrayProgressWidget.h"
#include "XrayStatusBar.h"
#include "XrayQLed.h"
#include "XrayQListWidget.h"
#include "XrayTcpServer.h"

#include <QMainWindow>
#include <QPushButton>
#include <QLabel>
#include <QAction>
#include <QLineEdit>
#include <QListWidget>
#include <QSettings>
#include <QCloseEvent>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayTcpServerMainWidget : public QMainWindow
{
	Q_OBJECT

public:
	XrayTcpServerMainWidget(QWidget* parent = nullptr);
	~XrayTcpServerMainWidget();

	/*!
	   Description:
	   Function that saves the settings.
	   \param - a reference to QSettings.
	*/
	void saveSettings(QSettings& _settings);
	/*!
	   Description:
	   Function that restores the settings.
	   \param - a reference to QSettings.
	*/
	void restoreSettings(QSettings& _settings);

public slots:
	void runServer();
	void stopServer();
	void browseFiles();
	void updateList(const std::vector<int>& _removedRows);
	/*!
	   Description:
	   This slot is being triggered when the data has been sent to the client.
	*/
	void onFinish();

private:
	void closeEvent(QCloseEvent* _event) override;

protected:
	XrayProgressWidget* p_progressWidget;
	XrayStatusBar* p_statusBar;

	QLabel* p_serverIpLabel;
	QLabel* p_serverPortLabel;
	QLineEdit* p_serverIp;
	QLineEdit* p_serverPort;
	QLabel* p_lastSentTime;
	QPushButton* p_browsFiles;
	QPushButton* p_serverConnect;
	QPushButton* p_serverDisconnect;
	XrayQLedBlink* p_blinker;


	XrayQListWidget* p_listWidget;
	XrayTcpServer* p_server;
};

XRAYLAB_END_NAMESPACE

#endif // XrayTcpServerMainWidget_h__