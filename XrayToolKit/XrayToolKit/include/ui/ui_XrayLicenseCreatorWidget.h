/********************************************************************************
** Form generated from reading UI file 'XrayLicenseCreatorWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_XRAYLICENSECREATORWIDGET_H
#define UI_XRAYLICENSECREATORWIDGET_H

#include <QtCore/QDate>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_XrayLicenseCreatorWidget
{
public:
    QGridLayout *gridLayout_2;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_3;
    QLabel *label_10;
    QGroupBox *groupBox_22;
    QGridLayout *gridLayout_26;
    QLabel *moduleLabel_2;
    QLineEdit *licenseFileEdit;
    QPushButton *btnBrowse;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout;
    QLabel *label_4;
    QLineEdit *licenseIdEdit;
    QLabel *label_5;
    QLineEdit *serialEdit;
    QLabel *label_11;
    QDateEdit *expireDate;
    QLabel *label_7;
    QDateEdit *issueDate;
    QLabel *label_8;
    QComboBox *typeCombo;
    QLabel *label_9;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *xreportCB;
    QCheckBox *userManCB;
    QVBoxLayout *verticalLayout;
    QCheckBox *xpaintCB;
    QCheckBox *xenhancerCB;
    QCheckBox *xvoidCB;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btnSave;
    QPushButton *btnCancel;

    void setupUi(QWidget *XrayLicenseCreatorWidget)
    {
        if (XrayLicenseCreatorWidget->objectName().isEmpty())
            XrayLicenseCreatorWidget->setObjectName(QString::fromUtf8("XrayLicenseCreatorWidget"));
        XrayLicenseCreatorWidget->resize(475, 595);
        XrayLicenseCreatorWidget->setMinimumSize(QSize(0, 550));
        XrayLicenseCreatorWidget->setMaximumSize(QSize(16777215, 16777215));
        gridLayout_2 = new QGridLayout(XrayLicenseCreatorWidget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        groupBox_6 = new QGroupBox(XrayLicenseCreatorWidget);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        gridLayout_3 = new QGridLayout(groupBox_6);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_10 = new QLabel(groupBox_6);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_10->setWordWrap(true);

        gridLayout_3->addWidget(label_10, 0, 0, 1, 1);


        gridLayout_2->addWidget(groupBox_6, 0, 0, 1, 2);

        groupBox_22 = new QGroupBox(XrayLicenseCreatorWidget);
        groupBox_22->setObjectName(QString::fromUtf8("groupBox_22"));
        gridLayout_26 = new QGridLayout(groupBox_22);
        gridLayout_26->setObjectName(QString::fromUtf8("gridLayout_26"));
        moduleLabel_2 = new QLabel(groupBox_22);
        moduleLabel_2->setObjectName(QString::fromUtf8("moduleLabel_2"));

        gridLayout_26->addWidget(moduleLabel_2, 0, 0, 1, 1);

        licenseFileEdit = new QLineEdit(groupBox_22);
        licenseFileEdit->setObjectName(QString::fromUtf8("licenseFileEdit"));

        gridLayout_26->addWidget(licenseFileEdit, 0, 1, 1, 1);

        btnBrowse = new QPushButton(groupBox_22);
        btnBrowse->setObjectName(QString::fromUtf8("btnBrowse"));

        gridLayout_26->addWidget(btnBrowse, 0, 2, 1, 1);


        gridLayout_2->addWidget(groupBox_22, 1, 0, 1, 2);

        groupBox_4 = new QGroupBox(XrayLicenseCreatorWidget);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout = new QGridLayout(groupBox_4);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_4 = new QLabel(groupBox_4);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 0, 0, 1, 1);

        licenseIdEdit = new QLineEdit(groupBox_4);
        licenseIdEdit->setObjectName(QString::fromUtf8("licenseIdEdit"));

        gridLayout->addWidget(licenseIdEdit, 0, 1, 1, 1);

        label_5 = new QLabel(groupBox_4);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 1, 0, 1, 1);

        serialEdit = new QLineEdit(groupBox_4);
        serialEdit->setObjectName(QString::fromUtf8("serialEdit"));

        gridLayout->addWidget(serialEdit, 1, 1, 1, 1);

        label_11 = new QLabel(groupBox_4);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 2, 0, 1, 1);

        expireDate = new QDateEdit(groupBox_4);
        expireDate->setObjectName(QString::fromUtf8("expireDate"));
        expireDate->setButtonSymbols(QAbstractSpinBox::PlusMinus);
        expireDate->setProperty("showGroupSeparator", QVariant(true));
        expireDate->setMinimumDate(QDate(2000, 9, 14));
        expireDate->setCalendarPopup(true);

        gridLayout->addWidget(expireDate, 2, 1, 1, 1);

        label_7 = new QLabel(groupBox_4);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 3, 0, 1, 1);

        issueDate = new QDateEdit(groupBox_4);
        issueDate->setObjectName(QString::fromUtf8("issueDate"));
        issueDate->setButtonSymbols(QAbstractSpinBox::PlusMinus);
        issueDate->setProperty("showGroupSeparator", QVariant(true));
        issueDate->setMinimumDate(QDate(2000, 9, 14));
        issueDate->setCalendarPopup(true);

        gridLayout->addWidget(issueDate, 3, 1, 1, 1);

        label_8 = new QLabel(groupBox_4);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 4, 0, 1, 1);

        typeCombo = new QComboBox(groupBox_4);
        typeCombo->addItem(QString());
        typeCombo->addItem(QString());
        typeCombo->addItem(QString());
        typeCombo->setObjectName(QString::fromUtf8("typeCombo"));

        gridLayout->addWidget(typeCombo, 4, 1, 1, 1);

        label_9 = new QLabel(groupBox_4);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout->addWidget(label_9, 5, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        xreportCB = new QCheckBox(groupBox_4);
        xreportCB->setObjectName(QString::fromUtf8("xreportCB"));

        verticalLayout_2->addWidget(xreportCB);

        userManCB = new QCheckBox(groupBox_4);
        userManCB->setObjectName(QString::fromUtf8("userManCB"));

        verticalLayout_2->addWidget(userManCB);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        xpaintCB = new QCheckBox(groupBox_4);
        xpaintCB->setObjectName(QString::fromUtf8("xpaintCB"));

        verticalLayout->addWidget(xpaintCB);

        xenhancerCB = new QCheckBox(groupBox_4);
        xenhancerCB->setObjectName(QString::fromUtf8("xenhancerCB"));

        verticalLayout->addWidget(xenhancerCB);

        xvoidCB = new QCheckBox(groupBox_4);
        xvoidCB->setObjectName(QString::fromUtf8("xvoidCB"));

        verticalLayout->addWidget(xvoidCB);


        horizontalLayout->addLayout(verticalLayout);


        gridLayout->addLayout(horizontalLayout, 5, 1, 1, 1);


        gridLayout_2->addWidget(groupBox_4, 2, 0, 1, 2);

        verticalSpacer = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 3, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(280, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 4, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        btnSave = new QPushButton(XrayLicenseCreatorWidget);
        btnSave->setObjectName(QString::fromUtf8("btnSave"));

        horizontalLayout_2->addWidget(btnSave);

        btnCancel = new QPushButton(XrayLicenseCreatorWidget);
        btnCancel->setObjectName(QString::fromUtf8("btnCancel"));

        horizontalLayout_2->addWidget(btnCancel);


        gridLayout_2->addLayout(horizontalLayout_2, 4, 1, 1, 1);


        retranslateUi(XrayLicenseCreatorWidget);

        QMetaObject::connectSlotsByName(XrayLicenseCreatorWidget);
    } // setupUi

    void retranslateUi(QWidget *XrayLicenseCreatorWidget)
    {
        XrayLicenseCreatorWidget->setWindowTitle(QCoreApplication::translate("XrayLicenseCreatorWidget", "XLicense Generator", nullptr));
        groupBox_6->setTitle(QCoreApplication::translate("XrayLicenseCreatorWidget", "Information", nullptr));
        label_10->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "<html><head/><body><p>We need a hardware key file from the customer to be able to generated a license file for the requested module(s). The customer must generate a hardware key from the requested module's licensing system. With the hardware key, this application will generate a license file which we will send to customer via email.<br/><br/>Please browse and select the file that you have received from the customer.</p></body></html>", nullptr));
        groupBox_22->setTitle(QCoreApplication::translate("XrayLicenseCreatorWidget", "Hardware key file", nullptr));
        moduleLabel_2->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "Path:", nullptr));
        btnBrowse->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "Browse...", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("XrayLicenseCreatorWidget", "License details", nullptr));
        label_4->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "License-id:", nullptr));
        label_5->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "Serial:", nullptr));
        label_11->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "Expire:", nullptr));
        label_7->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "Issue:", nullptr));
        label_8->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "Type:", nullptr));
        typeCombo->setItemText(0, QCoreApplication::translate("XrayLicenseCreatorWidget", "Evaluation License", nullptr));
        typeCombo->setItemText(1, QCoreApplication::translate("XrayLicenseCreatorWidget", "Permanent License", nullptr));
        typeCombo->setItemText(2, QCoreApplication::translate("XrayLicenseCreatorWidget", "Educational License", nullptr));

        label_9->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "Modules:", nullptr));
        xreportCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XREPORT PRO", nullptr));
        userManCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XUSERPANEL", nullptr));
        xpaintCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XPAINT PRO", nullptr));
        xenhancerCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XENHANCER PRO", nullptr));
        xvoidCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XVOID PRO", nullptr));
        btnSave->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "Save", nullptr));
        btnCancel->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class XrayLicenseCreatorWidget: public Ui_XrayLicenseCreatorWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_XRAYLICENSECREATORWIDGET_H
