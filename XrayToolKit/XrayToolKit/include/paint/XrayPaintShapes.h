/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayPaintShapes.h
** file base:	XrayPaintShapes
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		classes for paint shapes.
****************************************************************************/

#pragma once
#ifndef XrayPaintShapes_h__
#define XrayPaintShapes_h__

#include "XrayQtApiExport.h"
#include "fvkSimpleBlobDetector.h"

#include <QPolygon>
#include <QMouseEvent>
#include <QPainter>
#include <QPen>
#include <QSettings>

XRAYLAB_BEGIN_NAMESPACE

enum PaintDrawModeType
{
	Selection,
	Freehand,
	Line,
	Arrow,
	Rectangle,
	Circle,
	Ellipse,
	Polygon,
	Text,
	CustomBrush,
	CustomPolygon,
	CustomCircle
};
enum PaintShapeState { InProgress, Finished, Canceled };
enum PaintMouseState { Idle, Targetting, Dragging };

class XRAY_QT_API_EXPORT XrayPaintBaseShape // (abstract)
{
public:
	R3D::fvkBlobDetector& getBlobDetector();

	PaintShapeState getShapeState() const;

	const PaintDrawModeType getType() const;

	void setName(const QString &value);
	const QString& getName() const;

	void setPen(const QPen& _pen);
	const QPen& getPen() const;

	void setBrush(const QBrush& _brush);
	const QBrush& getBrush() const;

	void setFont(const QFont& _font);
	const QFont& getFont() const;

	void setSelected(bool _selected);
	bool isSelected() const;

	bool isTopLeftCornerSelected() const;
	bool isTopRightCornerSelected() const;
	bool isBottomLeftCornerSelected() const;
	bool isBottomRightCornerSelected() const;
	bool isCornerSelect() const;

	virtual const bool getIsMoving() const = 0;

	virtual XrayPaintBaseShape* clone() = 0;
	virtual void drawShape(QPainter &painter) = 0;
	virtual void drawSelectionMarker(QPainter &painter) const = 0;
	virtual void mousePressEvent(const QMouseEvent* _event, const QPoint& _pos) = 0;
	virtual void mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos) = 0;
	virtual void mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos) = 0;
	virtual bool mouseIntersectCheck(const QPoint& _pos) = 0;
	virtual void mouseDrag(const QPoint &newPos) = 0;
	virtual void cornerDrag(const QPoint &newPos)  = 0;
	virtual void wheelEvent(QWheelEvent* _event) = 0;
	virtual void nudgeUp() = 0;
	virtual void nudgeDown() = 0;
	virtual void nudgeLeft() = 0;
	virtual void nudgeRight() = 0;
	virtual const QPointF getTopLeft() const = 0;
	virtual const int calculateSurfaceArea() const = 0;
	virtual cv::Rect getBoundingRect() const = 0;

	// Function that returns the relative points of the shape contour. In other words, points local to rectangle of the shape.
	virtual std::vector<cv::Point> getShapePoints() const = 0;

	void setBlobSettings(QSettings& _settings, const QString& _key);
	void getBlobSettings(QSettings& _settings, const QString& _key);

protected:
	XrayPaintBaseShape(const PaintDrawModeType& _type = PaintDrawModeType::Selection);
	void drawCornerRect(QPainter& painter, QRect& cornerRect) const;

	R3D::fvkBlobDetector m_blob_detector;
	PaintShapeState m_shapeState;
	PaintDrawModeType m_type;
	QString m_name;
	QPen m_pen;
	QBrush m_brush;
	QFont m_font;
	bool m_isMoving;
	bool m_isSelected;

	bool m_isTopLeftCornerSelected;
	bool m_isTopRightCornerSelected;
	bool m_isBottomLeftCornerSelected;
	bool m_isBottomRightCornerSelected;
};
Q_DECLARE_METATYPE(XrayPaintBaseShape*)

class XRAY_QT_API_EXPORT XrayPaintBrushShape : public XrayPaintBaseShape
{
public:
	XrayPaintBrushShape(const QPoint& _firstPoint = QPoint());
	XrayPaintBrushShape(const XrayPaintBrushShape& _shape);
	XrayPaintBaseShape* clone() Q_DECL_OVERRIDE;

	void drawShape(QPainter &painter) Q_DECL_OVERRIDE;
	void drawSelectionMarker(QPainter &painter) const;
	void mousePressEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	bool mouseIntersectCheck(const QPoint& _pos);
	void mouseDrag(const QPoint &newPos) Q_DECL_OVERRIDE;
	void cornerDrag(const QPoint &newPos)  Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent* _event) Q_DECL_OVERRIDE;
	void nudgeUp() Q_DECL_OVERRIDE;
	void nudgeDown() Q_DECL_OVERRIDE;
	void nudgeLeft() Q_DECL_OVERRIDE;
	void nudgeRight() Q_DECL_OVERRIDE;
	const QPointF getTopLeft() const Q_DECL_OVERRIDE;
	const int calculateSurfaceArea() const Q_DECL_OVERRIDE;
	cv::Rect getBoundingRect() const Q_DECL_OVERRIDE;
	std::vector<cv::Point> getShapePoints() const Q_DECL_OVERRIDE;
	const bool getIsMoving() const Q_DECL_OVERRIDE;

	void setPoints(const QPolygonF& _points);
	const QPolygonF getPoints() const;
	void addPoint(const QPointF& _point);
	bool testX(const int mousePosX, const int pointX);
	bool testY(const int mousePosY, const int pointY);

private:
	void moveCorners(const QRect& rect) const;
	void drawCorners(QPainter& painter, const QRect& shapeRect) const;
	mutable QRect m_topLeftCornerRect;
	mutable QRect m_topRightCornerRect;
	mutable QRect m_bottomLeftCornerRect;
	mutable QRect m_bottomRightCornerRect;

	QPolygonF points;
	QPoint lastCornerPosition;
};

class XRAY_QT_API_EXPORT XrayPaintRectangleShape : public XrayPaintBaseShape
{
public:
	XrayPaintRectangleShape(const QPoint& _topLeft = QPoint());
	XrayPaintRectangleShape(const XrayPaintRectangleShape& _shape);
	XrayPaintBaseShape* clone() Q_DECL_OVERRIDE;

	void drawShape(QPainter &painter) Q_DECL_OVERRIDE;
	void drawSelectionMarker(QPainter &painter) const Q_DECL_OVERRIDE;
	void mousePressEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	bool mouseIntersectCheck(const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseDrag(const QPoint &newPos) Q_DECL_OVERRIDE;
	void cornerDrag(const QPoint &newPos)  Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent* _event) Q_DECL_OVERRIDE;
	void nudgeUp() Q_DECL_OVERRIDE;
	void nudgeDown() Q_DECL_OVERRIDE;
	void nudgeLeft() Q_DECL_OVERRIDE;
	void nudgeRight() Q_DECL_OVERRIDE;
	const QPointF getTopLeft() const Q_DECL_OVERRIDE;
	const int calculateSurfaceArea() const;
	cv::Rect getBoundingRect() const Q_DECL_OVERRIDE;
	std::vector<cv::Point> getShapePoints() const Q_DECL_OVERRIDE;
	const bool getIsMoving() const Q_DECL_OVERRIDE;

	void setRect(const QRect& _rect);
	const QRect getRect() const;

private:
	void moveCorners(const QRect& shapeRect) const;
	void drawCorners(QPainter& painter, const QRect& shapeRect) const;
	mutable QRect m_topLeftCornerRect;
	mutable QRect m_topRightCornerRect;
	mutable QRect m_bottomLeftCornerRect;
	mutable QRect m_bottomRightCornerRect;
	QRect rect;
};

class XRAY_QT_API_EXPORT XrayPaintCircleShape : public XrayPaintBaseShape
{
public:
	XrayPaintCircleShape();
	XrayPaintCircleShape(const QPoint& _centerPoint);
	XrayPaintCircleShape(const XrayPaintCircleShape& _shape);
	XrayPaintBaseShape* clone() Q_DECL_OVERRIDE;

	void drawShape(QPainter &painter) Q_DECL_OVERRIDE;
	void drawSelectionMarker(QPainter &painter) const Q_DECL_OVERRIDE;
	void mousePressEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	bool mouseIntersectCheck(const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseDrag(const QPoint &newPos) Q_DECL_OVERRIDE;
	void cornerDrag(const QPoint &newPos)  Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent* _event) Q_DECL_OVERRIDE;
	void nudgeUp() Q_DECL_OVERRIDE;
	void nudgeDown() Q_DECL_OVERRIDE;
	void nudgeLeft() Q_DECL_OVERRIDE;
	void nudgeRight() Q_DECL_OVERRIDE;
	const QPointF getTopLeft() const Q_DECL_OVERRIDE;
	const int calculateSurfaceArea() const Q_DECL_OVERRIDE;
	cv::Rect getBoundingRect() const Q_DECL_OVERRIDE;
	std::vector<cv::Point> getShapePoints() const Q_DECL_OVERRIDE;
	const bool getIsMoving() const Q_DECL_OVERRIDE;

	void setRadius(double _radius);
	double getRadius() const;

	void setSegments(int _segments);
	int getSegments() const;

	void setCenter(const QPoint& _centerPoint);
	QPoint getCenter() const;

	void updatePoints();
	const QPolygonF getPoints() const;

private:
	void moveCorners(const QRect& _rect) const;
	void drawCorners(QPainter& painter, const QRect& shapeRect) const;
	mutable QRect m_topLeftCornerRect;
	mutable QRect m_topRightCornerRect;
	mutable QRect m_bottomLeftCornerRect;
	mutable QRect m_bottomRightCornerRect;

	QPolygonF points;
	double m_radius;
	int m_segments;
	QPoint m_centerPoint;
};

class XRAY_QT_API_EXPORT XrayPaintEllipseShape : public XrayPaintBaseShape 
{
public:
	XrayPaintEllipseShape(const QPoint& _topLeft = QPoint());
	XrayPaintEllipseShape(const XrayPaintEllipseShape& _shape);
	XrayPaintBaseShape* clone() Q_DECL_OVERRIDE;

	void drawShape(QPainter &painter) Q_DECL_OVERRIDE;
	void drawSelectionMarker(QPainter &painter) const Q_DECL_OVERRIDE;
	void mouseDrag(const QPoint &newPos) Q_DECL_OVERRIDE;
	void cornerDrag(const QPoint &newPos)  Q_DECL_OVERRIDE;
	void mousePressEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	bool mouseIntersectCheck(const QPoint& _pos) Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent* _event) Q_DECL_OVERRIDE;
	void nudgeUp() Q_DECL_OVERRIDE;
	void nudgeDown() Q_DECL_OVERRIDE;
	void nudgeLeft() Q_DECL_OVERRIDE;
	void nudgeRight() Q_DECL_OVERRIDE;
	const QPointF getTopLeft() const Q_DECL_OVERRIDE;
	const int calculateSurfaceArea() const;
	cv::Rect getBoundingRect() const Q_DECL_OVERRIDE;
	std::vector<cv::Point> getShapePoints() const Q_DECL_OVERRIDE;
	const bool getIsMoving() const Q_DECL_OVERRIDE;

	void setRect(const QRect& _rect);
	const QRect getRect() const;

private:
	void moveCorners(const QRect& _rect) const;
	void drawCorners(QPainter& painter, const QRect& shapeRect) const;
	mutable QRect m_topLeftCornerRect;
	mutable QRect m_topRightCornerRect;
	mutable QRect m_bottomLeftCornerRect;
	mutable QRect m_bottomRightCornerRect;

	void generateEllipsePoints(int xc, int yc, int a, int b, float alpha);
	void generateEllipsePoints(const QRect& rect);
	QVector<QPoint> points;
	QRect rect;
};

class XRAY_QT_API_EXPORT XrayPaintLineShape : public XrayPaintBaseShape
{
public:
	XrayPaintLineShape(const QPoint& _firstPoint = QPoint());
	XrayPaintLineShape(const XrayPaintLineShape& _shape);
	XrayPaintBaseShape* clone() Q_DECL_OVERRIDE;

	void drawShape(QPainter &painter) Q_DECL_OVERRIDE;
	void drawSelectionMarker(QPainter &painter) const Q_DECL_OVERRIDE;
	void mousePressEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	bool mouseIntersectCheck(const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseDrag(const QPoint &newPos) Q_DECL_OVERRIDE;
	void cornerDrag(const QPoint &newPos)  Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent* _event) Q_DECL_OVERRIDE;
	void nudgeUp() Q_DECL_OVERRIDE;
	void nudgeDown() Q_DECL_OVERRIDE;
	void nudgeLeft() Q_DECL_OVERRIDE;
	void nudgeRight() Q_DECL_OVERRIDE;
	const QPointF getTopLeft() const Q_DECL_OVERRIDE;
	const int calculateSurfaceArea() const;
	cv::Rect getBoundingRect() const Q_DECL_OVERRIDE;
	std::vector<cv::Point> getShapePoints() const Q_DECL_OVERRIDE;
	const bool getIsMoving() const Q_DECL_OVERRIDE;

	void setLine(const QLine& _line);
	const QLine getLine() const;

protected:
	void moveCorners(const QRect& shapeRect) const;
	void drawCorners(QPainter& painter, const QRect& shapeRect) const;
	mutable QRect m_topLeftCornerRect;
	mutable QRect m_topRightCornerRect;

	QLine m_line;
	QRect boundingRectangle;
};

class XRAY_QT_API_EXPORT XrayPaintPolygonShape : public XrayPaintBaseShape
{
public:
	XrayPaintPolygonShape();
	XrayPaintPolygonShape(const QPoint& _firstPoint);
	XrayPaintPolygonShape(const XrayPaintPolygonShape& _shape);
	XrayPaintBaseShape* clone() Q_DECL_OVERRIDE;

	void drawShape(QPainter &painter) Q_DECL_OVERRIDE;
	void drawSelectionMarker(QPainter &painter) const Q_DECL_OVERRIDE;
	void mousePressEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	bool mouseIntersectCheck(const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseDrag(const QPoint &newPos) Q_DECL_OVERRIDE;
	void cornerDrag(const QPoint &newPos)  Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent* _event) Q_DECL_OVERRIDE;
	void nudgeUp() Q_DECL_OVERRIDE;
	void nudgeDown() Q_DECL_OVERRIDE;
	void nudgeLeft() Q_DECL_OVERRIDE;
	void nudgeRight() Q_DECL_OVERRIDE;
	const QPointF getTopLeft() const Q_DECL_OVERRIDE;
	const int calculateSurfaceArea() const Q_DECL_OVERRIDE;
	cv::Rect getBoundingRect() const Q_DECL_OVERRIDE;
	std::vector<cv::Point> getShapePoints() const Q_DECL_OVERRIDE;
	const bool getIsMoving() const Q_DECL_OVERRIDE;

	void removeLastPoint();
	void setPoints(const QPolygonF& points);
	const QPolygonF getPoints() const;

private:
	void moveCorners(const QRect& rect) const;
	void drawCorners(QPainter& painter, const QRect& shapeRect) const;
	mutable QRect m_topLeftCornerRect;
	mutable QRect m_topRightCornerRect;
	mutable QRect m_bottomLeftCornerRect;
	mutable QRect m_bottomRightCornerRect;

	QPolygonF points;
	QPoint lastCornerPosition;
};

class XRAY_QT_API_EXPORT XrayPaintArrowShape : public XrayPaintLineShape
{
public:
	XrayPaintArrowShape(const QPoint &firstPoint = QPoint());
	XrayPaintArrowShape(const XrayPaintArrowShape& _shape);
	XrayPaintBaseShape* clone() Q_DECL_OVERRIDE;

	void drawShape(QPainter &painter) Q_DECL_OVERRIDE;

	const int calculateSurfaceArea() const Q_DECL_OVERRIDE;

	cv::Rect getBoundingRect() const Q_DECL_OVERRIDE;
	std::vector<cv::Point> getShapePoints() const Q_DECL_OVERRIDE;
	QRectF boundingRect() const;

	void setArrowSize(qreal s);
	qreal getArrowSize() const;

	void setArrowHeads(const QPair<QPolygonF, QPolygonF>& _heads);
	QPair<QPolygonF, QPolygonF> getArrowHeads() const;

	enum ArrowDir
	{
		Left = 0,
		Right,
		Both
	};
	void setArrowDirection(int dir);
	int getArrowDirection() const;

private:
	qreal arrowSize;
	QPolygonF arrowHead1;
	QPolygonF arrowHead2;
	ArrowDir m_arrow_dir;
};

class XRAY_QT_API_EXPORT XrayPaintTextShape : public XrayPaintBaseShape
{
public:
	XrayPaintTextShape(const QPoint& _firstPoint = QPoint());
	XrayPaintTextShape(const XrayPaintTextShape& _shape);
	XrayPaintBaseShape* clone() Q_DECL_OVERRIDE;

	void drawShape(QPainter &painter) Q_DECL_OVERRIDE;
	void drawSelectionMarker(QPainter &painter) const Q_DECL_OVERRIDE;
	void mousePressEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos) Q_DECL_OVERRIDE;
	bool mouseIntersectCheck(const QPoint& _pos) Q_DECL_OVERRIDE;
	void mouseDrag(const QPoint &newPos) Q_DECL_OVERRIDE;
	void cornerDrag(const QPoint &newPos)  Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent* _event) Q_DECL_OVERRIDE;
	void nudgeUp() Q_DECL_OVERRIDE;
	void nudgeDown() Q_DECL_OVERRIDE;
	void nudgeLeft() Q_DECL_OVERRIDE;
	void nudgeRight() Q_DECL_OVERRIDE;
	const QPointF getTopLeft() const Q_DECL_OVERRIDE;
	const int calculateSurfaceArea() const Q_DECL_OVERRIDE;
	cv::Rect getBoundingRect() const Q_DECL_OVERRIDE;
	std::vector<cv::Point> getShapePoints() const Q_DECL_OVERRIDE;
	const bool getIsMoving() const Q_DECL_OVERRIDE;

	void setPoint(const QPoint& _point);
	QPoint getPoint() const;

	void setText(const QString& _text);
	QString getText() const;

private:
	QPoint m_point;
	QString m_text;
};

XRAYLAB_END_NAMESPACE

#endif // XrayPaintShapes_h__