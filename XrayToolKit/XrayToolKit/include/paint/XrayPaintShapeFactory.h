/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayPaintShapeFactory.h
** file base:	XrayPaintShapeFactory
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides shape factory for the Paint.
****************************************************************************/

#pragma once
#ifndef XrayPaintShapeFactory_h__
#define XrayPaintShapeFactory_h__

#include "XrayQtApiExport.h"
#include "XrayPaintShapes.h"

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayPaintShapeFactory
{
public:
	XrayPaintShapeFactory();
	XrayPaintBaseShape* createNewShape(const QPoint& _initialPos, const PaintDrawModeType& _shapeType);
};

XRAYLAB_END_NAMESPACE

#endif // XrayPaintShapeFactory_h__