/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/05/20
** filename: 	XrayImageFilters.h
** file base:	XrayImageFilters
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the GUI for contrast enhancement on 8,
16, 24, 32 bit images.
****************************************************************************/

#pragma once
#ifndef XrayImageFilters_h__
#define XrayImageFilters_h__

#include "XrayQtApiExport.h"

#include <QWidget>
#include <QPushButton>
#include <QGroupBox>
#include <QSettings>

#include <opencv2/opencv.hpp>

namespace Ui 
{
	class XrayImageFilters;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayImageFilters : public QWidget
{
	Q_OBJECT

public:
	explicit XrayImageFilters(QWidget *parent = nullptr);
	~XrayImageFilters();

	/*!
		Description:
		Function to write settings of this module to the given settings.
	*/
	void writeSettings(QSettings& _settings);
	/*!
		Description:
		Function to read settings from the given settings.
	*/
	void readSettings(QSettings& _settings);

public slots:
	void apply();
	void update();
	void reset();
	void preview();
	void process();

private:
	cv::Mat getFilteredImage(const cv::Mat& _mat);

	Ui::XrayImageFilters *ui;
	QWidget* p_parent;
};

XRAYLAB_END_NAMESPACE

#endif // XrayImageFilters_h__
