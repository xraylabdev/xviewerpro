/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/03/28
** filename: 	XraySaveReportAsExcelDialog.h
** file base:	XraySaveReportAsExcelDialog
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the image cropper widget with UI.
****************************************************************************/

#pragma once
#ifndef XraySaveReportAsExcelDialog_h__
#define XraySaveReportAsExcelDialog_h__

#include "XrayMainPaintWidget.h"

#include <QDialog>

namespace Ui
{
	class XraySaveReportAsExcelDialog;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XraySaveReportAsExcelDialog : public QDialog
{
	Q_OBJECT

public:
	explicit XraySaveReportAsExcelDialog(QWidget* _parent = nullptr);

	void writeSettings(QSettings& _settings);
	void readSettings(QSettings& _settings);
	
	void setCurrentPaintArea(XrayMainPaintWidget* _area);
	void setPaintAreas(const QVector<XrayMainPaintWidget*>& _areas);

	bool isCurrentPictureSelected() const;

signals:
	void statusBarMessage(const QString& text, int _timeout);

public slots:
	void cancel();
	void save();

private:
	Ui::XraySaveReportAsExcelDialog* ui;
	XrayMainPaintWidget* paintArea;
	QVector<XrayMainPaintWidget*> paintAreas;
};

XRAYLAB_END_NAMESPACE

#endif // XraySaveReportAsExcelDialog_h__
