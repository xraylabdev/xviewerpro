/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayPaintUndoCommands.h
** file base:	XrayPaintUndoCommands
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides shape factory for the Paint.
****************************************************************************/

#pragma once
#ifndef XrayPaintUndoCommands_h__
#define XrayPaintUndoCommands_h__

#include "XrayPaintShapes.h"
#include "XrayPaintShapeListModel.h"

#include <QUndoCommand>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayPaintAddShapeCommand : public QUndoCommand
{
public:
	XrayPaintAddShapeCommand(XrayPaintShapeListModel& _shapeListModel, XrayPaintBaseShape* _newShape, QUndoCommand* _parent = 0);

	void undo() override;
	void redo() override;

private:
	XrayPaintShapeListModel& shapeListModel;
	QScopedPointer<XrayPaintBaseShape> newShape;
};

class XRAY_QT_API_EXPORT XrayPaintDeleteShapeCommand : public QUndoCommand
{
public:
	XrayPaintDeleteShapeCommand(XrayPaintShapeListModel& _shapeListModel, const int& _row, QUndoCommand* _parent = 0);

	void undo() override;
	void redo() override;

private:
	int row;
	XrayPaintShapeListModel& shapeListModel;
	QScopedPointer<XrayPaintBaseShape> deletedShape;
};

class XRAY_QT_API_EXPORT XrayPaintDeleteImageCommand : public QUndoCommand
{
public:
	XrayPaintDeleteImageCommand(QImage& _image, XrayPaintShapeListModel& _model);
	~XrayPaintDeleteImageCommand();

	void undo() override;
	void redo() override;

private:
	QImage& image;
	QImage oldImage;
	XrayPaintShapeListModel& model;
	QList<XrayPaintBaseShape*> storedShapes{};
};

XRAYLAB_END_NAMESPACE

#endif // XrayPaintUndoCommands_h__