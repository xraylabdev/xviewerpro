/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayPaintShapeFactory.h
** file base:	XrayPaintShapeFactory
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a list view for Paint shapes.
****************************************************************************/

#pragma once
#ifndef XrayPaintShapeListView_h__
#define XrayPaintShapeListView_h__

#include "XrayQtApiExport.h"
#include "XrayPaintShapes.h"

#include <QMenu>
#include <QListView>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayPaintShapeListView : public QListView
{
	Q_OBJECT

public:
	XrayPaintShapeListView(QWidget* parent = 0);

signals:
	void shapeChanged();

public slots:
	void on_custom_contextMenu_requested(const QPoint& pos);
	void on_action_deleteShape_triggered();
	void on_action_deleteShapes_triggered();
	void on_action_selected_deleteShape();
	void on_action_moveShapeUp_triggered();
	void on_action_moveShapeDown_triggered();
	void selectShape(const XrayPaintBaseShape* const newShape);
	void updateSelection(const QModelIndex& topLeft, const int& first, const int& last);

private:
	void initializeContextMenu();

	QMenu* menu_Context;
	QAction* action_DeleteShape;
	QAction* action_MoveShapeUp;
	QAction* action_MoveShapeDown;
	QModelIndex itemIndex;
};

XRAYLAB_END_NAMESPACE

#endif // XrayPaintShapeListView_h__