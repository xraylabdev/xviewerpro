/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/05/20
** filename: 	XrayFlashFilter.h
** file base:	XrayFlashFilter
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the GUI for contrast enhancement on 8,
16, 24, 32 bit images.
****************************************************************************/

#pragma once
#ifndef XrayFlashFilter_h__
#define XrayFlashFilter_h__

#include "XrayQtApiExport.h"
#include "fvkContrastEnhancerUtil.h"

#include <QWidget>
#include <QPushButton>
#include <QGroupBox>
#include <QSettings>

#include <opencv2/opencv.hpp>

namespace Ui 
{
	class XrayFlashFilter;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayFlashFilter : public QWidget
{
	Q_OBJECT

public:
	explicit XrayFlashFilter(QWidget *parent = nullptr);
	~XrayFlashFilter();

	/*!
		Description:
		Function that returns a pointer to batch process directories box.
	*/
	QGroupBox* getDirectoriesBox() const;

	/*!
		Description:
		Function that returns a pointer to batch process button.
	*/
	QPushButton* getProcessBtn() const;

	/*!
		Description:
		Function to write settings of this module to the given settings.
	*/
	void writeSettings(QSettings& _settings);
	/*!
		Description:
		Function to read settings from the given settings.
	*/
	void readSettings(QSettings& _settings);

public slots:
	void apply();
	void reset();
	void preview();
	void process();

	void loadPresets();
	void savePresets();
	void savePreset();
	void removePreset();
	void selectPreset(int _index);
	void updateWithCurrentPreset();

	void startIntensityRange(bool value);
	void adjustIntensityRange();

private:
	void setRangeSlidersMinMax(int _min, int _max);
	void setRangeSlidersEnabled(bool b);
	cv::Mat getFilteredImage(const cv::Mat& _mat);
	void setParametersToSliders(R3D::fvkContrastEnhancerUtil::Params _params);
	R3D::fvkContrastEnhancerUtil::Params getParametersFromSliders();

	Ui::XrayFlashFilter *ui;
	QWidget* p_parent;

	QList<R3D::fvkContrastEnhancerUtil::Params> m_presets;
};

XRAYLAB_END_NAMESPACE

#endif // XrayFlashFilter_h__
