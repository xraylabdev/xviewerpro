/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayPaintMainWindow.h
** file base:	XrayPaintMainWindow
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the main paint window.
****************************************************************************/

#pragma once
#ifndef XrayPaintMainWindow_h__
#define XrayPaintMainWindow_h__

#include "XrayQtApiExport.h"
#include "XrayPaintShapeListView.h"
#include "XrayMainPaintWidget.h"
#include "XrayQRecentFilesMenu.h"
#include "XrayStatusBar.h"
#include "XrayResizeDialog.h"
#include "XrayFlashFilter.h"
#include "XrayVoidAnalysisFilter.h"
#include "XrayBGAInspectionFilter.h"

#include <QApplication>
#include <QMainWindow>
#include <QClipboard>
#include <QStatusBar>
#include <QFileDialog>
#include <QMenuBar>
#include <QDockWidget>
#include <QButtonGroup>
#include <QRadioButton>
#include <QComboBox>
#include <QPen>
#include <QToolBar>
#include <QGroupBox>
#include <QSettings>
#include <QFontComboBox>
#include <QToolBox>
#include <QToolButton>
#include <QMouseEvent>
#include <QScrollArea>

XRAYLAB_BEGIN_NAMESPACE

class XrayStandardModelCompleterTreeViewWidget;
class XrayStandardModelTreeView;
class TreeViewSortSourceModel;

class XRAY_QT_API_EXPORT XrayPaintTabWidget : public QTabWidget
{
	Q_OBJECT

public:
	explicit XrayPaintTabWidget(QWidget* parent = nullptr);


signals:
	void tabAboutToCloseOnSave(int index);
	void tabAboutToClose(int index);

public slots:
	void deleteTab(int index);
	void deleteTab(const QString& tabText);
	void deleteTabWithWarning(int index);
};

class XRAY_QT_API_EXPORT XrayPaintMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	/*!
		Description:
		Default constructor to create a main window.
	*/
	explicit XrayPaintMainWindow(QWidget* parent = nullptr);

	/*!
		Description:
		Function that loads a project into this main widget.
	*/
	void openProject(const QString& fileName);
	/*!
		Description:
		Function that loads a list of images into this main widget.
	*/
	void openFiles(const QStringList& fileNames);

	/*!
		Description:
		Function that loads the given file name.
	*/
	bool openFile(const QString& fileName);

	/*!
		Description:
		Function that loads a list of images into this main widget.
	*/
	void openFiles(const QVector<QPair<QString, QString>>& fileNames);
	/*!
		Description:
		Function that loads all directory images into this main widget.
	*/
	void openDirectory(const QString& _path);

	/*!
		Description:
		Function that activates the last fileName from the given list.
		It looks for the tab that already exists and activates the last one.
		It returns the activated tab index.
	*/
	int setCurrentTabIndex(const QStringList& fileNames);

	/*!
		Description:
		Function that closes all opened images.
	*/
	void closeAllFiles();

	bool saveAsProjectImages(const QString& projectFilePath);
	bool copyAllOpenedImages(const QString& dirPath);

	/*!
		Description:
		Function that returns all images with its filename and caption.
	*/
	QVector<std::tuple<QString, QString, QImage>> getImagesWithCaptionsAndFiles();
	/*!
		Description:
		Function that returns all loaded filenames and captions.
	*/
	void changeImagesLocation(const QString& dirPath);
	/*!
		Description:
		Function that returns all loaded filenames and captions.
	*/
	QVector<QPair<QString, QString>> getFileNamesAndCaptions();

	/*!
		Description:
		Function that returns true if anti-aliasing is enabled.
	*/
	bool getUseAntialiasing() const;

	/*!
		Description:
		Function that returns the pen used by shapes.
	*/
	QPen getPen() const;
	/*!
		Description:
		Function that returns the brush used by shapes.
	*/
	QBrush getBrush() const;
	/*!
		Description:
		Function that returns the draw mode used by shapes.
	*/
	PaintDrawModeType getMode() const;
	/*!
		Description:
		Function that returns the font used by text shape.
	*/
	QFont getFont() const;
	/*!
		Description:
		Function that returns the arrow direction used by arrow shapes.
	*/
	int getArrowDirection() const;
	/*!
		Description:
		Function that returns the arrow width used by arrow shapes.
	*/
	int getArrowSize() const;

	/*!
		Description:
		Function to zoom the paint area by specifying the scale factor.
	*/
	void zoom(double _scale);
	/*!
		Description:
		Function that returns the zoom factor.
	*/
	double zoom() const;

	/*!
		Description:
		Function that returns the number of images that can be used to align the images in the reporting module.
	*/
	int getNumImagesPerPage() const;

	/*!
		Description:
		Function that returns a pointer to current paint area.
	*/
	XrayMainPaintWidget* getCurrentPaintArea() const;

	/* function that returns the void filter */
	XrayVoidAnalysisFilter* XrayPaintMainWindow::getVoidFilter() const;
	/*!
		Description:
		Function that returns a list of all valid paint areas.
	*/
	QVector<XrayMainPaintWidget*> getPaintAreas() const;

	void XrayPaintMainWindow::setActiveTab(int _index);

	/*!
		Description:
		Function that returns a pointer to XEnahncer batch process directories box.
	*/
	void setEnhancerDirectoriesBoxVisible(bool _b);
	/*!
		Description:
		Function to enable or disable the demo version features.
	*/
	void enableDemoVersion(const QString& _modules, bool _b);
	/*!
		Description:
		Function to enable or disable the demo version features.
	*/
	void updateAllPaintWidgets();

	bool saveProjectSettings(const QString& fileName);

	/*!
		Description:
		Function that writes the current settings to the given settings object.
	*/
	void writeSettings(QSettings& _settings, const QString& projectFileName = QString());
	/*!
		Description:
		Function that reads settings and updates all relevant widgets.
	*/
	void readSettings(QSettings& _settings, const QString& projectFileName = QString());
	/*!
		Description:
		Function that writes the current settings to the given settings object.
	*/
	void writeShapeSettings(QSettings& _settings);
	/*!
		Description:
		Function that reads settings and updates all relevant widgets.
	*/
	void readShapeSettings(QSettings& _settings);

	/*!
		Description:
		Function to hide or show the toolbar that is specific to XReport Pro module only.
	*/
	void setReportingToolBarVisible(bool _b);

	/*!
		Description:
		Function that returns a pointer to the status bar.
	*/
	auto statusBar() const { return p_statusBar; }

	/*!
		Description:
		Function that returns a pointer to the status bar.
	*/
	auto getExitAction() const { return action_Exit; }

protected:
	QWidget* centralWidget;
	QHBoxLayout* centralWidgetLayout;
	QDockWidget* dockWidget;
	QDockWidget* leftTreeDockWidget;
	XrayStandardModelCompleterTreeViewWidget* projTreeView;
	QClipboard* clipboard;
	XrayPaintTabWidget* documentTab;
	XrayStatusBar* p_statusBar;
	XrayFlashFilter* p_flashfilter;
	XrayVoidAnalysisFilter* p_voidFilter;
	XrayBGAInspectionFilter* p_bgaFilter;

	QAction* action_OpenProj;
	QAction* action_SaveProj;
	QAction* action_SaveProjAs;
	QAction* action_Open;
	QAction* action_OpenDir;
	QAction* action_Reload;
	QAction* action_Close;
	QAction* action_CloseAll;
	QAction* action_Save;
	QAction* action_SaveAs;
	QAction* action_Print;
	QAction* action_Caption;
	QAction* action_EditText;
	QAction* action_NumImages;
	QAction* action_Exit;
	QAction* action_Undo;
	QAction* action_Redo;
	QAction* action_Copy;
	QAction* action_Paste;
	QAction* action_Delete;
	QAction* action_Erase;
	QAction* action_ResizeImage;
	QAction* action_About;
	QAction* action_ToggleAntiAliasing;

	XrayQRecentFilesMenu* menu_recentProjFiles;
	XrayQRecentFilesMenu* menu_recentFiles;
	QMenu* menu_File;
	QMenu* menu_FileOpen;
	QMenu* menu_Edit;

	QMenu* menu_View;
	QComboBox* zoomCombo;
	QAction* action_zoomIn;
	QAction* action_zoomOut;
	QAction* action_zoomOriginal;
	QAction* action_zoomAllOriginal;
	QAction* action_BackgroundTransparent;
	QAction* action_BackgroundWhite;
	QAction* action_BackgroundGrid;
	QAction* action_Crosshair;
	QAction* action_Grid;
	QAction* action_GridSize;

	QMenu* menu_Help;

	QToolBar* shapesToolBar;
	QToolBar* fileToolBar;
	QToolBar* textToolBar;
	QToolBar* editToolBar;
	QToolBar* colorToolBar;
	QToolBar* dialogsToolbar;
	QToolBar* zoomToolbar;

	QActionGroup* actiongroup_Tools;
	QAction* tool_Select;
	QAction* tool_Freehand;
	QAction* tool_Line;
	QAction* tool_Rectangle;
	QAction* tool_Circle;
	QAction* tool_Ellipse;
	QAction* tool_Polygon;
	QAction* tool_Arrow;
	QAction* tool_Text;
	QAction* tool_CustomCurve;
	QAction* tool_CustomPolygon;
	QAction* tool_CustomCircle;

	QTabWidget* optionsTab;
	QVBoxLayout* optionsTabLayout;

	QPushButton* button_strokeColor;
	QSlider* slider_strokecolor_red;
	QSlider* slider_strokecolor_green;
	QSlider* slider_strokecolor_blue;
	QSpinBox* spinbox_strokecolor_red;
	QSpinBox* spinbox_strokecolor_green;
	QSpinBox* spinbox_strokecolor_blue;
	QSlider* slider_stroke_width;
	QSpinBox* spinbox_stroke_width;
	QSlider* slider_arrow_width;
	QSpinBox* spinbox_arrow_width;
	QSlider* slider_stroke_opacity;
	QSpinBox* spinbox_stroke_opacity;
	QButtonGroup* buttongroup_capstyle;
	QRadioButton* radiobutton_roundcap;
	QRadioButton* radiobutton_flatcap;
	QComboBox* combobox_penstyle;
	QComboBox* combobox_arrowdir;

	QPushButton* button_fillColor;
	QSlider* slider_fillcolor_red;
	QSlider* slider_fillcolor_green;
	QSlider* slider_fillcolor_blue;
	QSpinBox* spinbox_fillcolor_red;
	QSpinBox* spinbox_fillcolor_green;
	QSpinBox* spinbox_fillcolor_blue;
	QSlider* slider_fill_opacity;
	QSpinBox* spinbox_fill_opacity;
	QComboBox* combobox_brushstyle;

	XrayPaintShapeListView* shapeListView;
	QLabel* label_mousecoordinates;
	QLabel* label_selectedShape;
	QLabel* label_currentSurface;
	QLabel* label_totalSurface;

	QComboBox* itemColorCombo;
	QComboBox* textColorCombo;
	QComboBox* fontSizeCombo;
	QFontComboBox* fontCombo;

	QToolBox* toolBox;
	QButtonGroup* buttonGroup;
	QButtonGroup* pointerTypeGroup;
	QButtonGroup* backgroundButtonGroup;
	QToolButton* fillColorToolButton;
	QToolButton* lineColorToolButton;
	QAction* boldAction;
	QAction* underlineAction;
	QAction* italicAction;
	QAction* fillColorAction;
	QAction* lineColorAction;
	void itemColorChanged(QAction* action, bool _isFill);

	QSignalMapper* colorMapper;
	bool m_use_antialiasing;
	PaintDrawModeType m_mode;
	QBrush m_brush;
	QPen m_pen;
	QFont m_font;
	int m_arrow_dir;
	int m_arrow_size;
	int m_numImagesPerPage;
	double m_zoomScale;

	int m_pageId;

	QString m_projectFilePath;

	QVector<XrayPaintBaseShape*> selectedShapes;

	void closeEvent(QCloseEvent* _event) Q_DECL_OVERRIDE;
	void dragEnterEvent(QDragEnterEvent* event) Q_DECL_OVERRIDE;
	void dropEvent(QDropEvent* event) Q_DECL_OVERRIDE;

	void initializeUi();
	void initializeMenus();
	void initializeToolBar();
	void initializeStatusBar();
	void initializeOptionsTab();
	void createNewTabPage(const QString& fileName, const QString& caption);
	void updateTotalSurfaceArea();

	void updateWidgetsOnSelection(const XrayPaintBaseShape* const shape);
	void updateWidgetsOnSelection();
	void updateFontWidgetsOnSelection();
	void updateArrowWidgetsOnSelection();

	double getFitToViewportFactor(int _paintAreaIndex);

signals:
	void drawModeChanged(const PaintDrawModeType&);

public slots:
	void on_action_closeAllFiles_triggered();
	/*!
		Description:
		Function to fit the last image to the viewport size.
	*/
	void fitToViewportLastView();
	/*!
		Description:
		Function to fit the first image to the viewport size.
	*/
	void fitToViewportFirstView();
	/*!
		Description:
		Function to fit the current image to the viewport size.
	*/
	void fitToViewportCurrentView();
	/*!
		Description:
		Function to fit all images to the viewport size.
	*/
	void fitToViewportAllViews(bool _toViewPort = true);

private slots:
	QMenu* createColorMenu(const char* slot, QColor defaultColor);
	QIcon createColorToolButtonIcon(const QString& image, QColor color);
	QIcon createColorIcon(QColor color);
	void handleFontChange();
	void zoomWithPercent(const QString& scale);
	void fontSizeChanged(const QString&);
	void currentFontChanged(const QFont&);
	void itemColorChanged();
	void lineColorChanged();
	void fillToolButtonTriggered();
	void lineToolButtonTriggered();
	void updateTextShapeText();
	void doubleClickEvent(QMouseEvent* _event);
	void onShapeCreated(XrayPaintBaseShape* const shape);

	void on_action_OpenProj_triggered();
	void on_action_SaveProj_triggered();
	void on_action_SaveProjAs_triggered();
	void on_action_Reload_triggered();
	void on_action_Undo_triggered();
	void on_action_Redo_triggered();
	void on_action_newFile_triggered();
	void on_action_fileCaption_triggered();
	void on_action_numImages_triggered();
	void on_action_openFile_triggered();
	void on_action_openDir_triggered();
	void on_action_closeFile_triggered();
	void on_action_save_triggered();
	void on_action_saveFileAs_triggered();
	void on_action_print_triggered();
	void on_action_copy_triggered();
	void on_action_paste_triggered();
	void on_action_deleteAllShapes_triggered();
	void on_action_resizeImage_triggered();
	void on_action_ToggleToolBar_triggered();
	void on_action_ToggleAntiAliasing_triggered();
	void on_action_About_triggered();
	void on_actiongroup_Tools_triggered(QAction* pressedAction);
	void on_documentTab_currentChanged(int newIndex);
	void on_colorChanged(const QString& changed);
	void on_buttonGroup_capType_buttonClicked(QAbstractButton* btn);
	void on_combobox_penStyle_activated(int index);
	void on_combobox_brushStyle_activated(int index);
	void on_combobox_arrowDir_activated(int index);
	void on_slider_arrowWidth_changed(int _width);
	void update_mousePosition(const QPoint& mousePos);
	void update_paintArea();
	void on_paintArea_selectionChanged(XrayPaintBaseShape* const shape);
	void on_paintArea_filenameChanged(const QString& newName);

	void tabAboutToCloseOnSave(int index);
	void tabAboutToClose(int index);

	void customShapeChanged(const QVector<QPointF>& _points);

	void activateTabFromTreeItem(const QString& fileName);
	void activateTreeItemFromTab(int index);
	void deleteTabFromTreeItem(const QString& fileName);
	void deleteTreeItemFromTab(int index);
	void syncTabsWithTreeModel();
	void syncTreeModelWithTabs();
};

XRAYLAB_END_NAMESPACE

#endif // XrayPaintMainWindow_h__
