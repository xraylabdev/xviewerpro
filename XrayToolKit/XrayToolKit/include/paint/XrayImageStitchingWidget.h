/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/11/22
** filename: 	XrayImageStitchingWidget.h
** file base:	XrayImageStitchingWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the image stitching widget with UI.
****************************************************************************/

#pragma once
#ifndef XrayImageStitchingWidget_h__
#define XrayImageStitchingWidget_h__

#include "XrayMainPaintWidget.h"
#include <QToolButton>

namespace Ui
{
	class XrayImageStitchingWidget;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayStitchPaintWidget : public XrayMainPaintWidget
{
	Q_OBJECT

public:
	/*!
		Description:
		Default constructor to create a widget by specifying the image file name and paint area.
	*/
	explicit XrayStitchPaintWidget(QWidget* _parent = nullptr);

//protected:
//	void mousePressEvent(QMouseEvent* _event) Q_DECL_OVERRIDE;
};


class XRAY_QT_API_EXPORT XrayImageStitchingWidget : public QWidget
{
	Q_OBJECT

public:
	explicit XrayImageStitchingWidget(QWidget* _parent = nullptr);

	void enableDemoVersion(bool b);

signals:
	void statusBarMessage(const QString& text, int _timeout);
	void openInXPaint(const QString& fileName);

public slots:
	void browseImages();
	void browseLeftImage();
	void browseRightImage();
	void apply();
	void save();
	void editInXPaint();
	void next();
	void prev();
	void repeat();

private:
	void createToolButtonsActions(XrayMainPaintWidget* paint, QToolButton* zoomIn, QToolButton* zoomOut, QToolButton* flip, QToolButton* rotate);

	Ui::XrayImageStitchingWidget* ui;
	XrayMainPaintWidget* leftPaint;
	XrayMainPaintWidget* rightPaint;
	QStringList m_fileList;
	int m_currentIndex;
	QImage m_lastImage;

	bool m_demoVersion;
};

XRAYLAB_END_NAMESPACE

#endif // XrayImageStitchingWidget_h__
