/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayMainPaintWidget.h
** file base:	XrayMainPaintWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the main paint widget.
****************************************************************************/

#pragma once
#ifndef XrayMainPaintWidget_h__
#define XrayMainPaintWidget_h__

#include "XrayQtApiExport.h"
#include "XrayPaintShapeListModel.h"
#include "XrayPaintShapes.h"
#include "XrayPaintUndoCommands.h"

#include <QItemSelection>
#include <QStyleOption>
#include <QWidget>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayMainPaintWidget : public QWidget
{
	Q_OBJECT

public:
	/*!
		Description:
		Default constructor to create a widget by specifying it's parent.
	*/
	explicit XrayMainPaintWidget(QWidget* _parent = nullptr);
	/*!
		Description:
		Constructor to create a widget by specifying the image file name and paint area.
	*/
	explicit XrayMainPaintWidget(const QString& _fileName, const QSize& _canvasSize = QSize(800, 600), QWidget* _parent = nullptr);

	/*!
		Description:
		Function to load an image from disk.
	*/
	bool load(const QString& _fileName);
	/*!
		Description:
		Function to reload the existing image from the disk.
	*/
	bool reload();
	/*!
		Description:
		Function to reload the existing image from the disk.
		/return the reloaded image.
	*/
	QImage reloadImage();
	/*!
		Description:
		Function that returns the paint image with all shapes.
	*/
	QImage copyOriginalImage(bool _grayToRGB);
	QImage copyOriginalImage(XrayPaintBaseShape* _shape, bool _grayToRGB);
	QImage copyImage(bool _grayToRGB);
	QImage copyImage(XrayPaintBaseShape* _shape);
	QImage copyImage();
	QImage getWaterMarkedImage(const QImage& _watermark, double _opacity, bool _grayToRGB);
	QImage getWaterMarkedOriginalImage(const QImage& _watermark, double _opacity, bool _grayToRGB);

	XrayPaintBaseShape* getTextBoxShape(const QString& text);
	QImage drawRelatedTextBoxOnImage(XrayPaintBaseShape* shape, QImage& img);
	QImage drawAllShapesOnImage(QImage& img, PaintDrawModeType type);
	QImage drawShapes(const QImage& _img, XrayPaintBaseShape* _shape, bool _grayToRGB);
	QImage getOriginalImageWithVoids(XrayPaintBaseShape* shape, bool drawTypeShapes, PaintDrawModeType type);

	/*!
		Description:
		Function to resize the paint area along with the image.
		/param _newSize : new size of the paint area.
	*/
	void resizeImage(const QSize& _newSize);
	/*!
		Description:
		Function to delete the image from the paint area.
	*/
	void deleteImage();
	/*!
		Description:
		Function that paints all shapes to the image and then clears the shape list.
	*/
	void rasterizeAndClearShapes();

	/*!
		Description:
		Function that performs undo.
	*/
	void undo();
	/*!
		Description:
		Function that performs redo.
	*/
	void redo();

	/*!
		Description:
		Function that copies the given _image to the internal image.
		Perform update after that in order to update the paint area.
	*/
	void setImage(const cv::Mat& _mat);
	void setImage(const QImage& _image);
	/*!
		Description:
		Function that returns a reference to the internal image.
	*/
	QImage& getImage() const;
	/*!
		Description:
		Function that returns a reference to the internal original image.
		Original image is the disk image or the image that you don't want to perform any operation on.
	*/
	QImage& getOriginalImage() const;
	/*!
		Description:
		Function that returns a reference to the original image if it was 16 bit.
	*/
	cv::Mat& getImage16();
	/*!
		Description:
		Function that returns a reference to the original image if it was 32 bit.
	*/
	cv::Mat& getImage32();
	/*!
		Description:
		Function that copies the current internal image to the original image.
		Its important when you want to use the internal image as the original image.
	*/
	void setCurrentImageAsOriginalImage();
	/*!
		Description:
		Function to copy the original image to the internal image.
		Original image is the disk image or the image that you don't want to perform any operation on.
	*/
	void resetImage();
	/*!
		Description:
		Function to set the given image to the current image and the original image and update the widget.
	*/
	void resetImage(const QImage& _image);
	/*!
		Description:
		Function to embed the given image onto the internal image.
	*/
	void pasteImage(const QImage& _newImage);
	/*!
		Description:
		Function to save the current painted image (with all shapes) at the given file name.
	*/
	bool saveImage(const QString& _fileName);
	/*!
		Description:
		Function to rotate the internal image.
	*/
	void rotate(double _angle);
	/*!
		Description:
		Function to flip the internal image.
		/param _mode = 0 => Horizontal, _mode = 1 => Vertical, _mode = -1 => Both
	*/
	void flip(int _mode);
	/*!
		Description:
		Function to crop the image so it only contains the current selected shape.
	*/
	void crop();

	/*!
		Description:
		Function that returns the file name of the loaded image.
	*/
	void setFileName(const QString& _fileName);
	QString getFileName() const;
	/*!
		Description:
		Function to set the caption information to this widget.
	*/
	void setFileCaption(const QString& _text);
	/*!
		Description:
		Function that returns the caption information of this widget.
	*/
	QString getFileCaption() const;


	/*!
		Description:
		Function to zoom the paint area by specifying the scale factor.
	*/
	void zoom(double _scale);
	/*!
		Description:
		Function that returns the zoom factor.
	*/
	double zoom() const;

	/*!
		Description:
		Function to fit image to the specified size.
	*/
	void fitToViewSize(const QSize& _size);

	/*!
		Description:
		Function to zoom in by 0.125.
	*/
	void zoomIn();
	/*!
		Description:
		Function to zoom out by 0.125.
	*/
	void zoomOut();

	/*!
		Description:
		Function that copies the selected shape ROI to the internal data structure.
		This function helps to set same shape ROI at different paint areas.
	*/
	void copyShapeRoi();
	/*!
		Description:
		Function that sets the copied shape ROI to the selected shape.
		This function helps to set same shape ROI at different paint areas.
	*/
	void pasteShapeRoi();

	/*!
		Description:
		Function that returns the total surface area of the shapes.
	*/
	int calculateTotalSurface() const;

	/*!
		Description:
		Function that returns a pointer to undo stack.
	*/
	QUndoStack* getUndoStack() const;

	/*!
		Description:
		Function to add a pointer to shape to shape in progress.
		In case of rendering only shape, just use this method and that's all.
	*/
	void addShapeInProgress(XrayPaintBaseShape* shape);

	/*!
		Description:
		Function that creates a new shape.
	*/
	void addShape(XrayPaintBaseShape* shape);
	/*!
		Description:
		Function that creates a new shape from the copied shape in the current paint area.
	*/
	void pasteShape(const QVector<XrayPaintBaseShape*>& shapes);

	/*!
		Description:
		Function that returns a pointer to shape list model.
	*/
	XrayPaintShapeListModel* getShapeListModel();
	/*!
		Description:
		Function to set a pointer to a selected shape.
	*/
	void setSelectedShape(XrayPaintBaseShape* shape);
	/*!
		Description:
		Function that returns a pointer to selected shape.
	*/
	XrayPaintBaseShape* getSelectedShape() const;
	/*!
		Description:
		Function that reset the shape list model.
	*/
	void resetShapeListModel();

	/*!
		Description:
		Function to set the given color to the internal image.
	*/
	void setBackgroundColor(const QColor& _color);
	/*!
		Description:
		Function to set a background image by specifying the image file name.
	*/
	void setBackground(const QString& fileName);

	/*!
		Description:
		Function that returns true if any modification has been made to this widget.
	*/
	bool getIsModified() const;

	/*!
		Description:
		Function set visibility of the crosshair in the paint area.
	*/
	void setCrosshairEnabled(bool _b);
	/*!
		Description:
		Function that returns true if the crosshair is enabled.
	*/
	bool isCrosshairEnabled();
	/*!
		Description:
		Function that toggles the visibility of crosshair.
	*/
	void toggleCrosshair();
	/*!
		Description:
		Function set visibility of the crosshair in the paint area.
	*/
	void setGridEnabled(bool _b);
	/*!
		Description:
		Function that returns true if the crosshair is enabled.
	*/
	bool isGridEnabled();
	/*!
		Description:
		Function that toggles the visibility of grid.
	*/
	void toggleGrid();

	/*!
		Description:
		Function set visibility of the crosshair in the paint area.
	*/
	void setCrosshairColor(const QColor& _c);
	/*!
		Description:
		Function that returns true if the crosshair is enabled.
	*/
	QColor getCrosshairColor();
	/*!
		Description:
		Function set visibility of the crosshair in the paint area.
	*/
	void setGridColor(const QColor& _c);
	/*!
		Description:
		Function that returns true if the crosshair is enabled.
	*/
	QColor getGridColor();
	/*!
		Description:
		Function set visibility of the crosshair in the paint area.
	*/
	void setGridSize(int _size);
	/*!
		Description:
		Function that returns true if the crosshair is enabled.
	*/
	int getGridSize();

	/*!
		Description:
		Static function to set the watermark image which will be used for the DEMO version.
	*/
	static void setWatermarkImage(const QImage& _img);
	/*!
		Description:
		Static function to enable the watermark image.
	*/
	static void setWatermarkEnabled(bool _b);
	/*!
		Description:
		Static function that returns true if the watermark image is enabled.
	*/
	static bool isWatermarkEnabled();

	/*!
		Description:
		Function to get the mouse clicked position.
	*/
	QPoint getMouseClickPos() const { return m_mouse_press_pos; }

	auto& getSelectedShapes() { return selectedShapes; }

signals:
	void mouseLeftClicked(const QPoint& mousePos);
	void mouseMoved(const QPoint& mousePos);
	void selectionChanged(XrayPaintBaseShape* const);
	void onShapeCreated(XrayPaintBaseShape* const);
	void filenameChanged(const QString);
	void doubleClicked(QMouseEvent* _event);
	void customShapeChanged(const QVector<QPointF>& _points);

public slots:
	void setMode(const PaintDrawModeType& newMode);
	void selectShape(const QItemSelection& selected, const QItemSelection& deselected);

protected:
	void paintEvent(QPaintEvent* _event) Q_DECL_OVERRIDE;
	void mousePressEvent(QMouseEvent* _event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent* _event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent* _event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent* e) Q_DECL_OVERRIDE;
	void keyPressEvent(QKeyEvent* _event) Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent* event) Q_DECL_OVERRIDE;
	void drawWatermarkImagePaintEvent(QPainter& _painter);
	void drawWatermarkImage(const QImage& _watermark, QPainter& _painter);
	void drawWatermarkImage(QPainter& _painter);
	void drawCrosshair(QPainter& _painter);
	void drawGrid(QPainter& _painter);

private:
	/*!
		Description:
		Function that returns a shape which is currently under the given position.
	*/
	XrayPaintBaseShape* getShapeUnderMouse(const QPoint& _pos);
	/*!
		Description:
		Function that changes the mouse cursor on selection, dragging, etc.
	*/
	void changeMouseCursor();
	/*!
		Description:
		Function that changes the mouse cursor on selection, dragging, etc.
	*/
	void drawShapeInProgress();
	/*!
		Description:
		Function that paints all shapes to the paint area.
	*/
	void drawShapesInList();
	/*!
		Description:
		Function that paints all custom shapes to the paint area.
	*/
	void drawCustomShapes();
	/*!
		Description:
		Function that returns a new name of the shape as incremental numbered string.
	*/
	QString createShapeName(PaintDrawModeType type);

	QWidget* p_parent;
	bool isModified;
	bool isMousePressed;
	bool m_isMiddleBtnPressed;
	PaintMouseState m_mouse_state;
	QPoint m_mouse_press_pos;
	QPoint m_mouse_press_offset;
	QString m_file_name;
	QString m_file_caption;
	QSize m_canvas_size;
	PaintDrawModeType m_mode;
	QUndoStack* undoStack;
	QScopedPointer<XrayPaintShapeListModel> shapeListModel;
	QPainter m_painter;

	static QImage m_watermark;
	static bool m_isWatermark;

	QColor m_crosshairColor;
	bool m_isCrosshair;
	QColor m_gridColor;
	bool m_isGrid;
	int m_gridSize;

	int rectangleCount;
	int circleCount;
	int ellipseCount;
	int polylineCount;
	int freelineCount;
	int lineCount;
	int arrowCount;
	int textCount;

	static double m_lastCircleRadius;

	QScopedPointer<XrayPaintBrushShape> customBrushShape;
	QScopedPointer<XrayPaintPolygonShape> customPolygonShape;
	QScopedPointer<XrayPaintCircleShape> customCircleShape;

protected:
	QScopedPointer<QImage> m_image;
	QScopedPointer<QImage> m_backup_image;
	cv::Mat m_image16;
	cv::Mat m_image32;

	XrayPaintBaseShape* selectedShape;
	QVector<XrayPaintBaseShape*> selectedShapes;
	QScopedPointer<XrayPaintBaseShape> shapeInProgress;
	double m_scale;
};

XRAYLAB_END_NAMESPACE

#endif // XrayMainPaintWidget_h__
