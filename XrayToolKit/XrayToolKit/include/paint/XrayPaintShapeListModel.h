/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayPaintShapeListModel.h
** file base:	XrayPaintShapeListModel
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a model for Paint shapes.
****************************************************************************/

#pragma once
#ifndef XrayPaintShapeListModel_h__
#define XrayPaintShapeListModel_h__

#include "XrayQtApiExport.h"
#include "XrayPaintShapes.h"

#include <QWidget>
#include <QItemSelection>
#include <QUndoStack>

XRAYLAB_BEGIN_NAMESPACE

class XrayMainPaintWidget;

class XRAY_QT_API_EXPORT XrayPaintShapeListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    XrayPaintShapeListModel(XrayMainPaintWidget* parent = 0);
    void                 insertShapeAt(XrayPaintBaseShape* newShape, int row);
    XrayPaintBaseShape*  removeShapeAt(int row);
    void                 swapList(QList<XrayPaintBaseShape*>& newList);
    void                 moveShapeUp(int row);
    void                 moveShapeDown(int row);
    void                 renameShape(const QString& newName);
    void                 deleteShapeViaContextMenu (int row);
	void clear();
    const QList<XrayPaintBaseShape*>& getShapeListConst() const;
    QList<XrayPaintBaseShape*>&   getShapeList();

private:
    QVariant             headerData(int section, Qt::Orientation orientation, int role) const Q_DECL_OVERRIDE;
    QVariant             data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    bool                 setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;
    bool                 insertRows(int row, int count, const QModelIndex &parent) Q_DECL_OVERRIDE;
    bool                 removeRows(int row, int count, const QModelIndex &parent) Q_DECL_OVERRIDE;
    int                  rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    Qt::ItemFlags        flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    Qt::DropActions      supportedDropActions() const Q_DECL_OVERRIDE;
    QUndoStack&          undoStack;
    QList<XrayPaintBaseShape*> shapeList;
};

XRAYLAB_END_NAMESPACE

#endif // XrayPaintShapeListModel_h__