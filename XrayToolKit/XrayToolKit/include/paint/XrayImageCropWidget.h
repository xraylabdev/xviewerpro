/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/11/27
** filename: 	XrayImageCropWidget.h
** file base:	XrayImageCropWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the image cropper widget with UI.
****************************************************************************/

#pragma once
#ifndef XrayImageCropWidget_h__
#define XrayImageCropWidget_h__

#include "XrayMainPaintWidget.h"
#include <QToolButton>

namespace Ui
{
	class XrayImageCropWidget;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayImageCropWidget : public QWidget
{
	Q_OBJECT

public:
	explicit XrayImageCropWidget(QWidget* _parent = nullptr);

	void setImage(const QImage& image);
	void fitImage();
	void setROI(const QRect& roi);

	QImage croppedImage();

signals:
	void statusBarMessage(const QString& text, int _timeout);
	void ok();
	void cancel();

public slots:
	void save();
	void reset();

private:

	Ui::XrayImageCropWidget* ui;
	XrayMainPaintWidget* paintArea;
	XrayPaintRectangleShape* rectShape;
};

XRAYLAB_END_NAMESPACE

#endif // XrayImageCropWidget_h__
