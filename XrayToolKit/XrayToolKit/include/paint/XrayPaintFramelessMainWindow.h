/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XrayPaintFramelessMainWindow.h
** file base:	XrayPaintFramelessMainWindow
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a frame less window for the paint widget.
****************************************************************************/

#pragma once
#ifndef XrayPaintFramelessMainWindow_h__
#define XrayPaintFramelessMainWindow_h__

#include "XrayQtApiExport.h"
#include "XrayFramelessWindowWin32.h"
#include "XrayPaintMainWindow.h"

#include <QWidget>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayPaintFramelessMainWindow : public XrayFramelessWindowWin32
{
public:
	XrayPaintFramelessMainWindow(const QString& _title, QWidget* _parent = nullptr);
	~XrayPaintFramelessMainWindow();

	/*!
		Description:
		Function that loads a project into this main widget.
	*/
	void openProject(const QString& fileName);
	/*!
		Description:
		Function that loads the given file name.
	*/
	bool openFile(const QString& fileName);

	/*!
		Description:
		Function to fit the current image to the viewport size.
	*/
	void fitToViewportCurrentView();

	/*!
		Description:
		Function that returns a pointer to XEnahncer batch process directories box.
	*/
	void setEnhancerDirectoriesBoxVisible(bool _b);

	/*!
		Description:
		Function to enable and disable features based on given modules.
	*/
	void enableDemoVersion(const QString& _modules, bool _b);

protected:
	void closeEvent(QCloseEvent* _event) override;

	QMenu* p_menu;
	QAction* p_actionWebsite;
	QAction* p_actionAbout;

	XrayPaintMainWindow* p_mainWindow;
};

XRAYLAB_END_NAMESPACE

#endif // XrayPaintFramelessMainWindow_h__