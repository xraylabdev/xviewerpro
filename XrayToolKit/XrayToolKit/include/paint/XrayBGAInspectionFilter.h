#pragma once
#ifndef XrayBGAInspectionFilter_h__
#define XrayBGAInspectionFilter_h__

#include "XrayQtApiExport.h"
#include "XrayTableModel.h"
#include "XraySaveReportAsExcelDialog.h"

#include <QWidget>
#include <QWidget>
#include <QPushButton>
#include <QSettings>
#include <QScopedPointer>

#include <opencv2/opencv.hpp>

namespace Ui
{
	class XrayBGAInspectionFilter;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayBGAInspectionFilter : public QWidget
{
	Q_OBJECT

public:
	explicit XrayBGAInspectionFilter(QWidget *parent = Q_NULLPTR);
	  struct Params
  {
      int MinDist;
	  int Param1;
	  int Param2;
	  int MinRadius;
	  int MaxRadius;
  };

	~XrayBGAInspectionFilter();

public slots:

	void preview();
	void performVoidAnalysis();
	void clearShapes();
	void drawGrid();
	void setBGAdimensions();
	void reOrderCircles();
	void fitContoursToCircles();

	void loadPresets();
	void savePresets();
	void savePreset();
	void removePreset();
	void selectPreset(int _index);
	void updateWithCurrentPreset();

private:

	int _rows;
	int _cols;
	cv::Mat gray;
	bool valuesset;	

	void setParametersToSliders(Params _params);
	Params getParametersFromSliders();
	void generateGrid(QPoint firstCircle, QPoint lastCircle, 
												double radius);
	Ui::XrayBGAInspectionFilter *ui;
	QWidget* p_parent;
	QList<Params> m_presets;


};

XRAYLAB_END_NAMESPACE

#endif // XrayBGAInspectionFilter_h__