/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/22
** filename: 	XrayVoidAnalysisFilter.h
** file base:	XrayVoidAnalysisFilter
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the file system monitoring widget for
				specified folders.
****************************************************************************/

#pragma once
#ifndef XrayVoidAnalysisFilter_h__
#define XrayVoidAnalysisFilter_h__

#include "XrayQtApiExport.h"
#include "XrayTableModel.h"
#include "XraySaveReportAsExcelDialog.h"

#include <QWidget>
#include <QPushButton>
#include <QSettings>
#include <QScopedPointer>

#include <opencv2/opencv.hpp>

namespace Ui 
{
	class XrayVoidAnalysisFilter;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayVoidAnalysisFilter : public QWidget
{
	Q_OBJECT

public:
	explicit XrayVoidAnalysisFilter(QWidget *parent = nullptr);
	~XrayVoidAnalysisFilter();

	void clearTable(const QString& _pictureName);

	void writeSettings(QSettings& _settings);
	void readSettings(QSettings& _settings);

	QPushButton* getSaveBtn() const;

	bool drawCustomContour(const cv::Mat& _mat, const std::vector<cv::Point>& _contour);
	void previewCustomContour(const QVector<QPointF>& _points);

	void setDemoLimitEnabled(bool b);

	bool loadPresetFile(const QString& fileName);
	void savePresetFile(const QString& fileName);

public slots:
	void colorize(bool b);
	void apply();
	void reset();
	void preview();
	void updateTable();
	void saveTableCSV(const QString& fileName);
	void saveTable();
	void removeTableRows(const std::vector<int>& _removedRows);
	void updateSelectedShapeParameters();

	void loadPresets();
	void loadPresetsAs();
	void savePresets();
	void savePresetsAs();
	void savePreset();
	void removePreset();
	void selectPreset(int _index);
	void setPreset1();
	void setPreset2();
	void updateWithCurrentPreset();
	void clearTable();

	void updateContourColor(const QColor& _color);
	void updateContourIdColor(const QColor& _color);

private:
	void setParametersToSliders(cv::SimpleBlobDetector::Params _params);
	cv::SimpleBlobDetector::Params getParametersFromSliders();
	bool detect(const cv::Mat& _mat);

	Ui::XrayVoidAnalysisFilter *ui;
	QWidget* p_parent;
	XrayTableModel* p_tableModel;
	QList<cv::SimpleBlobDetector::Params> m_presets;
	QScopedPointer<XraySaveReportAsExcelDialog> p_saveAsExcelDialog;

	bool m_demoLimit;
};

XRAYLAB_END_NAMESPACE

#endif // XrayVoidAnalysisFilter_h__
