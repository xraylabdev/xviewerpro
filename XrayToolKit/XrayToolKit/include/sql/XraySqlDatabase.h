/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/05
** filename: 	XraySqlDatabase.h
** file base:	XraySqlDatabase
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a ability to connect to SQLITE database.
****************************************************************************/

#pragma once
#ifndef XraySqlDatabase_h__
#define XraySqlDatabase_h__

#include "XrayQtApiExport.h"

#include <QSqlDatabase>
#include <QSqlQuery>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XraySqlDatabase
{
public:
	XraySqlDatabase();
	~XraySqlDatabase();

	/*!
		Description:
		Function that finalizes a connection to the given database.
		/param _databaseFilePath - absolute path of the database file.
	*/
	bool connect(const QString& _databaseFilePath);
	/*!
		Description:
		Function that finalizes a connection to the default database that
		locates at the application data location.
	*/
	bool connect();
	/*!
		Description:
		Function that closes the current database connection.
	*/
	bool disconnect();
	/*!
		Description:
		Function that returns true if the connected is opened successfully.
	*/
	bool isConnected() const;

	/*!
		Description:
		Function that returns a SQL query of the current database for execution.
	*/
	QSqlQuery query();
	/*!
		Description:
		Function that returns a SQL query of the given database for execution.
	*/
	static QSqlQuery query(const QString& _databaseName);

	/*!
		Description:
		Function that Returns a list of tables for the current active database connection.
	*/
	QStringList tables();

	/*!
		Description:
		Function that Returns true if the given table name exists in the database.
	*/
	bool tableExists(const QString& _table);

	/*!
		Description:
		Function that returns the file path of the database file.
		If the application data location doesn't exists, it creates a new directory
		and returns the path of the database file.
	*/
	QString databaseLocalFilePath();

	QSqlDatabase m_db;
};

namespace QDBLite {
	class DB : public XraySqlDatabase {};
}

XRAYLAB_END_NAMESPACE

#endif // XraySqlDatabase_h__                                                                                                                                                                                                                                                                                   
