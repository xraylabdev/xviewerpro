/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XraySqlUserManagementWidget.h
** file base:	XraySqlUserManagementWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a user registration widget with SQLITE
*				database connection.
****************************************************************************/

#pragma once
#ifndef XraySqlUserManagementWidget_h__
#define XraySqlUserManagementWidget_h__

#include "XrayQtApiExport.h"

#include <QMainWindow>
#include <QSqlTableModel>

namespace Ui 
{
	class XraySqlUserManagementWidget;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XraySqlUserManagementWidget : public QMainWindow
{
	Q_OBJECT

public:
	/*!
	 * 
	 * 
	 */
	explicit XraySqlUserManagementWidget(QWidget *parent = 0);
	~XraySqlUserManagementWidget();

	/*!
		Description:
		Function that returns true if any of the user has logged in, otherwise false.
	*/
	bool hasLoggedIn() const;
	/*!
		Description:
		Function that returns true if the current user is admin, otherwise false.
	*/
	bool hasLoggedInByAdmin() const;

	/*!
		Description:
		Function to print out all users in the database.
	*/
	void printUsers();

protected:
	void createAdminUser();
	void updateUsersTable();
	void updateAdminsTable();

signals:
	void emitEnterToApp();
	void emitLogin();

public slots:
	void on_loginButton_clicked();

	void on_logoutButton_clicked();

	void on_completeRegButton_clicked();

	void on_backButton_clicked();

	void on_regButton_clicked();

	void on_backButton_2_clicked();

	void on_editButton_clicked();

	void on_delButton_clicked();

	void on_editedButton_clicked();

	void on_winStack_currentChanged(int arg1);

	void on_uplButton_clicked();

	void on_uplButton_2_clicked();

	void on_adminButton_clicked();

	void on_pageButton_clicked();

	void on_editedButton_2_clicked();

	void on_backButton_5_clicked();

	void on_userBrowse_clicked();

	void on_delUButton_clicked();

	void on_stackedWidget_currentChanged(int _arg);

	void on_adminBrowse_clicked();

	void on_delAButton_clicked();

	void on_enterToAppButton_clicked();

protected:
	bool login(const QString _u, const QString _p);
	bool m_logged_in;
	bool m_adminLogged_in;
	QString m_pic_name;
	QString m_pic_dir;
	QSqlTableModel* p_sql_table_model;

private:
	Ui::XraySqlUserManagementWidget *ui;
	QString m_username;
	QString m_password;
};

XRAYLAB_END_NAMESPACE

#endif // XraySqlUserManagementWidget_h__