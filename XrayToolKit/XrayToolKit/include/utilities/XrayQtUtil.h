/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/01
** filename: 	XrayQtUtil.h
** file base:	XrayQtUtil
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides utility functions for Qt applications.

// <font color='red'>R:%3 </font>
****************************************************************************/

#pragma once
#ifndef XrayQtUtil_h__
#define XrayQtUtil_h__

#include "XrayQtApiExport.h"

#include <QMainWindow>
#include <QLayoutItem>
#include <QWidgetAction>
#include <QMenu>
#include <QUuid>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQtUtil
{
public:
	/*!
		Description:
		Function that returns a pointer to the top level main window.
		/param _objectName - object name of the main window.
	*/
	static QMainWindow* getMainWindow(const QString& _objectName = QString("AppMainWindow"));

	/*!
		Description:
		Function to remove and delete all child widgets from the given layout.
		/param item - given layout item.
	*/
	static void deleteChilds(QLayout* layout);
	static void deleteChildWidgets(QLayoutItem* item);
	static void deleteChildLabels(QLayoutItem *item);

	/*!
		Description:
		Function that removed all widgets from the given layout and deletes it.
		/param item - given layout item.
	*/
	static void clearLayout(QLayout* layout);
	/*!
		Description:
		Function that removes all layouts from the given widget and deletes it.
		/param item - given widget.
	*/
	static void removeLayout(QWidget* widget);

	/*!
		Description:
		Function that create a separator in the given QMenu with a given text.
		/param _menu - given menu.
		/param _text - given menu item text.
	*/
	static QWidgetAction* createTextSeparator(QMenu* _menu, const QString& _text);

	/*!
		Description:
		Function that converts the given seconds to days and time.
		/param _seconds - given seconds.
		/param _withDays - if true then time will be returned with days, i.e, 1 day 02:34:09,
		othewise only time in 02:34:09 (hh:mm:ss) format.
	*/
	static QString convertSecToTime(int _seconds, bool _withDays = false);
	static QString convertToTime(int _seconds);

	/*!
		Description:
		Function that calculate the remaining seconds from the given sizes.
	*/
	static int calculateRemainingSec(int _totalSize, int _processedSize, int _speed);

	/*!
		Description:
		Function that returns a Uuid as a string.
	*/
	static const int NUM_BYTES_RFC4122_UUID = 16;
	static QString getUuidWithoutCurlyBraces(const QUuid& _uuid);

	/*!
		Description:
		Function that encodes the given string to base64 encryption.
	*/
	static QString base64Encode(const QString& _string);
	/*!
		Description:
		Function that decodes the given string from base64 encryption.
	*/
	static QString base64Decode(const QString& _string);
	/*!
		Description:
		Function that encodes the given string to md5 hash encryption.
	*/
	static QString md5Encode(const QString& _string);

	/*!
		Description:
		Function that converts the given image to gray scale image.
	*/
	static QImage convertToGray(const QImage& image);
	/*!
		Description:
		Function that rotates the src image to +90 or -90 degrees.
		_mode =  1 =>  90
		_mode = -1 => -90
		Ex. rotate90YUV420P(reinterpret_cast<uchar*>(out.bits()), (const uchar*)in.bits(), nWidth, nHeight, -1);
	*/
	static void rotate90YUV420P(uchar* _dst, const uchar* _src, int _srcWidth, int _srcHeight, int _mode);
	/*!
		Description:
		Function that mirror the src image to X, Y, or XY axis.
		_mode =  0 =>   Y
		_mode =  1 =>   X
		_mode = -1 =>  XY
	*/
	static void mirrorYUV420P(uchar* _dst, const uchar* _src, int _srcWidth, int _srcHeight, int _mode);

	/*!
		Description:
		Flood fill algorithm for the specified image.
	*/
	static QVector<QPoint> imagePixelFloodFill(const QImage *image, const QPoint &startPos, const QColor &targetColour, const QColor &replacementColour);
	static QVector<QPoint> imageGreedyPixelFill(const QImage *image, const QPoint &startPos, const QColor &targetColour, const QColor &replacementColour);

	template <typename Type>
	static QRect boundingRect(const QVector<Type>& points);
	template <typename Type>
	static void scalePoints(QVector<Type>& points, double scale);
	static void scalePolygon(QPolygonF& points, double scale);
};

XRAYLAB_END_NAMESPACE

#endif // XrayQtUtil_h__                                                                                                                                                                                                                                                                                   
