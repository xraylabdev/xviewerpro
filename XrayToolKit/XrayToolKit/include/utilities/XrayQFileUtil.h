/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/04
** filename: 	XrayQFileUtil.h
** file base:	XrayQFileUtil
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides utility functions related to QFile.
****************************************************************************/

#pragma once
#ifndef XrayQFileUtil_h__
#define XrayQFileUtil_h__

#include "XrayQtApiExport.h"

#include <QStandardPaths>
#include <QDirIterator>
#include <QStringList>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQFileUtil
{
public:
	/*!
		Description:
		Function that creates a new folder (if it doesn't exists) at the specified directory and returns its path.
		/param _subfolder - name of the created folder.
		/param _directory - location where the folder must be created.
	*/
	static QString getStandardPath(const QString& _subfolder, const QString& _directory);
	/*!
		Description:
		Function that creates a new folder (if it doesn't exists) at the specified standard
		location and returns its path.
		/param _subfolder - name of the created folder.
		/param _location - location where the folder must be created.
	*/
	static QString getStandardPath(const QString& _subfolder, QStandardPaths::StandardLocation _location);
	/*!
		Description:
		Function that creates a new folder (if it doesn't exists) at the specified standard
		location and returns its path.
		/param _subfolder - name of the created folder.
		/param _fileName - name of the file.
		/param _location - location where the folder must be created.
	*/
	static QString getStandardPath(const QString& _subfolder, const QString& _fileName, QStandardPaths::StandardLocation _location);
	/*!
	   Description:
	   Function that returns a string with the absolute path to the temporary file (a unique named file) that is not exist yet.
	   \return - If function failed to create unique path, it will return empty string.
	*/
	static QString getTempFileName(const QString& _prefix, const QString& _extension);

	/*!
	   Description:
	   Function that returns all sub-directories (only directories with numerical name such as 3242 or 424242)
	   from the specified directory path.
	   \return - a list of the directories.
	*/
	static QStringList getSubNumericalDirs(const QString& _path);
	/*!
	   Description:
	   Function that returns all sub-directories from the specified directory path.
	   \return - a list of the directories.
	*/
	static QStringList getSubDirs(const QString& _path, const QStringList& _nameFilters = {}, QDir::Filters _filters = QDir::NoFilter, QDirIterator::IteratorFlags _flags = QDirIterator::NoIteratorFlags);
	/*!
	   Description:
	   Function that returns all sub-directories as well as sub-files from the specified directory path.
	   \return - a list of the directories and files.
	   Example:
	   auto paths = XrayQFileUtil::getSubDirFiles("D:/Temp", { "*.jpg", "*.jpeg" }, QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot, QDirIterator::NoIteratorFlags); 	// 293749, *.jpg
	*/
	static QStringList getSubDirFiles(const QString& _path, const QStringList& _nameFilters, QDir::Filters _filters = QDir::NoFilter, QDirIterator::IteratorFlags _flags = QDirIterator::NoIteratorFlags);

	/*!
	   Description:
	   Function that returns the Md5 of the given file path.
	   \return - byte array of Md5.
	*/
	static QByteArray getFileMd5Sum(const QString& _filePath);
	/*!
	   Description:
	   Function that returns the Hex Md5 of the given file path.
	   \return - byte array of Md5.
	*/
	static QString getFileMd5SumString(const QString& _filePath);

	/*!
	   Description:
	   Function that removes the given directory as well as its subdirectories.
	   \return - byte array of Md5.
	*/
	static bool removeDirectory(const QString& _dirName);

	/*!
	   Description:
	   Function that converts the given size into like this: 1B, 2KB, 3.1MB, 4GB, 1TB.
	   \return - string of bytes.
	*/
	static QString getHumanReadableSize(const qlonglong& bytes);
	/*!
	   Description:
	   Function that returns the file size of the given file like this: 1B, 2KB, 3.1MB, 4GB, 1TB.
	   \return - string of bytes.
	*/
	static QString getHumanReadableFileSize(const QString& _fileName);

	enum FileType
	{
		Text,
		Binary
	};

	/*!
	   Description:
	   Function that reads the text or binary file from the given path.
	   \return - data as string.
	*/
	static QString read(const QString& _filePath, FileType _type);
	/*!
	   Description:
	   Function that writes the given data as text or binary at the given path.
	   \return - true on success.
	*/
	static bool write(const QString& _data, const QString& _filePath, FileType _type, QFile::OpenMode _mode = QFile::WriteOnly | QFile::Truncate);

	bool copy(const QString& _source, QString& _destination, bool _overwrite, bool _move);
	void copy(const QString& src, const QString& dst);
	bool replaceString(const QString& fileName, const QByteArray& before, const QByteArray& after);
};

XRAYLAB_END_NAMESPACE

#endif // XrayQFileUtil_h__                                                                                                                                                                                                                                                                                   
