/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/08
** filename: 	XrayQDateTime.h
** file base:	XrayQDateTime
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a class for date and time.
****************************************************************************/

#pragma once
#ifndef XrayQDateTime_h__
#define XrayQDateTime_h__

#include "XrayQtApiExport.h"

#include <QDateTime>
#include <QString>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQDateTime : public QDateTime
{
public:
	XrayQDateTime(const QDateTime& other);

	QString formatDate(const QString& format) const;
	QString toStringZ(const QString& format) const;

private:
	int timeDiff() const;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQDateTime_h__