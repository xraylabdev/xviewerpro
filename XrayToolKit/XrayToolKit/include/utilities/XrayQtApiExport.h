/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/05
** filename: 	XrayQtApiExport.h
** file base:	XrayQtApiExport
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides DLL and LIB support.
****************************************************************************/

#pragma once
#ifndef XrayQtApiExport_h__
#define XrayQtApiExport_h__

#  if defined(XRAY_QT_API_DLL)
#    ifdef XRAY_QT_API_LIBRARY
#      define XRAY_QT_API_EXPORT	__declspec(dllexport)
#    else
#      define XRAY_QT_API_EXPORT	__declspec(dllimport)
#    endif /* XRAY_QT_API_LIBRARY */
#  else
#    define XRAY_QT_API_EXPORT
#  endif /* XRAY_QT_API_DLL */

# define XRAYLAB_BEGIN_NAMESPACE namespace XRAYLAB_NAMESPACE {
# define XRAYLAB_END_NAMESPACE }
# define XRAYLAB_USING_NAMESPACE using namespace XRAYLAB_NAMESPACE;


#endif // XrayQtApiExport_h__                                                                                                                                                                                                                                                                                   
