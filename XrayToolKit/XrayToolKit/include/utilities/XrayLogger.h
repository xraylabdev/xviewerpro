/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/22
** filename: 	XrayLogger.h
** file base:	XrayLogger
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the various types of loggers.
****************************************************************************/

#pragma once
#ifndef XrayLogger_h__
#define XrayLogger_h__

#include "XrayQtApiExport.h"

#include <spdlog/logger.h>
#include <memory>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayLogger
{
public:
	/*!
		Description:
		Function to initialize the logging.
	*/
	static void init();
	/*!
		Description:
		Function to get a shared pointer to core logger.
	*/
	static std::shared_ptr<spdlog::logger>& getAppLogger() { return p_appLogger; }
	/*!
		Description:
		Function to get a shared pointer to client logger.
	*/
	static std::shared_ptr<spdlog::logger>& getClientLogger() { return p_clientLogger; }

private:
	static std::shared_ptr<spdlog::logger> p_appLogger;
	static std::shared_ptr<spdlog::logger> p_clientLogger;
};

/*! core log macros */
#ifdef _DEBUG
#define xAppTrace(...)		XrayLogger::getAppLogger()->trace(__VA_ARGS__)
#define xAppInfo(...)		XrayLogger::getAppLogger()->info(__VA_ARGS__)
#define xAppWarn(...)		XrayLogger::getAppLogger()->warn(__VA_ARGS__)
#define xAppError(...)		XrayLogger::getAppLogger()->error(__VA_ARGS__)
#define xAppCritical(...)	XrayLogger::getAppLogger()->critical(__VA_ARGS__)
#else
#define xAppTrace(...)
#define xAppInfo(...)
#define xAppWarn(...)
#define xAppError(...)
#define xAppCritical(...)
#endif // _DEBUG

/*! client log macros */
#ifndef _DEBUG
#define xClientTrace(...)		XrayLogger::getClientLogger()->trace(__VA_ARGS__)
#define xClientInfo(...)		XrayLogger::getClientLogger()->info(__VA_ARGS__)
#define xClientWarn(...)		XrayLogger::getClientLogger()->warn(__VA_ARGS__)
#define xClientError(...)		XrayLogger::getClientLogger()->error(__VA_ARGS__)
#define xClientCritical(...)	XrayLogger::getClientLogger()->critical(__VA_ARGS__)
#else
#define xClientTrace(...)
#define xClientInfo(...)
#define xClientWarn(...)
#define xClientError(...)
#define xClientCritical(...)
#endif // _DEBUG

XRAYLAB_END_NAMESPACE

#endif // XrayLogger_h__