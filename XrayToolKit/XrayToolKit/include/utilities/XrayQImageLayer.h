/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/03/12
** filename: 	XrayQImageLayer.h
** file base:	XrayQImageLayer
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides utility functions for Qt applications.

// <font color='red'>R:%3 </font>
****************************************************************************/

#pragma once
#ifndef XrayQImageLayer_h__
#define XrayQImageLayer_h__

#include "XrayQtApiExport.h"

XRAYLAB_BEGIN_NAMESPACE

#include <QImage>
#include <QObject>

class QJsonObject;

class XRAY_QT_API_EXPORT XrayQImageLayer : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString name READ name NOTIFY nameChanged)
	Q_PROPERTY(qreal opacity READ opacity NOTIFY opacityChanged)
	Q_PROPERTY(bool visible READ isVisible NOTIFY visibleChanged)

public:
	/*!
		Description:
		Function that returns a pointer to the top level main window.
		/param _objectName - object name of the main window.
	*/
	XrayQImageLayer();
	explicit XrayQImageLayer(QObject *parent, const QImage &image = QImage());
	~XrayQImageLayer();

	QString name() const;
	void setName(const QString &name);

	QImage *image();
	const QImage *image() const;

	QSize size() const;
	void setSize(const QSize &newSize);

	qreal opacity() const;
	void setOpacity(const qreal &opacity);

	bool isVisible() const;
	void setVisible(bool visible);

	void read(const QJsonObject &jsonObject);
	void write(QJsonObject &jsonObject);

signals:
	void nameChanged();
	void opacityChanged();
	void visibleChanged();

private:
	QString mName;
	bool mVisible;
	qreal mOpacity;
	QImage mImage;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQImageLayer_h__                                                                                                                                                                                                                                                                                   
