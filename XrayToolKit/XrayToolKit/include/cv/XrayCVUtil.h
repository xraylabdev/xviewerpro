/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/05/20
** filename: 	XrayCVUtil.h
** file base:	XrayCVUtil
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides utility function for cv::Mat.
****************************************************************************/

#pragma once
#ifndef XrayCVUtil_h__
#define XrayCVUtil_h__

#include "XrayQtApiExport.h"

#include <opencv2/opencv.hpp>

#include <QImage>
#include <QPixmap>
#include <QDebug>
#include <QFileInfo>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayCVUtil
{
public:
	/*!
		Description:
		Function that converts the given cv::mat to QImage.
	*/
	inline static QImage toQImage(const cv::Mat& _mat)
	{
		switch (_mat.type())
		{
			// 8-bit, 4 channel
		case CV_8UC4:
		{
			QImage image(_mat.data, _mat.cols, _mat.rows, static_cast<int>(_mat.step), QImage::Format_ARGB32);
			return image;
		}
		// 8-bit, 3 channel
		case CV_8UC3:
		{
			QImage image(_mat.data, _mat.cols, _mat.rows, static_cast<int>(_mat.step), QImage::Format_RGB888);
			return image.rgbSwapped();
		}
		// 8-bit, 1 channel
		case CV_8UC1:
		{
			QImage image(_mat.data, _mat.cols, _mat.rows, static_cast<int>(_mat.step), QImage::Format_Grayscale8);
			return image;
		}

		default:
			qWarning() << "Couldn't handle cv::Mat image type!" << _mat.type();
			break;
		}

		return QImage();
	}
	/*!
		Description:
		Function that converts the given cv::mat to QPixmap.
	*/
	inline static QPixmap toQPixmap(const cv::Mat& _mat)
	{
		return QPixmap::fromImage(toQImage(_mat));
	}
	/*!
		Description:
		Function that converts the given QImage to cv::Mat.
	*/
	inline static cv::Mat toMat(const QImage& _img, const bool _clone_image_data = true)
	{
		switch (_img.format())
		{
			// 8-bit, 4 channel
		case QImage::Format_ARGB32:
		case QImage::Format_ARGB32_Premultiplied:
		{
			cv::Mat mat(_img.height(), _img.width(), CV_8UC4, const_cast<uchar*>(_img.bits()), static_cast<std::size_t>(_img.bytesPerLine()));
			return (_clone_image_data ? mat.clone() : mat);
		}
		// 8-bit, 4 channel
		case QImage::Format_RGB32:
		{
			cv::Mat mat(_img.height(), _img.width(), CV_8UC4, const_cast<uchar*>(_img.bits()), static_cast<std::size_t>(_img.bytesPerLine()));
			cv::Mat matNoAlpha;
			cv::cvtColor(mat, matNoAlpha, cv::COLOR_BGRA2BGR);   // drop the all-white alpha channel
			return matNoAlpha;
		}
		// 8-bit, 3 channel
		case QImage::Format_RGB888:
		{
			auto swapped = _img.rgbSwapped();
			return cv::Mat(swapped.height(), swapped.width(), CV_8UC3, const_cast<uchar*>(swapped.bits()), static_cast<std::size_t>(swapped.bytesPerLine())).clone();
		}
		// 8-bit, 1 channel
		case QImage::Format_Grayscale8:
		{
			cv::Mat mat(_img.height(), _img.width(), CV_8UC1, const_cast<uchar*>(_img.bits()), static_cast<std::size_t>(_img.bytesPerLine()));
			return (_clone_image_data ? mat.clone() : mat);
		}
		default:
			qWarning() << "Couldn't handle QImage format!" << _img.format();
			break;
		}

		return cv::Mat();
	}
	/*!
		Description:
		Function that converts the given QPixmap to cv::Mat.
	*/
	inline static cv::Mat toMat(const QPixmap& _img, const bool _clone_image_data = true)
	{
		return toMat(_img.toImage(), _clone_image_data);
	}

	/*!
		Description:
		Function that returns the equivalent format from QImage to cv::Mat.
	*/
	inline static int getCVFormat(const QImage& _img)
	{
		switch (_img.format())
		{
			// 8-bit, 4 channel
		case QImage::Format_ARGB32:
		case QImage::Format_ARGB32_Premultiplied:
		{
			return CV_8UC4;
		}
		// 8-bit, 3 channel
		case QImage::Format_RGB32:
		{
			return CV_8UC4;
		}
		// 8-bit, 3 channel
		case QImage::Format_RGB888:
		{
			return CV_8UC3;
		}
		// 8-bit, 1 channel
		case QImage::Format_Grayscale8:
		{
			return CV_8UC1;
		}
		// 16-bit, 1 channel
		case QImage::Format_Grayscale16:
		{
			return CV_16U;
		}
		default:
			qWarning() << "Couldn't handle QImage format!" << _img.format();
			break;
		}

		return CV_8UC1;
	}

	/*!
		Description:
		Function that returns the equivalent format from cv::Mat to QImage.
	*/
	inline static QImage::Format getQImageFormat(const cv::Mat& _mat)
	{
		switch (_mat.type())
		{
			// 8-bit, 4 channel
		case CV_8UC4:
		{
			return QImage::Format_ARGB32;
		}
		// 8-bit, 3 channel
		case CV_8UC3:
		{
			return QImage::Format_RGB888;
		}
		// 8-bit, 1 channel
		case CV_8UC1:
		{
			return QImage::Format_Grayscale8;
		}
		// 16-bit, 1 channel
		case CV_16U:
		case CV_16S:
		case CV_16F:
		{
			return QImage::Format_Grayscale16;
		}

		default:
			qWarning() << "Couldn't handle cv::Mat image type!" << _mat.type();
			break;
		}

		return QImage::Format_Grayscale8;
	}

	/*!
	 * Description:
	 * Function that returns OpenCV supported image extensions.
	 * otherwise returns false.
	 */
	inline static QStringList getSupportedImageExtensions()
	{
		return QStringList() << "jpg" << "jpeg" << "jpe" << "jp2" << "png" << "bmp" << "dib" << "tif" << "tiff" << "pgm" << "pbm" << "ppm" << "ras" << "sr" << "webp" << "dcm";
	}
	/*!
	 * Description:
	 * Function that returns OpenCV supported image extensions.
	 * otherwise returns false.
	 */
	inline static QString getSupportedImageExtensions(const QString& _text /*= QString("Image Files")*/)
	{
		return _text + " (" + getSupportedImageExtensions().join(' ') + ")";
	}
	/*!
	 * Description:
	 * Function that returns true if the given file is supported for reading with OpenCV,
	 * otherwise returns false.
	 */
	inline static bool isSupportedImageExtension(const QString& _fileName)
	{
		auto ext = QFileInfo(_fileName).suffix();
		return getSupportedImageExtensions().join(',').contains(ext, Qt::CaseInsensitive);
	}

	/*!
	 * Description:
	 * Function that returns a random color.
	 * declare RNG with cv::RNG rng( 0xFFFFFFFF );
	 */
	inline static cv::Scalar randomColor(cv::RNG& rng)
	{
		int icolor = (unsigned)rng;
		return cv::Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);
	}

	inline static cv::Rect toRect(const QRect& rect)
	{
		return cv::Rect(rect.x(), rect.y(), rect.width(), rect.height());
	}
	inline static cv::Rect toRect(const QRectF& rect)
	{
		return cv::Rect(rect.x(), rect.y(), rect.width(), rect.height());
	}

	inline static QRect fromRect(const cv::Rect& rect)
	{
		return QRect(rect.x, rect.y, rect.width, rect.height);
	}

	template <typename Type>
	inline static cv::Point toPoint(const Type& point)
	{
		return cv::Point(point.x(), point.y());
	}
	template <typename Type>
	inline static Type fromPoint(const cv::Point& point)
	{
		return Type(point.x, point.y);
	}

	template <typename Type>
	inline static std::vector<cv::Point> toPoints(const QVector<Type>& pts)
	{
		std::vector<cv::Point> points(pts.size());
		for (auto i = 0; i < pts.size(); i++)
			points[i] = cv::Point(pts[i].x(), pts[i].y());
		return points;
	}
	template <typename Type>
	inline static QVector<Type> fromPoints(const std::vector<cv::Point>& pts)
	{
		QVector<Type> points(static_cast<int>(pts.size()));
		for (std::size_t i = 0; i < pts.size(); i++)
			points[i] = Type(pts[i].x, pts[i].y);
		return points;
	}

	inline static void contourOffset(const std::vector<cv::Point>& src, std::vector<cv::Point>& dst, const cv::Point& offset)
	{
		dst.clear();
		dst.resize(src.size());
		for (std::size_t j = 0; j < src.size(); j++)
			dst[j] = src[j] + offset;
	}
	inline static void scaleContour(const std::vector<cv::Point>& src, std::vector<cv::Point>& dst, float scale)
	{
		auto rct = cv::boundingRect(src);

		std::vector<cv::Point> dc_contour;
		cv::Point rct_offset(-rct.tl().x, -rct.tl().y);
		contourOffset(src, dc_contour, rct_offset);

		std::vector<cv::Point> dc_contour_scale(dc_contour.size());

		for (std::size_t i = 0; i < dc_contour.size(); i++)
			dc_contour_scale[i] = dc_contour[i] * scale;

		auto rct_scale = cv::boundingRect(dc_contour_scale);

		cv::Point offset((rct.width - rct_scale.width) / 2, (rct.height - rct_scale.height) / 2);
		offset -= rct_offset;
		dst.clear();
		dst.resize(dc_contour_scale.size());
		for (std::size_t i = 0; i < dc_contour_scale.size(); i++)
			dst[i] = dc_contour_scale[i] + offset;
	}
	inline static void scaleContours(const std::vector<std::vector<cv::Point>>& src, std::vector<std::vector<cv::Point>>& dst, float scale)
	{
		dst.clear();
		dst.resize(src.size());
		for (std::size_t i = 0; i < src.size(); i++)
			scaleContour(src[i], dst[i], scale);
	}

	inline static void scalePoints(std::vector<cv::Point>& points, double scale)
	{
		// #1
		//auto r = cv::boundingRect(points);
		//auto center = cv::Point(r.x + double(r.width) / 2.0 + 0.5, double(r.y) + r.height / 2.0 + 0.5);

		// #2
		auto moms = cv::moments(points);
		cv::Point center(moms.m10 / moms.m00, moms.m01 / moms.m00);

		// #3
		//cv::Point center(0, 0);
		//for (auto i = 0; i < points.size(); i++)
		//	center = center + points[i];
		//center.x = center.x * (1.0 / points.size()) + 0.5;
		//center.y = center.y * (1.0 / points.size()) + 0.5;

		for (std::size_t i = 0; i < points.size(); i++)
			points[i] = (points[i] - center) * scale + center;
	}

	template <typename Type>
	inline static QVector<QVector<Type> > toQt(const std::vector<std::vector<cv::Point> >& points2DArray)
	{
		QVector<QVector<Type> > con;
		con.resize(static_cast<int>(points2DArray.size()));

		for (auto i = 0; i < points2DArray.size(); i++)
		{
			con[i].resize(points2DArray[i].size());
			for (auto j = 0; j < points2DArray[i].size(); j++)
			{
				con[i][j].rx = points2DArray[i][j].x;
				con[i][j].ry = points2DArray[i][j].y;
			}
		}

		return con;
	}
	template <typename Type>
	inline static std::vector<std::vector<cv::Point> > fromQt(const QVector<QVector<Type> >& points2DArray)
	{
		std::vector<std::vector<cv::Point> > con;
		con.resize(points2DArray.size());

		for (auto i = 0; i < points2DArray.size(); i++)
		{
			con[i].resize(points2DArray[i].size());
			for (auto j = 0; j < points2DArray[i].size(); j++)
			{
				con[i][j].x = points2DArray[i][j].x();
				con[i][j].y = points2DArray[i][j].y();
			}
		}

		return con;
	}

	/*!
	 * Description:
	 * Function to adjust the brightness of the image.
	 * _value should be between -100 and 100.
	 * _value less than 0 will darken the image while values greater than 0 will brighten.
	 */
	static QImage setBrightness(const QImage& _img, int _value);
	static void setAlpha(QImage& _img, int _value);

	static int getWindowLevelValue(int x, int r1, int s1, int r2, int s2, int typeMaxValue = 255);
	static void applyWindowLevel(const cv::Mat& image, cv::Mat& outImage, int r1, int s1, int r2, int s2);
	static void applyWindowLevel16(const cv::Mat& image, cv::Mat& outImage, int r1, int s1, int r2, int s2);

	/*!
	 *	Description:
		Function that calculates the area of the points using Shoelace Formula.
		cv::contourArea() method returns same area as this function. 
		https://www.geeksforgeeks.org/area-of-a-polygon-with-given-n-ordered-vertices/

		Wrong way to compute the area, it only gives the apprximate rectangular area.
		std::vector<cv::Point> poly;
		cv::approxPolyDP(contours[0], poly, 3, false);
		auto proximateArea = cv::boundingRect(poly);
	 */
	static const int getAreaUsingShoelaceFormula(const std::vector<cv::Point>& points);
	/*!
	 *	Description:
		Function that fills all contours with the specified color in the given src image.
	*/
	static cv::Mat fillContours(const cv::Size& size, const std::vector<std::vector<cv::Point> >& contours, const cv::Scalar& color = cv::Scalar(255, 255, 255));
	/*!
	 *	Description:
		Function that fills the given contour with the specified color in the given src image.
	*/
	static cv::Mat fillContour(const cv::Size& size, const std::vector<cv::Point>& contour, const cv::Scalar& color = cv::Scalar(255, 255, 255));
	/*!
	 *	Description:
		Function that returns non-zero pixels in the given image.
	*/
	static std::vector<cv::Point> getNonZeroPixels(cv::Mat& mat);
	/*!
	 *	Description:
		Function that returns the total number of pixels (area of the contour)
		that are inside and on edge of the given contour.
	*/
	static int getNumberOfPixelsInsidePolygon(const cv::Size& size, const std::vector<cv::Point>& contour);
	/*!
	 *	Description:
		Function that returns true the given point is inside or on edge of the given contour points,
		otherwise it returns false.
	*/
	static bool isPixelInside(const std::vector<cv::Point>& contour, const cv::Point& point);
	/*!
	 *	Description:
		Function that returns true if all points of the given insideContour
		are inside or on edge of the boundContour, otherwise it returns false.
	*/
	static bool isContourInsideContour(const std::vector<cv::Point>& insideContour, const std::vector<cv::Point>& boundContour);
};

XRAYLAB_END_NAMESPACE

#endif // XrayCVUtil_h__
