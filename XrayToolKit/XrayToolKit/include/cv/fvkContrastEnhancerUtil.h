#pragma once
#ifndef fvkContrastEnhancerUtil_h__
#define fvkContrastEnhancerUtil_h__

/*********************************************************************************
created:	2018/08/18   02:02AM
filename: 	fvkContrastEnhancerUtil.h
file base:	fvkContrastEnhancerUtil
file ext:	h
author:		Furqan Ullah (Post-docs, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	This class provides algorithms for contrast enhancements for 8, 16,
24, 32 bit images.
This class is a part of Fast Visualization Kit (FVK) which is developed and
maintained by Dr. Furqan Ullah.

/**********************************************************************************
*	Fast Visualization Kit (FVK)
*	Copyright (C) 2017-2019 REAL3D
*
* This file and its content is protected by a software license.
* You should have received a copy of this license with this file.
* If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include <opencv2/opencv.hpp>

namespace R3D
{

class fvkContrastEnhancerUtil
{
public:
	// Description:
	// Enhance the contrast with Speeded Up Adaptive Contrast Enhancement (SUACE);
	// given source image should be 1-channel CV_8UC1 image.
	// output image would also be 1-channel CV_8UC1 image.
	// distance the old reference contrast range to be enhanced. less value means more contrast.
	// sigma the sigma value of the Gaussian filter in SUACE.
	static bool performSUACE(const cv::Mat& _src, cv::Mat& _dst, int _distance, double _sigma);
	// Description:
	// Function that performs CLAHE (Contrast Limited Adaptive Histogram Equalization) filter on the
	// given _img.
	// _cliplimit is the threshold for contrast limiting.
	// _tile_grid_size is the size of grid for histogram equalization.
	static void performCLAHE(cv::Mat& _img, double _cliplimit, cv::Size _tile_grid_size);
	// Description:
	// Function that rescale the gray level of the input image.
	// _histogram_clipping_limit is the clip limit ranges from [0 ~ 50].
	static void rescaleGrayLevelMat(const cv::Mat& _input_mat, cv::Mat& _output_mat, const float _histogram_clipping_limit);
	// Description:
	// Function to apply gamma filter to the given image.
	// gamma value range should be [-100 ~ 100].
	static void performGammaFilter(cv::Mat& _img, int _value);
	// Description:
	// Function to apply gamma filter to the given image.
	// gamma value range should be [-100 ~ 100].
	static void performGammaLUT(cv::Mat& _img, const int _value = 50);
	// Description:
	// Function to apply basic levels to the given buffer data.
	// exposure value range should be [-100 ~ 100].
	static void performLevelsGray(unsigned char* buffer, const unsigned int width, const unsigned int height, const int exposure, const float brightness = 0.f, const float contrast = 1.f, const float saturation = 0.f);
	static void performLevelsRGB(unsigned char* buffer, const unsigned int width, const unsigned int height, const int exposure, const float brightness = 0.f, const float contrast = 1.f, const float saturation = 0.f);
	static void performLevels(unsigned char* buffer, const unsigned int width, const unsigned int height, int channels, const int exposure, const float brightness = 0.f, const float contrast = 1.f, const float saturation = 0.f);
	// Description:
	// Function to apply exposure filter to the given image.
	// exposure value range should be [-100 ~ 100].
	static void performExposure(cv::Mat& _img, const int exposure);

	struct Params
	{
		int Delta;
		int Sigma;
		int Clipping;
		int Exposure;
		int Gamma;
		int Optimize;
	};

	// Description:
	// Function to apply exposure filter to the given image.
	// exposure value range should be [-100 ~ 100].
	static void performContrastEnhancerFilter(const cv::Mat& _src, cv::Mat& _dest, int _delta = 40, int _sigma = 20, int _clip = 0, int _exposure = 70, int _gamma = 0, int _normalize = 1);
	static void performContrastEnhancerFilter(const cv::Mat& _src, cv::Mat& _dest, Params param);

	// Description:
	// Function that performs Automatic brightness and contrast optimization with optional histogram clipping.
	// src Input image GRAY or BGR or BGRA
	// dst Destination image
	// clipHistPercent cut wings of histogram at given percent tipical => 1, 0 => Disabled
	// In case of BGRA image, we won't touch the transparency.
	static void autoBrightnessAndContrast(const cv::Mat &src, cv::Mat &dst, float clipHistPercent = 0);
};

}

#endif // fvkContrastEnhancerUtil_h__