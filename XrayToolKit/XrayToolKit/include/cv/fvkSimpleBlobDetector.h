#pragma once
#ifndef fvkSimpleBlobDetector_h__
#define fvkSimpleBlobDetector_h__

/*********************************************************************************
created:	2017/08/05   02:02AM
filename: 	fvkSimpleBlobDetector.h
file base:	fvkSimpleBlobDetector
file ext:	h
author:		Furqan Ullah (Post-docs, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	fvkSimpleBlobDetector class extends the cv::SimpleBlobDetector
to detect the blobs in the gray scale image.
This class is a part of Fast Visualization Kit (FVK) which is developed and
maintained by Dr. Furqan Ullah.

/**********************************************************************************
FL-ESSENTIALS (FLE) - FLTK Utility Widgets
Copyright (C) 2017-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "XrayQtApiExport.h"

#include <opencv2/opencv.hpp>

namespace R3D
{

class XRAY_QT_API_EXPORT fvkSimpleBlobDetector : public cv::SimpleBlobDetector
{
public:
	fvkSimpleBlobDetector(const cv::SimpleBlobDetector::Params& params = cv::SimpleBlobDetector::Params());

	struct XRAY_QT_API_EXPORT Center
	{
		cv::Point2d location;
		double radius;
		double confidence;
	};

	virtual void detect(const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, const cv::Mat& mask = cv::Mat()) const;
	virtual void findBlobs(const cv::Mat& image, const cv::Mat& binaryImage, std::vector<Center>& centers, std::vector<std::vector<cv::Point>>& contours) const;
	static cv::Ptr<fvkSimpleBlobDetector> create(const cv::SimpleBlobDetector::Params& params);

	std::vector<std::vector<cv::Point>> getContours();

	cv::SimpleBlobDetector::Params params;
};

class XRAY_QT_API_EXPORT fvkBlobDetector
{
public:
	fvkBlobDetector();

	static std::vector<double> detect(cv::Mat& _img, cv::SimpleBlobDetector::Params params, int _smoothness, int _sharpness, std::vector<int> _filtered_indices = std::vector<int>());
	static cv::Mat execute(const cv::Mat& _img, const cv::Rect& roi, std::vector<double>& _areas, std::vector<int> _filtered_indices = std::vector<int>(), std::size_t _minRep = 8, float _threshStep = 3.f, float _minThresh = 10.f, float _maxThresh = 220.f, float _minArea = 1.5f, float _maxArea = 100.f, float _minCir = 0.54f, float _minConv = 0.9f, float _minIner = 0.13f);

	void sortContours();

	bool update();
	bool detect();
	bool draw();

	std::string m_name;
	cv::Mat m_image;
	cv::Mat m_grayscale;
	cv::Mat m_cropped;
	cv::Mat m_output;
	cv::Rect m_roi;
	std::vector<cv::Point> m_shapeContour;
	int m_smoothness;
	int m_sharpness;
	cv::SimpleBlobDetector::Params m_params;

	cv::Scalar m_contour_color;
	cv::Scalar m_keypoint_color;
	std::vector<cv::KeyPoint> m_keypoints;
	std::vector<std::vector<cv::Point> > m_contours;
	std::vector<double> m_areas;

	std::vector<cv::KeyPoint> m_custom_keypoints;
	std::vector<std::vector<cv::Point> > m_custom_contours;
	std::vector<double> m_custom_areas;

	bool m_colorizeContourOnPercentage;

	void addCustomContour(const std::vector<cv::Point>& contour);
	void removeContours(const std::vector<int>& _indexList);
	void clear();
	void resize(int size);
};

}

#endif // fvkSimpleBlobDetector_h__