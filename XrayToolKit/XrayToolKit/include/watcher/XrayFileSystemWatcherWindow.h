/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/02
** filename: 	XrayFileSystemWatcherWindow.h
** file base:	XrayFileSystemWatcherWindow
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the file system monitoring window for
				specified folders.
****************************************************************************/

#pragma once
#ifndef XrayFileSystemWatcherMainWindow_h__
#define XrayFileSystemWatcherMainWindow_h__

#include "XrayQtApiExport.h"
#include "XrayFramelessWindowWin32.h"
#include "XrayFileSystemWatcherWidget.h"

#include <QMenu>
#include <QAction>
#include <QGroupBox>
#include <QLineEdit>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayFileSystemWatcherWindow : public XrayFramelessWindowWin32
{
	Q_OBJECT

public:
	XrayFileSystemWatcherWindow(const QString& _title, QWidget* _parent = nullptr);
	~XrayFileSystemWatcherWindow();

protected:
	void closeEvent(QCloseEvent* _event) override;

private:
	QMenu* createMenu();
	QMenu* p_menu;
	QAction* p_actionViewHelp;
	QAction* p_actionWebsite;
	QAction* p_actionAbout;
	QAction* p_actionRestoreLastTree;
	QAction* p_actionRootPath;
	QAction* p_actionUpdate;
	QAction* p_actionDockVisibility;

	XrayFileSystemWatcherWidget* p_fileSysWatcher;
};

XRAYLAB_END_NAMESPACE

#endif // XrayFileSystemWatcherMainWindow_h__