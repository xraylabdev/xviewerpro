/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/06
** filename: 	XrayDeleteProjectFilesWidget.h
** file base:	XrayDeleteProjectFilesWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the main window of the application.
****************************************************************************/

#pragma once
#ifndef XrayDeleteProjectFilesWidget_h__
#define XrayDeleteProjectFilesWidget_h__

#include "XrayQtApiExport.h"
#include "XrayFileSystemStructure.h"
#include "XrayTableModel.h"

#include <QWidget>
#include <QCloseEvent>
#include <QSettings>
#include <QStringListModel>

namespace Ui
{
	class XrayDeleteProjectFilesWidget;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayProjectCleanerScanWorker : public QObject
{
	Q_OBJECT

public:
	/*!
	 * Description:
	 * Default constructor.
	*/
	explicit XrayProjectCleanerScanWorker(const QStringList& projDir, const QString& subDir, const QString& extFilters, const QString& containFilters, bool isNumericSort, int skipStart, int skipEnd, QObject* parent = nullptr);

signals:
	/*!
	 * Description:
	 * Signal that emits when a process is started (for progress bar).
	*/
	void onStart();
	/*!
	 * Description:
	 * Signal that emits when progress value is changed (for progress bar).
	*/
	void onProgress(double value);
	/*!
	 * Description:
	 * Signal that emits when a process is finished (for progress bar).
	*/
	void result(const QVector<QVector<QString> >& listView, const QVector< QVector<QPair<QString, QStringList> > >& dirStructure, std::size_t totalFiles, std::size_t totalSize);

public slots:
	/*!
	 * Description:
	 * Slot function to start scanning the root directory.
	*/
	void scan();

private:
	QStringList m_projectDir;
	QString m_subDir;
	QString m_extFilters;
	QString m_containFilters;
	bool m_isNumericSort;
	int m_skipStart;
	int m_skipEnd;
};

class XRAY_QT_API_EXPORT XrayProjectCleanerScanController : public QObject
{
	Q_OBJECT

public:
	/*!
	 * Description:
	 * Default constructor.
	*/
	XrayProjectCleanerScanController(const QStringList& projDir, const QString& subDir, const QString& extFilters, const QString& containFilters, bool isNumericSort, int skipStart, int skipEnd, int _updateInterval, QObject* parent = nullptr);
	/*!
	 * Description:
	 * Destructor to release the internal memory.
	*/
	~XrayProjectCleanerScanController();

	/*!
	 * Description:
	 * Function to set and get the timer interval in milliseconds.
	*/
	void setInterval(int _msec);
	int interval() const;

signals:
	/*!
	 * Description:
	 * Signal that emits when a process is started (for progress bar).
	*/
	void onStart();
	/*!
	 * Description:
	 * Signal that emits when progress value is changed (for progress bar).
	*/
	void onProgress(double value);
	/*!
	 * Description:
	 * Signal that emits when a process is finished (for progress bar).
	*/
	void result(const QVector<QVector<QString> >& listView, const QVector< QVector<QPair<QString, QStringList> > >& dirStructure, std::size_t totalFiles, std::size_t totalSize);

public slots:
	/*!
	 * Description:
	 * Slot function to start the process thread.
	*/
	void start();

private:
	QThread* p_thread;
	QTimer* p_timer;
};


class XRAY_QT_API_EXPORT XrayDeleteProjectFilesWidget : public QWidget
{
	Q_OBJECT

public:
	/*!
	 * Description:
	 * Default constructor that takes the parent widget.
	 * Default extension filters are {".tif", ".tiff"}
	 * Default contain filters are {"dark", "bright"}
	*/
	XrayDeleteProjectFilesWidget(QWidget* parent = nullptr);
	/*!
	 * Description:
	 * Destructor to release the internal memory.
	*/
	~XrayDeleteProjectFilesWidget();

	/*!
	 * Description:
	 * Function to write settings of this class to the given settings.
	*/
	void writeSettings(QSettings& settings);
	/*!
	 * Description:
	 * Function to read settings from the given settings.
	*/
	void readSettings(QSettings& settings);

	/*!
	 * Description:
	 * Function that checks scan on startup value. If true then it start scanning on startup.
	*/
	void scanOnStartup();

	/*!
	 * Description:
	 * Function to enable the demo version that only cleans 4 files from a directory.
	*/
	void enableDemoVersion(bool b);

signals:
	/*!
	 * Description:
	 * Signal that emits when a process is started (for progress bar).
	*/
	void onStart();
	/*!
	 * Description:
	 * Signal that emits when progress value is changed (for progress bar).
	*/
	void onProgress(double value);
	/*!
	 * Description:
	 * Signal that emits when a process is finished (for progress bar).
	*/
	void onFinished();

public slots:
	/*!
	 * Description:
	 * Slot function to start scanning the root directory and update the directory structure.
	*/
	void startScan();
	/*!
	 * Description:
	 * Slot function to cancel the scanning thread.
	*/
	void cancelScan();
	/*!
	 * Description:
	 * Slot function to update the statistics of the selected scanned directories.
	*/
	void updateStats();

	/*!
	 * Description:
	 * Slot function to start cleaning in the selected directories.
	*/
	void runCleaning();

private slots:
	/*!
	 * Description:
	 * Slot function to browse for root directory.
	*/
	void browseRoot();
	/*!
	 * Description:
	 * Slot function to open the directory when double clicked event occurs.
	*/
	void doubleClickedItem(const QModelIndex& index);

protected:
	Ui::XrayDeleteProjectFilesWidget* ui;

	XrayTableModel* p_tableModel;
	QVector<QVector<QString> > m_tableRows;
	QVector< QVector<QPair<QString, QStringList> > > m_scannedFiles;

	XrayProjectCleanerScanController* p_scanController;

private:
	void closeEvent(QCloseEvent* _event) Q_DECL_OVERRIDE;

	void updateProperties(std::size_t files, std::size_t size);

	/*!
	 * Description:
	 * Function that scans the root directory and returns all sub-directories.
	*/
	QStringList getFilesThatMustBeDeleted();
	/*!
	 * Description:
	 * Function that scans the root directory and returns all sub-directories.
	*/
	QStringList getSubDirs();

	/*!
	 * Description:
	 * Function that delete files.
	*/
	void deleteFiles(const QStringList& files);

	bool m_demoVersion;
};

XRAYLAB_END_NAMESPACE

#endif // XrayDeleteProjectFilesWidget_h__
