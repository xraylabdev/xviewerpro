/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/22
** filename: 	XrayCTMachinesStatusWidget.h
** file base:	XrayCTMachinesStatusWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the file system monitoring widget for
				specified folders.
****************************************************************************/

#pragma once
#ifndef XrayCTMachinesStatusWidget_h__
#define XrayCTMachinesStatusWidget_h__

#include "XrayQtApiExport.h"
#include "XrayMachineLogFileDictionary.h"
#include "XrayQLed.h"

#include <QWidget>
#include <QTreeWidget>
#include <QLabel>
#include <QGroupBox>
#include <QTimer>
#include <QTimeEdit>
#include <QThread>

namespace Ui
{
	class XrayCTMachinesStatusWidget;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayMachineLogFileDictionaryLoaderWorker : public QObject
{
	Q_OBJECT

public:
	explicit XrayMachineLogFileDictionaryLoaderWorker(const QStringList& _files, QObject *parent = nullptr);

signals:
	void result(const QMap<QString, QVariant>& _map);

public slots:
	void read();

private:
	QStringList m_projectDir;
};

class XRAY_QT_API_EXPORT XrayMachineLogFileDictionaryLoaderController : public QObject
{
	Q_OBJECT

public:
	XrayMachineLogFileDictionaryLoaderController(const QStringList& _files, int _updateInterval, QObject *parent = nullptr);
	~XrayMachineLogFileDictionaryLoaderController();

	void setInterval(int _msec);
	int interval() const;

signals:
	void result(const QMap<QString, QVariant>& _map);

public slots:
	void start();

private:
	QThread* p_thread;
	QTimer* p_timer;
};

class XRAY_QT_API_EXPORT XrayCTMachineStatusWidget : public QWidget
{
	Q_OBJECT

public:
	XrayCTMachineStatusWidget(const QStringList& _files, int _updateInterval, QWidget* parent = nullptr);
	~XrayCTMachineStatusWidget();

public slots:
	void updateWidgets(const QMap<QString, QVariant>& _map);
	void updateMoreParamTree(const QMap<QString, QVariant>& _map);
	void start();

private:
	XrayMachineLogFileDictionaryLoaderController* p_logsLoader;

	QGroupBox* groupBox;
	QLabel *computerLabel;
	QLabel *computerValue;
	QLabel *userLabel;
	QLabel *userValue;
	QLabel *powerLabel;
	QLabel *powerValue;

	QLabel *systemRunningStatusLabel;
	XrayQLedBlink* systemRunningStatusValue;

	QLabel *xsampleStatusLabel;
	XrayQLedBlink* xsampleStatusValue;

	QLabel *projectRunningStatusLabel;
	XrayQLedBlink* projectRunningStatusValue;

	QLabel *totalScannedImagesLabel;
	QLabel *totalScannedImagesValue;
	int m_lastTotalScannedImages;

	QLabel *lastScannedImageNumberLabel;
	QLabel *lastScannedImageNumberValue;

	QLabel *scannedImagesDiffLabel;
	QLabel *scannedImagesDiffValue;

	QLabel *controlStartedLabel;
	QLabel *controlStartedValue;
	QLabel *controlUpTimeLabel;
	QLabel *controlUpTimeValue;

	QLabel *xrayRunupTimeLabel;
	QLabel *xrayRunupTimeValue;
	QLabel *xrayRunupUpTimeLabel;
	QLabel *xrayRunupUpTimeValue;

	QLabel *onTimeSrcLabel;
	QLabel *onTimeSrcValue;
	QLabel *calibrationStartedLabel;
	QLabel *calibrationStartedValue;
	QLabel *calibrationSucceededLabel;
	QLabel *calibrationSucceededValue;

	QTreeWidget* p_moreParamTree;
};


class XRAY_QT_API_EXPORT XrayCTMachinesStatusWidget : public QWidget
{
	Q_OBJECT

public:
	XrayCTMachinesStatusWidget(QWidget* parent = nullptr);
	~XrayCTMachinesStatusWidget();

private:
	Ui::XrayCTMachinesStatusWidget* ui;

	XrayMachineLogFileDictionary m_dict;
};

XRAYLAB_END_NAMESPACE

#endif // XrayCTMachinesStatusWidget_h__