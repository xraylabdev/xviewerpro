/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/06
** filename: 	XrayFileSystemStructure.h
** file base:	XrayFileSystemStructure
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the main window of the application.
****************************************************************************/

#pragma once
#ifndef XrayFileSystemStructure_h__
#define XrayFileSystemStructure_h__

#include "XrayQtApiExport.h"
#include <vector>
#include <string>
#include <filesystem>

XRAYLAB_BEGIN_NAMESPACE

class XraySysDirectory;

/****************************************************************************
	Base class for handling directory and files of the file system
****************************************************************************/
class XRAY_QT_API_EXPORT XraySysBase
{
public:
	/*!
	 * Description:
	 * Default constructor.
	*/
	XraySysBase();
	/*!
	 * Description:
	 * Constructor that takes the name of the directory or file and it's parent directory.
	*/
	XraySysBase(const std::string& name, XraySysDirectory* parent);
	/*!
	 * Description:
	 * Destructor to release the internal memory.
	*/
	virtual ~XraySysBase();

	/*!
	 * Description:
	 * Function that remove this object from it's parent directory.
	*/
	bool remove();

	/*!
	 * Description:
	 * Pure virtual function to get the size of the discrete childs.
	*/
	virtual std::size_t getSize() const = 0;

	/*!
	 * Description:
	 * Function to set and get the write time.
	*/
	void setLastWriteTime(std::filesystem::file_time_type time) { m_lastWriteTime = time; }
	std::filesystem::file_time_type getLastWriteTime() const { return m_lastWriteTime; }

	/*!
	 * Description:
	 * Function to set and get the name. It could be a directory or file path.
	*/
	void setName(const std::string& name) { m_name = name; }
	std::string getName() const { return m_name; }

	/*!
	 * Description:
	 * Function to set and get the base name (name of the directory or name of the file without extension).
	*/
	void setBaseName(const std::string& name) { m_baseName = name; }
	std::string getBaseName() const { return m_baseName; }

private:
	XraySysDirectory* m_parent;
	std::string m_name;
	std::string m_baseName;
	std::filesystem::file_time_type m_lastWriteTime;
};

/****************************************************************************
	Class for directory files
****************************************************************************/
class XRAY_QT_API_EXPORT XraySysFile : public XraySysBase
{
public:
	/*!
	 * Description:
	 * Constructor that takes the name of the file (absolute file path), its parent and size of file in bytes.
	*/
	XraySysFile(const std::string& name, XraySysDirectory* parent, std::size_t size);
	/*!
	 * Description:
	 * Destructor to release the internal memory.
	*/
	~XraySysFile() override;

	/*!
	 * Description:
	 * Function to get the size of file in bytes.
	*/
	std::size_t getSize() const override { return m_size; }

	/*!
	 * Description:
	 * Function to set and get the name of the file without extension.
	*/
	void setStem(const std::string& name) { m_stem = name; }
	std::string getStem() const { return m_stem; }

private:
	std::size_t m_size;
	std::string m_stem;	// file name without extension name.tif => name
};

/****************************************************************************
	Class for directories
****************************************************************************/
class XRAY_QT_API_EXPORT XraySysDirectory : public XraySysBase
{
public:
	/*!
	 * Description:
	 * Default constructor.
	*/
	XraySysDirectory();
	/*!
	 * Description:
	 * Constructor that takes the name of the directory (absolute directory path), its parent.
	*/
	XraySysDirectory(const std::string& name, XraySysDirectory* parent);
	/*!
	 * Description:
	 * Destructor to release the internal memory.
	*/
	~XraySysDirectory() override;

	/*!
	 * Description:
	 * Function to clear the internal directory list.
	*/
	void clear();

	/*!
	 * Description:
	 * Function to get the size of all files in bytes.
	*/
	std::size_t getSize() const override;

	/*!
	 * Description:
	 * Function to get the total number of directory and files in the list.
	*/
	int getNumberOfChilds();

	/*!
	 * Description:
	 * Function to get all directories from the internal list.
	*/
	std::vector<XraySysDirectory> getDirectories();
	/*!
	 * Description:
	 * Function to get all files from the internal list.
	*/
	std::vector<XraySysFile> getFiles();

	/*!
	 * Description:
	 * Function to print the whole directory structure.
	*/
	void print();

	/*!
	 * Description:
	 * Function to print the given directory and its files.
	*/
	void print(XraySysBase* parent);

	/*!
	 * Description:
	 * Function to find the directory or file.
	*/
	XraySysBase* get(const std::string& name);

	/*!
	 * Description:
	 * Function to get the directory or file by it's index.
	*/
	XraySysBase* get(std::size_t index);

	/*!
	 * Description:
	 * Function to add a directory or file to the list.
	*/
	void add(XraySysBase* entry);

	/*!
	 * Description:
	 * Function to remove a directory or file from the list.
	*/
	bool remove(XraySysBase* entry);

	/*!
	 * Description:
	 * Function that returns a reference to internal list.
	*/
	std::vector<XraySysBase*>& getContents() { return m_contents; }

private:
	std::vector<XraySysBase*> m_contents;
};

/****************************************************************************
	Class for file system structure
****************************************************************************/
class XRAY_QT_API_EXPORT XrayFileSystemStructure
{
public:
	/*!
	 * Description:
	 * Default constructor that optionally takes extensions filters and name-contain filters.
	 * Default extension filters are {".tif", ".tiff"}
	 * Default contain filters are {"dark", "bright"}
	*/
	XrayFileSystemStructure(const std::vector<std::string>& fileExtFilters = {}, const std::vector<std::string>& fileNameContainFilters = {});
	/*!
	 * Description:
	 * Destructor to release the internal memory.
	*/
	~XrayFileSystemStructure();

	/*!
	 * Description:
	 * Function to scan the root directory and create the directory structure.
	*/
	void scan();

	/*!
	 * Description:
	 * Function to clear the internal directory list.
	*/
	void clear();

	/*!
	 * Description:
	 * Function to get the size of all files in bytes.
	*/
	std::size_t getSize() const;

	/*!
	 * Description:
	 * Function to get the total number of directory and files in the list.
	*/
	int getNumberOfChilds();

	/*!
	 * Description:
	 * Function to get all directories from the internal list.
	*/
	std::vector<XraySysDirectory> getDirectories();
	/*!
	 * Description:
	 * Function to get all files from the internal list.
	*/
	std::vector<XraySysFile> getFiles();

	/*!
	 * Description:
	 * Function to print the whole directory structure.
	*/
	void print();

	/*!
	 * Description:
	 * Function to print the given directory and its files.
	*/
	void print(XraySysBase* parent);

	/*!
	 * Description:
	 * Function to find the directory or file.
	*/
	XraySysBase* get(const std::string& name);

	/*!
	 * Description:
	 * Function to get the directory or file by it's index.
	*/
	XraySysBase* get(std::size_t index);

	/*!
	 * Description:
	 * Function to add a directory or file to the list.
	*/
	void add(XraySysBase* entry);

	/*!
	 * Description:
	 * Function to remove a directory or file from the list.
	*/
	bool remove(XraySysBase* entry);

	/*!
	 * Description:
	 * Function that returns a reference to internal list.
	*/
	std::vector<XraySysBase*>& getContents();

	/*!
	 * Description:
	 * Function to set and get the root directory path.
	*/
	void setRoot(const std::string& name) { m_root = name; }
	std::string getRoot() const { return m_root; }

	/*!
	 * Description:
	 * Function to set and get the file extension filters.
	 * Default extension filters are {".tif", ".tiff"}
	*/
	void setFileExtFilters(const std::vector<std::string>& filters);
	std::vector<std::string> getFileExtFilters() const { return m_fileExtFilters; }

	/*!
	 * Description:
	 * Function to set and get the file contain filters.
	 * Default contain filters are {"dark", "bright"}
	*/
	void setFileContainFilters(const std::vector<std::string>& filters);
	std::vector<std::string> getFileContainFilters() const { return m_fileContainFilters; }

private:
	void scan(const std::filesystem::path& rootPath);
	std::string m_root;
	XraySysDirectory m_sysDir;
	std::vector<std::string> m_fileExtFilters;
	std::vector<std::string> m_fileContainFilters;
};

XRAYLAB_END_NAMESPACE

#endif // XrayFileSystemStructure_h__
