/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/02
** filename: 	XrayFileSystemWatcherWidget.h
** file base:	XrayFileSystemWatcherWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the file system monitoring widget for
				specified folders.
****************************************************************************/

#pragma once
#ifndef XrayFileSystemWatcherWidget_h__
#define XrayFileSystemWatcherWidget_h__

#include "XrayStatusBar.h"
#include "XrayProgressWidget.h"
#include "XrayImageSlideShowWidget.h"
#include "XrayFileSystemWatcherTreeWidget.h"

#include <QMainWindow>
#include <QDockWidget>
#include <QAction>
#include <QLineEdit>
#include <QTabWidget>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayFileSystemWatcherWidget : public QMainWindow
{
	Q_OBJECT

public:
	XrayFileSystemWatcherWidget(QWidget* parent = nullptr);
	~XrayFileSystemWatcherWidget();

	static void updatePcaFileInfoLayout(const QString& _pcaFileName, QGridLayout* _layout);

	bool isDockVisible() const { return dockWidget->isVisible(); }

	auto getFileSystemWatcher() const { return p_fileSysTree; }

signals:
	void dockVisibilityChanged(bool visible);

public slots:
	void setDockVisible(bool _visible) { dockWidget->setVisible(_visible); }
	void updatePcaFileInfoLayout(const QString& _pcaFileName);
	void updateDockWidgetTitle(const QString& _path);
	void updateDockWidgetTitle(const QStringList& _paths);

private:
	void closeEvent(QCloseEvent* _event) override;

protected:
	QDockWidget* dockWidget;
	QGridLayout* pcaFileInfoLayout;
	XrayImageSlideShowWidget* p_slideShowWidget;
	XrayFileSystemWatcherTreeWidget* p_fileSysTree;
	XrayProgressWidget* p_progressWidget;
	XrayStatusBar* p_statusBar;
	QList<QPair<QLabel*, QLabel*> > m_pcaFileInfoLabels;
	QLineEdit* p_searchLineEdit;
	QTabWidget* p_centeralTabWidget;
};

XRAYLAB_END_NAMESPACE

#endif // XrayFileSystemWatcherWidget_h__