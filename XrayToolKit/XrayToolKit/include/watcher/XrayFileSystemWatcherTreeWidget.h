/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayFileSystemWatcher.h
** file base:	XrayFileSystemWatcher
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a tree widget with file system watcher.
****************************************************************************/

#pragma once
#ifndef XrayFileSystemWatcherTreeWidget_h__
#define XrayFileSystemWatcherTreeWidget_h__

#include "XrayQtApiExport.h"
#include "XrayThreadedFileSystemWatcher.h"

#include <QStyledItemDelegate>
#include <QObject>
#include <QDir>
#include <QTimer>
#include <QDateTime>
#include <QString>
#include <QTreeWidget>
#include <QMenu>
#include <QAction>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayFileSystemWatcherTreeWidgetItem : public QObject, public QTreeWidgetItem
{
	Q_OBJECT

public:
	XrayFileSystemWatcherTreeWidgetItem() : QTreeWidgetItem(), p_par(nullptr) {}
	XrayFileSystemWatcherTreeWidgetItem(const QStringList &strings, const QString& _path) : QTreeWidgetItem(strings), p_par(nullptr), m_path(_path) {}
	explicit XrayFileSystemWatcherTreeWidgetItem(QTreeWidget *view) : QTreeWidgetItem(view), p_par(nullptr) {}
	XrayFileSystemWatcherTreeWidgetItem(QTreeWidget *view, const QStringList &strings) : QTreeWidgetItem(view, strings), p_par(nullptr) {}
	explicit XrayFileSystemWatcherTreeWidgetItem(QTreeWidgetItem *parent) : QTreeWidgetItem(parent), p_par(nullptr) {}
	XrayFileSystemWatcherTreeWidgetItem(QTreeWidgetItem *parent, const QStringList &strings, const QString& _path, const QDateTime& _dateTime = QDateTime()) : QTreeWidgetItem(parent, strings), p_par(nullptr), m_path(_path), m_dateTime(_dateTime) {}
	XrayFileSystemWatcherTreeWidgetItem(const XrayFileSystemWatcherTreeWidgetItem &other) : QTreeWidgetItem(other), p_par(nullptr) {}

	/*!
	   Description:
	   Function that returns a clone of this item including it's children.
	*/
	XrayFileSystemWatcherTreeWidgetItem* clone2() const;

	/*!
	   Description:
	   Static function that returns a tree item if any item matches with the given path.
	*/
	static QTreeWidgetItem* searchItem(QTreeWidget* _tree, const QString& _path, QTreeWidgetItemIterator::IteratorFlags _flags = QTreeWidgetItemIterator::All);

	/*!
	   Description:
	   Function to set an additional path string to this item.
	*/
	void setPath(const QString& _fileName) { m_path = _fileName; }
	/*!
	   Description:
	   Function to get the path string of this item.
	*/
	auto getPath() const { return m_path; }
	/*!
	   Description:
	   Function to set an additional path string to this item.
	*/
	void setDateTime(const QDateTime& _dateTime) { m_dateTime = _dateTime; }
	/*!
	   Description:
	   Function to get the path string of this item.
	*/
	auto getDateTime() const { return m_dateTime; }

	/*!
	   Description:
	   Function to get a pointer to item's parent.
	*/
	auto parent() const { return p_par; }

	bool operator < (const QTreeWidgetItem& other) const;

private:
	QTreeWidgetItem* p_par;
	QString m_path;
	QDateTime m_dateTime;
};

class XRAY_QT_API_EXPORT XrayFileSystemWatcherTreeWidget : public QTreeWidget
{
	Q_OBJECT

public:
	/*!
	   Description:
	   Default constructor that takes a pointer to parent object
	   and create a tree widget that provides file system watcher.
	*/
	XrayFileSystemWatcherTreeWidget(QWidget* parent = 0);
	virtual ~XrayFileSystemWatcherTreeWidget();

	/*!
	   Description:
	   Function to set the root directory that should be watched.
	*/
	void setRootPath(const QString& _path);
	/*!
	   Description:
	   Function to get the root directory that is being watched.
	*/
	QString getRootPath() const;
	/*!
	   Description:
	   Function to set the update interval in milliseconds.
	*/
	void setUpdateInterval(int _updateInterval);
	/*!
	   Description:
	   Function to get the update interval.
	*/
	int getUpdateInterval() const;
	/*!
	   Description:
	   Function to restart the thread to update the tree with latest directory structure.
	*/
	void restart();

	/*!
	   Description:
	   Function to build a tree by specifying the given paths.
	*/
	void buildTree();

	/*!
	   Description:
	   Function that saves the watched and tree paths to the given settings.
	   \param - a reference to QSettings.
	*/
	void saveSettings(QSettings& _settings);
	/*!
	   Description:
	   Function that restores the watched and tree paths from the given settings.
	   \param - a reference to QSettings.
	*/
	void restoreSettings(QSettings& _settings);

	/*!
	   Description:
	   Function that perform searching in the entire tree of an item with the given column and text,
	   and if found then returns a pointer to the found item, otherwise return nullptr.
	*/
	QTreeWidgetItem* searchItem(int _column, const QString& _text, QTreeWidgetItemIterator::IteratorFlags _flags = QTreeWidgetItemIterator::All);
	/*!
	   Description:
	   Function that perform searching in the given parent item with the given column and text,
	   and if found then returns a pointer to the found item, otherwise return nullptr.
	*/
	QTreeWidgetItem* searchItem(QTreeWidgetItem* _parent, int _column, const QString& _text, QTreeWidgetItemIterator::IteratorFlags _flags = QTreeWidgetItemIterator::All);

	/*!
	   Description:
	   Function that perform searching in the given parent item with the given column and text,
	   and if found then returns a pointer to the found item, otherwise return nullptr.
	*/
	QTreeWidgetItem* searchChildItem(QTreeWidgetItem* _item, int _column, const QString& _text);

signals:
	/*!
	   Description:
	   This signal is being triggered on start of building directory structure.
	*/
	void onStart();
	/*!
	   Description:
	   This signal is being triggered in the computation of building the directory structure.
	   \param _value - value for the progress bar from [0 ~ 1].
	*/
	void onProgress(double _value);
	/*!
	   Description:
	   This signal is being triggered when the computation of directory structure is finished.
	*/
	void onFinished();
	/*!
	   Description:
	   This signal is being triggered when a user double clicks on the PCA file tree item.
	*/
	void onClicked_onPcaFileItem(const QString& _fileName);
	/*!
	   Description:
	   This signal is being triggered when a user double clicks on the image file tree item.
	*/
	void onClicked_on2DItem(const QStringList& _imgs);

public slots:
	/*!
	   Description:
	   Function to update the whole tree with new directory structure.
	*/
	void updateTree();

	/*!
	   Description:
	   Slots for user interaction with the tree items.
	*/
	void onItemPressed(QTreeWidgetItem *item, int column);
	void onItemClicked(QTreeWidgetItem *item, int column);
	void onItemDoubleClicked(QTreeWidgetItem *item, int column);
	void onItemActivated(QTreeWidgetItem *item, int column);
	void onItemEntered(QTreeWidgetItem *item, int column);
	void searchAndSelectionItem(const QString& _text, int _column);

private:
	/*!
	   Description:
	   Function that build the whole tree with a new directory structure.
	*/
	void buildTree(const QStringList& _list, const QString& _path);
	void buildTree(const QStringList& _paths);

	/*!
	   Description:
	   Function that manages the user interactions with the data.
	*/
	void onClicked_ImageItems(QTreeWidgetItem *item, int column);
	
	/*!
	   Description:
	   Function that shows the context menu of the clicked item.
	*/
	void showContextMenu(const QPoint& _point);
	/*!
	   Description:
	   Function that creates a menu and assign actions to menu items.
	*/
	void createCustomMenu();
	/*!
	   Description:
	   Function that returns the full path of the given item in this tree.
	*/
	QString getItemAbsolutePath(QTreeWidgetItem* _item, int _column);

	QMenu* p_contextMenu;
	QPoint m_currentPoint;

	XrayThreadedFileSystemWatcher* p_watcher;
	QStringList m_paths;
	QString m_rootPath;
	int m_delay;
};

XRAYLAB_END_NAMESPACE

#endif // XrayFileSystemWatcherTreeWidget_h__