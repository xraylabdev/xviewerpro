/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/22
** filename: 	XrayMachineLogFileDictionary.h
** file base:	XrayMachineLogFileDictionary
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that reads the log files of XRAY-LAB CT machines.
****************************************************************************/

#pragma once
#ifndef XrayMachineLogFileDictionary_h__
#define XrayMachineLogFileDictionary_h__

#include "XrayQtApiExport.h"
#include "XrayQFileReader.h"

#include <QSettings>

XRAYLAB_BEGIN_NAMESPACE

/*!
	Description:
	Example to use the class.
	Currently, following keys are being parsing from the file.

	XrayMachineLogFileDictionary dict;

	dict.set_syncmove_file("Log_files/syncmove.log");
	dict.set_calibration_file("Log_files/calibration.log");
	dict.set_dtxa_file("Log_files/dtxa.log");
	dict.set_xs_control_1886_file("Log_files/xs_control-1886.log");
	dict.set_xs_xray_1886_file("Log_files/xs_xray-1886.log");
	dict.read();

	qDebug() << "--------- syncmove.log ---------";

	qDebug() << dict.value("sm_computer");
	qDebug() << dict.value("sm_user");

	qDebug() << "--------- calibration.log ---------";

	qDebug() << dict.value("cal_date");
	qDebug() << dict.value("cal_time");
	qDebug() << dict.value("cal_actual");
	qDebug() << dict.value("cal_nominal");
	qDebug() << dict.value("cal_fod");
	qDebug() << dict.value("cal_fod_delta");
	qDebug() << dict.value("cal_fdd");
	qDebug() << dict.value("cal_user");

	qDebug() << "--------- dtxa.log ---------";

	qDebug() << dict.value("dt_pca_location");
	qDebug() << dict.value("dt_mean");
	qDebug() << dict.value("dt_standard_deviation");
	qDebug() << dict.value("dt_median");
	qDebug() << dict.value("dt_gain_points_0");
	qDebug() << dict.value("dt_gain_points_1");
	qDebug() << dict.value("dt_gain_points_2");
	qDebug() << dict.value("dt_calibration_start_date");
	qDebug() << dict.value("dt_calibration_start_time");
	qDebug() << dict.value("dt_calibration_success_date");
	qDebug() << dict.value("dt_calibration_success_time");
	qDebug() << dict.value("dt_calibration_success_status");
	qDebug() << dict.value("dt_starting_scan_date");
	qDebug() << dict.value("dt_starting_scan_time");

	qDebug() << "--------- xs_control-1886.log ---------";

	qDebug() << dict.value("xsc_voltage");
	qDebug() << dict.value("xsc_current");
	qDebug() << dict.value("xsc_on_time_source");
	qDebug() << dict.value("xsc_runup_date");
	qDebug() << dict.value("xsc_runup_time");
	qDebug() << dict.value("xsc_filament_adjust");
	qDebug() << dict.value("xsc_filament_adjust_ep");
	qDebug() << dict.value("xsc_filament_adjust_moni");
	qDebug() << dict.value("xsc_filament_resistance");
	qDebug() << dict.value("xsc_filament_resistance_voltage");
	qDebug() << dict.value("xsc_gridload_resistance");
	qDebug() << dict.value("xsc_gridload_voltage");
	qDebug() << dict.value("xsc_filament_adjust_started_date");
	qDebug() << dict.value("xsc_filament_adjust_started_time");
	qDebug() << dict.value("xsc_filament_adjust_finished_date");
	qDebug() << dict.value("xsc_filament_adjust_finished_time");

	qDebug() << "--------- xs_xray-1886.log ---------";

	qDebug() << dict.value("xsx_voltage");
	qDebug() << dict.value("xsx_current");
	qDebug() << dict.value("xsx_on_time_source");
	qDebug() << dict.value("xsx_runup_date");
	qDebug() << dict.value("xsx_runup_time");

	qDebug() << "--------- pca_file.pca ---------";

	qDebug() << dict.value("pca_fdd");
	qDebug() << dict.value("pca_fod");
	qDebug() << dict.value("pca_magnification");
	qDebug() << dict.value("pca_voxel_size_x");
	qDebug() << dict.value("pca_number_images");
	qDebug() << dict.value("pca_rotation_sector");
	qDebug() << dict.value("pca_active");
	qDebug() << dict.value("pca_timing_val");
	qDebug() << dict.value("pca_avg");
	qDebug() << dict.value("pca_skip");
	qDebug() << dict.value("pca_binning");
	qDebug() << dict.value("pca_name");
	qDebug() << dict.value("pca_id");
	qDebug() << dict.value("pca_voltage");
	qDebug() << dict.value("pca_current");
	qDebug() << dict.value("pca_mode");
	qDebug() << dict.value("pca_filter");
	qDebug() << dict.value("pca_y_sample");
	qDebug() << dict.value("pca_bhc_param");

*/

class XrayMachineLogFileDictionary
{
public:
	XrayMachineLogFileDictionary();

	/*!
		Description:
		Functions to set log files paths.
	*/
	void set_syncmove_file(const QString& _fileName);
	void set_calibration_file(const QString& _fileName);
	void set_dtxa_file(const QString& _fileName);
	void set_xs_control_1886_file(const QString& _fileName);
	void set_xs_xray_1886_file(const QString& _fileName);
	void set_xs_control_0000_file(const QString& _fileName);
	void parse_pca_file(const QString& _fileName);
	void set_ct_data_dir_path(const QString& _path);

	/*!
		Description:
		Function that read and parse all log files and creates a map of the keys.
	*/
	void read();

	/*!
		Description:
		Function that returns a value of the specified key.
	*/
	auto value(const QString& _key) { return m_map.value(_key); }

	/*!
		Description:
		Function that returns a reference to the internal map.
	*/
	auto& map() { return m_map; }

private:
	/*!
		Description:
		Functions to parse all log files.
	*/
	void parse_syncmove();
	void parse_calibration();
	void parse_dtxa();
	void parse_xs_control_1886();
	void parse_xs_xray_1886();
	void parse_xs_control_0000();
	void parse_pca_file(QSettings& settings);
	void parse_ct_data_dir();

	XrayQFileReader m_syncmove_reader;
	XrayQFileReader m_calibration_reader;
	XrayQFileReader m_dtxae_reader;
	XrayQFileReader m_xs_control_1886_reader;
	XrayQFileReader m_xs_xray_1886_reader;
	XrayQFileReader m_xs_control_0000_reader;
	QString m_ct_data_dir_path;

	QMap<QString, QVariant> m_map;
};

XRAYLAB_END_NAMESPACE

#endif // XrayMachineLogFileDictionary_h__                                                                                                                                                                                                                                                                                   
