/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayFileSystemWatcher.h
** file base:	XrayFileSystemWatcher
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a file system watcher with a time
				interval feature.
****************************************************************************/

#pragma once
#ifndef XrayThreadedFileSystemWatcher_h__
#define XrayThreadedFileSystemWatcher_h__

#include "XrayQtApiExport.h"
#include "XrayQThread.h"

#include <QFileSystemWatcher>
#include <QDir>
#include <QVector>
#include <QSettings>

XRAYLAB_BEGIN_NAMESPACE

/*!
	Description:
	This class can monitor file changes in a directory and calls a callback in response.
*/
class XRAY_QT_API_EXPORT XrayThreadedFileSystemWatcher : public XrayQThread
{
	Q_OBJECT

public:
	/*!
	   Description:
	   Default constructor that takes a pointer to parent object and the interval
	   in (msec) to recompute the directory structure after a change is detected. 
	*/
	XrayThreadedFileSystemWatcher(QObject* parent, const QString& _rootPath, int _updateInterval = 3600000);
	virtual ~XrayThreadedFileSystemWatcher();

	/*!
	   Description:
	   Function to set a directory to watch for changes.
	*/
	void setRootPath(const QString& _path);

	/*!
	   Description:
	   Function to get the files that are being watched.
	   \return - a list of files.
	*/
	QStringList getFiles();
	/*!
	   Description:
	   Function to get the directories that are being watched.
	   \return - a list of directories.
	*/
	QStringList getDirectories();
	/*!
	   Description:
	   Function to set the paths that should be watched.
	   \param _paths - a list of paths.
	*/
	void setPaths(const QStringList& _paths);

	/*!
	   Description:
	   Function to remove all paths from the watcher.
	*/
	void clear();
	/*!
	   Description:
	   Function to remove the specified path from the watcher.
	*/
	void remove(const QString& _path);

	/*!
	   Description:
	   Function to get a tree paths to build a tree structure.
	*/
	void setTreePaths(const QStringList& _paths);
	/*!
	   Description:
	   Function to get the tree paths that was used to build a tree structure.
	   \return - a list of paths.
	*/
	QStringList getTreePaths();

	/*!
	   Description:
	   Function that saves the watched and tree paths to the given settings.
	   \param - a reference to QSettings.
	*/
	void saveSettings(QSettings& _settings);
	/*!
	   Description:
	   Function that restores the watched and tree paths from the given settings.
	   \param - a reference to QSettings.
	*/
	void restoreSettings(QSettings& _settings);

signals:
	/*!
	   Description:
	   This signal is being triggered on start of building directory structure.
	*/
	void onStart();
	/*!
	   Description:
	   This signal is being triggered in the computation of building the directory structure.
	   \param _value - value for the progress bar from [0 ~ 1].
	*/
	void onProgress(double _value);
	/*!
	   Description:
	   This signal is being triggered when the computation of directory structure is finished.
	*/
	void onFinished();

public slots:
	/*!
	   Description:
	   This slot is being triggered when any change in the directory occurs.
	   \param _path - Path of the directory where the change was detected.
	*/
	void directoryChanged(const QString& _path);
	/*!
	   Description:
	   This slot is being triggers when any change in with the file occurs.
	   \param _path - Path of the file where the change was detected.
	*/
	void fileChanged(const QString& _path);

protected:
	/*!
	   Description:
	   Overridden function that is responsible for executing all processes.
	*/
	void run() override;

	/*!
	   Description:
	   This is the main function that locate all directories and files and builds 
	   the directory tree structure.
	*/
	void addPaths();

private:
	QDir m_dir;
	QFileSystemWatcher m_watcher;
	QStringList m_paths;
};

XRAYLAB_END_NAMESPACE

#endif // XrayThreadedFileSystemWatcher_h__