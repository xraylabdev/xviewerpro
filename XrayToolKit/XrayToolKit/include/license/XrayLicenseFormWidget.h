/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/22
** filename: 	XrayLicenseFormWidget.h
** file base:	XrayLicenseFormWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the file system monitoring widget for
				specified folders.
****************************************************************************/

#pragma once
#ifndef XrayLicenseFormWidget_h__
#define XrayLicenseFormWidget_h__

#include "XrayQtApiExport.h"

#include <QStackedWidget>
#include <QSettings>
#include <QCloseEvent>

namespace Ui
{
	class XrayLicenseFormWidget;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayLicenseFormWidget : public QWidget
{
	Q_OBJECT

public:
	XrayLicenseFormWidget(const QString& _winTitle = QString("XLicense Manager"), const QString& _email = QString("request@xray-lab.com"), const QString& _copyRight = QString("2019 by XRAY-LAB GmbH &amp; Co. Kg"), const QString& _engineName = QString("XRAY"), QWidget* parent = nullptr);
	~XrayLicenseFormWidget();

	QStackedWidget* stackedWidget() const;

	void writeSettings(QSettings& _settings);
	void readSettings(QSettings& _settings);

signals:
	void decodingFinished();

public slots:
	void open();
	void save(const QString& fileName);
	void saveAs();

protected:
	void updateHardwareId();
	void readLicenseFile();

	Ui::XrayLicenseFormWidget* ui;

private:
	QString getLicenseFileName() const;

	void closeEvent(QCloseEvent* _event) Q_DECL_OVERRIDE;
};

XRAYLAB_END_NAMESPACE

#endif // XrayLicenseFormWidget_h__