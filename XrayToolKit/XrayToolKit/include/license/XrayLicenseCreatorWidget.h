/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/22
** filename: 	XrayLicenseCreatorWidget.h
** file base:	XrayLicenseCreatorWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the file system monitoring widget for
				specified folders.
****************************************************************************/

#pragma once
#ifndef XrayLicenseCreatorWidget_h__
#define XrayLicenseCreatorWidget_h__

#include "XrayQtApiExport.h"

#include <QWidget>
#include <QSettings>
#include <QCloseEvent>

namespace Ui
{
	class XrayLicenseCreatorWidget;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayLicenseCreatorWidget : public QWidget
{
	Q_OBJECT

public:
	XrayLicenseCreatorWidget(const QString& _winTitle = QString("XLicense Generator"), const QString& _engineName = QString("XRAY"), QWidget* parent = nullptr);
	~XrayLicenseCreatorWidget();

	void writeSettings(QSettings& _settings);
	void readSettings(QSettings& _settings);

protected:
	Ui::XrayLicenseCreatorWidget* ui;

private:
	void closeEvent(QCloseEvent* _event) Q_DECL_OVERRIDE;
};

XRAYLAB_END_NAMESPACE

#endif // XrayLicenseCreatorWidget_h__