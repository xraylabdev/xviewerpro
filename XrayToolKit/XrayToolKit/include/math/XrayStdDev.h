/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/04
** filename: 	XrayStdDev.h
** file base:	XrayStdDev
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the standard deviation of the given data.
****************************************************************************/

#pragma once
#ifndef XrayStdDev_h__
#define XrayStdDev_h__

#include "XrayQtApiExport.h"
#include <vector>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayStdDev
{
public:
	XrayStdDev(int _sample_count = 500);

	void reset();

	void addValue(float _v);

	float getAverage();

	float getSD();

	auto getSampleCount() const { return m_sampleCount; }

private:
	std::vector<float> m_data;
	int m_sampleCount;
};

XRAYLAB_END_NAMESPACE

#endif // XrayStdDev_h__