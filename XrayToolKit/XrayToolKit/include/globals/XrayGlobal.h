/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/01
** filename: 	XrayGlobal.h
** file base:	XrayGlobal
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides global variables and functions for xray
*				app manager.
****************************************************************************/

#pragma once
#ifndef XrayGlobal_h__
#define XrayGlobal_h__

#include "XrayQtApiExport.h"
#include "XrayLogger.h"

#include <spdlog/fmt/ostr.h>

#include <QCoreApplication>
#include <QStringList>
#include <QColor>
#include <QDir>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayGlobal
{
public:
	/*!
	 * Description:
	 * Function to set the current app language.
	*/
	static void setCurrentAppLanguage(const QString& _lang);
	static QString getCurrentAppLanguage(const QString& _defaultLang = "en");
	static bool isCurrentAppLanguageEnglish();

	/*!
	 * Description:
	 * Function to set the current theme style.
	*/
	static void setCurrentThemeStyle(const QString& _style);
	static QString getCurrentThemeStyle();

	/*!
	 * Description:
	 * Function to set the last file dialog open path.
	 * \param _file - name of the file.
	 */
	static void setLastFileOpenPath(const QString& _file);
	/*!
	 * Description:
	 * Function to set the last file dialog open path.
	 * \param _files - This function will extract the absolute path from
	 *	the first file.
	 */
	static void setLastFileOpenPath(const QStringList& _files);

	/*!
	 * Description:
	 * Function to get the last file dialog open path.
	 */
	static QString getLastFileOpenPath();

	/*!
	 * Description:
	 * Function to get all supported image filters.
	 */
	static QStringList getAllSupportedImageExtensions();
	/*!
	 * Description:
	 * Function to get all supported image filters.
	 */
	static QString getAllSupportedImageExtensions(const QString& _text, const QStringList& _extra = QStringList());
	static QString getAllSupportedImageExtensionsForSaveDialog();
	/*!
	 * Description:
	 * Function that returns true if the given file is supported for reading with Qt,
	 * otherwise returns false.
	 */
	static bool isSupportedImageExtension(const QString& _fileName);

	/*!
	 * Description:
	 * Function to get the new size after keeping the same aspect ratio.
	  * \param _old_w -  old width
	  * \param _old_h - old height
	  * \param _new_w - new width
	  * \param _new_h - new height
	  * \return - new size with same aspect ratio
	 */
	static QSize getNewSizeKeepAspectRatio(int _old_w, int _old_h, int _new_w, int _new_h);
	static QSize getNewSizeKeepAspectRatio(const QSize& _old, const QSize& _new);

	/*!
		Description:
		Function that returns true if the current user is admin, otherwise false.
		Used for XraySqlUserManagementWidget.
	*/
	static bool hasLoggedInByAdmin();
	static void setLoggedInByAdmin(bool _b);

private:
	static QDir lastFileOpenPath;
	static QString currentAppLanguage;
	static QString currentThemeStyle;
	static int currentLayoutStyle;
	static bool currentLoggedInByAdmin;
};


XRAYLAB_END_NAMESPACE

// custom types for fmt/logger.
namespace fmt
{
	template <>
	struct fmt::formatter<QString>
	{
		template <typename ParseContext>
		constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

		template <typename FormatContext>
		auto format(const QString &s, FormatContext &ctx)
		{
			return format_to(ctx.out(), "{}", s.toStdString());
		}
	};

	template <>
	struct fmt::formatter<QColor>
	{
		template <typename ParseContext>
		constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

		template <typename FormatContext>
		auto format(const QColor &c, FormatContext &ctx)
		{
			return format_to(ctx.out(), "({}, {}, {}, {})", c.red(), c.green(), c.blue(), c.alpha());
		}
	};

	static inline std::ostream& operator << (std::ostream& stream, const QString& str)
	{
		stream << str.toStdString();
		return stream;
	}
	static inline std::ostream& operator << (std::ostream& stream, const QColor& c)
	{
		stream << "(" << c.red() << ", " << c.green() << ", " << c.blue() << ", " << c.alpha() << ")";
		return stream;
	}
	static inline std::ostream& operator << (std::ostream& stream, const QPoint& c)
	{
		stream << "(" << c.x() << ", " << c.y() << ")";
		return stream;
	}
}

#endif // XrayGlobal_h__                                                                                                                                                                                                                                                                                   
