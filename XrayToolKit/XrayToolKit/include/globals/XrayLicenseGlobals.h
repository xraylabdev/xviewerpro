/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/06/13
** filename: 	XrayLicenseGlobals.h
** file base:	XrayLicenseGlobals
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides global variables and functions for xray
*				licensing system.
****************************************************************************/

#pragma once
#ifndef XrayLicenseGlobals_h__
#define XrayLicenseGlobals_h__

#include "XrayQtApiExport.h"

#include <QString>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayLicenseGlobals
{
public:
	/*!
	 * Description:
	 * Function to set the engine name.
	*/
	static void setEngineName(const QString& _name);

	enum LicenseType
	{
		DEMO = 0,
		EVALUATION,
		PERMANENT,
		EDUCATIONAL
	};

	/*!
	 * Description:
	 * Function to set the license type.
	*/
	static void setLicenseType(LicenseType _type);
	static void setLicenseType(const QString& _type);
	/*!
	 * Description:
	 * Function that returns the license type.
	*/
	static LicenseType getLicenseType();
	static QString getLicenseTypeAsString();
	/*!
	 * Description:
	 * Function that returns true if the DEMO version is enabled.
	*/
	static bool isDemoVersion();
	/*!
	 * Description:
	 * Function that returns true if the license is for commercial use, otherwise returns false.
	 * Only LicenseType::PERMANENT license gives commercial use rights.
	*/
	static bool isForCommercialUse();
	/*!
	 * Description:
	 * Function that returns true if the license is for evaluation use, otherwise returns false.
	*/
	static bool isForEvaluationUse();
	/*!
	 * Description:
	 * Function that returns true if the license is for educational use, otherwise returns false.
	*/
	static bool isForEducationalUse();

	/*!
	 * Description:
	 * Function that returns the encryption key.
	*/
	static quint64 encryptionKey();

	/*!
	 * Description:
	 * Function to set the available modules.
	*/
	static void setModules(const QString& _type);
	static QString getModules();

	/*!
	 * Description:
	 * Function that returns true if XREPORT PRO module is found in the given string.
	*/
	static bool hasXReportProModule();
	/*!
	 * Description:
	 * Function that returns true if XVOID PRO module is found in the given string.
	*/
	static bool hasXVoidProModule();
	/*!
	 * Description:
	 * Function that returns true if XENHANCER PRO module is found in the given string.
	*/
	static bool hasXEnhancerProModule();
	/*!
	 * Description:
	 * Function that returns true if XUSER-MANAGEMENT module is found in the given string.
	*/
	static bool hasXUserPanelModule();
	/*!
	 * Description:
	 * Function that returns true if XUSER-MANAGEMENT module is found in the given string.
	*/
	static bool hasXFilesCleanerModule();

	/*!
	 * Description:
	 * Function to set the expired status
	*/
	static void setExpired(bool _value);
	/*!
	 * Description:
	 * Function that returns true if the license is expired.
	*/
	static bool hasExpired();
};

XRAYLAB_END_NAMESPACE

#endif // XrayLicenseGlobals_h__                                                                                                                                                                                                                                                                                   
