/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/25
** filename: 	XrayQFileReader.h
** file base:	XrayQFileReader
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class to read the text files.
****************************************************************************/

#pragma once
#ifndef XrayQFileReader_h__
#define XrayQFileReader_h__

#include "XrayQtApiExport.h"

#include <QString>
#include <QFile>

XRAYLAB_BEGIN_NAMESPACE

/*!
	This class reads the text files.
*/
class XRAY_QT_API_EXPORT XrayQFileReader
{
public:
	/*!
		Description:
		Default constructor to create an object with default values.
	*/
	XrayQFileReader();
	/*!
		Description:
		Constructor to create an object by specified values.
		\param _fileName - name of the file to be read.
		\param _nLines - number of lines that should be read from the end to start.
		\param _charactersPerLine - number of characters per line. 
		(Usually a guess on, how many characters a line can have in the file.)
	*/
	XrayQFileReader(const QString& _fileName, int _nLines = -1, int _charactersPerLine = 256);

	/*!
		Description:
		Function that reads the file, split into lines and returns a list of lines.
	*/
	QStringList getLines();
	/*!
		Description:
		Function that reads the file, split into lines and returns a list of lines.
		\param _fileName - name of the file to be read.
		\param _nLines - number of lines that should be read from the end to start. (Less than 0 means to read all lines of the file.)
		\param _charactersPerLine - number of characters per line.
		(Usually a guess on, how many characters a line can have in the file.)
	*/
	QStringList getLines(const QString& _fileName, int _nLines = -1, int _charactersPerLine = 256);

	/*!
		Description:
		Function to set a number of lines that should be read from the end to start.
		\param _lines - less than 0 means to read all lines of the file.
	*/
	void setNumberOfLines(int _lines) { m_nLines = _lines; }
	/*!
		Description:
		Function that returns a number of lines that should be read from the end to start.
	*/
	int getNumberOfLines() const { return m_nLines; }

	/*!
		Description:
		Function to set number of characters per line.
		(Usually a guess on, how many characters a line can have in the file.)
	*/
	void setCharactersPerLine(int _char) { m_charactersPerLine = _char; }
	/*!
		Description:
		Function that returns number of characters per line.
	*/
	int getCharactersPerLine() const { return m_charactersPerLine; }

	/*!
		Description:
		Function to set the file name that should be read.
	*/
	void setFileName(const QString& _fileName) { return m_file.setFileName(_fileName); }
	/*!
		Description:
		Function that returns the file name.
	*/
	auto fileName() const { return m_file.fileName(); }

private:
	QFile m_file;
	int m_nLines;
	int m_charactersPerLine;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQFileReader_h__