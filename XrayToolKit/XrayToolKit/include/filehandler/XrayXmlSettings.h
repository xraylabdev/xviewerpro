/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/01
** filename: 	XrayXmlSettings.h
** file base:	XrayXmlSettings
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides reading and writing QSettings to xml.

** Usage example:
QSettings s("C:/Users/ful/Music/XPAINTPRO/apps_new_xs_info.xml", XrayXmlSettings::format);
s.beginGroup("XPAINT_PRO");	// note: never use space in any of QSettings keys, instead use underscores (_).
//s.setValue("Version", "114");
//s.setValue("ReleaseDate", "19/06/2019");
//s.setValue("ReleaseStatus", "Major Update");
qDebug() << s.value("Version").toString();
qDebug() << s.value("ReleaseDate").toString();
qDebug() << s.value("ReleaseStatus").toString();
s.endGroup();

Important for reading xml files:
This class does not read values if a file contains more than one groups.
Save values into one group and then read values, which works fine.
****************************************************************************/

#pragma once
#ifndef XrayXmlSettings_h__
#define XrayXmlSettings_h__

#include "XrayQtApiExport.h"

#include <QIODevice>
#include <QSettings>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayXmlSettings
{
public:
	/*!
		Description:
		Function that reads the xml file to QSettings.
	*/
	static bool read(QIODevice& device, QSettings::SettingsMap& map);
	/*!
		Description:
		Function that writes QSettings to xml file.
	*/
	static bool write(QIODevice& device, const QSettings::SettingsMap& map);

	static const QSettings::Format format;
};

XRAYLAB_END_NAMESPACE

#endif // XrayXmlSettings_h__                                                                                                                                                                                                                                                                                   
