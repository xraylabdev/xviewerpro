/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/26
** filename: 	XrayCSVHandler.h
** file base:	XrayCSVHandler
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class to read the *.csv file with optional delimiter.
****************************************************************************/

#pragma once
#ifndef XrayCSVHandler_h__
#define XrayCSVHandler_h__

#include "XrayQtApiExport.h"

#include <string>
#include <vector>

XRAYLAB_BEGIN_NAMESPACE

/*!
	This class reads the *.csv file with optional delimiter.
*/
class XRAY_QT_API_EXPORT XrayCSVHandler
{
public:
	XrayCSVHandler();
	XrayCSVHandler(const std::string& fileName, char delimiter);

	/*!
	 * Description:
	 * Function to read the *.csv file.
	 * \param fileName - absolute file path with file name.
	 * \param delimiter - separator that could be , or ; or etc.
	 * \return - it returns true on success.
	 */
	bool read(const std::string& fileName, char delimiter);

	/*!
	 * Description:
	 * Function to print out the data.
	 */
	void print(char delimiter);

	/*!
	 * Description:
	 * Function to get the header elements.
	 */
	std::vector<std::string> getHeaders() const;
	/*!
	 * Description:
	 * Function to get the data elements.
	 */
	std::vector<std::vector<std::string> > getData() const;

private:
	bool XrayCSVHandler::read_old(const std::string& fileName, char delimiter);

	std::vector<std::string> m_headers;
	std::vector<std::vector<std::string>> m_data;
};

XRAYLAB_END_NAMESPACE

#endif // XrayCSVHandler_h__