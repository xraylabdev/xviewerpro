/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/28
** filename: 	XraySingletonQApplication.h
** file base:	XraySingletonQApplication
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that helps to run only one instance of this application.
If a user wants to run a second instance, bringToFront() will bring
the first instance to front, instead to start a new instance.
Put this function just after setting the QApplication::setApplicationName() and
before creating any QWidget object.

****************************************************************************/

#pragma once
#ifndef XraySingletonQApplication_h__
#define XraySingletonQApplication_h__

#include "XrayQApplication.h"

#include <QLocalServer>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XraySingletonQApplication : public XrayQApplication
{
	Q_OBJECT

public:
	/*!
	   Description:
	   Constructor to create a QApp singleton object.
	   \param serverName - a unique name of the application
	*/
	XraySingletonQApplication(int &argc, char **argv, const QString &serverName);
	virtual ~XraySingletonQApplication();

	/*!
	   Description:
	   Function that checks if there is already an instance running.
	   \returns - true if there is an instance running.
	*/
	bool isRunning();

	/*!
	   Description:
	   Function to bring the main window of this program to front.
	   This function checks if the application is already running and
	   takes the applicationName() as to look for the running executable.
	   It calls std::exit(0) if the application is already been running.
	   Note: only works on windows.
	*/
	void bringToFront();

signals:
	/*!
	   Description:
	   The program instance is run through socket communication, and
	   the function is triggered when a new connection is detected.

	   This signal is emitted when a new/second instance sends signal to the first instance.
	   Ex. QObject::connect(&app, &XraySingletonQApplication::firstInstanceCalled, []() {qDebug() << "First Instance Called"; });
	*/
	void firstInstanceCalled();

private slots:
	void newLocalConnection();

private:
	/*!
	   Description:
	   The program instance is run through socket communication, and
	   the function is triggered when a new connection is detected.
	*/
	void initLocalConnection();

	/*!
	   Description:
	   Implement program single instance running through socket communication,
	   Initialize the local connection, if the connection is not on the server,
	   create otherwise.
	*/
	void newLocalServer();

	QLocalServer* p_localServer;
	QString m_serverName;
	bool m_isRunning;
};

XRAYLAB_END_NAMESPACE

#endif // XraySingletonQApplication_h__