/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/08
** filename: 	XrayHistogramViewWidget.h
** file base:	XrayHistogramViewWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a double value slider.
****************************************************************************/

#pragma once
#ifndef XrayHistogramViewWidget_h__
#define XrayHistogramViewWidget_h__

#include "XrayQtApiExport.h"

#include <QWidget>
#include <QPaintEvent>
#include <QPolygonF>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayHistogramViewWidget : public QWidget
{
	Q_OBJECT

public:
	XrayHistogramViewWidget(QWidget* _parent);

	void clear(const QColor& _color =  Qt::black);

	QSize sizeHint() const { return QSize(150, 40); }

	void setHistogram(const QVector<qreal>& _histogram);

	void setColor1(const QColor& _color);
	void setColor2(const QColor& _color);
	void setColor(const QColor& _color1, const QColor& _color2);
	void setLineColor(const QColor& _color);
	void setBorderEnabled(bool b);

	bool isBorderEnabled() const;

private:
	void paintEvent(QPaintEvent* e);

	void generateShade();

	QColor m_color1;
	QColor m_color2;
	QColor m_penColor;

	bool m_drawBorder;

	QImage m_shade;

	QVector<qreal> m_histogram;
};

XRAYLAB_END_NAMESPACE

#endif // XrayHistogramViewWidget_h__
