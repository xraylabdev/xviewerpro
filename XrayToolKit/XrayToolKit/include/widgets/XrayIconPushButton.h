/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/06
** filename: 	XrayIconPushButton.h
** file base:	XrayIconPushButton
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a customized push button with an icon
*				on it.
****************************************************************************/

#pragma once
#ifndef XrayIconPushButton_h__
#define XrayIconPushButton_h__

#include "XrayQtApiExport.h"

#include <QPushButton>
#include <QEvent>
#include <QToolButton>
#include <QStyle>
#include <QStyleOptionToolButton>
#include <QPainter>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayIconPushButton : public QPushButton
{
public:
	/*!
		Description:
		Default constructor to create a push button by specifying an icon and text.
	*/
	XrayIconPushButton(const QIcon& icon, const QString& text, QWidget* parent = nullptr);
	XrayIconPushButton(const QIcon& icon, QWidget* parent = nullptr);

protected:
	bool event(QEvent* _event) override;
};

class XrayIconToolButton : public QToolButton
{
	Q_OBJECT

public:
	explicit XrayIconToolButton(QWidget *parent) :
		QToolButton(parent)
	{
	}

	void paintEvent(QPaintEvent *) override
	{
		QPainter p(this);
		QStyleOptionToolButton opt;
		opt.init(this);

		if (isEnabled() && underMouse())
		{
			if (isDown())
				opt.state |= QStyle::State_Sunken;
			else
				opt.state |= QStyle::State_Raised;

			style()->drawPrimitive(QStyle::PE_PanelButtonTool, &opt, &p, this);
		}

		opt.subControls = QStyle::SC_None;
		opt.features = QStyleOptionToolButton::None;
		opt.icon = icon();
		opt.iconSize = size();
		style()->drawComplexControl(QStyle::CC_ToolButton, &opt, &p, this);
	}
};

XRAYLAB_END_NAMESPACE

#endif // XrayIconPushButton_h__                                                                                                                                                                                                                                                                                   
