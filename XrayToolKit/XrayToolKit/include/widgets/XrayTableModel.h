/****************************************************************************
** Copyright (C) 2007-2018 Klaralvdalens Datakonsult AB.  All rights reserved.
**
** This file is part of the KD Reports library.
**
** Licensees holding valid commercial KD Reports licenses may use this file in
** accordance with the KD Reports Commercial License Agreement provided with
** the Software.
**
**
** This file may be distributed and/or modified under the terms of the
** GNU Lesser General Public License version 2.1 and version 3 as published by the
** Free Software Foundation and appearing in the file LICENSE.LGPL.txt included.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Contact info@kdab.com if any conditions of this licensing are not
** clear to you.
**
**********************************************************************/

#pragma once
#ifndef XrayTableModel_h__
#define XrayTableModel_h__

#include "XrayQtApiExport.h"

#include <QVariant>
#include <QVector>
#include <QAbstractTableModel>
#include <QStringList>

XRAYLAB_BEGIN_NAMESPACE

/** TableModel uses a simple rectangular vector of vectors to represent a data
	table that can be displayed in regular Qt Interview views.
	Additionally, it provides a method to load CSV files exported by
	OpenOffice Calc in the default configuration. This allows to prepare test
	data using spreadsheet software.

	It expects the CSV files in the subfolder ./modeldata. If the application
	is started from another location, it will ask for the location of the
	model data files.
*/

class XRAY_QT_API_EXPORT XrayTableModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	explicit XrayTableModel(QObject * parent = 0);
	~XrayTableModel();

	/** Return header data from the model.
		The model will use the first data row and the first data column of the
		physical data as source of column and row header data. This data isnot
		exposed as model data, that means, the first model row and column will
		start at index (1,1).
	*/
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	int rowCount(const QModelIndex & parent = QModelIndex()) const override;

	int columnCount(const QModelIndex & parent = QModelIndex()) const override;

	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;

	bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole) override;

	/*!
		Description:
		Inserts a number of rows into the model at the specified position.
		@returns true if successful, false otherwise
	*/
	bool insertRows(int position, int rows, const QModelIndex &parent = QModelIndex()) override;
	/*!
		Description:
		Inserts a number of columns into the model at the specified position.
		Each entry in the list is extended in turn with the required number of
		empty strings.
		@returns true if successful, false otherwise
	*/
	bool insertColumns(int position, int columns, const QModelIndex &parent = QModelIndex()) override;
	/*!
		Description:
		Removes a number of rows from the model at the specified position.
		@returns true if successful, false otherwise
	*/
	bool removeRows(int position, int rows, const QModelIndex &parent = QModelIndex()) override;
	/*!
		Description:
		Removes a number of columns from the model at the specified position.
		Each row is shortened by the number of columns specified.
		@returns true if successful, false otherwise
	*/
	bool removeColumns(int position, int columns, const QModelIndex &parent = QModelIndex()) override;

	 /*!
		 Description:
		 Function to create a table from the given headers and rows.
		 @returns true if successful, false otherwise
	 */
	bool createTable(const QVector<QString>& _headers, const QVector<QVector<QString> >& _rows);
	bool createTable(const std::vector<std::string>& _headers, const std::vector<std::vector<std::string> >& _rows);
	/*!
		Description:
		Function to create a table from the given CSV file and the delimiter (comma, space, colon, semicolon, etc.). 
		@returns true if successful, false otherwise
	*/
	bool readCSV(const QString& filename, QChar delimiter);
	/*!
		Description:
		Function to write the table data to CSV/txt file with the given delimiter (comma, space, colon, semicolon, etc.).
		@returns true if successful, false otherwise
	*/
	bool writeCSV(const QString& filename, QChar delimiter);
	bool writeCSV(const QString& filename, const std::vector<std::string>& _header, const std::vector<std::vector<std::string> >& _rows, QChar delimiter);
		/*!
		Description:
		Function to write the table data to string list with the given delimiter (comma, space, colon, semicolon, etc.).
		@returns true if successful, false otherwise
	*/
	QStringList writeToString(QChar delimiter);

	/** Make the model invalid, that is, provide no data. */
	void clear();

	/**
	 * Set to false if the data has no horizontal header
	 */
	void setDataHasHorizontalHeaders(bool value) { m_dataHasHorizontalHeaders = value; }
	/**
	 * Set to false if the data has no vertical header
	 */
	void setDataHasVerticalHeaders(bool value) { m_dataHasVerticalHeaders = value; }
	/**
	 * setSupplyHeaderData(false) allows to prevent the model from supplying header data,
	 * even if parsing found any
	 */
	void setSupplyHeaderData(bool value) { m_supplyHeaderData = value; }

private:
	// the vector of rows:
	QList< QList<QVariant> > m_rows;

	// the header data:
	QStringList m_horizontalHeaderData;
	QStringList m_verticalHeaderData;
	bool m_dataHasHorizontalHeaders;
	bool m_dataHasVerticalHeaders;
	bool m_supplyHeaderData;
};

XRAYLAB_END_NAMESPACE

#endif   // XrayTableModel_h__
