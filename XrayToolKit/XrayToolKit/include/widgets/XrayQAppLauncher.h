/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/28
** filename: 	XrayQAppLauncher.h
** file base:	XrayQAppLauncher
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a QApplication object with some
				pre-defined attributes.
****************************************************************************/

#pragma once
#ifndef XrayQAppLauncher_h__
#define XrayQAppLauncher_h__

#include "XrayQtApiExport.h"

#include <QApplication>

#include <QObject>
#include <QProcess>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQAppLauncher : public QObject
{
	Q_OBJECT

public:
	/*!
	   Description:
	   Constructor to create a QApp object.
	*/
	explicit XrayQAppLauncher(QObject *parent = 0);

	/*!
	   Description:
	   Function that initializes globals attributes.
	*/
	Q_INVOKABLE void lauch(const QString &program);
	Q_INVOKABLE void lauch(const QString &program, const QStringList &arguments);
	Q_INVOKABLE bool lauchDetached(const QString &program);
	Q_INVOKABLE bool isRunning();

signals:
	void errorOccurred(const QString error);

public slots:
	void onErrorOccurred(QProcess::ProcessError error);

protected:
	QProcess* m_process;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQAppLauncher_h__