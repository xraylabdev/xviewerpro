/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/18
** filename: 	XrayQListWidget.h
** file base:	XrayQListWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a list widget.
** Usage:
auto list = new XrayQListWidget();
****************************************************************************/

#pragma once
#ifndef XrayQListWidget_h__
#define XrayQListWidget_h__

#include "XrayQtApiExport.h"

#include <QListWidget>
#include <QMenu>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQListWidget : public QListWidget
{
	Q_OBJECT

public:
	/*!
	   Description:
	   Default constructor to create a list widget.
	   Set setContextMenuPolicy(Qt::NoContextMenu) in order to disable custom menu;
	   @param - parent is the parent to this widget.
	*/
	XrayQListWidget(QWidget *parent = nullptr);
	virtual ~XrayQListWidget();

	enum MenuAction
	{
		SelectAll = 1,
		InvertSelection = 2,
		Copy = 4,
		Paste = 8,
		RemoveSelected = 16
	};

	/*!
	   Description:
	   Function to set a flag for custom menu actions.
	   Set setContextMenuPolicy(Qt::NoContextMenu) in order to disable custom menu;
	   @param - _actions can be set like (MenuActions::SelectAll | MenuActions::InvertSelection | MenuActions::RemoveSelected).
	*/
	void setCustomMenuAction(const MenuAction _actions) { m_menuAction = _actions; }
	/*!
	   Description:
	   Function to get a flag for custom menu actions.
	   @returns - actions flag.
	*/
	MenuAction getCustomMenuAction() const { return m_menuAction; }

signals:
	/*!
	   Description:
	   This signal is being triggered when a custom-menu-action MenuAction::Copy is triggered.
	*/
	void copy();
	/*!
	   Description:
	   This signal is being triggered when a custom-menu-action MenuAction::Paste is triggered.
	*/
	void paste();
	/*!
	   Description:
	   This signal is being triggered when a custom-menu-action MenuAction::RemoveSelected is triggered.
	   @param - _indexList is the list of selected rows.
	*/
	void removed(const std::vector<int>& _removedRows);

public slots:
	/*!
	   Description:
	   Function that shows the context menu of the clicked item.
	*/
	void showContextMenu(const QPoint& _point);

protected:
	/*!
	   Description:
	   Function that creates a menu and assign actions to menu items.
	*/
	virtual void createCustomMenu();

	QMenu* p_contextMenu;
	QPoint m_currentPoint;
	MenuAction m_menuAction;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQListWidget_h__