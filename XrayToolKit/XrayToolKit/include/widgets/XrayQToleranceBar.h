/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/19
** filename: 	XrayQToleranceBar.h
** file base:	XrayQToleranceBar
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a colorized (rainbow) tolerance bar.
****************************************************************************/

#pragma once
#ifndef XrayQToleranceBar_h__
#define XrayQToleranceBar_h__

#include "XrayQtApiExport.h"

#include <QWidget>
#include <QtGui>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQToleranceBar : public QWidget
{
	Q_OBJECT

public:
	explicit XrayQToleranceBar(QWidget *parent = 0);

	/*!
		Function to set the painter width and height.
	*/
	void setPainterSize(const QSize& _size);
	/*!
		Function that returns the painter width and height.
	*/
	QSize painterSize() const;

	/*!
		Function that returns widget value.
	*/
	int value() const;
	/*!
		Function that returns the total bars in the widget.
	*/
	int totalBars() const;

public slots:
	/*!
		Function to set the widget value.
	*/
	void setValue(int _value);
	/*!
		Function to set the total bars in the widget.
	*/
	void setTotalBars(int _value);

protected:
	void paintEvent(QPaintEvent*);
	bool calcValue(const QPoint& _pos);

	void mousePressEvent(QMouseEvent*  _event);
	void mouseMoveEvent(QMouseEvent* _event);
	void mouseReleaseEvent(QMouseEvent* _event);

	int m_value;
	int m_totalBars;
	bool m_pressed;
	QSize m_painterSize;
	QPoint m_pos;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQToleranceBar_h__