/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/18
** filename: 	XrayQTreeWidget.h
** file base:	XrayQTreeWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a list widget.
** Usage:
auto tree = new XrayQTreeWidget();
****************************************************************************/

#pragma once
#ifndef XrayQTreeWidget_h__
#define XrayQTreeWidget_h__

#include "XrayQtApiExport.h"

#include <QTreeWidget>
#include <QMenu>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQTreeWidgetItem : public QObject, public QTreeWidgetItem
{
	Q_OBJECT

public:
	explicit XrayQTreeWidgetItem(int type = Type);
	explicit XrayQTreeWidgetItem(const QStringList &strings, int type = Type);
	explicit XrayQTreeWidgetItem(QTreeWidget *view, int type = Type);
	XrayQTreeWidgetItem(QTreeWidget *view, const QStringList &strings, int type = Type);
	XrayQTreeWidgetItem(QTreeWidget *view, QTreeWidgetItem *after, int type = Type);
	explicit XrayQTreeWidgetItem(QTreeWidgetItem *parent, int type = Type);
	XrayQTreeWidgetItem(QTreeWidgetItem *parent, const QStringList &strings, int type = Type);
	XrayQTreeWidgetItem(QTreeWidgetItem *parent, QTreeWidgetItem *after, int type = Type);
	XrayQTreeWidgetItem(const QTreeWidgetItem &other);
	virtual ~XrayQTreeWidgetItem();

	bool testFlag(Qt::ItemFlag flag) const;
	void setFlag(Qt::ItemFlag flag, bool enabled = true);

	virtual void setData(int column, int role, const QVariant& value);
};

class XRAY_QT_API_EXPORT XrayQTreeWidget : public QTreeWidget
{
	Q_OBJECT

public:
	/*!
	   Description:
	   Default constructor to create a tree widget.
	   Set setContextMenuPolicy(Qt::NoContextMenu) in order to disable custom menu;
	   @param - parent is the parent to this widget.
	*/
	XrayQTreeWidget(QWidget *parent = nullptr);
	virtual ~XrayQTreeWidget();

	/*!
	   Description:
	   Function that perform searching in the entire tree of an item with the given column and text,
	   and if found then returns a pointer to the found item, otherwise return nullptr.
	*/
	QTreeWidgetItem* searchItem(int _column, const QString& _text, QTreeWidgetItemIterator::IteratorFlags _flags = QTreeWidgetItemIterator::All);
	/*!
	   Description:
	   Static function that perform searching in the given parent item with the given column and text,
	   and if found then returns a pointer to the found item, otherwise return nullptr.
	*/
	static QTreeWidgetItem* searchItem(QTreeWidgetItem* _item, int _column, const QString& _text);

	/*!
	   Description:
	   Function that perform searching in the given column with the given text,
	   and if found then select the item and make it current item if _setCurrentItem is true.
	*/
	void selectItem(int _column, const QString& _text, bool _setCurrentItem = false);

	enum MenuAction
	{
		SelectAll = 1,
		InvertSelection = 2,
		Copy = 4,
		Paste = 8,
		RemoveSelected = 16
	};

	/*!
	   Description:
	   Function to set a flag for custom menu actions.
	   Set setContextMenuPolicy(Qt::NoContextMenu) in order to disable custom menu;
	   @param - _actions can be set like (MenuActions::SelectAll | MenuActions::InvertSelection | MenuActions::RemoveSelected).
	*/
	void setCustomMenuAction(const MenuAction _actions) { m_menuAction = _actions; }
	/*!
	   Description:
	   Function to get a flag for custom menu actions.
	   @returns - actions flag.
	*/
	MenuAction getCustomMenuAction() const { return m_menuAction; }

signals:
	/*!
	   Description:
	   This signal is being triggered when a custom-menu-action MenuAction::Copy is triggered.
	*/
	void copy();
	/*!
	   Description:
	   This signal is being triggered when a custom-menu-action MenuAction::Paste is triggered.
	*/
	void paste();
	/*!
	   Description:
	   This signal is being triggered when a custom-menu-action MenuAction::RemoveSelected is triggered.
	   @param - _indexList is the index list of top level items.
	*/
	void removed(const std::vector<int>& _removedTopLevelItems);

	/*!
	   Description:
	   This signal is being triggered when item's check state is changed.
	   @param - state changed item.
	*/
	void itemCheckStateChanged(XrayQTreeWidgetItem* _item);

public slots:
	/*!
	   Description:
	   Function that shows the context menu of the clicked item.
	*/
	void showContextMenu(const QPoint& _point);

	/*!
	   Description:
	   Function to make all top level items checked.
	*/
	void allChecked();
	/*!
	   Description:
	   Function to make all top level items unchecked.
	*/
	void allUnchecked();
	/*!
	   Description:
	   Function to toggle all top level items.
	*/
	void doToggleCheckState(int _column);

protected:
	/*!
	   Description:
	   Function that creates a menu and assign actions to menu items.
	*/
	virtual void createCustomMenu();

	QMenu* p_contextMenu;
	QPoint m_currentPoint;
	MenuAction m_menuAction;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQTreeWidget_h__