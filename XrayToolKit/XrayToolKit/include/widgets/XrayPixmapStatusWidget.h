/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayPixmapStatusWidget.h
** file base:	XrayPixmapStatusWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a timer widget that changes the pixmaps
				for a slideshow.
****************************************************************************/

#pragma once
#ifndef XrayPixmapStatusWidget_h__
#define XrayPixmapStatusWidget_h__

#include "XrayQtApiExport.h"

#include <QGridLayout>
#include <QImage>
#include <QLabel>
#include <QList>
#include <QMap>
#include <QPixmap>
#include <QTimer>
#include <QWidget>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayPixmapStatusWidget : public QWidget
{
	Q_OBJECT

public:
	enum Role
	{
		Okay,
		Error,
		Busy
	};

	XrayPixmapStatusWidget(QWidget* parent = 0, Role role = Okay);
	virtual ~XrayPixmapStatusWidget();

	void setIcon(Role role, const QPixmap& icon);
	const QPixmap& getIcon(Role role) const;
	void setFrames(Role role, const QPixmap& frames, int numFrames, double frameRate = 10.0);
	void setFrames(Role role, const QList<QPixmap>& frameList, double frameRate = 10.0);
	const QList<QPixmap>& getFrames(Role role) const;
	void setFrameRate(Role role, double frameRate);
	double getFrameRate(Role role) const;
	void setCurrentRole(Role role, const QString& toolTip = QString());
	Role getCurrentRole() const;

	void pushCurrentRole();
	bool popCurrentRole();

signals:
	void currentRoleChanged(Role role);

private:
	QGridLayout* p_layout;
	QLabel* p_labelIcon;
	QTimer* p_timer;

	QMap<Role, QList<QPixmap> > m_frames;
	QMap<Role, double> m_frameRates;
	QList<Role> m_roleStack;
	QList<QString> m_toolTipStack;

	Role m_currentRole;
	int m_currentFrame;

	void start();
	void step();
	void stop();

private slots:
	void timerTimeout();
};

XRAYLAB_END_NAMESPACE

#endif // XrayPixmapStatusWidget_h__