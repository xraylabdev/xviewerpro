/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayThreadedTimer.h
** file base:	XrayThreadedTimer
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a thread with a timer in it.
****************************************************************************/

#pragma once
#ifndef XrayThreadedTimer_h__
#define XrayThreadedTimer_h__

#include "XrayQtApiExport.h"

#include <QThread>
#include <QTimer>
#include <QMutex>
#include <QWaitCondition>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayThreadedTimer : public QThread
{
	Q_OBJECT

public:
	/*!
	   Description:
	   Default constructor that takes a pointer to parent object.
	*/
	XrayThreadedTimer(QObject* parent = nullptr);
	virtual ~XrayThreadedTimer();

	/*!
	   Description:
	   Function to get the timer id.
	*/
	int getTimerId() const;
	/*!
	   Description:
	   Function to set the timer interval in milliseconds.
	*/
	void setInterval(int _interval);
	/*!
	   Description:
	   Function to get the timer interval in milliseconds.
	*/
	int getInterval() const;

private slots:
	/*!
		Description:
		Slot for the timer's timeout.
	*/
	void timerTimeout();

protected:
	/*!
		Description:
		Virtual function that is expected to be overridden.
	*/
	void run() override;

	QTimer* p_timer;
};

XRAYLAB_END_NAMESPACE

#endif // XrayThreadedTimer_h__