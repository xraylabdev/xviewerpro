/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/06
** filename: 	XraySortFilterProxyModel.h
** file base:	XraySortFilterProxyModel
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a a multi column filter model.
****************************************************************************/

#pragma once
#ifndef XrayQThread_h__
#define XrayQThread_h__

#include "XrayQtApiExport.h"

#include <QSortFilterProxyModel>
#include <QVariant>
#include <QRegExp>
#include <QMap>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayProxyModelFilter
{
public:
	/*!
	   Description:
	   Constructor to create a new filter.
	*/
	XrayProxyModelFilter(const QVariant& value = QVariant(), const int role = Qt::DisplayRole, const Qt::MatchFlags flags = Qt::MatchContains);

	/*!
	   Description:
	   Function that returns true if the given value is matching the internal value.
	*/
	bool acceptsValue(const QVariant& value);

	QVariant m_value;
	int m_role;
	Qt::MatchFlags m_flags;
};

class XRAY_QT_API_EXPORT XraySortFilterProxyModel : public QSortFilterProxyModel
{
	Q_OBJECT

public:
	/*!
	   Description:
	   Constructor to create a new model.
	*/
	XraySortFilterProxyModel(QObject *parent = 0);

	/*!
	   Description:
	   Function to set the root index of the model.
	   For example: setRootIndex(QFileSystemModel->index(QFileSystemModel->rootPath()))
	*/
	void setRootIndex(const QModelIndex& _index);

	/*!
	   Description:
	   Function that tells the model you want to declare a new filter.
	   If you have a lot of data in your model it can be slow to declare more than one filter,
	   because the model will always rebuild itself.If you call this member before setting the
	   new filters the model will invalidate its contents not before you call
	*/
	void beginDeclareFilter();
	/*!
	   Description:
	   Function that stops the filter declaration and invalidates the filter.
	*/
	void endDeclareFilter();

	/*!
	   Description:
	   Function to set/add a new filter to the model.
	   Always use QVariant to set a value, don't use QVariant(QRegExp("\\d*")) but instead use QVariant("\\d*").
	   Qt::MatchExactly = QVariant based matching
	   All other match flags are QString based matching
	*/
	void setFilter(const int column, const QVariant &value, const int role = Qt::DisplayRole, Qt::MatchFlags flags = Qt::MatchContains);
	/*!
	   Description:
	   Function to set the filter value.
	   Always use QVariant to set a value, don't use QVariant(QRegExp("\\d*")) but instead use QVariant("\\d*").
	*/
	void setFilterValue(const int column, const QVariant &value);
	/*!
	   Description:
	   Function to filter role.
	*/
	void setFilterRole(const int column, const int role = Qt::DisplayRole);
	/*!
	   Description:
	   Function to filter flags.
	   Qt::MatchExactly = QVariant based matching
	   All other match flags are QString based matching
	*/
	void setFilterFlags(const int column, const Qt::MatchFlags flags = Qt::MatchContains);
	/*!
	   Description:
	   Function to remove a filter from the given column.
	*/
	void removeFilter(const int column);

	/*!
	   Description:
	   Function that returns the filter value of the given column.
	   Note: if the column is not filtered it will return a null variant.
	*/
	QVariant filterValue(const int column) const;
	/*!
	   Description:
	   Function that returns the filter role of the given column.
	   Note: if the column is not filtered it will return -1.
	*/
	int filterRole(const int column) const;
	/*!
	   Description:
	   Function that returns the filter flags of the given column.
	   Note: if the column is not filtered it will return the default value.
	*/
	Qt::MatchFlags filterFlags(const int column) const;

	/*!
	   Description:
	   Function that returns true if the given column is being filtered.
	*/
	bool isFiltered(const int column);

protected:
	virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;

private:
	QMap<int, XrayProxyModelFilter> filters;
	bool m_declaringFilter;
	QModelIndex m_rootIndex;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQThread_h__