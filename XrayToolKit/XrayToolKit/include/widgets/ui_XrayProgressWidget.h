/********************************************************************************
** Form generated from reading UI file 'XrayProgressWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_XRAYPROGRESSWIDGET_H
#define UI_XRAYPROGRESSWIDGET_H


#include <QVariant>
#include <QApplication>
#include <QGridLayout>
#include <QProgressBar>
#include <QWidget>

QT_BEGIN_NAMESPACE

class Ui_XrayProgressWidget
{
public:
    QGridLayout *gridLayout;
    QProgressBar *progressBar;
    rqt_multiplot::StatusWidget *widgetStatus;

    void setupUi(QWidget *XrayProgressWidget)
    {
        if (XrayProgressWidget->objectName().isEmpty())
            XrayProgressWidget->setObjectName(QString::fromUtf8("XrayProgressWidget"));
        XrayProgressWidget->resize(706, 23);
        gridLayout = new QGridLayout(XrayProgressWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setHorizontalSpacing(5);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        progressBar = new QProgressBar(XrayProgressWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setEnabled(true);
        progressBar->setValue(0);
        progressBar->setTextVisible(false);
        progressBar->setInvertedAppearance(false);

        gridLayout->addWidget(progressBar, 0, 0, 1, 1);

        widgetStatus = new rqt_multiplot::StatusWidget(XrayProgressWidget);
        widgetStatus->setObjectName(QString::fromUtf8("widgetStatus"));
        widgetStatus->setMinimumSize(QSize(22, 22));
        widgetStatus->setMaximumSize(QSize(22, 22));

        gridLayout->addWidget(widgetStatus, 0, 1, 1, 1);


        retranslateUi(XrayProgressWidget);

        QMetaObject::connectSlotsByName(XrayProgressWidget);
    } // setupUi

    void retranslateUi(QWidget *XrayProgressWidget)
    {
        XrayProgressWidget->setWindowTitle(QApplication::translate("XrayProgressWidget", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class XrayProgressWidget: public Ui_XrayProgressWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_XRAYPROGRESSWIDGET_H
