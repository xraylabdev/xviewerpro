/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XrayFramelessWindow.h
** file base:	XrayFramelessWindow
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a frame less widget.
****************************************************************************/

#pragma once
#ifndef XrayFramelessWindow_h__
#define XrayFramelessWindow_h__

#include "XrayQtApiExport.h"
#include "XrayFramelessWindowDragger.h"

#include <QWidget>
#include <QToolButton>
#include <QLabel>
#include <QStackedWidget>

namespace Ui
{
	class XrayFramelessWindow;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayFramelessWindow : public QWidget
{
	Q_OBJECT

public:
	explicit XrayFramelessWindow(QWidget* parent = nullptr);
	virtual ~XrayFramelessWindow();
	void saveSettings();
	void restoreSettings();
	void clearSettings();

	/*!
		Description:
		Function to set the current index to the stacked widget.
	*/
	void setCurrentIndex(int index);
	/*!
		Description:
		Function to add the given widget to the stacked widget.
	*/
	void addWidget(QWidget *w);
	/*!
		Description:
		Function to remove the given widget from the stacked widget.
	*/
	void removeWidget(QWidget *w);

	QToolButton* maximumButton() const;
	QToolButton* minimumButton() const;
	QToolButton* menuButton() const;
	QLabel* titleLabel() const;
	XrayFramelessWindowDragger* titleBar() const;

private:
	bool leftBorderHit(const QPoint& pos);
	bool rightBorderHit(const QPoint& pos);
	bool topBorderHit(const QPoint& pos);
	bool bottomBorderHit(const QPoint& pos);
	void styleWindow(bool bActive, bool bNoState);

public slots:
	void setWindowTitle(const QString& text);
	void setWindowTitleIcon(const QIcon& ico, const QSize& size);
	void setWindowIcon(const QIcon& ico);
	void on_closeButton_clicked();

protected slots:
	void on_applicationStateChanged(Qt::ApplicationState state);
	void on_minimizeButton_clicked();
	void on_restoreButton_clicked();
	void on_maximizeButton_clicked();
	void on_windowTitlebar_doubleClicked();

protected:
	virtual void changeEvent(QEvent* _event) override;
	virtual void mousePressEvent(QMouseEvent* _event) override;
	virtual void mouseReleaseEvent(QMouseEvent* _event) override;
	virtual bool eventFilter(QObject* _obj, QEvent* _event) override;
	virtual void mouseDoubleClickEvent(QMouseEvent* _event) override;
	virtual void checkBorderDragging(QMouseEvent* _event);

protected:
	Ui::XrayFramelessWindow *ui;

private:
	QRect m_start_geometry;
	const quint8 CONST_DRAG_BORDER_SIZE = 15;
	bool m_mouse_pressed;
	bool m_drag_top;
	bool m_drag_left;
	bool m_drag_right;
	bool m_drag_bottom;
	bool m_clear_settings;
};

XRAYLAB_END_NAMESPACE

#endif // XrayFramelessWindow_h__