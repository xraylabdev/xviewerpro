/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayButtonGroupBox.h
** file base:	XrayButtonGroupBox
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides list model based group box for buttons.
****************************************************************************/

#pragma once
#ifndef XrayButtonGroupBox_h__
#define XrayButtonGroupBox_h__

#include "XrayQtApiExport.h"
#include "XrayAbstractButtonListModel.h"

#include <QGroupBox>
#include <QVariant>
#include <QAbstractButton>
#include <QButtonGroup>
#include <QBoxLayout>
#include <QCheckBox>
#include <QRadioButton>
#include <QStyle>
#include <QScopedPointer>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayButtonGroupBox : public QGroupBox
{
	Q_OBJECT

	Q_PROPERTY(Qt::Orientation orientation READ orientation WRITE setOrientation)
	Q_PROPERTY(XrayButtonGroupBox::Type type READ type WRITE setType)
	Q_PROPERTY(QSize iconSize READ iconSize WRITE setIconSize)
	Q_PROPERTY(bool exclusive READ exclusive WRITE setExclusive)
	Q_PROPERTY(int checkedIndex READ checkedIndex)
	Q_PROPERTY(int checkedId READ checkedId)
	Q_PROPERTY(bool isEmpty READ isEmpty)
	Q_PROPERTY(int count READ count)
	Q_PROPERTY(QStringList items READ items WRITE setItems)

public:
	enum Type 
	{
		CheckBox,
		RadioBox
	};
	Q_ENUM(Type)

	using Button = QMap<int, QVariant>;
	using Buttons = QVector<Button>;

	explicit XrayButtonGroupBox(QWidget *parent = nullptr);

	Qt::Orientation orientation() const;
	void setOrientation(Qt::Orientation orientation);

	XrayButtonGroupBox::Type type() const;
	void setType(XrayButtonGroupBox::Type type);

	QSize iconSize() const;
	void setIconSize(const QSize& _iconSize);

	bool exclusive() const;
	void setExclusive(bool exclusive);

	void insertButton(int index, const Button &button);
	void addButton(const Button &button);
	void removeButton(int index);

	QVariant buttonData(int index, int role) const;
	void setButtonData(int index, int role, const QVariant &data);

	void clear();
	int count() const;
	bool isEmpty() const;

	int checkedIndex() const;
	int checkedId() const;

	QStringList items() const;
	void setItems(const QStringList &items);

	template <typename T>
	T buttonData(int index, int role) const 
	{
		buttonData(index, role).value<T>();
	}

	template <typename T>
	void setButtonData(int index, int role, const T &data) 
	{
		setButtonData(index, role, QVariant::fromValue(data));
	}

signals:
	void buttonClicked(int index, int id);
	void buttonPressed(int index, int id);
	void buttonReleased(int index, int id);
	void buttonToggled(int index, int id, bool checked);

protected:
	void resetButtons_();
	void createButton_(int index);
	void updateButton_(int index, const QVector<int> &roles = {});
	void removeButton_(int index);
	virtual QAbstractButton *createButton() const;

	QBoxLayout *layout;
	QButtonGroup *group;
	XrayAbstractButtonListModel *model;
	XrayButtonGroupBox::Type m_type;
	QSize m_iconSize;
};

XRAYLAB_END_NAMESPACE

#endif // XrayButtonGroupBox_h__
