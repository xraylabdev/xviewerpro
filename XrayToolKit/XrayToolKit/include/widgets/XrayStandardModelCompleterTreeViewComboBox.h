/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/19/07
** filename: 	XrayStandardModelCompleterTreeViewComboBox.cpp
** file base:	XrayStandardModelCompleterTreeViewComboBox
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a table view.
****************************************************************************/

#pragma once
#ifndef XrayStandardModelCompleterTreeViewComboBox_h__
#define XrayStandardModelCompleterTreeViewComboBox_h__

#include "XrayQtApiExport.h"

#include <QComboBox>
#include <QCompleter>
#include <QStringListModel>

XRAYLAB_BEGIN_NAMESPACE

class XrayStandardModelTreeView;

class XRAY_QT_API_EXPORT XrayStandardModelCompleterTreeViewComboBox : public QComboBox
{
	Q_OBJECT

public:
	XrayStandardModelCompleterTreeViewComboBox(int menuAction, QWidget *p_parent = nullptr);

	void showPopup() override;
	void hidePopup() override;

	void selectIndex(const QModelIndex &index);

	/*!
	   Description:
	   Function that returns a pointer to tree view.
	*/
	auto treeView() const { return p_view; }

private:
	QCompleter* p_completer;
	QStringListModel* p_completerModel;
	XrayStandardModelTreeView* p_view;
};

XRAYLAB_END_NAMESPACE

#endif // XrayStandardModelCompleterTreeViewComboBox_h__
