/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/17/07
** filename: 	XrayStandardModelTreeView.cpp
** file base:	XrayStandardModelTreeView
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a table view.
****************************************************************************/

#pragma once
#ifndef XrayStandardModelTreeView_h__
#define XrayStandardModelTreeView_h__

#include "XrayQtApiExport.h"

#include <QTreeView>
#include <QStandardItemModel>
#include <QMenu>
#include <QKeyEvent>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT TreeViewSortSourceModel : public QStandardItemModel
{
public:
	TreeViewSortSourceModel(QWidget* parent = nullptr) : QStandardItemModel(parent) {}

	//Qt::ItemFlags flags(const QModelIndex& index) const
	//{
	//	if (!index.isValid())
	//		return Qt::NoItemFlags;

	//	//if (index.data(Qt::AccessibleDescriptionRole).toString() == "parent")
	//	//{
	//	//	return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDropEnabled;
	//	//}
	//	//else if (index.data(Qt::AccessibleDescriptionRole).toString() == "child")
	//	//{
	//	//	return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled;
	//	//}

	//	return Qt::NoItemFlags;
	//}

	bool move(int from, bool isUp)
	{
		// range checking etc.
		const auto numRows = rowCount();

		if (from < 0 || from >= numRows || (from == 0 && isUp) || (from == numRows - 1 && !isUp))
			return false;

		const auto to = isUp ? from - 1 : from + 1;

		// See documentation http://doc.qt.io/qt-5/qabstractitemmodel.html#beginMoveRows on why this is needed?
		const auto toSignal = isUp ? to : to + 1;

		if (!beginMoveRows(QModelIndex(), from, from, QModelIndex(), toSignal))
			return false;

		blockSignals(true);
		auto fromRow = takeRow(from); //this also removes it from the model!

		// add this 
		beginInsertRows(QModelIndex(), toSignal, toSignal);

		insertRow(to, fromRow); // but the actual insertion must be done using to, not toSignal!
		blockSignals(false);

		endMoveRows();

		return true;
	}

	QStringList findIndicesTexts(bool includeParents = true) const
	{
		QStringList list;
		list.reserve(rowCount());

		for (auto i = 0; i < rowCount(); i++)
		{
			auto it = item(i);
			if (it)
			{
				QStringList childs;
				childs.reserve(it->rowCount());

				for (auto j = 0; j < it->rowCount(); j++)
				{
					auto ch = it->child(j);
					if (ch)
					{
						childs.append(ch->text());
					}
				}

				if (includeParents)
					list.append(it->text());
				list.append(childs);
			}
		}

		return list;
	}
};

class XRAY_QT_API_EXPORT XrayStandardModelTreeView : public QTreeView
{
	Q_OBJECT

public:
	enum MenuAction
	{
		None = 1,
		SelectAll = 2,
		InvertSelection = 4,
		Sort = 8,
		MoveTo = 16,
		AddParent = 32,
		ChangeText = 64,
		Copy = 128,
		RemoveSelected = 256,
		ExpandAll = 512,
		CollapseAll = 1024,
		OpenInExplorer = 2048
	};

	XrayStandardModelTreeView(int menuAction, QWidget* parent = nullptr);

	/*!
	   Description:
	   Function to set a flag for custom menu actions.
	   Set setContextMenuPolicy(Qt::NoContextMenu) in order to disable custom menu;
	   @param - _actions can be set like (MenuActions::SelectAll | MenuActions::InvertSelection | MenuActions::RemoveSelected).
	*/
	void setCustomMenuAction(MenuAction action) { m_menuAction = action; }
	/*!
	   Description:
	   Function to get a flag for custom menu actions.
	   @returns - actions flag.
	*/
	MenuAction getCustomMenuAction() const { return m_menuAction; }

	bool isParent(const QModelIndex& Index);
	bool isChild(const QModelIndex& Index);

	QStandardItem* addParent(const QString& text, bool bold = true, bool italic = false);
	QStandardItem* addChild(QStandardItem* parent, const QString& text, const QVariant& data, bool checkable = true);
	QStandardItem* addChild(const QString& parentText, const QString& text, const QVariant& data, bool checkable);
	QStandardItem* childItemByData(const QVariant& data);
	void setChildData(const QVariant& data, const QVariant& newData);
	void setChildDataMatchFileName(const QString& newData);

	void setParentFlags(Qt::ItemFlags flags);
	void setChildFlags(Qt::ItemFlags flags);

	QList<QStandardItem*> parentItems() const;
	QList<QStandardItem*> childItems(QStandardItem* parent) const;
	bool hasDuplicateChildItem() const;
	QStringList childsDataText() const;
	QStringList childsDataText(QStandardItem* parent) const;

	bool removeParent(const QString& text, int column = 0);
	void removeChilds(const QString& text, bool removeAllSimilar);
	void removeChild(const QString& text);
	void removeChilds(QStandardItem* parent, const QString& text, bool removeAllSimilar);
	void removeChild(QStandardItem* parent, const QString& text);
	void removeSelected();

	void sortChildren(int column, Qt::SortOrder order = Qt::AscendingOrder);
	void setCheckState(Qt::CheckState state);

	void setCurrentChildIndex(const QString& text);

	void setTreeData(const QVector<QPair<QString, QVector<QPair<QString, QString> > > >& parentAndChildItems);
	QVector<QPair<QString, QVector<QPair<QString, QString> > > >& treeData();
	void updateTreeData();
	void buildFromTreeData();


	void moveChildsTo(const QString& groupName);

	void moveCurrentRow(bool isUp);

	void setSourceModel(TreeViewSortSourceModel* model);
	TreeViewSortSourceModel* sourceModel() const;

	void setShowWarningMessageBoxOnRemove(bool b);
	bool showWarningMessageBoxOnRemove() const;

signals:
	/*!
	   Description:
	   This signal is being triggered when a custom-menu-action MenuAction::Copy is triggered.
	*/
	void copy(const QString& text);
	void removedItem(const QString& text);
	void itemTextChanged(QStandardItem* item);
	void activatedItem(const QModelIndex& index);
	void itemsSorted(int column, Qt::SortOrder order);
	void itemsMoved();

public slots:
	/*!
	   Description:
	   Function that shows the context menu of the clicked item.
	*/
	void showContextMenu(const QPoint& point);
	void moveUp();
	void moveDown();

	// if required connect with:
	// connect(this, &QTreeView::clicked, this, &XrayStandardModelTreeView::checkableClicked);
	void checkableClicked(const QModelIndex& index);

protected:
	/*!
	   Description:
	   Function that creates a menu and assign actions to menu items.
	*/
	virtual void createCustomMenu();

	void keyPressEvent(QKeyEvent* event) override;
	void dropEvent(QDropEvent* event) override;

	void updateMoveToMenu();

	QMenu* p_contextMenu;
	QPoint m_currentPoint;
	MenuAction m_menuAction;
	bool m_changeOnlyParentText;
	QMenu* p_groupMenu;

	TreeViewSortSourceModel* p_sourceModel;

	QVector<QPair<QString, QVector<QPair<QString, QString> > > > m_treeStruct;

	Qt::ItemFlags m_parentFlags;
	Qt::ItemFlags m_childFlags;
	bool m_showWarningMsgBoxOnRemove;
};

XRAYLAB_END_NAMESPACE

#endif // XrayStandardModelTreeView_h__
