#pragma once
#ifndef XrayAboutDialog_h__
#define XrayAboutDialog_h__

/*********************************************************************************
created:	2018/07/09   03:37AM
filename: 	XrayAboutDialog.h
file base:	XrayAboutDialog
file ext:	h
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class that creates a about dialog.

/**********************************************************************************
*	Fast Visualization Kit (FVK)
*	Copyright (C) 2017 REAL3D
*
* This file and its content is protected by a software license.
* You should have received a copy of this license with this file.
* If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "XrayQtApiExport.h"

#include <QDialog>

namespace Ui 
{
	class XrayAboutDialog;
}

XRAYLAB_BEGIN_NAMESPACE

class XrayAboutDialog : public QDialog
{
	Q_OBJECT

public:
	explicit XrayAboutDialog(QWidget* _parent = nullptr);
	~XrayAboutDialog();

	void setImage(const QPixmap& _image);
	void setTitle(const QString& _text);
	void setVersion(const QString& _text);
	void setReleaseDate(const QString& _text);
	void setLicenseType(const QString& _text);
	void setModule(const QString& _module);
	void setCopyRight(const QString& _text);
	void setWebsite(const QString& _text);

private:
	Ui::XrayAboutDialog *ui;
};

XRAYLAB_END_NAMESPACE

#endif // XrayAboutDialog_h__