/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayProgressBarWidget.h
** file base:	XrayProgressBarWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides progress bar widget to be used in the 
** QStatusBar of the application to show progress for time consuming tasks in
** the application.

** 1. It adds support for an abort button that can be enabled to allow
** aborting for interruptible processing while progress in active.
** 2. It does not use QMacStyle on OsX instead uses "fusion" or "cleanlooks"
** show that the text is shown on the progress bar.
** It does not render progress bar grove for a more "flat" look and avoid
** dramatic UI change when toggling between showing progress and not.

Usage:
QWidget m;
XrayProgressBarWidget w(&m);
w.enableProgress(true);
w.setBusyText("Busy");
w.setReadyText("Ready");
w.setFixedWidth(400);
m.show();
QTimer t;
t.setInterval(100);
QObject::connect(&t, &QTimer::timeout, [&]()
{
	static int i = 0;
	w.setProgress("Message %", i++);
});
t.start();
****************************************************************************/

#pragma once
#ifndef XrayProgressBarWidget_h__
#define XrayProgressBarWidget_h__

#include "XrayQtApiExport.h"

#include <QScopedPointer>
#include <QWidget>
#include <QToolButton>

XRAYLAB_BEGIN_NAMESPACE

class XrayProgressBarWidgetLabel;

/**
 * @class XrayProgressBarWidget
 * @brief widget to show progress in a QStatusBar.
 *
 * XrayProgressBarWidget is a widget designed to be used in the QStatusBar of the
 * application to show progress for time consuming tasks in the application.
 *
 * XrayProgressBarWidget is a replacement for QProgressBar. It has the following
 * differences with QProgressBar.
 *
 * \li 1. It adds support for an abort button that can be enabled to allow
 *        aborting for interruptible processing while progress in active.
 * \li 2. It does not use QMacStyle on OsX instead uses "fusion" or "cleanlooks"
 *        show that the text is shown on the progress bar.
 * \li 3. It does not render progress bar grove for a more "flat" look and avoid
 *        dramatic UI change when toggling between showing progress and not.
 */
class XRAY_QT_API_EXPORT XrayProgressBarWidget : public QWidget
{
	Q_OBJECT;
	typedef QWidget Superclass;
	Q_PROPERTY(QString readyText READ readyText WRITE setReadyText)
		Q_PROPERTY(QString busyText READ busyText WRITE setBusyText)

public:
	XrayProgressBarWidget(QWidget* parent = 0);
	~XrayProgressBarWidget() override;

	/*!
		Description:
		Function that returns a pointer to the abort button.
	*/
	auto abortButton() const { return p_abortButton; }

	/**
	 * Set the text to use by default when the progress bar is not enabled
	 * which typically corresponds to application not being busy.
	 * Default value is empty.
	 */
	void setReadyText(const QString&);
	const auto& readyText() const { return m_readyText; }

	/**
	 * Set the text to use by default when the progress bar is enabled
	 * which typically corresponds to application being busy.
	 * Default value is "Busy".
	 */
	void setBusyText(const QString&);
	const auto& busyText() const { return m_busyText; }

public slots:
	/**
	 * Set the progress. Progress must be enabled by calling 'enableProgress`
	 * otherwise this method will have no effect.
	 */
	void setProgress(const QString& message, int value);

	/**
	 * Enabled/disable the progress. This is different from
	 * enabling/disabling the widget itself. This shows/hides
	 * the progress part of the widget.
	 */
	void enableProgress(bool enabled);

	/**
	 * Enable/disable the abort button.
	 */
	void enableAbort(bool enabled);

signals:
	/**
	 * triggered with the abort button is pressed.
	 */
	void abortPressed();

protected:
	XrayProgressBarWidgetLabel* p_progressLabel;
	QToolButton* p_abortButton;

	/**
	 * request to paint the widgets.
	 */
	void updateUI();

private:
	Q_DISABLE_COPY(XrayProgressBarWidget);
	QString m_readyText;
	QString m_busyText;
};

XRAYLAB_END_NAMESPACE

#endif // XrayProgressBarWidget_h__