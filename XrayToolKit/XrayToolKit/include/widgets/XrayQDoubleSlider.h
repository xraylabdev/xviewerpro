/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/08
** filename: 	XrayQDoubleSlider.h
** file base:	XrayQDoubleSlider
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a double value slider.
****************************************************************************/

#pragma once
#ifndef XrayQDoubleSlider_h__
#define XrayQDoubleSlider_h__

#include "XrayQtApiExport.h"

#include <QSlider>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQDoubleSlider : public QSlider
{
	Q_OBJECT

public:
	explicit XrayQDoubleSlider(QWidget *parent = 0);

	double doubleMinimum()
	{
		return m_DoubleMin;
	}

	double doubleMaximum()
	{
		return m_DoubleMax;
	}

	double doubleSingleStep()
	{
		return m_DoubleStep;
	}

	void setDoubleMinimum(double value)
	{
		m_DoubleMin = value;
		updateRange();
	}

	void setDoubleMaximum(double value)
	{
		m_DoubleMax = value;
		updateRange();
	}

	void setDoubleSingleStep(double value)
	{
		m_DoubleStep = value;
		updateRange();
	}

	double doubleValue();

signals:

public slots:
	void setDoubleValue(double _value);

private:
	double m_DoubleMin;
	double m_DoubleMax;
	double m_DoubleStep;
	double m_DoubleValue;

	int m_CorrespondingIntValue;

	void updateRange();

	void updateStep()
	{
		QSlider::setSingleStep(static_cast<int>(1000 * m_DoubleStep / (m_DoubleMax - m_DoubleMin)));
	}
};

XRAYLAB_END_NAMESPACE

#endif // XrayQDoubleSlider_h__