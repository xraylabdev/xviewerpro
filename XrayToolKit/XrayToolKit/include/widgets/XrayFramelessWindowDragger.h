/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XrayFramelessWindowDragger.h
** file base:	XrayFramelessWindowDragger
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a mouse dragger for a widget.
****************************************************************************/

#pragma once
#ifndef XrayFramelessWindowDragger_h__
#define XrayFramelessWindowDragger_h__

#include "XrayQtApiExport.h"

#include <QMouseEvent>
#include <QWidget>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayFramelessWindowDragger : public QWidget
{
	Q_OBJECT

public:
	explicit XrayFramelessWindowDragger(QWidget* _parent = nullptr);
	virtual ~XrayFramelessWindowDragger() {}

signals:
	void doubleClicked();

protected:
	void mousePressEvent(QMouseEvent* _event) override;
	void mouseMoveEvent(QMouseEvent* _event) override;
	void mouseReleaseEvent(QMouseEvent* _event) override;
	void mouseDoubleClickEvent(QMouseEvent* _event) override;
	void paintEvent(QPaintEvent* _event) override;

	QPoint m_mouse_pos;
	QPoint m_wnd_pos;
	bool m_mouse_pressed;
};

XRAYLAB_END_NAMESPACE

#endif // XrayFramelessWindowDragger_h__