/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/28
** filename: 	XrayQApplication.h
** file base:	XrayQApplication
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a QApplication object with some
				pre-defined attributes.
****************************************************************************/

#pragma once
#ifndef XrayQApplication_h__
#define XrayQApplication_h__

#include "XrayQtApiExport.h"

#include <QApplication>
#include <QTranslator>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQApplication : public QApplication
{
	Q_OBJECT

public:
	/*!
	   Description:
	   Constructor to create a QApp object.
	*/
	XrayQApplication(int &argc, char **argv);
	virtual ~XrayQApplication();

	/*!
	   Description:
	   Function to install the message handler for qt messages.
	*/
	static void installMessageHandler();

	/*!
	   Description:
	   Function that initializes globals attributes.
	*/
	static void setGlobalAttributes();

	/*!
	   Description:
	   Function to bring the main window of this program to front.
	   This function takes applicationName() as to look for the running executable.
	   So, call it after settings the setApplicationName().
	*/
	static void bringToFront();

	/*!
	   Description:
	   Function that helps to run only one instance of this application.
	   If a user wants to run a second instance, this function will bring
	   the first instance to front, instead to start a new instance.
	   Put this function just after setting the QApplication::setApplicationName() and
	   before creating any QWidget object.
	   /param key - a unique key for this application (usually a ProductCode).
	*/
	void startAsSingleton(const QString& _key);

	/*!
	   Description:
	   Function to set a string of build date.
	*/
	static void setApplicationBuildDate(const QString& _date);
	/*!
	   Description:
	   Function to get a string of build date.
	*/
	static QString applicationBuildDate();

	/*!
	   Description:
	   Function that loads the translation files of this api and qt.
	   /param _language - en, de, fr, etc.
	*/
	static void loadLanguage(const QString& _language);
};

XRAYLAB_END_NAMESPACE

#endif // XrayQApplication_h__