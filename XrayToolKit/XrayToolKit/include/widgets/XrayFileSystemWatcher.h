/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayFileSystemWatcher.h
** file base:	XrayFileSystemWatcher
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a file system watcher.
****************************************************************************/

#pragma once
#ifndef XrayFileSystemWatcher_h__
#define XrayFileSystemWatcher_h__

#include "XrayQtApiExport.h"

#include <QFileSystemWatcher>
#include <QObject>
#include <QDir>
#include <QTimer>
#include <QDateTime>
#include <functional>

XRAYLAB_BEGIN_NAMESPACE

/*!
	Description:
	This class can monitor file changes in a directory and calls a callback in response.
*/
class XRAY_QT_API_EXPORT XrayFileSystemWatcher : public QObject
{
	Q_OBJECT

public:
	/*!
	   Description:
	   The callback function to be executed whenever changes are detected.
   */
	XrayFileSystemWatcher(int _timerInterval = 100);
	~XrayFileSystemWatcher();

	/*!
	   Description:
	   The callback function to be executed whenever changes are detected.
   */
	void setCallback(std::function<void()> _callback);

	/*!
	   Description:
	   Function to remove all paths from the watcher.
   */
	void clear();
	/*!
	   Description:
	   Function to remove the specified path from the watcher.
   */
	void remove(const QString& _path);

	/*!
	   Description:
	   Set a directory to watch for changes.
   */
	void setDirectory(const QString& path);
	/*!
	   Description:
	   Function to get the files that are being watched.
	   \return - a list of files.
   */
	QStringList getFiles() const;
	/*!
	   Description:
	   Function to get the files that are being watched.
	   \return - a list of files.
   */
	QStringList getDirectories() const;

	/*!
	   Description:
	   Function to get the last modified date and time of the directory.
	   \return - list of the date and time.
   */
	QList<QDateTime> getModifiedDateTime() const;

	/*!
	   Description:
	   Function to get a reference to internal directory.
	   \return - a reference to QDir.
   */
	QDir& getDir();

public slots:
	/*!
	   Description:
	   Slot connected to the directoryChanged signal from internal QFileSystemWatcher.
	   \param - Path of the directory where the change was detected.
   */
	void directoryChanged(const QString& path);
	/*!
	   Description:
	   Slot connected to the fileChanged signal from internal QFileSystemWatcher.
	   \param - Path of the file where the change was detected.
   */
	void fileChanged(const QString& path);

	/*!
	   Description:
	   Adds the paths of the files to be monitored in the current directory.
   */
	void addPaths();

private:
	std::function<void()> m_callback;
	QFileSystemWatcher m_watcher;
	QDir m_dir;
	QTimer m_timer;
};

XRAYLAB_END_NAMESPACE

#endif // XrayFileSystemWatcher_h__