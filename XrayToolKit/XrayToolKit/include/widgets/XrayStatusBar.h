/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/015
** filename: 	XrayStatusBar.h
** file base:	XrayStatusBar
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that creates a statusbar widget with a timer.
****************************************************************************/

#pragma once
#ifndef XrayStatusBar_h__
#define XrayStatusBar_h__

#include "XrayQtApiExport.h"

#include <QStatusBar>
#include <QLabel>
#include <QTimer>
#include <QString>

XRAYLAB_BEGIN_NAMESPACE

class XrayLabel;

/****************************************************************************/
/*!
	Class that create a wizard for the report generator tool.
*/
class XRAY_QT_API_EXPORT XrayStatusBar : public QStatusBar
{
	Q_OBJECT

public:
	explicit XrayStatusBar(QWidget *parent = nullptr);
	~XrayStatusBar();

public slots:
	void showMessage(const QString& _message, int _timeout = 0);
	void setDefaultMessage(const QString& _message);

protected slots:
	void reallyShowMessage();

private:
	XrayLabel* p_statusLabel; // for a stable (elided) text
	QTimer* p_messageTimer;
	QString m_lastMessage;
	int m_lastTimeOut;
	int m_messageDelay;	// in msec
};

XRAYLAB_END_NAMESPACE

#endif  // XrayStatusBar_h__