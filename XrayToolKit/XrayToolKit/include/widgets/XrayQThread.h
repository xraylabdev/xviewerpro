/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayQThread.h
** file base:	XrayQThread
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a thread with pause and delay support.
****************************************************************************/

#pragma once
#ifndef XrayQThread_h__
#define XrayQThread_h__

#include "XrayQtApiExport.h"

#include <QThread>
#include <QMutex>
#include <QWaitCondition>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQThread : public QThread
{
	Q_OBJECT

public:
	/*!
	   Description:
	   Default constructor that takes a pointer to parent object.
	*/
	XrayQThread(QObject* parent = 0);
	virtual ~XrayQThread();

	/*!
	   Description:
	   Function to set the callback function to be executed in the thread.
	   Warning: setCallback is not thread safe,
	   so do not call this function while the thread is running.
	   The setCallback function is supposed call before starting the thread
	   or after stopping the thread.
	*/
	void setCallback(std::function<void()> _callback);

	/*!
	   Description:
	   Function to set the frame delay in milliseconds.
	   Warning: setDelay and getDelay is not thread safe,
	   so do not call these function while the thread is running.
	   The delay is supposed to set or get before starting the thread
	   or after stopping the thread.
	*/
	void setDelay(unsigned long _delay);
	/*!
	   Description:
	   Function to get the frame delay in milliseconds.
	   Warning: setDelay and getDelay is not thread safe,
	   so do not call these function while the thread is running.
	   The delay is supposed to set or get before starting the thread
	   or after stopping the thread.
	*/
	unsigned long getDelay() const;

	/*!
		Description:
		Function that returns true if the thread is on pause.
	*/
	bool isPause();

public slots:
	/*!
		Description:
		Function to pause the thread.
	*/
	void pause();
	/*!
		Description:
		Function to resume the thread.
	*/
	void resume();
	/*!
		Description:
		Function to stop the thread.
	*/
	void stop();

protected:
	/*!
		Description:
		Virtual function that is expected to be overridden.
	*/
	void run() override;

	bool m_ispause;
	bool m_isstop;
	QMutex m_stopmutex;
	QMutex m_pausemutex;
	QWaitCondition m_pausecond;
	std::function<void()> m_callback;
	unsigned long m_delay;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQThread_h__