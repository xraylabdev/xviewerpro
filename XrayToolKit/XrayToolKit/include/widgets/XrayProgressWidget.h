/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayProgressWidget.h
** file base:	XrayProgressWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides progress widget.

Usage:
QWidget m;
XrayProgressWidget w(&m);
w.setFixedWidth(400);
w.start("Tooltip");
m.show();
QTimer t;
t.setInterval(100);
QObject::connect(&t, &QTimer::timeout, [&]()
{
	static int i = 0;
	w.setCurrentProgress(i++ / 100.0);
});
t.start();
****************************************************************************/

#pragma once
#ifndef XrayProgressWidget_h__
#define XrayProgressWidget_h__

#include "XrayQtApiExport.h"
#include "XrayPixmapStatusWidget.h"

#include <QWidget>
#include <QGridLayout>
#include <QProgressBar>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayProgressWidget : public QWidget
{
	Q_OBJECT

public:
	XrayProgressWidget(QWidget* parent = 0);
	virtual ~XrayProgressWidget();

	double getCurrentProgress() const;
	bool isStarted() const;
	void start(const QString& toolTip);
	void finish(const QString& toolTip);
	void fail(const QString& toolTip);
	void setStatusWidgetVisible(bool _b);
	void setPercentTextVisible(bool _b);

public slots:
	void setCurrentProgress(double progress);
	void onStart();
	void onFinish();
	void onFail();

private:
	QGridLayout* gridLayout;
	QProgressBar* progressBar;
	XrayPixmapStatusWidget* statusWidget;
	bool m_started;
	bool m_percentTextVisible;
};

XRAYLAB_END_NAMESPACE

#endif // XrayProgressWidget_h__