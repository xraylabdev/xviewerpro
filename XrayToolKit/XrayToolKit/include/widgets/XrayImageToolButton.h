/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/11
** filename: 	XrayImageToolButton.h
** file base:	XrayImageToolButton
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a tool button for image and text.
****************************************************************************/

#pragma once
#ifndef XrayImageSlideShowToolBtn_h__
#define XrayImageSlideShowToolBtn_h__

#include "XrayQtApiExport.h"

#include <QToolButton>
#include <QPainter>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayImageToolButton : public QToolButton
{
	Q_OBJECT

public:
	explicit XrayImageToolButton(int _index, const QString& _imgFile, const QString& _imgText, QWidget* _parent = nullptr);
	void setToUnpressed(bool _b);
	void setToPressed(bool _b);
	int getIndex() const { return m_index; }

signals:
	void mouseEnter();
	void mouseLeave();
	void parentSignal(XrayImageToolButton*);

public slots:
	void pressedSlot();

protected:
	void enterEvent(QEvent* _ev) override;
	void leaveEvent(QEvent* _ev) override;
	void paintEvent(QPaintEvent* _ev) override;
	void painterinfo(int _topPartOpacity, int _bottomPartOpacity, QPainter* _painter);

	bool m_hovered;
	bool m_pressed;
	QString m_imgFile;
	QString m_imgText;
	int m_index;
};

XRAYLAB_END_NAMESPACE

#endif // XrayImageSlideShowToolBtn_h__