/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/18
** filename: 	XrayQLed.h
** file base:	XrayQLed
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a led widget.
** Usage:
auto led = new XrayQLed();
led->setFixedSize(22, 22);
led->setOnColor(XrayQLed::ledColor::Green);
led->setShape(XrayQLed::ledShape::Circle);
led->setValue(true);
****************************************************************************/

#pragma once
#ifndef XrayQLed_h__
#define XrayQLed_h__

#include "XrayQtApiExport.h"

#include <QWidget>
#include <QTimer>

class QColor;
class QSvgRenderer;

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQLed : public QWidget
{
	Q_OBJECT

	Q_ENUMS(ledColor)
	Q_ENUMS(ledShape)
	Q_PROPERTY(bool value READ value WRITE setValue);
	Q_PROPERTY(ledColor onColor READ onColor WRITE setOnColor);
	Q_PROPERTY(ledColor offColor READ offColor WRITE setOffColor);
	Q_PROPERTY(ledShape shape READ shape WRITE setShape)

public:
	XrayQLed(QWidget *parent = 0);
	virtual ~XrayQLed();

	bool value() const { return m_value; }
	enum ledColor { Red = 0, Green, Yellow, Grey, Orange, Purple, Blue };
	enum ledShape { Circle = 0, Square, Triangle, Rounded };
	ledColor onColor() const { return m_onColor; }
	ledColor offColor() const { return m_offColor; }
	ledShape shape() const { return m_shape; }

public slots:
	void setValue(bool);
	void setOnColor(ledColor);
	void setOffColor(ledColor);
	void setShape(ledShape);
	void toggleValue();

protected:
	void paintEvent(QPaintEvent *event);

	bool m_value;
	ledColor m_onColor, m_offColor;
	ledShape m_shape;
	QStringList shapes;
	QStringList colors;

private:
	QSvgRenderer* renderer;
};

class XRAY_QT_API_EXPORT XrayQLedBlink : public XrayQLed
{
	Q_OBJECT

public:
	XrayQLedBlink(QWidget *parent = 0);
	virtual ~XrayQLedBlink();

	void start();
	void stop();
	void setInterval(int msec);
	int interval() const;

protected:
	QTimer m_timer;
	int m_index;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQLed_h__