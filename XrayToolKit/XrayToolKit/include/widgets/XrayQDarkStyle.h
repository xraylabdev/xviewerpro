/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XrayQDarkStyle.h
** file base:	XrayQDarkStyle
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a dark style for Qt applications.
****************************************************************************/

#pragma once
#ifndef XrayQDarkStyle_h__
#define XrayQDarkStyle_h__

#include "XrayQtApiExport.h"

#include <QApplication>
#include <QFile>
#include <QFont>
#include <QProxyStyle>
#include <QStyle>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQDarkStyle : public QProxyStyle
{
	Q_OBJECT

public:
	XrayQDarkStyle();
	explicit XrayQDarkStyle(QStyle* _style);

	static QStyle* baseStyle();

	void polish(QPalette& _palette) override;
	void polish(QApplication* _app) override;

private:
	static QStyle* styleBase(QStyle* _style = nullptr);
};

XRAYLAB_END_NAMESPACE

#endif  // XrayQDarkStyle_h__