/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayResizeDialog.h
** file base:	XrayResizeDialog
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a dialog to get the new size from a user.
****************************************************************************/

#pragma once
#ifndef XrayResizeDialog_h__
#define XrayResizeDialog_h__

#include "XrayQtApiExport.h"

#include <QDialog>
#include <QLayout>
#include <QPushButton>
#include <QSpinBox>
#include <QCheckBox>
#include <QLabel>
#include <QSignalMapper>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayResizeDialog : public QDialog
{
	Q_OBJECT

public:
	XrayResizeDialog(QWidget* parent = nullptr, QSize currentSize = QSize(800, 600));

	bool isKeepAspectRatioChecked() const;

	const QSize getNewSize();

private:
	void retranslate();

	QVBoxLayout* mainLayout;
	QGridLayout* gridLayout;
	QHBoxLayout* buttonLayout;
	QPushButton* button_OK;
	QPushButton* button_Cancel;
	QLabel* horizontal_label;
	QLabel* vertical_label;
	QSpinBox* spinbox_width;
	QSpinBox* spinbox_height;
	QCheckBox* checkbox_aspect;
	QSignalMapper* mapper;
	QSize newSize;
};

XRAYLAB_END_NAMESPACE

#endif // XrayResizeDialog_h__