/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayUpDownButton.h
** file base:	XrayUpDownButton
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides up and down buttons.
****************************************************************************/

#pragma once
#ifndef XrayUpDownButton_h__
#define XrayUpDownButton_h__

#include "XrayQtApiExport.h"

#include <QWidget>
#include <QToolButton>
#include <QBoxLayout>
#include <QScopedPointer>

XRAYLAB_BEGIN_NAMESPACE

class XrayUpDownButton;

class XRAY_QT_API_EXPORT XrayUpDownButtonP : public QObject
{
	Q_OBJECT

public:
	XrayUpDownButtonP(XrayUpDownButton* _widget, Qt::Orientation _orientation);
	Qt::Orientation directionToOrientation(QBoxLayout::Direction _direction) const;
	QBoxLayout::Direction orientationToDirection(Qt::Orientation _orientation) const;
	void updateButtonArrows();

	XrayUpDownButton *widget;
	QBoxLayout *layout;
	QToolButton *upButton;
	QToolButton *downButton;
	bool autoRaise;
};

class XRAY_QT_API_EXPORT XrayUpDownButton : public QWidget
{
	Q_OBJECT

	friend class XrayUpDownButtonP;
	QScopedPointer<XrayUpDownButtonP> d;

	Q_PROPERTY(Qt::Orientation orientation READ orientation WRITE setOrientation)
	Q_PROPERTY(int spacing READ spacing WRITE setSpacing)
	Q_PROPERTY(bool autoRaise READ autoRaise WRITE setAutoRaise)
	Q_PROPERTY(QToolButton::ToolButtonPopupMode popupMode READ popupMode WRITE setPopupMode)
	Q_PROPERTY(QKeySequence upShortcut READ upShortcut WRITE setUpShortcut)
	Q_PROPERTY(QKeySequence downShortcut READ downShortcut WRITE setDownShortcut)

public:
	explicit XrayUpDownButton(QWidget *parent = 0);
	explicit XrayUpDownButton(Qt::Orientation orientation, QWidget *parent = 0);

	Qt::Orientation orientation() const;
	void setOrientation(Qt::Orientation orientation);

	int spacing() const;
	void setSpacing(int spacing);

	bool autoRaise() const;
	void setAutoRaise(bool autoRaise);

	QToolButton::ToolButtonPopupMode popupMode() const;
	void setPopupMode(QToolButton::ToolButtonPopupMode mode);

	QKeySequence upShortcut() const;
	void setUpShortcut(const QKeySequence &key);

	QKeySequence downShortcut() const;
	void setDownShortcut(const QKeySequence &key);

	QMenu *upMenu() const;
	void setUpMenu(QMenu *menu);

	QMenu *downMenu() const;
	void setDownMenu(QMenu *menu);

public slots:
	void upAnimateClick(int msec = 100);
	void downAnimateClick(int msec = 100);
	void upClick();
	void downClick();
	void upToggle();
	void downToggle();
	void upShowMenu();
	void downShowMenu();

signals:
	void upClicked(bool checked = false);
	void downClicked(bool checked = false);
	void upPressed();
	void downPressed();
	void upReleased();
	void downReleased();
	void upToggled(bool checked);
	void downToggled(bool checked);
	void upTriggered(QAction *action);
	void downTriggered(QAction *action);
};

XRAYLAB_END_NAMESPACE

#endif // XrayUpDownButton_h__