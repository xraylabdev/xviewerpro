/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayProgressChangeEvent.h
** file base:	XrayProgressChangeEvent
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides progress change event.
****************************************************************************/

#pragma once
#ifndef XrayProgressChangeEvent_h__
#define XrayProgressChangeEvent_h__

#include "XrayQtApiExport.h"

#include <QEvent>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayProgressChangeEvent : public QEvent
{
public:
	static const QEvent::Type Type;

	XrayProgressChangeEvent(double _progress);
	virtual ~XrayProgressChangeEvent();

	double getProgress() const;

private:
	double m_progress;
};

XRAYLAB_END_NAMESPACE

#endif // XrayProgressChangeEvent_h__