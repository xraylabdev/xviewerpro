/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/17/07
** filename: 	XrayStandardModelCompleterTreeViewWidget.cpp
** file base:	XrayStandardModelCompleterTreeViewWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a table view.
****************************************************************************/

#pragma once
#ifndef XrayStandardModelCompleterTreeViewWidget_h__
#define XrayStandardModelCompleterTreeViewWidget_h__

#include "XrayQtApiExport.h"

#include <QWidget>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QCompleter>
#include <QStringListModel>

XRAYLAB_BEGIN_NAMESPACE

class XrayStandardModelTreeView;

class XRAY_QT_API_EXPORT XrayStandardModelCompleterTreeViewWidget : public QWidget
{
	Q_OBJECT

public:
	XrayStandardModelCompleterTreeViewWidget(int menuAction, QWidget* parent = nullptr);

	/*!
	   Description:
	   Function that returns a pointer to tree view.
	*/
	auto treeView() const { return p_view; }

signals:
	void pressed(const QString &index);
	void activatedItem(const QString &index);
	void removedItem(const QString &index);

protected:
	QVBoxLayout* vLayout;
	QLineEdit* p_lineEdit;
	QCompleter* p_completer;
	QStringListModel* p_completerModel;
	XrayStandardModelTreeView* p_view;
};

XRAYLAB_END_NAMESPACE

#endif // XrayStandardModelCompleterTreeViewWidget_h__
