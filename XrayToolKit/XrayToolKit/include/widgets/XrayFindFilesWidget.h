/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/04
** filename: 	XrayFindFilesWidget.h
** file base:	XrayFindFilesWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a widget for searching files that
*				contain the given text.
****************************************************************************/

#pragma once
#ifndef XrayFindFilesWidget_h__
#define XrayFindFilesWidget_h__

#include "XrayQtApiExport.h"

#include <QWidget>
#include <QDir>

QT_BEGIN_NAMESPACE
class QComboBox;
class QLabel;
class QPushButton;
class QTableWidget;
class QTableWidgetItem;
class QSettings;
QT_END_NAMESPACE

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayFindFilesWidget : public QWidget
{
	Q_OBJECT

public:
	/*!
		Description:
		Default constructor takes the parent of the widget.
	*/
	explicit XrayFindFilesWidget(QWidget *parent = 0);
	/*!
		Description:
		Destructor that saves the settings and destroys the widget.
	*/
	~XrayFindFilesWidget();

	/*!
		Description:
		Function that writes settings to the given settings.
	*/
	void writeSettings(QSettings& _settings);
	/*!
		Description:
		Function that reads settings from the given settings and update the widget.
	*/
	void readSettings(QSettings& _settings);

	/*!
		Description:
		Static function that returns the file name from the given table item.
	*/
	static QString getFilename(const QTableWidgetItem* item);

	/*!
		Description:
		Function that sets the new file types items to comboBox.
		It clears all the previous items first.
	*/
	void setFileTypes(const QStringList& _fileTypes);
	QStringList getFileTypes() const;

	/*!
		Description:
		Function that sets the new texts items to comboBox.
		It clears all the previous items first.
	*/
	void setTexts(const QStringList& _texts);
	QStringList getTexts() const;

	/*!
		Description:
		Function that sets the new directories items to comboBox.
		It clears all the previous items first.
	*/
	void setDirectories(const QStringList& _dir);
	QStringList getDirectories() const;

	/*!
		Description:
		Function that shows a text message at the bottom of the widget.
	*/
	void setMessageText(const QString& _text);

	/*!
		Description:
		Function that returns a pointer to table widget.
	*/
	QTableWidget* getTableWidget() const;

signals:
	void tableCellActivated(int row, int column);
	void firstColumnActivated(const QString& _filePath);

private slots:
	void browse();
	void find();
	void animateFindClick();
	void openFileOfItem(int row, int column);
	void contextMenu(const QPoint& pos);

private:
	QStringList findFiles(const QStringList& files, const QString& text);
	void showFiles(const QStringList& paths);
	QComboBox *createComboBox(const QStringList& _items);
	void createFilesTable();

	QPushButton* browseBtn;
	QComboBox* fileComboBox;
	QComboBox* textComboBox;
	QComboBox* directoryComboBox;
	QLabel* filesFoundLabel;
	QPushButton* findButton;
	QTableWidget* filesTable;
	QLabel* fileTypeLabel;
	QLabel* tagLabel;
	QLabel* lookInLabel;
	QAction* copyNameAction;
	QAction* openAction;

	QDir currentDir;
};

XRAYLAB_END_NAMESPACE

#endif // XrayFindFilesWidget_h__