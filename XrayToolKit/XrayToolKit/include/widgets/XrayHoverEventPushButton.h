/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/14
** filename: 	XrayHoverEventPushButton.h
** file base:	XrayHoverEventPushButton
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a push button that emits signals on
				mouse hover events.
****************************************************************************/

#pragma once
#ifndef XrayImageSlideShowPushBtn_h__
#define XrayImageSlideShowPushBtn_h__

#include "XrayQtApiExport.h"

#include <QPushButton>
#include <QHoverEvent>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayHoverEventPushButton : public QPushButton
{
	Q_OBJECT

public:
	explicit XrayHoverEventPushButton(const QIcon& _icon, const QString& _text, QWidget* _parent = nullptr);

signals:
	void mouseEnter();
	void mouseLeave();
	void mouseHoverEnter(QEvent* _e);
	void mouseHoverLeave(QEvent* _e);
	void mouseHoverMove(QEvent* _e);

protected:
	void enterEvent(QEvent* _e) override;
	void leaveEvent(QEvent* _e) override;
	void hoverEnter(QHoverEvent* _e);
	void hoverLeave(QHoverEvent* _e);
	void hoverMove(QHoverEvent* _e);
	bool event(QEvent* _e) override;
};

XRAYLAB_END_NAMESPACE

#endif // XrayImageSlideShowPushBtn_h__