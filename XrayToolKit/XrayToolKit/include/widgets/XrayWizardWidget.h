/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/015
** filename: 	XrayWizardWidget.h
** file base:	XrayWizardWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that creates a wizard of the added pages.
****************************************************************************/

#pragma once
#ifndef XrayWizardWidget_h__
#define XrayWizardWidget_h__

#include "XrayQtApiExport.h"

#include <QWizard>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayWizardWidget : public QWizard
{
	Q_OBJECT

public:
	explicit XrayWizardWidget(QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());

};

XRAYLAB_END_NAMESPACE

#endif  // XrayWizardWidget_h__