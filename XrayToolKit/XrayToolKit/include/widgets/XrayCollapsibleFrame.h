/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayCollapsibleFrame.h
** file base:	XrayCollapsibleFrame
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a collapsible frame widget.
****************************************************************************/

#pragma once
#ifndef XrayCollapsibleFrame_h__
#define XrayCollapsibleFrame_h__

#include "XrayQtApiExport.h"

#include <QWidget>
#include <QFrame>
#include <QIcon>
#include <QPointer>
#include <QStyle>
#include <QTimer>
#include <QToolButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPainter>

XRAYLAB_BEGIN_NAMESPACE

class XrayCollapsibleFrame;

class XRAY_QT_API_EXPORT XrayCollapsibleToolButton : public QToolButton
{
public:
	using QToolButton::QToolButton;

protected:
	void paintEvent(QPaintEvent *event) override;
};

class XRAY_QT_API_EXPORT XrayCollapsibleFrameP : public QObject
{
	Q_OBJECT

public:
	XrayCollapsibleFrameP(XrayCollapsibleFrame *_editor);

public slots:
	void button_toggled(bool toggled);
	void storeSizes(QWidget *widget);
	void restoreSize(QWidget *widget);

public:
	XrayCollapsibleFrame *editor;
	QHBoxLayout *horizontalLayout;
	QVBoxLayout *verticalLayout;
	QToolButton *button;
	QPointer<QWidget> title;
	QPointer<QWidget> widget;
	QIcon collapsedIcon;
	QIcon expandedIcon;
	bool collapsed;

	enum RestoreSizeBehavior { NoRestoreSizeBehavior, RestoreWindowSizeBehavior };
	RestoreSizeBehavior resizeBehavior = RestoreWindowSizeBehavior;
	QSize oldCollapsedSize;
	QSize oldExpandedSize;
};

class XRAY_QT_API_EXPORT XrayCollapsibleFrame : public QFrame
{
	Q_OBJECT

	Q_PROPERTY(QWidget *title READ title WRITE setTitle NOTIFY titleChanged)
	Q_PROPERTY(QWidget *widget READ widget WRITE setWidget NOTIFY widgetChanged)

public:

	explicit XrayCollapsibleFrame(QWidget *parent = nullptr);

	QSize minimumSizeHint() const override;

	XrayCollapsibleFrameP::RestoreSizeBehavior restoreSizeBehavior() const;
	void setRestoreSizeBehavior(XrayCollapsibleFrameP::RestoreSizeBehavior behavior);

	QWidget *title() const;
	QWidget *takeTitle();
	void setTitle(QWidget *title);

	QWidget *widget() const;
	QWidget *takeWidget();
	void setWidget(QWidget *widget);

	QString collapseToolTip() const;
	QIcon collapsedIcon() const;
	QIcon expandedIcon() const;

	bool isCollapsed() const;
	bool isExpanded() const;
	int indentation() const;

public slots:
	void setCollapseToolTip(const QString &toolTip);
	void setCollapsedIcon(const QIcon &icon);
	void setExpandedIcon(const QIcon &icon);
	void setIcon(const QIcon &icon);
	void setCollapsed(bool collapsed);
	void setExpanded(bool expanded);
	void setIndentation(int indentation);

signals:
	void titleChanged();
	void widgetChanged();
	void aboutToCollapse();
	void collapsed();
	void aboutToExpand();
	void expanded();

private:
	friend class XrayCollapsibleFrameP;
	QScopedPointer<XrayCollapsibleFrameP> d;
};

XRAYLAB_END_NAMESPACE

#endif // XrayCollapsibleFrame_h__