/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/18
** filename: 	XrayQTabWidget.h
** file base:	XrayQTabWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a tab widget with actions and animation.
** Usage:
auto tree = new XrayQTabWidget();
****************************************************************************/

#pragma once
#ifndef XrayQTabWidget_h__
#define XrayQTabWidget_h__

#include "XrayQtApiExport.h"

#include <QTabWidget>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQTabWidget : public QTabWidget
{
	Q_OBJECT
	Q_PROPERTY(bool alwaysShowTabBar READ alwaysShowTabBar WRITE setAlwaysShowTabBar)
	Q_PROPERTY(Qt::ContextMenuPolicy tabContextMenuPolicy READ tabContextMenuPolicy WRITE setTabContextMenuPolicy)

public:
	explicit XrayQTabWidget(QWidget* parent = 0);
	virtual ~XrayQTabWidget();

	/*!
		Description:
		whether the tab bar is shown always
		regardless of how many tabs there are.
		The default value of this property is true.
		Set this property to false if you want to show
		the tab bar only in case there are more than one tabs.	
	*/
	void setAlwaysShowTabBar(bool always);
	bool alwaysShowTabBar() const;

	/*!
		Description:
		how the tab specific context menus are handled.
		The default value of this property is Qt::DefaultContextMenu,
		which means that the tabContextMenuEvent() handler is called.
		Other values are Qt::NoContextMenu, Qt::PreventContextMenu
		(since Qt 4.2), Qt::ActionsContextMenu, and Qt::CustomContextMenu.
		With Qt::CustomContextMenu, the signal tabContextMenuRequested() is
		emitted.
	*/
	void setTabContextMenuPolicy(Qt::ContextMenuPolicy policy);
	Qt::ContextMenuPolicy tabContextMenuPolicy() const;

	/*!
		Description:
		Appends the action to the list of actions of the tab at index.
	*/
	void addTabAction(int index, QAction* action);
	/*!
		Description:
		This convenience function creates a new action with text. The function
		adds the newly created action to the list of actions of the tab at index, and returns it.
	*/
	QAction* addTabAction(int index, const QString& text);
	/*!
		Description:
		This convenience function creates a new action with icon and text. The function
		adds the newly created action to the list of actions of the tab at index, and returns it.
	*/
	QAction* addTabAction(int index, const QIcon& icon, const QString& text);
	/*!
		Description:
		This convenience function creates a new action with text and
		an optional shortcut. The action's triggered() signal is
		connected to the receiver's member slot. The function adds
		the newly created action to the list of actions of the tab at
		index, and returns it.
		Note: In order to make it possible for the shortcut to work even
		when the context menu is not open, the action must be added to
		a visible widget. The corresponding tab is a good alternative.

		QWidget* tab = createNewTab();
		tabWidget->addTab(tab, title);
		QAction* action = tabWidget->addTabAction(index, tr("Close"), this, SLOT(closeCurrentTab()), tr("Ctrl+W"));
		tab->addAction(act);
	*/
	QAction* addTabAction(int index, const QString& text, const QObject* receiver, const char* member, const QKeySequence& shortcut = 0);
	/*!
		Description:
		This convenience function creates a new action with icon, text
		and an optional shortcut. The action's triggered() signal is connected
		to the receiver's member slot. The function adds the newly created
		action to the list of actions of the tab at index, and returns it.
	*/
	QAction* addTabAction(int index, const QIcon& icon, const QString& text, const QObject* receiver, const char* member, const QKeySequence& shortcut = 0);
	/*!
		Description:
		Appends the actions to the list of actions of the tab at index.
	*/
	void addTabActions(int index, QList<QAction*> actions);

	/*!
		Description:
		Clears the list of actions of the tab at index.
		Note: Only actions owned by the tab widget are deleted.
	*/
	void clearTabActions(int index);

	/*!
		Description:
		Inserts the action to the list of actions of the
		tab at index, before the action before. It appends
		the action if before is 0.
	*/
	void insertTabAction(int index, QAction* before, QAction* action);
	/*!
		Description:
		Inserts the actions to the list of actions of the
		tab at index, before the action before. It appends
		the action if before is 0.
	*/
	void insertTabActions(int index, QAction* before, QList<QAction*> actions);

	/*!
		Description:
		Removes the action \a action from the list of actions of the tab at index.
		Note: The removed action is not deleted.
	*/
	void removeTabAction(int index, QAction* action);

	/*!
		Description:
		Returns the (possibly empty) list of actions for the tab at index.
	*/
	QList<QAction*> tabActions(int index) const;

	/*!
		Description:
		Returns the animation of the tab at index or 0 if no animation has been set.
	*/
	QMovie* tabAnimation(int index) const;
	/*!
		Description:
		Returns the (possibly empty) list of actions for the tab at index.
	*/
	void setTabAnimation(int index, QMovie* animation, bool start = true);
	/*!
		Description:
		This is an overloaded member function, provided for convenience.
		The QMovie animation is constructed from fileName with this as parent.
		The animation is started if start is true.
	*/
	void setTabAnimation(int index, const QString& fileName, bool start = true);
	/*!
		Description:
		Removes the animation of the tab at index and returns it.
	*/
	QMovie* takeTabAnimation(int index);

	/*!
		Description:
		Returns the (possibly empty) list of actions for the tab at index.
	*/
	int tabIndexAt(const QPoint& pos) const;

public Q_SLOTS:
	void setMovieFrame(int frame);

Q_SIGNALS:
	void tabContextMenuRequested(int index, const QPoint& globalPos);

protected:
	virtual void tabInserted(int index) override;
	virtual void tabRemoved(int index) override;

	virtual void contextMenuEvent(QContextMenuEvent* event) override;

	/*!
		Description:
		This event handler, for event, can be reimplemented in a
		subclass to receive context menu events for the tab at index.
		The handler is called when tabContextMenuPolicy is
		Qt::DefaultContextMenu.
		The default implementation ignores the context menu event.
	*/
	virtual void tabContextMenuEvent(int index, QContextMenuEvent* event);

private:
	bool m_always;
	QList<QList<QAction*> > m_actions;
	Qt::ContextMenuPolicy m_policy;
	QList<QMovie*> m_animations;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQTabWidget_h__