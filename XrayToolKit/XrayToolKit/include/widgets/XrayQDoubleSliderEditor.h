/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/08
** filename: 	XrayQDoubleSliderEditor.h
** file base:	XrayQDoubleSliderEditor
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a double value slider with an editor.
****************************************************************************/

#pragma once
#ifndef XrayQDoubleSliderEditor_h__
#define XrayQDoubleSliderEditor_h__

#include "XrayQtApiExport.h"

#include <QWidget>
#include <QDoubleSpinBox>

namespace Ui {
	class XrayQDoubleSliderEditor;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQDoubleSliderEditor : public QWidget
{
	Q_OBJECT

public:

	Q_PROPERTY(double value
	READ value
	WRITE setValue
	NOTIFY valueChanged)

	Q_PROPERTY(double minimum READ minimum WRITE setMinimum)
	Q_PROPERTY(double maximum READ maximum WRITE setMaximum)
	Q_PROPERTY(double singleStep READ singleStep WRITE setSingleStep)

	explicit XrayQDoubleSliderEditor(QWidget *parent = 0);
	~XrayQDoubleSliderEditor();

	double value();
	void setValue(double newval);

	void setValueToNull();

	double minimum();
	double maximum();
	double singleStep();
	int decimals();
	void setMinimum(double);
	void setMaximum(double);
	void setSingleStep(double);
	void setDecimals(int prec);
	void setOrientation(Qt::Orientation) {}

	void setValues(double _value, double _step, int _decimals, double _min, double _max);

	/** Whether the slider uses discrete steps (in units of SingleStep) or
	 * a continuous range of values */
	void setForceDiscreteSteps(bool useDiscreteSteps);

	QDoubleSpinBox* spinBox() const;

public slots:

	void sliderValueChanged(int);
	void spinnerValueChanged(double);
	void stepUp();
	void stepDown();

signals:
	void valueChanged(double);

private:

	void updateSliderFromSpinner();

	Ui::XrayQDoubleSliderEditor *ui;

	bool m_IgnoreSliderEvent, m_IgnoreSpinnerEvent;
	bool m_ForceDiscreteSteps;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQDoubleSliderEditor_h__