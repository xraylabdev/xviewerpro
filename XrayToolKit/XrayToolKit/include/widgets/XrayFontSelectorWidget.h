/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/05
** filename: 	XrayFontSelectorWidget.h
** file base:	XrayFontSelectorWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a widget that contains font family, size
				bold, italic, underline, and color widgets.
****************************************************************************/

#pragma once
#ifndef XrayFontSelectorWidget_h__
#define XrayFontSelectorWidget_h__

#include "XrayQtApiExport.h"
#include "XrayColorPushButton.h"

#include <QWidget>
#include <QComboBox>
#include <QFontComboBox>
#include <QHBoxLayout>
#include <QAction>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayFontSelectorWidget : public QWidget
{
	Q_OBJECT

public:
	XrayFontSelectorWidget(QWidget* pParent = nullptr);

	void setFont(const QFont& _font);
	QFont getFont() const;
	
	void setColor(const QColor& _color);
	QColor getColor() const;

signals:
	void fontChanged(const QFont&);

private slots:
	void fontSizeChanged(const QString&);
	void currentFontChanged(const QFont&);
	void handleFontChange();

private:
	QComboBox* fontSizeCombo;
	QFontComboBox* fontCombo;
	QPushButton* boldAction;
	QPushButton* underlineAction;
	QPushButton* italicAction;
	XrayColorFrame* p_colorDialog;
	QHBoxLayout* p_layout;
	QFont m_font;
};

XRAYLAB_END_NAMESPACE

#endif // XrayFontSelectorWidget_h__