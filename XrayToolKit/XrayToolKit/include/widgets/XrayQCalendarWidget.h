/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/28
** filename: 	XrayQCalendarWidget.h
** file base:	XrayQCalendarWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a QApplication object with some
				pre-defined attributes.
****************************************************************************/

#pragma once
#ifndef XrayQCalendarWidget_h__
#define XrayQCalendarWidget_h__

#include "XrayQtApiExport.h"

#include <QWidget>

QT_BEGIN_NAMESPACE
class QCalendarWidget;
class QCheckBox;
class QComboBox;
class QDate;
class QDateEdit;
class QGridLayout;
class QGroupBox;
class QLabel;
QT_END_NAMESPACE

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQCalendarWidget : public QWidget
{
	Q_OBJECT

public:
	XrayQCalendarWidget();

private slots:
	void localeChanged(int index);
	void firstDayChanged(int index);
	void selectionModeChanged(int index);
	void horizontalHeaderChanged(int index);
	void verticalHeaderChanged(int index);
	void selectedDateChanged();
	void minimumDateChanged(const QDate &date);
	void maximumDateChanged(const QDate &date);
	void weekdayFormatChanged();
	void weekendFormatChanged();
	void reformatHeaders();
	void reformatCalendarPage();

private:
	void createPreviewGroupBox();
	void createGeneralOptionsGroupBox();
	void createDatesGroupBox();
	void createTextFormatsGroupBox();
	QComboBox *createColorComboBox();

	QGroupBox *previewGroupBox;
	QGridLayout *previewLayout;
	QCalendarWidget *calendar;

	QGroupBox *generalOptionsGroupBox;
	QLabel *localeLabel;
	QLabel *firstDayLabel;

	QLabel *selectionModeLabel;
	QLabel *horizontalHeaderLabel;
	QLabel *verticalHeaderLabel;
	QComboBox *localeCombo;
	QComboBox *firstDayCombo;
	QComboBox *selectionModeCombo;
	QCheckBox *gridCheckBox;
	QCheckBox *navigationCheckBox;
	QComboBox *horizontalHeaderCombo;
	QComboBox *verticalHeaderCombo;

	QGroupBox *datesGroupBox;
	QLabel *currentDateLabel;
	QLabel *minimumDateLabel;
	QLabel *maximumDateLabel;
	QDateEdit *currentDateEdit;
	QDateEdit *minimumDateEdit;
	QDateEdit *maximumDateEdit;

	QGroupBox *textFormatsGroupBox;
	QLabel *weekdayColorLabel;
	QLabel *weekendColorLabel;
	QLabel *headerTextFormatLabel;
	QComboBox *weekdayColorCombo;
	QComboBox *weekendColorCombo;
	QComboBox *headerTextFormatCombo;

	QCheckBox *firstFridayCheckBox;

	QCheckBox *mayFirstCheckBox;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQCalendarWidget_h__