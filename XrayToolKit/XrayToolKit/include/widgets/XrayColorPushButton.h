/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/05
** filename: 	XrayColorPushButton.h
** file base:	XrayColorPushButton
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a dialog to get a color from a user.
****************************************************************************/

#pragma once
#ifndef XrayColorPushButton_h__
#define XrayColorPushButton_h__

#include "XrayQtApiExport.h"

#include <QPushButton>
#include <QColor>
#include <QFrame>
#include <QComboBox>
#include <QGridLayout>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayColorPushButton : public QPushButton
{
	Q_OBJECT

public:
	XrayColorPushButton(QWidget* _parent = nullptr);

	virtual void paintEvent(QPaintEvent* _paintEvent);
	virtual void mousePressEvent(QMouseEvent* _event);

	int		getMargin(void) const;
	void	setMargin(const int& _margin);
	int		getRadius(void) const;
	void	setRadius(const int& _radius);
	QColor	getColor(void) const;
	void	setColor(const QColor& _color, bool _blockSignals = false);

private slots:
	void onCurrentColorChanged(const QColor& Color);

signals:
	void currentColorChanged(const QColor&);

private:
	int m_margin;
	int m_radius;
	QColor m_color;
	QColor m_lastColor;
};

class XRAY_QT_API_EXPORT XrayColorFrame : public QFrame
{
	Q_OBJECT

public:
	XrayColorFrame(QWidget* _parent = nullptr);

	QColor getColor(void) const;
	void setColor(const QColor& _color, bool _blockSignals = false);

private slots:
	void onCurrentColorChanged(const QColor&);

signals:
	void currentColorChanged(const QColor&);

private:
	QGridLayout m_mainLayout;
	XrayColorPushButton m_colorButton;
	QComboBox m_colorCombo;
};

XRAYLAB_END_NAMESPACE

#endif // XrayColorPushButton_h__