/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/11
** filename: 	XrayImageSlideShowWidget.h
** file base:	XrayImageSlideShowWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides slide show widget with next, previous,
				play and pause buttons..
****************************************************************************/

#pragma once
#ifndef XrayImageSlideShowWidget_h__
#define XrayImageSlideShowWidget_h__

#include "XrayQtApiExport.h"

#include <QLabel>
#include <QPixmap>
#include <QResizeEvent>
#include <QTimer>
#include <QStatusBar>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayImageSlideShowWidget : public QLabel
{
	Q_OBJECT

public:
	/*!
		Description:
		Constructor to create a image slide show widget by specifying image file paths.
		The first image will be shown in the widget during construction.
		/param _imageFiles - list of image file paths.
	*/
	explicit XrayImageSlideShowWidget(const QStringList& _imageFiles, QWidget* _parent = nullptr);
	virtual ~XrayImageSlideShowWidget();

	/*!
		Description:
		Function to set the internal images scaled with the main widget.
		/param _scaled - true to scale the contents.
	*/
	void setScaledContents(bool _scaled);

	/*!
		Description:
		Enums for the types of transition animations.
	*/
	enum TransitionType
	{
		None = 0,
		LeftToRight,
		RightToLeft,
		TopToBottom,
		BottomToTop,
		FadeIn,
		FadeOut,
	};

	/*!
		Description:
		Function to set transition animation type.
		/param _type - transition animation type.
	*/
	void setTransitionType(TransitionType _type);
	/*!
		Description:
		Function to get transition animation type.
		/return - transition animation type.
	*/
	TransitionType getTransitionType() const;

	/*!
		Description:
		Function to set the transition animation duration in milliseconds.
		This duration is for only slideshow. Slideshow can be started
		by calling the start(2000) function by specifying the slide delay.
		/param _msec - transition duration of one image in milliseconds.
	*/
	void setSlideShowTransitionDuration(int _msec);
	/*!
		Description:
		Function to get the transition animation duration in milliseconds.
		/return - transition duration of one image in milliseconds.
	*/
	int getSlideShowTransitionDuration() const;
	/*!
		Description:
		Function to set the transition animation duration in milliseconds.
		This duration is for to show an image using showImage() function.
		For this function no need to start the timer.
		/param _msec - transition duration of one image in milliseconds.
	*/
	void setTransitionDuration(int _msec);
	/*!
		Description:
		Function to get the transition animation duration in milliseconds.
		/return - transition duration of one image in milliseconds.
	*/
	int getTransitionDuration() const;

	/*!
		Description:
		Function that returns true if the slide show timer is on pause.
		/return - true if the slide show timer is on pause.
	*/
	bool pause() const;

	/*!
		Description:
		Function that returns the list of image files.
		/return - list of image files.
	*/
	QStringList getImageFiles() const;

	/*!
		Description:
		Function that returns a reference to status bar that
		contains the next, previous, play and pause buttons.
		/return - a reference to status bar widget.
	*/
	QStatusBar& getStatusBar();

public slots:
	/*!
		Description:
		Function to show the next image.
	*/
	void next();
	/*!
		Description:
		Function to show the previous image.
	*/
	void previous();
	/*!
		Description:
		Function to show the image at the given index.
	*/
	void showImage(int _index);
	/*!
		Description:
		Function to show the image at the given file path.
	*/
	void showImage(const QString& _fileName);
	/*!
		Description:
		Function to set the image file paths.
	*/
	void setImageFiles(const QStringList& _fileNames);
	/*!
		Description:
		Function to start the timer to start the slide show.
		/param _delay - delay between images in milliseconds
		(interval to show the next image).
	*/
	void start(int _delay);
	/*!
		Description:
		Function to pause the slide show timer.
		/param _b - true to pause the timer.
	*/
	void pause(bool _b);
	/*!
		Description:
		Function to stop the slide show timer.
	*/
	void stop();

protected:
	void showImage(int _index, int _transitionDuration);
	void nextSlide();
	void enterEvent(QEvent* _e) override;
	void leaveEvent(QEvent* _e) override;

	QStringList m_imageFiles;
	QLabel m_frontFrame;
	QTimer m_timer;
	TransitionType m_transitionType;
	QStatusBar m_statusBar;
	int m_currentIndex;
	int m_lastIndex;
	int m_slideShowDuration;
	int m_imageShowDuration;
	bool m_pauseAnimation;
};

XRAYLAB_END_NAMESPACE

#endif // XrayImageSlideShowWidget_h__