/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/28
** filename: 	XraySplashScreen.h
** file base:	XraySplashScreen
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a splash screen with a progress bar.
****************************************************************************/

#pragma once
#ifndef XraySplashScreen_h__
#define XraySplashScreen_h__

#include "XrayQtApiExport.h"

#include <QSplashScreen>
#include <QPainter>
#include <QThread>
#include <QTimer>
#include <QVector>
#include <QFont>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XraySplashScreen : public QSplashScreen
{
	Q_OBJECT

public:
	/*!
	   Description:
	   Constructor to create a QApp object.
	   /param pixmap - image to be shown
	   /param progressValue - it's the multiplyer to 100ms. If set to 100, progress bar will be displayed at 100%.
	   // if set to 10, progress will be updated 100/10 times from 0 to 100.
	*/
	explicit XraySplashScreen(const QPixmap &pixmap, int progressValue, Qt::WindowFlags f = Qt::WindowFlags());
	~XraySplashScreen() override;

	/*!
	   Description:
	   Function that start the progress bar thread, show the screen, and starts the event loop.
	*/
	void start();

	/*!
	   Description:
	   Function to set the progress bar position and size.
	*/
	void setBarROI(const QRect& rect) { m_barRect = rect; }
	/*!
	   Description:
	   Function that returns the progress bar position and size.
	*/
	QRect getBarROI() const { return m_barRect; }

	/*!
	   Description:
	   Function to set the progress bar color.
	*/
	void setBarColor(const QColor& color) { m_barColor = color; }
	/*!
	   Description:
	   Function that returns the progress bar color.
	*/
	QColor getBarColor() const { return m_barColor; }

	/*!
	   Description:
	   Function to set the bar text color.
	*/
	void setBarTextColor(const QColor& color) { m_textColor = color; }
	/*!
	   Description:
	   Function that returns the bar text color.
	*/
	QColor getBarTextColor() const { return m_textColor; }

	/*!
	   Description:
	   Function to set text on top of the screen.
	*/
	void setBarText(const QString& text) { m_barText = text; }
	/*!
	   Description:
	   Function that returns the text.
	*/
	QString getBarText() const { return m_barText; }

	/*!
	   Description:
	   Function to set text on top of the screen.
	*/
	void setTextBarAlignment(int align) { m_barTextAlign = align; }
	/*!
	   Description:
	   Function that returns the text.
	*/
	int getTextBarAlignment() const { return m_barTextAlign; }

	struct Text
	{
		QString m_text;
		QFont m_font;
		QColor m_textColor;
		QRect m_rect;
		int m_textAlign;
	};

	/*!
	   Description:
	   Function to add a text as a overlay to the splash screen.
	*/
	void addText(const Text& text) { m_texts.push_back(text); }
	/*!
	   Description:
	   Function to set text at a given index.
	*/
	void setText(const Text& text, int index) { if(index >= 0 || index < m_texts.size()) m_texts[index] = text; }
	/*!
	   Description:
	   Function to get text at a given index.
	*/
	Text getText(int index) { return index < 0 || index >= m_texts.size() ? Text() : m_texts[index]; }
	/*!
	   Description:
	   Function to remove all overlay texts.
	*/
	void removedAllTexts() { m_texts.clear(); }

signals:
	/*!
	   Description:
	   Function that emits when the progress reaches to 100.
	*/
	void finished();

public slots:
	/*!
	   Description:
	   Function to set the progress bar percentage.
	*/
	void setProgress(int value);

protected:
	/*!
	   Description:
	   Function that draws all screen contents.
	*/
	void drawContents(QPainter *painter) override;

	QThread* timerThread;
	QTimer* timer;
	int m_progress;
	int m_incremental;
	int m_incrementalValue;
	QString m_barText;
	int m_barTextAlign;
	QRect m_barRect;
	QColor m_barColor;
	QColor m_textColor;
	QVector<Text> m_texts;
};

XRAYLAB_END_NAMESPACE

#endif // XraySplashScreen_h__