/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/13
** filename: 	XrayStackedFramelessWindowWin32.h
** file base:	XrayStackedFramelessWindowWin32
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a frame less main window with a stacked
				widget as it's central widget.
****************************************************************************/

#pragma once
#ifndef XrayStackedFramelessWindowWin32_h__
#define XrayStackedFramelessWindowWin32_h__

#include "XrayFramelessWindowWin32.h"
#include <QStackedWidget>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayStackedFramelessWindowWin32 : public XrayFramelessWindowWin32
{
	Q_OBJECT

public:
	/*!
		Description:
		Default constructor that create a frame less window with
		a stacked widget as its central widget.
	*/
	XrayStackedFramelessWindowWin32(const QString& _title, QWidget* _parent = nullptr);

	/*!
		Description:
		Function to set the current index to the stacked widget.
	*/
	void setCurrentIndex(int index);
	/*!
		Description:
		Function to add the given widget to the stacked widget.
	*/
	void addWidget(QWidget *w);
	/*!
		Description:
		Function to remove the given widget from the stacked widget.
	*/
	void removeWidget(QWidget *w);

	/*!
		Description:
		Function to get a pointer to internal stacked widget.
	*/
	QStackedWidget* stackedWidget() const;

private:
	QStackedWidget* p_stackedWidget;
};

XRAYLAB_END_NAMESPACE

#endif // XrayStackedFramelessWindowWin32_h__