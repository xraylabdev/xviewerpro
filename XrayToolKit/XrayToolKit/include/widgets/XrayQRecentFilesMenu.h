/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/18
** filename: 	XrayQRecentFilesMenu.h
** file base:	XrayQRecentFilesMenu
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a recent files menu with actions.
** Usage:

fileMenu = menuBar()->addMenu(tr("&File"));
fileMenu->addAction(openAct);
recentFilesMenu = new XrayQRecentFilesMenu(tr("Recent Files"), fileMenu);
QSettings settings;
recentFilesMenu->restoreState(settings.value("recentFiles").toByteArray());
connect(recentFilesMenu, &XrayQRecentFilesMenu::recentFileTriggered, this, &XrayMainWindow::loadFile);
void XrayMainWindow::loadFile(const QString& fileName)
{
	...
	...
	recentFilesMenu->addRecentFile(fileName);
}
****************************************************************************/

#pragma once
#ifndef XrayQRecentFilesMenu_h__
#define XrayQRecentFilesMenu_h__

#include "XrayQtApiExport.h"

#include <QMenu>
#include <QStringList>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQRecentFilesMenu : public QMenu
{
	Q_OBJECT

	Q_PROPERTY(int maxCount READ maxCount WRITE setMaxCount)
	Q_PROPERTY(QString format READ format WRITE setFormat)

public:
	//! Constructs a menu with parent parent.
	XrayQRecentFilesMenu(QWidget * parent = 0);

	//! Constructs a menu with a title and a parent.
	XrayQRecentFilesMenu(const QString & title, QWidget * parent = 0);

	//! Returns the maximum number of entries in the menu.
	int maxCount() const;

	/** This property holds the string used to generate the item text.
	 * %d is replaced by the item number
	 * %s is replaced by the item text
	 * The default value is "%d %s".
	 */
	void setFormat(const QString &format);

	//! Returns the current format. /sa setFormat
	const QString & format() const;

	/** Saves the state of the recent entries.
	 * Typically this is used in conjunction with QSettings to remember entries
	 * for a future session. A version number is stored as part of the data.
	 * Here is an example:
	 * QSettings settings;
	 * settings.setValue("recentFiles", recentFilesMenu->saveState());
	 */
	QByteArray saveState() const;

	/** Restores the recent entries to the state specified.
	 * Typically this is used in conjunction with QSettings to restore entries from a past session.
	 * Returns false if there are errors.
	 * Here is an example:
	 * QSettings settings;
	 * recentFilesMenu->restoreState(settings.value("recentFiles").toByteArray());
	 */
	bool restoreState(const QByteArray &state);

public slots:
	//!
	void addRecentFile(const QString &fileName);
	void removeRecentFile(const QString &fileName);

	//! Removes all the menu's actions.
	void clearMenu();

	//! Sets the maximum number of entries int he menu.
	void setMaxCount(int);

signals:
	//! This signal is emitted when a recent file in this menu is triggered.
	void recentFileTriggered(const QString& filename);

private slots:
	void menuTriggered(QAction*);
	void updateRecentFileActions();

private:
	int m_maxCount;
	QString m_format;
	QStringList m_projectDir;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQRecentFilesMenu_h__