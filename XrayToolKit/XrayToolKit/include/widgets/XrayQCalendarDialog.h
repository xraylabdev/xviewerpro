/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/12
** filename: 	XrayQCalendarDialog.h
** file base:	XrayQCalendarDialog
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a dialog with calendar widget.
****************************************************************************/

#pragma once
#ifndef XrayQCalendarWidget_h__
#define XrayQCalendarWidget_h__

#include "XrayQtApiExport.h"

#include <QDialog>
#include <QGridLayout>
#include <QCalendarWidget>
#include <QCloseEvent>
#include <QDate>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQCalendarDialog : public QDialog
{
	Q_OBJECT

public:
	XrayQCalendarDialog(QWidget* _parent = nullptr, Qt::WindowFlags _flags = Qt::WindowFlags());

	void setMinimumDate(const QDate& _date);

	void setMaximumDate(const QDate& _date);

	QDate selectedDate() const;

protected:
	void closeEvent(QCloseEvent*) override;
	void saveSettings();

private:
	QGridLayout* p_layout;
	QCalendarWidget* p_calendar;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQCalendarWidget_h__
