/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XrayToolButtonGridLayoutWidget.h
** file base:	XrayToolButtonGridLayoutWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that creates a widget with a grid of tool buttons.
****************************************************************************/

#pragma once
#ifndef XrayImageSlideShowButtonsWidget_h__
#define XrayImageSlideShowButtonsWidget_h__

#include "XrayQtApiExport.h"
#include "XrayImageToolButton.h"

#include <QWidget>
#include <QGridLayout>
#include <QPair>

XRAYLAB_BEGIN_NAMESPACE

/*!
	This class create a widget that contains a grid layout to make a grid of
	tool buttons with icons and texts.
*/
class XRAY_QT_API_EXPORT XrayToolButtonGridLayoutWidget : public QWidget
{
	Q_OBJECT

public:
	/*!
		Description:
		Default constructor of the ButtonsWidget class.
		/param _button_texts list of the button texts.
		/param _icon path of the icon and must be specified like this ":/res/images/app_icon_%1.png".
		/param _ncols how many columns need to be created.
		/param _parent pointer to parent widget.
	*/
	explicit XrayToolButtonGridLayoutWidget(const QList<QPair<QString, QString> >& _button_texts, int _ncols, QWidget* _parent = nullptr);
	explicit XrayToolButtonGridLayoutWidget(QWidget* _parent = nullptr);
	/*!
		Description:
		Function that removes all the previous added widgets from the internal grid layout
		and adds new buttons to the layout. The buttons will be created by the size of _button_texts list.
		/param _button_texts - list of the image files and its texts (first => image file, second => image text).
		/param _icon path of the icon and must be specified like this ":/res/images/app_icon_%1.png".
		/param _ncols how many columns need to be created.
	*/
	void addButtons(const QList<QPair<QString, QString> >& _button_texts, int _ncols);
	/*!
		Description:
		Function that remove and deletes all the widgets from the internal grid layout.
	*/
	void removeAll();
	/*!
		Description:
		Function that returns the reference to the list of buttons.
	*/
	XrayImageToolButton* getButton(const QString& _text);
	/*!
		Description:
		Function that returns the reference to the list of buttons.
	*/
	auto& getButtons() { return m_buttons; }
	/*!
		Description:
		Function that returns the reference to the list of buttons.
	*/
	auto& getButtonsTexts() { return m_button_texts; }

public slots:
	void setToUnpressed(XrayImageToolButton* _btn);

protected:
	void resizeEvent(QResizeEvent*) override;

	QList<XrayImageToolButton*> m_buttons;	/**< List of buttons. */
	QList<QPair<QString, QString> > m_button_texts;	/**< List of button texts. */
	QGridLayout m_layout;		/**< Grid layout that holds the buttons and texts. */
};

XRAYLAB_END_NAMESPACE

#endif // XrayImageSlideShowButtonsWidget_h__