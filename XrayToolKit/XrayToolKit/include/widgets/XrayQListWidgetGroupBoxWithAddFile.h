/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/06/08
** filename: 	XrayQListWidgetGroupBoxWithAddFile.h
** file base:	XrayQListWidgetGroupBoxWithAddFile
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a list widget.
** Usage:
auto list = new XrayQListWidgetGroupBoxWithAddFile();
****************************************************************************/

#pragma once
#ifndef XrayQListWidgetGroupBoxWithAddFile_h__
#define XrayQListWidgetGroupBoxWithAddFile_h__

#include "XrayQListWidget.h"

#include <QGroupBox>
#include <QHBoxLayout>
#include <QPushButton>
#include <QMenu>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayQListWidgetGroupBoxWithAddFile : public QGroupBox
{
	Q_OBJECT

public:
	/*!
	   Description:
	   Default constructor to create a list widget.
	   Set setContextMenuPolicy(Qt::NoContextMenu) in order to disable custom menu;
	   @param - parent is the parent to this widget.
	*/
	XrayQListWidgetGroupBoxWithAddFile(const QString &title, QWidget *parent = nullptr);
	virtual ~XrayQListWidgetGroupBoxWithAddFile();

	/*!
	   Description:
	   Function to add texts to internal list.
	   This function will only add unique string by discarding the duplicates.
	*/
	void addListTexts(const QStringList &list);
	/*!
	   Description:
	   Function to set texts to internal list.
	*/
	void setListTexts(const QStringList &list);
	/*!
	   Description:
	   Function to get texts from internal list.
	*/
	QStringList getListTexts() const;
	/*!
	   Description:
	   Function to set selected the given text list.
	*/
	void setSelectedListTexts(const QStringList& texts);
	/*!
	   Description:
	   Function to get selected texts from internal list.
	*/
	QStringList getSelectedListTexts() const;

	/*!
	   Description:
	   Function to get a pointer to internal list widget.
	*/
	XrayQListWidget* getListWidget() const;

	/*!
	   Description:
	   Function to set extension filters for browse dialog.
	   @param - filters extension filters
	*/
	void setBrowseExtensionFilters(const QString& filters);
	/*!
	   Description:
	   Function to get extension filters.
	*/
	QString getBrowseExtensionFilters() const;

public slots:
	void browseFile();

protected:
	XrayQListWidget* listWidget;
	QVBoxLayout* vLayout;
	QHBoxLayout* hLayout;
	QPushButton* browseButton;
	QString extensionFilters;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQListWidgetGroupBoxWithAddFile_h__