/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/13
** filename: 	XrayFramelessWindowWin32.h
** file base:	XrayFramelessWindowWin32
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a frame less main window.
****************************************************************************/

#pragma once
#ifndef XrayFramelessWindowWin32_h__
#define XrayFramelessWindowWin32_h__

#include "XrayQtApiExport.h"

#include <QMainWindow>
#include <QToolButton>
#include <QMouseEvent>
#include <QSettings>
#include <QLabel>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayFramelessWindowWin32 : public QMainWindow
{
	Q_OBJECT

public:
	/*!
		Description:
		Default constructor that create a frame less window.
	*/
	explicit XrayFramelessWindowWin32(const QString& _title, QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());

	/*!
		Description:
		Function to add the given widget in the central vertical layout.
	*/
	void setWindowTitle(const QString& title);
	/*!
		Description:
		Function to add the given widget in the central vertical layout.
	*/
	void addCentralWidget(QWidget* _widget);
	/*!
		Description:
		Function to add the given widget in the central vertical layout.
	*/
	void setMenuBar(QMenuBar *menubar);

	/*!
		Description:
		Function to save the current window geometry and state.
	*/
	void saveSettings();
	/*!
		Description:
		Function to restore the last window geometry and state.
	*/
	void restoreSettings();
	/*!
		Description:
		Function to clear the last window geometry and state.
	*/
	void clearSettings();

	/*!
		Description:
		Function that saves the current window screen-shot to given file name.
	*/
	bool saveScreenShot(const QString& _fileName);

	/*!
		Description:
		Function that changes the top right buttons icons to dark or light style.
	*/
	void setDarkThemeEnabled(bool _b);

	QToolButton* maximumButton() const;
	QToolButton* restoreButton() const;
	QToolButton* minimumButton() const;
	QToolButton* menuButton() const;

protected:
	bool nativeEvent(const QByteArray& _event_type, void* _message, long* _result) override;
	void mousePressEvent(QMouseEvent* _event) override;
	void changeEvent(QEvent* _event) override;

private slots:
	void slot_show_system_menu() const;
	void slot_minimized();
	void slot_maximized();
	void slot_restored();
	void slot_closed();

protected:
	auto setBorderless(bool enabled, bool shadow) const -> void;

	QToolButton* p_menuButton;
	QToolButton* p_minimizeButton;
	QToolButton* p_maximizeButton;
	QToolButton* p_restoreButton;
	QToolButton* p_closeButton;
	QToolButton* p_iconButton;
	QLabel* p_titleLabel;
	QWidget* p_centralWidget;
	QScopedPointer<QSettings> p_settings;
	bool m_clear_settings;
};

XRAYLAB_END_NAMESPACE

#endif // XrayFramelessWindowWin32_h__