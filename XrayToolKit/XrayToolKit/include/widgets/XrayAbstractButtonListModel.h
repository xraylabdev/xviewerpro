/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayAbstractButtonListModel.h
** file base:	XrayAbstractButtonListModel
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides list model for buttons.
****************************************************************************/

#pragma once
#ifndef XrayAbstractButtonListModel_h__
#define XrayAbstractButtonListModel_h__

#include "XrayQtApiExport.h"

#include <QAbstractListModel>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayAbstractButtonListModel : public QAbstractListModel
{
	Q_OBJECT

public:
	enum Roles 
	{
		IdRole = Qt::UserRole,
		ShortcutRole
	};

	explicit XrayAbstractButtonListModel(QObject *parent = 0);

	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;

	virtual QVariant data(const QModelIndex &index, int role) const;
	virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

	virtual Qt::ItemFlags flags(const QModelIndex &index) const;

	virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
	virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());

	void clear();

private:
	using Button = QMap<int, QVariant>;
	using Buttons = QVector<Button>;
	Buttons m_buttons;
};

XRAYLAB_END_NAMESPACE

#endif // XrayAbstractButtonListModel_h__