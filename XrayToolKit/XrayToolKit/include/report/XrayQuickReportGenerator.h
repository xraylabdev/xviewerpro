/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/015
** filename: 	XrayQuickReportGenerator.h
** file base:	XrayQuickReportGenerator
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		classes that provide wizard pages for the reporting widget.
****************************************************************************/

#pragma once
#ifndef XrayQuickReportGenerator_h__
#define XrayQuickReportGenerator_h__

#include "XrayQtApiExport.h"

#include <QObject>
#include <QSettings>

XRAYLAB_BEGIN_NAMESPACE

/*!
	This class create a widget for the intro page of the report generator.
*/
class XRAY_QT_API_EXPORT XrayQuickReportGenerator
{
public:
	XrayQuickReportGenerator();
	void clear();

	static QStringList parseSrcImages(const QString &html);
	static QString parseValue(const QString &html, const QString &str, const QString &splitStr, Qt::CaseSensitivity cs = Qt::CaseSensitive);
	static QString parseValue(const QString &html, const QString &str, Qt::CaseSensitivity cs = Qt::CaseSensitive);
	static bool containValue(const QString &html, const QString &str, Qt::CaseSensitivity cs = Qt::CaseSensitive);

	enum ReportType
	{
		None = 0,
		Nominal,
		Porosity,
		WallThicknessRay,
		WallThicknessSphere
	};

	void setReportType(ReportType type) { m_reportType = type; }
	ReportType getReportType() const { return m_reportType; }

	/*!
		Description:
		Function that reads the specified HTML file and extract information from it.
	*/
	bool parseIniFile(const QString &fileName);
	/*!
		Description:
		Function that reads the specified HTML file and extract information from it.
	*/
	int parseHtmlFile(const QString &fileName);

	/*!
		Description:
		Function that returns a value of the specified key.
	*/
	auto value(const QString& _key) { return m_map.value(_key); }
	/*!
		Description:
		Function that returns a reference to the internal map.
	*/
	auto& map() { return m_map; }
	/*!
		Description:
		Function that returns a reference to the images.
	*/
	auto& images() { return m_images; }

	/*!
		Description:
		Function that print values and images to console.
	*/
	void print();

	QString getImageCaption(int index = 0);

protected:
	QMap<QString, QString> readNominalComparison(const QString& str);
	QMap<QString, QString> readPorosityAnalysis(const QString& str);
	QMap<QString, QString> readWallThicknessAnalysisRayMethod(const QString& str);
	QMap<QString, QString> readWallThicknessAnalysisSphereMethod(const QString& str);
	QMap<QString, QString> readProjectIniFile(const QString &fileName);
	QString nominalComparisonImageText(int index = 0);
	QString porosityAnalysisImageText(int index = 0);
	QString wallThicknessAnalysisRayMethodImageText(int index = 0);
	QString wallThicknessAnalysisSphereMethodImageText(int index = 0);

	QMap<QString, QString> m_map;
	QStringList m_images;
	ReportType m_reportType;
};

XRAYLAB_END_NAMESPACE

#endif // XrayQuickReportGenerator_h__