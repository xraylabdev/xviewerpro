/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/015
** filename: 	XrayReportWizardPages.h
** file base:	XrayReportWizardPages
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		classes that provide wizard pages for the reporting widget.
****************************************************************************/

#pragma once
#ifndef XrayReportWizardPages_h__
#define XrayReportWizardPages_h__

#include "XrayQtApiExport.h"

#include "XrayWizardWidget.h"
#include "XrayFindFilesWidget.h"
#include "XrayPaintMainWindow.h"
#include "XrayColorPushButton.h"
#include "XrayFontSelectorWidget.h"
#include "XrayQListWidgetGroupBoxWithAddFile.h"
#include "XrayQListWidgetGroupBoxWithAddText.h"

#include <QPrinter>
#include <QLabel>
#include <QGridLayout>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QCheckBox>
#include <QGroupBox>
#include <QCloseEvent>
#include <QFontComboBox>
#include <QSettings>

XRAYLAB_BEGIN_NAMESPACE

enum ReportWizardPage
{
	FindFiles, 
	DocTitle,
	DocHeader,
	DocFooter,
	ClientInfo, 
	Investigation, 
	CustomizedSections, 
	Results, 
	Paint, 
	CustomizedSections2, 
	Finish
};

/*!
	This class create a widget for the intro page of the report generator.
*/
class XRAY_QT_API_EXPORT XrayFindFilesWizardPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit XrayFindFilesWizardPage(QWidget *parent = nullptr);
	
	/*!
		Description:
		Function that writes settings to the given settings.
	*/
	void writeSettings(QSettings& _settings);
	/*!
		Description:
		Function that reads settings from the given settings and update the widget.
	*/
	void readSettings(QSettings& _settings);

	/*!
		Description:
		Function that shows a text message at the bottom of the widget.
	*/
	void setMessageText(const QString& _text);

	/*!
		Function that returns the next page id.
	*/
	int nextId() const;

	/*!
		Function to set the restore file path.
	*/
	void setRestoreFilePath(const QString& _type);
	/*!
		Function that returns the restore file path.
	*/
	QString getRestoreFilePath() const;

	/*!
		Function that retranslates the language texts.
	*/
	void retranslate();

signals:
	void clickedBrowse();
	void firstColumnActivated(const QString& _filePath);
	void lineEditReturnPressed(const QString& _text);

public:
	XrayFindFilesWidget* p_findFilesWidget;

	QGroupBox* p_findFilesGroupBox;
	QGroupBox* p_restorePreviousGroupBox;

	QLabel* p_docRestoreLabel;
	QLineEdit* p_docRestore;
	QPushButton* p_browseBtn;
};

/*!
	This class create a widget for the intro page of the report generator.
*/
class XRAY_QT_API_EXPORT XrayDocTitleWizardPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit XrayDocTitleWizardPage(QWidget *parent = nullptr);

	/*!
		Function that returns the next page id.
	*/
	int nextId() const;
	/*!
		Function that retranslates the language texts.
	*/
	void retranslate();

	/*!
		Function that returns the contact person name.
	*/
	QString getGroupBoxTitle() const;
	/*!
		Function that returns the document name.
	*/
	QString getDocNameLabel() const;
	QString getDocName() const;

	/*!
		Function that returns the document title.
	*/
	QString getDocTitleLabel() const;
	QString getDocTitle() const;

	/*!
		Function that returns the document title.
	*/
	void setDocType(const QString& _type);
	QString getDocTypeLabel() const;
	QString getDocType() const;

	/*!
		Function that returns a pointer to heading font selector.
	*/
	XrayFontSelectorWidget* getHeadingFontSelector() const;
	/*!
		Function that returns a pointer to body text font selector.
	*/
	XrayFontSelectorWidget* getBodyFontSelector() const;

	void setPaperSizeCurrentIndex(int _index);
	int getPaperSizeCurrentIndex() const;

	void setPaperOrientationCurrentIndex(int _index);
	int getPaperOrientationCurrentIndex() const;

	QPrinter::PageSize getPageSize();
	QPrinter::Orientation getPageOrientation();

public:
	void setupComboBoxes();
	QLabel* paperSizeComboLabel;
	QLabel* paperOrientationCombLabel;
	QComboBox* paperSizeCombo;
	QComboBox* paperOrientationCombo;
	QPrinter::PageSize m_pageSize;
	QPrinter::Orientation m_orientation;

	QGroupBox* p_groupBox;
	QLabel* p_docNameLabel;
	QLabel* p_docTitleLabel;
	QLabel* p_docTypeLabel;
	QLineEdit* p_docName;
	QLineEdit* p_docTitle;
	QLineEdit* p_docType;

	QLabel* p_docHeadingFontLabel;
	QLabel* p_docBodyFontLabel;
	XrayFontSelectorWidget* p_textFontSelector;
	XrayFontSelectorWidget* p_headingFontSelector;
	//QFontComboBox* p_textFontComboBox;
	//QFontComboBox* p_headingFontComboBox;
	//XrayColorFrame p_table_heading_color;
	//XrayColorFrame p_doc_heading_color;
};
/*!
	This class create a widget for the header page of the report generator.
*/
class XRAY_QT_API_EXPORT XrayDocHeaderWizardPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit XrayDocHeaderWizardPage(QWidget *parent = nullptr);

	/*!
		Function that returns the next page id.
	*/
	int nextId() const;
	/*!
		Function that retranslates the language texts.
	*/
	void retranslate();

	/*!
		Function that returns the contact person name.
	*/
	QString getGroupBoxTitle() const;
	/*!
		Function that returns the contact person name.
	*/
	QString getContactPersonName() const;
	QString getContactPersonNameLabel() const;
	/*!
		Function that returns the contact person phone.
	*/
	QString getContactPersonPhone() const;
	QString getContactPersonPhoneLabel() const;
	/*!
		Function that returns the contact person email.
	*/
	QString getContactPersonEmail() const;
	QString getContactPersonEmailLabel() const;

public:
	QGroupBox* p_groupBox;
	QLabel* p_contactPersonNameLabel;
	QLabel* p_contactPersonPhoneLabel;
	QLabel* p_contactPersonEmailLabel;
	QLineEdit* p_contactPersonName;
	QLineEdit* p_contactPersonPhone;
	QLineEdit* p_contactPersonEmail;
};
/*!
	This class create a widget for the footer page of the report generator.
*/
class XRAY_QT_API_EXPORT XrayDocFooterWizardPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit XrayDocFooterWizardPage(QWidget *parent = nullptr);

	/*!
		Function that returns the next page id.
	*/
	int nextId() const;
	/*!
		Function that retranslates the language texts.
	*/
	void retranslate();

	/*!
		Function that returns the company name.
	*/
	QString getGroupBoxTitle() const;
	/*!
		Function that returns the company name.
	*/
	QString getCompanyName() const;
	/*!
		Function that returns the company location.
	*/
	QString getCompanyLocation() const;
	/*!
		Function that returns the company phone.
	*/
	QString getCompanyPhone() const;
	/*!
		Function that returns the company fax.
	*/
	QString getCompanyFax() const;

	/*!
		Function that returns the company fax.
	*/
	void setLogoFile(const QString& fileName);
	QString getLogoFile() const;

public:
	QGroupBox* p_groupBox;
	QLabel* p_companyNameLabel;
	QLabel* p_companyLocationLabel;
	QLabel* p_companyPhoneLabel;
	QLabel* p_companyFaxLabel;
	QLabel* p_logoImageLabel;
	QLineEdit* p_companyName;
	QLineEdit* p_companyLocation;
	QLineEdit* p_companyPhone;
	QLineEdit* p_companyFax;
	QLineEdit* p_logoFile;
	QLabel* p_logoImage;
};
/*!
	This class create a widget for the client info page of the report generator.
*/
class XRAY_QT_API_EXPORT XrayClientInfoWizardPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit XrayClientInfoWizardPage(QWidget *parent = nullptr);

	/*!
		Function that returns the next page id.
	*/
	int nextId() const;
	/*!
		Function that retranslates the language texts.
	*/
	void retranslate();

	/*!
		Function that returns the order number.
	*/
	QString getGroupBoxTitle() const;
	/*!
		Function that returns the order number.
	*/
	QString getOrderNumberLabel() const;
	/*!
		Function that returns the date.
	*/
	QString getDateLabel() const;
	/*!
		Function that returns the client name.
	*/
	QString getClientNameLabel() const;
	/*!
		Function that returns the sample name.
	*/
	QString getSampleNameLabel() const;
	/*!
		Function that returns the quantity.
	*/
	QString getQuantityLabel() const;

	/*!
		Function that returns the order number.
	*/
	QString getOrderNumber() const;
	/*!
		Function that returns the date.
	*/
	QString getDate() const;
	/*!
		Function that returns the client name.
	*/
	QString getClientName() const;
	/*!
		Function that returns the sample name.
	*/
	QString getSampleName() const;
	/*!
		Function that returns the quantity.
	*/
	QString getQuantity() const;

public:
	QGroupBox* p_groupBox;
	QLabel* p_orderNumberLabel;
	QLabel* p_dateLabel;
	QLabel* p_clientNameLabel;
	QLabel* p_sampleNameLabel;
	QLabel* p_quantityLabel;

	QLineEdit* p_orderNumber;
	QLineEdit* p_date;
	QLineEdit* p_clientName;
	QLineEdit* p_sampleName;
	QLineEdit* p_quantity;
	QPushButton* p_calendarBtn;
};
/*!
	This class create a widget for the body text page of the report generator.
*/
class XRAY_QT_API_EXPORT XrayInvestigationWizardPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit XrayInvestigationWizardPage(QWidget *parent = nullptr);

	/*!
		Function that returns the next page id.
	*/
	int nextId() const;
	/*!
		Function that retranslates the language texts.
	*/
	void retranslate();

	/*!
	Function that returns the order number.
*/
	QString getInvestigationAnalysisLabel() const;
	/*!
		Function that returns the date.
	*/
	QString getProblemStatementLabel() const;
	/*!
		Function that returns the client name.
	*/
	QString getHypotheticalFactorLabel() const;
	/*!
		Function that returns the sample name.
	*/
	QString getEquipmentLabel() const;


	/*!
		Function that returns the order number.
	*/
	QString getInvestigationAnalysis() const;
	/*!
		Function that returns the date.
	*/
	QString getProblemStatement() const;
	/*!
		Function that returns the client name.
	*/
	QString getHypotheticalFactor() const;
	/*!
		Function that returns the sample name.
	*/
	QString getEquipmentDescription() const;

	void setMachinesList(const QStringList& list);
	QStringList getMachinesList() const;
	void setSelectedMachinesList(const QStringList& list);
	QStringList getSelectedMachinesList() const;

	/*!
		Function that returns the sample name.
	*/
	QString getEquipmentsAsString() const;

	int getVoltages() const;
	int getCurrent() const;

public:
	QLabel* p_investigationLabel;
	QLabel* p_problemLabel;
	QLabel* p_hypotheticalErrorLabel;
	QLabel* p_equipmentLabel;

	QTextEdit* p_investigation;
	QTextEdit* p_problem;
	QTextEdit* p_hypotheticalfactor;
	QTextEdit* p_equipmentDescription;

	QLabel* p_voltageUsedLabel;
	QLineEdit* p_voltageUsed;
	QLabel* p_currentUsedLabel;
	QLineEdit* p_currentUsed;

	XrayQListWidgetGroupBoxWithAddText* listWidget;
};

/*!
	This class create a widget for the assigned parts page of the report generator.
*/
class XRAY_QT_API_EXPORT XrayCustomizedSectionsWizardPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit XrayCustomizedSectionsWizardPage(QWidget *parent = nullptr);

	/*!
		Function that returns the next page id.
	*/
	int nextId() const override;
	/*!
		Function that retranslates the language texts.
	*/
	void retranslate();

	/*!
		Function deletes all widgets from the layout and resets the counter.
	*/
	void resetLayout();

	struct Section
	{
		QHBoxLayout* layout;
		QLineEdit* lineEdit;
		QTextEdit* textEdit;
		QPushButton* button;
	};

	/*!
		Function that add a new section (QLabel + QTextEdit) to the layout.
	*/
	Section createSection();

	/*!
		Function that returns a reference to sections map.
	*/
	auto& getSections() { return p_sections; }

public:
	QGroupBox* p_sectionGroupBox;
	QLabel* p_removeSectionLabel;
	QVBoxLayout* p_layout;

	QVector<Section> p_sections;
};

/*!
	This class create a widget for the assigned parts page of the report generator.
*/
class XRAY_QT_API_EXPORT XrayResultsWizardPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit XrayResultsWizardPage(QWidget *parent = nullptr);

	/*!
		Function that returns the next page id.
	*/
	int nextId() const;
	/*!
		Function that retranslates the language texts.
	*/
	void retranslate();

	/*!
		Function that returns the results.
	*/
	QString getResultDescriptionLabel() const;
	/*!
		Function that returns the path of the data file.
	*/
	QString getResultDataLabel() const;
	/*!
		Function that returns the path of the data file.
	*/
	QString getImageDescriptionLabel() const;

	/*!
		Function that returns the results.
	*/
	QString getResultDescription() const;
	/*!
		Function that returns the path of the data file.
	*/
	void setDataFiles(const QStringList& fileNames);
	QStringList getDataFiles() const;
	/*!
		Function that returns the path of the data file.
	*/
	void setSelectedDataFiles(const QStringList& fileNames);
	QStringList getSelectedDataFiles() const;
	/*!
		Function that returns the path of the data file.
	*/
	QString getCsvDelimeter() const;
	/*!
		Function that returns the path of the data file.
	*/
	QString getImageDescription() const;

public:
	QLabel* p_resultsDescriptionLabel;
	QLabel* p_resultsDataLabel;
	QLabel* p_imageDescriptionLabel;
	QTextEdit* p_resultsDescription;
	QLineEdit* p_csv_delimeter;
	QTextEdit* p_imageDescription;

	XrayQListWidgetGroupBoxWithAddFile* listWidget;
};

/*!
	This class create a widget for the assigned parts page of the report generator.
*/
class XRAY_QT_API_EXPORT XrayPaintWizardPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit XrayPaintWizardPage(QWidget *parent = nullptr);

	/*!
		Function that returns the next page id.
	*/
	int nextId() const;
	/*!
		Function that retranslates the language texts.
	*/
	void retranslate();

	/*!
		Function that returns a pointer to paint widget.
	*/
	XrayPaintMainWindow* getPaintWidget() const;

private:
	XrayPaintMainWindow* p_paintWidget;
};


/*!
	This class create a widget for the finish page of the report generator.
*/
class XRAY_QT_API_EXPORT XrayCustomizedSections2WizardPage : public XrayCustomizedSectionsWizardPage
{
	Q_OBJECT

public:
	explicit XrayCustomizedSections2WizardPage(QWidget *parent = nullptr);

	/*!
		Function that returns the next page id.
	*/
	int nextId() const override;
};


/*!
	This class create a widget for the finish page of the report generator.
*/
class XRAY_QT_API_EXPORT XrayFinishWizardPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit XrayFinishWizardPage(QWidget *parent = nullptr);

	/*!
		Function that returns the next page id.
	*/
	int nextId() const;
	/*!
		Function that retranslates the language texts.
	*/
	void retranslate();

	/*!
		Function to set the path of the signature image file.
	*/
	void setSignatureFile(const QString& _filePath);
	/*!
		Function that returns the path of the signature image file.
	*/
	QString getSignatureFile() const;

	/*!
		Function that returns the path of the data file.
	*/
	void setRegardsDescription(const QString& _text);
	QString getRegardsDescription() const;

public:
	QLabel* p_signatureLabel;
	QLineEdit* p_signatureFile;
	QPushButton* p_signatureFileBrowseBtn;

	QLabel* p_bestRegardsLabel;
	QTextEdit* p_bestRegardsDescription;
};

XRAYLAB_END_NAMESPACE

#endif // XrayReportWizardPages_h__