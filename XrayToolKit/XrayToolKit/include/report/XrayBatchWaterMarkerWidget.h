/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/015
** filename: 	XrayBatchWaterMarkerWidget.h
** file base:	XrayBatchWaterMarkerWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		classes that provide wizard pages for the reporting widget.
****************************************************************************/

#pragma once
#ifndef XrayBatchWaterMarkerWidget_h__
#define XrayBatchWaterMarkerWidget_h__

#include "XrayQtApiExport.h"

#include <QWidget>
#include <QSettings>

#include <KDReports>

namespace Ui
{
	class XrayBatchWaterMarkerWidget;
}

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayBatchWaterMarkerWidget : public QWidget
{
	Q_OBJECT

public:
	explicit XrayBatchWaterMarkerWidget(QWidget* _parent = nullptr);

	/*!
		Description:
		Function that writes settings to the given settings.
	*/
	void writeSettings(QSettings& _settings);
	/*!
		Description:
		Function that reads settings from the given settings and update the widget.
	*/
	void readSettings(QSettings& _settings);

	/*!
		Description:
		Function that enable or disable the demo version restrictions.
	*/
	void enableDemoVersion(bool _b);

signals:
	void statusBarMessage(const QString& text, int _timeout);

public slots:
	void browseWatermark();
	void browseImages();
	void browseSave();
	void rgbOnGray(bool _b);
	void preview(int _value);
	void apply();

private:
	QStringList getAllImages();

	Ui::XrayBatchWaterMarkerWidget* ui;
	bool m_isDemo;
};

XRAYLAB_END_NAMESPACE

#endif // XrayBatchWaterMarkerWidget_h__