/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/11/22
** filename: 	XrayBatchWaterMarkerWindow.h
** file base:	XrayBatchWaterMarkerWindow
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the main window for image stitcher.
****************************************************************************/

#pragma once
#ifndef XrayBatchWaterMarkerWindow_h__
#define XrayBatchWaterMarkerWindow_h__

#include "XrayQtApiExport.h"
#include "XrayFramelessWindowWin32.h"
#include "XrayBatchWaterMarkerWidget.h"
#include "XrayStatusBar.h"

#include <QMenu>
#include <QAction>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayBatchWaterMarkerWindow : public XrayFramelessWindowWin32
{
	Q_OBJECT

public:
	XrayBatchWaterMarkerWindow(const QString& _title, QWidget* _parent = nullptr);
	~XrayBatchWaterMarkerWindow();

	void enableDemoVersion(const QString& _modules, bool b);

protected:
	void closeEvent(QCloseEvent* _event) override;

private:
	QMenu* createMenu();
	QMenu* p_menu;
	QAction* p_actionViewHelp;
	QAction* p_actionWebsite;
	QAction* p_actionAbout;
	QAction* p_actionRestoreLastTree;
	QAction* p_actionRootPath;
	QAction* p_actionUpdate;
	QAction* p_actionDockVisibility;

	XrayBatchWaterMarkerWidget* p_widget;
	XrayStatusBar* p_statusBar;
};

XRAYLAB_END_NAMESPACE

#endif // XrayBatchWaterMarkerWindow_h__