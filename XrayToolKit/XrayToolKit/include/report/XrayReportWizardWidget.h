/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/015
** filename: 	XrayReportWizardWidget.h
** file base:	XrayReportWizardWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		classes that create a wizard of the report sections.
****************************************************************************/

#pragma once
#ifndef XrayReportWizardWidget_h__
#define XrayReportWizardWidget_h__

#include "XrayQtApiExport.h"

#include "XrayWizardWidget.h"
#include "XrayReportWizardPages.h"

#include <QLabel>
#include <QGridLayout>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QCheckBox>
#include <QGroupBox>
#include <QCloseEvent>
#include <QFontComboBox>
#include <QSettings>

#include <KDReports>

XRAYLAB_BEGIN_NAMESPACE

/*!
	class that create a wizard for the report generator tool.
*/
class XRAY_QT_API_EXPORT XrayReportWizardWidget : public XrayWizardWidget
{
	Q_OBJECT

public:
	explicit XrayReportWizardWidget(QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());
	~XrayReportWizardWidget();

	/*!
		Description:
		Function that writes settings to the given settings.
	*/
	void writeSettings(QSettings& settings);
	/*!
		Description:
		Function that reads settings from the given settings and update the widget.
	*/
	void readSettings(QSettings& settings);
	/*!
		Description:
		Function that restore settings from the given settings and update the widget.
		This function clears those entries which the settings file doesn't have.
	*/
	void restoreSettings(QSettings& settings);
	/*!
		Description:
		Function that reads settings from the given settings and update the widget.
	*/
	void clearSettings();

	/*!
		Description:
		Function that returns a pointer to XEnahncer batch process directories box.
	*/
	void setEnhancerDirectoriesBoxVisible(bool b);
	/*!
		Description:
		Function to enable the demo version that limits some of the features.
	*/
	void enableDemoVersion(const QString& modules, bool b);

public slots:
	void showHelp();
	void idChanged(int id);
	/*!
		Function that returns the next page id.
	*/
	void generate(KDReports::Report& report);
	void preview();
	void saveAs();
	void saveBackup();
	void saveBackupAs();
	void restoreBackupByBrowse();
	/*!
		Description:
		Function that restore settings from the given *.ini file.
	*/
	bool restoreBackup(const QString& fileName);

private:
	void closeEvent(QCloseEvent *event) override;
	void keyPressEvent(QKeyEvent* event);

	XrayFindFilesWizardPage* p_findFilesPage;
	XrayDocTitleWizardPage* p_titlePage;
	XrayDocHeaderWizardPage* p_headerPage;
	XrayDocFooterWizardPage* p_footerPage;
	XrayClientInfoWizardPage* p_clientInfoPage;
	XrayInvestigationWizardPage* p_investigationPage;
	XrayCustomizedSectionsWizardPage* p_customizedSectionsPage;
	XrayResultsWizardPage* p_resultsPage;
	XrayPaintWizardPage* p_paintPage;
	XrayCustomizedSections2WizardPage* p_customizedSections2Page;
	XrayFinishWizardPage* p_finishPage;
};

XRAYLAB_END_NAMESPACE

#endif // XrayReportWizardWidget_h__