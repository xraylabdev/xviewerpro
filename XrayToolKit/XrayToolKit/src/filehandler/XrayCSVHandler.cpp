/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/26
** filename: 	XrayCSVHandler.cpp
** file base:	XrayCSVHandler
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class to read the *.csv file with optional delimiter.
****************************************************************************/

#include "XrayCSVHandler.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <locale>

XRAYLAB_USING_NAMESPACE

XrayCSVHandler::XrayCSVHandler() 
{
}

XrayCSVHandler::XrayCSVHandler(const std::string& fileName, char delimiter) 
{ 
	read(fileName, delimiter);
}

std::vector<std::string> XrayCSVHandler::getHeaders() const
{ 
	return m_headers; 
}

std::vector<std::vector<std::string> > XrayCSVHandler::getData() const
{ 
	return m_data; 
}

enum class CSVState
{
	UnquotedField,
	QuotedField,
	QuotedQuote
};
enum class CSVDelimiter
{
	Comma,
	SemiColon
};

static std::vector<std::string> readCSVRow(const std::string &row, char delimiter)
{
	CSVState state = CSVState::UnquotedField;
	std::vector<std::string> fields{ "" };
	std::size_t i = 0; // index of the current field
	for (char c : row)
	{
		switch (state)
		{
		case CSVState::UnquotedField:
			if (c == delimiter)					// end of field
			{
				fields.emplace_back(""); 
				i++;
			}
			else if (c == '"')
			{
				state = CSVState::QuotedField;
			}
			else
			{
				fields[i].push_back(c);
			}
			break;

		case CSVState::QuotedField:
			if (c == '"')
			{
				state = CSVState::QuotedQuote;
			}
			else
			{
				fields[i].push_back(c);
			}
			break;

		case CSVState::QuotedQuote:
			if (c == delimiter)					// , after closing quote
			{
				fields.push_back(""); 
				i++;
				state = CSVState::UnquotedField;
			}
			else if (c == '"')					// "" -> "
			{
				fields[i].push_back('"');
				state = CSVState::QuotedField;
			}
			else								// end of quote
			{
				state = CSVState::UnquotedField;
			}
			break;
		}
	}
	return fields;
}

// Read CSV file, Excel dialect. Accept "quoted fields ""with quotes"""
bool XrayCSVHandler::read(const std::string& fileName, char delimiter)
{
	std::ifstream file(fileName.c_str());
	if (file.fail())
		return false;

	if (!file.is_open())
		return false;

	auto setHeaders = false;

	std::vector<std::vector<std::string>> table;
	std::string row;
	while (!file.eof())
	{
		std::getline(file, row);
		if (file.bad() || file.fail())
		{
			break;
		}

		if (!setHeaders)
		{
			setHeaders = true;
			m_headers = readCSVRow(row, delimiter);
		}
		else
		{
			auto fields = readCSVRow(row, delimiter);

			while (fields.size() < m_headers.size())
				fields.push_back("");

			m_data.emplace_back(fields);
		}
	}

	return true;
}

// doesn't work well if last char is not a delimiter
bool XrayCSVHandler::read_old(const std::string& fileName, char delimiter)
{
	std::ifstream file(fileName.c_str());
	if (file.fail())
		return false;

	if (!file.is_open())
		return false;

	std::stringstream sstr;
	sstr << file.rdbuf();
	auto f = sstr.str();
	file.close();

	const auto len = f.length();

	auto setHeaders = false;

	std::size_t pos = 0;
	while (pos < len) // While the file's not empty
	{ 
		std::vector<std::string> line;
		while (f.at(pos) != '\n' && pos < len) // For each character in the line
		{ 
			std::string element;
			while (f.at(pos) != delimiter && pos < len && f.at(pos) != '\n' && f.at(pos) != '\r') // For each element
			{ 
				if (f.at(pos) == '"') // If we have a quote, continue till the next quote
				{ 
					pos++;
					while (f[pos] != '"' && pos < len)
					{
						element += f.at(pos);
						pos++;
					}
					pos++; // Last quote
				}
				else 
				{
					element += f.at(pos);
					pos++;
				}
			}

			// remove leading whitespaces
			while (!element.empty() && std::isspace<char>(*element.begin(), std::locale::classic()))
				element.erase(element.begin());

			// remove trailing whitespaces
			while (!element.empty() && std::isspace<char>(*element.rbegin(), std::locale::classic()))
				element.erase(element.length() - 1);

			line.push_back(element);

			if (f.at(pos) == '\n') 
			{
				break;
			}
			pos++;
		}

		if (!setHeaders) 
		{
			setHeaders = true;
			m_headers = line;
		}
		else 
		{
			while (line.size() < m_headers.size())
				line.push_back("");

			m_data.push_back(line);
		}

		pos++;
	}

	return true;
}

void XrayCSVHandler::print(char delimiter)
{
	if (!m_headers.empty())
	{
		std::cout << '\n' << '\n';

		std::cout << m_headers[0];
		for (auto i = 1; i < m_headers.size(); i++)
			std::cout << delimiter << m_headers[i];

		std::cout <<'\n';
	}

	for (auto& i : m_data)
	{
		const auto& cols = i;

		if (!cols.empty())
		{
			std::cout << cols[0];
			for (auto j = 1; j < cols.size(); j++)
				std::cout << delimiter << cols[j];
		}

		std::cout << '\n';
	}
}