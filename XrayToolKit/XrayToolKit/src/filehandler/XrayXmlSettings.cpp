﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/01
** filename: 	XrayXmlSettings.cpp
** file base:	XrayXmlSettings
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides reading and writing QSettings to xml.
****************************************************************************/

#include "XrayXmlSettings.h"

#include <QMap>
#include <QList>
#include <QSharedPointer>
#include <QStringList>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

XRAYLAB_USING_NAMESPACE

const QSettings::Format XrayXmlSettings::format = QSettings::registerFormat("xml", XrayXmlSettings::read, XrayXmlSettings::write);


bool XrayXmlSettings::read(QIODevice& device, QSettings::SettingsMap& map) 
{
	QXmlStreamReader xmlReader(&device);

	QStringList groups;

	while (!xmlReader.atEnd()) 
	{
		xmlReader.readNext();

		if (xmlReader.isStartElement())
			groups.append(xmlReader.name().toString());
		else if (xmlReader.isCharacters() && !xmlReader.isWhitespace())
			map[groups.join("/")] = xmlReader.text().toString();
		else if (xmlReader.isEndElement())
			groups.removeLast();

	}

	return !xmlReader.hasError();
}

bool XrayXmlSettings::write(QIODevice& device, const QSettings::SettingsMap& map) 
{
	struct NestedMap;
	struct NestedMap : QMap<QString, QSharedPointer<NestedMap> > {};

	QSharedPointer<NestedMap> nestedMap(new NestedMap());

	for (auto it = map.begin(); it != map.end(); ++it) 
	{
		auto currentMap = nestedMap;

		auto groups = it.key().split("/");

		for (auto jt = groups.begin(); jt != groups.end(); ++jt)
		{
			auto kt = currentMap->find(*jt);

			if (kt == currentMap->end()) 
			{
				kt = currentMap->insert(*jt, QSharedPointer<NestedMap>(new NestedMap()));
				currentMap = kt.value();
			}
			else
				currentMap = kt.value();

		}

	}

	QXmlStreamWriter xmlWriter(&device);

	xmlWriter.setAutoFormatting(true);
	xmlWriter.writeStartDocument();

	QStringList groups;
	QList<QSharedPointer<NestedMap> > nestedMaps;
	QList<NestedMap::iterator> nestedMapIterators;

	nestedMaps.append(nestedMap);
	nestedMapIterators.append(nestedMap->begin());

	while (!nestedMaps.isEmpty()) 
	{
		auto currentMap = nestedMaps.last();
		auto it = nestedMapIterators.last();

		if (it != currentMap->end()) 
		{
			xmlWriter.writeStartElement(it.key());

			groups.append(it.key());
			nestedMaps.append(it.value());
			nestedMapIterators.append(it.value()->begin());

		}
		else 
		{
			if (currentMap->isEmpty())
				xmlWriter.writeCharacters(map[groups.join("/")].toString());

			xmlWriter.writeEndElement();

			if (!groups.isEmpty())
				groups.removeLast();
			nestedMaps.removeLast();
			nestedMapIterators.removeLast();

			if (!nestedMaps.isEmpty())
				++nestedMapIterators.last();

		}
	}

	xmlWriter.writeEndDocument();

	return true;
}

