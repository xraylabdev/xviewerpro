/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/25
** filename: 	XrayQFileReader.cpp
** file base:	XrayQFileReader
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class to read the text files.
****************************************************************************/

#include "XrayQFileReader.h"
#include "XrayLogger.h"


XRAYLAB_USING_NAMESPACE

XrayQFileReader::XrayQFileReader() :
	m_nLines(-1),
	m_charactersPerLine(256)
{
}
XrayQFileReader::XrayQFileReader(const QString& _fileName, int _nLines, int _charactersPerLine) :
	m_nLines(_nLines),
	m_charactersPerLine(_charactersPerLine)
{
	m_file.setFileName(_fileName);
}

QStringList XrayQFileReader::getLines()
{
	if (!m_file.exists())
	{
		xAppInfo("This file {} doesn't exists.", m_file.fileName().toStdString());
		return {};
	}

	if (!m_file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		xAppInfo("Unable to read this file: {}", m_file.fileName().toStdString());
		return {};
	}

	if (m_nLines > 0)
		m_file.seek(m_file.size() - m_nLines * m_charactersPerLine);

	auto text = m_file.readAll();

	m_file.close();

	return QString(text).split('\n');
}
QStringList XrayQFileReader::getLines(const QString& _fileName, int _nLines, int _charactersPerLine)
{
	m_file.setFileName(_fileName);
	m_nLines = _nLines;
	m_charactersPerLine = _charactersPerLine;
	return getLines();
}