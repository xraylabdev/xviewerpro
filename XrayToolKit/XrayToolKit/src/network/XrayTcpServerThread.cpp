/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/10
** filename: 	XrayTcpServerThread.cpp
** file base:	XrayTcpServerThread
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a threaded-based server side that
send multiple files data to the client side.
****************************************************************************/

#include "XrayTcpServerThread.h"

#include <QtNetwork>

XRAYLAB_USING_NAMESPACE

XrayTcpServerThread::XrayTcpServerThread(int _socketDescriptor, const QStringList& _files, QObject* _parent) :
	QThread(_parent),
	m_socketDescriptor(_socketDescriptor),
	m_projectDir(_files)
{
}
XrayTcpServerThread::XrayTcpServerThread(int _socketDescriptor, const QByteArray& _data, QObject* _parent) :
	QThread(_parent),
	m_socketDescriptor(_socketDescriptor),
	m_data(_data)
{
}

void XrayTcpServerThread::run()
{
	QTcpSocket socket;
	if (!socket.setSocketDescriptor(m_socketDescriptor))
	{
		emit error(socket.error());
		return;
	}

	emit onStart();

	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_5_12);

	if (!m_projectDir.empty())
	{
		auto n = 0;
		emit onProgress(0.0);

		auto ok = false;
		out << static_cast<qint16>(m_projectDir.size());
		for (const auto& i : m_projectDir)
		{
			QFile file(i);
			if (file.exists())
			{
				if (file.open(QIODevice::ReadOnly))
				{
					out << i;
					out << file.readAll();
					file.close();
					ok = true;
					emit onProgress(n++ / static_cast<double>(m_projectDir.size()));
				}
			}
		}

		if (!block.isEmpty() && ok)
		{
			socket.write(block);
			emit onFinished();
		}
	}
	else
	{
		if (!m_data.isEmpty())
		{
			socket.write(m_data);
			emit onFinished();
		}
	}

	socket.disconnectFromHost();
	socket.waitForDisconnected();
}
