﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/04
** filename: 	XrayQFileDownloaderWidget.cpp
** file base:	XrayQFileDownloaderWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a file downloader.
****************************************************************************/

#include "XrayQFileDownloaderWidget.h"
#include "XrayGlobal.h"
#include "XrayQtUtil.h"
#include "XrayQFileUtil.h"

#include <QStandardPaths>
#include <QFileDialog>
#include <QApplication>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayQFileDownloaderWidget::XrayQFileDownloaderWidget(QWidget* _parent) :
	QWidget(_parent),
	p_downloader(nullptr)
{
	p_fileSizeLabel = new QLabel;
	p_downloadedLabel = new QLabel;
	p_transferRateLabel = new QLabel;
	p_speedLabel = new QLabel;

	p_downloadInfolayout = new QGridLayout;
	p_downloadInfolayout->addWidget(new QLabel(QApplication::translate("XrayQFileDownloaderWidget", "File size")), 0, 0);
	p_downloadInfolayout->addWidget(p_fileSizeLabel, 0, 1);

	p_downloadInfolayout->addWidget(new QLabel(QApplication::translate("XrayQFileDownloaderWidget", "Downloaded")), 1, 0);
	p_downloadInfolayout->addWidget(p_downloadedLabel, 1, 1);

	p_downloadInfolayout->addWidget(new QLabel(QApplication::translate("XrayQFileDownloaderWidget", "Transfer rate")), 2, 0);
	p_downloadInfolayout->addWidget(p_transferRateLabel, 2, 1);

	p_downloadInfolayout->addWidget(new QLabel(QApplication::translate("XrayQFileDownloaderWidget", "Time left")), 3, 0);
	p_downloadInfolayout->addWidget(p_speedLabel, 3, 1);

	p_progressBar = new XrayProgressWidget;
	p_progressBar->setPercentTextVisible(false);
	p_progressBar->setStatusWidgetVisible(false);

	auto downloadTo = new QLabel(QApplication::translate("XrayQFileDownloaderWidget", "Download to: "));

	p_downloadPathEdit = new QLineEdit;
	p_downloadPathEdit->setText(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));

	p_browseBtn = new QPushButton(QApplication::translate("XrayQFileDownloaderWidget", "&Browse..."));
	p_browseBtn->setToolTip(QApplication::translate("XrayQFileDownloaderWidget", "Select download directory"));
	connect(p_browseBtn, &QPushButton::clicked, [this]()
	{
		auto dir = QFileDialog::getExistingDirectory(this, QApplication::translate("XrayQFileDownloaderWidget", "Open Directory"), XrayGlobal::getLastFileOpenPath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
		if (dir.isEmpty())
			return;

		p_downloadPathEdit->setText(dir);
		XrayGlobal::setLastFileOpenPath(dir);
	});

	p_abortBtn = new QPushButton(QApplication::translate("XrayQFileDownloaderWidget", "Abort"));
	p_abortBtn->setToolTip(QApplication::translate("XrayQFileDownloaderWidget", "Abort download"));
	
	connect(p_abortBtn, &QPushButton::clicked, this, &XrayQFileDownloaderWidget::abort);

	p_layout = new QGridLayout;
	p_layout->addLayout(p_downloadInfolayout, 0, 0, 1, 3);
	p_layout->addWidget(p_progressBar, 1, 0, 1, 3);

	p_layout->addWidget(downloadTo, 2, 0);
	p_layout->addWidget(p_downloadPathEdit, 2, 1);
	p_layout->addWidget(p_browseBtn, 2, 2);
	p_layout->addWidget(p_abortBtn, 3, 2);
	setLayout(p_layout);
}

void XrayQFileDownloaderWidget::setUrl(const QUrl& _url)
{
	m_url = _url;
}
QUrl XrayQFileDownloaderWidget::getUrl() const
{
	return m_url;
}
void XrayQFileDownloaderWidget::setAuthentication(const QPair<QString, QString>& _username_password)
{ 
	m_authenticator = _username_password;
}
QPair<QString, QString> XrayQFileDownloaderWidget::getAuthentication() const
{ 
	return m_authenticator; 
}

void XrayQFileDownloaderWidget::start()
{
	if (m_url.isEmpty())
		return;

	p_downloader = std::make_unique<XrayQFileDownloader>(m_url);
	p_downloader->setAuthentication(m_authenticator);

	connect(p_downloader.get(), &XrayQFileDownloader::startDownload, [this]() 
	{ 
		m_time.start();
		p_progressBar->start(QApplication::translate("XrayQFileDownloaderWidget", "Downloading..."));
		emit startDownload(); 
	});

	connect(p_downloader.get(), &XrayQFileDownloader::downloadProgress, [this](qint64 _bytesReceived, qint64 _bytesTotal)
	{
		// calculate the download speed
		auto speed = _bytesReceived * 1000.0 / m_time.elapsed();
		QString unit;
		if (speed < 1024)
		{
			unit = "bytes/sec";
		}
		else if (speed < 1024 * 1024)
		{
			speed /= 1024;
			unit = "kB/s";
		}
		else
		{
			speed /= 1024 * 1024;
			unit = "MB/s";
		}

		// calculate the percentage
		auto percent = _bytesReceived / static_cast<double>(_bytesTotal);

		// calculate the time left
		auto remainingTime = 0.0;
		if ((speed != 0) && (_bytesTotal != 0))
			remainingTime = (_bytesTotal - _bytesReceived) / speed;
		auto remainingLeft = XrayQtUtil::convertSecToTime(remainingTime, false);

		setDownloadInfo(_bytesReceived, _bytesTotal, percent, speed, unit, remainingLeft);

		p_progressBar->setCurrentProgress(percent);
	});

	QObject::connect(p_downloader.get(), &XrayQFileDownloader::downloaded, [this]()
	{
		p_progressBar->finish("");
		setDownloadInfo(0, 0, 0, 0, "", "0");

		if (p_downloader->saveToDisk(p_downloadPathEdit->text()))
		{
			m_savedFile = p_downloader->getSavedFileName();
			emit downloaded();
		}
	});

	connect(p_downloader.get(), &XrayQFileDownloader::downloadError, [this](const QString&)
	{
		p_speedLabel->setText(QApplication::translate("XrayQFileDownloaderWidget", "Temporary network failure."));
	});

	QTimer::singleShot(100, p_downloader.get(), &XrayQFileDownloader::start);
}

void XrayQFileDownloaderWidget::setDownloadInfo(qint64 _bytesReceived, qint64 _bytesTotal, double _percent, double _speed, const QString& _speedUnit, const QString& _timeLeft)
{
	if(_bytesTotal == 0)
		p_fileSizeLabel->setText("");
	else
		p_fileSizeLabel->setText(XrayQFileUtil::getHumanReadableSize(_bytesTotal));

	if (_bytesReceived == 0)
		p_downloadedLabel->setText("");
	else
		p_downloadedLabel->setText(XrayQFileUtil::getHumanReadableSize(_bytesReceived) + QString(" ( %1 % )").arg(_percent * 100.0, 2, 'f', 1));

	if (_speed == 0)
		p_transferRateLabel->setText("");
	else
		p_transferRateLabel->setText(QString::fromLatin1("%1 %2").arg(_speed, 3, 'f', 1).arg(_speedUnit));

	if(_timeLeft == "0")
		p_speedLabel->setText("");
	else
		p_speedLabel->setText(_timeLeft);
}
void XrayQFileDownloaderWidget::abort()
{
	p_progressBar->finish("");
	setDownloadInfo(0, 0, 0, 0, "", "0");
	p_downloader.reset(new XrayQFileDownloader);	// reset to destroy the object
}

QString XrayQFileDownloaderWidget::getSavedFileName() const
{
	return m_savedFile;
}

QGridLayout* XrayQFileDownloaderWidget::getDownloadInfoLayout() const
{
	return p_downloadInfolayout;
}

QPushButton* XrayQFileDownloaderWidget::getBrowseButton() const
{
	return p_abortBtn;
}
QPushButton* XrayQFileDownloaderWidget::getAbortButton() const
{
	return p_browseBtn;
}