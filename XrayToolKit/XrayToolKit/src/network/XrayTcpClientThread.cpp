/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/10
** filename: 	XrayTcpClientThread.cpp
** file base:	XrayTcpClientThread
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a threaded-based client side that
receive multiple files or data from the server side and save to the disk
and emit a signal with the data. If you are only interested in the data but
don't want to save files to the disk then just make the directory path empty.
****************************************************************************/

#include "XrayTcpClientThread.h"

#include <QtNetwork>

XRAYLAB_USING_NAMESPACE

XrayTcpClientThread::XrayTcpClientThread(const QString& _dirPath, QObject* _parent) :
	QThread(_parent),
	m_quit(false),
	m_dirPath(_dirPath)
{
}

XrayTcpClientThread::~XrayTcpClientThread()
{
	m_mutex.lock();
	m_quit = true;
	m_cond.wakeOne();
	m_mutex.unlock();
	wait();
}

void XrayTcpClientThread::start(const QString& _hostName, quint16 _port, const QString& _dirPath)
{
	QMutexLocker locker(&m_mutex);
	m_hostName = _hostName;
	m_port = _port;
	m_dirPath = _dirPath;

	if (!isRunning())
		QThread::start();
	else
		m_cond.wakeOne();
}

void XrayTcpClientThread::run()
{
	m_mutex.lock();
	auto serverName = m_hostName;
	auto serverPort = m_port;
	m_mutex.unlock();

	while (!m_quit) 
	{
		const int timeout = 30 * 1000;

		QTcpSocket socket;
		socket.connectToHost(serverName, serverPort);

		if (!socket.waitForConnected(timeout))
		{
			emit error(socket.error(), socket.errorString());
			return;
		}

		emit onStart();

		emit onProgress(0.0);

		QDataStream in(&socket);
		in.setVersion(QDataStream::Qt_5_12);

		qint16 size;
		QVector<QPair<QString, QByteArray> > files;
		do 
		{
			if (!socket.waitForReadyRead(timeout))
			{
				emit error(socket.error(), socket.errorString());
				return;
			}

			in.startTransaction();

			in >> size;
			files.resize(size);

			for(qint16 i = 0; i < size; i++)
				in >> files[i].first >> files[i].second;

		} while (!in.commitTransaction());

		m_mutex.lock();

		QVector<QString> names;
		names.reserve(size);

		for (qint16 i = 0; i < size; i++)
		{
			emit onProgress(i / static_cast<double>(size));

			names.push_back(files[i].first);

			auto name = files[i].first.section('/', -1);

			if (saveToDisk(m_dirPath, name, files[i].second))
				qDebug() << QString("Successfully saved '%1' at '%2'").arg(name).arg(m_dirPath);
			else
				qDebug() << QString("Couldn't save '%1'").arg(name);
		}

		emit receivedFiles(names);
		emit onFinished();

		m_cond.wait(&m_mutex);
		serverName = m_hostName;
		serverPort = m_port;
		m_mutex.unlock();
	}
}

bool XrayTcpClientThread::saveToDisk(const QString& _dirName, const QString& _fileName, const QByteArray& _data)
{
	if (_dirName.isEmpty() || _fileName.isEmpty() || _data.isEmpty())
		return false;

	QString path;
	if (_dirName.endsWith('/'))
		path = _dirName + _fileName;
	else
		path = _dirName + "/" + _fileName;

	QFile file(path);
	if (file.open(QIODevice::WriteOnly | QIODevice::Truncate))
	{
		file.write(_data);
		file.close();
		return true;
	}
	return false;
}
