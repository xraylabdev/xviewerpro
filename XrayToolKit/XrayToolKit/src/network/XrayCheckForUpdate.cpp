﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/04
** filename: 	XrayCheckForUpdate.cpp
** file base:	XrayCheckForUpdate
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a file downloader.
****************************************************************************/

#include "XrayCheckForUpdate.h"
#include "XrayQtUtil.h"
#include "XrayXmlSettings.h"

#include <QApplication>
#include <QGroupBox>
#include <QMessageBox>
#include <QCheckBox>
#include <QDesktopServices>
#include <QSettings>
#include <QFileInfo>
#include <QDir>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayCheckForUpdate::XrayCheckForUpdate(const QUrl& _url, QWidget* _parent) :
	QWidget(_parent),
	m_url(_url),
	m_remindMeLaterDays(7)	// remind me later after 7 days
{
	setWindowTitle(QApplication::translate("XrayCheckForUpdate", "Software Update"));

	p_releaseNotesGBox = new QGroupBox(QApplication::translate("XrayCheckForUpdate", "Release Notes"));
	auto textGroupLayout = new QGridLayout;
	p_textBrowser = new QTextBrowser;
	p_textBrowser->setOpenExternalLinks(true);
	textGroupLayout->addWidget(p_textBrowser);
	p_releaseNotesGBox->setLayout(textGroupLayout);

	p_updateAvailableLabel = new QLabel(QApplication::translate("XrayCheckForUpdate", "Update is available"));
	p_updateAvailableLabel->setAlignment(Qt::AlignCenter);
	p_updateAvailableLabel->setStyleSheet("QLabel { font-weight: bold; }");

	p_installedLabel = new QLabel;
	p_availableLabel = new QLabel;
	p_releaseDateLabel = new QLabel;
	p_releaseStatusLabel = new QLabel;

	auto versionGroup = new QGroupBox(QApplication::translate("XrayCheckForUpdate", "Version"));
	auto versionGroupLayout = new QGridLayout;
	versionGroupLayout->addWidget(new QLabel(QApplication::translate("XrayCheckForUpdate", "Installed")), 0, 0);
	versionGroupLayout->addWidget(p_installedLabel, 0, 1);
	versionGroupLayout->addWidget(new QLabel(QApplication::translate("XrayCheckForUpdate", "Available")), 1, 0);
	versionGroupLayout->addWidget(p_availableLabel, 1, 1);
	versionGroupLayout->addWidget(new QLabel(QApplication::translate("XrayCheckForUpdate", "Release Date")), 2, 0);
	versionGroupLayout->addWidget(p_releaseDateLabel, 2, 1);
	versionGroupLayout->addWidget(new QLabel(QApplication::translate("XrayCheckForUpdate", "Release Status")), 3, 0);
	versionGroupLayout->addWidget(p_releaseStatusLabel, 3, 1);
	versionGroup->setLayout(versionGroupLayout);

	p_updateDownloader = new XrayQFileDownloaderWidget;
	connect(p_updateDownloader, &XrayQFileDownloaderWidget::downloaded, this, &XrayCheckForUpdate::launchDownloadedFile);	// launch the download file when it is downloaded.

	p_installUpdateBtn = new QPushButton(QApplication::translate("XrayCheckForUpdate", "Install Update"));
	connect(p_installUpdateBtn, &QPushButton::clicked, [this]()
	{
		p_downloadGBox->show();
		p_updateDownloader->start();
	});

	p_checkUpdateBtn = new QPushButton(QApplication::translate("XrayCheckForUpdate", "Remind Me Later"));
	connect(p_checkUpdateBtn, &QPushButton::clicked, [this]()
	{	
		QSettings settings(QApplication::organizationName(), QApplication::applicationName());
		settings.beginGroup("CheckForUpdate");
		settings.setValue("Date", QDate::currentDate().addDays(m_remindMeLaterDays).toString("dd/MM/yyyy"));	// check after 7 days.
		settings.endGroup();
		close();
	});

	auto btnLayout = new QHBoxLayout;
	btnLayout->addStretch(1);
	btnLayout->addWidget(p_checkUpdateBtn);
	btnLayout->addWidget(p_installUpdateBtn);

	p_downloadGBox = new QGroupBox(QApplication::translate("XrayCheckForUpdate", "Download"));
	auto downloadGBoxLayout = new QGridLayout;
	downloadGBoxLayout->addWidget(p_updateDownloader);
	p_downloadGBox->setLayout(downloadGBoxLayout);

	auto chkBox = new QCheckBox(QApplication::translate("XrayCheckForUpdate", "Always check on startup"));
	chkBox->setChecked(isCheckOnStartupEnabled());
	connect(chkBox, &QCheckBox::toggled, [this](bool checked) { checkOnStartupEnabled(checked);	});
	
	p_layout = new QGridLayout;
	p_layout->addWidget(p_updateAvailableLabel, 0, 0);
	p_layout->addWidget(versionGroup, 1, 0);
	p_layout->addWidget(p_releaseNotesGBox, 2, 0);
	p_layout->addLayout(btnLayout, 3, 0);
	p_layout->addWidget(p_downloadGBox, 4, 0);
	p_layout->addWidget(chkBox, 5, 0);

	p_releaseNotesGBox->setHidden(true);
	p_downloadGBox->setHidden(true);

	setLayout(p_layout);
	resize(600, 500);

	// if the version is checked by remind me later, then do not check by startup.
	if (!remindeMeLaterCheck())
	{
		if (isCheckOnStartupEnabled())
			checkForUpdate(false);
	}
}

void XrayCheckForUpdate::setUrl(const QUrl& _url)
{
	m_url = _url;
}
QUrl XrayCheckForUpdate::getUrl() const
{
	return m_url;
}
void XrayCheckForUpdate::checkOnStartupEnabled(bool _b)
{
	QSettings settings(QApplication::organizationName(), QApplication::applicationName());
	settings.beginGroup("CheckForUpdate");
	settings.setValue("CheckOnStartup", _b);
	settings.endGroup();
}
bool XrayCheckForUpdate::isCheckOnStartupEnabled() const
{
	QSettings settings(QApplication::organizationName(), QApplication::applicationName());
	settings.beginGroup("CheckForUpdate");
	auto b = settings.value("CheckOnStartup", true).toBool();
	settings.endGroup();
	return b;
}
void XrayCheckForUpdate::setRemindMeLaterDays(int _days)
{
	m_remindMeLaterDays = _days;
}
int XrayCheckForUpdate::getRemindMeLaterDays() const
{
	return m_remindMeLaterDays;
}

bool XrayCheckForUpdate::remindeMeLaterCheck()
{
	QSettings settings(QApplication::organizationName(), QApplication::applicationName());
	settings.beginGroup("CheckForUpdate");
	auto date = settings.value("Date").toString();	// check after 15 days.
	settings.endGroup();
	if (date == QDate::currentDate().toString("dd/MM/yyyy"))
	{
		checkForUpdate(false);
		return true;
	}
	return false;
}

void XrayCheckForUpdate::checkForUpdate(bool _atStartUp)
{
	auto downloader = new XrayQFileDownloader(m_url);
	connect(downloader, &XrayQFileDownloader::downloaded, [this, downloader, _atStartUp]()
	{
		if (downloader->saveToDisk(QDir::tempPath() + "/"))
		{
			auto fileInfo = QFileInfo(downloader->getSavedFileName());
			if (fileInfo.exists())
			{
				QSettings s(fileInfo.absoluteFilePath(), XrayXmlSettings::format);
				s.beginGroup("Application");
				auto changeLogFileLink = s.value("Changelog_File_URL").toString();
#ifdef _WIN64
				auto executableFileLink = s.value("Primary_Download_URL").toString();
#else
				auto executableFileLink = s.value("Secondary_Download_URL").toString();
#endif
				auto appName = s.value("Program_Name").toString();
				auto releaseStatus = s.value("Program_Release_Status").toString();
				auto releaseDate = s.value("Program_Release_Year").toString() + '-' + s.value("Program_Release_Month").toString() + '-' + s.value("Program_Release_Day").toString();
				auto version = s.value("Program_Version").toString();
				s.endGroup();
				auto currentVersion = QApplication::applicationVersion().remove('.').toInt();
				auto aVersion = version;
				auto avilableVersion = aVersion.remove('.').toInt();

				if (QApplication::applicationName() == appName && avilableVersion <= currentVersion)
				{
					#if _DEBUG
					qDebug() << QString("You are up to date! The current version %1 is the newest version.").arg(currentVersion);
					#endif // _DEBUG
					if (_atStartUp)
					{
						QMessageBox(QMessageBox::Information, windowTitle(),
							QApplication::translate("XrayCheckForUpdate", "You are up to date!\n\n%1 %2 is currently the newest version\navailable. Please check back again for updates at\na later time.").arg(QApplication::applicationName()).arg(QApplication::applicationVersion())).exec();
					}
				}
				else if (QApplication::applicationName() == appName && avilableVersion > currentVersion)
				{
					p_installedLabel->setText(QApplication::applicationVersion());
					p_availableLabel->setText(version);
					p_releaseDateLabel->setText(releaseDate);
					p_releaseStatusLabel->setText(releaseStatus);
					p_updateDownloader->setUrl(executableFileLink);

					// download the change log file.
					auto releaseNotesDownloader = new XrayQFileDownloader(QUrl(changeLogFileLink));
					connect(releaseNotesDownloader, &XrayQFileDownloader::downloaded, [this, releaseNotesDownloader]()
					{
						p_textBrowser->setHtml(releaseNotesDownloader->getData());
						p_textBrowser->update();
						p_releaseNotesGBox->setHidden(false);
						releaseNotesDownloader->deleteLater();

						if(isHidden())
							setHidden(false);

						emit updateAvailable();
					});
					QTimer::singleShot(0, releaseNotesDownloader, &XrayQFileDownloader::start);

					auto f = p_availableLabel->font();
					f.setBold(true);
					p_availableLabel->setFont(f);
				}
			}
		}
		downloader->deleteLater();
	});

	QTimer::singleShot(1000, downloader, &XrayQFileDownloader::start);
}

void XrayCheckForUpdate::start()
{
	checkForUpdate(true);
}
void XrayCheckForUpdate::launchDownloadedFile()
{
	if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayCheckForUpdate", "Do you want to close the application for the update process to complete?"), QMessageBox::Yes | QMessageBox::No).exec())
	{
		if (QDesktopServices::openUrl(QUrl(p_updateDownloader->getSavedFileName(), QUrl::TolerantMode)))
			XrayQtUtil::getMainWindow()->close();	// close the main window to update the new version.
	}
}

void XrayCheckForUpdate::closeEvent(QCloseEvent* _event)
{
	p_updateDownloader->abort();
	p_releaseNotesGBox->setHidden(true);
	p_downloadGBox->setHidden(true);

	_event->accept();
}