/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/02
** filename: 	XrayTcpServerMainWidget.cpp
** file base:	XrayTcpServerMainWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a client server interface.
****************************************************************************/

#include "XrayTcpClientMainWidget.h"
#include "XrayGlobal.h"

#include <QApplication>
#include <QtNetwork>
#include <QFileDialog>
#include <QDateTime>
#include <QGroupBox>
#include <QMessageBox>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayTcpClientMainWidget::XrayTcpClientMainWidget(QWidget* parent) :
	QMainWindow(parent)
{
	setWindowTitle("XClient Manager");
	setWindowIcon(QApplication::windowIcon());

	auto centeral = new QWidget(this);

	p_progressWidget = new XrayProgressWidget(this);
	p_progressWidget->setStyleSheet("QWidget { background-color: transparent; }");
	p_progressWidget->setMaximumWidth(320);
	connect(&m_thread, &XrayTcpClientThread::onStart, p_progressWidget, &XrayProgressWidget::onStart);
	connect(&m_thread, &XrayTcpClientThread::onProgress, p_progressWidget, &XrayProgressWidget::setCurrentProgress);
	connect(&m_thread, &XrayTcpClientThread::onFinished, p_progressWidget, &XrayProgressWidget::onFinish);
	connect(&m_thread, &XrayTcpClientThread::onFinished, this, &XrayTcpClientMainWidget::onFinish);
	connect(&m_thread, &XrayTcpClientThread::receivedFiles, this, &XrayTcpClientMainWidget::receivedFiles);
	connect(&m_thread, &XrayTcpClientThread::error, this, &XrayTcpClientMainWidget::displayError);

	p_serverIpLabel = new QLabel(QApplication::translate("XrayTcpClientMainWidget", "Name:"), this);
	p_serverIp = new QLineEdit("0.0.0.0", this);
	p_serverIpLabel->setBuddy(p_serverIp);

	p_serverPortLabel = new QLabel(QApplication::translate("XrayTcpClientMainWidget", "Port:"), this);
	p_serverPort = new QLineEdit("60000", this);
	p_serverPort->setValidator(new QIntValidator(1, 65535, this));
	p_serverPortLabel->setBuddy(p_serverPort);

	connect(p_serverIp, &QLineEdit::textChanged, this, &XrayTcpClientMainWidget::enableStartReceiving);
	connect(p_serverPort, &QLineEdit::textChanged, this, &XrayTcpClientMainWidget::enableStartReceiving);

	p_startReceiving = new QPushButton(QApplication::translate("XrayTcpClientMainWidget", "Run"), this);
	connect(p_startReceiving, &QPushButton::clicked, this, &XrayTcpClientMainWidget::startClient);


	auto serverGBox = new QGroupBox(QApplication::translate("XrayTcpClientMainWidget", "Server"), this);
	auto gridLayout = new QGridLayout;
	gridLayout->addWidget(p_serverIpLabel, 0, 0);
	gridLayout->addWidget(p_serverIp, 0, 1);
	gridLayout->addWidget(p_serverPortLabel, 1, 0);
	gridLayout->addWidget(p_serverPort, 1, 1);
	serverGBox->setLayout(gridLayout);

	p_timerLabel = new QLabel(QApplication::translate("XrayTcpClientMainWidget", "Update interval:"), this);
	p_timerValue = new QLineEdit("0", this);
	p_timerValue->setToolTip(QApplication::translate("XrayTcpClientMainWidget", "Specify the update time interval in seconds.\nTimer will be disabled on value 0."));
	p_timerValue->setValidator(new QIntValidator(0, 2592000, this));
	connect(&m_timer, &QTimer::timeout, this, &XrayTcpClientMainWidget::startClient);
	connect(p_timerValue, &QLineEdit::editingFinished, [this]()
	{
		auto value = p_timerValue->text().toInt();
		if (value == 0)
		{
			m_timer.stop();
		}
		else 
		{
			m_timer.setInterval(value * 1000);
			m_timer.start();
		}
	});
	p_timerLabel->setBuddy(p_timerValue);
	auto timerGBox = new QGroupBox(QApplication::translate("XrayTcpClientMainWidget", "Timer"), this);
	gridLayout = new QGridLayout;
	gridLayout->addWidget(p_timerLabel, 0, 0);
	gridLayout->addWidget(p_timerValue, 0, 1);
	timerGBox->setLayout(gridLayout);

	p_lastRecTime = new QLabel("", this);
	auto lastGBox = new QGroupBox(QApplication::translate("XrayTcpServerMainWidget", "Last received time"), this);
	gridLayout = new QGridLayout;
	gridLayout->addWidget(p_lastRecTime);
	lastGBox->setLayout(gridLayout);

	auto leftHLayout = new QVBoxLayout;
	leftHLayout->addWidget(serverGBox);
	leftHLayout->addWidget(timerGBox);
	leftHLayout->addWidget(lastGBox);
	leftHLayout->addStretch(1);
	leftHLayout->addWidget(p_startReceiving);

	p_listWidget = new XrayQListWidget(centeral);

	p_browsDir = new QPushButton(QApplication::translate("XrayTcpClientMainWidget", "Browse directory"), this);
	p_browsDir->setToolTip(QApplication::translate("XrayTcpClientMainWidget", "Select directory for the received files."));
	connect(p_browsDir, &QPushButton::clicked, this, &XrayTcpClientMainWidget::browseDir);
	p_dirPath = new QLineEdit(this);
	p_dirPath->setToolTip(QApplication::translate("XrayTcpClientMainWidget", "Specify directory for the received files."));

	auto listGBox = new QGroupBox(QApplication::translate("XrayTcpClientMainWidget", "Received files"), this);
	auto rightLayout = new QGridLayout;
	rightLayout->addWidget(p_listWidget, 0, 0, 1, 2);
	rightLayout->addWidget(p_dirPath, 1, 0);
	rightLayout->addWidget(p_browsDir, 1, 1);
	listGBox->setLayout(rightLayout);

	gridLayout = new QGridLayout;
	gridLayout->addLayout(leftHLayout, 0, 0);
	gridLayout->addWidget(listGBox, 0, 1);

	centeral->setLayout(gridLayout);
	setCentralWidget(centeral);

	p_blinker = new XrayQLedBlink(this);
	p_blinker->setOnColor(XrayQLed::ledColor::Yellow);
	p_blinker->setOffColor(XrayQLed::ledColor::Grey);
	p_blinker->setValue(true);
	p_blinker->setInterval(1000);
	p_blinker->setFixedSize(20, 20);
	p_blinker->setToolTip(QApplication::translate("XrayTcpClientMainWidget", "Reading to receive!"));

	p_statusBar = new XrayStatusBar(this);
	p_statusBar->setSizeGripEnabled(false);
	p_statusBar->setContentsMargins(5, 0, 5, 0);
	p_statusBar->setStyleSheet("QStatusBar { font-size: 11px; font-weight: normal; color: rgb(200, 200, 200); } QLabel { font-size: 11px; font-weight: normal; color: rgb(200, 200, 200); }");
	p_statusBar->addPermanentWidget(p_progressWidget);
	p_statusBar->insertWidget(0, p_blinker);
	p_statusBar->showMessage(QApplication::translate("XrayTcpClientMainWidget", "Ready"));

	setStatusBar(p_statusBar);
}
XrayTcpClientMainWidget::~XrayTcpClientMainWidget()
{
}

void XrayTcpClientMainWidget::closeEvent(QCloseEvent* _event)
{
	//if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayMainWindow", "Are you sure you want to quit?"), QMessageBox::Yes | QMessageBox::No).exec())
	//{
	_event->accept();
	return;
	//}
	_event->ignore();
}

void XrayTcpClientMainWidget::startClient()
{
	if (QFile::exists(p_dirPath->text()))
	{
		m_thread.start(p_serverIp->text(), p_serverPort->text().toInt(), p_dirPath->text());

		p_startReceiving->setEnabled(false);

		p_blinker->setOnColor(XrayQLed::ledColor::Green);
		p_blinker->setToolTip(QApplication::translate("XrayTcpClientMainWidget", "Receiving..."));
		p_blinker->start();

		p_statusBar->showMessage(QApplication::translate("XrayTcpClientMainWidget", "Receiving..."));
	}
	else
	{
		p_statusBar->showMessage(QApplication::translate("XrayTcpClientMainWidget", "Specified directory doesn't exists!"));
	}
}
void XrayTcpClientMainWidget::stopServer()
{
}

void XrayTcpClientMainWidget::browseDir()
{
	auto dir = QFileDialog::getExistingDirectory(this, QApplication::translate("XrayTcpClientMainWidget", "Select Directory"), XrayGlobal::getLastFileOpenPath());
	if (dir.isEmpty())
		return;
	
	p_dirPath->setText(dir);
}

void XrayTcpClientMainWidget::receivedFiles(const QVector<QString>& _files)
{
	QSet<QString> files;
	files.reserve(_files.size() + p_listWidget->count());

	for (auto i = 0; i < p_listWidget->count(); i++)
		files.insert(p_listWidget->item(i)->text());

	for (const auto& i : _files)
		files.insert(i);

	p_listWidget->clear();
	p_listWidget->addItems(files.toList());

	p_startReceiving->setEnabled(true);
}

void XrayTcpClientMainWidget::enableStartReceiving()
{
	auto enable = (!p_serverIp->text().isEmpty() && !p_serverPort->text().isEmpty());
	p_startReceiving->setEnabled(enable);
}
void XrayTcpClientMainWidget::displayError(int _socketError, const QString& _message)
{
	switch (_socketError)
	{
	case QAbstractSocket::HostNotFoundError:
		QMessageBox::information(this, windowTitle(), QApplication::translate("XrayTcpClientMainWidget", "Host was not found. Make sure the host name and port are correct."));
		break;
	case QAbstractSocket::ConnectionRefusedError:
		QMessageBox::information(this, windowTitle(), QApplication::translate("XrayTcpClientMainWidget", "The connection was refused by the peer. Make sure the server is running and host name and port are correct."));
		break;
	default:
		QMessageBox::information(this, windowTitle(), QApplication::translate("XrayTcpClientMainWidget", "Following error occurred: %1.").arg(_message));
	}

	p_blinker->setOnColor(XrayQLed::ledColor::Red);
	p_blinker->setToolTip(QApplication::translate("XrayTcpClientMainWidget", "Error"));
	p_blinker->start();

	p_startReceiving->setEnabled(true);
	p_statusBar->showMessage(QApplication::translate("XrayTcpClientMainWidget", "Error"));
	p_progressWidget->onFinish();
}

void XrayTcpClientMainWidget::onFinish()
{
	p_lastRecTime->setText(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss"));
	p_statusBar->showMessage(QApplication::translate("XrayTcpClientMainWidget", "Ready"));

	p_blinker->setOnColor(XrayQLed::ledColor::Yellow);
	p_blinker->setToolTip(QApplication::translate("XrayTcpClientMainWidget", "Ready to receive!"));
	p_blinker->start();
}

void XrayTcpClientMainWidget::saveSettings(QSettings& _settings)
{
	QStringList files;
	files.reserve(p_listWidget->count());
	for (auto i = 0; i < p_listWidget->count(); i++)
		files.push_back(p_listWidget->item(i)->text());

	_settings.beginGroup("TcpClientReceiver");
	_settings.setValue("client/host", p_serverIp->text());
	_settings.setValue("client/port", p_serverPort->text());
	_settings.setValue("client/interval", p_timerValue->text());
	_settings.setValue("client/lastReceivedTime", p_lastRecTime->text());
	_settings.setValue("client/dir", p_dirPath->text());
	_settings.setValue("client/files", files);
	_settings.endGroup();
}
void XrayTcpClientMainWidget::restoreSettings(QSettings& _settings)
{
	QString ipAddress;
	auto ipAddressesList = QNetworkInterface::allAddresses();
	for (auto i = 0; i < ipAddressesList.size(); ++i)
	{
		if (ipAddressesList.at(i) != QHostAddress::LocalHost && ipAddressesList.at(i).toIPv4Address())	// use the first non-localhost IPv4 address
		{
			ipAddress = ipAddressesList.at(i).toString();
			break;
		}
	}
	if (ipAddress.isEmpty())
		ipAddress = QHostAddress(QHostAddress::LocalHost).toString();	// if we did not find one, use IPv4 localhost

	_settings.beginGroup("TcpClientReceiver");
	p_serverIp->setText(_settings.value("client/host", ipAddress).toString());
	p_serverPort->setText(_settings.value("client/port", p_serverPort->text()).toString());
	p_timerValue->setText(_settings.value("client/interval", p_timerValue->text()).toString());
	p_lastRecTime->setText(_settings.value("client/lastReceivedTime", p_lastRecTime->text()).toString());
	p_dirPath->setText(_settings.value("client/dir", p_dirPath->text()).toString());
	p_listWidget->clear();
	p_listWidget->addItems(_settings.value("client/files").toStringList());
	_settings.endGroup();
}