/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/02
** filename: 	XrayTcpServerMainWidget.cpp
** file base:	XrayTcpServerMainWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a client server interface.
****************************************************************************/

#include "XrayTcpServerMainWidget.h"
#include "XrayGlobal.h"

#include <QApplication>
#include <QtNetwork>
#include <QFileDialog>
#include <QDateTime>
#include <QDebug>
#include <QMessageBox>
#include <QGroupBox>

XRAYLAB_USING_NAMESPACE

XrayTcpServerMainWidget::XrayTcpServerMainWidget(QWidget* parent) :
	QMainWindow(parent),
	p_server(new XrayTcpServer)
{
	setWindowTitle("XServer Manager");
	setWindowIcon(QApplication::windowIcon());

	auto centeral = new QWidget(this);

	p_progressWidget = new XrayProgressWidget(this);
	p_progressWidget->setStyleSheet("QWidget { background-color: transparent; }");
	p_progressWidget->setMaximumWidth(320);
	connect(p_server, &XrayTcpServer::onStart, p_progressWidget, &XrayProgressWidget::onStart);
	connect(p_server, &XrayTcpServer::onProgress, p_progressWidget, &XrayProgressWidget::setCurrentProgress);
	connect(p_server, &XrayTcpServer::onFinished, p_progressWidget, &XrayProgressWidget::onFinish);
	connect(p_server, &XrayTcpServer::onFinished, this, &XrayTcpServerMainWidget::onFinish);

	p_serverIpLabel = new QLabel(QApplication::translate("XrayTcpServerMainWidget", "Name:"), this);
	p_serverIp = new QLineEdit("0.0.0.0", this);
	p_serverIpLabel->setBuddy(p_serverIp);

	p_serverPortLabel = new QLabel(QApplication::translate("XrayTcpServerMainWidget", "Port:"), this);
	p_serverPort = new QLineEdit("60000", this);
	p_serverPort->setValidator(new QIntValidator(1, 65535, this));
	p_serverPortLabel->setBuddy(p_serverPort);

	p_serverConnect = new QPushButton(QApplication::translate("XrayTcpServerMainWidget", "Run"), this);
	connect(p_serverConnect, &QPushButton::clicked, this, &XrayTcpServerMainWidget::runServer);
	p_serverDisconnect = new QPushButton(QApplication::translate("XrayTcpServerMainWidget", "Stop"), this);
	connect(p_serverDisconnect, &QPushButton::clicked, this, &XrayTcpServerMainWidget::stopServer);

	auto lastGBox = new QGroupBox(QApplication::translate("XrayTcpServerMainWidget", "Last sent time"), this);
	p_lastSentTime = new QLabel("", this);
	auto gridLayout = new QGridLayout;
	gridLayout->addWidget(p_lastSentTime);
	lastGBox->setLayout(gridLayout);

	auto serverGBox = new QGroupBox(QApplication::translate("XrayTcpServerMainWidget", "Host"), this);
	auto leftLayout = new QGridLayout;
	leftLayout->addWidget(p_serverIpLabel, 0, 0);
	leftLayout->addWidget(p_serverIp, 0, 1);
	leftLayout->addWidget(p_serverPortLabel, 1, 0);
	leftLayout->addWidget(p_serverPort, 1, 1);
	serverGBox->setLayout(leftLayout);

	auto leftHLayout = new QVBoxLayout;
	leftHLayout->addWidget(serverGBox);
	leftHLayout->addWidget(lastGBox);
	leftHLayout->addStretch(1);
	leftHLayout->addWidget(p_serverConnect);
	leftHLayout->addWidget(p_serverDisconnect);

	p_listWidget = new XrayQListWidget(centeral);
	connect(p_listWidget, &XrayQListWidget::removed, this, &XrayTcpServerMainWidget::updateList);

	p_browsFiles = new QPushButton(QApplication::translate("XrayTcpServerMainWidget", "Add Files"), this);
	p_browsFiles->setToolTip(QApplication::translate("XrayTcpServerMainWidget", "Browse and select files that needs to be sent."));
	connect(p_browsFiles, &QPushButton::clicked, this, &XrayTcpServerMainWidget::browseFiles);

	auto listGBox = new QGroupBox(QApplication::translate("XrayTcpServerMainWidget", "Sent files"), this);
	auto rightLayout = new QGridLayout;
	rightLayout->addWidget(p_listWidget);
	rightLayout->addWidget(p_browsFiles);
	listGBox->setLayout(rightLayout);

	gridLayout = new QGridLayout;
	gridLayout->addLayout(leftHLayout, 0, 0);
	gridLayout->addWidget(listGBox, 0, 1);

	centeral->setLayout(gridLayout);
	setCentralWidget(centeral);

	p_blinker = new XrayQLedBlink(this);
	p_blinker->setOnColor(XrayQLed::ledColor::Red);
	p_blinker->setOffColor(XrayQLed::ledColor::Grey);
	p_blinker->setValue(true);
	p_blinker->setInterval(1000);
	p_blinker->setFixedSize(20, 20);
	p_blinker->setToolTip(QApplication::translate("XrayTcpServerMainWidget", "Not running"));

	p_statusBar = new XrayStatusBar(this);
	p_statusBar->setSizeGripEnabled(false);
	p_statusBar->setContentsMargins(5, 0, 5, 0);
	p_statusBar->setStyleSheet("QStatusBar { font-size: 11px; font-weight: normal; color: rgb(200, 200, 200); } QLabel { font-size: 11px; font-weight: normal; color: rgb(200, 200, 200); }");
	p_statusBar->addPermanentWidget(p_progressWidget);
	p_statusBar->insertWidget(0, p_blinker);
	p_statusBar->showMessage(QApplication::translate("XrayTcpServerMainWidget", "Not running"));

	setStatusBar(p_statusBar);
}
XrayTcpServerMainWidget::~XrayTcpServerMainWidget()
{
	p_server->close();
	p_server->deleteLater();
}

void XrayTcpServerMainWidget::closeEvent(QCloseEvent* _event)
{
	//if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayMainWindow", "Are you sure you want to quit?"), QMessageBox::Yes | QMessageBox::No).exec())
	//{
	_event->accept();
	return;
	//}
	_event->ignore();
}

void XrayTcpServerMainWidget::runServer()
{
	if (p_server->isListening())
		p_server->close();

	if (p_listWidget->count() == 0)
	{
		p_statusBar->showMessage(QApplication::translate("XrayTcpServerMainWidget", "Couldn't find any file in the list."));
		p_blinker->setOnColor(XrayQLed::ledColor::Red);
		p_blinker->setToolTip(QApplication::translate("XrayTcpServerMainWidget", "Not running"));
		p_blinker->start();
		return;
	}

	updateList({});

	QHostAddress add;
	add.setAddress(p_serverIp->text());

	if (!p_server->listen(add, static_cast<quint16>(p_serverPort->text().toUShort())))
	{
		QMessageBox::critical(this, windowTitle(), QApplication::translate("XrayTcpServerMainWidget", "Unable to start the server: %1.").arg(p_server->errorString()));
		p_statusBar->showMessage(QApplication::translate("XrayTcpServerMainWidget", "Not running"));
		p_blinker->setOnColor(XrayQLed::ledColor::Red);
		p_blinker->setToolTip(QApplication::translate("XrayTcpServerMainWidget", "Not running"));
		p_blinker->start();
	}
	else
	{
		p_serverIpLabel->setEnabled(false);
		p_serverPortLabel->setEnabled(false);
		p_serverIp->setEnabled(false);
		p_serverPort->setEnabled(false);
		p_serverConnect->setEnabled(false);

		p_statusBar->showMessage(QApplication::translate("XrayTcpServerMainWidget", "The server is running on IP: %1 port: %2").arg(add.toString()).arg(p_server->serverPort()));
		p_blinker->setOnColor(XrayQLed::ledColor::Green);
		p_blinker->setToolTip(QApplication::translate("XrayTcpServerMainWidget", "Running"));
		p_blinker->start();
	}
}
void XrayTcpServerMainWidget::stopServer()
{
	if (p_server->isListening())
	{
		p_server->close();
		p_statusBar->showMessage(QApplication::translate("XrayTcpServerMainWidget", "Not running"));
		p_blinker->setOnColor(XrayQLed::ledColor::Red);
		p_blinker->setToolTip(QApplication::translate("XrayTcpServerMainWidget", "Not running"));
		p_blinker->start();

		p_serverIpLabel->setEnabled(true);
		p_serverPortLabel->setEnabled(true);
		p_serverIp->setEnabled(true);
		p_serverPort->setEnabled(true);
		p_serverConnect->setEnabled(true);

		p_progressWidget->onFinish();
	}
}

void XrayTcpServerMainWidget::browseFiles()
{
	auto fileNames = QFileDialog::getOpenFileNames(this, QApplication::translate("XrayTcpServerMainWidget", "Select Files"), XrayGlobal::getLastFileOpenPath(), QString(), nullptr);
	if (fileNames.isEmpty())
		return;

	// no duplicates are allowed.
	QSet<QString> files;
	files.reserve(fileNames.size() + p_listWidget->count());

	for (auto i = 0; i < p_listWidget->count(); i++)
		files.insert(p_listWidget->item(i)->text());

	for (const auto& i : fileNames)
		files.insert(i);

	p_listWidget->clear();
	p_listWidget->addItems(files.toList());
	p_server->setFiles(files.toList());
}
void XrayTcpServerMainWidget::updateList(const std::vector<int>& _removedRows)
{
	Q_UNUSED(_removedRows);

	// no duplicates are allowed.
	QSet<QString> files;
	files.reserve(p_listWidget->count());
	for (auto i = 0; i < p_listWidget->count(); i++)
		files.insert(p_listWidget->item(i)->text());
	p_server->setFiles(files.toList());
}

void XrayTcpServerMainWidget::onFinish()
{
	p_lastSentTime->setText(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss"));
	p_statusBar->showMessage(QApplication::translate("XrayTcpServerMainWidget", "Running"));
}

void XrayTcpServerMainWidget::saveSettings(QSettings& _settings)
{
	QStringList files;
	files.reserve(p_listWidget->count());
	for (auto i = 0; i < p_listWidget->count(); i++)
		files.push_back(p_listWidget->item(i)->text());

	_settings.beginGroup("TcpServerListener");
	_settings.setValue("server/host", p_serverIp->text());
	_settings.setValue("server/port", p_serverPort->text());
	_settings.setValue("server/lastSentTime", p_lastSentTime->text());
	_settings.setValue("server/files", files);
	_settings.endGroup();
}
void XrayTcpServerMainWidget::restoreSettings(QSettings& _settings)
{
	QString ipAddress;
	auto ipAddressesList = QNetworkInterface::allAddresses();
	for (auto i = 0; i < ipAddressesList.size(); ++i)
	{
		if (ipAddressesList.at(i) != QHostAddress::LocalHost && ipAddressesList.at(i).toIPv4Address())	// use the first non-localhost IPv4 address
		{
			ipAddress = ipAddressesList.at(i).toString();
			break;
		}
	}
	if (ipAddress.isEmpty())
		ipAddress = QHostAddress(QHostAddress::LocalHost).toString();	// if we did not find one, use IPv4 localhost

	_settings.beginGroup("TcpServerListener");
	p_serverIp->setText(_settings.value("server/host", ipAddress).toString());
	p_serverPort->setText(_settings.value("server/port", p_serverPort->text()).toString());
	p_lastSentTime->setText(_settings.value("server/lastSentTime", p_lastSentTime->text()).toString());
	auto files = _settings.value("server/files").toStringList();
	p_listWidget->clear();
	p_listWidget->addItems(files);
	_settings.endGroup();
}