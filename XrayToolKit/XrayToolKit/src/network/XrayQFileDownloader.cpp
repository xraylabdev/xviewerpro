﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/04
** filename: 	XrayQFileDownloader.cpp
** file base:	XrayQFileDownloader
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a file downloader.
****************************************************************************/

#include "XrayQFileDownloader.h"

#include <QApplication>
#include <QAuthenticator>
#include <QFile>
#include <QFileInfo>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayQFileDownloader::XrayQFileDownloader(QObject* _parent) :
	QObject(_parent)
{
}
XrayQFileDownloader::XrayQFileDownloader(const QUrl& _url, QObject* _parent) :
	QObject(_parent),
	m_url(_url)
{
}

void XrayQFileDownloader::setUrl(const QUrl& _url)
{
	m_url = _url;
}
QUrl XrayQFileDownloader::getUrl() const
{
	return m_url;
}
void XrayQFileDownloader::setAuthentication(const QPair<QString, QString>& _username_password)
{ 
	m_authenticator = _username_password;
}
QPair<QString, QString> XrayQFileDownloader::getAuthentication() const
{ 
	return m_authenticator; 
}
QString XrayQFileDownloader::getData() const
{
	return m_data;
}

void XrayQFileDownloader::start()
{
	connect(&m_networkAccessManager, &QNetworkAccessManager::finished, this, &XrayQFileDownloader::finished);

	QNetworkRequest request(m_url);
	auto reply = m_networkAccessManager.get(request);

	connect(reply, &QNetworkReply::downloadProgress, this, &XrayQFileDownloader::downloadProgress);

	connect(&m_networkAccessManager, &QNetworkAccessManager::authenticationRequired, reply, [this, reply](QNetworkReply* _reply, QAuthenticator* _authenticator) -> void 
	{
		if (_reply != reply)
			return;

		_authenticator->setUser(m_authenticator.first);
		_authenticator->setPassword(m_authenticator.second);
	});

	emit startDownload();
}

void XrayQFileDownloader::finished(QNetworkReply* _reply)
{
	auto url = _reply->url();
	if (_reply->error())
	{
#ifdef _DEBUG
		qDebug() << QString("Download of %1 failed: %2").arg(url.toEncoded().constData()).arg(_reply->errorString());
#endif // _DEBUG
		emit downloadError(_reply->errorString());
	}
	else
	{
		auto statusCode = _reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
		if (statusCode == 301 || statusCode == 302 || statusCode == 303
			|| statusCode == 305 || statusCode == 307 || statusCode == 308)
		{
#ifdef _DEBUG
			qDebug() << QString("Download of %1 failed: Request was redirected.").arg(url.toEncoded().constData());
#endif // _DEBUG
		}
		else
		{
			m_data = _reply->readAll();
			_reply->deleteLater();

#ifdef _DEBUG
			qDebug() << QString("Download of %1 succeeded.").arg(url.toEncoded().constData());
#endif // _DEBUG

			emit downloaded();
		}
	}
}

static QString saveFileName_(const QUrl& _url)
{
	auto path = _url.path();
	QString basename;

	if (QFileInfo(path).fileName().isEmpty())
		basename = QApplication::applicationName() + QFileInfo(path).suffix();
	else
		basename = QFileInfo(path).fileName();

	if (QFile::exists(basename))
	{
		// already exists, don't overwrite.

		auto ext = QFileInfo(path).suffix();
		auto name = QFileInfo(path).baseName();

		auto i = 0;
		name += '.';
		while (QFile::exists(name + QString::number(i) + '.' + ext))
			++i;

		name += QString::number(i) + '.' + ext;

		basename = name;
	}

	return basename;
}

bool XrayQFileDownloader::saveToDisk(const QString& _dirPath)
{
	if (m_data.isEmpty() || _dirPath.isEmpty())
		return false;

	QString path;
	if (_dirPath.endsWith('/'))
		path = _dirPath + saveFileName_(m_url);
	else
		path = _dirPath + "/" + saveFileName_(m_url);

	QFile file(path);
	if (!file.open(QIODevice::WriteOnly)) 
	{
#ifdef _DEBUG
		qDebug() << QString("Couldn't open %1 for writing: %2").arg(path).arg(file.errorString());
#endif // _DEBUG
		return false;
	}

	file.write(m_data);
	file.close();
	m_savedFile = path;

	return true;
}

QString XrayQFileDownloader::getSavedFileName() const
{
	return m_savedFile;
}
