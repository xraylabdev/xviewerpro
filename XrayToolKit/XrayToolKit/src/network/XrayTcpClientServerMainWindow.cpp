/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/02
** filename: 	XrayTcpClientServerMainWindow.cpp
** file base:	XrayTcpClientServerMainWindow
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the file system monitoring window for
				specified folders.
****************************************************************************/

#include "XrayTcpClientServerMainWindow.h"
#include "ui_XrayAboutDialog.h"
#include "XrayIconPushButton.h"
#include "XrayGlobal.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QDesktopServices>
#include <QMessageBox>
#include <QSettings>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayTcpClientServerMainWindow::XrayTcpClientServerMainWindow(const QString& _title, QWidget* _parent) :
	XrayFramelessWindowWin32(_title, _parent),
	p_centeralTabWidget(new QTabWidget(this)),
	p_server(new XrayTcpServerMainWidget(p_centeralTabWidget)),
	p_client(new XrayTcpClientMainWidget(p_centeralTabWidget))
{
	setWindowIcon(QApplication::windowIcon());

	p_menu = createMenu();
	menuButton()->setMenu(p_menu);
	menuButton()->setVisible(true);

	p_centeralTabWidget->addTab(p_client, QApplication::translate("XrayTcpClientServerMainWindow", "Client"));
	p_centeralTabWidget->addTab(p_server, QApplication::translate("XrayTcpClientServerMainWindow", "Server"));
	addCentralWidget(p_centeralTabWidget);

	const auto geometry = QApplication::desktop()->screenGeometry(this);
	resize(geometry.width() / 1.1, geometry.height() / 1.1);
	move((geometry.width() / 1.1 - width()) / 2, (geometry.height() / 1.1 - height()) / 2);
	restoreSettings();

	QSettings settings(QApplication::organizationName(), windowTitle());
	p_client->restoreSettings(settings);
	p_server->restoreSettings(settings);
}
XrayTcpClientServerMainWindow::~XrayTcpClientServerMainWindow()
{
	p_server->deleteLater();
	p_client->deleteLater();
}
void XrayTcpClientServerMainWindow::closeEvent(QCloseEvent* _event)
{
	QSettings settings(QApplication::organizationName(), windowTitle());
	p_client->saveSettings(settings);
	p_server->saveSettings(settings);
	saveSettings();

	_event->accept();
}

QMenu* XrayTcpClientServerMainWindow::createMenu()
{
	auto menu = new QMenu;

	p_actionViewHelp = new QAction(QIcon(":/res/images/help_blue_icon.png"), QApplication::translate("XrayTcpClientServerMainWindow", "View Help"), this);
	connect(p_actionViewHelp, &QAction::triggered, [this]() {});
	menu->addAction(p_actionViewHelp);

	p_actionWebsite = new QAction(QIcon(":/res/images/website_blue_icon.png"), QApplication::translate("XrayTcpClientServerMainWindow", "Website"), this);
	connect(p_actionWebsite, &QAction::triggered, [this]() { QDesktopServices::openUrl(QUrl("https://xray-lab.com/")); });
	menu->addAction(p_actionWebsite);

	menu->addSeparator();
	p_actionAbout = new QAction(QIcon(":/res/images/about_blue_icon.png"), QApplication::translate("XrayTcpClientServerMainWindow", "About"), this);
	connect(p_actionAbout, &QAction::triggered, [this]()
	{
		QDialog dialog;
		dialog.setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint);
		dialog.setWindowIcon(QApplication::windowIcon());
		dialog.setFixedSize(650, 480);
		Ui::XrayAboutDialog d;
		d.setupUi(&dialog);
		d.applicationTitle->setText(QApplication::applicationName() + " v" + QApplication::applicationVersion() + " (64 bit)");
		d.releaseDate->setText("15.04.2019");
		d.moduleName->setText("Client-Server Manager");
		dialog.setStyleSheet(QString::fromUtf8("QDialog { background: rgb(70, 70, 70); border:1px solid rgb(21, 142, 21); } QLabel { color: rgb(240, 240, 240); }"));
		dialog.exec();
	});
	menu->addAction(p_actionAbout);

	return menu;
}