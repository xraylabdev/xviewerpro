/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/10
** filename: 	XrayTcpServer.cpp
** file base:	XrayTcpServer
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a threaded-based server side that
send multiple files data to the client side.
****************************************************************************/

#include "XrayTcpServer.h"
#include "XrayTcpServerThread.h"

XRAYLAB_USING_NAMESPACE

XrayTcpServer::XrayTcpServer(const QStringList& _files, QObject* _parent) :
	QTcpServer(_parent),
	m_projectDir(_files)
{
}

void XrayTcpServer::clear()
{
	m_projectDir.clear();
}
void XrayTcpServer::addFile(const QString& _fileName)
{
	m_projectDir.push_back(_fileName);
}
void XrayTcpServer::setFiles(const QStringList& _files)
{
	m_projectDir = _files;
}
QStringList XrayTcpServer::getFiles() const
{
	return m_projectDir;
}

void XrayTcpServer::incomingConnection(qintptr _socketDescriptor)
{
	auto thread = new XrayTcpServerThread(_socketDescriptor, m_projectDir, this);
	connect(thread, &XrayTcpServerThread::onStart, this, &XrayTcpServer::onStart);
	connect(thread, &XrayTcpServerThread::onProgress, this, &XrayTcpServer::onProgress);
	connect(thread, &XrayTcpServerThread::onFinished, this, &XrayTcpServer::onFinished);
	connect(thread, &XrayTcpServerThread::finished, thread, &XrayTcpServerThread::deleteLater);
	thread->start();
}
