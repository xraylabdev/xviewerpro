/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/22
** filename: 	XrayLicenseCreatorWidget.cpp
** file base:	XrayLicenseCreatorWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a user registration widget with SQLITE
*				database connection.
****************************************************************************/

#include "XrayLicenseCreatorWidget.h"
#include "ui_XrayLicenseCreatorWidget.h"
#include "fvkLicenseEncoderServer.h"
#include "fvkLicenseDecoderServer.h"
#include "fvkLicenseDecoderClient.h"

#include "XrayLicenseGlobals.h"
#include "XrayGlobal.h"

#include <QDesktopServices>
#include <QTableWidget>
#include <QHeaderView>
#include <QMessageBox>
#include <QFileDialog>
#include <QDate>
#include <QDebug>

XRAYLAB_USING_NAMESPACE
using namespace R3D;

XrayLicenseCreatorWidget::XrayLicenseCreatorWidget(const QString& _winTitle, const QString& _engineName, QWidget* _parent) :
	QWidget(_parent),
	ui(new Ui::XrayLicenseCreatorWidget)
{
	ui->setupUi(this);
	ui->xreportCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XREPORT PRO", nullptr));
	ui->xquickReportCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XQUICKREPORT PRO", nullptr));
	ui->userManCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XUSERPANEL", nullptr));
	ui->xpaintCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XPAINT PRO", nullptr));
	ui->xenhancerCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XENHANCER PRO", nullptr));
	ui->xvoidCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XVOID PRO", nullptr));
	ui->xfilesCleanerCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XFILESCLEANER PRO", nullptr));
	ui->ximgStitcherCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XIMAGESTITCHER PRO", nullptr));
	ui->xwatermarkCB->setText(QCoreApplication::translate("XrayLicenseCreatorWidget", "XWATERMARK PRO", nullptr));

	//ui->label_9->hide();
	//ui->xreportCB->hide();
	//ui->userManCB->hide();
	//ui->xpaintCB->hide();
	//ui->xenhancerCB->hide();
	//ui->xvoidCB->hide();
	//ui->xfilesCleanerCB->hide();
	//ui->ximgStitcherCB->hide();
	//ui->gridLayout->removeItem(ui->horizontalLayout);

	setWindowTitle(_winTitle);

	ui->expireDate->setDisplayFormat("dd/MM/yyyy");
	ui->issueDate->setDisplayFormat("dd/MM/yyyy");
	ui->expireDate->setDate(QDate::currentDate().addDays(30));
	ui->issueDate->setDate(QDate::currentDate());

	XrayLicenseGlobals::setEngineName(_engineName);

	QSettings settings(QApplication::organizationName(), windowTitle());
	readSettings(settings);

	connect(ui->btnSave, &QPushButton::clicked, [this]()
	{
		QString appName;
		if (QFile::exists(ui->licenseFileEdit->text()))
		{
			auto texts = ui->licenseFileEdit->text().split('_');
			if (!texts.empty())
				appName = texts[0] + "_";
		}
		else
		{
			QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Please browse and select the customer's hardware key file!")).exec();
			return;
		}

		if (ui->licenseIdEdit->text().isEmpty())
		{
			QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Please specify the license id!")).exec();
			return;
		}

		if (ui->serialEdit->text().isEmpty())
		{
			QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Please specify the serial number!")).exec();
			return;
		}

		auto expire = QDate::fromString(ui->expireDate->text(), ui->expireDate->displayFormat());
		auto issue = QDate::fromString(ui->issueDate->text(), ui->issueDate->displayFormat());
		if(issue.daysTo(expire) == 0)
		{
			QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Please check the expire date. Is it correct?")).exec();
			return;
		}

		auto name = QString(appName + ui->licenseIdEdit->text() + "_" + ui->serialEdit->text() + "_" + ui->typeCombo->currentText() + ".dat").remove(' ');

		auto fileName = QFileDialog::getSaveFileName(this, QApplication::translate("XrayLicenseCreatorWidget", "Save File As..."), XrayGlobal::getLastFileOpenPath() + "/" + name, QApplication::translate("XrayLicenseCreatorWidget", "All Supported Files (*.dat)"), nullptr, XrayGlobal::isCurrentAppLanguageEnglish() ? QFileDialog::Options() : QFileDialog::DontUseNativeDialog);
		if (!fileName.isEmpty())
		{
			QString modules;
			if (ui->xreportCB->isChecked())
				modules += ui->xreportCB->text() + ',';
			if (ui->xquickReportCB->isChecked())
				modules += ui->xquickReportCB->text() + ',';
			if (ui->xvoidCB->isChecked())
				modules += ui->xvoidCB->text() + ',';
			if (ui->xenhancerCB->isChecked())
				modules += ui->xenhancerCB->text() + ',';
			if (ui->userManCB->isChecked())
				modules += ui->userManCB->text() + ',';
			if (ui->xpaintCB->isChecked())
				modules += ui->xpaintCB->text() + ',';
			if (ui->xfilesCleanerCB->isChecked())
				modules += ui->xfilesCleanerCB->text() + ',';
			if (ui->ximgStitcherCB->isChecked())
				modules += ui->ximgStitcherCB->text() + ',';
			if (ui->xwatermarkCB->isChecked())
				modules += ui->xwatermarkCB->text() + ',';

			fvkLicenseEncoderServer serverEncoder;
			serverEncoder.setKey(XrayLicenseGlobals::encryptionKey());
			serverEncoder.setIdentifier(
				HardwareIdentifier::ProcessorName |
				HardwareIdentifier::ProcessorId |
				HardwareIdentifier::BaseBoardProduct |
				HardwareIdentifier::BaseBoardManufacturer);
			serverEncoder.setLicenseId(ui->licenseIdEdit->text());
			serverEncoder.setSerial(ui->serialEdit->text());
			serverEncoder.setExpireDate(ui->expireDate->text());
			serverEncoder.setIssueDate(ui->issueDate->text());
			serverEncoder.setLicenseType(ui->typeCombo->currentText());
			serverEncoder.setModules(modules);
			serverEncoder.setInputFileLocation(ui->licenseFileEdit->text());
			serverEncoder.setOutputFileLocation(fileName);

			auto r = serverEncoder.encode();
			if(r == 0)
				QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Couldn't open the specified customer license file!")).exec();
			else if (r == -1)
				QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Couldn't decode the specified customer license file!")).exec();
			else if (r == -2)
				QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "The specified file is not a valid license file!")).exec();
			else if (r == -3)
				QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "The decoded key is not a valid key!")).exec();
			else if (r == -4)
				QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Couldn't encode!")).exec();
			else if (r == -5)
				QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Couldn't encrypt!")).exec();
			else if (r == 1)
			{
				if(!serverEncoder.write())
					QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Couldn't save license file to the disk!")).exec();
				else
					QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "License file has been saved successfully!")).exec();
			}
			else
			{
				QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Couldn't encode the customer license key!")).exec();
			}

			XrayGlobal::setLastFileOpenPath(fileName);
		}
	});

	connect(ui->btnBrowse, &QPushButton::clicked, [this]()
	{
		auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayLicenseCreatorWidget", "Select File"), XrayGlobal::getLastFileOpenPath(), QApplication::translate("XrayLicenseCreatorWidget", "All Supported Files (*.dat)"), nullptr, XrayGlobal::isCurrentAppLanguageEnglish() ? QFileDialog::Options() : QFileDialog::DontUseNativeDialog);
		if (!fileName.isEmpty())
		{
			ui->licenseFileEdit->setText(fileName);
			XrayGlobal::setLastFileOpenPath(fileName);
		}
	});

	connect(ui->btnKeyInfo, &QPushButton::clicked, [this]()
	{
		QString appName;
		if (QFile::exists(ui->licenseFileEdit->text()))
		{
			fvkLicenseDecoderClient decoder;
			decoder.setIdentifier(
				HardwareIdentifier::ProcessorName |
				HardwareIdentifier::ProcessorId |
				HardwareIdentifier::BaseBoardProduct |
				HardwareIdentifier::BaseBoardManufacturer);

			decoder.setKey(XrayLicenseGlobals::encryptionKey());
			decoder.setInputFileLocation(ui->licenseFileEdit->text());
			if (!decoder.read())
			{
				QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Couldn't open the specified hardware key file!")).exec();
				return;
			}

			if (decoder.decode() != 1)
			{
				QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Couldn't decode the specified hardware key file!")).exec();
				return;
			}

			if (decoder.getOrderDate().isEmpty())
			{
				QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Couldn't find the order date in the specified hardware key file!")).exec();
				return;
			}

			auto row = 0;
			auto tableWidget = new QTableWidget(10, 2);
			tableWidget->setWindowTitle(QFileInfo(ui->licenseFileEdit->text()).fileName() + " - " + windowTitle());
			tableWidget->setAttribute(Qt::WA_DeleteOnClose);
			tableWidget->setSortingEnabled(false);
			tableWidget->setAlternatingRowColors(true);
			tableWidget->setEditTriggers(QTableView::EditTrigger::DoubleClicked);
			tableWidget->setSelectionMode(QAbstractItemView::SelectionMode::NoSelection);
			tableWidget->verticalHeader()->setVisible(false);
			tableWidget->horizontalHeader()->setVisible(false);
			tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
			tableWidget->horizontalHeader()->setStretchLastSection(true);

			tableWidget->setItem(row, 0, new QTableWidgetItem(QApplication::translate("XrayLicenseCreatorWidget", "Order Date")));
			tableWidget->setItem(row++, 1, new QTableWidgetItem(decoder.getOrderDate()));

			tableWidget->setItem(row, 0, new QTableWidgetItem(QApplication::translate("XrayLicenseCreatorWidget", "Client Name")));
			tableWidget->setItem(row++, 1, new QTableWidgetItem(decoder.getClientName()));

			tableWidget->setItem(row, 0, new QTableWidgetItem(QApplication::translate("XrayLicenseCreatorWidget", "Client Company")));
			tableWidget->setItem(row++, 1, new QTableWidgetItem(decoder.getClientCompany()));

			tableWidget->setItem(row, 0, new QTableWidgetItem(QApplication::translate("XrayLicenseCreatorWidget", "Client Address")));
			tableWidget->setItem(row++, 1, new QTableWidgetItem(decoder.getClientAddress()));

			tableWidget->setItem(row, 0, new QTableWidgetItem(QApplication::translate("XrayLicenseCreatorWidget", "Client Email")));
			tableWidget->setItem(row++, 1, new QTableWidgetItem(decoder.getClientEmail()));

			tableWidget->setItem(row, 0, new QTableWidgetItem(QApplication::translate("XrayLicenseCreatorWidget", "Client Phone")));
			tableWidget->setItem(row++, 1, new QTableWidgetItem(decoder.getClientPhone()));

			tableWidget->setItem(row, 0, new QTableWidgetItem(QApplication::translate("XrayLicenseCreatorWidget", "Client Comments")));
			tableWidget->setItem(row++, 1, new QTableWidgetItem(decoder.getClientComments()));

			tableWidget->setItem(row, 0, new QTableWidgetItem(QApplication::translate("XrayLicenseCreatorWidget", "Application Name")));
			tableWidget->setItem(row++, 1, new QTableWidgetItem(decoder.getAppName()));

			tableWidget->setItem(row, 0, new QTableWidgetItem(QApplication::translate("XrayLicenseCreatorWidget", "Application Version")));
			tableWidget->setItem(row++, 1, new QTableWidgetItem(decoder.getAppVersion()));

			tableWidget->setItem(row, 0, new QTableWidgetItem(QApplication::translate("XrayLicenseCreatorWidget", "Hardware")));
			tableWidget->setItem(row++, 1, new QTableWidgetItem(decoder.getHardwareKey()));

			tableWidget->resize(width(), height());
			tableWidget->show();
		}
		else
		{
			QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseCreatorWidget", "Please browse and select a valid customer's hardware key file!")).exec();
		}
	});

	connect(ui->btnCancel, &QPushButton::clicked, [this]() { close(); });

	adjustSize();
}
XrayLicenseCreatorWidget::~XrayLicenseCreatorWidget()
{
	QSettings settings(QApplication::organizationName(), windowTitle());
	writeSettings(settings);
	delete ui;
}

void XrayLicenseCreatorWidget::writeSettings(QSettings& _settings)
{
	_settings.beginGroup(windowTitle());

	_settings.setValue("License/Id", ui->licenseIdEdit->text());
	_settings.setValue("License/Serial", ui->serialEdit->text());
	_settings.setValue("License/Type", ui->typeCombo->currentText());

	_settings.setValue("LicenseForm/File", ui->licenseFileEdit->text());

	_settings.endGroup();
}
void XrayLicenseCreatorWidget::readSettings(QSettings& _settings)
{
	_settings.beginGroup(windowTitle());

	ui->licenseIdEdit->setText(_settings.value("License/Id").toString());
	ui->serialEdit->setText(_settings.value("License/Serial").toString());
	ui->typeCombo->setCurrentText(_settings.value("License/Type").toString());

	ui->licenseFileEdit->setText(_settings.value("LicenseForm/File").toString());

	_settings.endGroup();
}

void XrayLicenseCreatorWidget::closeEvent(QCloseEvent* _event)
{
	QSettings settings(QApplication::organizationName(), windowTitle());
	writeSettings(settings);

	_event->accept();
}
