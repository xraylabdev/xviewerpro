/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/22
** filename: 	XrayLicenseFormWidget.cpp
** file base:	XrayLicenseFormWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a user registration widget with SQLITE
*				database connection.
****************************************************************************/

#include "XrayLicenseFormWidget.h"
#include "ui_XrayLicenseFormWidget.h"
#include "fvkLicenseEncoderClient.h"
#include "fvkLicenseDecoderClient.h"
#include "fvkLicenseDecoderServer.h"
#include "XrayLicenseGlobals.h"

#include "XrayGlobal.h"

#include <QApplication>
#include <QButtonGroup>
#include <QDesktopServices>
#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

using namespace R3D;

XrayLicenseFormWidget::XrayLicenseFormWidget(const QString& _winTitle, const QString& _email, const QString& _copyRight, const QString& _engineName, QWidget* _parent) :
	QWidget(_parent),
	ui(new Ui::XrayLicenseFormWidget)
{
	ui->setupUi(this);
	ui->label_3->setText(QApplication::translate("XrayLicenseFormWidget", "<html><head/><body><p>To apply for a new license, please create registration form, fill out form, and send the fully filled out form to the email address: <span style=\" font-weight:600;\">%1</span></p></body></html>").arg(_email));
	ui->copyrightLabel->setText(QCoreApplication::translate("XrayLicenseFormWidget", "<html><head/><body><p>\342\222\270 Copyright %1. All content, including but not limited to graphics and source code are protected by law. All rights reserved, including but not limited to: reproduction, publication, adaptation, and translation in any other language.<br/><br/>Please browse and select the file that you have recieved from us.</p></body></html>").arg(_copyRight));
	setWindowTitle(_winTitle);

	ui->stackedWidget->setCurrentIndex(0);

	ui->dateEdit->setDisplayFormat("dd/MM/yyyy");
	ui->dateEdit->setDate(QDate::currentDate());

	ui->licenseStatus->setStyleSheet("QLabel { font-size: 14px; font-weight: bold; color: red; }");

	auto radioBtns = new QButtonGroup(this);
	radioBtns->setExclusive(true);
	radioBtns->addButton(ui->radioBtnDemo);
	radioBtns->addButton(ui->radioBtnForm);
	radioBtns->addButton(ui->radioBtnLicense);

	XrayLicenseGlobals::setEngineName(_engineName);

	QSettings settings(QApplication::organizationName(), windowTitle());
	readSettings(settings);

	updateHardwareId();
	readLicenseFile();

	connect(ui->btnOk, &QPushButton::clicked, [this]()
	{
		if (ui->radioBtnDemo->isChecked())
		{
			XrayLicenseGlobals::setLicenseType(XrayLicenseGlobals::LicenseType::DEMO);
			emit decodingFinished();
			close();
		}
		else if (ui->radioBtnForm->isChecked())
		{
			ui->stackedWidget->setCurrentIndex(1);
			updateHardwareId();
		}
		else if (ui->radioBtnLicense->isChecked())
		{
			ui->stackedWidget->setCurrentIndex(2);
			readLicenseFile();
		}
	});
	
	connect(ui->btnExit, &QPushButton::clicked, [this]() { 	close(); QApplication::exit(); });
	connect(ui->btnOk2, &QPushButton::clicked, this, &XrayLicenseFormWidget::close);
	connect(ui->btnSave, &QPushButton::clicked, this, &XrayLicenseFormWidget::saveAs);
	connect(ui->btnBrowse, &QPushButton::clicked, this, &XrayLicenseFormWidget::open);

	connect(ui->btnGoBack1, &QPushButton::clicked, [this]() { ui->stackedWidget->setCurrentIndex(0); });
	connect(ui->btnGoBack, &QPushButton::clicked, [this]() { ui->stackedWidget->setCurrentIndex(0); });

	connect(ui->btnSendByEmail, &QPushButton::clicked, [this, _email]()
	{
		auto appPath = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + '/' + QApplication::applicationName();
		if (!QDir(appPath).exists())
			QDir().mkdir(appPath);

		appPath += "/" + getLicenseFileName();
		save(appPath);

		QString info = "Note: The filled form along with the hardware key has bee saved at '" + appPath + "',\nplease attach the saved file with this email and press Send.";
		info += "\n------------------------------------------------------------------------------\n\n";
		auto subject = "License request for " + QApplication::applicationName() + " v" + QApplication::applicationVersion();
		auto body = info + "\n\nDear " + QApplication::organizationName() + " Representative,\n\nPlease find the attached hardware key.\n\nBest Regards,\n\n" + ui->fullNameEdit->text() + "\n" + ui->companyEdit->text();
		auto attachment = appPath;
		auto mailto = "mailto:" + _email + "?subject=" + subject + "&body=" + body + "&attachment=" + "\"" + attachment + "\"";	// attachment doesn't work with Qt.
		QDesktopServices::openUrl(QUrl(mailto));
	});

	adjustSize();	// this adjusts the size of layouts.
}
XrayLicenseFormWidget::~XrayLicenseFormWidget()
{
	QSettings settings(QApplication::organizationName(), windowTitle());
	writeSettings(settings);
	delete ui;
}
QString XrayLicenseFormWidget::getLicenseFileName() const
{
	QString company;
	if (!ui->companyEdit->text().isEmpty())
		company = '_' + ui->companyEdit->text();
	auto name = QString(QApplication::applicationName() + "_v" + QApplication::applicationVersion() + '_' + ui->fullNameEdit->text() + company).remove(' ').remove('.');
	return name + ".dat";
}

QStackedWidget* XrayLicenseFormWidget::stackedWidget() const
{
	return ui->stackedWidget;
}

void XrayLicenseFormWidget::updateHardwareId()
{
	fvkLicenseEncoderClient encoder;
	encoder.setOrderDate(ui->dateEdit->text());
	encoder.setAppName(QApplication::applicationName());
	encoder.setAppVersion(QApplication::applicationVersion());
	encoder.setKey(XrayLicenseGlobals::encryptionKey());
	encoder.setIdentifier(
		HardwareIdentifier::ProcessorName |
		HardwareIdentifier::ProcessorId |
		HardwareIdentifier::BaseBoardProduct |
		HardwareIdentifier::BaseBoardManufacturer);

	if (encoder.encode() == 1)
		ui->hardwareIdEdit->setPlainText(encoder.getKeyText());
}

void XrayLicenseFormWidget::readLicenseFile()
{
	fvkLicenseDecoderServer decoder;
	decoder.setKey(XrayLicenseGlobals::encryptionKey());
	decoder.setIdentifier(
		HardwareIdentifier::ProcessorName |
		HardwareIdentifier::ProcessorId |
		HardwareIdentifier::BaseBoardProduct |
		HardwareIdentifier::BaseBoardManufacturer);
	decoder.setInputFileLocation(ui->licenseFileEdit->text());
	if (decoder.read())
	{
		ui->licenseStatus->setStyleSheet("QLabel { font-size: 14px; font-weight: bold; color: red; }");
		ui->fullNameLabel->setText("n/a");
		ui->companyLabel->setText("n/a");
		ui->emailLabel->setText("n/a");

		ui->licenseIdLabel->setText("n/a");
		ui->serialLabel->setText("n/a");
		ui->licenseTypeLabel->setText("n/a");
		ui->expireLabel->setText("n/a");
		ui->issueLabel->setText("n/a");
		ui->modulesLabel->setText("n/a");
		XrayLicenseGlobals::setModules("n/a");
		XrayLicenseGlobals::setLicenseType("n/a");

		if (ui->btnExit->isHidden())
			ui->btnExit->show();

		if (decoder.decode() == 1)
		{
			// check the hardware if its matched.
			if (!decoder.isHardwareMatched())
				ui->licenseStatus->setText(QApplication::translate("XrayLicenseFormWidget", "License available but not registered with this hardware"));

			// check the version number, the license will b valid for the first two digits (major and minor).
			// For example: In case of 1.1.5, if the app version changes to 1.2.0 then the license will be expired.
			auto registeredVersion = decoder.getAppVersion().remove('.');
			auto currentAppVersion = QApplication::applicationVersion().remove('.');
			auto isVersionSame = false;
			if (registeredVersion.at(0) == currentAppVersion.at(0)/* && registeredVersion.at(1) == currentAppVersion.at(1)*/)	// currently only consider the major version.
				isVersionSame = true;

			// check the name of the application.
			auto isAppNameSame = decoder.getAppName() == QApplication::applicationName();

			if (!isAppNameSame || !isVersionSame)
				ui->licenseStatus->setText(QApplication::translate("XrayLicenseFormWidget", "License available but not registered for this application/version"));

			if (decoder.isHardwareMatched() && isAppNameSame && isVersionSame)
			{
				ui->fullNameLabel->setText(decoder.getClientName().isEmpty() ? "n/a" : decoder.getClientName());
				ui->companyLabel->setText(decoder.getClientCompany().isEmpty() ? "n/a" : decoder.getClientCompany());
				ui->emailLabel->setText(decoder.getClientEmail().isEmpty() ? "n/a" : decoder.getClientEmail());

				ui->licenseIdLabel->setText(decoder.getLicenseId().isEmpty() ? "n/a" : decoder.getLicenseId());
				ui->serialLabel->setText(decoder.getSerial().isEmpty() ? "n/a" : decoder.getSerial());
				ui->licenseTypeLabel->setText(decoder.getLicenseType().isEmpty() ? "n/a" : decoder.getLicenseType());
				ui->expireLabel->setText(decoder.getExpireDate().isEmpty() ? "n/a" : decoder.getExpireDate());
				ui->issueLabel->setText(decoder.getIssueDate().isEmpty() ? "n/a" : decoder.getIssueDate());
				ui->modulesLabel->setText(decoder.getModules().isEmpty() ? "n/a" : decoder.getModules());
				if (decoder.getModules().isEmpty())
				{
					ui->label_21->hide();
					ui->modulesLabel->hide();
				}

				if (!ui->radioBtnLicense->isChecked())
					ui->radioBtnLicense->setChecked(true);	// license is available, so check details.
				if (!ui->btnExit->isHidden())
					ui->btnExit->hide();

				auto isPermanentLicense = ui->licenseTypeLabel->text().contains("Permanent", Qt::CaseInsensitive);
				if (isPermanentLicense)
				{
					XrayLicenseGlobals::setExpired(false);
					ui->expireLabel->setText(QApplication::translate("XrayLicenseFormWidget", "Never"));
					ui->licenseStatus->setStyleSheet("QLabel { font-size: 14px; font-weight: bold; color: green; }");
					ui->licenseStatus->setText(QApplication::translate("XrayLicenseFormWidget", "License available"));
				}
				else
				{
					auto now = QDate::fromString(QDate::currentDate().toString(ui->dateEdit->displayFormat()), ui->dateEdit->displayFormat());
					auto expire = QDate::fromString(decoder.getExpireDate(), ui->dateEdit->displayFormat());
					auto days = now.daysTo(expire);
					if (days < 0)
					{
						ui->licenseStatus->setText(QApplication::translate("XrayLicenseFormWidget", "License available but expired"));
						XrayLicenseGlobals::setExpired(true);
					}
					else
					{
						XrayLicenseGlobals::setExpired(false);
						ui->licenseStatus->setStyleSheet("QLabel { font-size: 14px; font-weight: bold; color: green; }");
						ui->licenseStatus->setText(QApplication::translate("XrayLicenseFormWidget", "License available"));
					}
				}
			}

			XrayLicenseGlobals::setModules(ui->modulesLabel->text());
			XrayLicenseGlobals::setLicenseType(ui->licenseTypeLabel->text());
		}
		else
		{
			ui->licenseStatus->setText(QApplication::translate("XrayLicenseFormWidget", "License not available"));
		}
	}
	else
	{
		// QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseFormWidget", "Couldn't open the license file!")).exec();
	}

	emit decodingFinished();
}

void XrayLicenseFormWidget::open()
{
	auto path = QDir(ui->licenseFileEdit->text()).absolutePath();
	if (path.isEmpty())
		path = XrayGlobal::getLastFileOpenPath();

	auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayLicenseFormWidget", "Select File"), path, QApplication::translate("XrayLicenseFormWidget", "All Supported Files (*.dat)"), nullptr);
	if (!fileName.isEmpty())
	{
		ui->licenseFileEdit->setText(fileName);
		XrayGlobal::setLastFileOpenPath(fileName);

		readLicenseFile();
	}
}
void XrayLicenseFormWidget::save(const QString& fileName)
{
	if (ui->fullNameEdit->text().isEmpty())
	{
		QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseFormWidget", "Please fill in required fields (Full name: *)!")).exec();
		return;
	}
	if (ui->emailEdit->text().isEmpty())
	{
		QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseFormWidget", "Please fill in required fields (Email: *)!")).exec();
		return;
	}

	if (!fileName.isEmpty())
	{
		fvkLicenseEncoderClient encoder;
		encoder.setOutputFileLocation(fileName);
		encoder.setClientName(ui->fullNameEdit->text());
		encoder.setOrderDate(ui->dateEdit->text());
		encoder.setClientCompany(ui->companyEdit->text());
		encoder.setClientAddress(ui->addressEdit->text());
		encoder.setClientEmail(ui->emailEdit->text());
		encoder.setClientPhone(ui->phoneEdit->text());
		encoder.setClientComments(ui->commentsEdit->toPlainText());

		encoder.setAppName(QApplication::applicationName());
		encoder.setAppVersion(QApplication::applicationVersion());
		encoder.setKey(XrayLicenseGlobals::encryptionKey());
		encoder.setIdentifier(
			HardwareIdentifier::ProcessorName |
			HardwareIdentifier::ProcessorId |
			HardwareIdentifier::BaseBoardProduct |
			HardwareIdentifier::BaseBoardManufacturer);

		auto r = encoder.encode();
		if (r == 0)
			QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseFormWidget", "Something went wrong!"/*"The decoded key is not a valid key!"*/)).exec();
		else if (r == -1)
			QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseFormWidget", "Something went wrong!"/*"Couldn't encode!"*/)).exec();
		else if (r == -2)
			QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseFormWidget", "Something went wrong!"/*"Couldn't encrypt!"*/)).exec();
		else if (r == 1)
		{
			ui->hardwareIdEdit->setPlainText(encoder.getKeyText());

			if (!encoder.write())
				QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseFormWidget", "Couldn't save hardware key file to the disk!")).exec();
			else
				QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayLicenseFormWidget", "Hardware key file has been saved successfully!")).exec();
		}

		XrayGlobal::setLastFileOpenPath(fileName);
	}
}
void XrayLicenseFormWidget::saveAs()
{	
	auto fileName = QFileDialog::getSaveFileName(this, QApplication::translate("XrayLicenseFormWidget", "Save File As..."), XrayGlobal::getLastFileOpenPath() + "/" + getLicenseFileName(), QApplication::translate("XrayLicenseFormWidget", "All Supported Files (*.dat)"), nullptr);
	save(fileName);
}
void XrayLicenseFormWidget::writeSettings(QSettings& _settings)
{
	_settings.beginGroup(QApplication::applicationName());
	_settings.beginGroup("LicenseSettings");

	_settings.setValue("RegForm/Name", ui->fullNameEdit->text());
	_settings.setValue("RegForm/Date", ui->dateEdit->text());
	_settings.setValue("RegForm/Company", ui->companyEdit->text());
	_settings.setValue("RegForm/Address", ui->addressEdit->text());
	_settings.setValue("RegForm/Email", ui->emailEdit->text());
	_settings.setValue("RegForm/Phone", ui->phoneEdit->text());
	_settings.setValue("RegForm/Comments", ui->commentsEdit->toPlainText());

	_settings.setValue("LicenseForm/File", ui->licenseFileEdit->text());

	_settings.endGroup();
	_settings.endGroup();
}
void XrayLicenseFormWidget::readSettings(QSettings& _settings)
{
	_settings.beginGroup(QApplication::applicationName());
	_settings.beginGroup("LicenseSettings");

	ui->fullNameEdit->setText(_settings.value("RegForm/Name").toString());
	ui->dateEdit->setDate(QDate::fromString(_settings.value("RegForm/Date").toString(), ui->dateEdit->displayFormat()));
	ui->companyEdit->setText(_settings.value("RegForm/Company").toString());
	ui->addressEdit->setText(_settings.value("RegForm/Address").toString());
	ui->emailEdit->setText(_settings.value("RegForm/Email").toString());
	ui->phoneEdit->setText(_settings.value("RegForm/Phone").toString());
	ui->commentsEdit->setText(_settings.value("RegForm/Comments").toString());

	ui->licenseFileEdit->setText(_settings.value("LicenseForm/File", ui->licenseFileEdit->text()).toString());

	_settings.endGroup();
	_settings.endGroup();
}

void XrayLicenseFormWidget::closeEvent(QCloseEvent* _event)
{
	QSettings settings(QApplication::organizationName(), windowTitle());
	writeSettings(settings);

	_event->accept();
}
