﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XraySqlUserManagementWidget.cpp
** file base:	XraySqlUserManagementWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a user registration widget with SQLITE
*				database connection.
****************************************************************************/

#include "XraySqlUserManagementWidget.h"
#include "ui_XraySqlUserManagementWidget.h"
#include "XraySqlDatabase.h"
#include "XrayGlobal.h"
#include "XrayLicenseGlobals.h"
#include "XrayQSimpleCrypt.h"

#include <QCoreApplication>
#include <QSqlRecord>
#include <QSqlError>
#include <QFileDialog>
#include <QStandardItemModel>
#include <QMessageBox>
#include <QStatusBar>
#include <QSettings>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

static const quint64 XRAY_SIMPLE_ENCRYPT_KEY = 8945634473829;
static XrayQSimpleCrypt simpleCrypt = XrayQSimpleCrypt(XRAY_SIMPLE_ENCRYPT_KEY);

static QString EncryptToStr(const QString& _text)
{
	simpleCrypt.setCompressionMode(XrayQSimpleCrypt::CompressionMode::CompressionAlways);
	simpleCrypt.setIntegrityProtectionMode(XrayQSimpleCrypt::IntegrityProtectionMode::ProtectionHash);
	return simpleCrypt.encryptToString(_text);
}
static QString DecryptToStr(const QString& _text)
{
	return simpleCrypt.decryptToString(_text);
}

QDBLite::DB db;

XraySqlUserManagementWidget::XraySqlUserManagementWidget(QWidget* _parent) :
	QMainWindow(_parent),
	ui(new Ui::XraySqlUserManagementWidget),
	m_logged_in(false),
	m_adminLogged_in(false)
{
	ui->setupUi(this);
	setWindowTitle("XUser Panel");

	if (!db.connect()) 
	{
		xAppInfo("Failed to connect to database. {}", db.m_db.lastError().text());
	}
	else
	{
		xAppInfo("Succeeded to connect to database.");
	}

	createAdminUser();
	printUsers();

	ui->winStack->setCurrentIndex(0);
	ui->stackedWidget->setCurrentIndex(1);

	ui->passwordBox->setEchoMode(QLineEdit::Password);
	ui->passwordBox->setInputMethodHints(Qt::ImhHiddenText | Qt::ImhNoPredictiveText | Qt::ImhNoAutoUppercase);
	ui->pBox->setEchoMode(QLineEdit::Password);
	ui->pBox->setInputMethodHints(Qt::ImhHiddenText | Qt::ImhNoPredictiveText | Qt::ImhNoAutoUppercase);
	ui->pBox_2->setEchoMode(QLineEdit::Password);
	ui->pBox_2->setInputMethodHints(Qt::ImhHiddenText | Qt::ImhNoPredictiveText | Qt::ImhNoAutoUppercase);

	//ui->tableView->setEditTriggers(QAbstractItemView::SelectedClicked);
	ui->tableView->verticalHeader()->setVisible(true);
	ui->tableView->horizontalHeader()->setVisible(true);
	ui->tableView->horizontalHeader()->setStretchLastSection(true);
	ui->tableView->horizontalHeader()->setSectionsMovable(true);
	ui->tableView_2->verticalHeader()->setVisible(true);
	ui->tableView_2->horizontalHeader()->setVisible(true);
	ui->tableView_2->horizontalHeader()->setStretchLastSection(true);
	ui->tableView_2->horizontalHeader()->setSectionsMovable(true);

	setContentsMargins(0, 0, 0, 0);
	ui->gridLayout_6->setContentsMargins(0, 0, 0, 0);
	ui->gridLayout_5->setContentsMargins(0, 0, 0, 0);

	QSettings settings(QApplication::organizationName(), windowTitle());
	if (settings.value("Login/SavePermission", false).toBool())
	{
		ui->usernameBox->setText(settings.value("Login/UserName", "").toString());
		ui->passwordBox->setText(DecryptToStr(settings.value("Login/Password", "").toString()));
	}
}

XraySqlUserManagementWidget::~XraySqlUserManagementWidget()
{
	delete ui;
}

void XraySqlUserManagementWidget::createAdminUser()
{
	if (!db.tableExists("sys_users"))
	{
		xAppWarn("Database do not contain the sys_users table.");


		QSqlQuery query;
		query.exec("CREATE TABLE `sys_users` ("
			"`id`		INTEGER PRIMARY KEY AUTOINCREMENT,"
			"`username`	TEXT NOT NULL UNIQUE,"
			"`passwd`	TEXT NOT NULL,"
			"`fname`	TEXT NOT NULL,"
			"`lname`	TEXT NOT NULL,"
			"`rank`		INTEGER DEFAULT (1),"
			"`mobile`	TEXT,"
			"`email`	TEXT NOT NULL UNIQUE"
			");"
		);

		query.exec("INSERT INTO sys_users (id, username, passwd, fname, lname, rank, mobile, email) VALUES (0, 'admin', 'admin', 'System', 'Admin', -1, '+49 (0) 000 1111111', 'admin@xray-lab.com');");
		//query.exec("INSERT INTO sys_users (id, username, passwd, fname, lname, rank, mobile, email) VALUES (1, 'user', 'user', 'Guest', 'User', 1, '+49 (0) 000 1111111', 'user@xyz.xyz');");
	}
	else
	{
		xAppWarn("Database already contains the sys_users table.");
	}
}
void XraySqlUserManagementWidget::printUsers()
{
	QSqlQuery q("SELECT * FROM sys_users");
	while (q.next())
	{
		xAppInfo("{}, {}, {}, {}, {}, {}, {}, {}",
			q.value(0).toInt(),
			q.value("username").toString(),
			q.value("passwd").toString(),
			q.value("fname").toString(),
			q.value("lname").toString(),
			q.value("mobile").toString(),
			q.value("email").toString(),
			q.value("rank").toInt());
	}
}

void XraySqlUserManagementWidget::on_loginButton_clicked()
{
	auto userName = ui->usernameBox->text();
	auto password = ui->passwordBox->text();

	m_logged_in = login(userName, password);

	QSettings settings(QApplication::organizationName(), windowTitle());
	if (DecryptToStr(settings.value("Login/Password", "").toString()) != password && m_logged_in)
	{
		if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XraySqlUserManagementWidget", "Do you want User Manager to remember your name and password?"), QMessageBox::Yes | QMessageBox::No).exec())
		{
			settings.setValue("Login/UserName", userName);
			settings.setValue("Login/Password", EncryptToStr(password));
			settings.setValue("Login/SavePermission", true);
		}
		else
		{
			settings.setValue("Login/SavePermission", false);
		}
	}

	if (m_logged_in)
	{
		m_username = userName;
		m_password = password;

		ui->loginLabel->setText("");
		ui->winStack->setCurrentIndex(2);

		emit emitLogin();
	}
	else
	{
		ui->loginLabel->setText(QApplication::translate("XraySqlUserManagementWidget", "Login Failed: The user name or password entered is incorrect."));
	}
}

bool XraySqlUserManagementWidget::hasLoggedInByAdmin() const
{
	return m_adminLogged_in;
}

bool XraySqlUserManagementWidget::login(const QString _u, const QString _p)
{
	ui->adminButton->setVisible(false);
	m_adminLogged_in = false;

	auto q = db.query();
	q.prepare("SELECT username FROM sys_users WHERE username = (:un) AND passwd = (:pw)");
	q.bindValue(":un", _u);
	q.bindValue(":pw", _p);

	if (q.exec())
	{
		if (q.next())
			return true;
	}

	return false;
}
bool XraySqlUserManagementWidget::hasLoggedIn() const
{
	return m_logged_in;
}


void XraySqlUserManagementWidget::on_regButton_clicked()
{
	ui->uBox->setText(ui->usernameBox->text());
	ui->pBox->setText(ui->passwordBox->text());
	ui->winStack->setCurrentIndex(1);
}

void XraySqlUserManagementWidget::on_logoutButton_clicked()
{
	// if you haven't logged in yet then do nothing.
	if (!m_logged_in)
		return;

	QSettings settings(QApplication::organizationName(), windowTitle());
	if (!settings.value("Login/SavePermission", false).toBool())
	{
		ui->usernameBox->setText("");
		ui->passwordBox->setText("");
	}

	m_logged_in = false;
	ui->loginLabel->setText(QApplication::translate("XraySqlUserManagementWidget", "You have been logged out."));
	ui->winStack->setCurrentIndex(0);
}

void XraySqlUserManagementWidget::on_completeRegButton_clicked()
{
	if (XrayLicenseGlobals::isDemoVersion())
	{
		// limit the number of users to get registered.
		auto n = 0;
		QSqlQuery q("SELECT * FROM sys_users");
		while (q.next()) n++;
		if (n > 1)
		{
			QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XraySqlUserManagementWidget", "Demo version allows only 2 users to register.")).exec();
			return;
		}
	}


	auto halt = false;

	if (ui->uBox->text() == "")
	{
		ui->uBox->setPlaceholderText(tr("User Name"));
		halt = true;
	}

	if (ui->pBox->text() == "")
	{
		ui->pBox->setPlaceholderText(tr("Password"));
		halt = true;
	}

	if (ui->eBox->text() == "")
	{
		ui->eBox->setPlaceholderText(tr("Email"));
		halt = true;
	}

	if (ui->fBox->text() == "")
	{
		ui->fBox->setPlaceholderText(tr("First Name"));
		halt = true;
	}

	if (ui->mBox->text() == "")
	{
		ui->mBox->setPlaceholderText(tr("Phone Number"));
		halt = true;
	}

	if (ui->lBox->text() == "")
	{
		ui->lBox->setPlaceholderText(tr("Last Name"));
		halt = true;
	}

	auto q1 = db.query();
	q1.prepare("SELECT username FROM sys_users WHERE username = (:un)");
	q1.bindValue(":un", ui->uBox->text());

	if (q1.exec())
	{
		if (q1.next())
		{
			ui->uBox->setText("");
			ui->regLabel->setText(QApplication::translate("XraySqlUserManagementWidget", "Choose a different User Name!"));
			halt = true;
			return;
		}
	}

	auto q2 = db.query();
	q2.prepare("SELECT email FROM sys_users WHERE email = (:em)");
	q2.bindValue(":em", ui->eBox->text());

	if (q2.exec())
	{
		if (q2.next())
		{
			ui->eBox->setText("");
			ui->regLabel->setText(QApplication::translate("XraySqlUserManagementWidget", "Use another Email address!"));
			halt = true;
			return;
		}
	}


	if (halt)
	{
		ui->regLabel->setText(QApplication::translate("XraySqlUserManagementWidget", "Please fill all entries."));
	}
	else
	{
		if (m_pic_name != "")
		{
			auto to = m_pic_dir + "/" + ui->uBox->text();

			if (QFile::exists(to))
			{
				QFile::remove(to);
			}

			QFile::copy(m_pic_name, to);
			m_pic_name = "";
		}

		ui->regLabel->setText("");

		auto q = db.query();
		q.prepare("INSERT INTO sys_users(username, passwd, fname, lname, mobile, email)"
			"VALUES(:un, :pw, :fn, :ln, :mb, :em)");
		q.bindValue(":un", ui->uBox->text());
		q.bindValue(":pw", ui->pBox->text());
		q.bindValue(":fn", ui->fBox->text());
		q.bindValue(":ln", ui->lBox->text());
		q.bindValue(":mb", ui->mBox->text());
		q.bindValue(":em", ui->eBox->text());

		if (q.exec())
		{
			ui->uBox->setText("");
			ui->pBox->setText("");
			ui->eBox->setText("");
			ui->fBox->setText("");
			ui->mBox->setText("");
			ui->lBox->setText("");
			ui->rpLabel->setPixmap(QPixmap(":/res/images/avatar.png"));
			ui->loginLabel->setText(QApplication::translate("XraySqlUserManagementWidget", "Registration Successful! You can now login."));
			ui->winStack->setCurrentIndex(0);
		}
	}
}

void XraySqlUserManagementWidget::on_backButton_clicked()
{
	ui->loginLabel->setText("");
	ui->winStack->setCurrentIndex(0);
}

void XraySqlUserManagementWidget::on_backButton_2_clicked()
{
	ui->winStack->setCurrentIndex(2);
}

void XraySqlUserManagementWidget::on_editButton_clicked()
{
	QSqlQuery q;
	q.prepare("SELECT * FROM sys_users WHERE username = (:un) AND passwd = (:pw)");
	q.bindValue(":un", m_username);
	q.bindValue(":pw", m_password);
	q.exec();

	const auto idUsername = q.record().indexOf("username");
	const auto idPasswd = q.record().indexOf("passwd");
	const auto idFname = q.record().indexOf("fname");
	const auto idLname = q.record().indexOf("lname");
	const auto idMname = q.record().indexOf("mobile");
	const auto idEmail = q.record().indexOf("email");

	while (q.next())
	{
		ui->uBox_2->setText(q.value(idUsername).toString());
		ui->pBox_2->setText(q.value(idPasswd).toString());
		ui->fBox_2->setText(q.value(idFname).toString());
		ui->lBox_2->setText(q.value(idLname).toString());
		ui->mBox_2->setText(q.value(idMname).toString());
		ui->eBox_2->setText(q.value(idEmail).toString());
	}

	ui->winStack->setCurrentIndex(3);
}

void XraySqlUserManagementWidget::on_delButton_clicked()
{
	if (QMessageBox::Yes == QMessageBox(QMessageBox::Question, windowTitle(), QApplication::translate("XraySqlUserManagementWidget", "Are you sure you want to delete your account?"), QMessageBox::Yes | QMessageBox::No).exec())
	{
		auto to = m_pic_dir + "/" + m_username;
		if (QFile::exists(to))
			QFile::remove(to);

		auto q = db.query();
		q.prepare("DELETE FROM sys_users WHERE username = (:un)");
		q.bindValue(":un", m_username);

		if (q.exec())
		{
			ui->usernameBox->setText("");
			ui->passwordBox->setText("");
			ui->loginLabel->setText(QApplication::translate("XraySqlUserManagementWidget", "Your account has been deleted."));
			ui->winStack->setCurrentIndex(0);
		}
	}
}

void XraySqlUserManagementWidget::on_editedButton_clicked()
{
	auto halt = false;

	if (ui->uBox_2->text() == "")
	{
		ui->uBox_2->setPlaceholderText(QApplication::translate("XraySqlUserManagementWidget", "User Name"));
		halt = true;
	}

	if (ui->pBox_2->text() == "")
	{
		ui->pBox_2->setPlaceholderText(QApplication::translate("XraySqlUserManagementWidget", "Password"));
		halt = true;
	}

	if (ui->eBox_2->text() == "")
	{
		ui->eBox_2->setPlaceholderText(QApplication::translate("XraySqlUserManagementWidget", "Email"));
		halt = true;
	}

	if (ui->fBox_2->text() == "")
	{
		ui->fBox_2->setPlaceholderText(QApplication::translate("XraySqlUserManagementWidget", "First Name"));
		halt = true;
	}

	if (ui->mBox_2->text() == "")
	{
		ui->mBox_2->setPlaceholderText(QApplication::translate("XraySqlUserManagementWidget", "Phone Number"));
		halt = true;
	}

	if (ui->lBox_2->text() == "")
	{
		ui->lBox_2->setPlaceholderText(QApplication::translate("XraySqlUserManagementWidget", "Last Name"));
		halt = true;
	}

	auto q1 = db.query();
	q1.prepare("SELECT username FROM sys_users WHERE username = (:un)");
	q1.bindValue(":un", ui->uBox->text());

	if (q1.exec())
	{
		if (q1.next() && ui->uBox_2->text() != q1.value(0).toString())
		{
			ui->uBox_2->setText("");
			ui->uBox_2->setPlaceholderText(QApplication::translate("XraySqlUserManagementWidget", "Choose a different User Name!"));
			halt = true;
		}
	}

	auto q2 = db.query();
	q2.prepare("SELECT email FROM sys_users WHERE email = (:em)");
	q2.bindValue(":em", ui->eBox_2->text());

	if (q2.exec())
	{
		if (q2.next() && ui->eBox_2->text() != q2.value(0).toString())
		{
			ui->eBox_2->setText("");
			ui->eBox_2->setPlaceholderText(QApplication::translate("XraySqlUserManagementWidget", "Use another Email!"));
			halt = true;
		}
	}


	if (halt)
	{
		ui->regLabel_2->setText(QApplication::translate("XraySqlUserManagementWidget", "Please use valid entries."));
	}
	else
	{
		if (m_pic_name != "")
		{
			auto to = m_pic_dir + "/" + ui->uBox_2->text();

			if (QFile::exists(to))
				QFile::remove(to);

			QFile::copy(m_pic_name, to);
			m_pic_name = "";
		}

		ui->regLabel_2->setText("");

		auto q = db.query();
		q.prepare("UPDATE sys_users SET username=(:un), passwd=(:pw), fname=(:fn), lname=(:ln), mobile=(:mb), email=(:em) WHERE username=(:uno)");
		q.bindValue(":un", ui->uBox_2->text());
		q.bindValue(":pw", ui->pBox_2->text());
		q.bindValue(":fn", ui->fBox_2->text());
		q.bindValue(":ln", ui->lBox_2->text());
		q.bindValue(":mb", ui->mBox_2->text());
		q.bindValue(":em", ui->eBox_2->text());
		q.bindValue(":uno", ui->uBox_2->text());

		if (q.exec())
			ui->winStack->setCurrentIndex(2);
	}
}

void XraySqlUserManagementWidget::on_winStack_currentChanged(int _arg)
{
	if (_arg == 3 && m_logged_in)
	{
		if (QFile::exists(m_pic_dir + "/" + m_username))
		{
			ui->rpLabel_2->setText(
				"<img src=\"file:///" + m_pic_dir + "/" + m_username +
				"\" alt=\"Image read error!\" height=\"128\" width=\"128\" />");
		}
		else
		{
			ui->rpLabel_2->setPixmap(QPixmap(":/res/images/avatar.png"));
		}
	}

	if (_arg == 2 && m_logged_in)
	{
		if (QFile::exists(m_pic_dir + "/" + m_username))
		{
			ui->loggedPic->setText(
				"<img src=\"file:///" + m_pic_dir + "/" + m_username +
				"\" alt=\"Image read error!\" height=\"128\" width=\"128\" />");
		}
		else
		{
			ui->loggedPic->setPixmap(QPixmap(":/res/images/avatar.png"));
		}

		QSqlQuery q;
		q.prepare("SELECT * FROM sys_users WHERE username = (:un)");
		q.bindValue(":un", m_username);
		q.exec();

		const auto idFname = q.record().indexOf("fname");
		const auto idLname = q.record().indexOf("lname");
		const auto idRank = q.record().indexOf("rank");
		const auto idMname = q.record().indexOf("mobile");
		const auto idEmail = q.record().indexOf("email");

		QString fullname, email, rank;

		while (q.next())
		{
			fullname = q.value(idFname).toString();
			fullname += " " + q.value(idLname).toString();
			rank = q.value(idRank).toString();
			email = q.value(idEmail).toString();
		}

		if (rank == "-1")
		{
			ui->adminButton->setVisible(true);
			m_adminLogged_in = true;
		}

		ui->nameLabel->setText(fullname);
		ui->rankLabel->setText(rank);
		ui->emailLabel->setText(email);
	}

	if (_arg == 4 && m_logged_in)
		ui->stackedWidget->setCurrentIndex(0);
}

void XraySqlUserManagementWidget::on_uplButton_clicked()
{
	m_pic_name = QFileDialog::getOpenFileName(this, tr("Open Image"), "/", tr("Image Files (*.png *.jpg *.bmp)"), nullptr, QFileDialog::DontUseNativeDialog);
	if (!m_pic_name.isEmpty())
		ui->rpLabel->setText("<img src=\"file:///" + m_pic_name + "\" alt=\"Image read error!\" height=\"128\" width=\"128\" />");
	else
		ui->rpLabel->setPixmap(QPixmap(":/res/images/avatar.png"));
}

void XraySqlUserManagementWidget::on_uplButton_2_clicked()
{
	m_pic_name = QFileDialog::getOpenFileName(this, tr("Open Image"), "/", tr("Image Files (*.png *.jpg *.bmp)"), nullptr, QFileDialog::DontUseNativeDialog);
	if (!m_pic_name.isEmpty())
		ui->rpLabel_2->setText("<img src=\"file:///" + m_pic_name + "\" alt=\"Image read error!\" height=\"128\" width=\"128\" />");
	else
		ui->rpLabel->setPixmap(QPixmap(":/res/images/avatar.png"));
}

void XraySqlUserManagementWidget::on_adminButton_clicked()
{
	ui->winStack->setCurrentIndex(4);
}

void XraySqlUserManagementWidget::on_pageButton_clicked()
{
	ui->winStack->setCurrentIndex(2);
}

void XraySqlUserManagementWidget::on_editedButton_2_clicked()
{
	if (p_sql_table_model->submitAll())
	{
		p_sql_table_model->database().commit();
		ui->adminLabel->setText(QApplication::translate("XraySqlUserManagementWidget", "Saved! database has been updated successfully."));
	}
	else
	{
		p_sql_table_model->database().rollback();
	}
}

void XraySqlUserManagementWidget::on_backButton_5_clicked()
{
	p_sql_table_model->revertAll();
	p_sql_table_model->database().rollback();
}

void XraySqlUserManagementWidget::on_userBrowse_clicked()
{
	ui->stackedWidget->setCurrentIndex(0);
}
void XraySqlUserManagementWidget::on_adminBrowse_clicked()
{
	ui->stackedWidget->setCurrentIndex(1);
}

/*!
	Description:
	Function to check if the database has any user.
	/param _rank rank value of the user. Specify 1 for normal user, -1 for admin.
*/
bool hasAnyUser(int _rank)
{
	auto b = false;
	QSqlQuery q("SELECT * FROM sys_users");
	while (q.next())
	{
		const auto r = q.value("rank").toInt();
		if (r == _rank)
		{
			b = true;
			break;
		}
	}
	return b;
}

void XraySqlUserManagementWidget::updateUsersTable()
{
	p_sql_table_model = new QSqlTableModel;
	p_sql_table_model->setTable("sys_users");
	p_sql_table_model->setFilter("rank != -1 AND rank != 0");
	p_sql_table_model->select();

	p_sql_table_model->setHeaderData(0, Qt::Horizontal, tr("Id"));
	p_sql_table_model->setHeaderData(1, Qt::Horizontal, QApplication::translate("XraySqlUserManagementWidget", "User Name"));
	p_sql_table_model->setHeaderData(2, Qt::Horizontal, QApplication::translate("XraySqlUserManagementWidget", "Password"));
	p_sql_table_model->setHeaderData(3, Qt::Horizontal, QApplication::translate("XraySqlUserManagementWidget", "First Name"));
	p_sql_table_model->setHeaderData(4, Qt::Horizontal, QApplication::translate("XraySqlUserManagementWidget", "Last Name"));
	p_sql_table_model->setHeaderData(5, Qt::Horizontal, QApplication::translate("XraySqlUserManagementWidget", "Rank"));
	p_sql_table_model->setHeaderData(6, Qt::Horizontal, QApplication::translate("XraySqlUserManagementWidget", "Phone Number"));
	p_sql_table_model->setHeaderData(7, Qt::Horizontal, tr("Email"));

	ui->tableView->model()->deleteLater();
	ui->tableView->setModel(p_sql_table_model);
	//ui->tableView->resizeColumnsToContents();
	p_sql_table_model->database().transaction();

	// view header data only if there is a user, otherwise don't.
	if (!hasAnyUser(1))
	{
		p_sql_table_model->clear();
		ui->tableView->horizontalHeader()->setVisible(false);
	}
	else
	{
		ui->tableView->horizontalHeader()->setVisible(true);
	}
}

void XraySqlUserManagementWidget::updateAdminsTable()
{
	p_sql_table_model->clear();
	p_sql_table_model = new QSqlTableModel;
	p_sql_table_model->setTable("sys_users");
	p_sql_table_model->setFilter("rank == -1 OR rank == 0");
	p_sql_table_model->select();

	p_sql_table_model->setHeaderData(0, Qt::Horizontal, tr("Id"));
	p_sql_table_model->setHeaderData(1, Qt::Horizontal, QApplication::translate("XraySqlUserManagementWidget", "User Name"));
	p_sql_table_model->setHeaderData(2, Qt::Horizontal, QApplication::translate("XraySqlUserManagementWidget", "Password"));
	p_sql_table_model->setHeaderData(3, Qt::Horizontal, QApplication::translate("XraySqlUserManagementWidget", "First Name"));
	p_sql_table_model->setHeaderData(4, Qt::Horizontal, QApplication::translate("XraySqlUserManagementWidget", "Last Name"));
	p_sql_table_model->setHeaderData(5, Qt::Horizontal, QApplication::translate("XraySqlUserManagementWidget", "Rank"));
	p_sql_table_model->setHeaderData(6, Qt::Horizontal, QApplication::translate("XraySqlUserManagementWidget", "Phone Number"));
	p_sql_table_model->setHeaderData(7, Qt::Horizontal, tr("Email"));

	ui->tableView_2->setModel(p_sql_table_model);
	//ui->tableView_2->resizeColumnsToContents();
	p_sql_table_model->database().transaction();
}

void XraySqlUserManagementWidget::on_delUButton_clicked()
{
	if (QMessageBox::Yes == QMessageBox(QMessageBox::Question, windowTitle(), QApplication::translate("XraySqlUserManagementWidget", "Are you sure you want to delete all user accounts?"), QMessageBox::Yes | QMessageBox::No).exec())
	{
		auto q = db.query();
		q.prepare("DELETE FROM sys_users WHERE rank != 0 AND rank != -1");
		if (q.exec())
		{
			ui->adminLabel->setText(QApplication::translate("XraySqlUserManagementWidget", "All users have been deleted. You must save the changes if you are sure about this deletion."));
			updateUsersTable();
		}
	}
}

void XraySqlUserManagementWidget::on_delAButton_clicked()
{
	if (QMessageBox::Yes == QMessageBox(QMessageBox::Question, windowTitle(), QApplication::translate("XraySqlUserManagementWidget", "Are you sure you want to delete all admins?\n(This won't delete regular users and you.)"), QMessageBox::Yes | QMessageBox::No).exec())
	{
		auto q = db.query();
		q.prepare("DELETE FROM sys_users WHERE rank != 1 AND username != \"" + m_username + "\"");
		if (q.exec())
		{
			ui->adminLabel->setText(QApplication::translate("XraySqlUserManagementWidget", "All other admins have been deleted. You must save the changes if you are sure about this deletion."));
			updateAdminsTable();
		}
	}
}

void XraySqlUserManagementWidget::on_stackedWidget_currentChanged(int _arg)
{
	if (_arg == 0 && m_logged_in)
		updateUsersTable();

	if (_arg == 1 && m_logged_in)
		updateAdminsTable();
}

void XraySqlUserManagementWidget::on_enterToAppButton_clicked()
{
	emit emitEnterToApp();
}