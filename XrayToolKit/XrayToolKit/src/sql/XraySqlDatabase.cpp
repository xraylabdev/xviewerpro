﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/05
** filename: 	XraySqlDatabase.cpp
** file base:	XraySqlDatabase
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a ability to connect to SQLITE database.
****************************************************************************/

#include "XraySqlDatabase.h"

#include <QSqlError>
#include <QDesktopServices>
#include <QDir>
#include <QDebug>
#include <QMessageBox>

XRAYLAB_USING_NAMESPACE

XraySqlDatabase::XraySqlDatabase()
{
}

XraySqlDatabase::~XraySqlDatabase()
{
	disconnect();
}

//bool XraySqlDatabase::connectToMSSQL(QString driver, QString server, int port, QString database, QString login, QString password)
//{
//	m_db.setDatabaseName(connection_string_sqlauth.arg(driver).arg(server).arg(port).arg(database).arg(login).arg(password));
//
//	return db.open();
//}
//
//bool XraySqlDatabase::connectToMSSQL(QString driver, QString server, int port, QString database)
//{
//	m_db.setDatabaseName(connection_string_winauth.arg(driver).arg(server).arg(port).arg(database));
//
//	return db.open();
//}

bool XraySqlDatabase::connect(const QString& _databaseFilePath)
{
	// if a database is currently opened, then close the connection.
	if (m_db.isOpen())
		m_db.close();

	m_db = QSqlDatabase::database();
	if (!m_db.isValid())
	{
		m_db = QSqlDatabase::addDatabase("QSQLITE");
		if (!m_db.isValid())
			QMessageBox::critical(nullptr, "SQL Error", QString("Something went wrong! Couldn't add database.\n") + qPrintable(m_db.lastError().text()));
	}

	m_db.setDatabaseName(_databaseFilePath);

	return m_db.open();
}

bool XraySqlDatabase::connect()
{
	return connect(databaseLocalFilePath());
}
bool XraySqlDatabase::disconnect()
{
	if (m_db.isOpen())
	{
		m_db.close();
		return true;
	}

	return false;
}
bool XraySqlDatabase::isConnected() const
{
	return m_db.isOpen();
}

QSqlQuery XraySqlDatabase::query()
{
	QSqlQuery q(m_db);
	return q;
}
QSqlQuery XraySqlDatabase::query(const QString& _databaseName)
{
	return QSqlQuery(QSqlDatabase::database(_databaseName));
}

QStringList XraySqlDatabase::tables()
{
	if (m_db.isOpen())
		return m_db.tables();
	return QStringList();
}
bool XraySqlDatabase::tableExists(const QString& _table)
{
	return m_db.tables().contains(_table);
}

QString XraySqlDatabase::databaseLocalFilePath()
{
	const auto dir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
	const auto path = QDir::cleanPath(QString("%1/xam_database.s3db").arg(dir));
	if (!QFile::exists(path))
	{
		if (!QDir().mkpath(dir))
			QMessageBox::critical(nullptr, "Database Directory", QString("Failed to create a directory at ") + qPrintable(QDir(dir).absolutePath()));
	}

	return path;
}