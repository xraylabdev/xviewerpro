﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/08
** filename: 	XrayQDateTime.cpp
** file base:	XrayQDateTime
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a class for date and time.
****************************************************************************/

#include "XrayQDateTime.h"

#include <QDebug>

XRAYLAB_USING_NAMESPACE

#include <QLocale>

XrayQDateTime::XrayQDateTime(const QDateTime& other) : 
	QDateTime(other)
{
}

QString XrayQDateTime::formatDate(const QString& format) const
{
	auto pos = format.indexOf("MMMM");
	if (pos == -1)
		return toString(format);
	auto d = toString(QString(format).remove(pos, 4));
	return d.insert(pos, QLocale().monthName(date().month()));
}

int XrayQDateTime::timeDiff() const
{
	if (!isValid() || timeSpec() == Qt::UTC)
		return 0;

	auto utc = toUTC();
	utc.setTimeSpec(Qt::LocalTime);
	return utc.secsTo(toLocalTime());
}

QString XrayQDateTime::toStringZ(const QString& format) const
{
	if (!isValid()) 
		return "";

	auto diffsec = timeDiff();
	return toString(format) + QString(" %1%2").arg(diffsec >= 0 ? "+" : "-")
		.arg(QTime(0, 0).addSecs(diffsec >= 0 ? diffsec : -diffsec).toString("hh:mm"));
}