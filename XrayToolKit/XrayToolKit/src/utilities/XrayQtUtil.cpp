﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/01
** filename: 	XrayQtUtil.cpp
** file base:	XrayQtUtil
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides utility functions for Qt applications.
****************************************************************************/

#include "XrayQtUtil.h"

#include <QApplication>
#include <QLabel>
#include <QLayout>
#include <QDesktopServices>
#include <QDir>
#include <QTime>
#include <QDebug>
#include <QMessageBox>
#include <QCryptographicHash>
#include <QByteArray>
#include <QQueue>
#include <QSet>

XRAYLAB_USING_NAMESPACE

QMainWindow* XrayQtUtil::getMainWindow(const QString& _objectName)
{
	foreach(QWidget* w, qApp->topLevelWidgets())
	{
		if (QMainWindow* mainWin = qobject_cast<QMainWindow*>(w))
		{
			if(mainWin->objectName() == _objectName)
				return mainWin;
		}
	}
	return nullptr;
}

void XrayQtUtil::deleteChilds(QLayout* layout)
{
	QLayoutItem* child;
	while (layout->count() != 0)
	{
		child = layout->takeAt(0);
		if (child->layout() != 0)
			deleteChilds(child->layout());
		else if (child->widget() != 0)
			delete child->widget();

		delete child;
	}
}

void XrayQtUtil::deleteChildWidgets(QLayoutItem *item)
{
	if (item->layout())
	{
		// Process all child items recursively.
		while (item->layout()->count() > 0)
			deleteChildWidgets(item->layout()->itemAt(0));
	}
	delete item->widget();
}
void XrayQtUtil::deleteChildLabels(QLayoutItem *item)
{
	if (item->layout())
	{
		// Process all child items recursively.
		while (item->layout()->count() > 0)
			deleteChildWidgets(item->layout()->itemAt(0));
	}
	auto label = qobject_cast<QLabel*>(item->widget());
	if(label) delete item->widget();
}

void XrayQtUtil::clearLayout(QLayout* layout)
{
	if (!layout)
		return;

	QLayoutItem* item;
	while ((item = layout->takeAt(0)) != nullptr)
	{
		if (item->widget())
		{
			item->widget()->setVisible(false);
			layout->removeWidget(item->widget());
		}
		else if (item->layout())
		{
			clearLayout(item->layout());
		}
	}

	delete layout;
}

void XrayQtUtil::removeLayout(QWidget* widget)
{
	auto layout = widget->layout();
	clearLayout(layout);

	// this works fine as well.
	//if (layout != nullptr)
	//{
	//	QLayoutItem* item;
	//	while ((item = layout->takeAt(0)) != nullptr)
	//	{
	//		if (layout->widget())
	//			layout->widget()->setVisible(false);
	//		layout->removeItem(item);
	//	}
	//	delete layout;
	//}
}


QWidgetAction* XrayQtUtil::createTextSeparator(QMenu* _menu, const QString& _text)
{
	auto label = new QLabel(_text);
	label->setMinimumWidth(_menu->minimumWidth() - 4);
	label->setMinimumHeight(_menu->height());
	label->setStyleSheet("background: #FF4B4B4B;");
	label->setAlignment(Qt::AlignCenter);
	auto separator = new QWidgetAction(_menu);
	separator->setDefaultWidget(label);
	return separator;
}

QString XrayQtUtil::getUuidWithoutCurlyBraces(const QUuid& _uuid)
{
	return _uuid.toString().mid(1, _uuid.toString().length() - 2);
}

QString XrayQtUtil::convertSecToTime(int _seconds, bool _withDays)
{
	QString time;
	if (_withDays)
	{
		auto days = _seconds / (24 * 3600);
		_seconds = _seconds % (24 * 3600);
		auto hours = _seconds / 3600;
		_seconds %= 3600;
		auto minutes = _seconds / 60;
		_seconds %= 60;
		auto seconds = _seconds;
		auto t = (hours < 10 ? "0" + QString::number(hours) : QString::number(hours)) + ":" +
			(minutes < 10 ? "0" + QString::number(minutes) : QString::number(minutes)) + ":" +
			(seconds < 10 ? "0" + QString::number(seconds) : QString::number(seconds));
		time = QString::number(days) + (days < 2 ? " day " : " days ") + t;
	}
	else
	{
		auto minutes = _seconds / 60;
		auto seconds = _seconds % 60;
		auto hours = minutes / 60;
		minutes = minutes % 60;
		time = (hours < 10 ? "0" + QString::number(hours) : QString::number(hours)) + ":" +
			(minutes < 10 ? "0" + QString::number(minutes) : QString::number(minutes)) + ":" +
			(seconds < 10 ? "0" + QString::number(seconds) : QString::number(seconds));
	}
	return time;
}
QString XrayQtUtil::convertToTime(int _seconds)
{
	auto days = _seconds / 86400;
	auto hours = (_seconds - (days * 86400)) / 3600;
	auto mins = (_seconds - (days * 86400) - (hours * 3600)) / 60;
	_seconds = (_seconds - (days * 86400) - (hours * 3600) - (mins * 60));

	const QTime time(hours, mins, _seconds);
	if (days > 0)
		return QString::number(days) + (days < 2 ? " day " : " days ") + time.toString(QStringLiteral("hh:mm:ss"));

	return time.toString(QStringLiteral("hh:mm:ss"));
}
int XrayQtUtil::calculateRemainingSec(int _totalSize, int _processedSize, int _speed)
{
	if ((_speed != 0) && (_totalSize != 0))
		return (_totalSize - _processedSize) / _speed;
		
	return 0;
}

QString XrayQtUtil::base64Encode(const QString& _string)
{
	QByteArray ba;
	ba.append(_string);
	return ba.toBase64();
}
QString XrayQtUtil::base64Decode(const QString& _string)
{
	QByteArray ba;
	ba.append(_string);
	return QByteArray::fromBase64(ba);
}
QString XrayQtUtil::md5Encode(const QString& _string)
{
	QByteArray ba;
	ba.append(_string);
	return QString(QCryptographicHash::hash((ba), QCryptographicHash::Md5).toHex());
}

static uchar _RGB2GRAY(uchar r, uchar g, uchar b)
{
	return (uchar)((((qint32)((r << 5) + (r << 2) + (r << 1)))
		+ (qint32)((g << 6) + (g << 3) + (g << 1) + g)
		+ (qint32)((b << 4) - b)) >> 7);
}
QImage XrayQtUtil::convertToGray(const QImage& image)
{
	auto size = image.size();
	auto grayImage = QImage(size, QImage::Format_Grayscale8);
	int width = size.width();
	int height = size.height();
	auto rgbImageData = image.bits();
	auto grayImageData = grayImage.bits();
	if (image.allGray())
	{
		grayImage = QImage(image);
		return grayImage;
	}
	// If the width is not a multiple of 4, will automatically add bytes, so that it is aligned to a multiple of 4.
	auto realWidth1 = image.bytesPerLine();
	auto realWidth2 = grayImage.bytesPerLine();
	auto backup1 = rgbImageData;
	auto backup2 = grayImageData;
	for (auto i = 0; i < height; i++)
	{
		for (auto j = 0; j < width; j++)
		{
			auto R = *rgbImageData;
			auto G = *(rgbImageData + 1);
			auto B = *(rgbImageData + 2);
			*grayImageData = _RGB2GRAY(R, G, B);
			rgbImageData += 3;
			grayImageData++;
		}
		rgbImageData = backup1 + realWidth1 * i;
		grayImageData = backup2 + realWidth2 * i;
	}

	return grayImage;
}
void rotate90_YUV420P(uchar *dst, const uchar *src, int srcWidth, int srcHeight)
{
	static int nWidth = 0, nHeight = 0;
	static int wh = 0;
	static int uvHeight = 0;
	if (srcWidth != nWidth || srcHeight != nHeight)
	{
		nWidth = srcWidth;
		nHeight = srcHeight;
		wh = srcWidth * srcHeight;
		uvHeight = srcHeight >> 1; //uvHeight = height / 2
	}

	// rotate Y  
	int k = 0;
	for (int i = 0; i < srcWidth; i++) 
	{
		int nPos = 0;
		for (int j = 0; j < srcHeight; j++) 
		{
			dst[k] = src[nPos + i];
			k++;
			nPos += srcWidth;
		}
	}

	for (int i = 0; i < srcWidth; i += 2) 
	{
		int nPos = wh;
		for (int j = 0; j < uvHeight; j++) 
		{
			dst[k] = src[nPos + i];
			dst[k + 1] = src[nPos + i + 1];
			k += 2;
			nPos += srcWidth;
		}
	}
	return;
}

void rotateN90_YUV420P(uchar *dst, const uchar *src, int srcWidth, int height)
{
	static int nWidth = 0, nHeight = 0;
	static int wh = 0;
	static int uvHeight = 0;
	if (srcWidth != nWidth || height != nHeight)
	{
		nWidth = srcWidth;
		nHeight = height;
		wh = srcWidth * height;
		uvHeight = height >> 1; //uvHeight = height / 2
	}

	// rotate Y  
	int k = 0;
	for (int i = 0; i < srcWidth; i++) 
	{
		int nPos = srcWidth - 1;
		for (int j = 0; j < height; j++)
		{
			dst[k] = src[nPos - i];
			k++;
			nPos += srcWidth;
		}
	}

	for (int i = 0; i < srcWidth; i += 2) 
	{
		int nPos = wh + srcWidth - 1;
		for (int j = 0; j < uvHeight; j++)
		{
			dst[k] = src[nPos - i - 1];
			dst[k + 1] = src[nPos - i];
			k += 2;
			nPos += srcWidth;
		}
	}

	return;
}

void XrayQtUtil::rotate90YUV420P(uchar* _dst, const uchar* _src, int _srcWidth, int _srcHeight, int _mode)
{
	switch (_mode)
	{
	case 1:
		rotate90_YUV420P(_dst, _src, _srcWidth, _srcHeight);
		break;
	case -1:
		rotateN90_YUV420P(_dst, _src, _srcWidth, _srcHeight);
		break;
	default:
		break;
	}
	return;
}

// mirroring with the Y axis
void mirrorY_YUV420P(uchar *dst, const uchar *src, int srcWidth,
	int srcHeight)
{
	// mirror Y
	int k = 0;
	int nPos = -1;
	for (int j = 0; j < srcHeight; j++) 
	{
		nPos += srcWidth;
		for (int i = 0; i < srcWidth; i++)
		{
			dst[k] = src[nPos - i];
			k++;
		}
	}

	int uvHeight = srcHeight >> 1; // uvHeight = height / 2
	for (int j = 0; j < uvHeight; j++) 
	{
		nPos += srcWidth;
		for (int i = 0; i < srcWidth; i += 2)
		{
			dst[k] = src[nPos - i - 1];
			dst[k + 1] = src[nPos - i];
			k += 2;
		}
	}
}

// mirror with XY axis
void mirrorXY_YUV420P(uchar *dst, const uchar *src, int width, int srcHeight)
{
	static int nWidth = 0, nHeight = 0;
	static int wh = 0;
	static int nUVPos = 0;
	static int uvHeight = 0;
	if (width != nWidth || srcHeight != nHeight)
	{
		nWidth = width;
		nHeight = srcHeight;

		wh = width * srcHeight;
		uvHeight = srcHeight >> 1; //uvHeight = height / 2
		nUVPos = wh + uvHeight * width - 1;
	}

	// mirror Y 
	int k = 0;
	int nPos = wh - 1;
	for (int j = 0; j < srcHeight; j++) 
	{
		for (int i = 0; i < width; i++)
		{
			dst[k] = src[nPos - i];
			k++;
		}
		nPos -= width;
	}

	nPos = nUVPos;
	for (int j = 0; j < uvHeight; j++)
	{
		for (int i = 0; i < width; i += 2)
		{
			dst[k] = src[nPos - i - 1];
			dst[k + 1] = src[nPos - i];
			k += 2;
		}
		nPos -= width;
	}
}

// mirroring with the X axis
void mirrorX_YUV420P(uchar *dst, const uchar *src, int width, int srcHeight)
{
	static int nWidth = 0, nHeight = 0;
	static int wh = 0;
	static int nUVPos = 0;
	static int uvHeight = 0;
	if (width != nWidth || srcHeight != nHeight)
	{
		nWidth = width;
		nHeight = srcHeight;

		wh = width * srcHeight;
		uvHeight = srcHeight >> 1; // uvHeight = height / 2
		nUVPos = wh + uvHeight * width;
	}

	// mirror Y 
	int k = 0;
	int nPos = wh - 1;
	for (int j = 0; j < srcHeight; j++) {
		nPos -= width;
		for (int i = 0; i < width; i++)
		{
			dst[k] = src[nPos + i];
			k++;
		}
	}

	nPos = nUVPos;
	for (int j = 0; j < uvHeight; j++) {
		nPos -= width;
		for (int i = 0; i < width; i += 2)
		{
			dst[k] = src[nPos + i];
			dst[k + 1] = src[nPos + i + 1];
			k += 2;
		}
	}
}

void XrayQtUtil::mirrorYUV420P(uchar* _dst, const uchar* _src, int _srcWidth, int _srcHeight, int _mode)
{
	switch (_mode) 
	{
	case 0:
		return mirrorY_YUV420P(_dst, _src, _srcWidth, _srcHeight);
		break;
	case 1:
		return mirrorX_YUV420P(_dst, _src, _srcWidth, _srcHeight);
	case -1:
		return mirrorXY_YUV420P(_dst, _src, _srcWidth, _srcHeight);
	default:
		break;
	}
}

uint qHash(const QPoint &point, uint seed)
{
	return qHash(point.x() ^ seed) ^ point.y();
}
// https://en.wikipedia.org/wiki/Flood_fill#Alternative_implementations
// "[...] use a loop for the west and east directions as an optimization to avoid the overhead of stack or queue management."
QVector<QPoint> XrayQtUtil::imagePixelFloodFill(const QImage *image, const QPoint &startPos, const QColor &targetColour, const QColor &replacementColour)
{
	QSet<QPoint> filledPositions;
	const QRect imageBounds(0, 0, image->width(), image->height());
	if (!imageBounds.contains(startPos)) 
		return QVector<QPoint>();

	if (image->pixelColor(startPos) == replacementColour) 
		return QVector<QPoint>();		// The pixel at startPos is already the colour that we want to replace it with.

	if (image->pixelColor(startPos) != targetColour) 
		return QVector<QPoint>();

	QQueue<QPoint> queue;
	queue.append(startPos);
	filledPositions.insert(startPos);

	for (int i = 0; i < queue.size(); ++i) 
	{
		QPoint node = queue.at(i);
		QPoint west = node;
		QPoint east = node;

		while (1) 
		{
			QPoint newWest = west - QPoint(1, 0);
			if (imageBounds.contains(newWest) && !filledPositions.contains(newWest) && image->pixelColor(newWest) == targetColour)
				west = newWest;
			else
				break;
		}

		while (1)
		{
			QPoint newEast = east + QPoint(1, 0);
			if (imageBounds.contains(newEast) && !filledPositions.contains(newEast) && image->pixelColor(newEast) == targetColour) 
				east = newEast;
			else 
				break;
		}

		for (int x = west.x(); x <= east.x(); ++x) 
		{
			const QPoint n = QPoint(x, node.y());
			// This avoids startPos being added twice.
			if (!filledPositions.contains(n)) 
			{
				queue.append(n);
				filledPositions.insert(n);
			}

			const QPoint north(n - QPoint(0, 1));
			if (imageBounds.contains(north) && !filledPositions.contains(north) && image->pixelColor(north) == targetColour) 
			{
				queue.append(north);
				filledPositions.insert(north);
			}

			const QPoint south(n + QPoint(0, 1));
			if (imageBounds.contains(south) && !filledPositions.contains(south) && image->pixelColor(south) == targetColour)
			{
				queue.append(south);
				filledPositions.insert(south);
			}
		}
	}

	// TODO: convert from QSet => QVector manually, as this is just silly
	return filledPositions.toList().toVector();
}

QVector<QPoint> XrayQtUtil::imageGreedyPixelFill(const QImage *image, const QPoint &startPos, const QColor &targetColour, const QColor &replacementColour)
{
	QVector<QPoint> filledPositions;
	const QRect imageBounds(0, 0, image->width(), image->height());
	if (!imageBounds.contains(startPos))
		return filledPositions;

	if (image->pixelColor(startPos) == replacementColour)
		return filledPositions;		// The pixel at startPos is already the colour that we want to replace it with.

	for (int y = 0; y < image->height(); ++y)
	{
		for (int x = 0; x < image->width(); ++x) 
		{
			if (image->pixelColor(x, y) == targetColour)
				filledPositions.append(QPoint(x, y));
		}
	}

	return filledPositions;
}

template <typename Type>
QRect XrayQtUtil::boundingRect(const QVector<Type>& points)
{
	auto pd = points.constData();
	auto pe = pd + points.size();
	if (pd == pe)
		return QRect(0, 0, 0, 0);
	int minx, maxx, miny, maxy;
	minx = maxx = pd->x();
	miny = maxy = pd->y();
	++pd;
	for (; pd != pe; ++pd)
	{
		if (pd->x() < minx)
			minx = pd->x();
		else if (pd->x() > maxx)
			maxx = pd->x();
		if (pd->y() < miny)
			miny = pd->y();
		else if (pd->y() > maxy)
			maxy = pd->y();
	}
	return QRect(QPoint(minx, miny), QPoint(maxx, maxy));
}
template QRect XrayQtUtil::boundingRect(const QVector<QPoint>& points);
template QRect XrayQtUtil::boundingRect(const QVector<QPointF>& points);
template <typename Type>
void XrayQtUtil::scalePoints(QVector<Type>& points, double scale)
{
	auto cen = boundingRect(points).center();
	for (std::size_t i = 0; i < points.size(); i++)
		points[i] = (points[i] - cen) * scale + cen;
}
template void XrayQtUtil::scalePoints(QVector<QPoint>& points, double scale);
template void XrayQtUtil::scalePoints(QVector<QPointF>& points, double scale);
void XrayQtUtil::scalePolygon(QPolygonF& points, double scale)
{
	auto cen = points.boundingRect().center();
	for (std::size_t i = 0; i < points.size(); i++)
		points[i] = (points[i] - cen) * scale + cen;
}


// create a thread-safe singleton
//QScopedPointer<GifWidget> GifWidget::self;
//GifWidget *GifWidget::Instance()
//{
//	if (self.isNull()) {
//		static QMutex mutex;
//		QMutexLocker locker(&mutex);
//		if (self.isNull()) {
//			self.reset(new GifWidget);
//		}
//	}
//
//	return self.data();
//}
//For example, you can add a search button to the left side of the text box, and set the icon with the button.
//QPushButton *btn = new QPushButton;
//btn->resize(30, ui->lineEdit->height());
//QHBoxLayout *layout = new QHBoxLayout(ui->lineEdit);
//layout->setMargin(0);
//layout->addStretch();
//layout->addWidget(btn);
//The administrator runs the program and is limited to the MSVC compiler.
//QMAKE_LFLAGS += / MANIFESTUAC:\"level=\'requireAdministrator\' uiAccess=\'false\'\" #Run as administrator
//QMAKE_LFLAGS += / SUBSYSTEM:WINDOWS, \"5.01\" #VS2013 Running on XP