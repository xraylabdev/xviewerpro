﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/03/12
** filename: 	XrayQImageLayer.cpp
** file base:	XrayQImageLayer
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides utility functions for Qt applications.
****************************************************************************/

#include "XrayQImageLayer.h"

#include <QBuffer>
#include <QJsonObject>

XRAYLAB_USING_NAMESPACE

XrayQImageLayer::XrayQImageLayer(QObject *parent, const QImage &image) :
	QObject(parent),
	mVisible(true),
	mOpacity(1.0),
	mImage(image)
{
}

XrayQImageLayer::~XrayQImageLayer()
{
}

QString XrayQImageLayer::name() const
{
	return mName;
}

void XrayQImageLayer::setName(const QString &name)
{
	if (name == mName)
		return;

	mName = name;
	setObjectName(mName);
	emit nameChanged();
}

QSize XrayQImageLayer::size() const
{
	return !mImage.isNull() ? mImage.size() : QSize();
}

void XrayQImageLayer::setSize(const QSize &newSize)
{
	if (newSize == size())
		return;

	mImage = mImage.copy(0, 0, newSize.width(), newSize.height());
}

QImage *XrayQImageLayer::image()
{
	return &mImage;
}

const QImage *XrayQImageLayer::image() const
{
	return &mImage;
}

qreal XrayQImageLayer::opacity() const
{
	return mOpacity;
}

void XrayQImageLayer::setOpacity(const qreal &opacity)
{
	if (qFuzzyCompare(opacity, mOpacity))
		return;

	mOpacity = opacity;
	emit opacityChanged();
}

bool XrayQImageLayer::isVisible() const
{
	return mVisible;
}

void XrayQImageLayer::setVisible(bool visible)
{
	if (visible == mVisible)
		return;

	mVisible = visible;
	emit visibleChanged();
}

void XrayQImageLayer::read(const QJsonObject &jsonObject)
{
	setName(jsonObject.value("name").toString());
	setOpacity(jsonObject.value("opacity").toDouble());
	setVisible(jsonObject.value("visible").toBool());

	const QString base64ImageData = jsonObject.value("imageData").toString();
	QByteArray imageData = QByteArray::fromBase64(base64ImageData.toLatin1());
	mImage.loadFromData(imageData, "png");
}

void XrayQImageLayer::write(QJsonObject &jsonObject)
{
	jsonObject["name"] = mName;
	jsonObject["opacity"] = mOpacity;
	jsonObject["visible"] = mVisible;

	QByteArray imageData;
	QBuffer buffer{ &imageData };
	buffer.open(QIODevice::WriteOnly);
	mImage.save(&buffer, "png");
	const QByteArray base64ImageData = buffer.data().toBase64();
	jsonObject["imageData"] = QString::fromLatin1(base64ImageData);
}