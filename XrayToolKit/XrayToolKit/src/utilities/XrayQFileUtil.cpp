﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/04
** filename: 	XrayQFileUtil.cpp
** file base:	XrayQFileUtil
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides utility functions related to QFile.
****************************************************************************/

#include "XrayQFileUtil.h"

#include <QApplication>
#include <QCryptographicHash>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <QMessageBox>
#include <QTemporaryFile>
#include <QProcess>

XRAYLAB_USING_NAMESPACE

QString XrayQFileUtil::getStandardPath(const QString& _subfolder, const QString& _directory)
{
	auto path = _directory;

	auto sub(_subfolder);

	if (!sub.startsWith("/") && !path.endsWith("/"))
		sub.prepend("/");

	if (!sub.endsWith("/"))
		sub.append("/");

	path.append(sub);
	QDir d(path);
	if (!d.exists(path))
		d.mkpath(path);

	return path;
}

QString XrayQFileUtil::getStandardPath(const QString& _subfolder, QStandardPaths::StandardLocation _location)
{
	auto path = QStandardPaths::writableLocation(_location);

	auto sub(_subfolder);

	if (!sub.startsWith("/"))
		sub.prepend("/");

	if (!sub.endsWith("/"))
		sub.append("/");

	path.append(sub);
	QDir d(path);
	if (!d.exists(path)) 
		d.mkpath(path);

	return path;
}
QString XrayQFileUtil::getStandardPath(const QString& _subfolder, const QString& _fileName, QStandardPaths::StandardLocation _location)
{
	return getStandardPath(_subfolder, _location) + _fileName;
}
QString XrayQFileUtil::getTempFileName(const QString& _prefix, const QString& _extension)
{
	auto nameTemplate = QDir::tempPath() + "/" + _prefix + QString::number(QCoreApplication::applicationPid()) + "_%1." + _extension;

	for (auto counter = 0; counter < std::numeric_limits<int>::max(); ++counter)
	{
		auto name = nameTemplate.arg(QString::number(qrand()));
		if (false == QFile::exists(name))
			return name;
	}

	return QString();
}

QStringList XrayQFileUtil::getSubNumericalDirs(const QString& _path)
{
	if (!QFile::exists(_path))
		return {};

	QRegExp re("\\d*");  // a digit (\d), zero or more times (*)
	QStringList paths;
	QDirIterator it(_path, QDir::Dirs | QDir::NoDotAndDotDot, QDirIterator::NoIteratorFlags);

	while (it.hasNext())
	{
		auto p = it.next();
		if (re.exactMatch(p.split('/').back()))
			paths.push_back(p);
	}

	return paths;
}
QStringList XrayQFileUtil::getSubDirs(const QString& _path, const QStringList& _nameFilters, QDir::Filters _filters, QDirIterator::IteratorFlags _flags)
{
	QStringList paths;

	if (!QFile::exists(_path))
		return paths;

	QDirIterator it(_path, _nameFilters, _filters, _flags);

	while (it.hasNext())
	{
		paths.push_back(it.next());
	}

	return paths;
}
QStringList XrayQFileUtil::getSubDirFiles(const QString& _path, const QStringList& _nameFilters, QDir::Filters _filters, QDirIterator::IteratorFlags _flags)
{
	QStringList paths;

	if (!QFile::exists(_path))
		return paths;

	QDirIterator it(_path, QStringList(), _filters, _flags);

	auto hasNameFilters = !_nameFilters.empty();

	while (it.hasNext())
	{
		auto p = it.next();

		QFileInfo file(p);
		if (file.isDir())
		{
			paths.push_back(p);
		}
		else
		{
			if (hasNameFilters)
			{
				auto ext = file.suffix();
				for (const auto& j : _nameFilters)
				{
					if (j.endsWith(ext, Qt::CaseInsensitive))
					{
						paths.push_back(p);
						break;
					}
				}
			}
			else
			{
				paths.push_back(p);
			}
		}
	}

	return paths;
}

QByteArray XrayQFileUtil::getFileMd5Sum(const QString& _filePath)
{
	QFile localFile(_filePath);
	if (!localFile.open(QFile::ReadOnly))
		return 0;

	QCryptographicHash ch(QCryptographicHash::Md5);

	quint64 totalBytes = 0;
	quint64 bytesWritten = 0;
	quint64 bytesToWrite = 0;
	quint64 loadSize = 1024 * 4;
	QByteArray buf;

	totalBytes = localFile.size();
	bytesToWrite = totalBytes;

	while (1)
	{
		if (bytesToWrite > 0)
		{
			buf = localFile.read(qMin(bytesToWrite, loadSize));
			ch.addData(buf);
			bytesWritten += buf.length();
			bytesToWrite -= buf.length();
			buf.resize(0);
		}
		else
		{
			break;
		}

		if (bytesWritten == totalBytes)
		{
			break;
		}
	}

	localFile.close();
	auto md5 = ch.result();
	return md5;
}

QString XrayQFileUtil::getFileMd5SumString(const QString& _filePath)
{
	return getFileMd5Sum(_filePath).toHex();
}

bool XrayQFileUtil::removeDirectory(const QString& _dirName)
{
	QDir dir(_dirName);
	QString tmpdir = "";
	if (!dir.exists())
		return false;

	auto fileInfoList = dir.entryInfoList();
	foreach(QFileInfo fileInfo, fileInfoList) 
	{
		if (fileInfo.fileName() == "." || fileInfo.fileName() == "..")
			continue;

		if (fileInfo.isDir()) 
		{
			tmpdir = _dirName + ("/") + fileInfo.fileName();
			removeDirectory(tmpdir);
			dir.rmdir(fileInfo.fileName()); // remove subdirectory
		}
		else if (fileInfo.isFile()) 
		{
			QFile tmpFile(fileInfo.fileName());
			dir.remove(tmpFile.fileName()); // delete temporary files
		}
	}

	dir.cdUp();            /*! Return to the parent directory, because you can only delete this directory if you return to the parent directory. */
	if (dir.exists(_dirName)) 
	{
		if (!dir.rmdir(_dirName))
			return false;
	}
	return true;
}

QString XrayQFileUtil::getHumanReadableSize(const qlonglong& bytes)
{
	// First solution
	//static const char* suffix[] = { "B","KB","MB","GB","TB" };
	//int exp = 0;
	//if (_size)
	//	exp = std::min(static_cast<int>(std::log(_size) / std::log(1024)), static_cast<int>(sizeof(suffix) / sizeof(suffix[0]) - 1));
	//return QString().setNum(_size / std::pow(1024, exp), 'f', 2).append(suffix[exp]);

	QString number;

	if (bytes < 0x400) //If less than 1 KB, report in B
	{
		number = QLocale::system().toString(bytes);
		number.append(" B");
		return number;
	}
	else
	{
		if (bytes >= 0x400 && bytes < 0x100000) // If less than 1 MB, report in KB, unless rounded result is 1024 KB, then report in MB
		{
			qlonglong result = (bytes + (0x400 / 2)) / 0x400;

			if (result < 0x400)
			{
				number = QLocale::system().toString(result);
				number.append(" KB");
				return number;
			}
			else
			{
				qlonglong result = (bytes + (0x100000 / 2)) / 0x100000;
				number = QLocale::system().toString(result);
				number.append(" MB");
				return number;
			}
		}
		else
		{
			if (bytes >= 0x100000 && bytes < 0x40000000) // If less than 1 GB, report in MB, unless rounded result is 1024 MB, then report in GB
			{
				qlonglong result = (bytes + (0x100000 / 2)) / 0x100000;

				if (result < 0x100000)
				{
					number = QLocale::system().toString(result);
					number.append(" MB");
					return number;
				}
				else
				{
					qlonglong result = (bytes + (0x40000000 / 2)) / 0x40000000;
					number = QLocale::system().toString(result);
					number.append(" GB");
					return number;
				}
			}
			else
			{
				if (bytes >= 0x40000000 && bytes < 0x10000000000) // If less than 1 TB, report in GB, unless rounded result is 1024 GB, then report in TB
				{
					qlonglong result = (bytes + (0x40000000 / 2)) / 0x40000000;

					if (result < 0x40000000)
					{
						number = QLocale::system().toString(result);
						number.append(" GB");
						return number;
					}
					else
					{
						qlonglong result = (bytes + (0x10000000000 / 2)) / 0x10000000000;
						number = QLocale::system().toString(result);
						number.append(" TB");
						return number;
					}
				}
				else
				{
					qlonglong result = (bytes + (0x10000000000 / 2)) / 0x10000000000; // If more than 1 TB, report in TB
					number = QLocale::system().toString(result);
					number.append(" TB");
					return number;
				}
			}
		}
	}

	return number;
}
QString XrayQFileUtil::getHumanReadableFileSize(const QString& _fileName)
{
	if (!QFile::exists(_fileName))
		return "0 B";
	return getHumanReadableSize(QFileInfo(_fileName).size());
}

QString XrayQFileUtil::read(const QString& _filePath, FileType _type)
{
	if (_filePath.isEmpty())
		return QString();

	QFile file(_filePath);
	QString fileContent;
	if (file.open(QIODevice::ReadOnly))
	{
		if (_type == FileType::Binary)
		{
			QDataStream in(&file);
			in >> fileContent;
		}
		else
		{
			QString line;
			QTextStream t(&file);
			do
			{
				line = t.readLine();
				fileContent += line;
			} while (!line.isNull());
		}

		file.close();
	}
	else
	{
		return QString();
	}

	return fileContent;
}

bool XrayQFileUtil::write(const QString& _data, const QString& _filePath, FileType _type, QFile::OpenMode _mode)
{
	QFile file(_filePath);
	if (!file.open(_mode))
		return false;

	if (_type == FileType::Binary)
	{
		QDataStream out(&file);
		out << _data;
	}
	else
	{
		QTextStream out(&file);
		out << _data;
	}

	file.close();
	return true;
}

// copy file from src to dst with percentage.
bool XrayQFileUtil::copy(const QString& _source, QString& _destination, bool _overwrite, bool _move)
{
	QFileInfo sourceInfo = QFileInfo(_source);
	QFileInfo destinationInfo = QFileInfo(_destination);

	if (!sourceInfo.exists())
	{
		qDebug("File Copy Failed - Source File does not exists.");
		return false;
	}
	else if (!sourceInfo.isFile())
	{
		qDebug("File Copy Failed - Source is not a file.");
		return false;
	}
	else if (!destinationInfo.exists())
	{
		qDebug("File Copy Failed - Destination cannot be found.");
		return false;
	}

	if (destinationInfo.isDir())
	{
		_destination = (QFileInfo(QDir(destinationInfo.absoluteFilePath()), sourceInfo.fileName())).filePath();
	}
	else if (!destinationInfo.isFile())
	{
		qDebug("File Copy failed - Destination is neither a file or directory.");
		return false;
	}

	if (!_overwrite && QFile::exists(_destination))
	{
		qDebug("File Copy failed - Destination file exists, overwrite disabled.");
		return false;
	}

	QFile sourceFile(_source);
	QFile destinationFile(_destination);

	if (sourceFile.open(QIODevice::ReadOnly) && destinationFile.open(QIODevice::WriteOnly))
	{
		// show progress
		auto nPercent = 0.0;
		auto nNewPercent = 0.0;
		auto dataLength = 32 * 1024;
		auto data = new char[dataLength];
		auto completed = 0.0;
		auto nFsize = double(sourceFile.size());

		if (nFsize > 0)
		{
			auto dMn = 100.0 / nFsize;
			auto nCount = 0;
			while (!sourceFile.atEnd())
			{
				auto nReaded = sourceFile.read(data, dataLength);
				if (nReaded <= 0)
					continue;

				completed += nReaded;
				destinationFile.write(data, nReaded);
				destinationFile.flush();
				nNewPercent = nPercent;
				nCount++;

				if (nCount > 100 || dataLength != nReaded)
				{
					nPercent = completed * dMn;
					//db.SetPercent((qint64)nPercent, nSysID);
					//db.CloseDB();
					nCount = 0;
				}
			}
		}
		else
		{
			delete[] data;
			return false;
		}
		delete[] data;

		//db.SetPercent(100, nSysID);
		//db.CloseDB();

		if (_move)
		{
			if (!sourceFile.remove())
			{
				destinationFile.remove();
				sourceFile.close();
				destinationFile.close();
				qDebug("File Copy failed - Source file could not be removed.");
				return false;
			}
		}
		sourceFile.close();
		destinationFile.close();
	}
	else
	{
		sourceFile.close();
		destinationFile.close();
		qDebug("DBG: Error - Source or destination file could not be opened.");
		qDebug("File Copy failed - Source or destination file could not be opened.");
		return false;
	}
	return true;
}
void XrayQFileUtil::copy(const QString& src, const QString& dst)
{
	QFile sfile(src);
	QFileInfo dfile(dst);
	if (sfile.exists() && !dfile.exists()) 
	{
		if (sfile.copy(dst)) 
			printf("  created   %s\n", qPrintable(QDir::cleanPath(dst)));
		else
			qCritical("failed to create a file %s", qPrintable(QDir::cleanPath(dst)));
	}
}

bool XrayQFileUtil::replaceString(const QString &fileName, const QByteArray &before, const QByteArray &after)
{
	QFile file(fileName);
	if (!file.exists() || !file.open(QIODevice::ReadOnly))
		return false;

	QByteArray text = file.readAll();
	text.replace(before, after);
	file.close();

	if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
		return false;

	file.write(text);
	file.close();
	return true;
}

static QString diff(const QString& data1, const QString& data2)
{
	QTemporaryFile f1, f2;
	if (!f1.open() || f1.write(data1.toUtf8()) < 0) 
		return QString();

	if (!f2.open() || f2.write(data2.toUtf8()) < 0)
		return QString();

	f1.close();
	f2.close();

	if (!QFileInfo(f1).exists() || !QFileInfo(f2).exists())
	{
		qCritical("Intarnal error [%s:%d]", __FILE__, __LINE__);
		return QString();
	}

	QProcess df;
	QStringList args;
	args << "-u" << f1.fileName() << f2.fileName();
	df.start("diff", args);

	if (!df.waitForStarted() || !df.waitForFinished())
		return QString();

	return QString::fromUtf8(df.readAll().data());
}