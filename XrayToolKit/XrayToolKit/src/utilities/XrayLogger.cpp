﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/22
** filename: 	XrayLogger.cpp
** file base:	XrayLogger
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the various types of loggers.
****************************************************************************/

#include "XrayLogger.h"
#include <spdlog/sinks/stdout_color_sinks.h>

XRAYLAB_USING_NAMESPACE

std::shared_ptr<spdlog::logger> XrayLogger::p_appLogger;
std::shared_ptr<spdlog::logger> XrayLogger::p_clientLogger;

/*!
	Creating loggers with multiple sinks:

	std::vector<spdlog::sink_ptr> sinks;
	sinks.push_back(std::make_shared<spdlog::sinks::stdout_sink_st>());
	sinks.push_back(std::make_shared<spdlog::sinks::daily_file_sink_st>("logfile", 23, 59));
	auto combined_logger = std::make_shared<spdlog::logger>("name", begin(sinks), end(sinks));
	//register it if you need to access it globally
	spdlog::register_logger(combined_logger);
	-------------------------------------------------------------------------------------------
	Creating multiple file loggers with the same output file:

	If you want to have different loggers to write to the same output file all of them must share the same sink.
	Otherwise you might get strange results.
	auto sharedFileSink = std::make_shared<spdlog::sinks::basic_file_sink_mt>("fileName.txt");
	auto firstLogger = std::make_shared<spdlog::logger>("firstLoggerName", sharedFileSink);
	auto secondLogger = std::make_unique<spdlog::logger>("secondLoggerName", sharedFileSink);
 */
 
void XrayLogger::init()
{
	p_appLogger = spdlog::stdout_color_mt("App");
	p_appLogger->set_pattern("%^[%T] %n[%L]: %v%$");
	p_appLogger->set_level(spdlog::level::trace);

	p_clientLogger = spdlog::stdout_color_mt("Client");
	p_clientLogger->set_pattern("%^[%T] %n: %v%$");
	p_clientLogger->set_level(spdlog::level::trace);
}