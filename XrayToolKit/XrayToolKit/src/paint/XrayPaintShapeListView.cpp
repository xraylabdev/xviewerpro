/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayPaintShapeFactory.cpp
** file base:	XrayPaintShapeFactory
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a list view for Paint shapes.
****************************************************************************/

#include "XrayPaintShapeListView.h"
#include "XrayPaintShapeListModel.h"

#include <QApplication>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayPaintShapeListView::XrayPaintShapeListView(QWidget* parent) : 
	QListView(parent)
{
	setContextMenuPolicy(Qt::CustomContextMenu);
	setSelectionMode(QAbstractItemView::ExtendedSelection);
	setDragEnabled(true);
	setDragDropMode(QAbstractItemView::InternalMove);
	setEditTriggers(QListView::EditTrigger::DoubleClicked);

	menu_Context = new QMenu(this);
	action_DeleteShape = new QAction(QIcon(":/paint/images/delete_icon.png"), QApplication::translate("XrayPaintShapeListView", "Delete"), this);
	action_DeleteShape->setStatusTip(QApplication::translate("XrayPaintShapeListView", "Delete this shape."));
	action_MoveShapeUp = new QAction(QIcon(":/paint/images/swipe_up_blue_icon.png"), QApplication::translate("XrayPaintShapeListView", "Move Up"), this);
	action_MoveShapeUp->setStatusTip(QApplication::translate("XrayPaintShapeListView", "Move this shape up."));
	action_MoveShapeDown = new QAction(QIcon(":/paint/images/swipe_down_blue_icon.png"), QApplication::translate("XrayPaintShapeListView", "Move Down"), this);
	action_MoveShapeDown->setStatusTip(QApplication::translate("XrayPaintShapeListView", "Move this shape down."));
	initializeContextMenu();
}

void XrayPaintShapeListView::initializeContextMenu()
{
	menu_Context->addAction(action_MoveShapeUp);
	menu_Context->addAction(action_MoveShapeDown);
	menu_Context->addAction(action_DeleteShape);

	connect(this, &QListView::customContextMenuRequested, this, &XrayPaintShapeListView::on_custom_contextMenu_requested);
	connect(action_MoveShapeUp, &QAction::triggered, this, &XrayPaintShapeListView::on_action_moveShapeUp_triggered);
	connect(action_MoveShapeDown, &QAction::triggered, this, &XrayPaintShapeListView::on_action_moveShapeDown_triggered);
	connect(action_DeleteShape, &QAction::triggered, this, &XrayPaintShapeListView::on_action_deleteShape_triggered);
}


void XrayPaintShapeListView::on_custom_contextMenu_requested(const QPoint& pos)
{
	itemIndex = indexAt(pos);
	if (itemIndex.isValid())
	{
		selectionModel()->select(itemIndex, QItemSelectionModel::Select);
		menu_Context->popup(mapToGlobal(pos));
	}
}

void XrayPaintShapeListView::on_action_deleteShape_triggered()
{
	dynamic_cast<XrayPaintShapeListModel*>(model())->deleteShapeViaContextMenu(itemIndex.row());
	emit shapeChanged();
}
void XrayPaintShapeListView::on_action_deleteShapes_triggered()
{
	auto count = model()->rowCount();
	for (int i = count - 1; i >= 0; i--)
		dynamic_cast<XrayPaintShapeListModel*>(model())->deleteShapeViaContextMenu(i);
	emit shapeChanged();
}
void XrayPaintShapeListView::on_action_selected_deleteShape()
{
	if (selectedIndexes().empty())
		return;

	if (selectedIndexes().at(0).row() < 0)
		return;

	dynamic_cast<XrayPaintShapeListModel*>(model())->deleteShapeViaContextMenu(selectedIndexes().at(0).row());

	// select the last shape on delete
	//if (model()->rowCount() > 0)
	//{
	//	for (int i = 0; i < static_cast<XrayPaintShapeListModel*>(model())->getShapeListConst().size(); i++)
	//	{
	//		auto s = static_cast<XrayPaintShapeListModel*>(model())->getShapeListConst().at(i);
	//		if (s->getType() == PaintDrawModeType::Freehand || 
	//			s->getType() == PaintDrawModeType::Rectangle ||
	//			s->getType() == PaintDrawModeType::Circle ||
	//			s->getType() == PaintDrawModeType::Ellipse ||
	//			s->getType() == PaintDrawModeType::Polygon)
	//		{
	//			selectionModel()->select(model()->index(i, 0), QItemSelectionModel::ClearAndSelect);
	//			emit shapeChanged();
	//			return;
	//		}
	//	}

	//	selectionModel()->select(model()->index(model()->rowCount() - 1, 0), QItemSelectionModel::ClearAndSelect);
	//}

	emit shapeChanged();
}

void XrayPaintShapeListView::on_action_moveShapeUp_triggered()
{
	dynamic_cast<XrayPaintShapeListModel*>(model())->moveShapeUp(itemIndex.row());
	emit shapeChanged();
}

void XrayPaintShapeListView::on_action_moveShapeDown_triggered()
{
	dynamic_cast<XrayPaintShapeListModel*>(model())->moveShapeDown(itemIndex.row());
	emit shapeChanged();
}

void XrayPaintShapeListView::selectShape(const XrayPaintBaseShape* const newShape)
{
	if (newShape != nullptr)
	{
		int row = 0;
		for (int i = 0; i < static_cast<XrayPaintShapeListModel*>(model())->getShapeListConst().size(); i++)
		{
			if (newShape == static_cast<XrayPaintShapeListModel*>(model())->getShapeListConst().at(i))
			{
				row = i;
			}
		}
		selectionModel()->select(model()->index(row, 0), QItemSelectionModel::ClearAndSelect);
	}
	else
	{
		selectionModel()->clearSelection();
	}
}

void XrayPaintShapeListView::updateSelection(const QModelIndex& topLeft, const int& first, const int& last)
{
	Q_UNUSED(last)
	selectionModel()->select(model()->index(first, 0, topLeft), QItemSelectionModel::ClearAndSelect);
}
