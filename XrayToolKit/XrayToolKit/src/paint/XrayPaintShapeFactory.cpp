/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayPaintShapeFactory.cpp
** file base:	XrayPaintShapeFactory
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides shape factory for the Paint.
****************************************************************************/

#include "XrayPaintShapeFactory.h"

XRAYLAB_USING_NAMESPACE

XrayPaintShapeFactory::XrayPaintShapeFactory()
{
}

XrayPaintBaseShape* XrayPaintShapeFactory::createNewShape(const QPoint& _initialPos, const PaintDrawModeType& _shapeType)
{
	switch (_shapeType)
	{
	case PaintDrawModeType::Freehand:
		return new XrayPaintBrushShape(_initialPos);
		break;
	case PaintDrawModeType::Line:
		return new XrayPaintLineShape(_initialPos);
		break;
	case PaintDrawModeType::Arrow:
		return new XrayPaintArrowShape(_initialPos);
		break;
	case PaintDrawModeType::Rectangle:
		return new XrayPaintRectangleShape(_initialPos);
		break;
	case PaintDrawModeType::Circle:
		return new XrayPaintCircleShape(_initialPos);
		break;
	case PaintDrawModeType::Ellipse:
		return new XrayPaintEllipseShape(_initialPos);
		break;
	case PaintDrawModeType::Polygon:
		return new XrayPaintPolygonShape(_initialPos);
		break;
	case PaintDrawModeType::Text:
		return new XrayPaintTextShape(_initialPos);
		break;
	default:
		return nullptr;
	}
}
