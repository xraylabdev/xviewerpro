/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/03/28
** filename: 	XraySaveReportAsExcelDialog.cpp
** file base:	XraySaveReportAsExcelDialog
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the image cropper widget with UI.
****************************************************************************/

#include "XraySaveReportAsExcelDialog.h"
#include "ui_XraySaveReportAsExcelDialog.h"
#include "XrayGlobal.h"
#include "XrayCVUtil.h"
#include "XrayQtUtil.h"
#include "XrayQFileUtil.h"
#include "xlsxdocument.h"

#include <QApplication>
#include <QFileDialog>
#include <QInputDialog>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XraySaveReportAsExcelDialog::XraySaveReportAsExcelDialog(QWidget* _parent) :
	QDialog(_parent),
	ui(new Ui::XraySaveReportAsExcelDialog)
{
	ui->setupUi(this);
	setWindowTitle("XVoid Report As Excel Sheet");

	//connect(ui->imageWBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this](int value)
	//{
	//});

	//connect(ui->imageHBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this](int value)
	//{
	//});

	auto cancel = ui->buttonBox->button(QDialogButtonBox::Cancel);
	auto save = ui->buttonBox->button(QDialogButtonBox::Save);
	connect(cancel, &QPushButton::clicked, this, &XraySaveReportAsExcelDialog::cancel);
	connect(save, &QPushButton::clicked, this, &XraySaveReportAsExcelDialog::save);
}

void XraySaveReportAsExcelDialog::writeSettings(QSettings& _settings)
{
	_settings.beginGroup("SaveReportAsExcel");

	_settings.setValue("Parameters/Project", ui->projectEdit->text());
	_settings.setValue("Parameters/ProjectValue", ui->projectValueEdit->text());
	_settings.setValue("Parameters/Part", ui->partEdit->text());
	_settings.setValue("Parameters/PartValue", ui->partValueEdit->text());
	_settings.setValue("Parameters/ImageW", ui->imageWBox->value());
	_settings.setValue("Parameters/ImageH", ui->imageHBox->value());
	_settings.setValue("Parameters/isCurrentPic", ui->currentPictureCbox->isChecked());

	_settings.endGroup();
}
void XraySaveReportAsExcelDialog::readSettings(QSettings& _settings)
{
	_settings.beginGroup("SaveReportAsExcel");

	ui->projectEdit->setText(_settings.value("Parameters/Project", ui->projectEdit->text()).toString());
	ui->projectValueEdit->setText(_settings.value("Parameters/ProjectValue", ui->projectValueEdit->text()).toString());
	ui->partEdit->setText(_settings.value("Parameters/Part", ui->partEdit->text()).toString());
	ui->partValueEdit->setText(_settings.value("Parameters/PartValue", ui->partValueEdit->text()).toString());
	ui->imageWBox->setValue(_settings.value("Parameters/ImageW", ui->imageWBox->value()).toInt());
	ui->imageHBox->setValue(_settings.value("Parameters/ImageH", ui->imageHBox->value()).toInt());
	ui->currentPictureCbox->setChecked(_settings.value("Parameters/isCurrentPic", ui->currentPictureCbox->isChecked()).toBool());

	_settings.endGroup();
}

void XraySaveReportAsExcelDialog::setCurrentPaintArea(XrayMainPaintWidget* _area)
{
	paintArea = _area;
}
void XraySaveReportAsExcelDialog::setPaintAreas(const QVector<XrayMainPaintWidget*>& _areas)
{
	paintAreas = _areas;
}

bool XraySaveReportAsExcelDialog::isCurrentPictureSelected() const
{
	return ui->currentPictureCbox->isChecked();
}

void XraySaveReportAsExcelDialog::cancel()
{
	close();
}

// paint area shape
class PAShape 
{
public:
	QString name;
	QImage image;
	QVector<QVector<QString> > areas;
	QString percentageSum;
};

char mapNumberToAlphabet(int number)
{
	if (number == 1) return 'A';
	else if (number == 2) return 'B';
	else if (number == 3) return 'C';
	else if (number == 4) return 'D';
	else if (number == 5) return 'E';
	else if (number == 6) return 'F';
	else if (number == 7) return 'G';
	else if (number == 8) return 'H';
	else if (number == 9) return 'I';
	else if (number == 10) return 'J';
	else if (number == 11) return 'K';
	else if (number == 12) return 'L';
	else if (number == 13) return 'M';
	else if (number == 14) return 'N';
	else if (number == 15) return 'O';
	else if (number == 16) return 'P';
	else if (number == 17) return 'Q';
	else if (number == 18) return 'R';
	else if (number == 19) return 'S';
	else if (number == 20) return 'T';
	else if (number == 21) return 'U';
	else if (number == 22) return 'V';
	else if (number == 23) return 'W';
	else if (number == 24) return 'X';
	else if (number == 25) return 'Y';
	else if (number == 26) return 'Z';
	else return 'A';
}

static bool saveAsExcelFile(const QString& fileName, const QString& projectName, const QString& partName, const QString& partNameValue, const QVector<QString>& header, const QVector<PAShape>& shapes, bool defineSumRule)
{
	QXlsx::Document xlsx;

	// the index of row/col starts from 1.
	auto row = 1;
	auto col = 1;
	if (row < 1) row = 1;
	if (col < 1) col = 1;

	// Set style for the row 11th.
	QXlsx::Format titleFormat;
	titleFormat.setFontBold(true);
	titleFormat.setFontColor(QColor(Qt::black));
	titleFormat.setFontSize(14);

	QXlsx::Format partNameValueFormat;
	partNameValueFormat.setFontBold(true);
	partNameValueFormat.setFontColor(QColor(Qt::black));
	partNameValueFormat.setFontSize(11);

	QXlsx::Format cellHFormat;
	cellHFormat.setFontBold(false);
	cellHFormat.setFontColor(QColor(Qt::black));
	cellHFormat.setFontSize(11);
	cellHFormat.setPatternBackgroundColor(QColor(255, 255, 255));
	cellHFormat.setBorderStyle(QXlsx::Format::BorderThin);
	cellHFormat.setBorderColor(QColor(220, 220, 220));

	QXlsx::Format cellFormat;
	cellFormat.setFontBold(false);
	cellFormat.setFontColor(QColor(Qt::black));
	cellFormat.setFontSize(11);
	cellFormat.setPatternBackgroundColor(QColor(255, 255, 255));
	cellFormat.setBorderStyle(QXlsx::Format::BorderThin);
	cellFormat.setBorderColor(QColor(169, 208, 142));

	QXlsx::Format cellFormatGreen;
	cellFormatGreen.setFontBold(false);
	cellFormatGreen.setFontColor(QColor(Qt::black));
	cellFormatGreen.setFontSize(11);
	cellFormatGreen.setPatternBackgroundColor(QColor(226, 239, 218));

	auto sheetName = partNameValue.simplified().remove(' ');

	xlsx.setColumnWidth(1, 1, 25.0);
	xlsx.setColumnWidth(2, header.size() + col, 11.0);

	xlsx.write(row++, col, projectName, titleFormat);
	xlsx.write(row, col, partName, titleFormat);
	xlsx.write(row++, col + 1, sheetName, partNameValueFormat);
	xlsx.write(row++, col, "");

	for (const auto& shape : shapes)
	{
		xlsx.write(row, col, shape.name, cellHFormat);
		for (auto h = 0; h < header.size(); h++)
			xlsx.write(row, h + col + 1, header[h], cellFormatGreen);

		xlsx.insertImage(row - 1, header.size() + col + 1, shape.image);

		row++;

		auto startRow = row;
		auto alternate = 0;
		for (const auto& area : shape.areas)
		{
			for (auto j = 0; j < area.size(); j++)
			{
				bool ok;
				auto v = area[j].toDouble(&ok);
				if (ok)
					xlsx.write(row, j + col + 1, v, alternate % 2 ? cellFormatGreen : cellFormat);
				else
					xlsx.write(row, j + col + 1, area[j], alternate % 2 ? cellFormatGreen : cellFormat);
			}
			row++;
			alternate++;
		}
		auto endtRow = row - 1;

		// number of columns of header and all others are same
		xlsx.write(row, header.size() - 1 + col, "SUM", alternate % 2 ? cellFormatGreen : cellFormat);
		
		// define rules - Rule for SUM
		if (defineSumRule)
		{
			auto column = mapNumberToAlphabet(header.size() + col);
			QString ruleName = QString(column) + QString::number(startRow) + ":" + QString(column) + QString::number(endtRow);    // "F5F14"
			QString ruleText = "=SUM(" + ruleName + ")";                                                                                    // "=SUM(F5F14)"
			QString rule = "=" + sheetName + "!$" + QString(column) + "$" + QString::number(startRow) + ":$" + QString(column) + "$" + QString::number(endtRow);        // "=HL_ET2581!$F$5:$F$14" here sheetName must not have any white space

			xlsx.defineName(ruleName, rule);
			xlsx.write(row, header.size() + col, ruleText);
		}
		else
		{
			xlsx.write(row, header.size() + col, shape.percentageSum, alternate % 2 ? cellFormatGreen : cellFormat);
		}

		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");
		xlsx.write(row++, col, "");

	}

	xlsx.setDocumentProperty("title", "Void analysis of " + sheetName);
	xlsx.setDocumentProperty("subject", sheetName);
	xlsx.setDocumentProperty("creator", QApplication::applicationName() + " v" + QApplication::applicationVersion());
	xlsx.setDocumentProperty("company", QApplication::organizationName());
	xlsx.setDocumentProperty("category", "Void Analysis");
	xlsx.setDocumentProperty("keywords", "Voids, Analysis, Xray-analysis");
	xlsx.setDocumentProperty("description", "Void Analysis Report");
	xlsx.renameSheet("Sheet1", sheetName);

	return xlsx.saveAs(fileName);
}

static bool saveAsCSVFile(const QString& fileName, const QString& projectName, const QString& partName, const QString& partNameValue, const QVector<QString>& header, const QVector<PAShape>& shapes, QChar delimiter)
{
	auto totalColumn = header.size();

	QFile file(fileName);
	if (file.open(QIODevice::WriteOnly))
	{
		QTextStream stream(&file);

		stream << projectName;
		// add empty columns after projectName
		for (auto i = 0; i < totalColumn; i++) stream << delimiter; stream << endl;

		// add a space line
		for (auto i = 0; i < totalColumn; i++) stream << delimiter; stream << endl;

		stream << partName << delimiter << partNameValue;
		// add empty columns after partName
		for (auto i = 0; i < totalColumn - 1; i++) stream << delimiter; stream << endl;

		// add a space line
		for (auto i = 0; i < totalColumn; i++) stream << delimiter; stream << endl;

		int nshapes = 0;
		for (const auto& shape : shapes)
		{
			stream << shape.name;
			
			// add shapes
			for (const auto& i : header)
				stream << delimiter << i;
			stream << endl;

			auto imgFile = QFileInfo(fileName).absolutePath() + "/" + QFileInfo(fileName).baseName() + "_" + shape.name + ".jpg";
			if (shape.image.save(imgFile))
			{
				xAppInfo("Voids image file has been saved at {}", imgFile);
			}

			// add area values
			for (const auto& area : shape.areas)
			{
				for (const auto& j : area)
					stream << delimiter << j;
				stream << endl;
			}

			// add empty columns before SUM
			for (auto i = 0; i < totalColumn - 1; i++) stream << delimiter;
			stream << "SUM" << delimiter << shape.percentageSum << endl;

			// only add empty lines after each shape and do not add after the last shape.
			nshapes++;
			if (nshapes >= shapes.size())
			{
				// don't add empty lines in the end of the file
			}
			else
			{
				// add three space lines after one shape is done
				for (auto i = 0; i < totalColumn; i++) stream << delimiter; stream << endl;
				for (auto i = 0; i < totalColumn; i++) stream << delimiter; stream << endl;
				for (auto i = 0; i < totalColumn; i++) stream << delimiter; stream << endl;

			
			}
		}

		file.close();
		return true;
	}

	return false;
}

static QVector<PAShape> getShapesData(XrayMainPaintWidget* area, const QSize& imageSize)
{
	QVector<PAShape> shapes;
	if (!area)
		return shapes;

	shapes.reserve(area->getShapeListModel()->getShapeListConst().size());

	auto lang = QLocale::languageToString(QLocale().language());

	for (auto& shape : area->getShapeListModel()->getShapeListConst())
	{
		PAShape s;
		s.name = shape->getName();

		auto& areas = shape->getBlobDetector().m_areas;
		if (!areas.empty())	// do not add shape to the excel sheet if it doesn't have any void in it.
		{
			// Get a image that includes shape, voids inside the shape, and text boxes. 
			// If any of the text box contains the shape name, the returned image will have only matched text box, otherwise all text boxes will be printed out.
			s.image = area->getOriginalImageWithVoids(shape, true, PaintDrawModeType::Text).scaled(imageSize, Qt::KeepAspectRatio, Qt::TransformationMode::SmoothTransformation);

			auto sum = 0.0;
			for (std::size_t i = 0; i < areas.size(); i++)
			{
				auto percentage = 100.0 / shape->calculateSurfaceArea() * areas[i];
				sum += percentage;

				QString a;
				QString p;
				if (lang == "German" || lang == "Deutsch")
				{
					a = QString("%L1").arg(areas[i], 0, 'f', 1, '0');	// %L1 for local language based conversion 12345.212 => 12.345,2
					p = QString("%L1").arg(percentage, 0, 'f', 6, '0');
				}
				else // English
				{
					a = QString("%1").arg(areas[i], 0, 'f', 1, '0');
					p = QString("%1").arg(percentage, 0, 'f', 6, '0');
				}

				QVector<QString> values = { shape->getName(), QString("%1").arg(i), QString("%1").arg(shape->calculateSurfaceArea()), a, p };
				s.areas.push_back(values);
			}

			if (lang == "German" || lang == "Deutsch")
				s.percentageSum = QString("%L1").arg(sum, 0, 'f', 6, '0');
			else // English
				s.percentageSum = QString("%1").arg(sum, 0, 'f', 6, '0');

			shapes.push_back(s);
		}
	}

	return shapes;
}

void XraySaveReportAsExcelDialog::save()
{
	if (ui->currentPictureCbox->isChecked() && paintArea)
		paintAreas = QVector<XrayMainPaintWidget*>{ paintArea };
	
	if (paintAreas.empty())
	{
		emit statusBarMessage(QApplication::translate("XraySaveReportAsExcelDialog", "Couldn't find any picture!"), 15000);
		return;
	}

	static QString filters = "Excel Workbook (*.xlsx);;"
		"CSV (Semicolon delimited)(*.csv)";

	auto projectName = ui->partValueEdit->text().isEmpty() ? QString("Void_Analysis_Report.xlsx") : ui->partValueEdit->text() + QString(".xlsx");
	auto fileName = QFileDialog::getSaveFileName(this, QApplication::translate("XraySaveReportAsExcelDialog", "Save File As..."), XrayGlobal::getLastFileOpenPath() + '/' + projectName, filters, nullptr);
	if (!fileName.isEmpty())
	{
		auto project = ui->projectEdit->text() + ui->projectValueEdit->text();
		auto partName = ui->partEdit->text();
		auto partNameValue = ui->partValueEdit->text();

		if (project.isEmpty()) project = "Project";
		if (partName.isEmpty()) partName = "Part";
		if (partNameValue.isEmpty()) partNameValue = "SheetName";

		QVector<QString> header = { "Shape", "Void", "Shape area", "Void area", "%" };
		
		QVector<PAShape> shapes;
		for (const auto p : paintAreas)
		{
			auto s = getShapesData(p, QSize(ui->imageWBox->value(), ui->imageHBox->value()));
			if(!s.empty())
				shapes.append(s);
		}

		auto ext = QFileInfo(fileName).suffix();
		if (ext == "csv" || ext == "txt")
		{
			// save to comma separated file
			QChar delim = ';';

			if (saveAsCSVFile(fileName, project, partName, partNameValue, header, shapes, delim))
				emit statusBarMessage(QApplication::translate("XraySaveReportAsExcelDialog", "Report has been saved successfully!"), 15000);
			else
				emit statusBarMessage(QApplication::translate("XraySaveReportAsExcelDialog", "Couldn't save the report!"), 15000);
		}
		else if (ext == "xlsx")
		{
			auto lang = QLocale::languageToString(QLocale().language());
			auto sumRule = !(lang == "German" || lang == "Deutsch");

			if (saveAsExcelFile(fileName, project, partName, partNameValue, header, shapes, sumRule))
				emit statusBarMessage(QApplication::translate("XraySaveReportAsExcelDialog", "Report has been saved successfully!"), 15000);
			else
				emit statusBarMessage(QApplication::translate("XraySaveReportAsExcelDialog", "Couldn't save the report!"), 15000);
		}

		XrayGlobal::setLastFileOpenPath(fileName);

		close();
	}
}