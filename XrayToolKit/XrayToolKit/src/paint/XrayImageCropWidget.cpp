/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/11/27
** filename: 	XrayImageCropWidget.cpp
** file base:	XrayImageCropWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the image cropper widget with UI.
****************************************************************************/

#include "XrayImageCropWidget.h"
#include "ui_XrayImageCropWidget.h"
#include "XrayGlobal.h"
#include "XrayCVUtil.h"
#include "XrayQtUtil.h"
#include "XrayQFileUtil.h"

#include <QApplication>
#include <QFileDialog>
#include <QInputDialog>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayImageCropWidget::XrayImageCropWidget(QWidget* _parent) :
	QWidget(_parent),
	ui(new Ui::XrayImageCropWidget)
{
	ui->setupUi(this);
	setWindowTitle("Image Cropper");

	paintArea = new XrayMainPaintWidget(_parent);
	rectShape = new XrayPaintRectangleShape();
	rectShape->setName("Rect 0");
	rectShape->setBrush(QBrush(QColor(249, 0, 0, 200), Qt::NoBrush));
	rectShape->setPen(QPen(QBrush(QColor(249, 0, 0, 200), Qt::SolidPattern), 2.0, Qt::SolidLine, Qt::RoundCap));
	paintArea->addShapeInProgress(rectShape);
	paintArea->update();

	ui->scrollArea->setAlignment(Qt::AlignCenter);
	ui->scrollArea->setWidget(paintArea);

	connect(ui->xSBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this](int value)
	{
		auto rect = rectShape->getRect();
		rect.setX(value);
		rectShape->setRect(rect);
		paintArea->update();
	});

	connect(ui->ySBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this](int value)
	{
		auto rect = rectShape->getRect();
		rect.setY(value);
		rectShape->setRect(rect);
		paintArea->update();
	});

	connect(ui->wSBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this](int value)
	{
		auto rect = rectShape->getRect();
		rect.setWidth(value);
		rectShape->setRect(rect);
		paintArea->update();
	});

	connect(ui->hSBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this](int value)
	{
		auto rect = rectShape->getRect();
		rect.setHeight(value);
		rectShape->setRect(rect);
		paintArea->update();
	});

	auto ok = ui->buttonBox->button(QDialogButtonBox::Ok);
	auto cancel = ui->buttonBox->button(QDialogButtonBox::Cancel);
	auto save = ui->buttonBox->button(QDialogButtonBox::Save);
	save->setToolTip(QApplication::translate("XrayImageCropWidget", "Save the cropped image to the disk."));
	auto reset = ui->buttonBox->button(QDialogButtonBox::Reset);
	reset->setToolTip(QApplication::translate("XrayImageCropWidget", "Reset ROI to image size."));
	connect(ok, &QPushButton::clicked, this, &XrayImageCropWidget::ok);
	connect(cancel, &QPushButton::clicked, this, &XrayImageCropWidget::cancel);
	connect(save, &QPushButton::clicked, this, &XrayImageCropWidget::save);
	connect(reset, &QPushButton::clicked, this, &XrayImageCropWidget::reset);
}

void XrayImageCropWidget::setImage(const QImage& image)
{
	paintArea->resetImage(image);
}
void XrayImageCropWidget::fitImage()
{
	auto& image = paintArea->getImage();
	if (image.isNull())
		return;

	auto w = (ui->scrollArea->viewport()->size().width()) / static_cast<double>(image.width());
	auto h = (ui->scrollArea->viewport()->size().height()) / static_cast<double>(image.height());
	paintArea->zoom(std::min(w, h));
	paintArea->update();
}
void XrayImageCropWidget::setROI(const QRect& roi)
{
	rectShape->setRect(roi);
	paintArea->update();

	auto b = ui->xSBox->blockSignals(true);
	ui->xSBox->setRange(0, roi.width());
	ui->xSBox->setValue(roi.x());
	ui->xSBox->blockSignals(b);

	b = ui->ySBox->blockSignals(true);
	ui->ySBox->setRange(0, roi.height());
	ui->ySBox->setValue(roi.y());
	ui->ySBox->blockSignals(b);

	b = ui->wSBox->blockSignals(true);
	ui->wSBox->setRange(0, roi.width());
	ui->wSBox->setValue(roi.width());
	ui->wSBox->blockSignals(b);

	b = ui->hSBox->blockSignals(true);
	ui->hSBox->setRange(0, roi.height());
	ui->hSBox->setValue(roi.height());
	ui->hSBox->blockSignals(b);
}
void XrayImageCropWidget::save()
{
	auto image = croppedImage();
	if (image.isNull())
	{
		emit statusBarMessage(QApplication::translate("XrayImageCropWidget", "Couldn't find any image or invalid ROI!"), 15000);
		return;
	}

	auto fileName = QFileDialog::getSaveFileName(this, QApplication::translate("XrayImageCropWidget", "Save File As..."), XrayGlobal::getLastFileOpenPath() + '/' + "cropped-image.jpg", XrayGlobal::getAllSupportedImageExtensionsForSaveDialog(), nullptr);
	if (!fileName.isEmpty())
	{
		auto ext = QFileInfo(fileName).suffix();
		if (ext.isEmpty())
			fileName += ".jpg";

		if (image.save(fileName))
			emit statusBarMessage(QApplication::translate("XrayImageCropWidget", "Image has been saved successfully!"), 15000);
		else
			emit statusBarMessage(QApplication::translate("XrayImageCropWidget", "Couldn't save the image!"), 15000);

		XrayGlobal::setLastFileOpenPath(fileName);
	}
}
void XrayImageCropWidget::reset()
{
	auto& image = paintArea->getOriginalImage();
	if (image.isNull())
		return;

	setROI(QRect(0, 0, image.width(), image.height()));
}

QImage XrayImageCropWidget::croppedImage()
{
	auto& image = paintArea->getOriginalImage();
	if (image.isNull())
		return QImage();

	auto roi = rectShape->getRect();

	// full size of the image
	if(roi == QRect(0, 0, image.width(), image.height()))
		return image.copy();

	// valid region of interest
	if (roi.x() >= 0 && roi.y() >= 0 &&
		roi.x() < image.width() && roi.y() < image.height() &&
		roi.width() <= image.width() && roi.height() <= image.height() &&
		roi.width() > 0 && roi.height() > 0)
	{
		return image.copy(roi);
	}

	return QImage();
}
