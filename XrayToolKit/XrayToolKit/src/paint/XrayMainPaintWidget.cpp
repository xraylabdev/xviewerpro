/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayMainPaintWidget.cpp
** file base:	XrayMainPaintWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the main paint widget.
****************************************************************************/

#include "XrayMainPaintWidget.h"
#include "XrayPaintMainWindow.h"
#include "XrayPaintShapeFactory.h"
#include "XrayGlobal.h"
#include "XrayCVUtil.h"
#include "XrayQtUtil.h"

#include <QDebug>

XRAYLAB_BEGIN_NAMESPACE

QImage XrayMainPaintWidget::m_watermark = QImage();
bool XrayMainPaintWidget::m_isWatermark = false;
double XrayMainPaintWidget::m_lastCircleRadius = 20;

XrayMainPaintWidget::XrayMainPaintWidget(QWidget* _parent) :
	XrayMainPaintWidget("", QSize(400, 400), _parent)
{
}

XrayMainPaintWidget::XrayMainPaintWidget(const QString& _fileName, const QSize& _canvas_size, QWidget* _parent) :
	QWidget(_parent),
	p_parent(_parent),
	isModified(false),
	isMousePressed(false),
	m_isMiddleBtnPressed(false),
	m_mouse_state(PaintMouseState::Idle),
	m_canvas_size(_canvas_size),
	m_mode(PaintDrawModeType::Selection),
	undoStack(new QUndoStack(this)),
	shapeListModel(new XrayPaintShapeListModel(this)),
	m_image(new QImage(_canvas_size, QImage::Format_ARGB32_Premultiplied)),
	m_backup_image(new QImage(_canvas_size, QImage::Format_ARGB32_Premultiplied)),
	selectedShape(nullptr),
	customBrushShape(new XrayPaintBrushShape()),
	customPolygonShape(new XrayPaintPolygonShape()),
	customCircleShape(new XrayPaintCircleShape()),
	m_scale(1.0),
	m_isCrosshair(false),
	m_isGrid(false),
	m_gridSize(100),
	m_crosshairColor(Qt::cyan),
	m_gridColor(Qt::green),
	rectangleCount(1),
	circleCount(1),
	ellipseCount(1),
	polylineCount(1),
	freelineCount(1),
	lineCount(1),
	arrowCount(1),
	textCount(1)
{
	setObjectName("paintArea");
	setFocusPolicy(Qt::StrongFocus);
	setMouseTracking(true);
	resize(_canvas_size);
	setBackgroundColor(QColor(255, 255, 255, 0));
	setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

	m_crosshairColor.setAlpha(200);
	m_gridColor.setAlpha(200);

	if (QFile::exists(_fileName))
	{
		if (XrayCVUtil::isSupportedImageExtension(_fileName))
		{
			auto mat = cv::imread(_fileName.toLatin1().toStdString(), cv::IMREAD_UNCHANGED);
			if (!mat.empty())
			{
				// check the depth of the imported image and convert to 8 bit image.
				if (mat.depth() == CV_16U || mat.depth() == CV_16S || mat.depth() == CV_16F)
				{
					cv::normalize(mat, m_image16, 0, 65535, cv::NORM_MINMAX);

					double min, max;
					cv::minMaxLoc(mat, &min, &max);
					mat.convertTo(mat, CV_8UC1, 255.0 / (max - min), -min * 255.0 / (max - min));
				}
				else if (mat.depth() == CV_32S || mat.depth() == CV_32F || mat.depth() == CV_64F)
				{
					cv::normalize(mat, m_image32, 0, 65535, cv::NORM_MINMAX);

					double min, max;
					cv::minMaxLoc(mat, &min, &max);
					mat.convertTo(mat, CV_8UC4, 255.0 / (max - min), -min * 255.0 / (max - min));
				}

				m_image.reset(new QImage(_canvas_size, XrayCVUtil::getQImageFormat(mat)));
				m_backup_image.reset(new QImage(_canvas_size, XrayCVUtil::getQImageFormat(mat)));

				*m_image = XrayCVUtil::toQImage(mat).copy();
				*m_backup_image = *m_image;
				resize(m_image->size());
				m_file_name = _fileName;
			}
		}
		else if (XrayGlobal::isSupportedImageExtension(_fileName))
		{
			if (m_image->load(_fileName))
			{
				*m_backup_image = *m_image;
				resize(m_image->size());
				m_file_name = _fileName;
			}
		}
	}

	customBrushShape->setBrush(QBrush(QColor(249, 0, 0, 200), Qt::NoBrush));
	customBrushShape->setPen(QPen(QBrush(QColor(249, 0, 0, 200), Qt::SolidPattern), 1.0, Qt::SolidLine, Qt::RoundCap));

	customPolygonShape->setBrush(QBrush(QColor(249, 0, 0, 200), Qt::NoBrush));
	customPolygonShape->setPen(QPen(QBrush(QColor(249, 0, 0, 200), Qt::SolidPattern), 1.0, Qt::SolidLine, Qt::RoundCap));

	customCircleShape->setBrush(QBrush(QColor(249, 0, 0, 200), Qt::NoBrush));
	customCircleShape->setPen(QPen(QBrush(QColor(249, 0, 0, 200), Qt::SolidPattern), 1.0, Qt::SolidLine, Qt::RoundCap));
}

bool XrayMainPaintWidget::load(const QString& _fileName)
{
	if (QFile::exists(_fileName))
	{
		m_file_name = _fileName;
		m_scale = 1.0;
		return reload();
	}

	return false;
}
bool XrayMainPaintWidget::reload()
{
	if (QFile::exists(m_file_name))
	{
		if (XrayCVUtil::isSupportedImageExtension(m_file_name))
		{
			auto mat = cv::imread(m_file_name.toLatin1().toStdString(), cv::IMREAD_UNCHANGED);
			if (!mat.empty())
			{
				// check the depth of the imported image and convert to 8 bit image.
				if (mat.depth() == CV_16U || mat.depth() == CV_16S || mat.depth() == CV_16F)
				{
					cv::normalize(mat, m_image16, 0, 65535, cv::NORM_MINMAX);

					double min, max;
					cv::minMaxLoc(mat, &min, &max);
					mat.convertTo(mat, CV_8UC1, 255.0 / (max - min), -min * 255.0 / (max - min));
				}
				else if (mat.depth() == CV_32S || mat.depth() == CV_32F || mat.depth() == CV_64F)
				{
					cv::normalize(mat, m_image32, 0, 65535, cv::NORM_MINMAX);

					double min, max;
					cv::minMaxLoc(mat, &min, &max);
					mat.convertTo(mat, CV_8UC4, 255.0 / (max - min), -min * 255.0 / (max - min));
				}

				m_image.reset(new QImage(m_canvas_size, XrayCVUtil::getQImageFormat(mat)));
				m_backup_image.reset(new QImage(m_canvas_size, XrayCVUtil::getQImageFormat(mat)));

				*m_image = XrayCVUtil::toQImage(mat).copy();
				*m_backup_image = *m_image;
				resize(m_image->size() * m_scale);
				update();
				return true;
			}
		}
		else if (XrayGlobal::isSupportedImageExtension(m_file_name))
		{
			if (m_image->load(m_file_name))
			{
				*m_backup_image = *m_image;
				resize(m_image->size() * m_scale);
				update();
				return true;
			}
		}
	}

	return false;
}
QImage XrayMainPaintWidget::reloadImage()
{
	if (QFile::exists(m_file_name))
	{
		if (XrayCVUtil::isSupportedImageExtension(m_file_name))
		{
			auto mat = cv::imread(m_file_name.toLatin1().toStdString(), cv::IMREAD_UNCHANGED);
			if (!mat.empty())
			{
				// check the depth of the imported image and convert to 8 bit image.
				if (mat.depth() == CV_16U || mat.depth() == CV_16S || mat.depth() == CV_16F)
				{
					cv::normalize(mat, m_image16, 0, 65535, cv::NORM_MINMAX);

					double min, max;
					cv::minMaxLoc(mat, &min, &max);
					mat.convertTo(mat, CV_8UC1, 255.0 / (max - min), -min * 255.0 / (max - min));
				}
				else if (mat.depth() == CV_32S || mat.depth() == CV_32F || mat.depth() == CV_64F)
				{
					cv::normalize(mat, m_image32, 0, 65535, cv::NORM_MINMAX);

					double min, max;
					cv::minMaxLoc(mat, &min, &max);
					mat.convertTo(mat, CV_8UC4, 255.0 / (max - min), -min * 255.0 / (max - min));
				}

				return XrayCVUtil::toQImage(mat).copy();
			}
		}
		else if (XrayGlobal::isSupportedImageExtension(m_file_name))
		{
			QImage image;
			if (image.load(m_file_name))
				return image;
		}
	}
	return QImage();
}

void XrayMainPaintWidget::setSelectedShape(XrayPaintBaseShape* shape)
{
	selectedShape = shape;
}
XrayPaintBaseShape* XrayMainPaintWidget::getSelectedShape() const 
{ 
	return selectedShape; 
}

void XrayMainPaintWidget::setWatermarkImage(const QImage& _img)
{
	m_watermark = _img;
}
void XrayMainPaintWidget::setWatermarkEnabled(bool _b)
{
	m_isWatermark = _b;
}
bool XrayMainPaintWidget::isWatermarkEnabled()
{
	return m_isWatermark;
}

int XrayMainPaintWidget::calculateTotalSurface() const
{
	auto totalSurfaceArea = 0;
	for (auto& i : shapeListModel->getShapeListConst())
		totalSurfaceArea += i->calculateSurfaceArea();
	return totalSurfaceArea;
}

void XrayMainPaintWidget::changeMouseCursor()
{
	if (m_mode == PaintDrawModeType::Selection)
	{
		switch (m_mouse_state)
		{
		case PaintMouseState::Idle:
			setCursor(Qt::ArrowCursor);
			break;
		case PaintMouseState::Targetting:
			setCursor(Qt::OpenHandCursor);
			break;
		case PaintMouseState::Dragging:
			setCursor(Qt::ClosedHandCursor);
			break;
		default:
			setCursor(Qt::ArrowCursor);
			break;
		}
	}
	else
	{
		setCursor(QCursor(QPixmap(":/paint/images/mouse_pointer_icon.png").scaled(32, 32)));
	}
}

XrayPaintBaseShape* XrayMainPaintWidget::getTextBoxShape(const QString& text)
{
	if (text.isEmpty())
		return nullptr;

	for (auto& shape : shapeListModel->getShapeListConst())
	{
		if (shape->getType() == PaintDrawModeType::Text)
		{
			auto s = dynamic_cast<XrayPaintTextShape*>(shape);
			if (s)
			{
				if (s->getText().contains(text))
					return s;
			}
		}
	}
	return nullptr;
}
QImage XrayMainPaintWidget::drawRelatedTextBoxOnImage(XrayPaintBaseShape* shape, QImage& img)
{
	if (img.isNull())
		return img;

	auto tempImage = img;

	if (shape->getType() == PaintDrawModeType::Text && dynamic_cast<XrayPaintTextShape*>(shape))
	{
		QPainter painter(&tempImage);
		painter.setPen(shape->getPen());
		painter.setBrush(shape->getBrush());
		painter.setFont(shape->getFont());
		shape->drawShape(painter);
	}

	return tempImage;
}
QImage XrayMainPaintWidget::drawAllShapesOnImage(QImage& img, PaintDrawModeType type)
{
	if (img.isNull())
		return img;

	auto tempImage = img;

	QPainter painter(&tempImage);
	for (auto& shape : shapeListModel->getShapeListConst())
	{
		if (shape->getType() == type)
		{
			painter.setPen(shape->getPen());
			painter.setBrush(shape->getBrush());
			painter.setFont(shape->getFont());
			shape->drawShape(painter);
		}
	}

	return tempImage;
}

QImage XrayMainPaintWidget::getOriginalImageWithVoids(XrayPaintBaseShape* shape, bool drawTypeShapes, PaintDrawModeType type)
{
	if(!shape)
		return QImage();

	if (m_backup_image->isNull())
		return QImage();

	auto r = shape->getBoundingRect();
	if ((r.x < 0) || (r.y < 0) || ((r.x + r.width) > m_backup_image->width()) || ((r.y + r.height) > m_backup_image->height()) || (r.width < 2) || (r.height < 2))
		return QImage();

	QImage result;

	auto mat = XrayCVUtil::toMat(*m_backup_image, false);
	shape->getBlobDetector().m_image = mat;
	shape->getBlobDetector().m_roi = r;
	if (shape->getBlobDetector().update())
		result = drawShapes(XrayCVUtil::toQImage(shape->getBlobDetector().m_output), shape, true);
	else
		result = drawShapes(*m_backup_image, shape, true);

	if (drawTypeShapes)
	{
		// check if any of the text box contains the shape name, if true, then print that matched text box in the image.
		auto textBox = getTextBoxShape(shape->getName());
		if (textBox)
		{
			result = drawRelatedTextBoxOnImage(textBox, result);
		}
		else // if there is not text box that contains the shape name, then print all text boxes in the image.
		{
			result = drawAllShapesOnImage(result, type);
		}
	}

	return result;
}
QImage XrayMainPaintWidget::drawShapes(const QImage& img, XrayPaintBaseShape* shape, bool grayToRGB)
{
	QImage tempImage;

	if (img.isNull())
		return img;

	if (grayToRGB && (img.format() == QImage::Format::Format_Grayscale8 || img.format() == QImage::Format::Format_Indexed8))
		tempImage = img.convertToFormat(QImage::Format::Format_RGB888); // if there is a gray-scale image, change to 3 channel image for colored shapes.
	else
		tempImage = img;

	QPainter painter(&tempImage);
	painter.setPen(shape->getPen());
	painter.setBrush(shape->getBrush());
	painter.setFont(shape->getFont());
	shape->drawShape(painter);
	drawWatermarkImage(painter);

	return tempImage;
}

QImage XrayMainPaintWidget::copyImage(bool grayToRGB)
{
	QImage tempImage;

	if (m_image->isNull())
		return tempImage;

	if (grayToRGB && (m_image->format() == QImage::Format::Format_Grayscale8 || m_image->format() == QImage::Format::Format_Indexed8))
		tempImage = m_image->convertToFormat(QImage::Format::Format_RGB888); // if there is a gray-scale image, change to 3 channel image for colored shapes.
	else
		tempImage = *m_image;

	QPainter painter(&tempImage);
	for (auto& shape : shapeListModel->getShapeListConst())
	{
		painter.setPen(shape->getPen());
		painter.setBrush(shape->getBrush());
		painter.setFont(shape->getFont());
		shape->drawShape(painter);
	}
	drawWatermarkImage(painter);

	return tempImage;
}

QImage XrayMainPaintWidget::copyOriginalImage(bool grayToRGB)
{
	QImage tempImage;

	if (m_backup_image->isNull())
		return tempImage;

	if (grayToRGB && (m_backup_image->format() == QImage::Format::Format_Grayscale8 || m_backup_image->format() == QImage::Format::Format_Indexed8))
		tempImage = m_backup_image->convertToFormat(QImage::Format::Format_RGB888); // if there is a gray-scale image, change to 3 channel image for colored shapes.
	else
		tempImage = *m_backup_image;

	QPainter painter(&tempImage);
	for (auto& shape : shapeListModel->getShapeListConst())
	{
		painter.setPen(shape->getPen());
		painter.setBrush(shape->getBrush());
		painter.setFont(shape->getFont());
		shape->drawShape(painter);
	}
	drawWatermarkImage(painter);

	return tempImage;
}

QImage XrayMainPaintWidget::copyOriginalImage(XrayPaintBaseShape* shape, bool grayToRGB)
{
	QImage tempImage;

	if (m_backup_image->isNull())
		return tempImage;

	if (grayToRGB && (m_backup_image->format() == QImage::Format::Format_Grayscale8 || m_backup_image->format() == QImage::Format::Format_Indexed8))
		tempImage = m_backup_image->convertToFormat(QImage::Format::Format_RGB888); // if there is a gray-scale image, change to 3 channel image for colored shapes.
	else
		tempImage = *m_backup_image;

	QPainter painter(&tempImage);
	painter.setPen(shape->getPen());
	painter.setBrush(shape->getBrush());
	painter.setFont(shape->getFont());
	shape->drawShape(painter);
	drawWatermarkImage(painter);

	return tempImage;
}

QImage XrayMainPaintWidget::copyImage(XrayPaintBaseShape* shape)
{
	QImage tempImage;

	if (m_image->isNull())
		return tempImage;

	if (!shapeListModel->getShapeListConst().empty() && (m_image->format() == QImage::Format::Format_Grayscale8 || m_image->format() == QImage::Format::Format_Indexed8))
		tempImage = m_image->convertToFormat(QImage::Format::Format_RGB888); // if there is a gray-scale image, change to 3 channel image for colored shapes.
	else
		tempImage = *m_image;

	QPainter painter(&tempImage);
	painter.setPen(shape->getPen());
	painter.setBrush(shape->getBrush());
	painter.setFont(shape->getFont());
	shape->drawShape(painter);
	drawWatermarkImage(painter);

	return tempImage;
}
QImage XrayMainPaintWidget::copyImage()
{
	QImage tempImage;

	if (m_image->isNull())
		return tempImage;

	if (!shapeListModel->getShapeListConst().empty() && (m_image->format() == QImage::Format::Format_Grayscale8 || m_image->format() == QImage::Format::Format_Indexed8))
		tempImage = m_image->convertToFormat(QImage::Format::Format_RGB888); // if there is a gray-scale image, change to 3 channel image for colored shapes.
	else
		tempImage = *m_image;

	QPainter painter(&tempImage);
	for (auto& shape : shapeListModel->getShapeListConst())
	{
		painter.setPen(shape->getPen());
		painter.setBrush(shape->getBrush());
		painter.setFont(shape->getFont());
		shape->drawShape(painter);
	}
	drawWatermarkImage(painter);

	return tempImage;
}
QImage XrayMainPaintWidget::getWaterMarkedImage(const QImage& watermark, double opacity, bool grayToRGB)
{
	auto tempImage = copyImage(grayToRGB);
	QPainter painter(&tempImage);
	painter.setOpacity(opacity);  // 0.00 = 0%, 1.00 = 100% opacity.
	drawWatermarkImage(watermark, painter);
	return tempImage;
}
QImage XrayMainPaintWidget::getWaterMarkedOriginalImage(const QImage& watermark, double opacity, bool grayToRGB)
{
	auto tempImage = copyOriginalImage(grayToRGB);
	QPainter painter(&tempImage);
	painter.setOpacity(opacity);  // 0.00 = 0%, 1.00 = 100% opacity.
	drawWatermarkImage(watermark, painter);
	return tempImage;
}

void XrayMainPaintWidget::deleteImage()
{
	undoStack->push(new XrayPaintDeleteImageCommand(*m_image, *shapeListModel));
	update();
}

void XrayMainPaintWidget::drawShapesInList()
{
	for (auto var = shapeListModel->getShapeListConst().size() - 1; var >= 0; --var)
	{
		const auto shape = shapeListModel->getShapeListConst().at(var);
		m_painter.begin(this);
		if(m_scale != 1.0) m_painter.scale(m_scale, m_scale);
		if (dynamic_cast<XrayPaintMainWindow*>(p_parent)->getUseAntialiasing())
			m_painter.setRenderHint(QPainter::Antialiasing, true);
		else
			m_painter.setRenderHint(QPainter::Antialiasing, false);
		m_painter.setPen(shape->getPen());
		m_painter.setBrush(shape->getBrush());
		m_painter.setFont(shape->getFont());
		shape->drawShape(m_painter);

		if (!selectedShapes.empty())
		{
			for (auto& s : selectedShapes)
			{
				QPen red(Qt::black);
				red.setWidth(1);
				m_painter.setOpacity(0.2);
				red.setCapStyle(Qt::SquareCap);
				red.setStyle(Qt::DashLine);
				m_painter.setBrush(Qt::NoBrush);
				m_painter.setPen(red);
				s->drawSelectionMarker(m_painter);
			}
		}
		else
		{
			if (shape == selectedShape)
			{
				QPen red(Qt::black);
				red.setWidth(1);
				m_painter.setOpacity(0.6);
				red.setCapStyle(Qt::SquareCap);
				red.setStyle(Qt::DashLine);
				m_painter.setBrush(Qt::NoBrush);
				m_painter.setPen(red);
				shape->drawSelectionMarker(m_painter);
			}
		}

		m_painter.end();
	}
}
void XrayMainPaintWidget::drawCustomShapes()
{
	if (m_mode == PaintDrawModeType::CustomPolygon)
	{
		m_painter.begin(this);
		if (m_scale != 1.0) m_painter.scale(m_scale, m_scale);
		m_painter.setRenderHint(QPainter::Antialiasing, true);
		m_painter.setPen(customPolygonShape->getPen());
		m_painter.setBrush(customPolygonShape->getBrush());
		customPolygonShape->drawShape(m_painter);
		m_painter.end();
	}
	else if (m_mode == PaintDrawModeType::CustomCircle)
	{
		m_painter.begin(this);
		if (m_scale != 1.0) m_painter.scale(m_scale, m_scale);
		m_painter.setRenderHint(QPainter::Antialiasing, true);
		m_painter.setPen(customCircleShape->getPen());
		m_painter.setBrush(customCircleShape->getBrush());
		customCircleShape->drawShape(m_painter);
		m_painter.end();
	}
	else if (m_mode == PaintDrawModeType::CustomBrush)
	{
		m_painter.begin(this);
		if (m_scale != 1.0) m_painter.scale(m_scale, m_scale);
		m_painter.setRenderHint(QPainter::Antialiasing, true);
		m_painter.setPen(customBrushShape->getPen());
		m_painter.setBrush(customBrushShape->getBrush());
		customBrushShape->drawShape(m_painter);
		m_painter.end();
	}
}

void XrayMainPaintWidget::drawShapeInProgress()
{
	if (!shapeInProgress.isNull())
	{
		m_painter.begin(this);
		if (m_scale != 1.0) m_painter.scale(m_scale, m_scale);
		m_painter.setPen(shapeInProgress->getPen());
		m_painter.setBrush(shapeInProgress->getBrush());
		shapeInProgress->drawShape(m_painter);
		m_painter.end();
	}
}

void XrayMainPaintWidget::setFileName(const QString& _fileName)
{ 
	m_file_name = _fileName; 
};
QString XrayMainPaintWidget::getFileName() const
{
	return m_file_name;
}
void XrayMainPaintWidget::setFileCaption(const QString& _text)
{
	m_file_caption = _text;
}
QString XrayMainPaintWidget::getFileCaption() const
{
	return m_file_caption;
}

void XrayMainPaintWidget::setImage(const cv::Mat& _mat)
{
	*m_image = XrayCVUtil::toQImage(_mat).copy();
}
void XrayMainPaintWidget::setImage(const QImage& _image)
{
	*m_image = _image.copy();
}
QImage& XrayMainPaintWidget::getImage() const
{
	return *m_image;
}
QImage& XrayMainPaintWidget::getOriginalImage() const
{
	return *m_backup_image;
}
cv::Mat& XrayMainPaintWidget::getImage16()
{
	return m_image16;
}
cv::Mat& XrayMainPaintWidget::getImage32()
{
	return m_image32;
}
void XrayMainPaintWidget::setCurrentImageAsOriginalImage()
{
	*m_backup_image = *m_image;
}
void XrayMainPaintWidget::resetImage(const QImage& _image)
{
	if (_image.isNull())
		return;

	*m_image = _image.copy();
	*m_backup_image = *m_image;
	resize(m_image->size() * m_scale);
	update();
}
void XrayMainPaintWidget::resetImage()
{
	if (m_backup_image->isNull())
		return;

	*m_image = *m_backup_image;
	resize(m_image->size() * m_scale);
	update();
}
void XrayMainPaintWidget::rotate(double _angle)
{
	auto src = XrayCVUtil::toMat(*m_image, true);
	cv::Mat m(src.size(), src.type());
	if (_angle == 90.)
	{
		cv::transpose(src, m);
		cv::flip(m, m, 0);
		*m_image = XrayCVUtil::toQImage(m).copy();
	}
	else if (_angle == 180.)
	{
		cv::flip(src, m, -1);
		*m_image = XrayCVUtil::toQImage(m).copy();
	}
	else if (_angle == 270.)
	{
		cv::transpose(src, m);
		cv::flip(m, m, 1);
		*m_image = XrayCVUtil::toQImage(m).copy();
	}
	else
	{
		auto image = reloadImage();
		if (image.isNull())
			return;

		auto center = image.rect().center();
		QMatrix matrix;
		matrix.translate(center.x(), center.y());
		matrix.rotate(_angle);
		*m_image = image.transformed(matrix);
	}
	resize(m_image->size() * m_scale);
	*m_backup_image = *m_image;
	update();
}
void XrayMainPaintWidget::flip(int _mode)
{
	QImage img;
	if(_mode == 0)
		img = m_image->mirrored(false, true);
	else if (_mode == 1)
		img = m_image->mirrored(true, false);
	else if (_mode == -1)
		img = m_image->mirrored(true, true);
	*m_image = img;
	*m_backup_image = *m_image;
	update();
}
void XrayMainPaintWidget::crop()
{
	if (!selectedShape)
		return;

	auto rect = selectedShape->getBoundingRect();
	if (!rect.empty())
	{
		auto cropped = m_image->copy(XrayCVUtil::fromRect(rect));
		*m_image = cropped;
		resize(m_image->size() * m_scale);
		*m_backup_image = *m_image;
		selectedShape->mouseDrag(QPoint(0, 0)); // move shape to 0,0.
		update();
	}
}

XrayPaintShapeListModel* XrayMainPaintWidget::getShapeListModel()
{
	return shapeListModel.get();
}
void XrayMainPaintWidget::resetShapeListModel()
{
	shapeListModel->clear();
}

XrayPaintBaseShape* XrayMainPaintWidget::getShapeUnderMouse(const QPoint& _pos)
{
	for (auto shape : shapeListModel->getShapeListConst())
	{
		if (shape->mouseIntersectCheck(_pos))
			return shape;
	}
	return nullptr;
}

QUndoStack *XrayMainPaintWidget::getUndoStack() const
{
	return undoStack;
}

void XrayMainPaintWidget::keyPressEvent(QKeyEvent* event)
{
	if (m_mode == PaintDrawModeType::Selection)
	{
		if (event->key() == Qt::Key_Up)
		{
			if (!selectedShapes.empty())
			{
				for (auto& shape : selectedShapes)
					shape->nudgeUp();
				update();
			}
			else
			{
				if (!shapeListModel->getShapeListConst().isEmpty() && selectedShape != nullptr)
				{
					selectedShape->nudgeUp();
					update();
				}
			}
		}
		if (event->key() == Qt::Key_Down)
		{
			if (!selectedShapes.empty())
			{
				for (auto& shape : selectedShapes)
					shape->nudgeDown();
				update();
			}
			else
			{
				if (!shapeListModel->getShapeListConst().isEmpty() && selectedShape != nullptr)
				{
					selectedShape->nudgeDown();
					update();
				}
			}
		}
		if (event->key() == Qt::Key_Left)
		{
			if (!selectedShapes.empty())
			{
				for (auto& shape : selectedShapes)
					shape->nudgeLeft();
				update();
			}
			else
			{
				if (!shapeListModel->getShapeListConst().isEmpty() && selectedShape != nullptr)
				{
					selectedShape->nudgeLeft();
					update();
				}
			}
		}
		if (event->key() == Qt::Key_Right)
		{
			if (!selectedShapes.empty())
			{
				for (auto& shape : selectedShapes)
					shape->nudgeRight();
				update();
			}
			else
			{
				if (!shapeListModel->getShapeListConst().isEmpty() && selectedShape != nullptr)
				{
					selectedShape->nudgeRight();
					update();
				}
			}
		}
	}
}

bool XrayMainPaintWidget::getIsModified() const
{
	return isModified;
}

void XrayMainPaintWidget::addShapeInProgress(XrayPaintBaseShape* shape)
{
	if (shape)
	{
		createShapeName(shape->getType());	// increment the shape counter.
		shapeInProgress.reset(shape);
	}
}
void XrayMainPaintWidget::addShape(XrayPaintBaseShape* shape)
{
	if (shape)
	{
		createShapeName(shape->getType());	// increment the shape counter.
		shapeInProgress.reset(shape);

		undoStack->push(new XrayPaintAddShapeCommand(*shapeListModel, shapeInProgress.take()));
		//selectedShape = shapeListModel->getShapeListConst().at(0);	// do not set as a selected shape
		//emit onShapeCreated(selectedShape);	// this signal works only for text shape to open a text dialog, but here we are adding an already created shape, so no need for it.
		//emit selectionChanged(selectedShape);	// do not emit selection signal, because we might add more than one shapes

		update();
	}
}
void XrayMainPaintWidget::pasteShape(const QVector<XrayPaintBaseShape*>& shapes)
{
	for (auto& s : shapes)
	{
		if (s)
		{
			auto shape = s->clone();
			shape->getBlobDetector().clear();	// clear copied shape blobs, because it doesn't make sense to copy this shape blobs.
			shape->setName(createShapeName(shape->getType()));
			
			shapeInProgress.reset(shape);
			undoStack->push(new XrayPaintAddShapeCommand(*shapeListModel, shapeInProgress.take()));
			
			update();
		}
	}
}

void XrayMainPaintWidget::mousePressEvent(QMouseEvent* event)
{
	if (event->button() == Qt::MiddleButton)
	{
		m_isMiddleBtnPressed = true;
	}
	else if (event->button() == Qt::LeftButton)
	{
		isModified = true;
		m_mouse_press_pos = event->pos() / m_scale;
		emit mouseLeftClicked(m_mouse_press_pos);

		if (m_mode == PaintDrawModeType::CustomPolygon)
		{
			selectedShape = getShapeUnderMouse(event->pos() / m_scale);
			if (!selectedShape)
			{
				emit selectionChanged(nullptr);
			}
			else
			{
				emit selectionChanged(selectedShape);
				m_mouse_press_offset = selectedShape->getTopLeft().toPoint() - event->pos() / m_scale;
			}

			if (customPolygonShape->getPoints().empty())
			{
				QPolygonF points;
				points.append(event->pos() / m_scale);
				points.append(event->pos() / m_scale);
				customPolygonShape->setPoints(points); // first, insert the two points
			}

			customPolygonShape->mousePressEvent(event, event->pos() / m_scale);
			shapeInProgress.reset(customPolygonShape.get());
		}
		else if (m_mode == PaintDrawModeType::CustomCircle)
		{
			selectedShape = getShapeUnderMouse(event->pos() / m_scale);
			if (!selectedShape)
			{
				emit selectionChanged(nullptr);
			}
			else
			{
				emit selectionChanged(selectedShape);
				m_mouse_press_offset = selectedShape->getTopLeft().toPoint() - event->pos() / m_scale;
			}

			customCircleShape->mousePressEvent(event, event->pos() / m_scale);
		}
		else if (m_mode == PaintDrawModeType::CustomBrush)
		{
			selectedShape = getShapeUnderMouse(event->pos() / m_scale);
			if (!selectedShape)
			{
				emit selectionChanged(nullptr);
			}
			else
			{
				emit selectionChanged(selectedShape);
				m_mouse_press_offset = selectedShape->getTopLeft().toPoint() - event->pos() / m_scale;
			}

			customBrushShape->setPoints({}); // clear all points
			QPolygonF points;
			points.append(event->pos() / m_scale);
			customBrushShape->setPoints(points);
			customBrushShape->mousePressEvent(event, event->pos() / m_scale);
		}
		else if (m_mode == PaintDrawModeType::Selection)
		{
			selectedShape = getShapeUnderMouse(event->pos() / m_scale);

			if (selectedShape == nullptr)
			{
				emit selectionChanged(nullptr);
				m_mouse_state = PaintMouseState::Idle;
			}
			else
			{
				emit selectionChanged(selectedShape);
				m_mouse_press_offset = selectedShape->getTopLeft().toPoint() - event->pos() / m_scale;
				m_mouse_state = PaintMouseState::Dragging;
			}
		}
		else
		{
			if (shapeInProgress.isNull())
			{
				auto par = dynamic_cast<XrayPaintMainWindow*>(p_parent);
				if (par)
				{
					XrayPaintShapeFactory shapeFactory;
					auto shape = shapeFactory.createNewShape(event->pos() / m_scale, m_mode);
					shape->setPen(par->getPen());
					shape->setBrush(par->getBrush());
					shape->setName(createShapeName(m_mode));
					shape->setFont(par->getFont());
					if (m_mode == PaintDrawModeType::Arrow)
					{
						auto s = dynamic_cast<XrayPaintArrowShape*>(shape);
						if (s)
						{
							s->setArrowDirection(par->getArrowDirection());
							s->setArrowSize(par->getArrowSize());
						}
					}
					else if (m_mode == PaintDrawModeType::Circle)
					{
						auto s = dynamic_cast<XrayPaintCircleShape*>(shape);
						if (s)
						{
							s->setRadius(m_lastCircleRadius);
						}
					}
					shapeInProgress.reset(shape);
				}
			}
			else
			{
				shapeInProgress->mousePressEvent(event, event->pos() / m_scale);
			}
		}

		changeMouseCursor();

		// accumulate the selected shapes on Ctrl key pressed, othewise clear the list
		if (m_mode == PaintDrawModeType::Selection && (event->modifiers() & Qt::ControlModifier))
		{
			if (selectedShape)
			{
				if (!selectedShapes.contains(selectedShape))
					selectedShapes.append(selectedShape);
			}
		}
		else
		{
			selectedShapes.clear();
		}
	}

	update();
}
void XrayMainPaintWidget::mouseReleaseEvent(QMouseEvent* event)
{
	m_isMiddleBtnPressed = false;

	if (m_mode == PaintDrawModeType::CustomPolygon)
	{
		if (event->button() == Qt::RightButton)
		{
			customPolygonShape->mouseReleaseEvent(event, event->pos() / m_scale);

			emit customShapeChanged(customPolygonShape->getPoints());
			customPolygonShape->setPoints({});	// clear the points
			
			if (!shapeInProgress.isNull())
				shapeInProgress.take();	// take will make the internal pointer to nullptr but will not release it, and here this is what we want. We do not want to release the scopped pointer.
		}
	}
	else if (m_mode == PaintDrawModeType::CustomCircle)
	{
		customCircleShape->mouseReleaseEvent(event, event->pos() / m_scale);

		emit customShapeChanged(customCircleShape->getPoints());
	}
	else if (m_mode == PaintDrawModeType::CustomBrush)
	{
		customBrushShape->mouseReleaseEvent(event, event->pos() / m_scale);

		emit customShapeChanged(customBrushShape->getPoints());
		customBrushShape->setPoints({}); // clear the points
	}
	else if (m_mode == PaintDrawModeType::Selection)
	{
		if (getShapeUnderMouse(event->pos() / m_scale))
			m_mouse_state = PaintMouseState::Targetting;
		else
			m_mouse_state = PaintMouseState::Idle;
	}
	else
	{
		if (!shapeInProgress.isNull())
		{
			shapeInProgress->mouseReleaseEvent(event, event->pos() / m_scale);
			if (shapeInProgress->getShapeState() == PaintShapeState::Finished)
			{
				undoStack->push(new XrayPaintAddShapeCommand(*shapeListModel, shapeInProgress.take()));
				selectedShape = shapeListModel->getShapeListConst().at(0);
				emit onShapeCreated(selectedShape);
				emit selectionChanged(selectedShape);
			}
			else if (shapeInProgress->getShapeState() == PaintShapeState::Canceled)
			{
				shapeInProgress.reset();
			}
		}
	}

	changeMouseCursor();

	update();
}
void XrayMainPaintWidget::mouseMoveEvent(QMouseEvent* event)
{
	emit mouseMoved(event->pos() / m_scale);

	if (m_mode == PaintDrawModeType::CustomPolygon)
	{
		customPolygonShape->mouseMoveEvent(event, event->pos() / m_scale);
	}
	else if (m_mode == PaintDrawModeType::CustomCircle)
	{
		customCircleShape->mouseMoveEvent(event, event->pos() / m_scale);
	}
	else if (m_mode == PaintDrawModeType::CustomBrush)
	{
		customBrushShape->mouseMoveEvent(event, event->pos() / m_scale);
	}
	else if (m_mode == PaintDrawModeType::Selection)
	{
		if (m_mouse_state == PaintMouseState::Dragging)
		{
			if (selectedShape->isCornerSelect())
				selectedShape->cornerDrag(event->pos() / m_scale);
			else
				selectedShape->mouseDrag(event->pos() / m_scale + m_mouse_press_offset);
		}
		else
		{
			if (getShapeUnderMouse(event->pos() / m_scale))
			{
				m_mouse_state = PaintMouseState::Targetting;
			}
			else
			{
				m_mouse_state = PaintMouseState::Idle;
			}
		}
	}
	else
	{
		if (!shapeInProgress.isNull())
			shapeInProgress->mouseMoveEvent(event, event->pos() / m_scale);
	}

	changeMouseCursor();

	update();
}

QString XrayMainPaintWidget::createShapeName(PaintDrawModeType type)
{
	QString newName;
	switch (type)
	{
	case PaintDrawModeType::Freehand:
		newName = QString(QApplication::translate("XrayMainPaintWidget", "Brush") + " %1").arg(freelineCount++);
		break;
	case PaintDrawModeType::Line:
		newName = QString(QApplication::translate("XrayMainPaintWidget", "Line") + " %1").arg(lineCount++);
		break;
	case PaintDrawModeType::Arrow:
		newName = QString(QApplication::translate("XrayMainPaintWidget", "Arrow") + " %1").arg(arrowCount++);
		break;
	case PaintDrawModeType::Rectangle:
		newName = QString(QApplication::translate("XrayMainPaintWidget", "Rectangle") + " %1").arg(rectangleCount++);
		break;
	case PaintDrawModeType::Circle:
		newName = QString(QApplication::translate("XrayMainPaintWidget", "Circle") + " %1").arg(circleCount++);
		break;
	case PaintDrawModeType::Ellipse:
		newName = QString(QApplication::translate("XrayMainPaintWidget", "Oval") + " %1").arg(ellipseCount++);
		break;
	case PaintDrawModeType::Polygon:
		newName = QString(QApplication::translate("XrayMainPaintWidget", "Polygon") + " %1").arg(polylineCount++);
		break;
	case PaintDrawModeType::Text:
		newName = QString(QApplication::translate("XrayMainPaintWidget", "Text") + " %1").arg(textCount++);
		break;
	default:
		break;
	}

	return newName;
}

void XrayMainPaintWidget::drawWatermarkImage(const QImage& _watermark, QPainter& _painter)
{
	// draw the watermark at the center of the widget.
	if (!_watermark.isNull())
	{
		QRectF target(0, 0, m_image->width(), m_image->height());

		auto temp = _watermark;
		temp = temp.scaled(m_image->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);

		QRectF source(0.0, 0.0, static_cast<double>(temp.width()), static_cast<double>(temp.height()));

		auto deltaX = 0;
		auto deltaY = 0;
		if (source.width() < target.width())
			deltaX = target.width() - source.width();
		else
			deltaX = source.width() - target.width();

		if (source.height() < target.height())
			deltaY = target.height() - source.height();
		else
			deltaY = source.height() - target.height();

		_painter.translate(deltaX / 2, deltaY / 2);
		_painter.drawImage(source, temp);
	}
}
void XrayMainPaintWidget::drawWatermarkImage(QPainter& _painter)
{
	// draw the watermark at the center of the widget.
	if (m_isWatermark)
		drawWatermarkImage(m_watermark, _painter);
}
void XrayMainPaintWidget::drawWatermarkImagePaintEvent(QPainter& _painter)
{
	// draw the watermark at the center of the widget.
	if (m_isWatermark)
	{
		if (!m_watermark.isNull())
		{
			QRectF target(0, 0, width(), height());

			auto temp = m_watermark;
			temp = temp.scaled(rect().size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);

			QRectF source(0.0, 0.0, static_cast<double>(temp.width()), static_cast<double>(temp.height()));

			auto deltaX = 0;
			auto deltaY = 0;
			if (source.width() < target.width())
				deltaX = target.width() - source.width();
			else
				deltaX = source.width() - target.width();

			if (source.height() < target.height())
				deltaY = target.height() - source.height();
			else
				deltaY = source.height() - target.height();

			_painter.translate(deltaX / 2, deltaY / 2);
			_painter.drawImage(source, temp);
		}
	}
}

void XrayMainPaintWidget::setCrosshairEnabled(bool _b)
{
	m_isCrosshair = _b;
	update();
}
bool XrayMainPaintWidget::isCrosshairEnabled()
{
	return m_isCrosshair;
}
void XrayMainPaintWidget::drawCrosshair(QPainter& _painter)
{
	if (m_isCrosshair)
	{
		auto w = rect().width();
		auto h = rect().height();

		QPoint p0(0, h / 2);
		QPoint p1(w, h / 2);
		QLine line0(p0, p1);

		QPoint p2(w / 2, 0);
		QPoint p3(w / 2, h);
		QLine line1(p2, p3);

		QPen newPen;
		newPen.setWidthF(1.0);
		newPen.setStyle(Qt::SolidLine);
		newPen.setColor(m_crosshairColor);
		_painter.setPen(newPen);
		_painter.drawLine(line0);
		_painter.drawLine(line1);
	}
}
void XrayMainPaintWidget::setGridEnabled(bool _b)
{
	m_isGrid = _b;
	update();
}
bool XrayMainPaintWidget::isGridEnabled()
{
	return m_isGrid;
}
void XrayMainPaintWidget::drawGrid(QPainter& _painter)
{
	if (m_isGrid)
	{
		const int gridSize = m_gridSize;

		qreal left = int(rect().left()) - (int(rect().left()) % gridSize);
		qreal top = int(rect().top()) - (int(rect().top()) % gridSize);

		QVarLengthArray<QLineF, 100> lines;

		for (qreal x = left; x < rect().right(); x += gridSize)
			lines.append(QLineF(x, rect().top(), x, rect().bottom()));
		for (qreal y = top; y < rect().bottom(); y += gridSize)
			lines.append(QLineF(rect().left(), y, rect().right(), y));

		//auto isize = rect().size() / m_scale;
		//if (m_gridSize >= isize.width() - 1 || m_gridSize >= isize.height() - 1)
		//	m_gridSize = 2;

		//auto incX = isize.width() / m_gridSize;
		//auto incY = isize.height() / m_gridSize;

		//QVarLengthArray<QLineF, 100> lines;

		//for (qreal x = incX; x < isize.width(); x += incX)
		//	lines.append(QLineF(x, 0, x, isize.height()));

		//for (qreal y = incY; y < isize.height(); y += incY)
		//	lines.append(QLineF(0, y, isize.width(), y));

		QPen newPen;
		newPen.setWidthF(1.0);
		newPen.setStyle(Qt::SolidLine);
		newPen.setColor(m_gridColor);
		_painter.setPen(newPen);
		_painter.drawLines(lines.data(), lines.size());
	}
}

void XrayMainPaintWidget::setCrosshairColor(const QColor& _c)
{
	m_crosshairColor = _c;
}
QColor XrayMainPaintWidget::getCrosshairColor()
{
	return m_crosshairColor;
}
void XrayMainPaintWidget::setGridColor(const QColor& _c)
{
	m_gridColor = _c;
}
QColor XrayMainPaintWidget::getGridColor()
{
	return m_gridColor;
}
void XrayMainPaintWidget::setGridSize(int _size)
{
	m_gridSize = _size;
}
int XrayMainPaintWidget::getGridSize()
{
	return m_gridSize;
}

void XrayMainPaintWidget::toggleCrosshair()
{
	if (m_isCrosshair)
		m_isCrosshair = false;
	else
		m_isCrosshair = true;
	update();
}
void XrayMainPaintWidget::toggleGrid()
{
	if (m_isGrid)
		m_isGrid = false;
	else
		m_isGrid = true;
	update();
}

void XrayMainPaintWidget::paintEvent(QPaintEvent* _event)
{
	m_painter.begin(this);
	if (m_image)
	{
		auto sourceRect = _event->rect();
		if(m_scale != 1.0)
			sourceRect = QRect(sourceRect.topLeft() / m_scale, sourceRect.size() / m_scale);

		if(m_isMiddleBtnPressed)
			m_painter.drawImage(_event->rect(), *m_backup_image, sourceRect);
		else
			m_painter.drawImage(_event->rect(), *m_image, sourceRect);

	}
	m_painter.end();

	drawShapesInList();
	drawShapeInProgress();
	drawCustomShapes();

	QPainter painter(this);
	drawGrid(painter);
	drawCrosshair(painter);
	drawWatermarkImagePaintEvent(painter);

	// support for css
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
void XrayMainPaintWidget::zoom(double _scale)
{
	if (m_image->isNull())
		return;

	if (m_scale == _scale)
		return;

	m_scale = _scale;
	resize(m_image->size() * _scale);
}
double XrayMainPaintWidget::zoom() const
{
	return m_scale;
}
void XrayMainPaintWidget::fitToViewSize(const QSize& _size)
{
	auto w = _size.width() / static_cast<double>(m_image->width());
	auto h = _size.height() / static_cast<double>(m_image->height());
	zoom(std::min(w, h));
}
void XrayMainPaintWidget::zoomIn()
{
	auto factor = zoom() + 0.0625;
	if (factor < 0.0625)
		factor = 0.0625;
	if (factor > 8.0)
		factor = 8.0;

	zoom(factor);
}
void XrayMainPaintWidget::zoomOut()
{
	auto factor = zoom() - 0.0625;
	if (factor < 0.0625)
		factor = 0.0625;
	if (factor > 8.0)
		factor = 8.0;

	zoom(factor);
}

void XrayMainPaintWidget::pasteImage(const QImage& newImage)
{
	m_painter.begin(m_image.data());
	m_painter.drawImage(0, 0, newImage);
	m_painter.end();
}

void XrayMainPaintWidget::rasterizeAndClearShapes()
{
	QPainter painter(m_image.data());
	for (auto& shape : shapeListModel->getShapeListConst())
	{
		painter.setPen(shape->getPen());
		painter.setBrush(shape->getBrush());
		painter.setFont(shape->getFont());
		shape->drawShape(painter);
	}
	shapeListModel->clear();
}

void XrayMainPaintWidget::redo()
{
	undoStack->redo();
	update();
}

void XrayMainPaintWidget::resizeImage(const QSize& size)
{
	resize(size);
	if(m_backup_image)
		*m_image = m_backup_image->scaled(size, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
}

bool XrayMainPaintWidget::saveImage(const QString &fileName)
{
	auto img = copyImage();

	// if image has alpha channel then convert to 3 channel image, because jpg, jpeg, bmp do not support alpha channel.
	if (img.format() == QImage::Format_ARGB32 || img.format() == QImage::Format_ARGB32_Premultiplied)
	{
		auto ext = QFileInfo(fileName).suffix();
		if (ext == "jpg" || ext == "jpeg" || ext == "bmp")
		{
			QImage rgbImg(img.size(), QImage::Format_RGB888);
			rgbImg.fill(QColor(Qt::white).rgb());
			QPainter painter(&rgbImg);
			painter.drawImage(0, 0, img);
			img = rgbImg;
		}
	}

	if (img.save(fileName))
	{
		m_file_name = fileName;
		emit filenameChanged(m_file_name);
		isModified = false;
		return true;
	}

	return false;
}

void XrayMainPaintWidget::selectShape(const QItemSelection& selected, const QItemSelection& deselected)
{
	Q_UNUSED(deselected)

	if (!selected.isEmpty())
	{
		selectedShape = shapeListModel->getShapeListConst().at(selected.at(0).top());
		emit selectionChanged(selectedShape);
		update();
	}
	else
	{
		selectedShape = nullptr;
		emit selectionChanged(nullptr);
		update();
	}
}

void XrayMainPaintWidget::setMode(const PaintDrawModeType& newMode)
{
	m_mode = static_cast<PaintDrawModeType>(newMode);
}

void XrayMainPaintWidget::undo()
{
	if (!shapeInProgress.isNull())
	{
		if (m_mode == PaintDrawModeType::Polygon || m_mode == PaintDrawModeType::CustomPolygon)
		{
			auto s = dynamic_cast<XrayPaintPolygonShape*>(shapeInProgress.get());
			if (s) s->removeLastPoint();
		}
	}
	else
	{
		undoStack->undo();
	}
	update();
}

void XrayMainPaintWidget::setBackgroundColor(const QColor& _color)
{
	m_image->fill(qRgba(_color.red(), _color.green(), _color.blue(), _color.alpha()));
}
void XrayMainPaintWidget::setBackground(const QString& fileName)
{
	QPalette palette;
	palette.setBrush(QPalette::Background, QPixmap(fileName));
	setPalette(palette);
}

void XrayMainPaintWidget::mouseDoubleClickEvent(QMouseEvent* _event)
{
	emit doubleClicked(_event);
}

void XrayMainPaintWidget::wheelEvent(QWheelEvent* event)
{
	if (m_mode == PaintDrawModeType::CustomCircle)
	{
		customCircleShape->wheelEvent(event);
		update();
		event->accept();
		return;
	}
	else if (m_mode == PaintDrawModeType::Circle)
	{
		if (selectedShape)
		{
			auto shape = dynamic_cast<XrayPaintCircleShape*>(selectedShape);
			if (shape)
			{
				shape->wheelEvent(event);
				m_lastCircleRadius = shape->getRadius();
				update();
				event->accept();
			}
			return;
		}
	}
	else if (m_mode == PaintDrawModeType::Rectangle)
	{
		if (selectedShape)
		{
			auto shape = dynamic_cast<XrayPaintRectangleShape*>(selectedShape);
			if (shape)
			{
				shape->wheelEvent(event);
				update();
				event->accept();
			}

			return;
		}
	}
	else if (m_mode == PaintDrawModeType::Ellipse)
	{
		if (selectedShape)
		{
			auto shape = dynamic_cast<XrayPaintEllipseShape*>(selectedShape);
			if (shape)
			{
				shape->wheelEvent(event);
				update();
				event->accept();
			}

			return;
		}
	}
	else if (m_mode == PaintDrawModeType::Polygon)
	{
		if (selectedShape)
		{
			auto shape = dynamic_cast<XrayPaintPolygonShape*>(selectedShape);
			if (shape)
			{
				shape->wheelEvent(event);
				update();
				event->accept();
			}

			return;
		}
	}
	else if (m_mode == PaintDrawModeType::Freehand)
	{
		if (selectedShape)
		{
			auto shape = dynamic_cast<XrayPaintBrushShape*>(selectedShape);
			if (shape)
			{
				shape->wheelEvent(event);
				update();
				event->accept();
			}

			return;
		}
	}
	else
	{
		if (!m_image)
			return;

		if (m_image->size() == QSize(0, 0))
			return;

		if (event->orientation() == Qt::Vertical && (event->modifiers() & Qt::ControlModifier))
		{
			auto factor = 0.0;
			if (event->delta() > 0)
				factor = zoom() + 0.0625;
			else
				factor = zoom() - 0.0625;

			if (factor < 0.0625)
				factor = 0.0625;
			if (factor > 8.0)
				factor = 8.0;

			zoom(factor);

			event->accept();
			return;
		}
	}
	event->ignore();
}


XRAYLAB_END_NAMESPACE