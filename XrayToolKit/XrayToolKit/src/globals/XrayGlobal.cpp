/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/01
** filename: 	XrayAppGlobals.cpp
** file base:	XrayAppGlobals
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides global variables and functions for xray
*				app manager.
****************************************************************************/

#include "XrayGlobal.h"

#include <QApplication>
#include <QSettings>
#include <QDir>
#include <QFileInfo>
#include <QMenu>
#include <QMimeDatabase>
#include <QImageReader>

XRAYLAB_USING_NAMESPACE

QDir XrayGlobal::lastFileOpenPath = QDir::homePath();
QString XrayGlobal::currentAppLanguage = "en";
QString XrayGlobal::currentThemeStyle = "Dark";
bool XrayGlobal::currentLoggedInByAdmin = true;

QSize XrayGlobal::getNewSizeKeepAspectRatio(const QSize& _old, const QSize& _new)
{
	return getNewSizeKeepAspectRatio(_old.width(), _old.height(), _new.width(), _new.height());
}

QSize XrayGlobal::getNewSizeKeepAspectRatio(int _old_w, int _old_h, int _new_w, int _new_h)
{
	int _final_w, _final_h;
	const auto w2 = _new_h * (_old_w / static_cast<double>(_old_h));
	const auto h1 = _new_w * (_old_h / static_cast<double>(_old_w));
	if (h1 <= _new_h)
	{
		_final_w = _new_w;
		_final_h = static_cast<int>(h1);
	}
	else
	{
		_final_w = static_cast<int>(w2);
		_final_h = _new_h;
	}
	return QSize(_final_w, _final_h);
}

QStringList XrayGlobal::getAllSupportedImageExtensions()
{
	QMimeDatabase db;
	QStringList imageGlobPatterns;
	foreach(const QByteArray &mimeType, QImageReader::supportedMimeTypes())
	{
		QMimeType mime(db.mimeTypeForName(mimeType));
		imageGlobPatterns << mime.globPatterns();
	}

	return imageGlobPatterns;
}
QString XrayGlobal::getAllSupportedImageExtensions(const QString& _text /*= QString("Image Files")*/, const QStringList& _extra)
{
	auto ext = getAllSupportedImageExtensions() + _extra;
	return _text + " (" + ext.join(' ') + ")";
}
bool XrayGlobal::isSupportedImageExtension(const QString& _fileName)
{
	auto ext = QFileInfo(_fileName).suffix();
	return getAllSupportedImageExtensions().join(',').contains(ext, Qt::CaseInsensitive);
}
QString XrayGlobal::getAllSupportedImageExtensionsForSaveDialog()
{
	auto ext = getAllSupportedImageExtensions();

	// reorder the file extensions
	ext.removeAt(ext.indexOf("*.jpg"));
	ext.removeAt(ext.indexOf("*.jpeg"));
	ext.removeAt(ext.indexOf("*.png"));
	ext.removeAt(ext.indexOf("*.tif"));
	ext.removeAt(ext.indexOf("*.tiff"));
	ext.push_front("*.tiff");
	ext.push_front("*.tif");
	ext.push_front("*.png");
	ext.push_front("*.jpeg");
	ext.push_front("*.jpg");

	for (auto i = 0; i < ext.size() - 1; i++)
		ext[i] = "(" + ext[i] + ");;";
	ext[ext.size() - 1] = "(" + ext[ext.size() - 1] + ")";
	return ext.join(' ');
}

QString XrayGlobal::getLastFileOpenPath()
{
	return lastFileOpenPath.absolutePath();
}

void XrayGlobal::setLastFileOpenPath(const QStringList& _files)
{
	setLastFileOpenPath(_files.first());
}

void XrayGlobal::setLastFileOpenPath(const QString& _file)
{
	QFileInfo fileInfo(_file);
	if(fileInfo.isDir())
		lastFileOpenPath = _file;
	else
		lastFileOpenPath = fileInfo.dir();
}

bool XrayGlobal::isCurrentAppLanguageEnglish()
{
	QSettings settings(QApplication::organizationName(), QApplication::applicationName());
	return settings.value("Window/Language", "en").toString() == "en";
}

QString XrayGlobal::getCurrentAppLanguage(const QString& _defaultLang)
{
	QSettings settings(QApplication::organizationName(), QApplication::applicationName());
	return settings.value("Window/Language", _defaultLang).toString();
}

void XrayGlobal::setCurrentAppLanguage(const QString& _lang)
{
	QSettings settings(QApplication::organizationName(), QApplication::applicationName());
	settings.setValue("Window/Language", _lang);
}

void XrayGlobal::setCurrentThemeStyle(const QString& _style)
{
	currentThemeStyle = _style;
}
QString XrayGlobal::getCurrentThemeStyle()
{
	return currentThemeStyle;
}

bool XrayGlobal::hasLoggedInByAdmin()
{
	return currentLoggedInByAdmin;
}
void XrayGlobal::setLoggedInByAdmin(bool _b)
{
	currentLoggedInByAdmin = _b;
}