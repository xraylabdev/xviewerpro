/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/06/13
** filename: 	XrayLicenseGlobals.h
** file base:	XrayLicenseGlobals
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides global variables and functions for xray
*				licensing system.
****************************************************************************/

#include "XrayLicenseGlobals.h"

#include <QDir>
#include <QFileInfo>
#include <QMenu>
#include <QMimeDatabase>
#include <QImageReader>

XRAYLAB_USING_NAMESPACE

static QString ENGINE_NAME = "XRAY";
static XrayLicenseGlobals::LicenseType LICENSE_TYPE = XrayLicenseGlobals::LicenseType::DEMO;
static QString MODULE_NAME = "n/a";
static bool HAS_EXPIRED = false;
static const quint64 XRAY_ENCRYPT_KEY	= 1734952487069;	// must not be greater than 13 digits
static const quint64 FVK_ENCRYPT_KEY	= 3648503429731;	// must not be greater than 13 digits


void XrayLicenseGlobals::setEngineName(const QString& _name)
{
	ENGINE_NAME = _name;
}
void XrayLicenseGlobals::setLicenseType(LicenseType _type)
{
	LICENSE_TYPE = _type;
}
void XrayLicenseGlobals::setLicenseType(const QString& _type)
{
	if (_type.contains("Evaluation"))
		LICENSE_TYPE = LicenseType::EVALUATION;
	else if (_type.contains("Permanent"))
		LICENSE_TYPE = LicenseType::PERMANENT;
	else if (_type.contains("Educational"))
		LICENSE_TYPE = LicenseType::EDUCATIONAL;
	else
		LICENSE_TYPE = LicenseType::DEMO;
}
XrayLicenseGlobals::LicenseType XrayLicenseGlobals::getLicenseType()
{
	return LICENSE_TYPE;
}
QString XrayLicenseGlobals::getLicenseTypeAsString()
{
	if (LICENSE_TYPE == LicenseType::EVALUATION)
		return "Evaluation";
	else if (LICENSE_TYPE == LicenseType::PERMANENT)
		return "Permanent";
	else if (LICENSE_TYPE == LicenseType::EDUCATIONAL)
		return "Educational";
	else
		return "Demo";
}
bool XrayLicenseGlobals::isDemoVersion()
{
	if (LICENSE_TYPE == LicenseType::DEMO || hasExpired())
		return true;
	return false;
}
bool XrayLicenseGlobals::isForCommercialUse()
{
	if (LICENSE_TYPE == LicenseType::PERMANENT)
		return true;
	return false;
}
bool XrayLicenseGlobals::isForEvaluationUse()
{
	if (LICENSE_TYPE == LicenseType::EVALUATION)
		return true;
	return false;
}
bool XrayLicenseGlobals::isForEducationalUse()
{
	if (LICENSE_TYPE == LicenseType::EDUCATIONAL)
		return true;
	return false;
}

quint64 XrayLicenseGlobals::encryptionKey()
{
	if (ENGINE_NAME == "XRAY")
		return XRAY_ENCRYPT_KEY + 43495797;	// change the encryption key so that hard to crack it. Note: key must be store in different locations in memory to make it strong.
	else if (ENGINE_NAME == "FVK")
		return FVK_ENCRYPT_KEY + 19375460;	// change the encryption key so that hard to crack it. Note: key must be store in different locations in memory to make it strong.
	else
		return XRAY_ENCRYPT_KEY + 43495797;
}

void XrayLicenseGlobals::setModules(const QString& _modules)
{
	MODULE_NAME = _modules;
}
QString XrayLicenseGlobals::getModules()
{
	return MODULE_NAME;
}
bool XrayLicenseGlobals::hasXReportProModule()
{
	return MODULE_NAME.contains("XREPORT PRO", Qt::CaseInsensitive);
}
bool XrayLicenseGlobals::hasXVoidProModule()
{
	return MODULE_NAME.contains("XVOID PRO", Qt::CaseInsensitive);
}
bool XrayLicenseGlobals::hasXEnhancerProModule()
{
	return MODULE_NAME.contains("XENHANCER PRO", Qt::CaseInsensitive);
}
bool XrayLicenseGlobals::hasXUserPanelModule()
{
	return MODULE_NAME.contains("XUSERPANEL", Qt::CaseInsensitive);
}
bool XrayLicenseGlobals::hasXFilesCleanerModule()
{
	return MODULE_NAME.contains("XFILESCLEANER PRO", Qt::CaseInsensitive);
}

void XrayLicenseGlobals::setExpired(bool _value)
{
	HAS_EXPIRED = _value;
}
bool XrayLicenseGlobals::hasExpired()
{
	return HAS_EXPIRED;
}