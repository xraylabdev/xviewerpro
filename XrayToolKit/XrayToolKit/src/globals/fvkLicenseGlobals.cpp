/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/06/13
** filename: 	XrayLicenseGlobals.h
** file base:	XrayLicenseGlobals
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides global variables and functions for xray
*				licensing system.
****************************************************************************/

#include "XrayLicenseGlobals.h"

#include <QDir>
#include <QFileInfo>
#include <QMenu>
#include <QMimeDatabase>
#include <QImageReader>

XRAYLAB_USING_NAMESPACE

XrayLicenseGlobals::LicenseType XrayLicenseGlobals::m_licenseType = XrayLicenseGlobals::LicenseType::DEMO;
QString XrayLicenseGlobals::m_modules = "n/a";
bool XrayLicenseGlobals::m_expire = false;
static const quint64 XRAY_ENCRYPT_KEY = 1734952487069;	// must not be greater than 14 digits

void XrayLicenseGlobals::setLicenseType(LicenseType _type)
{
	m_licenseType = _type;
}
void XrayLicenseGlobals::setLicenseType(const QString& _type)
{
	if (_type.contains("Evaluation"))
		m_licenseType = LicenseType::EVALUATION;
	else if (_type.contains("Permanent"))
		m_licenseType = LicenseType::PERMANENT;
	else if (_type.contains("Educational"))
		m_licenseType = LicenseType::EDUCATIONAL;
	else
		m_licenseType = LicenseType::DEMO;
}
XrayLicenseGlobals::LicenseType XrayLicenseGlobals::getLicenseType()
{
	return m_licenseType;
}
bool XrayLicenseGlobals::isDemoVersion()
{
	if (m_licenseType == LicenseType::DEMO || hasExpired())
		return true;
	return false;
}
bool XrayLicenseGlobals::isForCommercialUse()
{
	if (m_licenseType == LicenseType::PERMANENT)
		return true;
	return false;
}

quint64 XrayLicenseGlobals::encryptionKey()
{
	return XRAY_ENCRYPT_KEY + 43495797;	// change the encryption key so that hard to crack it. Note: key must be store in different locations in memory to make it strong.
}

void XrayLicenseGlobals::setModules(const QString& _modules)
{
	m_modules = _modules;
}
QString XrayLicenseGlobals::getModules()
{
	return m_modules;
}
bool XrayLicenseGlobals::hasXReportProModule()
{
	return m_modules.contains("XREPORT PRO", Qt::CaseSensitive);
}
bool XrayLicenseGlobals::hasXVoidProModule()
{
	return m_modules.contains("XVOID PRO", Qt::CaseSensitive);
}
bool XrayLicenseGlobals::hasXEnhancerProModule()
{
	return m_modules.contains("XENHANCER PRO", Qt::CaseSensitive);
}
bool XrayLicenseGlobals::hasXUserPanelModule()
{
	return m_modules.contains("XUSERPANEL", Qt::CaseSensitive);
}

void XrayLicenseGlobals::setExpired(bool _value)
{
	m_expire = _value;
}
bool XrayLicenseGlobals::hasExpired()
{
	return m_expire;
}