/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayPaintShapes.h
** file base:	XrayPaintShapes
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		classes for paint shapes.
****************************************************************************/
#include "XrayPaintShapes.h"

#include "XrayQtUtil.h"
#include "XrayCVUtil.h"

#include <qmath.h>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayPaintBaseShape::XrayPaintBaseShape(const PaintDrawModeType& _type) :
	m_shapeState(PaintShapeState::InProgress),
	m_type(_type),
	m_isMoving(false),
	m_isTopLeftCornerSelected(false),
	m_isTopRightCornerSelected(false),
	m_isBottomLeftCornerSelected(false),
	m_isBottomRightCornerSelected(false)
{
}
R3D::fvkBlobDetector& XrayPaintBaseShape::getBlobDetector()
{
	return m_blob_detector;
}
PaintShapeState XrayPaintBaseShape::getShapeState() const
{
	return m_shapeState;
}
const PaintDrawModeType XrayPaintBaseShape::getType() const
{
	return m_type;
}
const QString& XrayPaintBaseShape::getName() const
{
	return m_name;
}
void XrayPaintBaseShape::setName(const QString& value)
{
	m_name = value;
}
void XrayPaintBaseShape::setPen(const QPen& _pen)
{
	m_pen = _pen;
}
const QPen& XrayPaintBaseShape::getPen() const
{
	return m_pen;
}
void XrayPaintBaseShape::setBrush(const QBrush& _brush)
{
	m_brush = _brush;
}
const QBrush& XrayPaintBaseShape::getBrush() const
{
	return m_brush;
}
void XrayPaintBaseShape::setFont(const QFont& _font)
{
	m_font = _font;
}
const QFont& XrayPaintBaseShape::getFont() const
{
	return m_font;
}
void XrayPaintBaseShape::setSelected(bool _selected)
{
	m_isSelected = _selected;
}
bool XrayPaintBaseShape::isSelected() const
{
	return m_isSelected;
}
bool XrayPaintBaseShape::isTopLeftCornerSelected() const
{
	return m_isTopLeftCornerSelected;
}
bool XrayPaintBaseShape::isTopRightCornerSelected() const
{
	return m_isTopRightCornerSelected;
}
bool XrayPaintBaseShape::isBottomLeftCornerSelected() const
{
	return m_isBottomLeftCornerSelected;
}
bool XrayPaintBaseShape::isBottomRightCornerSelected() const
{ 
	return m_isBottomRightCornerSelected; 
}
bool XrayPaintBaseShape::isCornerSelect() const
{
	if (m_isTopLeftCornerSelected || m_isTopRightCornerSelected || m_isBottomLeftCornerSelected || m_isBottomRightCornerSelected)
		return true;

	return false;
}
void XrayPaintBaseShape::drawCornerRect(QPainter& painter, QRect& cornerRect) const
{
	QPen cornerPen;
	cornerPen.setWidthF(1.5);
	cornerPen.setStyle(Qt::DotLine);
	cornerPen.setColor(Qt::green);
	painter.setPen(cornerPen);
	painter.drawRect(cornerRect);
}
void XrayPaintBaseShape::setBlobSettings(QSettings& _settings, const QString& _key)
{
	_settings.setValue(_key + "/Name", QString::fromStdString(m_blob_detector.m_name));
	_settings.setValue(_key + "/Parameters/Repeatability", m_blob_detector.m_params.minRepeatability);
	_settings.setValue(_key + "/Parameters/ThresholdStep", m_blob_detector.m_params.thresholdStep);
	_settings.setValue(_key + "/Parameters/LowerThreshold", m_blob_detector.m_params.minThreshold);
	_settings.setValue(_key + "/Parameters/HigherThreshold", m_blob_detector.m_params.maxThreshold);
	_settings.setValue(_key + "/Parameters/SmallestVoid", m_blob_detector.m_params.minArea);
	_settings.setValue(_key + "/Parameters/LargestVoid", m_blob_detector.m_params.maxArea);
	_settings.setValue(_key + "/Parameters/VoidCircularity", m_blob_detector.m_params.minCircularity);
	_settings.setValue(_key + "/Parameters/VoidConvexity", m_blob_detector.m_params.minConvexity);
	_settings.setValue(_key + "/Parameters/VoidInertiaRatio", m_blob_detector.m_params.minInertiaRatio);
	_settings.setValue(_key + "/Parameters/VoidColor", m_blob_detector.m_params.blobColor);

	// saving contour and id color
	_settings.setValue(_key + "/Parameters/ContourColor", QColor(m_blob_detector.m_contour_color[2], m_blob_detector.m_contour_color[1], m_blob_detector.m_contour_color[0], 255));
	_settings.setValue(_key + "/Parameters/ContourIdColor", QColor(m_blob_detector.m_keypoint_color[2], m_blob_detector.m_keypoint_color[1], m_blob_detector.m_keypoint_color[0], 255));
	_settings.setValue(_key + "/Parameters/ContourColorize", m_blob_detector.m_colorizeContourOnPercentage);

	// saving auto detected voids
	_settings.beginWriteArray(_key + "/ShapeVoidsAuto");
	for (auto i = 0; i < m_blob_detector.m_contours.size(); i++)
	{
		_settings.setArrayIndex(i);

		_settings.setValue("ContourAuto", QVariant::fromValue(XrayCVUtil::fromPoints<QPoint>(m_blob_detector.m_contours[i])));
		_settings.setValue("CenterAuto", XrayCVUtil::fromPoint<QPointF>(m_blob_detector.m_keypoints[i].pt));
		_settings.setValue("AreaAuto", m_blob_detector.m_areas[i]);
	}
	_settings.endArray();

	// saving custom/manual voids
	_settings.beginWriteArray(_key + "/ShapeVoids");
	for (auto i = 0; i < m_blob_detector.m_custom_contours.size(); i++)
	{
		_settings.setArrayIndex(i);

		_settings.setValue("Contour", QVariant::fromValue(XrayCVUtil::fromPoints<QPoint>(m_blob_detector.m_custom_contours[i])));
		_settings.setValue("Center", XrayCVUtil::fromPoint<QPointF>(m_blob_detector.m_custom_keypoints[i].pt));
		_settings.setValue("Area", m_blob_detector.m_custom_areas[i]);
	}
	_settings.endArray();
}
void XrayPaintBaseShape::getBlobSettings(QSettings& _settings, const QString& _key)
{
	cv::SimpleBlobDetector::Params params;

	m_blob_detector.m_name =  _settings.value(_key + "/Name", QString::fromStdString(m_blob_detector.m_name)).toString().toStdString();
	params.minRepeatability = _settings.value(_key + "/Parameters/Repeatability", m_blob_detector.m_params.minRepeatability).toDouble();
	params.thresholdStep =    _settings.value(_key + "/Parameters/ThresholdStep", m_blob_detector.m_params.thresholdStep).toDouble();
	params.minThreshold =     _settings.value(_key + "/Parameters/LowerThreshold", m_blob_detector.m_params.minThreshold).toDouble();
	params.maxThreshold =     _settings.value(_key + "/Parameters/HigherThreshold", m_blob_detector.m_params.maxThreshold).toDouble();
	params.minArea =          _settings.value(_key + "/Parameters/SmallestVoid", m_blob_detector.m_params.minArea).toDouble();
	params.maxArea =          _settings.value(_key + "/Parameters/LargestVoid", m_blob_detector.m_params.maxArea).toDouble();
	params.minCircularity =   _settings.value(_key + "/Parameters/VoidCircularity", m_blob_detector.m_params.minCircularity).toDouble();
	params.minConvexity =     _settings.value(_key + "/Parameters/VoidConvexity", m_blob_detector.m_params.minConvexity).toDouble();
	params.minInertiaRatio =  _settings.value(_key + "/Parameters/VoidInertiaRatio", m_blob_detector.m_params.minInertiaRatio).toDouble();
	params.blobColor =        _settings.value(_key + "/Parameters/VoidColor", m_blob_detector.m_params.blobColor).toDouble();
	m_blob_detector.m_params = params;

	// saving contour and id color
	auto c = _settings.value(_key + "/Parameters/ContourColor", QColor(m_blob_detector.m_contour_color[2], m_blob_detector.m_contour_color[1], m_blob_detector.m_contour_color[0], 255)).value<QColor>();
	m_blob_detector.m_contour_color = cv::Scalar(c.blue(), c.green(), c.red());

	c =	_settings.value(_key + "/Parameters/ContourIdColor", QColor(m_blob_detector.m_keypoint_color[2], m_blob_detector.m_keypoint_color[1], m_blob_detector.m_keypoint_color[0], 255)).value<QColor>();
	m_blob_detector.m_keypoint_color = cv::Scalar(c.blue(), c.green(), c.red());

	m_blob_detector.m_colorizeContourOnPercentage = _settings.value(_key + "/Parameters/ContourColorize", m_blob_detector.m_colorizeContourOnPercentage).toBool();

	// saving detected voids
	auto totalAutoVoids = _settings.beginReadArray(_key + "/ShapeVoidsAuto");
	if (totalAutoVoids > 0)
	{
		m_blob_detector.m_contours.resize(totalAutoVoids);
		m_blob_detector.m_keypoints.resize(totalAutoVoids);
		m_blob_detector.m_areas.resize(totalAutoVoids);

		for (auto i = 0; i < totalAutoVoids; i++)
		{
			_settings.setArrayIndex(i);

			m_blob_detector.m_contours[i] = XrayCVUtil::toPoints(_settings.value("ContourAuto").value<QVector<QPoint> >());
			m_blob_detector.m_keypoints[i].pt = XrayCVUtil::toPoint<QPointF>(_settings.value("CenterAuto").toPointF());
			m_blob_detector.m_areas[i] = _settings.value("AreaAuto").toDouble();
		}

	}
	_settings.endArray();

	// saving detected voids
	auto totalVoids = _settings.beginReadArray(_key + "/ShapeVoids");
	if (totalVoids > 0)
	{
		m_blob_detector.m_custom_contours.resize(totalVoids);
		m_blob_detector.m_custom_keypoints.resize(totalVoids);
		m_blob_detector.m_custom_areas.resize(totalVoids);

		for (auto i = 0; i < totalVoids; i++)
		{
			_settings.setArrayIndex(i);

			m_blob_detector.m_custom_contours[i] = XrayCVUtil::toPoints(_settings.value("Contour").value<QVector<QPoint> >());
			m_blob_detector.m_custom_keypoints[i].pt = XrayCVUtil::toPoint<QPointF>(_settings.value("Center").toPointF());
			m_blob_detector.m_custom_areas[i] = _settings.value("Area").toDouble();
		}

	}
	_settings.endArray();
}
/****************************************************************************/
/*!
	class to create a freehand shape
*/
XrayPaintBrushShape::XrayPaintBrushShape(const QPoint& _firstPoint) :
	XrayPaintBaseShape(PaintDrawModeType::Freehand)
{
	points.append(_firstPoint);
}
XrayPaintBrushShape::XrayPaintBrushShape(const XrayPaintBrushShape& _shape)
{
	m_blob_detector = _shape.m_blob_detector;
	m_shapeState = _shape.m_shapeState;
	m_type = _shape.m_type;
	m_name = _shape.m_name;
	m_pen = _shape.m_pen;
	m_brush = _shape.m_brush;
	m_font = _shape.m_font;
	m_isMoving = _shape.m_isMoving;
	points = _shape.points;
}
XrayPaintBaseShape* XrayPaintBrushShape::clone()
{
	return new XrayPaintBrushShape(*this);
}
void XrayPaintBrushShape::drawShape(QPainter& painter)
{
	painter.drawPolyline(points);
}
void XrayPaintBrushShape::moveCorners(const QRect& shapeRect) const
{
	auto cornerRectSize = QSize(m_pen.width() + 15, m_pen.width() + 15);

	m_topLeftCornerRect.setSize(cornerRectSize);
	m_topRightCornerRect.setSize(cornerRectSize);
	m_bottomLeftCornerRect.setSize(cornerRectSize);
	m_bottomRightCornerRect.setSize(cornerRectSize);

	m_topLeftCornerRect.moveCenter(shapeRect.topLeft());
	m_topRightCornerRect.moveCenter(shapeRect.topRight());
	m_bottomLeftCornerRect.moveCenter(shapeRect.bottomLeft());
	m_bottomRightCornerRect.moveCenter(shapeRect.bottomRight());
}
void XrayPaintBrushShape::drawCorners(QPainter& painter, const QRect& shapeRect) const
{
	moveCorners(shapeRect);
	drawCornerRect(painter, m_topLeftCornerRect);
	drawCornerRect(painter, m_topRightCornerRect);
	drawCornerRect(painter, m_bottomLeftCornerRect);
	drawCornerRect(painter, m_bottomRightCornerRect);
}
void XrayPaintBrushShape::drawSelectionMarker(QPainter &painter) const
{
	// draw corners
	auto r = XrayCVUtil::fromRect(cv::boundingRect(XrayCVUtil::toPoints(points)));
	r.moveCenter(r.center());
	drawCorners(painter, r);

	QPen newPen(m_pen);
	if (m_pen.width() <= 10) newPen.setWidthF(m_pen.width() + 5);
	else newPen.setWidthF(newPen.widthF() * 1.5);
	QColor newColor = Qt::black;
	newColor.setAlpha(150);
	newPen.setStyle(Qt::SolidLine);
	newPen.setColor(newColor);
	painter.setPen(newPen);
	painter.drawPolyline(points);
}
void XrayPaintBrushShape::mousePressEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_event);
	Q_UNUSED(_pos);
}
void XrayPaintBrushShape::mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	if (_event->buttons() == Qt::LeftButton)
		points.append(_pos);
}
void XrayPaintBrushShape::mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_pos);
	if (_event->button() == Qt::LeftButton)
		m_shapeState = PaintShapeState::Finished;
	m_isMoving = false;
	if (_event->button() == Qt::RightButton)
		m_shapeState = PaintShapeState::Canceled;

}
bool XrayPaintBrushShape::mouseIntersectCheck(const QPoint& _pos)
{
	m_isTopLeftCornerSelected = false;
	m_isTopRightCornerSelected = false;
	m_isBottomLeftCornerSelected = false;
	m_isBottomRightCornerSelected = false;

	if (m_topLeftCornerRect.contains(_pos))
	{
		m_isTopLeftCornerSelected = true;
		return true;
	}
	else if (m_topRightCornerRect.contains(_pos))
	{
		m_isTopRightCornerSelected = true;
		return true;
	}
	else if (m_bottomLeftCornerRect.contains(_pos))
	{
		m_isBottomLeftCornerSelected = true;
		return true;
	}
	else if (m_bottomRightCornerRect.contains(_pos))
	{
		m_isBottomRightCornerSelected = true;
		return true;
	}
	else
	{
		for (auto& point : points)
		{
			if (testX(_pos.x(), point.x()) && testY(_pos.y(), point.y()))
			{
				return true;
			}
		}
	}
	return false;
}
const QPointF XrayPaintBrushShape::getTopLeft() const
{
	qreal lowestX{ 9999.0 };
	qreal lowestY{ 9999.0 };
	for (auto& currentPoint : points)
	{
		if (currentPoint.x() < lowestX)
		{
			lowestX = currentPoint.x();
		}
		if (currentPoint.y() < lowestY)
		{
			lowestY = currentPoint.y();
		}
	}
	return QPointF{ lowestX, lowestY };
}
void XrayPaintBrushShape::mouseDrag(const QPoint& newPos)
{
	m_isMoving = true;
	auto topLeft = getTopLeft();
	for (int i = 0; i < points.count(); i++)
	{
		auto individualPointOffset = points.at(i) - topLeft;
		points[i] = newPos + individualPointOffset;
	}
}
void XrayPaintBrushShape::cornerDrag(const QPoint& newPos)
{
	if (m_isTopLeftCornerSelected || m_isBottomLeftCornerSelected)
	{
		auto diffX = lastCornerPosition.x() - newPos.x();
		if (diffX > 0) XrayQtUtil::scalePolygon(points, 1.01);
		if (diffX < 0)	XrayQtUtil::scalePolygon(points, 0.99);
		lastCornerPosition = newPos;
	}
	if (m_isTopRightCornerSelected || m_isBottomRightCornerSelected)
	{
		auto diffX = lastCornerPosition.x() - newPos.x();
		if (diffX > 0) XrayQtUtil::scalePolygon(points, 0.99);
		if (diffX < 0)	XrayQtUtil::scalePolygon(points, 1.01);
		lastCornerPosition = newPos;
	}
}
void XrayPaintBrushShape::wheelEvent(QWheelEvent* event)
{
	if (event->delta() > 0)
		XrayQtUtil::scalePoints(points, 1.01);
	else
		XrayQtUtil::scalePoints(points, 0.99);
}
void XrayPaintBrushShape::nudgeUp()
{
	for (int i = 0; i < points.count(); i++)
	{
		points[i] += QPointF{ 0, -1 };
	}
	//boundingRectangle.translate(0, -1);
}
void XrayPaintBrushShape::nudgeDown()
{
	for (int i = 0; i < points.count(); i++)
	{
		points[i] += QPointF{ 0, 1 };
	}
	//boundingRectangle.translate(0, 1);
}
void XrayPaintBrushShape::nudgeLeft()
{
	for (int i = 0; i < points.count(); i++)
	{
		points[i] += QPointF{ -1, 0 };
	}
	//boundingRectangle.translate(-1, 0);
}
void XrayPaintBrushShape::nudgeRight()
{
	for (int i = 0; i < points.count(); i++)
	{
		points[i] += QPointF{ 1, 0 };
	}
	//boundingRectangle.translate(1, 0);
}
void XrayPaintBrushShape::addPoint(const QPointF& newPoint)
{
	points.append(newPoint);
	//updateBoundingRectangle();
}
bool XrayPaintBrushShape::testX(const int mousePosX, const int pointX)
{
	if (m_pen.width() <= 5)
	{
		if (mousePosX <= pointX + 5 && mousePosX >= pointX - 5)
		{
			return true;
		}
		return false;
	}
	else
	{
		if (mousePosX <= pointX + m_pen.widthF() / 2 && mousePosX >= pointX - m_pen.widthF() / 2)
		{
			return true;
		}
		return false;
	}
}
bool XrayPaintBrushShape::testY(const int mousePosY, const int pointY)
{
	if (m_pen.width() <= 5)
	{
		if (mousePosY <= pointY + 5 && mousePosY >= pointY - 5)
		{
			return true;
		}
		return false;
	}
	else
	{
		if (mousePosY <= pointY + m_pen.widthF() / 2 && mousePosY >= pointY - m_pen.widthF() / 2)
		{
			return true;
		}
		return false;
	}
}
const int XrayPaintBrushShape::calculateSurfaceArea() const
{
	return XrayCVUtil::getNumberOfPixelsInsidePolygon(m_blob_detector.m_cropped.size(), getShapePoints());
}
cv::Rect XrayPaintBrushShape::getBoundingRect() const
{
	return XrayCVUtil::toRect(points.boundingRect());
}
std::vector<cv::Point> XrayPaintBrushShape::getShapePoints() const
{
	// convert points to relative points (local to bounding rectangle).
	auto p = points;
	auto r = points.boundingRect();
	for (auto i = 0; i < points.size(); i++)
		p[i] = QPointF(points[i].x() - r.x(), points[i].y() - r.y());

	return XrayCVUtil::toPoints(p);
}
void XrayPaintBrushShape::setPoints(const QPolygonF& _points)
{
	points = _points;
}
const QPolygonF XrayPaintBrushShape::getPoints() const
{
	return points;
}
const bool XrayPaintBrushShape::getIsMoving() const
{
	return m_isMoving;
}
/*const QPointF Shape_Freehand::getTopLeft() const
{
	qreal lowestX{9999.0};
	qreal lowestY{9999.0};
	for (int i = 0; i < points.count(); i++)
	{
		if (points.at(i).x() < lowestX)
		{
			lowestX = points.at(i).x();
		}
		if (points.at(i).y() < lowestY)
		{
			lowestY = points.at(i).y();
		}
	}
	return QPointF{lowestX, lowestY};
}

const QPointF Shape_Freehand::getBottomRight() const
{
	qreal highestX{0.0};
	qreal highestY{0.0};
	for (int i = 0; i < points.count(); i++)
	{
		if (points.at(i).x() > highestX)
		{
			highestX = points.at(i).x();
		}
		if (points.at(i).y() > highestY)
		{
			highestY = points.at(i).y();
		}
	}
	return QPointF{highestX, highestY};
}*/
/****************************************************************************/
/*!
	class to create a rectangle shape
*/
XrayPaintRectangleShape::XrayPaintRectangleShape(const QPoint& _topLeft) :
	XrayPaintBaseShape(PaintDrawModeType::Rectangle),
	rect(QRect(_topLeft, _topLeft))
{
	m_topLeftCornerRect.setSize(QSize(15, 15));
	m_topRightCornerRect.setSize(QSize(15, 15));
	m_bottomLeftCornerRect.setSize(QSize(15, 15));
	m_bottomRightCornerRect.setSize(QSize(15, 15));
}
XrayPaintRectangleShape::XrayPaintRectangleShape(const XrayPaintRectangleShape& _shape)
{
	m_blob_detector = _shape.m_blob_detector;
	m_shapeState = _shape.m_shapeState;
	m_type = _shape.m_type;
	m_name = _shape.m_name;
	m_pen = _shape.m_pen;
	m_brush = _shape.m_brush;
	m_font = _shape.m_font;
	m_isMoving = _shape.m_isMoving;
	rect = _shape.rect;
}
XrayPaintBaseShape* XrayPaintRectangleShape::clone()
{
	return new XrayPaintRectangleShape(*this);
}
void XrayPaintRectangleShape::drawShape(QPainter& painter)
{
	painter.drawRect(rect);
}
void XrayPaintRectangleShape::moveCorners(const QRect& shapeRect) const
{
	auto cornerRectSize = QSize(m_pen.width() + 15, m_pen.width() + 15);

	m_topLeftCornerRect.setSize(cornerRectSize);
	m_topRightCornerRect.setSize(cornerRectSize);
	m_bottomLeftCornerRect.setSize(cornerRectSize);
	m_bottomRightCornerRect.setSize(cornerRectSize);

	m_topLeftCornerRect.moveCenter(shapeRect.topLeft());
	m_topRightCornerRect.moveCenter(shapeRect.topRight());
	m_bottomLeftCornerRect.moveCenter(shapeRect.bottomLeft());
	m_bottomRightCornerRect.moveCenter(shapeRect.bottomRight());
}
void XrayPaintRectangleShape::drawCorners(QPainter& painter, const QRect& shapeRect) const
{
	moveCorners(shapeRect);
	drawCornerRect(painter, m_topLeftCornerRect);
	drawCornerRect(painter, m_topRightCornerRect);
	drawCornerRect(painter, m_bottomLeftCornerRect);
	drawCornerRect(painter, m_bottomRightCornerRect);
}
void XrayPaintRectangleShape::drawSelectionMarker(QPainter& painter) const
{
	// painter.drawRect(rect.adjusted(-2 - pen.width()/2, -2 - pen.width()/2, 2 + pen.width()/2, 2 + pen.width()/2));

	// draw corners
	drawCorners(painter, rect);

	// draw shape rectangle
	QPen newPen(m_pen);
	if (m_pen.width() <= 10) newPen.setWidthF(m_pen.width() + 5);
	else newPen.setWidthF(newPen.widthF() * 1.5);
	QColor newColor = Qt::black;
	newColor.setAlpha(150);
	newPen.setStyle(Qt::SolidLine);
	newPen.setColor(newColor);
	painter.setPen(newPen);
	painter.drawRect(rect);
}
void XrayPaintRectangleShape::mousePressEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_event);
	Q_UNUSED(_pos);
}
void XrayPaintRectangleShape::mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_event);
	rect.setBottomRight(_pos);
}
void XrayPaintRectangleShape::mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_pos);
	if (_event->button() == Qt::LeftButton)
		m_shapeState = PaintShapeState::Finished;
	else if (_event->button() == Qt::RightButton)
		m_shapeState = PaintShapeState::Canceled;
}
bool XrayPaintRectangleShape::mouseIntersectCheck(const QPoint& _pos)
{
	m_isTopLeftCornerSelected = false;
	m_isTopRightCornerSelected = false;
	m_isBottomLeftCornerSelected = false;
	m_isBottomRightCornerSelected = false;

	if (m_topLeftCornerRect.contains(_pos))
	{
		m_isTopLeftCornerSelected = true;
		return true;
	}
	else if (m_topRightCornerRect.contains(_pos))
	{
		m_isTopRightCornerSelected = true;
		return true;
	}
	else if (m_bottomLeftCornerRect.contains(_pos))
	{
		m_isBottomLeftCornerSelected = true;
		return true;
	}
	else if (m_bottomRightCornerRect.contains(_pos))
	{
		m_isBottomRightCornerSelected = true;
		return true;
	}
	else
	{
		return rect.contains(_pos);
	}

	return false;
}
void XrayPaintRectangleShape::mouseDrag(const QPoint& newPos)
{
	rect.moveTopLeft(newPos);
}
void XrayPaintRectangleShape::cornerDrag(const QPoint& newPos)
{
	if (m_isTopLeftCornerSelected)
		rect.setTopLeft(newPos);
	else if (m_isTopRightCornerSelected)
		rect.setTopRight(newPos);
	else if (m_isBottomLeftCornerSelected)
		rect.setBottomLeft(newPos);
	else if (m_isBottomRightCornerSelected)
		rect.setBottomRight(newPos);
}
void XrayPaintRectangleShape::wheelEvent(QWheelEvent* event)
{
	if (event->delta() > 0)
		rect.adjust(-1, -1, 1, 1);
	else
		rect.adjust(1, 1, -1, -1);
}
void XrayPaintRectangleShape::nudgeUp()
{
	rect.translate(0, -1);
}
void XrayPaintRectangleShape::nudgeDown()
{
	rect.translate(0, 1);
}
void XrayPaintRectangleShape::nudgeLeft()
{
	rect.translate(-1, 0);
}
void XrayPaintRectangleShape::nudgeRight()
{
	rect.translate(1, 0);
}
const QPointF XrayPaintRectangleShape::getTopLeft() const
{
	return rect.topLeft();
}
const int XrayPaintRectangleShape::calculateSurfaceArea() const
{
	auto r = rect.normalized();	// rectangle that has a non-negative width and height.
	return r.width() * r.height();
	//return XrayCVUtil::getNumberOfPixelsInsidePolygon(m_blob_detector.m_cropped.size(), getShapePoints());
}
cv::Rect XrayPaintRectangleShape::getBoundingRect() const
{
	auto r = rect.normalized();	// rectangle that has a non-negative width and height.
	return XrayCVUtil::toRect(r);
}
std::vector<cv::Point> XrayPaintRectangleShape::getShapePoints() const
{
	// absolute coordinate
	auto r = XrayCVUtil::fromRect(getBoundingRect());
	QVector<QPoint> points;
	points.append(r.topLeft());
	points.append(r.topRight());
	points.append(r.bottomRight());
	points.append(r.bottomLeft());

	// convert points to relative points (local to bounding rectangle).
	for (auto i = 0; i < points.size(); i++)
		points[i] = QPoint(points[i].x() - r.x(), points[i].y() - r.y());

	return XrayCVUtil::toPoints(points);
}
void XrayPaintRectangleShape::setRect(const QRect& _rect)
{
	rect = _rect;
	moveCorners(rect);
}
const QRect XrayPaintRectangleShape::getRect() const
{
	return rect;
}
const bool XrayPaintRectangleShape::getIsMoving() const
{
	return m_isMoving;
}
/****************************************************************************/
/*!
	class to create ellipse shape
*/
XrayPaintEllipseShape::XrayPaintEllipseShape(const QPoint& _topLeft) :
	XrayPaintBaseShape(PaintDrawModeType::Ellipse),
	rect(QRect(_topLeft, QPoint(_topLeft.x() + 20, _topLeft.y() + 20)))
{
	points.resize(360);
}
XrayPaintEllipseShape::XrayPaintEllipseShape(const XrayPaintEllipseShape& _shape)
{
	m_blob_detector = _shape.m_blob_detector;
	m_shapeState = _shape.m_shapeState;
	m_type = _shape.m_type;
	m_name = _shape.m_name;
	m_pen = _shape.m_pen;
	m_brush = _shape.m_brush;
	m_font = _shape.m_font;
	m_isMoving = _shape.m_isMoving;
	rect = _shape.rect;
	points = _shape.points;
}
XrayPaintBaseShape* XrayPaintEllipseShape::clone()
{
	return new XrayPaintEllipseShape(*this);
}
void XrayPaintEllipseShape::generateEllipsePoints(int xc, int yc, int a, int b, float alpha)
{
	auto t = 3.1415926 / 180.0;
	alpha = 360 - alpha;

	// Filling each pixel corresponding to every angle from 0 to 360 
	int theta;
	for (int i = 0; i < 360; i += 1) 
	{
		theta = i;
		int x = a * std::cos(t * theta) * std::cos(t * alpha)
			  + b * std::sin(t * theta) * std::sin(t * alpha);

		int y = b * std::sin(t * theta) * std::cos(t * alpha)
			  - a * std::cos(t * theta) * std::sin(t * alpha);

		points[i] = QPoint(xc + x, yc - y);
	}
}
void XrayPaintEllipseShape::generateEllipsePoints(const QRect& rect)
{
	auto width = rect.width() / 2;
	auto height = rect.height() / 2;
	auto origin = rect.center();

	generateEllipsePoints(origin.x(), origin.y(), width, height, 0);
}
void XrayPaintEllipseShape::drawShape(QPainter &painter)
{
	painter.drawEllipse(rect);

	generateEllipsePoints(rect);
	// this works too
	//painter.drawPolygon(points);
}
void XrayPaintEllipseShape::moveCorners(const QRect& shapeRect) const
{
	auto cornerRectSize = QSize(m_pen.width() + 15, m_pen.width() + 15);

	m_topLeftCornerRect.setSize(cornerRectSize);
	m_topRightCornerRect.setSize(cornerRectSize);
	m_bottomLeftCornerRect.setSize(cornerRectSize);
	m_bottomRightCornerRect.setSize(cornerRectSize);

	m_topLeftCornerRect.moveCenter(shapeRect.topLeft());
	m_topRightCornerRect.moveCenter(shapeRect.topRight());
	m_bottomLeftCornerRect.moveCenter(shapeRect.bottomLeft());
	m_bottomRightCornerRect.moveCenter(shapeRect.bottomRight());
}
void XrayPaintEllipseShape::drawCorners(QPainter& painter, const QRect& shapeRect) const
{
	moveCorners(shapeRect);
	drawCornerRect(painter, m_topLeftCornerRect);
	drawCornerRect(painter, m_topRightCornerRect);
	drawCornerRect(painter, m_bottomLeftCornerRect);
	drawCornerRect(painter, m_bottomRightCornerRect);
}
void XrayPaintEllipseShape::drawSelectionMarker(QPainter& painter) const
{
	// draw corners
	drawCorners(painter, rect);

	// draw shape rectangle
	auto newPen(m_pen);
	if (m_pen.width() <= 10) newPen.setWidthF(m_pen.width() + 5);
	else newPen.setWidthF(newPen.widthF() * 1.5);
	QColor newColor = Qt::black;
	newColor.setAlpha(150);
	newPen.setStyle(Qt::SolidLine);
	newPen.setColor(newColor);
	painter.setPen(newPen);
	painter.drawEllipse(rect);
}
void XrayPaintEllipseShape::nudgeUp()
{
	rect.translate(0, -1);
}
void XrayPaintEllipseShape::nudgeDown()
{
	rect.translate(0, 1);
}
void XrayPaintEllipseShape::nudgeLeft()
{
	rect.translate(-1, 0);
}
void XrayPaintEllipseShape::nudgeRight()
{
	rect.translate(1, 0);
}
const QPointF XrayPaintEllipseShape::getTopLeft() const
{
	return rect.topLeft();
}
const int XrayPaintEllipseShape::calculateSurfaceArea() const
{
	return XrayCVUtil::getNumberOfPixelsInsidePolygon(m_blob_detector.m_cropped.size(), getShapePoints());
}
cv::Rect XrayPaintEllipseShape::getBoundingRect() const
{
	auto extra = (m_pen.width() + 1) / 2;
	auto n = rect.normalized();	// rectangle that has a non-negative width and height.
	auto r = n.adjusted(-extra, -extra, extra, extra);
	return XrayCVUtil::toRect(r);
}
std::vector<cv::Point> XrayPaintEllipseShape::getShapePoints() const
{
	// convert points to relative points (local to bounding rectangle).
	auto p = points;
	auto r = getBoundingRect();
	for (auto i = 0; i < points.size(); i++)
		p[i] = QPoint(points[i].x() - r.x, points[i].y() - r.y);

	return XrayCVUtil::toPoints(p);
}
void XrayPaintEllipseShape::setRect(const QRect& _rect)
{
	rect = _rect;
	moveCorners(rect);
}
const QRect XrayPaintEllipseShape::getRect() const
{
	return rect;
}
const bool XrayPaintEllipseShape::getIsMoving() const
{
	return m_isMoving;
}
void XrayPaintEllipseShape::mouseDrag(const QPoint& newPos)
{
	rect.moveTopLeft(newPos);
}
void XrayPaintEllipseShape::cornerDrag(const QPoint& newPos)
{
	if (m_isTopLeftCornerSelected)
		rect.setTopLeft(newPos);
	else if (m_isTopRightCornerSelected)
		rect.setTopRight(newPos);
	else if (m_isBottomLeftCornerSelected)
		rect.setBottomLeft(newPos);
	else if (m_isBottomRightCornerSelected)
		rect.setBottomRight(newPos);
}
void XrayPaintEllipseShape::wheelEvent(QWheelEvent* event)
{
	if (event->delta() > 0)
		rect.adjust(-1, -1, 1, 1);
	else
		rect.adjust(1, 1, -1, -1);
}
void XrayPaintEllipseShape::mousePressEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_event);
	Q_UNUSED(_pos);
}
void XrayPaintEllipseShape::mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_event);
	rect.setBottomRight(_pos);
}
void XrayPaintEllipseShape::mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_pos);
	if (_event->button() == Qt::LeftButton)
		m_shapeState = PaintShapeState::Finished;
	else if (_event->button() == Qt::RightButton)
		m_shapeState = PaintShapeState::Canceled;
}
bool XrayPaintEllipseShape::mouseIntersectCheck(const QPoint& _pos)
{
	m_isTopLeftCornerSelected = false;
	m_isTopRightCornerSelected = false;
	m_isBottomLeftCornerSelected = false;
	m_isBottomRightCornerSelected = false;

	if (m_topLeftCornerRect.contains(_pos))
	{
		m_isTopLeftCornerSelected = true;
		return true;
	}
	else if (m_topRightCornerRect.contains(_pos))
	{
		m_isTopRightCornerSelected = true;
		return true;
	}
	else if (m_bottomLeftCornerRect.contains(_pos))
	{
		m_isBottomLeftCornerSelected = true;
		return true;
	}
	else if (m_bottomRightCornerRect.contains(_pos))
	{
		m_isBottomRightCornerSelected = true;
		return true;
	}
	else
	{
		auto powX = std::pow(_pos.x() - rect.center().x(), 2);
		auto powY = std::pow(_pos.y() - rect.center().y(), 2);
		auto powLeft = std::pow(rect.width() / 2, 2);
		auto powTop = std::pow(rect.height() / 2, 2);

		return powX / powLeft + powY / powTop <= 1;
	}

	return false;
}
/****************************************************************************/
/*!
	class to create circle shape
*/
XrayPaintCircleShape::XrayPaintCircleShape() :
	XrayPaintBaseShape(PaintDrawModeType::Circle),
	m_segments(300),
	m_radius(10)
{
	points.resize(m_segments);
}
XrayPaintCircleShape::XrayPaintCircleShape(const QPoint& _centerPoint) :
	XrayPaintBaseShape(PaintDrawModeType::Circle),
	m_segments(300),
	m_radius(10),
	m_centerPoint(_centerPoint)
{
	points.resize(m_segments);
	updatePoints();
}
XrayPaintCircleShape::XrayPaintCircleShape(const XrayPaintCircleShape& _shape)
{
	m_blob_detector = _shape.m_blob_detector;
	m_shapeState = _shape.m_shapeState;
	m_type = _shape.m_type;
	m_name = _shape.m_name;
	m_pen = _shape.m_pen;
	m_brush = _shape.m_brush;
	m_font = _shape.m_font;
	m_isMoving = _shape.m_isMoving;
	m_segments = _shape.m_segments;
	m_radius = _shape.m_radius;
	m_centerPoint = _shape.m_centerPoint;
	points = _shape.points;
}
XrayPaintBaseShape* XrayPaintCircleShape::clone()
{
	return new XrayPaintCircleShape(*this);
}
void XrayPaintCircleShape::drawShape(QPainter &painter)
{
	painter.drawPolygon(points);
}
void XrayPaintCircleShape::moveCorners(const QRect& shapeRect) const
{
	auto cornerRectSize = QSize(m_pen.width() + 15, m_pen.width() + 15);

	m_topLeftCornerRect.setSize(cornerRectSize);
	m_topRightCornerRect.setSize(cornerRectSize);
	m_bottomLeftCornerRect.setSize(cornerRectSize);
	m_bottomRightCornerRect.setSize(cornerRectSize);

	m_topLeftCornerRect.moveCenter(shapeRect.topLeft());
	m_topRightCornerRect.moveCenter(shapeRect.topRight());
	m_bottomLeftCornerRect.moveCenter(shapeRect.bottomLeft());
	m_bottomRightCornerRect.moveCenter(shapeRect.bottomRight());
}
void XrayPaintCircleShape::drawCorners(QPainter& painter, const QRect& shapeRect) const
{
	moveCorners(shapeRect);
	drawCornerRect(painter, m_topLeftCornerRect);
	drawCornerRect(painter, m_topRightCornerRect);
	drawCornerRect(painter, m_bottomLeftCornerRect);
	drawCornerRect(painter, m_bottomRightCornerRect);
}
void XrayPaintCircleShape::drawSelectionMarker(QPainter& painter) const
{
	// draw corners
	QRect r;
	r.setSize(QSize(m_radius * 2, m_radius * 2));
	r.moveCenter(m_centerPoint);
	drawCorners(painter, r);

	// draw circle shape selection
	auto newPen(m_pen);
	if (m_pen.width() <= 10) newPen.setWidthF(m_pen.width() + 5);
	else newPen.setWidthF(newPen.widthF() * 1.5);
	QColor newColor = Qt::black;
	newColor.setAlpha(150);
	newPen.setStyle(Qt::SolidLine);
	newPen.setColor(newColor);
	painter.setPen(newPen);
	painter.drawPolygon(points);
}
void XrayPaintCircleShape::mouseDrag(const QPoint& newPos)
{
	m_centerPoint.rx() = newPos.x() + m_radius;
	m_centerPoint.ry() = newPos.y() + m_radius;

	const auto topLeft = getTopLeft();
	for (auto i = 0; i < points.count(); i++)
	{
		if (points.at(i).x() > newPos.x())
		{
			qreal diffX = topLeft.x() - newPos.x();
			points[i].setX(points.at(i).x() - diffX);
		}
		else
		{
			qreal diffX = newPos.x() - topLeft.x();
			points[i].setX(points.at(i).x() + diffX);
		}
		if (points.at(i).y() > newPos.y())
		{
			qreal diffY = topLeft.y() - newPos.y();
			points[i].setY(points.at(i).y() - diffY);
		}
		else
		{
			qreal diffY = newPos.y() - topLeft.y();
			points[i].setY(points.at(i).y() + diffY);
		}
	}
	//boundingRectangle.moveTopLeft(newPos);
}
void XrayPaintCircleShape::cornerDrag(const QPoint& newPos)
{
	if (m_isTopLeftCornerSelected || m_isTopRightCornerSelected || m_isBottomLeftCornerSelected || m_isBottomRightCornerSelected)
	{
		m_radius = qAbs(m_centerPoint.x() - newPos.x());
		if (m_radius < 1)
			m_radius = 1;
		updatePoints();
	}
}
void XrayPaintCircleShape::nudgeUp()
{
	for (auto i = 0; i < points.count(); i++)
		points[i] += QPoint(0, -1);

	m_centerPoint = points.boundingRect().center().toPoint();
	//boundingRectangle.translate(0, -1);
}
void XrayPaintCircleShape::nudgeDown()
{
	for (auto i = 0; i < points.count(); i++)
		points[i] += QPoint(0, 1);
	//boundingRectangle.translate(0, 1);
	m_centerPoint = points.boundingRect().center().toPoint();
}
void XrayPaintCircleShape::nudgeLeft()
{
	for (auto i = 0; i < points.count(); i++)
		points[i] += QPoint(-1, 0);
	//boundingRectangle.translate(-1, 0);
	m_centerPoint = points.boundingRect().center().toPoint();
}
void XrayPaintCircleShape::nudgeRight()
{
	for (auto i = 0; i < points.count(); i++)
		points[i] += QPoint(1, 0);
	//boundingRectangle.translate(1, 0);
	m_centerPoint = points.boundingRect().center().toPoint();
}
void XrayPaintCircleShape::mousePressEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	if (_event->button() == Qt::LeftButton)
	{
		m_centerPoint = _pos;
		updatePoints();
	}
}
void XrayPaintCircleShape::mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_event);
	m_centerPoint = _pos;
	updatePoints();
}
void XrayPaintCircleShape::mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_pos);
	Q_UNUSED(_event);
	m_shapeState = PaintShapeState::Finished;
}
bool XrayPaintCircleShape::mouseIntersectCheck(const QPoint& _pos)
{
	m_isTopLeftCornerSelected = false;
	m_isTopRightCornerSelected = false;
	m_isBottomLeftCornerSelected = false;
	m_isBottomRightCornerSelected = false;

	if (m_topLeftCornerRect.contains(_pos))
	{
		m_isTopLeftCornerSelected = true;
		return true;
	}
	else if (m_topRightCornerRect.contains(_pos))
	{
		m_isTopRightCornerSelected = true;
		return true;
	}
	else if (m_bottomLeftCornerRect.contains(_pos))
	{
		m_isBottomLeftCornerSelected = true;
		return true;
	}
	else if (m_bottomRightCornerRect.contains(_pos))
	{
		m_isBottomRightCornerSelected = true;
		return true;
	}
	else
	{
		if (points.containsPoint(_pos, Qt::OddEvenFill))
		{
			calculateSurfaceArea();
			return true;
		}
	}

	return false;
}
void XrayPaintCircleShape::wheelEvent(QWheelEvent* _event)
{
	m_radius += _event->delta() / 120.0;
	if (m_radius < 1)
		m_radius = 1;
	updatePoints();
}
const QPointF XrayPaintCircleShape::getTopLeft() const
{
	qreal lowestX(999999999.0);
	qreal lowestY(999999999.0);
	for (int i = 0; i < points.size(); i++)
	{
		QPointF currentPoint = points.at(i);
		if (currentPoint.x() < lowestX)
			lowestX = currentPoint.x();

		if (currentPoint.y() < lowestY)
			lowestY = currentPoint.y();
	}

	return QPointF(lowestX, lowestY);
}
const int XrayPaintCircleShape::calculateSurfaceArea() const
{
	return XrayCVUtil::getNumberOfPixelsInsidePolygon(m_blob_detector.m_cropped.size(), getShapePoints());
}
cv::Rect XrayPaintCircleShape::getBoundingRect() const
{
	return XrayCVUtil::toRect(points.boundingRect());
}
std::vector<cv::Point> XrayPaintCircleShape::getShapePoints() const
{
	// convert points to relative points (local to bounding rectangle).
	auto p = points;
	auto r = points.boundingRect();
	for (auto i = 0; i < points.size(); i++)
		p[i] = QPointF(points[i].x() - r.x(), points[i].y() - r.y());

	return XrayCVUtil::toPoints(p);
}
void XrayPaintCircleShape::setRadius(double _radius)
{
	m_radius = _radius;
	updatePoints();
}
double XrayPaintCircleShape::getRadius() const
{
	return m_radius;
}
void XrayPaintCircleShape::setSegments(int _segments)
{
	m_segments = _segments;
	points.resize(m_segments);
	updatePoints();
}
int XrayPaintCircleShape::getSegments() const
{
	return m_segments;
}
void XrayPaintCircleShape::setCenter(const QPoint& _centerPoint)
{
	m_centerPoint = _centerPoint;
}
QPoint XrayPaintCircleShape::getCenter() const
{
	return m_centerPoint;
}
void XrayPaintCircleShape::updatePoints()
{
	for (int i = 0; i < m_segments; i++)
	{
		auto theta = 2.0 * 3.1415926 * double(i) / double(m_segments);
		auto x = m_radius * qCos(theta);
		auto y = m_radius * qSin(theta);
		points[i] = QPointF(x + m_centerPoint.x(), y + m_centerPoint.y());
	}
}
const QPolygonF XrayPaintCircleShape::getPoints() const
{
	return points;
}
const bool XrayPaintCircleShape::getIsMoving() const
{
	return m_isMoving;
}
/****************************************************************************/
/*!
	class to create line shape
*/
XrayPaintLineShape::XrayPaintLineShape(const QPoint& _firstPoint) :
	XrayPaintBaseShape(PaintDrawModeType::Line),
	m_line(QLine(_firstPoint, _firstPoint)),
	boundingRectangle(QRect(_firstPoint, _firstPoint))
{
}
XrayPaintLineShape::XrayPaintLineShape(const XrayPaintLineShape& _shape)
{
	m_blob_detector = _shape.m_blob_detector;
	m_shapeState = _shape.m_shapeState;
	m_type = _shape.m_type;
	m_name = _shape.m_name;
	m_pen = _shape.m_pen;
	m_brush = _shape.m_brush;
	m_font = _shape.m_font;
	m_isMoving = _shape.m_isMoving;
	m_line = _shape.m_line;
	boundingRectangle = _shape.boundingRectangle;
}
XrayPaintBaseShape* XrayPaintLineShape::clone()
{
	return new XrayPaintLineShape(*this);
}
void XrayPaintLineShape::drawShape(QPainter &painter)
{
	painter.drawLine(m_line);
}
void XrayPaintLineShape::moveCorners(const QRect& shapeRect) const
{
	auto cornerRectSize = QSize(m_pen.width() + 15, m_pen.width() + 15);

	m_topLeftCornerRect.setSize(cornerRectSize);
	m_topRightCornerRect.setSize(cornerRectSize);

	m_topLeftCornerRect.moveCenter(m_line.p1());
	m_topRightCornerRect.moveCenter(m_line.p2());

	Q_UNUSED(shapeRect);
}
void XrayPaintLineShape::drawCorners(QPainter& painter, const QRect& shapeRect) const
{
	moveCorners(shapeRect);
	drawCornerRect(painter, m_topLeftCornerRect);
	drawCornerRect(painter, m_topRightCornerRect);
}
void XrayPaintLineShape::drawSelectionMarker(QPainter& painter) const
{
	// draw corners
	drawCorners(painter, QRect(m_line.p1(), m_line.p2()));

	// draw line shape selection
	auto newPen(m_pen);
	if (m_pen.width() <= 10) newPen.setWidthF(m_pen.width() + 5);
	else newPen.setWidthF(newPen.widthF() * 1.5);
	QColor newColor = Qt::black;
	newColor.setAlpha(150);
	newPen.setStyle(Qt::SolidLine);
	newPen.setColor(newColor);
	painter.setPen(newPen);
	painter.drawLine(m_line);
}
void XrayPaintLineShape::mousePressEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_event);
	Q_UNUSED(_pos);
}
void XrayPaintLineShape::mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_event);
	m_line.setP2(_pos);
}
void XrayPaintLineShape::mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_pos);
	if (_event->button() == Qt::LeftButton)
	{
		boundingRectangle = QRect(m_line.p1(), m_line.p2());
		m_shapeState = PaintShapeState::Finished;
	}
	else if (_event->button() == Qt::RightButton)
		m_shapeState = PaintShapeState::Canceled;
}
bool XrayPaintLineShape::mouseIntersectCheck(const QPoint& _pos)
{
	m_isTopLeftCornerSelected = false;
	m_isTopRightCornerSelected = false;
	m_isBottomLeftCornerSelected = false;
	m_isBottomRightCornerSelected = false;

	if (m_topLeftCornerRect.contains(_pos))
	{
		m_isTopLeftCornerSelected = true;
		return true;
	}
	else if (m_topRightCornerRect.contains(_pos))
	{
		m_isTopRightCornerSelected = true;
		return true;
	}
	else
	{
		auto x1 = m_line.p1().x();
		auto y1 = m_line.p1().y();
		auto dY = m_line.dy();
		auto dX = m_line.dx();

		float a, d;
		if (dX == 0)	// if this line is straight vertical
		{
			a = 0;
			d = boundingRectangle.height();
		}
		else if (dY == 0)	// if this line is straight horizontal
		{
			a = 0;
			d = boundingRectangle.width();
		}
		else
		{
			a = static_cast<float>(dY) / static_cast<float>(dX);
			d = 0;
		}

		// y = ax + b
		// ax + b = y
		// b = y - ax
		auto b = static_cast<int>(y1 - static_cast<int>(a * x1));

		if (_pos.y() >= (a * _pos.x() + b - d) - 5 && _pos.y() <= (a * _pos.x() + b + d) + 5)
		{
			if (boundingRectangle.contains(_pos))
				return true;
		}
	}

	return false;
}
void XrayPaintLineShape::mouseDrag(const QPoint& newPos)
{
	if (m_line.p1().x() > newPos.x())
	{
		qreal diffX = m_line.p1().x() - newPos.x();
		m_line.setP1(QPoint(m_line.p1().x() - diffX, m_line.p1().y()));
		m_line.setP2(QPoint(m_line.p2().x() - diffX, m_line.p2().y()));
	}
	else
	{
		qreal diffX = newPos.x() - m_line.p1().x();
		m_line.setP1(QPoint((m_line.p1().x() + diffX), m_line.p1().y()));
		m_line.setP2(QPoint((m_line.p2().x() + diffX), m_line.p2().y()));
	}
	if (m_line.p1().y() > newPos.y())
	{
		qreal diffY = m_line.p1().y() - newPos.y();
		m_line.setP1(QPoint(m_line.p1().x(), m_line.p1().y() - diffY));
		m_line.setP2(QPoint(m_line.p2().x(), m_line.p2().y() - diffY));
	}
	else
	{
		qreal diffY = newPos.y() - m_line.p1().y();
		m_line.setP1(QPoint(m_line.p1().x(), m_line.p1().y() + diffY));
		m_line.setP2(QPoint(m_line.p2().x(), m_line.p2().y() + diffY));
	}
	boundingRectangle.moveTopLeft(newPos);
}
void XrayPaintLineShape::cornerDrag(const QPoint& newPos)
{
	if (m_isTopLeftCornerSelected)
	{
		m_line.setP1(newPos);
	}
	else if (m_isTopRightCornerSelected)
	{
		m_line.setP2(newPos);
	}
	boundingRectangle = QRect(m_line.p1(), m_line.p2());
}
void XrayPaintLineShape::wheelEvent(QWheelEvent* _event)
{
	Q_UNUSED(_event);
}
void XrayPaintLineShape::nudgeUp()
{
	m_line.setP1(m_line.p1() += QPoint{ 0, -1 });
	m_line.setP2(m_line.p2() += QPoint{ 0, -1 });
}
void XrayPaintLineShape::nudgeDown()
{
	m_line.setP1(m_line.p1() += QPoint{ 0, 1 });
	m_line.setP2(m_line.p2() += QPoint{ 0, 1 });
}
void XrayPaintLineShape::nudgeLeft()
{
	m_line.setP1(m_line.p1() += QPoint{ -1, 0 });
	m_line.setP2(m_line.p2() += QPoint{ -1, 0 });
}
void XrayPaintLineShape::nudgeRight()
{
	m_line.setP1(m_line.p1() += QPoint{ 1, 0 });
	m_line.setP2(m_line.p2() += QPoint{ 1, 0 });
}
const QPointF XrayPaintLineShape::getTopLeft() const
{
	return m_line.p1();
}
const int XrayPaintLineShape::calculateSurfaceArea() const
{
	//auto r = boundingRectangle.normalized();	// rectangle that has a non-negative width and height.
	//return qSqrt((r.width() * r.width()) + (r.height() * r.height()));
	return XrayCVUtil::getNumberOfPixelsInsidePolygon(m_blob_detector.m_cropped.size(), getShapePoints());
}
cv::Rect XrayPaintLineShape::getBoundingRect() const
{
	auto r = boundingRectangle.normalized();	// rectangle that has a non-negative width and height.
	return XrayCVUtil::toRect(r);
}
std::vector<cv::Point> XrayPaintLineShape::getShapePoints() const
{
	// absolute coordinate
	auto r = XrayCVUtil::fromRect(getBoundingRect());
	QVector<QPoint> points;
	points.append(r.topLeft());
	points.append(r.topRight());
	points.append(r.bottomRight());
	points.append(r.bottomLeft());

	// convert points to relative points (local to bounding rectangle).
	for (auto i = 0; i < points.size(); i++)
		points[i] = QPoint(points[i].x() - r.x(), points[i].y() - r.y());

	return XrayCVUtil::toPoints(points);
}
void XrayPaintLineShape::setLine(const QLine& _line)
{
	m_line = _line;
	boundingRectangle = QRect(m_line.p1(), m_line.p2());
	moveCorners(boundingRectangle);
}
const QLine XrayPaintLineShape::getLine() const
{
	return m_line;
}
const bool XrayPaintLineShape::getIsMoving() const
{
	return m_isMoving;
}
/****************************************************************************/
/*!
	class to create polygon shape
	draw polygon with control points if needed: https://codar.club/blogs/qt-writing-custom-control-7-custom-draggable-polygon.html
*/
XrayPaintPolygonShape::XrayPaintPolygonShape() :
	XrayPaintBaseShape(PaintDrawModeType::Polygon)
{
}
XrayPaintPolygonShape::XrayPaintPolygonShape(const QPoint& _firstPoint) :
	XrayPaintBaseShape(PaintDrawModeType::Polygon)
{
	points.append(_firstPoint);
	points.append(_firstPoint);
}
XrayPaintPolygonShape::XrayPaintPolygonShape(const XrayPaintPolygonShape& _shape)
{
	m_blob_detector = _shape.m_blob_detector;
	m_shapeState = _shape.m_shapeState;
	m_type = _shape.m_type;
	m_name = _shape.m_name;
	m_pen = _shape.m_pen;
	m_brush = _shape.m_brush;
	m_font = _shape.m_font;
	m_isMoving = _shape.m_isMoving;
	points = _shape.points;
}
XrayPaintBaseShape* XrayPaintPolygonShape::clone()
{
	return new XrayPaintPolygonShape(*this);
}
void XrayPaintPolygonShape::drawShape(QPainter &painter)
{
	painter.drawPolygon(points);
}
void XrayPaintPolygonShape::moveCorners(const QRect& shapeRect) const
{
	auto cornerRectSize = QSize(m_pen.width() + 15, m_pen.width() + 15);

	m_topLeftCornerRect.setSize(cornerRectSize);
	m_topRightCornerRect.setSize(cornerRectSize);
	m_bottomLeftCornerRect.setSize(cornerRectSize);
	m_bottomRightCornerRect.setSize(cornerRectSize);

	m_topLeftCornerRect.moveCenter(shapeRect.topLeft());
	m_topRightCornerRect.moveCenter(shapeRect.topRight());
	m_bottomLeftCornerRect.moveCenter(shapeRect.bottomLeft());
	m_bottomRightCornerRect.moveCenter(shapeRect.bottomRight());
}
void XrayPaintPolygonShape::drawCorners(QPainter& painter, const QRect& shapeRect) const
{
	moveCorners(shapeRect);
	drawCornerRect(painter, m_topLeftCornerRect);
	drawCornerRect(painter, m_topRightCornerRect);
	drawCornerRect(painter, m_bottomLeftCornerRect);
	drawCornerRect(painter, m_bottomRightCornerRect);
}
void XrayPaintPolygonShape::drawSelectionMarker(QPainter& painter) const
{
	// draw corners
	auto r = XrayQtUtil::boundingRect(points);
	r.moveCenter(r.center());
	drawCorners(painter, r);

	auto newPen(m_pen);
	if (m_pen.width() <= 10) newPen.setWidthF(m_pen.width() + 5);
	else newPen.setWidthF(newPen.widthF() * 1.5);
	QColor newColor = Qt::black;
	newColor.setAlpha(150);
	newPen.setStyle(Qt::SolidLine);
	newPen.setColor(newColor);
	painter.setPen(newPen);
	painter.drawPolygon(points);
}
void XrayPaintPolygonShape::mouseDrag(const QPoint& newPos)
{
	const auto topLeft = getTopLeft();
	for (auto i = 0; i < points.count(); i++)
	{
		if (points.at(i).x() > newPos.x())
		{
			qreal diffX = topLeft.x() - newPos.x();
			points[i].setX(points.at(i).x() - diffX);
		}
		else
		{
			qreal diffX = newPos.x() - topLeft.x();
			points[i].setX(points.at(i).x() + diffX);
		}
		if (points.at(i).y() > newPos.y())
		{
			qreal diffY = topLeft.y() - newPos.y();
			points[i].setY(points.at(i).y() - diffY);
		}
		else
		{
			qreal diffY = newPos.y() - topLeft.y();
			points[i].setY(points.at(i).y() + diffY);
		}
	}
	//boundingRectangle.moveTopLeft(newPos);
}
void XrayPaintPolygonShape::cornerDrag(const QPoint& newPos)
{
	if (m_isTopLeftCornerSelected || m_isBottomLeftCornerSelected)
	{
		auto diffX = lastCornerPosition.x() - newPos.x();
		if (diffX > 0) XrayQtUtil::scalePolygon(points, 1.01);
		if (diffX < 0)	XrayQtUtil::scalePolygon(points, 0.99);
		lastCornerPosition = newPos;
	}
	if (m_isTopRightCornerSelected || m_isBottomRightCornerSelected)
	{
		auto diffX = lastCornerPosition.x() - newPos.x();
		if (diffX > 0) XrayQtUtil::scalePolygon(points, 0.99);
		if (diffX < 0)	XrayQtUtil::scalePolygon(points, 1.01);
		lastCornerPosition = newPos;
	}
}
void XrayPaintPolygonShape::wheelEvent(QWheelEvent* event)
{
	if (event->delta() > 0)
		XrayQtUtil::scalePoints(points, 1.01);
	else
		XrayQtUtil::scalePoints(points, 0.99);
}
void XrayPaintPolygonShape::nudgeUp()
{
	for (auto i = 0; i < points.count(); i++)
		points[i] += QPoint{ 0, -1 };
	//boundingRectangle.translate(0, -1);
}
void XrayPaintPolygonShape::nudgeDown()
{
	for (auto i = 0; i < points.count(); i++)
		points[i] += QPoint{ 0, 1 };
	//boundingRectangle.translate(0, 1);
}
void XrayPaintPolygonShape::nudgeLeft()
{
	for (auto i = 0; i < points.count(); i++)
		points[i] += QPoint{ -1, 0 };
	//boundingRectangle.translate(-1, 0);
}
void XrayPaintPolygonShape::nudgeRight()
{
	for (auto i = 0; i < points.count(); i++)
		points[i] += QPoint{ 1, 0 };
	//boundingRectangle.translate(1, 0);
}
void XrayPaintPolygonShape::mousePressEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	if (_event->button() == Qt::LeftButton)
		points.append(_pos);
}
void XrayPaintPolygonShape::mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_event);
	if(!points.empty())
		points.replace(points.size() - 1, _pos);
}
void XrayPaintPolygonShape::mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_pos);
	if (_event->button() == Qt::RightButton)
		m_shapeState = PaintShapeState::Finished;
}
bool XrayPaintPolygonShape::mouseIntersectCheck(const QPoint& _pos)
{
	m_isTopLeftCornerSelected = false;
	m_isTopRightCornerSelected = false;
	m_isBottomLeftCornerSelected = false;
	m_isBottomRightCornerSelected = false;

	if (m_topLeftCornerRect.contains(_pos))
	{
		m_isTopLeftCornerSelected = true;
		return true;
	}
	else if (m_topRightCornerRect.contains(_pos))
	{
		m_isTopRightCornerSelected = true;
		return true;
	}
	else if (m_bottomLeftCornerRect.contains(_pos))
	{
		m_isBottomLeftCornerSelected = true;
		return true;
	}
	else if (m_bottomRightCornerRect.contains(_pos))
	{
		m_isBottomRightCornerSelected = true;
		return true;
	}
	else
	{
		if (points.containsPoint(_pos, Qt::OddEvenFill))
		{
			calculateSurfaceArea();
			return true;
		}
	}

	return false;
}
const QPointF XrayPaintPolygonShape::getTopLeft() const
{
	qreal lowestX(9999.0);
	qreal lowestY(9999.0);
	for (int i = 0; i < points.size(); i++)
	{
		QPointF currentPoint = points.at(i);
		if (currentPoint.x() < lowestX)
			lowestX = currentPoint.x();

		if (currentPoint.y() < lowestY)
			lowestY = currentPoint.y();
	}

	return QPointF(lowestX, lowestY);
}
const int XrayPaintPolygonShape::calculateSurfaceArea() const
{
	// exact and accurate solution
	return XrayCVUtil::getNumberOfPixelsInsidePolygon(m_blob_detector.m_cropped.size(), getShapePoints());
}
cv::Rect XrayPaintPolygonShape::getBoundingRect() const
{
	return XrayCVUtil::toRect(points.boundingRect());
}
std::vector<cv::Point> XrayPaintPolygonShape::getShapePoints() const
{
	// convert points to relative points (local to bounding rectangle).
	auto p = points;
	auto r = points.boundingRect();
	for (auto i = 0; i < points.size(); i++)
		p[i] = QPointF(points[i].x() - r.x(), points[i].y() - r.y());

	return XrayCVUtil::toPoints(p);
}
void XrayPaintPolygonShape::removeLastPoint()
{
	if (points.size() > 1)
		points.pop_back();
}
void XrayPaintPolygonShape::setPoints(const QPolygonF& _points)
{
	points = _points;
}
const QPolygonF XrayPaintPolygonShape::getPoints() const
{
	return points;
}
const bool XrayPaintPolygonShape::getIsMoving() const
{
	return m_isMoving;
}
/****************************************************************************/
/*!
	class to create arrow shape
*/
XrayPaintArrowShape::XrayPaintArrowShape(const QPoint& _firstPoint) :
	XrayPaintLineShape(_firstPoint),
	arrowSize(20.0),
	m_arrow_dir(Left)
{
	m_pen.setJoinStyle(Qt::RoundJoin);
	m_type = PaintDrawModeType::Arrow;
}
XrayPaintArrowShape::XrayPaintArrowShape(const XrayPaintArrowShape& _shape)
{
	m_blob_detector = _shape.m_blob_detector;
	m_shapeState = _shape.m_shapeState;
	m_type = _shape.m_type;
	m_name = _shape.m_name;
	m_pen = _shape.m_pen;
	m_brush = _shape.m_brush;
	m_font = _shape.m_font;
	m_isMoving = _shape.m_isMoving;
	m_line = _shape.m_line;
	boundingRectangle = _shape.boundingRectangle;
	arrowSize = _shape.arrowSize;
	arrowHead1 = _shape.arrowHead1;
	arrowHead2 = _shape.arrowHead2;
	m_arrow_dir = _shape.m_arrow_dir;
}
XrayPaintBaseShape* XrayPaintArrowShape::clone()
{
	return new XrayPaintArrowShape(*this);
}
void XrayPaintArrowShape::setArrowSize(qreal s)
{ 
	arrowSize = s; 
}
qreal XrayPaintArrowShape::getArrowSize() const
{ 
	return arrowSize; 
}
void XrayPaintArrowShape::setArrowDirection(int dir)
{ 
	m_arrow_dir = static_cast<ArrowDir>(dir);
}
int XrayPaintArrowShape::getArrowDirection() const
{ 
	return static_cast<int>(m_arrow_dir);
}
void XrayPaintArrowShape::setArrowHeads(const QPair<QPolygonF, QPolygonF>& _heads)
{
	arrowHead1 = _heads.first;
	arrowHead2 = _heads.second;
}
QPair<QPolygonF, QPolygonF> XrayPaintArrowShape::getArrowHeads() const
{
	return QPair<QPolygonF, QPolygonF>(arrowHead1, arrowHead2);
}
void XrayPaintArrowShape::drawShape(QPainter &painter)
{
	XrayPaintLineShape::drawShape(painter);

	auto angle = std::atan2(-m_line.dy(), m_line.dx());
	auto arrowP1 = m_line.p1() + QPointF(std::sin(angle + M_PI / 3) * arrowSize, std::cos(angle + M_PI / 3) * arrowSize);
	auto arrowP2 = m_line.p1() + QPointF(std::sin(angle + M_PI - M_PI / 3) * arrowSize, std::cos(angle + M_PI - M_PI / 3) * arrowSize);
	arrowHead1.clear();
	arrowHead1 << m_line.p1() << arrowP1 << arrowP2;

	angle = std::atan2(m_line.dy(), -m_line.dx());
	arrowP1 = m_line.p2() + QPointF(std::sin(angle + M_PI / 3) * arrowSize, std::cos(angle + M_PI / 3) * arrowSize);
	arrowP2 = m_line.p2() + QPointF(std::sin(angle + M_PI - M_PI / 3) * arrowSize, std::cos(angle + M_PI - M_PI / 3) * arrowSize);
	arrowHead2.clear();
	arrowHead2 << m_line.p2() << arrowP1 << arrowP2;

	auto br = m_brush;
	auto c = br.color();
	c.setAlpha(255);
	br.setStyle(Qt::BrushStyle::SolidPattern);
	br.setColor(c);
	painter.setBrush(br);
	if (m_arrow_dir == ArrowDir::Left)
	{
		painter.drawPolygon(arrowHead1);
	}
	else if (m_arrow_dir == ArrowDir::Right)
	{
		painter.drawPolygon(arrowHead2);
	}
	else if (m_arrow_dir == ArrowDir::Both)
	{
		painter.drawPolygon(arrowHead1);
		painter.drawPolygon(arrowHead2);
	}
	painter.setBrush(m_brush);
}
const int XrayPaintArrowShape::calculateSurfaceArea() const
{
	return XrayCVUtil::getNumberOfPixelsInsidePolygon(m_blob_detector.m_cropped.size(), getShapePoints());

	//return XrayPaintLineShape::calculateSurfaceArea() +
	//	QPolygonF(arrowHead1).boundingRect().width() * QPolygonF(arrowHead1).boundingRect().height() +
	//	QPolygonF(arrowHead2).boundingRect().width() * QPolygonF(arrowHead2).boundingRect().height();
}
QRectF XrayPaintArrowShape::boundingRect() const
{
	qreal extra = (m_pen.width() + 1) / 2.0;
	return QRectF(m_line.p1(), QSizeF(m_line.p2().x() - m_line.p1().x(), m_line.p2().y() - m_line.p1().y())).normalized().adjusted(-extra, -extra, extra, extra);
}
cv::Rect XrayPaintArrowShape::getBoundingRect() const
{
	auto r = QPolygonF(arrowHead1 + arrowHead2).boundingRect();
	auto n = r.normalized();	// rectangle that has a non-negative width and height.

	auto extra = (m_pen.width() + 1) / 2;
	auto rect = n.adjusted(-extra, -extra, extra, extra);
	return XrayCVUtil::toRect(rect);
}
std::vector<cv::Point> XrayPaintArrowShape::getShapePoints() const
{
	// absolute coordinate
	auto r = XrayCVUtil::fromRect(getBoundingRect());
	QVector<QPoint> points;
	points.append(r.topLeft());
	points.append(r.topRight());
	points.append(r.bottomRight());
	points.append(r.bottomLeft());

	// convert points to relative points (local to bounding rectangle).
	for (auto i = 0; i < points.size(); i++)
		points[i] = QPoint(points[i].x() - r.x(), points[i].y() - r.y());

	return XrayCVUtil::toPoints(points);
}
/****************************************************************************/
/*!
	class to create text shape
*/
XrayPaintTextShape::XrayPaintTextShape(const QPoint& _firstPoint) :
	XrayPaintBaseShape(PaintDrawModeType::Text),
	m_point(_firstPoint)
{
}
XrayPaintTextShape::XrayPaintTextShape(const XrayPaintTextShape& _shape)
{
	m_blob_detector = _shape.m_blob_detector;
	m_shapeState = _shape.m_shapeState;
	m_type = _shape.m_type;
	m_name = _shape.m_name;
	m_pen = _shape.m_pen;
	m_brush = _shape.m_brush;
	m_font = _shape.m_font;
	m_isMoving = _shape.m_isMoving;
	m_point = _shape.m_point;
	m_text = _shape.m_text;
}
XrayPaintBaseShape* XrayPaintTextShape::clone()
{
	return new XrayPaintTextShape(*this);
}
void XrayPaintTextShape::drawShape(QPainter &painter)
{
	painter.drawText(m_point, m_text);
}
void XrayPaintTextShape::drawSelectionMarker(QPainter& painter) const
{
	auto newPen(m_pen);
	QFontMetrics fm(m_font);
	auto rect = fm.boundingRect(m_text);
	rect.translate(m_point);
	rect.setSize(rect.size() + QSize(2, 2));
	painter.setPen(QPen(QColor(143, 21, 21), 1, Qt::DashLine));
	painter.drawRect(rect);
}
void XrayPaintTextShape::mouseDrag(const QPoint& newPos)
{
	const auto topLeft = getTopLeft();
	if (m_point.x() > newPos.x())
	{
		qreal diffX = topLeft.x() - newPos.x();
		m_point.setX(m_point.x() - diffX);
	}
	else
	{
		qreal diffX = newPos.x() - topLeft.x();
		m_point.setX(m_point.x() + diffX);
	}

	if (m_point.y() > newPos.y())
	{
		qreal diffY = topLeft.y() - newPos.y();
		m_point.setY(m_point.y() - diffY);
	}
	else
	{
		qreal diffY = newPos.y() - topLeft.y();
		m_point.setY(m_point.y() + diffY);
	}
	//boundingRectangle.moveTopLeft(newPos);
}
void XrayPaintTextShape::cornerDrag(const QPoint& newPos)
{
	Q_UNUSED(newPos);
}
void XrayPaintTextShape::wheelEvent(QWheelEvent* _event)
{
	Q_UNUSED(_event);
}
void XrayPaintTextShape::nudgeUp()
{
	m_point += QPoint(0, -1);
	//boundingRectangle.translate(0, -1);
}
void XrayPaintTextShape::nudgeDown()
{
	m_point += QPoint(0, 1);
	//boundingRectangle.translate(0, 1);
}
void XrayPaintTextShape::nudgeLeft()
{
	m_point += QPoint(-1, 0);
	//boundingRectangle.translate(-1, 0);
}
void XrayPaintTextShape::nudgeRight()
{
	m_point += QPoint(1, 0);
	//boundingRectangle.translate(1, 0);
}
void XrayPaintTextShape::mousePressEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	if (_event->button() == Qt::LeftButton)
		m_point = _pos;
}
void XrayPaintTextShape::mouseMoveEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_event);
	Q_UNUSED(_pos);
}
void XrayPaintTextShape::mouseReleaseEvent(const QMouseEvent* _event, const QPoint& _pos)
{
	Q_UNUSED(_pos);
	if (_event->button() == Qt::LeftButton)
		m_shapeState = PaintShapeState::Finished;
	else if (_event->button() == Qt::RightButton)
		m_shapeState = PaintShapeState::Finished;
}
bool XrayPaintTextShape::mouseIntersectCheck(const QPoint& _pos)
{
	QFontMetrics fm(m_font);
	auto rect = fm.boundingRect(m_text);
	rect.translate(m_point);
	if (rect.contains(_pos))
	{
		calculateSurfaceArea();
		return true;
	}

	return false;
}
const QPointF XrayPaintTextShape::getTopLeft() const
{
	return QPointF(static_cast<qreal>(m_point.x()), static_cast<qreal>(m_point.y()));
}
const int XrayPaintTextShape::calculateSurfaceArea() const
{
	QFontMetrics fm(m_font);
	auto rect = fm.boundingRect(m_text);
	return rect.width() * rect.height();
}
cv::Rect XrayPaintTextShape::getBoundingRect() const
{
	QFontMetrics fm(m_font);
	auto rect = fm.boundingRect(m_text);
	rect.setX(m_point.x());
	rect.setY(m_point.y());
	return XrayCVUtil::toRect(rect);
}
std::vector<cv::Point> XrayPaintTextShape::getShapePoints() const
{
	// absolute coordinate
	auto r = XrayCVUtil::fromRect(getBoundingRect());
	QVector<QPoint> points;
	points.append(r.topLeft());
	points.append(r.topRight());
	points.append(r.bottomRight());
	points.append(r.bottomLeft());

	// convert points to relative points (local to bounding rectangle).
	for (auto i = 0; i < points.size(); i++)
		points[i] = QPoint(points[i].x() - r.x(), points[i].y() - r.y());

	return XrayCVUtil::toPoints(points);
}
const bool XrayPaintTextShape::getIsMoving() const
{
	return m_isMoving;
}
void XrayPaintTextShape::setText(const QString& _text)
{
	m_text = _text;
}
QString XrayPaintTextShape::getText() const
{
	return m_text;
}
void XrayPaintTextShape::setPoint(const QPoint& _point)
{
	m_point = _point;
}
QPoint XrayPaintTextShape::getPoint() const
{
	return m_point;
}