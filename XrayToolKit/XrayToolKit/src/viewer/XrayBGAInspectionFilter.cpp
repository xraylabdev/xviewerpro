
#include "XrayBGAInspectionFilter.h"
#include "ui_XrayBGAInspectionFilter.h"
#include "XrayGlobal.h"
#include "XrayQtUtil.h"
#include "XrayCVUtil.h"
#include "XrayQFileUtil.h"
#include "XrayQDoubleSliderEditor.h"
#include "XrayVoidAnalysisFilter.h"
#include "XrayProgressWidget.h"
#include "XrayMainPaintWidget.h"
#include "XrayPaintMainWindow.h"
#include "XrayPaintShapes.h"
#include <QStandardPaths>
#include <QMessageBox>
#include <QInputDialog>
#include <QFileDialog>
#include <QDateTime>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayBGAInspectionFilter::XrayBGAInspectionFilter(QWidget *parent)
	: QWidget(parent),
	p_parent(parent),
	ui(new Ui::XrayBGAInspectionFilter),
	valuesset(false)
{
	setWindowTitle("XVOID PRO");
	ui->setupUi(this);

	ui->minDistSlider->setValues(70, 10, 0, 1, 500);
	ui->threshParam1Slider->setValues(120, 3, 0, 1, 250);
	ui->threshParam2Slider->setValues(20, 3, 0, 1, 255);
	ui->minRadiusSlider->setValues(20, 3, 0, 1, 100);
	ui->maxRadiusSlider->setValues(30, 3, 0, 21, 255);

	auto spinBoxW = 50;
	ui->minDistSlider->spinBox()->setFixedWidth(spinBoxW);
	ui->threshParam1Slider->spinBox()->setFixedWidth(spinBoxW);
	ui->threshParam2Slider->spinBox()->setFixedWidth(spinBoxW);
	ui->minRadiusSlider->spinBox()->setFixedWidth(spinBoxW);
	ui->maxRadiusSlider->spinBox()->setFixedWidth(spinBoxW);

	loadPresets();
	connect(ui->presetComboBox_2, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &XrayBGAInspectionFilter::selectPreset);
	connect(ui->minDistSlider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayBGAInspectionFilter::preview);
	connect(ui->threshParam1Slider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayBGAInspectionFilter::preview);
	connect(ui->threshParam1Slider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayBGAInspectionFilter::preview);
	connect(ui->minRadiusSlider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayBGAInspectionFilter::preview);
	connect(ui->maxRadiusSlider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayBGAInspectionFilter::preview);
	connect(ui->previewBtn, &QPushButton::clicked, this, &XrayBGAInspectionFilter::preview);
	connect(ui->clearAllBtn, &QPushButton::clicked, this, &XrayBGAInspectionFilter::clearShapes);
	connect(ui->setValueBtn, &QPushButton::clicked, this, &XrayBGAInspectionFilter::setBGAdimensions);
	connect(ui->UpdateBtn, &QPushButton::clicked, this, &XrayBGAInspectionFilter::updateWithCurrentPreset);
	connect(ui->saveBtn, &QPushButton::clicked, this, &XrayBGAInspectionFilter::savePreset);
	connect(ui->removeBtn, &QPushButton::clicked, this, &XrayBGAInspectionFilter::removePreset);
	connect(ui->updateNamesBtn, &QPushButton::clicked, this, &XrayBGAInspectionFilter::reOrderCircles);

	connect(ui->contourBtn, &QPushButton::clicked, this, &XrayBGAInspectionFilter::fitContoursToCircles);
	connect(ui->voidAnalysisBtn, &QPushButton::clicked, this, &XrayBGAInspectionFilter::performVoidAnalysis);
	connect(ui->gridBtn, &QPushButton::clicked, this, &XrayBGAInspectionFilter::drawGrid);
}
XrayBGAInspectionFilter::~XrayBGAInspectionFilter()
{
	delete ui;
}
XrayBGAInspectionFilter::Params XrayBGAInspectionFilter::getParametersFromSliders() {


	Params params;

	params.MinDist = ui->minDistSlider->value();
	params.Param1 = ui->threshParam1Slider->value();
	params.Param2 = ui->threshParam2Slider->value();
	params.MinRadius = ui->minRadiusSlider->value();		
	params.MaxRadius = ui->maxRadiusSlider->value();

	return params;

}
void XrayBGAInspectionFilter::preview()
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;
	
	area->resetShapeListModel();
	
	auto m_image = XrayCVUtil::toMat(area->getOriginalImage(), false);

	if (m_image.empty())
		return;

	if (m_image.channels() == 3 || m_image.channels() == 4)
		cv::cvtColor(m_image, gray, cv::COLOR_BGR2GRAY);
	else
		gray = m_image.clone();

	if (gray.type() != CV_8UC1)
	{
		throw std::invalid_argument("BGA detector only supports 8-bit images!");
	}	
	if (gray.empty())
		return;

	Params _params = XrayBGAInspectionFilter::getParametersFromSliders();
	std::vector<cv::Vec3f> circles;
	cv::HoughCircles(gray, circles, cv::HOUGH_GRADIENT, 1,
		_params.MinDist,  // change this value to detect circles with different distances to each other
		_params.Param1, _params.Param2, _params.MinRadius, _params.MaxRadius //  change the last two parameters
	//(min_radius & max_radius) to detect larger  circles
	);

	if (circles.empty())
		return;

	for (int i = 0; i <= circles.size() - 1; i++)
	{
		cv::Vec3i c = circles[i];
		auto circle = new XrayPaintCircleShape();
		circle->setName("Circle_bga");
		circle->setCenter(QPoint(c[0], c[1]));
		circle->setRadius(c[2]);
		circle->setSegments(300);
		area->addShape(circle);		
	}

}
void XrayBGAInspectionFilter::setParametersToSliders(Params _params) {

	auto b = ui->minDistSlider->blockSignals(true);
	ui->minDistSlider->setValue(_params.MinDist);
	ui->minDistSlider->blockSignals(b);

	b = ui->threshParam1Slider->blockSignals(true);
	ui->threshParam1Slider->setValue(_params.Param1);
	ui->threshParam1Slider->blockSignals(b);

	b = ui->threshParam2Slider->blockSignals(true);
	ui->threshParam2Slider->setValue(_params.Param2);
	ui->threshParam2Slider->blockSignals(b);

	b = ui->minRadiusSlider->blockSignals(true);
	ui->minRadiusSlider->setValue(_params.MinRadius);
	ui->minRadiusSlider->blockSignals(b);

	b = ui->maxRadiusSlider->blockSignals(true);
	ui->maxRadiusSlider->setValue(_params.MaxRadius);
	ui->maxRadiusSlider->blockSignals(b);

	XrayBGAInspectionFilter::preview();

}
void XrayBGAInspectionFilter::performVoidAnalysis()
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	if (area->getShapeListModel()->getShapeListConst().empty())
	{
		par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Couldn't find any shape in the Paint area!"));
		return;
	}

	// set void analysis tab as active tab
	par->setActiveTab(2);
	
	XrayVoidAnalysisFilter* void_filter;
	void_filter = par->getVoidFilter();
	if (void_filter) 
		for (auto& shape : area->getShapeListModel()->getShapeListConst())
		{
			area->setSelectedShape(shape);
			void_filter->preview();
		}
	else { par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Can not access Void Analysis")); }

}
void XrayBGAInspectionFilter::clearShapes() {

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	area->resetShapeListModel();
	valuesset = false;
}
void XrayBGAInspectionFilter:: drawGrid() {

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	auto shapes = area->getShapeListModel()->getShapeListConst();

	if (shapes.empty()){
		par->statusBar()->showMessage(QString("No shape selected.. Please select a shape and then click Generate grid.."));
		return;
	}
	int counter = 0;
	for (auto& shape : shapes)
		if (XrayPaintCircleShape* circle_shape = dynamic_cast<XrayPaintCircleShape*>(shape)) {
			counter++;
		}

	if (counter!=2) {
		par->statusBar()->showMessage(QString("Please draw select exactly two extreme circles to generate grid over BGA.."));
		return;
	}
	
	setBGAdimensions();

	if (_rows == 0 || _cols == 0) { 
		par->statusBar()->showMessage(QString("Please set the dimensions of BGA to proceed!"));
		return; }

	QPoint center1, center2, lastCircle, firstCircle;
	double radius1, radius2, avgRadius;         

	bool first = true;

	for (auto& shape : shapes)
	if (XrayPaintCircleShape* circle_shape = dynamic_cast<XrayPaintCircleShape*>(shape)) {
				
		if(first){
			center1 = circle_shape->getCenter();
			radius1 = circle_shape->getRadius();
		}
		else{
			center2 = circle_shape->getCenter();
			radius2 = circle_shape->getRadius();
		}
		first = false;
	}

	avgRadius = (radius1 + radius2) / 2;
		
	if (center2.rx() > center1.rx() && center2.ry() > center2.ry())
	{
		lastCircle = center2;
		firstCircle = center1;
	}
	else if (center1.rx() > center2.rx() && center1.ry() > center2.ry())
	{
		lastCircle = center1;
		firstCircle = center2;
	}
	generateGrid(firstCircle, lastCircle, avgRadius);

}
void XrayBGAInspectionFilter::generateGrid(QPoint firstCircle, QPoint lastCircle, double radius) {

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;
	
	// calculating spaces between circles
	double range_horizontal = lastCircle.rx() - firstCircle.rx();
	double range_vertical = lastCircle.ry() - firstCircle.ry();

	double seperation_horizontal = range_horizontal / (_cols-1);
	double seperation_vertical = range_vertical / (_rows - 1);

	clearShapes();

	double counter = 1;
	for (int i = 0; i < _rows; i++){
		for (int j = 0; j < _cols; j++) {
			
			QPoint loc = QPoint(firstCircle.rx() + j * seperation_horizontal, firstCircle.ry() + i * seperation_vertical);

			auto circle = new XrayPaintCircleShape();

			circle->setName(QString("Circle_bga_%2").arg(counter));
			circle->setCenter(loc);
			circle->setRadius(radius);
			circle->setSegments(300);
			area->addShape(circle);
			counter++;
		}
	}

}
void XrayBGAInspectionFilter::setBGAdimensions(){
	
	_rows = ui->spinBoxRows->value();
	_cols = ui->spinBoxColumns->value();
	valuesset = true;

}
void XrayBGAInspectionFilter::reOrderCircles() {

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	auto shapes = area->getShapeListModel()->getShapeListConst();
	if (shapes.empty())
		return;

	std::vector<XrayPaintCircleShape*> CircleShapes;
	
	for (int shape = 0; shape < shapes.size(); shape++) {
		if (XrayPaintCircleShape* circle_shape = dynamic_cast<XrayPaintCircleShape*>(shapes[shape])) {
			CircleShapes.push_back(circle_shape);
		}
	}
	struct BGACircles
	{
		XrayPaintCircleShape* circle;
		int center_x;
		int center_y;
	};
	
	if (CircleShapes.size() == 0)
		return;
	std::vector<BGACircles> sortedCircles(CircleShapes.size());
	for (std::size_t i = 0; i < CircleShapes.size(); i++)
	{
		sortedCircles[i].circle = CircleShapes[i];
		sortedCircles[i].center_x = CircleShapes[i]->getCenter().rx();
		sortedCircles[i].center_y = CircleShapes[i]->getCenter().ry();
	}

	std::sort(sortedCircles.begin(), sortedCircles.end(), [](const BGACircles& a, const BGACircles& b) -> bool
	{
		return a.center_x < b.center_x;
	});

	std::sort(sortedCircles.begin(), sortedCircles.end(), [](const BGACircles& a, const BGACircles& b) -> bool
	{
		return a.center_y < b.center_y;
	});


	if(valuesset && _rows*_cols == sortedCircles.size()){
		int begin_index = 0;
		for (int row = 0; row < _rows; row++) {
			std::sort(sortedCircles.begin() + begin_index, sortedCircles.begin() + (_cols*row + _cols), [](const BGACircles& a, const BGACircles& b) -> bool
			{
				return a.center_x < b.center_x;
			});
			begin_index = _cols * row + _cols;
		}
	}

	if(!sortedCircles.empty()){
		for (int i = 0; i < sortedCircles.size(); i++)
		{
			sortedCircles[i].circle->setName(QString("Circle_bga_%2").arg(i + 1));
		}
	}

}
void XrayBGAInspectionFilter::fitContoursToCircles(){

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	auto shapes = area->getShapeListModel()->getShapeListConst();
	if (shapes.empty())
		return;

	auto src = area->getImage();
	if (src.isNull())
		return;

	auto m_image = XrayCVUtil::toMat(area->getImage()).clone();

	if (m_image.empty())
		return;

	if (m_image.channels() == 3 || m_image.channels() == 4)
		cv::cvtColor(m_image, gray, cv::COLOR_BGR2GRAY);
	else
		gray = m_image.clone();

	if (gray.type() != CV_8UC1)
	{
		throw std::invalid_argument("BGA detector only supports 8-bit images!");
	}
	if (gray.empty())
		return;

	area->resetShapeListModel();

	std::vector<XrayPaintCircleShape*> CircleShapes;
	std::vector<XrayPaintBaseShape*> OtherShapes;
	
	for (int shape = 0; shape < shapes.size(); shape++) {
		if (XrayPaintCircleShape* circle_shape = dynamic_cast<XrayPaintCircleShape*>(shapes[shape])) {
			CircleShapes.push_back(circle_shape);
		}
		else {
			OtherShapes.push_back(shapes[shape]);
		}
	}

	if(!OtherShapes.empty())
		for (auto othershape : OtherShapes) {
			area->addShape(othershape);
		}

	if (CircleShapes.empty())
		return;
	
	cv::Rect m_roi;
	int counter = 1;
	bool found;
	for (auto circle : CircleShapes) {
		found = false;
		auto radius = circle->getRadius();
		auto center = circle->getCenter();
		auto rect = new XrayPaintRectangleShape();
		auto _topLeft = QPoint(center.rx() - int(radius + radius * 0.3), center.ry() - int(radius + radius * 0.3));
		auto _bottomRight = QPoint(center.rx() + int(radius + radius * 0.3), center.ry() + int(radius + radius * 0.3));
		rect->setRect(QRect(_topLeft, _bottomRight));
		rect->setName(QString("rect_bga_%2").arg(counter));
		auto rect_area = rect->calculateSurfaceArea();
		//area->addShape(rect);
		m_roi = rect->getBoundingRect();
		if ((m_roi.x < 0) || (m_roi.y < 0) || ((m_roi.x + m_roi.width) > src.width()) || ((m_roi.y + m_roi.height) > src.height()) || (m_roi.width < 2) || (m_roi.height < 2))
		{
			par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Shape ROI is out of range, please try again!"));
			area->addShape(circle);
			continue;
		}
		counter++; 
		
		auto m_cropped = cv::Mat(gray, m_roi).clone();

		cv::Mat frame(m_cropped.size(), m_cropped.type());
		blur(m_cropped, frame, cv::Size(3, 3));
		
		cv::Mat canny_output;
		int thresh = 150;	 // threshold for canny edge detector

		Canny(frame, canny_output, thresh, thresh * 2);
		cv::Mat resized_canny_output = cv::Mat::zeros(m_image.rows, m_image.cols, canny_output.type());
		canny_output.copyTo(resized_canny_output(m_roi));

		std::vector<std::vector<cv::Point>> contours;
		std::vector<cv::Vec4i> hierarchy;
		findContours(resized_canny_output, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);

		struct Contours
		{
			std::vector<cv::Point> contour_points;
			cv::Vec4i hierarchy;
			int area;
		};

		if (contours.size() == 0) {
			//area->addShape(circle);
			continue;
		}

		std::vector<Contours> sortedContours(contours.size());
		for (std::size_t i = 0; i < contours.size(); i++)
		{
			sortedContours[i].contour_points = contours[i];
			cv::Moments moms = cv::moments(contours[i]);
			sortedContours[i].area = moms.m00; 
			sortedContours[i].hierarchy = hierarchy[i];	
		}

		std::sort(sortedContours.begin(), sortedContours.end(), [](const Contours& a, const Contours& b) -> bool
		{
			return a.area > b.area;
		});

		for (size_t i = 0; i < sortedContours.size(); i++)
		{	
			if (sortedContours[i].hierarchy[3]<0 && sortedContours[i].area >100) {  // check if contour is not a child of other contour and if it's area is > 100 pixels
				QVector<QPoint> qvector;
				std::for_each(sortedContours[i].contour_points.begin(),
								sortedContours[i].contour_points.end(),
								[&qvector](const cv::Point &item) {
								qvector.push_back(QPoint(item.x, item.y)); });

				auto poly = new XrayPaintPolygonShape();
				poly->setPoints(QPolygonF(qvector));
				poly->setName(circle->getName());
				area->addShape(poly);
				found = true;
			}

		}
		if (!found)
			area->addShape(circle);
	}

}
static QString getDefaultPresets()
{
	return "[BGAInspectionPresets]\n"
			"Preset\\size=3\n"
			"Preset\\1\\Name=1\n"
			"Preset\\1\\MinDist=40\n"
			"Preset\\1\\Param1=120\n"
			"Preset\\1\\Param2=20\n"
			"Preset\\1\\MinRadius=27\n"
			"Preset\\1\\MaxRadius=30\n"
			"Preset\\2\\Name=2\n"
			"Preset\\2\\MinDist=40\n"
			"Preset\\2\\Param1=120\n"
			"Preset\\2\\Param2=20\n"
			"Preset\\2\\MinRadius=13\n"
			"Preset\\2\\MaxRadius=21\n"
			"Preset\\3\\Name=3\n"
			"Preset\\3\\MinDist=41\n"
			"Preset\\3\\Param1=120\n"
			"Preset\\3\\Param2=20\n"
			"Preset\\3\\MinRadius=24\n"
			"Preset\\3\\MaxRadius=27\n";
}
void XrayBGAInspectionFilter::loadPresets()
{
	// get the presets file path
	auto docPath = XrayQFileUtil::getStandardPath(QApplication::applicationName(), QStandardPaths::DocumentsLocation);
	auto filePath = XrayQFileUtil::getStandardPath("presets", docPath) + "xenhancer_presets.dat";
	if (!QFile::exists(filePath))	// if file doesn't exists then create with default presets
		XrayQFileUtil::write(getDefaultPresets(), filePath, XrayQFileUtil::Text, QFile::WriteOnly);

	// first clear all presets
	m_presets.clear();
	ui->presetComboBox_2->clear();

	// load presets from file
	QSettings settings(filePath, QSettings::Format::IniFormat);

	settings.beginGroup("BGAInspectionPresets");
	auto totalPresets = settings.beginReadArray("Preset");

	for (auto i = 0; i < totalPresets; i++)
	{
		settings.setArrayIndex(i);

		Params params;
		params.MinDist = settings.value("MinDist", 50).toInt();
		params.Param1 = settings.value("Param1", 120).toInt();
		params.Param2 = settings.value("Param2", 20).toInt();
		params.MinRadius = settings.value("MinRadius", 26).toInt();
		params.MaxRadius = settings.value("MinRadius", 31).toInt();

		m_presets.append(params);
		ui->presetComboBox_2->addItem(settings.value("Name", QString::number(i)).toString());
	}

	settings.endArray();
	settings.endGroup();
}
void XrayBGAInspectionFilter::selectPreset(int _index)
{
	if (_index >= 0 && _index < m_presets.size())
	{
		setParametersToSliders(m_presets.at(_index));
		preview();
	}
}
void XrayBGAInspectionFilter::savePresets()
{
	auto docPath = XrayQFileUtil::getStandardPath(QApplication::applicationName(), QStandardPaths::DocumentsLocation);
	auto filePath = XrayQFileUtil::getStandardPath("presets", docPath) + "xenhancer_presets.dat";

	QSettings settings(filePath, QSettings::Format::IniFormat);
	settings.clear();	// first, clear the previous settings

	settings.beginGroup("BGAInspectionPresets");
	settings.beginWriteArray("Preset");

	for (auto i = 0; i < m_presets.size(); i++)
	{
		settings.setArrayIndex(i);

		auto params = m_presets.at(i);
		settings.setValue("MinDist", static_cast<double>(params.MinDist));
		settings.setValue("Param1", static_cast<double>(params.Param1));
		settings.setValue("Param2", static_cast<double>(params.Param2));
		settings.setValue("MinRadius", static_cast<double>(params.MinRadius));
		settings.setValue("MaxRadius", static_cast<double>(params.MaxRadius));

	}

	settings.endArray();
	settings.endGroup();
}
void XrayBGAInspectionFilter::savePreset()
{
	QString lastPresetName = "Preset 0";
	if (ui->presetComboBox_2->count() > 0)
		lastPresetName = ui->presetComboBox_2->itemText(ui->presetComboBox_2->count() - 1);

	bool ok;
	auto text = QInputDialog::getText(this, QApplication::translate("XrayFlashFilter", "Enter New Preset Name"),
		QApplication::translate("XrayFlashFilter", "Enter Text"), QLineEdit::Normal, lastPresetName, &ok);
	if (ok && !text.isEmpty())
	{
		// append to the list and then write to file.
		m_presets.append(getParametersFromSliders());

		// add this preset to the combo box and set as current
		ui->presetComboBox_2->addItem(text);
		auto b = ui->presetComboBox_2->blockSignals(true);
		ui->presetComboBox_2->setCurrentText(text);
		ui->presetComboBox_2->blockSignals(b);

		// save all presets to file
		savePresets();

		// preview to update voids
		preview();
	}
}
void XrayBGAInspectionFilter::removePreset()
{
	auto index = ui->presetComboBox_2->currentIndex();
	if (index >= 0 && index < m_presets.size())
	{
		m_presets.removeAt(index);
		ui->presetComboBox_2->removeItem(index);
		savePresets();
	}
}
void XrayBGAInspectionFilter::updateWithCurrentPreset()
{
	selectPreset(ui->presetComboBox_2->currentIndex());
}
