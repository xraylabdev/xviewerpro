/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayPaintShapeListModel.cpp
** file base:	XrayPaintShapeListModel
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a model for Paint shapes.
****************************************************************************/

#include "XrayPaintShapeListModel.h"
#include "XrayPaintUndoCommands.h"
#include "XrayMainPaintWidget.h"

#include <QApplication>

XRAYLAB_USING_NAMESPACE

XrayPaintShapeListModel::XrayPaintShapeListModel(XrayMainPaintWidget* parent) :
	QAbstractListModel(parent),
	undoStack(*parent->getUndoStack())
{
}

Qt::ItemFlags XrayPaintShapeListModel::flags(const QModelIndex& index) const
{
	if (index.isValid())
		return  QAbstractListModel::flags(index) | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsEnabled | Qt::ItemIsEditable;

	return Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsEnabled;
}

Qt::DropActions XrayPaintShapeListModel::supportedDropActions() const
{
	return Qt::CopyAction | Qt::MoveAction;
}

const QList<XrayPaintBaseShape*>& XrayPaintShapeListModel::getShapeListConst() const
{
	return shapeList;
}

QList<XrayPaintBaseShape*>& XrayPaintShapeListModel::getShapeList()
{
	return shapeList;
}

int XrayPaintShapeListModel::rowCount(const QModelIndex& parent) const
{
	parent;
	return shapeList.size();
}

QVariant XrayPaintShapeListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal)
		return QString(QApplication::translate("XrayPaintShapeListModel", "Column") + " %1").arg(section);
	else
		return QString(QApplication::translate("XrayPaintShapeListModel", "Row") + " %1").arg(section);
}

QVariant XrayPaintShapeListModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (index.row() >= shapeList.size())
		return QVariant();

	if (role == Qt::DisplayRole)
		return QVariant::fromValue(shapeList.at(index.row())->getName());
	else if (role == Qt::EditRole)	// it's important, when a user double click on item to edit it, if you don't have editRole here, the item clears it's text. So editrole is a must here.
		return QVariant::fromValue(shapeList.at(index.row())->getName());
	else
		return QVariant();
}

bool XrayPaintShapeListModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
	/* This is a factory Qt function whose purpose is to enter data into a specific place within
	* data model. It is most commonly used in editing data on a list, for example,
	* When a user makes a double click on an entry and changes its value, this change
	* is done through this function.
	* In this case:
	* -index - line within the list over which we want to make some change of data
	* -value - QVariant container containing data to be entered into the model
	* -role - the role that defines the context of data insertion */

	if (index.isValid() && role == Qt::UserRole)
	{
		/* Qt::UserRole is a self - defined role that is used in this case
		*  Add a new shape to a specific line in the data model.Before that it is necessary
		*  is to check if the index to which the form is to be added is valid. 
		*/

		XrayPaintBaseShape* shape = nullptr; // First, we are doing a new empty basic class.
		if (value.canConvert<XrayPaintBaseShape*>()) // It is necessary to check whether the data is compatible with the base class.
		{
			shape = value.value<XrayPaintBaseShape*>(); // If it is, the cursor is assigned the address of the form stored in the value.
		}
		shapeList.replace(index.row(), shape); // On the previously inserted empty line we insert content (the address of the new shape).
		emit dataChanged(index, index);	// We send a signal by which we inform the rest of the content change model on that index.
		return true; // Restoring true let's know that the operation was successfully performed.
	}

	if (index.isValid() && role == Qt::EditRole)
	{
		/* Qt::EditRole is used when a user creates a double-click on the name of a particular shape,
		* which allows him to change his name.*/
		
		if (!value.toString().isEmpty() && !value.toString().trimmed().isEmpty())
		{
			shapeList[index.row()]->setName(value.toString()); // The shape at the index position is assigned the name entered.
			emit dataChanged(index, index); // A signal that notifies the change of data on that index is sent.
			return true; // A message of successful operation is returned.
		}
	}
	else if (role == Qt::DisplayRole)
	{
		/* Qt::DisplayRole is active when, for example, the user tries to change the order of the form on the list(drag - n - drop). */

		XrayPaintBaseShape* shape = nullptr;
		for (int i = 0; i < shapeList.size(); i++)
		{
			if (i == index.row())
			{
				//do nothing
			}
			else
			{
				if (shapeList.at(i)->getName() == value.toString())
				{
					shape = shapeList.at(i);
					if (shape)
					{
						shapeList.replace(index.row(), shape);
						emit dataChanged(index, index);
						return true;
					}
				}
			}
		}
	}
	return false;
}

bool XrayPaintShapeListModel::insertRows(int row, int count, const QModelIndex& parent)
{   
	Q_UNUSED(parent)
	Q_UNUSED(count)
	// Inserts an empty line in the form list (used only when
	// drag-n-drop operation over forms).

	beginInsertRows(QModelIndex(), row, row);
	shapeList.insert(row, nullptr);
	endInsertRows();
	return true;
}

bool XrayPaintShapeListModel::removeRows(int row, int count, const QModelIndex& parent)
{
	Q_UNUSED(parent)
	// Deletes the row from the list (used only when drag-n-drop
	// operation over forms).

	beginRemoveRows(QModelIndex(), row, row + count - 1);
	shapeList.removeAt(row);
	endRemoveRows();
	return true;
}

void XrayPaintShapeListModel::insertShapeAt(XrayPaintBaseShape* newShape, int row)
{
	insertRows(row, 1, QModelIndex()); // By invoking the insertRows function, a new empty row is added to the zero index.
	QModelIndex index = QAbstractListModel::index(row, 0, QModelIndex()); // A new index is created that indicates this line.
	QVariant data; // A new QVariant type variable is created that will pack a new shape.
	data.setValue(newShape); // Data Variables are assigned the address of the new format.
	setData(index, data, Qt::UserRole); // By calling the setData function, we assign the data (the address of the new shape) to the inserted blank line.
}

XrayPaintBaseShape* XrayPaintShapeListModel::removeShapeAt(int row)
{
	XrayPaintBaseShape* shape = shapeList.at(row);			// Take the first form from the list and save it to the shape pointer
	removeRows(row, 1, QModelIndex());
	QModelIndex removedShapeIndex = index(row);				// Define an index of the deleted line
	emit dataChanged(removedShapeIndex, removedShapeIndex);	// Signalize the change of data on that index
	return shape;
}

void XrayPaintShapeListModel::clear()
{
	if (!shapeList.empty())
	{
		beginRemoveRows(QModelIndex(), 0, shapeList.size() - 1);
		shapeList.clear();
		endRemoveRows();
	}
}
void XrayPaintShapeListModel::swapList(QList<XrayPaintBaseShape*>& newList)
{
	if (newList.isEmpty() && shapeList.isEmpty())
	{
		// nothing
	}
	else
	{
		shapeList.swap(newList);
		emit dataChanged(index(0), index(newList.size() - 1));
	}
}

void XrayPaintShapeListModel::deleteShapeViaContextMenu(int row)
{
	/* This function deletes the shape in a specific position
	*  which is defined when the user right - click the mouse
	*  opens the context menu over some form in the list. 
	*/

	undoStack.push(new XrayPaintDeleteShapeCommand(*this, row)); // An undo command is added to the undo pile, which refers to the line that the user clicked.
}


void XrayPaintShapeListModel::moveShapeUp(int row)
{
	if (0 < row && row < shapeList.size())
	{
		beginMoveRows(QModelIndex(), row, row, QModelIndex(), row - 1);
		shapeList.swap(row, row - 1);
		endMoveRows();
		emit dataChanged(QModelIndex(index(row)), QModelIndex(index(row - 1)));
	}
}

void XrayPaintShapeListModel::moveShapeDown(int row)
{
	if (row + 1 < shapeList.size())
	{
		beginMoveRows(QModelIndex(), row, row, QModelIndex(), row + 2);
		shapeList.swap(row, row + 1);
		endMoveRows();
		emit dataChanged(QModelIndex(index(row)), QModelIndex(index(row + 1)));
	}
}
