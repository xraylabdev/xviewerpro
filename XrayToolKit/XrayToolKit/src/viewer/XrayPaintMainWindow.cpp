/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayPaintMainWindow.cpp
** file base:	XrayPaintMainWindow
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the main paint window.
****************************************************************************/

#include "XrayPaintMainWindow.h"
#include "XrayMainPaintWidget.h"
#include "XrayStandardModelCompleterTreeViewWidget.h"
#include "XrayStandardModelTreeView.h"
#include "XrayGlobal.h"
#include "XrayCVUtil.h"
#include "XrayQFileUtil.h"
#include "fvkContrastEnhancerUtil.h"

#include <QPrinter>
#include <QPrintDialog>
#include <QScrollArea>
#include <QScrollBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QColorDialog>
#include <QMimeData>
#include <QMessageBox>
#include <QInputDialog>
#include <QDateTime>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayPaintMainWindow::XrayPaintMainWindow(QWidget *parent) :
	QMainWindow(parent)
{
	initializeUi();
}
void XrayPaintMainWindow::initializeUi()
{
	setWindowTitle("XPAINT PRO");
	setAcceptDrops(true);

	m_pageId = 0;
	clipboard = QApplication::clipboard();
	m_use_antialiasing = true;
	m_mode = PaintDrawModeType::Selection;
	m_brush = QBrush(QColor(0, 0, 0, 10), Qt::SolidPattern);
	m_brush.setColor(QColor(255, 255, 255, 10));
	m_pen = QPen(m_brush, 1.0, Qt::SolidLine, Qt::RoundCap);
	m_arrow_dir = 0;
	m_arrow_size = 20;
	m_numImagesPerPage = 1;
	m_zoomScale = 1.0;

	leftTreeDockWidget = new QDockWidget(QApplication::translate("XrayPaintMainWindow", "Project"), this);
	leftTreeDockWidget->setMinimumSize(QSize(50, 10));
	leftTreeDockWidget->setAutoFillBackground(false);
	leftTreeDockWidget->setEnabled(true);
	leftTreeDockWidget->setFeatures(QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
	leftTreeDockWidget->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	//auto dockWidgetTitle = new QLabel("Settings", leftTreeDockWidget);
	//dockWidgetTitle->setStyleSheet("QLabel { font-size: 11px; font-weight: bold; color: rgb(255, 255, 255); }");
	//leftTreeDockWidget->setTitleBarWidget(dockWidgetTitle);
	leftTreeDockWidget->setStyleSheet("QDockWidget { font-size: 12px; } QLabel { font-size: 11px; font-weight: normal; }");

	//centralWidget = new QWidget(this);
	//centralWidgetLayout = new QHBoxLayout(centralWidget);
	//centralWidgetLayout->setContentsMargins(8, 8, 8, 8);
	documentTab = new XrayPaintTabWidget(this);
	documentTab->setTabPosition(QTabWidget::South);
	connect(documentTab, &QTabWidget::currentChanged, this, &XrayPaintMainWindow::on_documentTab_currentChanged);
	connect(documentTab, &XrayPaintTabWidget::tabAboutToClose, this, &XrayPaintMainWindow::tabAboutToClose);
	connect(documentTab, &XrayPaintTabWidget::tabAboutToCloseOnSave, this, &XrayPaintMainWindow::tabAboutToCloseOnSave);
	//centralWidgetLayout->addWidget(documentTab);
	setCentralWidget(documentTab);

	projTreeView = new XrayStandardModelCompleterTreeViewWidget(XrayStandardModelTreeView::SelectAll | XrayStandardModelTreeView::Sort | XrayStandardModelTreeView::MoveTo | XrayStandardModelTreeView::AddParent | XrayStandardModelTreeView::ChangeText | XrayStandardModelTreeView::Copy | XrayStandardModelTreeView::RemoveSelected | XrayStandardModelTreeView::ExpandAll | XrayStandardModelTreeView::CollapseAll | XrayStandardModelTreeView::OpenInExplorer, leftTreeDockWidget);
	projTreeView->treeView()->setParentFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);
	projTreeView->treeView()->setShowWarningMessageBoxOnRemove(true);
	connect(documentTab, &QTabWidget::currentChanged, this, &XrayPaintMainWindow::activateTreeItemFromTab);
	connect(projTreeView, &XrayStandardModelCompleterTreeViewWidget::pressed, this, &XrayPaintMainWindow::activateTabFromTreeItem);
	connect(projTreeView, &XrayStandardModelCompleterTreeViewWidget::activatedItem, this, &XrayPaintMainWindow::activateTabFromTreeItem);
	connect(projTreeView, &XrayStandardModelCompleterTreeViewWidget::removedItem, this, &XrayPaintMainWindow::deleteTabFromTreeItem);
	connect(documentTab, &XrayPaintTabWidget::tabAboutToClose, this, &XrayPaintMainWindow::deleteTreeItemFromTab);

	connect(projTreeView->treeView(), &XrayStandardModelTreeView::itemsMoved, this, &XrayPaintMainWindow::syncTabsWithTreeModel);
	connect(projTreeView->treeView(), &XrayStandardModelTreeView::itemsSorted, this, &XrayPaintMainWindow::syncTabsWithTreeModel);

	leftTreeDockWidget->setWidget(projTreeView);
	addDockWidget(Qt::LeftDockWidgetArea, leftTreeDockWidget);

	dockWidget = new QDockWidget(QApplication::translate("XrayPaintMainWindow", "Modules"), this);
	dockWidget->setMinimumSize(QSize(330, 10));
	dockWidget->setAutoFillBackground(false);
	dockWidget->setEnabled(true);
	dockWidget->setFeatures(QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
	dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	//auto dockWidgetTitle = new QLabel("Settings", dockWidget);
	//dockWidgetTitle->setStyleSheet("QLabel { font-size: 11px; font-weight: bold; color: rgb(255, 255, 255); }");
	//dockWidget->setTitleBarWidget(dockWidgetTitle);
	dockWidget->setStyleSheet("QDockWidget { font-size: 12px; } QLabel { font-size: 11px; font-weight: normal; }");

	initializeMenus();
	initializeStatusBar();
	initializeToolBar();
	initializeOptionsTab();

	resize(800, 800);
}
void XrayPaintMainWindow::writeSettings(QSettings& settings, const QString& projectFileName)
{
	settings.beginGroup("XPaintPro");

	p_flashfilter->writeSettings(settings);
	p_voidFilter->writeSettings(settings);

	writeShapeSettings(settings);

	settings.setValue("Images/DirName", QFileInfo(projectFileName).baseName());
	settings.setValue("Images/Captions", QVariant::fromValue(getFileNamesAndCaptions()));
	settings.setValue("Images/PerPageImages", m_numImagesPerPage);
	settings.setValue("Images/TreeDataItems", QVariant::fromValue(projTreeView->treeView()->treeData()));

	settings.setValue("Outline/Color", QColor(slider_strokecolor_red->value(), slider_strokecolor_green->value(), slider_strokecolor_blue->value(), slider_stroke_opacity->value()));
	settings.setValue("Outline/Size", slider_stroke_width->value());
	settings.setValue("Outline/Opacity", slider_stroke_opacity->value());
	settings.setValue("Outline/RoundCap", radiobutton_roundcap->isChecked());
	settings.setValue("Outline/Style", combobox_penstyle->currentIndex());
	settings.setValue("Outline/Arrow/Direction", m_arrow_dir);
	settings.setValue("Outline/Arrow/Size", m_arrow_size);

	settings.setValue("Fill/Color", QColor(slider_fillcolor_red->value(), slider_fillcolor_green->value(), slider_fillcolor_blue->value(), slider_fill_opacity->value()));
	settings.setValue("Fill/Opacity", slider_fill_opacity->value());
	settings.setValue("Fill/Pattern", combobox_brushstyle->currentIndex());

	settings.setValue("Font/Font", fontCombo->currentIndex());
	settings.setValue("Font/Size", fontSizeCombo->currentText());
	settings.setValue("Font/Bold", boldAction->isChecked());
	settings.setValue("Font/Italic", italicAction->isChecked());
	settings.setValue("Font/Underline", underlineAction->isChecked());

	settings.setValue("RecentProjectFiles", menu_recentProjFiles->saveState());
	settings.setValue("RecentFiles", menu_recentFiles->saveState());
	settings.setValue("CurrentDirectory", XrayGlobal::getLastFileOpenPath());

	settings.endGroup();
}
void XrayPaintMainWindow::writeShapeSettings(QSettings& settings)
{
	settings.beginWriteArray("Project/PaintAreas");

	for (auto i = 0; i < documentTab->count(); i++)
	{
		const auto scroll = documentTab->widget(i)->findChild<QScrollArea*>("tabScrollArea");
		const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (paintArea)
		{
			settings.setArrayIndex(i);

			auto key = QFileInfo(paintArea->getFileName()).fileName();

			settings.setValue(key + "/FilePath", paintArea->getFileName());
			settings.setValue(key + "/ZoomFactor", paintArea->zoom());
			settings.setValue(key + "/ShowCrosshair", paintArea->isCrosshairEnabled());
			settings.setValue(key + "/ShowGrid", paintArea->isGridEnabled());
			settings.setValue(key + "/ScrollPosX", scroll->horizontalScrollBar()->value());
			settings.setValue(key + "/ScrollPosY", scroll->verticalScrollBar()->value());

			settings.beginWriteArray("Shapes");

			for(auto j = 0; j < paintArea->getShapeListModel()->getShapeListConst().size(); j++)
			{
				settings.setArrayIndex(j);

				auto s = paintArea->getShapeListModel()->getShapeListConst().at(j);

				settings.setValue("Name", s->getName());
				settings.setValue("Type", static_cast<int>(s->getType()));

				switch (s->getType())
				{
				case PaintDrawModeType::Freehand:
				{
					auto brush = dynamic_cast<XrayPaintBrushShape*>(s);
					if (brush)
					{
						settings.setValue("Points", QVariant::fromValue(brush->getPoints()));
						settings.setValue("Pen", QVariant::fromValue(brush->getPen()));
						settings.setValue("Brush", QVariant::fromValue(brush->getBrush()));
						settings.setValue("Font", QVariant::fromValue(brush->getFont()));

						brush->setBlobSettings(settings, "XVoid");
					}
				}
				break;

				case PaintDrawModeType::Line:
				{
					auto line = dynamic_cast<XrayPaintLineShape*>(s);
					if (line)
					{
						settings.setValue("Line", line->getLine());
						settings.setValue("Pen", QVariant::fromValue(line->getPen()));
						settings.setValue("Brush", QVariant::fromValue(line->getBrush()));
						settings.setValue("Font", QVariant::fromValue(line->getFont()));

						line->setBlobSettings(settings, "XVoid");
					}
				}
				break;

				case PaintDrawModeType::Arrow:
				{
					auto arrow = dynamic_cast<XrayPaintArrowShape*>(s);
					if (arrow)
					{
						settings.setValue("ArrowLine", arrow->getLine());
						settings.setValue("ArrowSize", arrow->getArrowSize());
						settings.setValue("ArrowHead1", QVariant::fromValue(arrow->getArrowHeads().first));
						settings.setValue("ArrowHead2", QVariant::fromValue(arrow->getArrowHeads().second));
						settings.setValue("ArrowDir", arrow->getArrowDirection());

						settings.setValue("Pen", QVariant::fromValue(arrow->getPen()));
						settings.setValue("Brush", QVariant::fromValue(arrow->getBrush()));
						settings.setValue("Font", QVariant::fromValue(arrow->getFont()));

						arrow->setBlobSettings(settings, "XVoid");
					}
				}
				break;

				case PaintDrawModeType::Rectangle:
				{
					auto rectangle = dynamic_cast<XrayPaintRectangleShape*>(s);
					if (rectangle)
					{
						settings.setValue("Rect", rectangle->getRect());
						settings.setValue("Pen", QVariant::fromValue(rectangle->getPen()));
						settings.setValue("Brush", QVariant::fromValue(rectangle->getBrush()));
						settings.setValue("Font", QVariant::fromValue(rectangle->getFont()));

						rectangle->setBlobSettings(settings, "XVoid");
					}
				}
				break;

				case PaintDrawModeType::Circle:
				{
					auto circle = dynamic_cast<XrayPaintCircleShape*>(s);
					if (circle)
					{
						settings.setValue("Center", circle->getCenter());
						settings.setValue("Radius", circle->getRadius());
						settings.setValue("Segments", circle->getSegments());
						settings.setValue("Pen", QVariant::fromValue(circle->getPen()));
						settings.setValue("Brush", QVariant::fromValue(circle->getBrush()));
						settings.setValue("Font", QVariant::fromValue(circle->getFont()));

						circle->setBlobSettings(settings, "XVoid");
					}
				}
				break;

				case PaintDrawModeType::Ellipse:
				{
					auto ellipse = dynamic_cast<XrayPaintEllipseShape*>(s);
					if (ellipse)
					{
						settings.setValue("EllipseRect", ellipse->getRect());
						settings.setValue("Pen", QVariant::fromValue(ellipse->getPen()));
						settings.setValue("Brush", QVariant::fromValue(ellipse->getBrush()));
						settings.setValue("Font", QVariant::fromValue(ellipse->getFont()));

						ellipse->setBlobSettings(settings, "XVoid");
					}
				}
				break;

				case PaintDrawModeType::Polygon:
				{
					auto polygon = dynamic_cast<XrayPaintPolygonShape*>(s);
					if (polygon)
					{
						settings.setValue("PolyPoints", QVariant::fromValue(polygon->getPoints()));
						settings.setValue("Pen", QVariant::fromValue(polygon->getPen()));
						settings.setValue("Brush", QVariant::fromValue(polygon->getBrush()));
						settings.setValue("Font", QVariant::fromValue(polygon->getFont()));
						
						polygon->setBlobSettings(settings, "XVoid");
					}
				}
				break;

				case PaintDrawModeType::Text:
				{
					auto text = dynamic_cast<XrayPaintTextShape*>(s);
					if (text)
					{
						settings.setValue("Text", text->getText());
						settings.setValue("TextPoint", text->getPoint());
										   
						settings.setValue("Type", static_cast<int>(text->getType()));
						settings.setValue("Pen", QVariant::fromValue(text->getPen()));
						settings.setValue("Brush", QVariant::fromValue(text->getBrush()));
						settings.setValue("Font", QVariant::fromValue(text->getFont()));

						text->setBlobSettings(settings, "XVoid");
					}
				}
				break;

				default:
					break;
				}
			}

			settings.endArray();
		}
	}

	settings.endArray();
}
void XrayPaintMainWindow::readSettings(QSettings& settings, const QString& projectFileName)
{
	settings.beginGroup("XPaintPro");

	p_flashfilter->readSettings(settings);
	p_voidFilter->readSettings(settings);

	menu_recentProjFiles->restoreState(settings.value("RecentProjectFiles").toByteArray());
	menu_recentFiles->restoreState(settings.value("RecentFiles").toByteArray());
	XrayGlobal::setLastFileOpenPath(settings.value("CurrentDirectory", XrayGlobal::getLastFileOpenPath()).toString());

	auto dirName = settings.value("Images/DirName").toString();
	auto imageFiles = settings.value("Images/Captions").value<QVector<QPair<QString, QString> > >();
	if (!projectFileName.isEmpty())
	{
		for (auto& i : imageFiles)
		{
			auto fileName = i.first;
			i.first = QFileInfo(projectFileName).absolutePath() + '/' + dirName + '/' + QFileInfo(fileName).fileName();
			qDebug() << i;
		}
	}
	openFiles(imageFiles);
	//if (!imageFiles.empty())
	//	QTimer::singleShot(1000, [this]() { zoomFitToWidgetAllTabs(false); });	// wait until qt adjust the size of the widget after show().
	auto treeData = settings.value("Images/TreeDataItems").value<QVector<QPair<QString, QVector<QPair<QString, QString> > > > >();
	if (!treeData.empty())
	{
		projTreeView->treeView()->setTreeData(treeData);

		syncTreeModelWithTabs();
	}

	m_numImagesPerPage = settings.value("Images/PerPageImages", m_numImagesPerPage).toInt();

	// read and create shapes on the images. Must call after OpenFiles().
	readShapeSettings(settings);

	auto opacity = settings.value("Outline/Opacity", slider_stroke_opacity->value()).toInt();
	slider_stroke_opacity->setValue(opacity);
	auto color = settings.value("Outline/Color", QColor(slider_strokecolor_red->value(), slider_strokecolor_green->value(), slider_strokecolor_blue->value(), slider_stroke_opacity->value())).value<QColor>();
	slider_strokecolor_red->setValue(color.red());
	slider_strokecolor_green->setValue(color.green());
	slider_strokecolor_blue->setValue(color.blue());
	m_pen.setColor(color);
	button_strokeColor->setStyleSheet(QString("QPushButton { background-color: %1 } QToolTip {""} ").arg(color.name()));

	auto size = settings.value("Outline/Size", slider_stroke_width->value()).toInt();
	slider_stroke_width->setValue(size);
	m_pen.setWidth(size);

	auto roundCap = settings.value("Outline/RoundCap", radiobutton_roundcap->isChecked()).toBool();
	if(roundCap) radiobutton_roundcap->setChecked(true);
	else radiobutton_flatcap->setChecked(true);
	m_pen.setCapStyle(roundCap ? Qt::PenCapStyle::RoundCap : Qt::PenCapStyle::FlatCap);

	auto index = settings.value("Outline/Style", combobox_penstyle->currentIndex()).toInt();
	combobox_penstyle->setCurrentIndex(index);
	m_pen.setStyle(static_cast<Qt::PenStyle>(index));

	m_arrow_dir = settings.value("Outline/Arrow/Direction", m_arrow_dir).toInt();
	m_arrow_size = settings.value("Outline/Arrow/Size", m_arrow_size).toInt();

	opacity = settings.value("Fill/Opacity", slider_fill_opacity->value()).toInt();
	slider_fill_opacity->setValue(opacity);
	color = settings.value("Fill/Color", QColor(slider_fillcolor_red->value(), slider_fillcolor_green->value(), slider_fillcolor_blue->value(), slider_fill_opacity->value())).value<QColor>();
	slider_fillcolor_red->setValue(color.red());
	slider_fillcolor_green->setValue(color.green());
	slider_fillcolor_blue->setValue(color.blue());
	m_brush.setColor(color);
	button_fillColor->setStyleSheet(QString("QPushButton { background-color: %1 } QToolTip {""} ").arg(color.name()));

	index = settings.value("Fill/Pattern", combobox_brushstyle->currentIndex()).toInt();
	combobox_brushstyle->setCurrentIndex(index);
	m_brush.setStyle(static_cast<Qt::BrushStyle>(index));

	auto font = settings.value("Font/Font", fontCombo->currentIndex()).toInt();
	fontCombo->setCurrentIndex(font);
	auto font_size = settings.value("Font/Size", fontSizeCombo->currentText()).toString();
	fontSizeCombo->setCurrentText(font_size);
	auto b = settings.value("Font/Bold", boldAction->isChecked()).toBool();
	boldAction->setChecked(b);
	settings.value("Font/Italic", italicAction->isChecked()).toBool();
	italicAction->setChecked(b);
	settings.value("Font/Underline", underlineAction->isChecked()).toBool();
	underlineAction->setChecked(b);
	m_font = fontCombo->currentFont();
	m_font.setPointSize(font_size.toInt());
	m_font.setBold(boldAction->isChecked());
	m_font.setItalic(italicAction->isChecked());
	m_font.setUnderline(underlineAction->isChecked());

	settings.endGroup();
}
void XrayPaintMainWindow::readShapeSettings(QSettings& settings)
{
	auto paintAreas = settings.beginReadArray("Project/PaintAreas");

	for (auto i = 0; i < paintAreas; i++)
	{
		const auto scroll = documentTab->widget(i)->findChild<QScrollArea*>("tabScrollArea");
		const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (paintArea)
		{
			settings.setArrayIndex(i);

			auto key = QFileInfo(paintArea->getFileName()).fileName();

			paintArea->zoom(settings.value(key + "/ZoomFactor", getFitToViewportFactor(i)).toDouble());
			paintArea->setCrosshairEnabled(settings.value(key + "/ShowCrosshair", false).toBool());
			paintArea->setGridEnabled(settings.value(key + "/ShowGrid", false).toBool());
			scroll->horizontalScrollBar()->setValue(settings.value(key + "/ScrollPosX", scroll->horizontalScrollBar()->value()).toInt());
			scroll->verticalScrollBar()->setValue(settings.value(key + "/ScrollPosY", scroll->verticalScrollBar()->value()).toInt());

			paintArea->resetShapeListModel();	// first, clear the last shapes

			auto shapes = settings.beginReadArray("Shapes");

			for (int j = shapes - 1; j >= 0; j--)
			{
				settings.setArrayIndex(j);

				auto name = settings.value("Name").toString();
				auto type = static_cast<PaintDrawModeType>(settings.value("Type").toInt());

				switch (type)
				{
				case PaintDrawModeType::Freehand:
				{
					auto brush = new XrayPaintBrushShape();
					brush->setName(settings.value("Name").toString());
					auto p = settings.value("Points").value<QPolygonF>();
					if (p.empty())
						p = settings.value("Points").value<QVector<QPointF> >();	// for backward compatibility
					brush->setPoints(p);
					brush->setPen(settings.value("Pen").value<QPen>());
					brush->setBrush(settings.value("Brush").value<QBrush>());
					brush->setFont(settings.value("Font").value<QFont>());
					brush->getBlobSettings(settings, "XVoid");
					paintArea->addShape(brush);
				}
				break;

				case PaintDrawModeType::Line:
				{
					auto line = new XrayPaintLineShape();
					line->setName(settings.value("Name").toString());
					line->setLine(settings.value("Line").toLine());
					line->setPen(settings.value("Pen").value<QPen>());
					line->setBrush(settings.value("Brush").value<QBrush>());
					line->setFont(settings.value("Font").value<QFont>());
					line->getBlobSettings(settings, "XVoid");
					paintArea->addShape(line);
				}
				break;

				case PaintDrawModeType::Rectangle:
				{
					auto rectangle = new XrayPaintRectangleShape();
					rectangle->setName(settings.value("Name").toString());
					rectangle->setRect(settings.value("Rect").toRect());
					rectangle->setPen(settings.value("Pen").value<QPen>());
					rectangle->setBrush(settings.value("Brush").value<QBrush>());
					rectangle->setFont(settings.value("Font").value<QFont>());
					rectangle->getBlobSettings(settings, "XVoid");
					paintArea->addShape(rectangle);
				}
				break;

				case PaintDrawModeType::Circle:
				{
					auto circle = new XrayPaintCircleShape();
					circle->setName(settings.value("Name").toString());
					circle->setCenter(settings.value("Center").toPoint());
					circle->setRadius(settings.value("Radius").toDouble());
					circle->setSegments(settings.value("Segments").toInt());
					circle->setPen(settings.value("Pen").value<QPen>());
					circle->setBrush(settings.value("Brush").value<QBrush>());
					circle->setFont(settings.value("Font").value<QFont>());
					circle->getBlobSettings(settings, "XVoid");
					paintArea->addShape(circle);
				}
				break;

				case PaintDrawModeType::Ellipse:
				{
					auto ellipse = new XrayPaintEllipseShape();
					ellipse->setName(settings.value("Name").toString());
					ellipse->setRect(settings.value("EllipseRect").toRect());
					ellipse->setPen(settings.value("Pen").value<QPen>());
					ellipse->setBrush(settings.value("Brush").value<QBrush>());
					ellipse->setFont(settings.value("Font").value<QFont>());
					ellipse->getBlobSettings(settings, "XVoid");
					paintArea->addShape(ellipse);
				}
				break;

				case PaintDrawModeType::Polygon:
				{
					auto polygon = new XrayPaintPolygonShape();
					polygon->setName(settings.value("Name").toString());

					auto p = settings.value("PolyPoints").value<QPolygonF>();
					if (p.empty())
					{
						auto points = settings.value("PolyPoints").value<QVector<QPoint> >();	// for backward compatibility
						if (!points.empty())
						{
							p.resize(points.size());
							for (auto k = 0; k < points.size(); k++)
								p[k] = points[k];
						}
					}

					polygon->setPoints(p);
					polygon->setPen(settings.value("Pen").value<QPen>());
					polygon->setBrush(settings.value("Brush").value<QBrush>());
					polygon->setFont(settings.value("Font").value<QFont>());
					polygon->getBlobSettings(settings, "XVoid");
					paintArea->addShape(polygon);
				}
				break;

				case PaintDrawModeType::Arrow:
				{
					auto arrow = new XrayPaintArrowShape();
					arrow->setName(settings.value("Name").toString());
					arrow->setLine(settings.value("ArrowLine").toLine());
					arrow->setArrowSize(settings.value("ArrowSize").toInt());
					arrow->setArrowHeads(QPair<QPolygonF, QPolygonF>(settings.value("ArrowHead1").value<QPolygonF>(), settings.value("ArrowHead2").value<QPolygonF>()));
					arrow->setArrowDirection(settings.value("ArrowDir").toInt());
					arrow->setPen(settings.value("Pen").value<QPen>());
					arrow->setBrush(settings.value("Brush").value<QBrush>());
					arrow->setFont(settings.value("Font").value<QFont>());
					arrow->getBlobSettings(settings, "XVoid");
					paintArea->addShape(arrow);
				}
				break;

				case PaintDrawModeType::Text:
				{
					auto text = new XrayPaintTextShape();
					text->setName(settings.value("Name").toString());
					text->setText(settings.value("Text").toString());
					text->setPoint(settings.value("TextPoint").toPoint());
					text->setPen(settings.value("Pen").value<QPen>());
					text->setBrush(settings.value("Brush").value<QBrush>());
					text->setFont(settings.value("Font").value<QFont>());
					text->getBlobSettings(settings, "XVoid");
					paintArea->addShape(text);
				}
				break;

				default:
					break;
				}
			}

			settings.endArray();
		}
	}

	settings.endArray();
}
void XrayPaintMainWindow::initializeMenus()
{
	action_OpenProj = new QAction(QIcon(":/paint/images/new_picture_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Open Project..."), this);
	action_OpenProj->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Open an existing project."));
	action_OpenProj->setShortcut(Qt::CTRL + Qt::Key_O);
	connect(action_OpenProj, &QAction::triggered, this, &XrayPaintMainWindow::on_action_OpenProj_triggered);

	action_CloseAll = new QAction(QIcon(":/paint/images/close_win_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Close Project"), this);
	action_CloseAll->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Close the project with all pictures."));
	connect(action_CloseAll, &QAction::triggered, this, &XrayPaintMainWindow::on_action_closeAllFiles_triggered);

	action_SaveProj = new QAction(QIcon(":/paint/images/save_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Save Project"), this);
	action_SaveProj->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Save current settings to the last saved project."));
	action_SaveProj->setShortcut(Qt::CTRL + Qt::Key_S);
	connect(action_SaveProj, &QAction::triggered, this, &XrayPaintMainWindow::on_action_SaveProj_triggered);

	action_SaveProjAs = new QAction(QIcon(":/paint/images/saveas_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Save Project As..."), this);
	action_SaveProjAs->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Save current settings as a project."));
	connect(action_SaveProjAs, &QAction::triggered, this, &XrayPaintMainWindow::on_action_SaveProjAs_triggered);

	action_Open = new QAction(QIcon(":/paint/images/picture_dir_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Open Pictures..."), this);
	action_Open->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Open the existing file(s)."));
	action_Open->setShortcut(Qt::SHIFT + Qt::Key_O);
	connect(action_Open, &QAction::triggered, this, &XrayPaintMainWindow::on_action_openFile_triggered);
	
	action_OpenDir = new QAction(QIcon(":/paint/images/open_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Open Directory..."), this);
	action_OpenDir->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Open the existing file(s) from the directory."));
	action_OpenDir->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_O);
	connect(action_OpenDir, &QAction::triggered, this, &XrayPaintMainWindow::on_action_openDir_triggered);

	action_Reload = new QAction(QIcon(":/paint/images/restart_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Reload Current Picture"), this);
	action_Reload->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Reload the current picture from disk."));
	action_Reload->setShortcut(Qt::SHIFT + Qt::Key_R);
	connect(action_Reload, &QAction::triggered, this, &XrayPaintMainWindow::on_action_Reload_triggered);

	action_Close = new QAction(QIcon(":/paint/images/.png"), QApplication::translate("XrayPaintMainWindow", "Close Current Picture"), this);
	action_Close->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Close the current picture tab."));
	connect(action_Close, &QAction::triggered, this, &XrayPaintMainWindow::on_action_closeFile_triggered);

	action_Save = new QAction(QIcon(":/paint/images/.png"), QApplication::translate("XrayPaintMainWindow", "Save a Copy"), this);
	action_Save->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Save changes on the original file."));
	action_Save->setShortcut(Qt::SHIFT + Qt::Key_S);
	connect(action_Save, &QAction::triggered, this, &XrayPaintMainWindow::on_action_save_triggered);

	action_SaveAs = new QAction(QIcon(":/paint/images/.png"), QApplication::translate("XrayPaintMainWindow", "Save Picture As..."), this);
	action_SaveAs->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Save the selected file as another name or format."));
	connect(action_SaveAs, &QAction::triggered, this, &XrayPaintMainWindow::on_action_saveFileAs_triggered);
	
	action_Print = new QAction(QIcon(":/paint/images/print_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Print..."), this);
	action_Print->setShortcut(Qt::CTRL + Qt::Key_P);
	connect(action_Print, &QAction::triggered, this, &XrayPaintMainWindow::on_action_print_triggered);
	
	//action_Exit = new QAction(QIcon(":/paint/images/exit_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Exit"), this);
	//action_Exit->setShortcut(Qt::ALT + Qt::Key_F4);
	//connect(action_Exit, &QAction::triggered, this, &XrayPaintMainWindow::close);

	menu_File = menuBar()->addMenu(QApplication::translate("XrayPaintMainWindow", "File"));

	menu_FileOpen = menu_File->addMenu(QApplication::translate("XrayPaintMainWindow", "Open"));
	menu_FileOpen->addAction(action_OpenProj);
	menu_FileOpen->addSeparator();
	menu_FileOpen->addAction(action_Open);
	menu_FileOpen->addAction(action_OpenDir);
	menu_File->addSeparator();
	menu_File->addAction(action_CloseAll);
	menu_File->addSeparator();
	menu_File->addAction(action_SaveProj);
	menu_File->addAction(action_SaveProjAs);
	menu_File->addSeparator();
	menu_File->addAction(action_Save);
	menu_File->addAction(action_SaveAs);
	menu_File->addSeparator();
	menu_File->addAction(action_Reload);
	menu_File->addSeparator();
	menu_File->addAction(action_Close);
	menu_File->addSeparator();
	menu_File->addAction(action_Print);
	menu_File->addSeparator();

	menu_recentProjFiles = new XrayQRecentFilesMenu(QApplication::translate("XrayPaintMainWindow", "Recent Project Files"), menu_File);
	menu_recentProjFiles->setMaxCount(9);
	connect(menu_recentProjFiles, &XrayQRecentFilesMenu::recentFileTriggered, this, &XrayPaintMainWindow::openProject);
	menu_File->addMenu(menu_recentProjFiles);

	menu_recentFiles = new XrayQRecentFilesMenu(QApplication::translate("XrayPaintMainWindow", "Recent Picture Files"), menu_File);
	menu_recentFiles->setMaxCount(9);
	connect(menu_recentFiles, &XrayQRecentFilesMenu::recentFileTriggered, this, &XrayPaintMainWindow::openFile);
	menu_File->addMenu(menu_recentFiles);

	action_Undo = new QAction(QIcon(":/paint/images/undo_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Undo"), this);
	action_Undo->setShortcut(Qt::CTRL + Qt::Key_Z);
	connect(action_Undo, &QAction::triggered, this, &XrayPaintMainWindow::on_action_Undo_triggered);
	
	action_Redo = new QAction(QIcon(":/paint/images/redo_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Redo"), this);
	action_Redo->setShortcut(Qt::CTRL + Qt::Key_Y);
	connect(action_Redo, &QAction::triggered, this, &XrayPaintMainWindow::on_action_Redo_triggered);
	
	action_Copy = new QAction(QIcon(":/paint/images/copy_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Copy"), this);
	action_Copy->setShortcut(Qt::CTRL + Qt::Key_C);
	connect(action_Copy, &QAction::triggered, this, &XrayPaintMainWindow::on_action_copy_triggered);
	
	action_Paste = new QAction(QIcon(":/paint/images/paste_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Paste"), this);
	action_Paste->setShortcut(Qt::CTRL + Qt::Key_V);
	connect(action_Paste, &QAction::triggered, this, &XrayPaintMainWindow::on_action_paste_triggered);
	
	action_Erase = new QAction(QIcon(":/paint/images/erase_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Delete All Shapes"), this);
	action_Erase->setShortcut(Qt::CTRL + Qt::Key_Delete);
	connect(action_Erase, &QAction::triggered, this, &XrayPaintMainWindow::on_action_deleteAllShapes_triggered);
	
	action_Delete = new QAction(QIcon(":/paint/images/delete_icon.png"), QApplication::translate("XrayPaintMainWindow", "Delete Selected Shape"), this);
	action_Delete->setShortcut(Qt::Key_Delete);
	
	auto action_Rotate180 = new QAction(QIcon(""), QString::fromLatin1("180�"), this);
	connect(action_Rotate180, &QAction::triggered, [this]() { auto area = getCurrentPaintArea();	if (area) area->rotate(180.0); });
	auto action_Rotate90 = new QAction(QIcon(""), QString::fromLatin1("90�") + ' ' + QApplication::translate("XrayPaintMainWindow", "Clockwise"), this);
	action_Rotate90->setShortcut(Qt::CTRL + '.');
	connect(action_Rotate90, &QAction::triggered, [this]() { auto area = getCurrentPaintArea();	if (area) area->rotate(270.0); });
	auto action_RotateN90 = new QAction(QIcon(""), QString::fromLatin1("90�") + ' ' + QApplication::translate("XrayPaintMainWindow", "Counter clockwise"), this);
	action_RotateN90->setShortcut(Qt::CTRL + ',');
	connect(action_RotateN90, &QAction::triggered, [this]() { auto area = getCurrentPaintArea(); if (area) area->rotate(90.0); });
	auto action_arbitrary = new QAction(QIcon(""), QApplication::translate("XrayPaintMainWindow", "Arbitrary..."), this);
	connect(action_arbitrary, &QAction::triggered, [this]()
	{ 
		auto area = getCurrentPaintArea(); 
		if (area)
		{
			bool ok;
			auto n = QInputDialog::getDouble(this, QApplication::translate("XrayPaintMainWindow", "Specify the rotation angle"),
				QApplication::translate("XrayPaintMainWindow", "Angle (deg)"), 90.0, -180.0, 180.0, 2, &ok);
			if (ok) area->rotate(n); 
		}
	});

	auto action_FlipHor = new QAction(QIcon(""), QApplication::translate("XrayPaintMainWindow", "Horizontal"), this);
	connect(action_FlipHor, &QAction::triggered, [this]() { auto area = getCurrentPaintArea(); if (area) area->flip(0); });
	auto action_FlipVert = new QAction(QIcon(""), QApplication::translate("XrayPaintMainWindow", "Vertical"), this);
	connect(action_FlipVert, &QAction::triggered, [this]() { auto area = getCurrentPaintArea(); if (area) area->flip(1); });
	auto action_FlipBoth = new QAction(QIcon(""), QApplication::translate("XrayPaintMainWindow", "Both"), this);
	connect(action_FlipBoth, &QAction::triggered, [this]() { auto area = getCurrentPaintArea(); if (area) area->flip(-1); });

	auto action_Invert = new QAction(QIcon(""), QApplication::translate("XrayPaintMainWindow", "Invert"), this);
	action_Invert->setShortcut(Qt::ALT + 'i');
	connect(action_Invert, &QAction::triggered, [this]() 
	{ 
		auto area = getCurrentPaintArea(); 
		if (area)
		{
			area->getImage().invertPixels(QImage::InvertRgba);
			area->setCurrentImageAsOriginalImage();
			area->update();
		}
	});

	auto action_crop = new QAction(QIcon(":/paint/images/crop_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Crop"), this);
	action_crop->setShortcut(Qt::CTRL + Qt::SHIFT + 'x');
	action_crop->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Crop the picture so it only contains the current selection."));
	connect(action_crop, &QAction::triggered, [this]() { auto area = getCurrentPaintArea(); if (area) area->crop(); });

	//auto action_autoBrightEnhance = new QAction(QIcon(":/paint/images/.png"), QApplication::translate("XrayPaintMainWindow", "Auto Adjustment"), this);
	//action_autoBrightEnhance->setShortcut(Qt::CTRL + Qt::SHIFT + 'u');
	//action_autoBrightEnhance->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Perform automatic brightness and contrast optimization."));
	//connect(action_autoBrightEnhance, &QAction::triggered, [this]() 
	//{
	//	auto area = getCurrentPaintArea(); 
	//	if (area)
	//	{
	//		auto src = XrayCVUtil::toMat(area->getImage(), true);
	//		cv::Mat m(src.size(), src.type());
	//		R3D::fvkContrastEnhancerUtil::autoBrightnessAndContrast(src, m, 20);
	//		area->setImage(m);
	//		area->update();
	//	}
	//});

	action_ResizeImage = new QAction(QIcon(":/paint/images/resize_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Resize..."), this);
	action_ResizeImage->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Resize the picture."));
	connect(action_ResizeImage, &QAction::triggered, this, &XrayPaintMainWindow::on_action_resizeImage_triggered);
	
	menu_Edit = menuBar()->addMenu(QApplication::translate("XrayPaintMainWindow", "Edit"));
	menu_Edit->addAction(action_Undo);
	menu_Edit->addAction(action_Redo);
	menu_Edit->addSeparator();
	menu_Edit->addAction(action_Copy);
	menu_Edit->addAction(action_Paste);
	menu_Edit->addAction(action_Erase);
	menu_Edit->addAction(action_Delete);
	menu_Edit->addSeparator();
	auto menu_Rotate = menu_Edit->addMenu(QApplication::translate("XrayPaintMainWindow", "Rotate"));
	menu_Rotate->addAction(action_Rotate180);
	menu_Rotate->addAction(action_Rotate90);
	menu_Rotate->addAction(action_RotateN90);
	menu_Rotate->addSeparator();
	menu_Rotate->addAction(action_arbitrary);
	auto menu_Flip = menu_Edit->addMenu(QApplication::translate("XrayPaintMainWindow", "Flip"));
	menu_Flip->addAction(action_FlipHor);
	menu_Flip->addAction(action_FlipVert);
	menu_Flip->addAction(action_FlipBoth);
	menu_Edit->addSeparator();
	menu_Edit->addAction(action_Invert);
	//menu_Edit->addAction(action_autoBrightEnhance);
	menu_Edit->addSeparator();
	menu_Edit->addAction(action_crop);
	menu_Edit->addAction(action_ResizeImage);

	menu_View = menuBar()->addMenu(QApplication::translate("XrayPaintMainWindow", "View"));

	action_ToggleAntiAliasing = new QAction(QApplication::translate("XrayPaintMainWindow", "Smooth Edges"), this);
	action_ToggleAntiAliasing->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Apply anti-aliasing to smooth edges."));
	action_ToggleAntiAliasing->setCheckable(true);
	action_ToggleAntiAliasing->setChecked(true);
	connect(action_ToggleAntiAliasing, &QAction::triggered, this, &XrayPaintMainWindow::on_action_ToggleAntiAliasing_triggered);
	//menu_View->addAction(action_ToggleAntiAliasing);

	action_zoomIn = new QAction(QIcon(":/paint/images/zoom_in_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Zoom in"), this);
	action_zoomIn->setToolTip(QApplication::translate("XrayPaintMainWindow", "Zoom in on the current picture. [Ctrl+PgUp]"));
	action_zoomIn->setStatusTip(action_zoomIn->toolTip());
	action_zoomIn->setShortcut(Qt::CTRL + Qt::Key_PageUp);
	connect(action_zoomIn, &QAction::triggered, [this]() { zoom(0.125); });
	menu_View->addAction(action_zoomIn);

	action_zoomOut = new QAction(QIcon(":/paint/images/zoom_out_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Zoom out"), this);
	action_zoomOut->setToolTip(QApplication::translate("XrayPaintMainWindow", "Zoom out on the current picture. [Ctrl+PgDn]"));
	action_zoomOut->setStatusTip(action_zoomOut->toolTip());
	action_zoomOut->setShortcut(Qt::CTRL + Qt::Key_PageDown);
	connect(action_zoomOut, &QAction::triggered, [this]() { zoom(-0.125); });
	menu_View->addAction(action_zoomOut);

	action_zoomOriginal = new QAction(QIcon(":/paint/images/zoom_100_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Fit to screen"), this);
	action_zoomOriginal->setToolTip(QApplication::translate("XrayPaintMainWindow", "Fit to screen. [Ctrl+0]"));
	action_zoomOriginal->setStatusTip(action_zoomOriginal->toolTip());
	action_zoomOriginal->setShortcut(Qt::CTRL + Qt::Key_0);
	connect(action_zoomOriginal, &QAction::triggered, this, &XrayPaintMainWindow::fitToViewportFirstView);
	menu_View->addAction(action_zoomOriginal);

	action_zoomAllOriginal = new QAction(QIcon(":/paint/images/zoomall_100_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Fit all to screen"), this);
	action_zoomAllOriginal->setToolTip(QApplication::translate("XrayPaintMainWindow", "Fit all to screen. [Ctrl+Shift+0]"));
	action_zoomAllOriginal->setStatusTip(action_zoomOriginal->toolTip());
	action_zoomAllOriginal->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_0);
	connect(action_zoomAllOriginal, &QAction::triggered, this, &XrayPaintMainWindow::fitToViewportAllViews);
	menu_View->addAction(action_zoomAllOriginal);
	menu_View->addSeparator();

	auto menu_Background = menu_View->addMenu(QIcon(":/paint/images/opacity_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Background"));

	action_BackgroundTransparent = new QAction(QIcon(":/paint/images/paint_bk_transp_small.png"), QApplication::translate("XrayPaintMainWindow", "Transparent"), this);
	action_BackgroundTransparent->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Set transparent background."));
	connect(action_BackgroundTransparent, &QAction::triggered, [this]() 
	{ 
		auto area = getCurrentPaintArea(); 
		if (area) 
		{ 
			if (!area->getFileName().isEmpty()) // do not change if there is already an image in the paint area
				return; 
			area->setBackgroundColor(QColor(255, 255, 255, 0)); 
			area->setBackground(":/paint/images/paint_bk_transp_small.png"); 
		} 
	});
	menu_Background->addAction(action_BackgroundTransparent);

	action_BackgroundWhite = new QAction(QIcon(":/paint/images/paint_background4.png"), QApplication::translate("XrayPaintMainWindow", "White"), this);
	action_BackgroundWhite->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Set white background."));
	connect(action_BackgroundWhite, &QAction::triggered, [this]() 
	{
		auto area = getCurrentPaintArea(); 
		if (area) 
		{
			if (!area->getFileName().isEmpty()) // do not change if there is already an image in the paint area
				return;
			area->setBackgroundColor(QColor(255, 255, 255, 255));
			area->setBackground(":/paint/images/paint_background4.png"); 
		} 
	});
	menu_Background->addAction(action_BackgroundWhite);

	action_BackgroundGrid = new QAction(QIcon(":/paint/images/paint_background2.png"), QApplication::translate("XrayPaintMainWindow", "Grid"), this);
	action_BackgroundGrid->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Set grid as background."));
	connect(action_BackgroundGrid, &QAction::triggered, [this]() { auto area = getCurrentPaintArea(); if (area) area->setBackground(":/paint/images/paint_background2.png"); });
	//menu_Background->addAction(action_BackgroundGrid);
	menu_View->addSeparator();

	action_Crosshair = new QAction(QIcon(""), QApplication::translate("XrayPaintMainWindow", "Crosshair"), this);
	action_Crosshair->setShortcut(Qt::ALT + 'h');
	action_Crosshair->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Toggle crosshair in the paint area."));
	action_Crosshair->setCheckable(false);
	connect(action_Crosshair, &QAction::triggered, [this](bool) { auto area = getCurrentPaintArea(); if (area) area->toggleCrosshair(); });
	menu_View->addAction(action_Crosshair);

	action_Grid = new QAction(QIcon(""), QApplication::translate("XrayPaintMainWindow", "Grid"), this);
	action_Grid->setShortcut(Qt::ALT + 'g');
	action_Grid->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Toggle grid in the paint area."));
	action_Grid->setCheckable(false);
	connect(action_Grid, &QAction::triggered, [this](bool) { auto area = getCurrentPaintArea(); if (area) area->toggleGrid(); });
	menu_View->addAction(action_Grid);

	//action_GridSize = new QAction(QIcon(""), QApplication::translate("XrayPaintMainWindow", "Custom Grids"), this);
	//action_GridSize->setStatusTip(QApplication::translate("XrayPaintMainWindow", "Adjust custom grids"));
	//connect(action_GridSize, &QAction::triggered, [this](bool) 
	//{ 
	//	auto area = getCurrentPaintArea(); 
	//	if (area)
	//	{
	//		bool ok;
	//		auto n = QInputDialog::getInt(this, QApplication::translate("XrayPaintMainWindow", "Specify grid size"),
	//			QApplication::translate("XrayPaintMainWindow", "Size"), area->getGridSize(), 4, 999, 1, &ok);
	//		if (ok)
	//		{
	//			area->setGridSize(n);
	//			area->update();

	//		}
	//	}
	//});
	//menu_View->addAction(action_GridSize);

	action_About = new QAction(QApplication::translate("XrayPaintMainWindow", "About"), this);
	connect(action_About, &QAction::triggered, this, &XrayPaintMainWindow::on_action_About_triggered);
	//menu_Help = menuBar()->addMenu(QApplication::translate("XrayPaintMainWindow", "Help"));
	//menu_Help->addAction(action_About);

	fileToolBar = addToolBar(QApplication::translate("XrayPaintMainWindow", "File Toolbar"));
	fileToolBar->setIconSize(QSize(18, 18));
	fileToolBar->addAction(action_OpenProj);
	fileToolBar->addAction(action_SaveProj);
	fileToolBar->addSeparator();
	fileToolBar->addAction(action_Open);
	fileToolBar->addAction(action_OpenDir);
	fileToolBar->addAction(action_Reload);
}
void XrayPaintMainWindow::initializeToolBar()
{
	// shapes toolbar
	shapesToolBar = new QToolBar(QApplication::translate("XrayPaintMainWindow", "Shapes"), this);
	
	actiongroup_Tools = new QActionGroup(shapesToolBar);
	connect(actiongroup_Tools, &QActionGroup::triggered, this, &XrayPaintMainWindow::on_actiongroup_Tools_triggered);
	
	tool_Select = new QAction(QIcon(":/paint/images/hand_cursor_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Click here for selection shapes. [Key 1]"), actiongroup_Tools);
	tool_Select->setStatusTip(tool_Select->text());
	tool_Select->setObjectName("selectTool");
	tool_Select->setShortcut(Qt::Key_1);
	tool_Select->setCheckable(true);
	tool_Select->setChecked(true);
	tool_Select->setData(0);

	tool_Freehand = new QAction(QIcon(":/paint/images/brush_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Insert curve into the picture. [Key 2]"), actiongroup_Tools);
	tool_Freehand->setStatusTip(tool_Freehand->text());
	tool_Freehand->setShortcut(Qt::Key_2);
	tool_Freehand->setCheckable(true);
	tool_Freehand->setData(1);

	tool_Line = new QAction(QIcon(":/paint/images/line_color_icon.png"), QApplication::translate("XrayPaintMainWindow", "Insert line into the picture. [Key 3]"), actiongroup_Tools);
	tool_Line->setStatusTip(tool_Line->text());
	tool_Line->setShortcut(Qt::Key_3);
	tool_Line->setCheckable(true);
	tool_Line->setData(2);

	tool_Arrow = new QAction(QIcon(":/paint/images/down_arrow_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Insert arrow into the picture. [Key 4]"), actiongroup_Tools);
	tool_Arrow->setStatusTip(tool_Arrow->text());
	tool_Arrow->setShortcut(Qt::Key_4);
	tool_Arrow->setCheckable(true);
	tool_Arrow->setData(3);

	tool_Rectangle = new QAction(QIcon(":/paint/images/rectangle_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Insert rectangle into the picture. [Key 5]"), actiongroup_Tools);
	tool_Rectangle->setStatusTip(tool_Rectangle->text());
	tool_Rectangle->setShortcut(Qt::Key_5);
	tool_Rectangle->setCheckable(true);
	tool_Rectangle->setData(4);

	tool_Circle = new QAction(QIcon(":/paint/images/circle_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Insert circle into the picture. [Key 6]"), actiongroup_Tools);
	tool_Circle->setStatusTip(tool_Circle->text());
	tool_Circle->setShortcut(Qt::Key_6);
	tool_Circle->setCheckable(true);
	tool_Circle->setData(5);

	tool_Ellipse = new QAction(QIcon(":/paint/images/oval_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Insert oval into the picture. [Key 7]"), actiongroup_Tools);
	tool_Ellipse->setStatusTip(tool_Ellipse->text());
	tool_Ellipse->setShortcut(Qt::Key_7);
	tool_Ellipse->setCheckable(true);
	tool_Ellipse->setData(6);

	tool_Polygon = new QAction(QIcon(":/paint/images/polygon_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Insert polygon into the picture. [Key 8]\n(Right mouse click to close it!)"), actiongroup_Tools);
	tool_Polygon->setStatusTip(tool_Polygon->text());
	tool_Polygon->setShortcut(Qt::Key_8);
	tool_Polygon->setCheckable(true);
	tool_Polygon->setData(7);

	tool_Text = new QAction(QIcon(":/paint/images/text_box_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Insert text into the picture. [Key 9]"), actiongroup_Tools);
	tool_Text->setStatusTip(tool_Text->text());
	tool_Text->setShortcut(Qt::Key_9);
	tool_Text->setCheckable(true);
	tool_Text->setData(8);

	tool_CustomCurve = new QAction(QIcon(":/paint/images/custom_shape_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Insert void into the selected shape with curve selection. [Key 0]"), actiongroup_Tools);
	tool_CustomCurve->setStatusTip(tool_CustomCurve->text());
	tool_CustomCurve->setShortcut(Qt::Key_0);
	tool_CustomCurve->setCheckable(true);
	tool_CustomCurve->setData(9);

	tool_CustomPolygon = new QAction(QIcon(":/paint/images/custom_shape_pentagon_icon.png"), QApplication::translate("XrayPaintMainWindow", "Insert void into the selected shape with polygon selection. [Key -]\n(Right mouse click to close it!)"), actiongroup_Tools);
	tool_CustomPolygon->setStatusTip(tool_CustomPolygon->text());
	tool_CustomPolygon->setShortcut('-');
	tool_CustomPolygon->setCheckable(true);
	tool_CustomPolygon->setData(10);

	tool_CustomCircle = new QAction(QIcon(":/paint/images/custom_shape_circle_icon.png"), QApplication::translate("XrayPaintMainWindow", "Insert void into the selected shape with circular selection. [Key =]"), actiongroup_Tools);
	tool_CustomCircle->setStatusTip(tool_CustomCircle->text());
	tool_CustomCircle->setShortcut('=');
	tool_CustomCircle->setCheckable(true);
	tool_CustomCircle->setData(11);

	action_EditText = new QAction(QIcon(":/paint/images/font_icon.png"), QApplication::translate("XrayPaintMainWindow", "Edit Text"), this);
	action_EditText->setShortcut(Qt::SHIFT + Qt::Key_T);
	action_EditText->setToolTip(QApplication::translate("XrayPaintMainWindow", "Open a dialog to edit the text of the selected text-box. [Shift+T]"));
	action_EditText->setStatusTip(action_EditText->toolTip());
	connect(action_EditText, SIGNAL(triggered()), this, SLOT(updateTextShapeText()));

	shapesToolBar->addAction(tool_Select);
	shapesToolBar->insertSeparator(tool_Freehand);
	shapesToolBar->addAction(tool_Freehand);
	shapesToolBar->addAction(tool_Line);
	shapesToolBar->addAction(tool_Arrow);
	shapesToolBar->insertSeparator(tool_Rectangle);
	shapesToolBar->addAction(tool_Rectangle);
	shapesToolBar->addAction(tool_Circle);
	shapesToolBar->addAction(tool_Ellipse);
	shapesToolBar->addAction(tool_Polygon);
	shapesToolBar->insertSeparator(tool_Text);
	shapesToolBar->addAction(tool_Text);
	shapesToolBar->insertSeparator(tool_CustomCurve);
	shapesToolBar->addAction(tool_CustomCurve);
	shapesToolBar->addAction(tool_CustomPolygon);
	shapesToolBar->addAction(tool_CustomCircle);
	shapesToolBar->insertSeparator(action_EditText);
	shapesToolBar->addAction(action_EditText);

	actiongroup_Tools->setExclusive(true);
	addToolBar(Qt::LeftToolBarArea, shapesToolBar);

	// shape settings toolbar
	fontCombo = new QFontComboBox();
	fontCombo->setToolTip(QApplication::translate("XrayPaintMainWindow", "Change the font family."));
	connect(fontCombo, SIGNAL(currentFontChanged(QFont)), this, SLOT(currentFontChanged(QFont)));

	fontSizeCombo = new QComboBox;
	fontSizeCombo->setEditable(true);
	for (auto i = 8; i < 64; i = i + 2)
		fontSizeCombo->addItem(QString().setNum(i));
	auto validator = new QIntValidator(2, 64, this);
	fontSizeCombo->setValidator(validator);
	fontSizeCombo->setCurrentText("10");
	fontSizeCombo->setToolTip(QApplication::translate("XrayPaintMainWindow", "Change the font size."));
	connect(fontSizeCombo, SIGNAL(currentIndexChanged(QString)), this, SLOT(fontSizeChanged(QString)));

	boldAction = new QAction(QApplication::translate("XrayPaintMainWindow", "Bold"), this);
	boldAction->setCheckable(true);
	QPixmap pixmap(":/paint/images/bold_icon.png");
	boldAction->setIcon(QIcon(pixmap));
	boldAction->setShortcut(Qt::CTRL + Qt::Key_B);
	boldAction->setToolTip(QApplication::translate("XrayPaintMainWindow", "Change to the heavier font."));
	connect(boldAction, SIGNAL(triggered()), this, SLOT(handleFontChange()));

	italicAction = new QAction(QIcon(":/paint/images/italic_icon.png"), QApplication::translate("XrayPaintMainWindow", "Italic"), this);
	italicAction->setCheckable(true);
	italicAction->setShortcut(Qt::CTRL + Qt::Key_I);
	italicAction->setToolTip(QApplication::translate("XrayPaintMainWindow", "Change to an italic font."));
	connect(italicAction, SIGNAL(triggered()), this, SLOT(handleFontChange()));

	underlineAction = new QAction(QIcon(":/paint/images/underline_icon.png"), QApplication::translate("XrayPaintMainWindow", "Underline"), this);
	underlineAction->setCheckable(true);
	underlineAction->setShortcut(Qt::CTRL + Qt::Key_U);
	underlineAction->setToolTip(QApplication::translate("XrayPaintMainWindow", "Draw a line below the text."));
	connect(underlineAction, SIGNAL(triggered()), this, SLOT(handleFontChange()));

	textToolBar = addToolBar(QApplication::translate("XrayPaintMainWindow", "Font"));
	textToolBar->setIconSize(QSize(18, 18));
	textToolBar->addWidget(fontCombo);
	textToolBar->addWidget(fontSizeCombo);
	textToolBar->addAction(boldAction);
	textToolBar->addAction(italicAction);
	textToolBar->addAction(underlineAction);

	// color toolbar
	fillColorToolButton = new QToolButton;
	fillColorToolButton->setPopupMode(QToolButton::MenuButtonPopup);
	fillColorToolButton->setMenu(createColorMenu(SLOT(itemColorChanged()), Qt::white));
	fillColorAction = fillColorToolButton->menu()->defaultAction();
	fillColorToolButton->setIcon(createColorToolButtonIcon(":/paint/images/fill_color_icon.png", Qt::white));
	fillColorToolButton->setToolTip(QApplication::translate("XrayPaintMainWindow", "Select the medium for the shape fill."));
	fillColorToolButton->setStatusTip(fillColorToolButton->toolTip());
	connect(fillColorToolButton, SIGNAL(clicked()), this, SLOT(fillToolButtonTriggered()));

	lineColorToolButton = new QToolButton;
	lineColorToolButton->setPopupMode(QToolButton::MenuButtonPopup);
	lineColorToolButton->setMenu(createColorMenu(SLOT(lineColorChanged()), Qt::black));
	lineColorAction = lineColorToolButton->menu()->defaultAction();
	lineColorToolButton->setIcon(createColorToolButtonIcon(":/paint/images/line_color_icon.png", Qt::black));
	lineColorToolButton->setToolTip(QApplication::translate("XrayPaintMainWindow", "Select the medium for the shape outline/text."));
	lineColorToolButton->setStatusTip(lineColorToolButton->toolTip());
	connect(lineColorToolButton, SIGNAL(clicked()), this, SLOT(lineToolButtonTriggered()));

	combobox_penstyle = new QComboBox(this);
	combobox_penstyle->setToolTip(QApplication::translate("XrayPaintMainWindow", "Select the outline style."));
	combobox_penstyle->setStatusTip(combobox_penstyle->toolTip());
	combobox_penstyle->insertItem(0, QIcon(":/paint/images/line_empty_icon.png"), QApplication::translate("XrayPaintMainWindow", "Empty"));
	combobox_penstyle->insertItem(1, QIcon(":/paint/images/line_solid_icon.png"), QApplication::translate("XrayPaintMainWindow", "Solid"));
	combobox_penstyle->insertItem(2, QIcon(":/paint/images/line_dash_icon.png"), QApplication::translate("XrayPaintMainWindow", "Dash"));
	combobox_penstyle->insertItem(3, QIcon(":/paint/images/line_dot_icon.png"), QApplication::translate("XrayPaintMainWindow", "Dot"));
	combobox_penstyle->insertItem(4, QIcon(":/paint/images/line_dashdot_icon.png"), QApplication::translate("XrayPaintMainWindow", "Dash dot"));
	combobox_penstyle->insertItem(5, QIcon(":/paint/images/line_dashdotdot_icon.png"), QApplication::translate("XrayPaintMainWindow", "Dash dot dot"));
	combobox_penstyle->setCurrentIndex(1);
	connect(combobox_penstyle, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &XrayPaintMainWindow::on_combobox_penStyle_activated);

	combobox_brushstyle = new QComboBox(this);
	combobox_brushstyle->setToolTip(QApplication::translate("XrayPaintMainWindow", "Select the shape fill pattern."));
	combobox_brushstyle->setStatusTip(combobox_brushstyle->toolTip());
	combobox_brushstyle->insertItem(0, QApplication::translate("XrayPaintMainWindow", "Empty"));
	combobox_brushstyle->insertItem(1, QApplication::translate("XrayPaintMainWindow", "Solid"));
	combobox_brushstyle->insertItem(2, QApplication::translate("XrayPaintMainWindow", "Pattern #1"));
	combobox_brushstyle->insertItem(3, QApplication::translate("XrayPaintMainWindow", "Pattern #2"));
	combobox_brushstyle->insertItem(4, QApplication::translate("XrayPaintMainWindow", "Pattern #3"));
	combobox_brushstyle->insertItem(5, QApplication::translate("XrayPaintMainWindow", "Pattern #4"));
	combobox_brushstyle->insertItem(6, QApplication::translate("XrayPaintMainWindow", "Pattern #5"));
	combobox_brushstyle->insertItem(7, QApplication::translate("XrayPaintMainWindow", "Pattern #6"));
	combobox_brushstyle->insertItem(8, QApplication::translate("XrayPaintMainWindow", "Pattern #7"));
	combobox_brushstyle->insertItem(9, QApplication::translate("XrayPaintMainWindow", "Horizontal lines"));
	combobox_brushstyle->insertItem(10, QApplication::translate("XrayPaintMainWindow", "Vertical lines"));
	combobox_brushstyle->insertItem(11, QApplication::translate("XrayPaintMainWindow", "Grid"));
	combobox_brushstyle->insertItem(12, QApplication::translate("XrayPaintMainWindow", "Diagonal line #1"));
	combobox_brushstyle->insertItem(13, QApplication::translate("XrayPaintMainWindow", "Diagonal line #2"));
	combobox_brushstyle->insertItem(14, QApplication::translate("XrayPaintMainWindow", "Diagonal grid"));
	combobox_brushstyle->setCurrentIndex(0);
	connect(combobox_brushstyle, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &XrayPaintMainWindow::on_combobox_brushStyle_activated);

	combobox_arrowdir = new QComboBox(this);
	combobox_arrowdir->setToolTip(QApplication::translate("XrayPaintMainWindow", "Select the arrow direction."));
	combobox_arrowdir->setStatusTip(combobox_arrowdir->toolTip());
	combobox_arrowdir->insertItem(0, QIcon(":/paint/images/.png"), QApplication::translate("XrayPaintMainWindow", "Left arrow"));
	combobox_arrowdir->insertItem(1, QIcon(":/paint/images/.png"), QApplication::translate("XrayPaintMainWindow", "Right arrow"));
	combobox_arrowdir->insertItem(2, QIcon(":/paint/images/.png"), QApplication::translate("XrayPaintMainWindow", "Both arrows"));
	combobox_arrowdir->setCurrentIndex(1);
	connect(combobox_arrowdir, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &XrayPaintMainWindow::on_combobox_arrowDir_activated);

	colorToolBar = addToolBar(QApplication::translate("XrayPaintMainWindow", "Shape Color and Style"));
	colorToolBar->addWidget(fillColorToolButton);
	colorToolBar->addWidget(lineColorToolButton);
	colorToolBar->addSeparator();
	colorToolBar->addWidget(combobox_penstyle);
	colorToolBar->addWidget(combobox_brushstyle);
	colorToolBar->addWidget(combobox_arrowdir);

	// dialogs toolbar
	action_Caption = new QAction(QIcon(":/paint/images/caption_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Picture Caption"), this);
	action_Caption->setToolTip(QApplication::translate("XrayPaintMainWindow", "Open a dialog to edit the picture caption. [Alt+C]"));
	action_Caption->setStatusTip(action_Caption->toolTip());
	action_Caption->setShortcut(Qt::ALT + Qt::Key_C);
	connect(action_Caption, &QAction::triggered, this, &XrayPaintMainWindow::on_action_fileCaption_triggered);

	action_NumImages = new QAction(QIcon(":/paint/images/image_grid_blue_icon.png"), QApplication::translate("XrayPaintMainWindow", "Pictures Per Page..."), this);
	action_NumImages->setToolTip(QApplication::translate("XrayPaintMainWindow", "How many pictures you want in one page? Open a dialog to specify the number. [Alt+I]"));
	action_NumImages->setStatusTip(action_NumImages->toolTip());
	action_NumImages->setShortcut(Qt::ALT + Qt::Key_I);
	connect(action_NumImages, &QAction::triggered, this, &XrayPaintMainWindow::on_action_numImages_triggered);

	dialogsToolbar = addToolBar(QApplication::translate("XrayPaintMainWindow", "Picture Caption and Number"));
	dialogsToolbar->setIconSize(QSize(18, 18));
	dialogsToolbar->addAction(action_Caption);
	dialogsToolbar->addAction(action_NumImages);
	dialogsToolbar->setVisible(false);

	// zoom toolbar
	zoomCombo = new QComboBox(this);
	QStringList scales;
	scales << "Fit" << "12.5%" << "25%" << "50%" << "75%" << "100%" << "125%" << "150%" << "175%" << "200%" << "225%" << "250%" << "275%" << "300%";
	zoomCombo->addItems(scales);
	zoomCombo->setCurrentIndex(0);
	zoomCombo->setToolTip(QApplication::translate("XrayPaintMainWindow", "Zoom in/out on the current picture."));
	zoomCombo->setStatusTip(zoomCombo->toolTip());
	connect(zoomCombo, &QComboBox::currentTextChanged, this, &XrayPaintMainWindow::zoomWithPercent);
	 
	zoomToolbar = addToolBar(QApplication::translate("XrayPaintMainWindow", "Zoom"));
	zoomToolbar->setIconSize(QSize(18, 18));
	zoomToolbar->addAction(action_zoomIn);
	zoomToolbar->addAction(action_zoomOut);
	zoomToolbar->addAction(action_zoomOriginal);
	zoomToolbar->addWidget(zoomCombo);
}
void XrayPaintMainWindow::initializeStatusBar()
{
	auto vline = new QFrame(this);
	vline->setFrameShadow(QFrame::Plain);
	vline->setFrameShape(QFrame::VLine);
	vline->setLineWidth(1);
	vline->setStyleSheet("QFrame { color: rgb(65, 65, 65); }");

	auto vline2 = new QFrame(this);
	vline2->setFrameShadow(QFrame::Plain);
	vline2->setFrameShape(QFrame::VLine);
	vline2->setLineWidth(1);
	vline2->setStyleSheet("QFrame { color: rgb(65, 65, 65); }");

	label_mousecoordinates = new QLabel("0, 0px", this);
	label_selectedShape = new QLabel(QString(QApplication::translate("XrayPaintMainWindow", "Selected shape") + ":"), this);
	label_currentSurface = new QLabel(QString(QApplication::translate("XrayPaintMainWindow", "Area") + ": N/A"), this);
	label_totalSurface = new QLabel(QString(QApplication::translate("XrayPaintMainWindow", "Total area") + ": 0"), this);

	p_statusBar = new XrayStatusBar(this);
	p_statusBar->setSizeGripEnabled(false);
	p_statusBar->setDefaultMessage(QApplication::translate("XrayPaintMainWindow", "Ready"));
	p_statusBar->setContentsMargins(5, 0, 5, 0);

	p_statusBar->addPermanentWidget(label_mousecoordinates);
	p_statusBar->addPermanentWidget(vline);
	p_statusBar->addPermanentWidget(label_selectedShape);
	p_statusBar->addPermanentWidget(vline);
	p_statusBar->addPermanentWidget(label_currentSurface);
	p_statusBar->addPermanentWidget(vline2);
	p_statusBar->addPermanentWidget(label_totalSurface);

	setStatusBar(p_statusBar);
}
void XrayPaintMainWindow::setReportingToolBarVisible(bool _b)
{
	dialogsToolbar->setVisible(_b);
}
void XrayPaintMainWindow::initializeOptionsTab()
{
	colorMapper = new QSignalMapper(this);
	connect(colorMapper, static_cast<void (QSignalMapper::*)(const QString&)>(&QSignalMapper::mapped), this, &XrayPaintMainWindow::on_colorChanged);

	button_strokeColor = new QPushButton(this);
	button_strokeColor->setFixedSize(50, 25);
	button_strokeColor->setToolTip(QApplication::translate("XrayPaintMainWindow", "Select the medium for the shape outline/text."));
	button_strokeColor->setStyleSheet(QString("QPushButton { background-color: %1 } QToolTip {""} ").arg(m_pen.color().name()));
	connect(button_strokeColor, &QPushButton::clicked, colorMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	colorMapper->setMapping(button_strokeColor, "ButtonStroke");

	slider_strokecolor_red = new QSlider(Qt::Horizontal, this);
	slider_strokecolor_red->setMaximum(255);
	slider_strokecolor_red->setMinimum(0);
	slider_strokecolor_green = new QSlider(Qt::Horizontal, this);
	slider_strokecolor_green->setMaximum(255);
	slider_strokecolor_green->setMinimum(0);
	slider_strokecolor_blue = new QSlider(Qt::Horizontal, this);
	slider_strokecolor_blue->setMaximum(255);
	slider_strokecolor_blue->setMinimum(0);
	spinbox_strokecolor_red = new QSpinBox(this);
	spinbox_strokecolor_red->setMaximum(255);
	spinbox_strokecolor_red->setMinimum(0);
	spinbox_strokecolor_green = new QSpinBox(this);
	spinbox_strokecolor_green->setMaximum(255);
	spinbox_strokecolor_green->setMinimum(0);
	spinbox_strokecolor_blue = new QSpinBox(this);
	spinbox_strokecolor_blue->setMaximum(255);
	spinbox_strokecolor_blue->setMinimum(0);

	connect(slider_strokecolor_red, &QSlider::valueChanged, colorMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	connect(slider_strokecolor_green, &QSlider::valueChanged, colorMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	connect(slider_strokecolor_blue, &QSlider::valueChanged, colorMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	connect(slider_strokecolor_red, &QSlider::valueChanged, spinbox_strokecolor_red, &QSpinBox::setValue);
	connect(slider_strokecolor_green, &QSlider::valueChanged, spinbox_strokecolor_green, &QSpinBox::setValue);
	connect(slider_strokecolor_blue, &QSlider::valueChanged, spinbox_strokecolor_blue, &QSpinBox::setValue);
	connect(spinbox_strokecolor_red, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), slider_strokecolor_red, &QSlider::setValue);
	connect(spinbox_strokecolor_green, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), slider_strokecolor_green, &QSlider::setValue);
	connect(spinbox_strokecolor_blue, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), slider_strokecolor_blue, &QSlider::setValue);
	colorMapper->setMapping(slider_strokecolor_red, "StrokeRed");
	colorMapper->setMapping(slider_strokecolor_blue, "StrokeBlue");
	colorMapper->setMapping(slider_strokecolor_green, "StrokeGreen");

	slider_stroke_opacity = new QSlider(Qt::Horizontal, this);
	slider_stroke_opacity->setMinimum(0);
	slider_stroke_opacity->setMaximum(255);
	slider_stroke_opacity->setValue(255);
	slider_stroke_opacity->setToolTip(QApplication::translate("XrayPaintMainWindow", "Adjust the opacity for the shape outline/text."));
	spinbox_stroke_opacity = new QSpinBox(this);
	spinbox_stroke_opacity->setMinimum(0);
	spinbox_stroke_opacity->setMaximum(255);
	spinbox_stroke_opacity->setValue(255);
	spinbox_stroke_opacity->setToolTip(slider_stroke_opacity->toolTip());
	connect(slider_stroke_opacity, &QSlider::valueChanged, colorMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	connect(slider_stroke_opacity, &QSlider::valueChanged, spinbox_stroke_opacity, &QSpinBox::setValue);
	connect(spinbox_stroke_opacity, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), slider_stroke_opacity, &QSlider::setValue);
	colorMapper->setMapping(slider_stroke_opacity, "StrokeOpacity");

	slider_stroke_width = new QSlider(Qt::Horizontal, this);
	slider_stroke_width->setMinimum(1);
	slider_stroke_width->setMaximum(50);
	slider_stroke_width->setToolTip(QApplication::translate("XrayPaintMainWindow", "Adjust the width for the shape outline."));
	spinbox_stroke_width = new QSpinBox(this);
	spinbox_stroke_width->setMinimum(1);
	spinbox_stroke_width->setMaximum(50);
	spinbox_stroke_width->setToolTip(slider_stroke_width->toolTip());
	connect(slider_stroke_width, &QSlider::valueChanged, colorMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	connect(slider_stroke_width, &QSlider::valueChanged, spinbox_stroke_width, &QSpinBox::setValue);
	connect(spinbox_stroke_width, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), slider_stroke_width, &QSlider::setValue);
	colorMapper->setMapping(slider_stroke_width, "StrokeWidth");

	slider_arrow_width = new QSlider(Qt::Horizontal, this);
	slider_arrow_width->setMinimum(1);
	slider_arrow_width->setMaximum(99);
	slider_arrow_width->setValue(20);
	slider_arrow_width->setToolTip(QApplication::translate("XrayPaintMainWindow", "Adjust the size for the shape arrow(s)."));
	spinbox_arrow_width = new QSpinBox(this);
	spinbox_arrow_width->setMinimum(1);
	spinbox_arrow_width->setMaximum(99);
	spinbox_arrow_width->setValue(20);
	spinbox_arrow_width->setToolTip(slider_arrow_width->toolTip());
	connect(slider_arrow_width, &QSlider::valueChanged, this, &XrayPaintMainWindow::on_slider_arrowWidth_changed);
	connect(slider_arrow_width, &QSlider::valueChanged, spinbox_arrow_width, &QSpinBox::setValue);
	connect(spinbox_arrow_width, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), slider_arrow_width, &QSlider::setValue);

	radiobutton_roundcap = new QRadioButton(QApplication::translate("XrayPaintMainWindow", "Round Cap"), this);
	radiobutton_roundcap->setAutoExclusive(false);
	radiobutton_roundcap->setChecked(true);
	radiobutton_roundcap->setAutoExclusive(true);
	radiobutton_flatcap = new QRadioButton(QApplication::translate("XrayPaintMainWindow", "Flat Cap"), this);
	buttongroup_capstyle = new QButtonGroup(this);
	buttongroup_capstyle->addButton(radiobutton_flatcap);
	buttongroup_capstyle->addButton(radiobutton_roundcap);
	buttongroup_capstyle->setExclusive(true);
	connect(buttongroup_capstyle, static_cast<void (QButtonGroup::*)(QAbstractButton*)>(&QButtonGroup::buttonClicked), this, &XrayPaintMainWindow::on_buttonGroup_capType_buttonClicked);

	// Tabs
	optionsTab = new QTabWidget(this);
	optionsTab->setDocumentMode(false);
	optionsTab->setStyleSheet("QTabBar::tab { min-width: 60px; }");	// controls the size of the tab bar.

	// Tab: Outline
	auto colorGridLayout = new QGridLayout;
	colorGridLayout->addWidget(new QLabel("R"), 0, 0);
	colorGridLayout->addWidget(slider_strokecolor_red, 0, 1);
	colorGridLayout->addWidget(spinbox_strokecolor_red, 0, 2);
	colorGridLayout->addWidget(new QLabel("G"), 1, 0);
	colorGridLayout->addWidget(slider_strokecolor_green, 1, 1);
	colorGridLayout->addWidget(spinbox_strokecolor_green, 1, 2);
	colorGridLayout->addWidget(new QLabel("B"), 2, 0);
	colorGridLayout->addWidget(slider_strokecolor_blue, 2, 1);
	colorGridLayout->addWidget(spinbox_strokecolor_blue, 2, 2);
	colorGridLayout->addWidget(new QLabel("A"), 3, 0);
	colorGridLayout->addWidget(slider_stroke_opacity, 3, 1);
	colorGridLayout->addWidget(spinbox_stroke_opacity, 3, 2);

	auto colorGroupLayout = new QGridLayout;
	colorGroupLayout->addWidget(button_strokeColor, 0, 0, Qt::AlignCenter);
	colorGroupLayout->addLayout(colorGridLayout, 1, 0);

	auto colorGroupBox = new QGroupBox(QApplication::translate("XrayPaintMainWindow", "Outline color"), this);
	colorGroupBox->setLayout(colorGroupLayout);

	auto strokeGridLayout = new QGridLayout;
	strokeGridLayout->addWidget(new QLabel("W"), 0, 0);
	strokeGridLayout->addWidget(slider_stroke_width, 0, 1);
	strokeGridLayout->addWidget(spinbox_stroke_width, 0, 2);
	strokeGridLayout->addWidget(new QLabel("S"), 1, 0);
	strokeGridLayout->addWidget(slider_arrow_width, 1, 1);
	strokeGridLayout->addWidget(spinbox_arrow_width, 1, 2);
	strokeGridLayout->addWidget(radiobutton_roundcap, 2, 1);
	strokeGridLayout->addWidget(radiobutton_flatcap, 3, 1);
	auto lineGroupBox = new QGroupBox(QApplication::translate("XrayPaintMainWindow", "Outline settings"), this);
	lineGroupBox->setLayout(strokeGridLayout);

	// Tab: Fill
	button_fillColor = new QPushButton(this);
	button_fillColor->setFixedSize(50, 25);
	button_fillColor->setToolTip(QApplication::translate("XrayPaintMainWindow", "Select the medium for the shape fill."));
	button_fillColor->setStyleSheet(QString("QPushButton { background-color: %1 } QToolTip {""} ").arg(m_brush.color().name()));
	connect(button_fillColor, &QPushButton::clicked, colorMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	colorMapper->setMapping(button_fillColor, "ButtonFill");

	slider_fillcolor_red = new QSlider(Qt::Horizontal, this);
	slider_fillcolor_red->setMinimum(0);
	slider_fillcolor_red->setMaximum(255);
	slider_fillcolor_green = new QSlider(Qt::Horizontal, this);
	slider_fillcolor_green->setMinimum(0);
	slider_fillcolor_green->setMaximum(255);
	slider_fillcolor_blue = new QSlider(Qt::Horizontal, this);
	slider_fillcolor_blue->setMinimum(0);
	slider_fillcolor_blue->setMaximum(255);
	spinbox_fillcolor_red = new QSpinBox(this);
	spinbox_fillcolor_red->setMaximum(255);
	spinbox_fillcolor_red->setMinimum(0);
	spinbox_fillcolor_green = new QSpinBox(this);
	spinbox_fillcolor_green->setMaximum(255);
	spinbox_fillcolor_green->setMinimum(0);
	spinbox_fillcolor_blue = new QSpinBox(this);
	spinbox_fillcolor_blue->setMaximum(255);
	spinbox_fillcolor_blue->setMinimum(0);

	connect(slider_fillcolor_red, &QSlider::valueChanged, colorMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	connect(slider_fillcolor_green, &QSlider::valueChanged, colorMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	connect(slider_fillcolor_blue, &QSlider::valueChanged, colorMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	connect(slider_fillcolor_red, &QSlider::valueChanged, spinbox_fillcolor_red, &QSpinBox::setValue);
	connect(slider_fillcolor_green, &QSlider::valueChanged, spinbox_fillcolor_green, &QSpinBox::setValue);
	connect(slider_fillcolor_blue, &QSlider::valueChanged, spinbox_fillcolor_blue, &QSpinBox::setValue);
	connect(spinbox_fillcolor_red, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), slider_fillcolor_red, &QSlider::setValue);
	connect(spinbox_fillcolor_green, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), slider_fillcolor_green, &QSlider::setValue);
	connect(spinbox_fillcolor_blue, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), slider_fillcolor_blue, &QSlider::setValue);
	colorMapper->setMapping(slider_fillcolor_red, "FillRed");
	colorMapper->setMapping(slider_fillcolor_green, "FillGreen");
	colorMapper->setMapping(slider_fillcolor_blue, "FillBlue");

	slider_fill_opacity = new QSlider(Qt::Horizontal, this);
	slider_fill_opacity->setMinimum(0);
	slider_fill_opacity->setMaximum(255);
	slider_fill_opacity->setValue(255);
	spinbox_fill_opacity = new QSpinBox(this);
	spinbox_fill_opacity->setMinimum(0);
	spinbox_fill_opacity->setMaximum(255);
	spinbox_fill_opacity->setValue(255);
	connect(slider_fill_opacity, &QSlider::valueChanged, colorMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	connect(slider_fill_opacity, &QSlider::valueChanged, spinbox_fill_opacity, &QSpinBox::setValue);
	connect(spinbox_fill_opacity, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), slider_fill_opacity, &QSlider::setValue);
	colorMapper->setMapping(slider_fill_opacity, "FillOpacity");

	auto fillColorGridLayout = new QGridLayout;
	fillColorGridLayout->addWidget(new QLabel("R"), 0, 0);
	fillColorGridLayout->addWidget(slider_fillcolor_red, 0, 1);
	fillColorGridLayout->addWidget(new QLabel("G"), 1, 0);
	fillColorGridLayout->addWidget(slider_fillcolor_green, 1, 1);
	fillColorGridLayout->addWidget(new QLabel("B"), 2, 0);
	fillColorGridLayout->addWidget(slider_fillcolor_blue, 2, 1);
	fillColorGridLayout->addWidget(spinbox_fillcolor_red, 0, 2);
	fillColorGridLayout->addWidget(spinbox_fillcolor_green, 1, 2);
	fillColorGridLayout->addWidget(spinbox_fillcolor_blue, 2, 2);
	fillColorGridLayout->addWidget(new QLabel("A"), 3, 0);
	fillColorGridLayout->addWidget(slider_fill_opacity, 3, 1);
	fillColorGridLayout->addWidget(spinbox_fill_opacity, 3, 2);

	auto fillColorGroupLayout = new QGridLayout;
	fillColorGroupLayout->addWidget(button_fillColor, 0, 0, Qt::AlignCenter);
	fillColorGroupLayout->addLayout(fillColorGridLayout, 1, 0);

	auto fillColorGroupBox = new QGroupBox(QApplication::translate("XrayPaintMainWindow", "Fill color"), this);
	fillColorGroupBox->setLayout(fillColorGroupLayout);

	auto outLineTabLayout = new QVBoxLayout;
	outLineTabLayout->setContentsMargins(8, 8, 8, 8);
	outLineTabLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);
	outLineTabLayout->addWidget(fillColorGroupBox);
	outLineTabLayout->addWidget(colorGroupBox);
	outLineTabLayout->addWidget(lineGroupBox);

	auto strokeOptionsPage = new QWidget(optionsTab);
	strokeOptionsPage->setLayout(outLineTabLayout);
	optionsTab->addTab(strokeOptionsPage, QApplication::translate("XrayPaintMainWindow", "Shape"));

	p_flashfilter = new XrayFlashFilter(this);
	optionsTab->addTab(p_flashfilter, QApplication::translate("XrayPaintMainWindow", "XEnhancer"));

	p_voidFilter = new XrayVoidAnalysisFilter(this);
	optionsTab->addTab(p_voidFilter, QApplication::translate("XrayPaintMainWindow", "XVoid"));

	p_bgaFilter = new XrayBGAInspectionFilter(this);
	optionsTab->addTab(p_bgaFilter, QApplication::translate("XrayPaintMainWindow", "BGA Inspection"));


	// list view
	auto optionsTabWidget = new QWidget;
	shapeListView = new XrayPaintShapeListView(optionsTabWidget);
	connect(shapeListView, &XrayPaintShapeListView::shapeChanged, this, &XrayPaintMainWindow::update_paintArea);
	connect(action_Delete, &QAction::triggered, shapeListView, &XrayPaintShapeListView::on_action_selected_deleteShape);

	optionsTabLayout = new QVBoxLayout;
	optionsTabLayout->setContentsMargins(0, 0, 0, 0);
	optionsTabLayout->setSpacing(8);
	optionsTabLayout->addWidget(optionsTab);
	optionsTabLayout->addWidget(shapeListView);
	optionsTab->setCurrentIndex(0);
	optionsTabWidget->setLayout(optionsTabLayout);

	auto scrollArea = new QScrollArea;
	scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	scrollArea->setWidgetResizable(true);
	scrollArea->setWidget(optionsTabWidget);

	dockWidget->setWidget(scrollArea);
	addDockWidget(Qt::RightDockWidgetArea, dockWidget);
}
void XrayPaintMainWindow::setEnhancerDirectoriesBoxVisible(bool _b)
{
	p_flashfilter->getDirectoriesBox()->setVisible(_b);
}
void XrayPaintMainWindow::enableDemoVersion(const QString& _modules, bool _b)
{
	if (_b)
	{
		// remove save actions from the menu.
		action_Save->setEnabled(false);
		action_SaveAs->setEnabled(false);
		action_SaveProj->setEnabled(false);
		action_SaveProjAs->setEnabled(false);

		// remove process button from the flash filter tab.
		auto btn = p_flashfilter->getProcessBtn();
		auto b = btn->blockSignals(true);
		btn->hide();
		btn->blockSignals(b);

		// remove save button from the void filter tab.
		btn = p_voidFilter->getSaveBtn();
		b = btn->blockSignals(true);
		btn->hide();
		btn->blockSignals(b);

		// add filters tabs if not found.
		auto index = optionsTab->indexOf(p_voidFilter);	// returns -1 if not found.
		if (index < 0)
			optionsTab->addTab(p_voidFilter, "XVoid");
		index = optionsTab->indexOf(p_flashfilter);	// returns -1 if not found.
		if (index < 0)
			optionsTab->addTab(p_flashfilter, "XEnhancer");

		// enable water mark in the paint areas.
		XrayMainPaintWidget::setWatermarkImage(QImage(":/res/images/xray-lab_watermark.png"));
		XrayMainPaintWidget::setWatermarkEnabled(true);
	}
	else
	{
		if (_modules.contains("XENHANCER PRO", Qt::CaseInsensitive))
		{
			action_Save->setEnabled(true);
			action_SaveAs->setEnabled(true);
			action_SaveProj->setEnabled(true);
			action_SaveProjAs->setEnabled(true);

			// if XENHANCER PRO license found then add to the tab.
			auto btn = p_flashfilter->getProcessBtn();
			auto b = btn->blockSignals(true);
			btn->show();
			btn->blockSignals(b);
			auto index = optionsTab->indexOf(p_flashfilter);	// returns -1 if not found.
			if (index < 0)
				optionsTab->addTab(p_flashfilter, "XEnhancer");
		}
		else
		{
			// if don't find the XENHANCER PRO license then do not show the tab.
			auto index = optionsTab->indexOf(p_flashfilter);
			if (index >= 0)
				optionsTab->removeTab(index);
		}

		if (_modules.contains("XVOID PRO", Qt::CaseInsensitive))
		{
			action_Save->setEnabled(true);
			action_SaveAs->setEnabled(true);
			action_SaveProj->setEnabled(true);
			action_SaveProjAs->setEnabled(true);

			// if XVOID PRO license found then add to the tab.
			auto btn = p_voidFilter->getSaveBtn();
			auto b = btn->blockSignals(true);
			btn->show();
			btn->blockSignals(b);
			auto index = optionsTab->indexOf(p_voidFilter);	// returns -1 if not found.
			if (index < 0)
				optionsTab->addTab(p_voidFilter, "XVoid");
		}
		else
		{
			auto index = optionsTab->indexOf(p_voidFilter);
			if (index >= 0)
				optionsTab->removeTab(index);
		}

		XrayMainPaintWidget::setWatermarkEnabled(false);
	}

	p_voidFilter->setDemoLimitEnabled(_b);

	// update all painted images to enable or disable the watermark.
	updateAllPaintWidgets();
	dockWidget->adjustSize();
}
void XrayPaintMainWindow::updateAllPaintWidgets()
{
	for (auto i = 0; i < documentTab->count(); i++)
	{
		const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (paintArea) paintArea->update();
	}
}
void XrayPaintMainWindow::zoomWithPercent(const QString& _text)
{
	if (_text == "Fit")
	{
		const auto area = getCurrentPaintArea();
		const auto scroll = documentTab->currentWidget()->findChild<QScrollArea*>("tabScrollArea");
		if (scroll && area)
		{
			auto w = scroll->viewport()->size().width() / static_cast<double>(area->getImage().width());
			auto h = scroll->viewport()->size().height() / static_cast<double>(area->getImage().height());
			area->zoom(std::min(w, h));
		}
	}
	else
	{
		auto scale = _text.left(_text.indexOf(("%"))).toDouble() / 100.0;
		auto area = getCurrentPaintArea();
		if (area)
			area->zoom(scale);
	}
}
void XrayPaintMainWindow::fitToViewportFirstView()
{
	if(zoomCombo->currentIndex() == 0)	// if the current combo index is 0, combo doesn't trigger the signal, it must not be zero index.
		zoomWithPercent("Fit");
	else
		zoomCombo->setCurrentIndex(0);
}
void XrayPaintMainWindow::fitToViewportLastView()
{
	const auto area = getCurrentPaintArea();
	if (!area)
		return;

	area->zoom(getFitToViewportFactor(documentTab->count() - 1));
}
void XrayPaintMainWindow::fitToViewportCurrentView()
{
	if (documentTab->count() <= 0)
		return;

	auto index = documentTab->currentIndex();

	const auto area = documentTab->widget(index)->findChild<XrayMainPaintWidget*>();
	const auto scroll = documentTab->widget(index)->findChild<QScrollArea*>("tabScrollArea");
	if (scroll && area)
	{
		auto s = scroll->viewport()->size();
		auto w = s.width() / static_cast<double>(area->getImage().width());
		auto h = s.height() / static_cast<double>(area->getImage().height());
		area->zoom(std::min(w, h));
	}
}
double XrayPaintMainWindow::getFitToViewportFactor(int _paintAreaIndex)
{
	auto i = _paintAreaIndex;
	if (i >= 0 && i < documentTab->count())
	{
		documentTab->setCurrentIndex(i);	// very important that adjusts the internal widgets size.
		const auto area = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		const auto scroll = documentTab->widget(i)->findChild<QScrollArea*>("tabScrollArea");
		if (scroll && area)
		{
			auto s = scroll->viewport()->size();
			auto w = s.width() / static_cast<double>(area->getImage().width());
			auto h = s.height() / static_cast<double>(area->getImage().height());
			return std::min(w, h);
		}
	}

	return 1.0;
}
void XrayPaintMainWindow::fitToViewportAllViews(bool _toViewPort)
{
	for (auto i = 0; i < documentTab->count(); i++)
	{
		documentTab->setCurrentIndex(i);	// very important that adjusts the internal widgets size.
		const auto area = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		const auto scroll = documentTab->widget(i)->findChild<QScrollArea*>("tabScrollArea");
		if (scroll && area)
		{
			QSize s;

			if (_toViewPort)
			{
				s = scroll->viewport()->size();
			}
			else
			{
				s.setWidth(scroll->size().width() - 2);
				s.setHeight(scroll->size().height() - 2);
			}

			auto w = s.width() / static_cast<double>(area->getImage().width());
			auto h = s.height() / static_cast<double>(area->getImage().height());
			area->zoom(std::min(w, h));
		}
	}
}
void XrayPaintMainWindow::zoom(double _scale)
{
	auto area = getCurrentPaintArea();
	if (area)
	{
		auto factor = area->zoom() + _scale;
		if (factor < 0.125)
			factor = 0.125;
		if (factor > 8.0)
			factor = 8.0;
		area->zoom(factor);
	}
}
double XrayPaintMainWindow::zoom() const
{
	const auto area = getCurrentPaintArea();
	if (area) return area->zoom();
	return 1.0;
}
QMenu* XrayPaintMainWindow::createColorMenu(const char *slot, QColor defaultColor)
{
	QList<QColor> colors;
	colors << Qt::black << Qt::white << Qt::darkGray << Qt::gray << Qt::lightGray << Qt::red << Qt::green << Qt::blue << Qt::cyan << Qt::magenta << Qt::yellow << Qt::darkRed << Qt::darkGreen << Qt::darkBlue << Qt::darkCyan << Qt::darkMagenta << Qt::darkYellow;
	QStringList names;
	names << QApplication::translate("XrayPaintMainWindow", "black") << QApplication::translate("XrayPaintMainWindow", "white") << QApplication::translate("XrayPaintMainWindow", "darkGray") << QApplication::translate("XrayPaintMainWindow", "gray") << QApplication::translate("XrayPaintMainWindow", "lightGray") << QApplication::translate("XrayPaintMainWindow", "red") << QApplication::translate("XrayPaintMainWindow", "green") << QApplication::translate("XrayPaintMainWindow", "blue") << QApplication::translate("XrayPaintMainWindow", "cyan") << QApplication::translate("XrayPaintMainWindow", "magenta") << QApplication::translate("XrayPaintMainWindow", "yellow") << QApplication::translate("XrayPaintMainWindow", "darkRed") << QApplication::translate("XrayPaintMainWindow", "darkGreen") << QApplication::translate("XrayPaintMainWindow", "darkBlue") << QApplication::translate("XrayPaintMainWindow", "darkCyan") << QApplication::translate("XrayPaintMainWindow", "darkMagenta") << QApplication::translate("XrayPaintMainWindow", "darkYellow");

	auto colorMenu = new QMenu(this);
	for (auto i = 0; i < colors.count(); ++i) 
	{
		auto action = new QAction(names.at(i), this);
		action->setData(colors.at(i));
		action->setIcon(createColorIcon(colors.at(i)));
		connect(action, SIGNAL(triggered()), this, slot);
		colorMenu->addAction(action);
		if (colors.at(i) == defaultColor)
			colorMenu->setDefaultAction(action);
	}
	return colorMenu;
}
QIcon XrayPaintMainWindow::createColorToolButtonIcon(const QString &imageFile, QColor color)
{
	QPixmap pixmap(50, 80);
	pixmap.fill(Qt::transparent);
	QPainter painter(&pixmap);
	QPixmap image(imageFile);
	// draw icon centered horizontally on button.
	QRect target(4, 0, 42, 43);
	QRect source(0, 0, 42, 43);
	painter.fillRect(QRect(0, 60, 50, 80), color);
	painter.drawPixmap(target, image, source);

	return QIcon(pixmap);
}
QIcon XrayPaintMainWindow::createColorIcon(QColor color)
{
	QPixmap pixmap(20, 20);
	QPainter painter(&pixmap);
	painter.setPen(Qt::NoPen);
	painter.fillRect(QRect(0, 0, 20, 20), color);

	return QIcon(pixmap);
}
void XrayPaintMainWindow::handleFontChange()
{
	auto area = getCurrentPaintArea();
	if (area)
	{
		auto shape = dynamic_cast<XrayPaintTextShape*>(area->getSelectedShape());
		if (shape)
		{
			auto font = fontCombo->currentFont();
			font.setPointSize(fontSizeCombo->currentText().toInt());
			font.setWeight(boldAction->isChecked() ? QFont::Bold : QFont::Normal);
			font.setItalic(italicAction->isChecked());
			font.setUnderline(underlineAction->isChecked());

			shape->setFont(font);
			area->update();

			m_font = font;
		}
	}
}
void XrayPaintMainWindow::fontSizeChanged(const QString&)
{
	handleFontChange();
}
void XrayPaintMainWindow::currentFontChanged(const QFont&)
{
	handleFontChange();
}
void XrayPaintMainWindow::itemColorChanged(QAction* action, bool _isFill)
{
	auto color = qvariant_cast<QColor>(action->data());
	if (!color.isValid())
		return;

	auto area = getCurrentPaintArea();
	if (!area) return;

	auto shape = area->getSelectedShape();
	if (!shape) return;

	if (_isFill)
	{
		auto brush = shape->getBrush();
		color.setAlpha(brush.color().alpha());
		brush.setColor(color);
		shape->setBrush(brush);
	}
	else
	{
		auto pen = shape->getPen();
		color.setAlpha(pen.color().alpha());
		pen.setColor(color);
		shape->setPen(pen);
	}
	area->update();
}
void XrayPaintMainWindow::itemColorChanged()
{
	fillColorAction = qobject_cast<QAction *>(sender());
	itemColorChanged(fillColorAction, true);
	updateWidgetsOnSelection();
}
void XrayPaintMainWindow::lineColorChanged()
{
	lineColorAction = qobject_cast<QAction *>(sender());
	itemColorChanged(lineColorAction, false);
	updateWidgetsOnSelection();
}
void XrayPaintMainWindow::fillToolButtonTriggered()
{
	itemColorChanged(fillColorAction, true);
	updateWidgetsOnSelection();
}
void XrayPaintMainWindow::lineToolButtonTriggered()
{
	itemColorChanged(lineColorAction, false);
	updateWidgetsOnSelection();
}
void XrayPaintMainWindow::updateTextShapeText()
{
	auto area = getCurrentPaintArea();
	if (!area)
		return;

	if (!area->getSelectedShapes().empty())
	{
		// check if we have a text shape in the selected list
		auto hasText = false;
		for (auto& s : area->getSelectedShapes())
		{
			if (s->getType() == PaintDrawModeType::Text)
			{
				hasText = true;
			}
		}

		if (!hasText)
			return;

		auto last = dynamic_cast<XrayPaintTextShape*>(area->getSelectedShapes().last());
		bool ok;
		auto text = QInputDialog::getText(this, QApplication::translate("XrayPaintMainWindow", "Edit Text"),
			QApplication::translate("XrayPaintMainWindow", "Enter Text"), QLineEdit::Normal, last ? last->getText() : "Text", &ok);
		if (ok && !text.isEmpty())
		{
			auto n = 0;
			for (auto& s : area->getSelectedShapes())
			{
				auto shape = dynamic_cast<XrayPaintTextShape*>(s);
				if (shape)
				{
					shape->setText(QString(text + "%1").arg(n++));
					area->update();
				}
			}
		}
	}
	else if (area->getSelectedShape())
	{
		auto shape = dynamic_cast<XrayPaintTextShape*>(area->getSelectedShape());
		if (shape)
		{
			bool ok;
			auto text = QInputDialog::getText(this, QApplication::translate("XrayPaintMainWindow", "Edit Text"),
				QApplication::translate("XrayPaintMainWindow", "Enter Text"), QLineEdit::Normal, shape->getText(), &ok);
			if (ok && !text.isEmpty())
			{
				shape->setText(text);
				area->update();
			}
		}
	}
	else
	{
		statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "Please first select a text box!"), 10000);
	}
}
void XrayPaintMainWindow::createNewTabPage(const QString& fileName, const QString& caption)
{
	/* centralTab
	 *  ->newTab
	 *   ->newLayout
	 *    ->newScrollArea
	 *     ->newPaintArea
	 */

	QString tabName;

	auto notExists = false;
	if (!QFile::exists(fileName))
	{
		tabName = QApplication::translate("XrayPaintMainWindow", "Untitled") + " " +QString::number(m_pageId++);
		notExists = true;
	}
	else
	{
		tabName = QString(QFileInfo(fileName).fileName());
		menu_recentFiles->addRecentFile(fileName);
	}

	auto newTab = new QWidget(this);
	newTab->setObjectName(fileName);
	auto newLayout = new QHBoxLayout;
	auto newScrollArea = new QScrollArea(newTab);
	newScrollArea->setObjectName("tabScrollArea");
	auto newPaintArea = new XrayMainPaintWidget(fileName, QSize(800, 600), this);
	newPaintArea->setObjectName("paintArea");
	newPaintArea->setBackground(":/paint/images/paint_bk_transp_small.png");
	newPaintArea->setFileCaption(caption);
	newPaintArea->setMode(getMode());

	newScrollArea->setAlignment(Qt::AlignCenter);
	newScrollArea->setWidget(newPaintArea);
	newLayout->addWidget(newScrollArea);
	newTab->setLayout(newLayout);
	auto tabText = tabName + " [" + QString::number(newPaintArea->width()) + "x" + QString::number(newPaintArea->height()) + "]";
	documentTab->addTab(newTab, tabText);
	documentTab->setCurrentWidget(newTab);
	projTreeView->treeView()->addChild("Project", notExists ? QApplication::translate("XrayPaintMainWindow", "File not existed-") + tabText : tabText, fileName, true);
	activateTreeItemFromTab(documentTab->currentIndex());

	shapeListView->setModel(newPaintArea->getShapeListModel());
	connect(shapeListView->model(), &QAbstractItemModel::rowsRemoved, this, &XrayPaintMainWindow::update_paintArea);
	connect(shapeListView->selectionModel(), &QItemSelectionModel::selectionChanged, newPaintArea, &XrayMainPaintWidget::selectShape);

	connect(this, &XrayPaintMainWindow::drawModeChanged, newPaintArea, &XrayMainPaintWidget::setMode);
	connect(newPaintArea, &XrayMainPaintWidget::filenameChanged, this, &XrayPaintMainWindow::on_paintArea_filenameChanged);
	connect(newPaintArea, &XrayMainPaintWidget::mouseMoved, this, &XrayPaintMainWindow::update_mousePosition);
	connect(newPaintArea, &XrayMainPaintWidget::selectionChanged, this, &XrayPaintMainWindow::on_paintArea_selectionChanged);
	//connect(newPaintArea, &XrayMainPaintWidget::selectionChanged, shapeListView, &XrayPaintShapeListView::selectShape);
	connect(newPaintArea, &XrayMainPaintWidget::onShapeCreated, this, &XrayPaintMainWindow::onShapeCreated);
	connect(newPaintArea, &XrayMainPaintWidget::doubleClicked, this, &XrayPaintMainWindow::doubleClickEvent);
	connect(newPaintArea, &XrayMainPaintWidget::customShapeChanged, this, &XrayPaintMainWindow::customShapeChanged);
}
void XrayPaintMainWindow::onShapeCreated(XrayPaintBaseShape* const shape)
{
	auto text = dynamic_cast<XrayPaintTextShape*>(shape);
	if (text) updateTextShapeText();
}
void XrayPaintMainWindow::on_actiongroup_Tools_triggered(QAction* pressedAction)
{
	m_mode = static_cast<PaintDrawModeType>(pressedAction->data().toInt());
	emit drawModeChanged(m_mode);
}
void XrayPaintMainWindow::update_paintArea()
{
	auto area = getCurrentPaintArea();
	if (area) area->update();
	updateTotalSurfaceArea();
}
void XrayPaintMainWindow::updateFontWidgetsOnSelection()
{
	auto area = getCurrentPaintArea();
	if (!area)
		return;

	auto shape = dynamic_cast<XrayPaintTextShape*>(area->getSelectedShape());
	if (shape)
	{
		auto font = shape->getFont();

		auto b = fontCombo->blockSignals(true);
		fontCombo->setCurrentFont(font);
		fontCombo->blockSignals(b);

		b = fontSizeCombo->blockSignals(true);
		fontSizeCombo->setEditText(QString().setNum(font.pointSize()));
		fontSizeCombo->blockSignals(b);

		b = boldAction->blockSignals(true);
		boldAction->setChecked(font.weight() == QFont::Bold);
		boldAction->blockSignals(b);

		b = italicAction->blockSignals(true);
		italicAction->setChecked(font.italic());
		italicAction->blockSignals(b);

		b = underlineAction->blockSignals(true);
		underlineAction->setChecked(font.underline());
		underlineAction->blockSignals(b);

		m_font = font;
	}
}
void XrayPaintMainWindow::updateArrowWidgetsOnSelection()
{
	auto area = getCurrentPaintArea();
	if (!area)
		return;

	auto shape = dynamic_cast<XrayPaintArrowShape*>(area->getSelectedShape());
	if (shape)
	{
		auto b = slider_arrow_width->blockSignals(true);
		slider_arrow_width->setValue(shape->getArrowSize());
		slider_arrow_width->blockSignals(b);

		b = spinbox_arrow_width->blockSignals(true);
		spinbox_arrow_width->setValue(shape->getArrowSize());
		spinbox_arrow_width->blockSignals(b);

		b = combobox_arrowdir->blockSignals(true);
		combobox_arrowdir->setCurrentIndex(shape->getArrowDirection());
		combobox_arrowdir->blockSignals(b);

		m_arrow_size = shape->getArrowSize();
		m_arrow_dir = shape->getArrowDirection();
	}
}
void XrayPaintMainWindow::updateWidgetsOnSelection(const XrayPaintBaseShape* const shape)
{
	if (!shape)
		return;

	auto pen = shape->getPen();
	auto brush = shape->getBrush();
	auto pencolor = pen.color();
	auto brushcolor = brush.color();

	auto b = slider_strokecolor_red->blockSignals(true);
	slider_strokecolor_red->setValue(pencolor.red());
	slider_strokecolor_red->blockSignals(b);

	b = slider_strokecolor_green->blockSignals(true);
	slider_strokecolor_green->setValue(pencolor.green());
	slider_strokecolor_green->blockSignals(b);

	b = slider_strokecolor_blue->blockSignals(true);
	slider_strokecolor_blue->setValue(pencolor.blue());
	slider_strokecolor_blue->blockSignals(b);

	b = slider_stroke_opacity->blockSignals(true);
	slider_stroke_opacity->setValue(pencolor.alpha());
	slider_stroke_opacity->blockSignals(b);

	b = spinbox_strokecolor_red->blockSignals(true);
	spinbox_strokecolor_red->setValue(pencolor.red());
	spinbox_strokecolor_red->blockSignals(b);

	b = spinbox_strokecolor_green->blockSignals(true);
	spinbox_strokecolor_green->setValue(pencolor.green());
	spinbox_strokecolor_green->blockSignals(b);

	b = spinbox_strokecolor_blue->blockSignals(true);
	spinbox_strokecolor_blue->setValue(pencolor.blue());
	spinbox_strokecolor_blue->blockSignals(b);

	b = spinbox_stroke_opacity->blockSignals(true);
	spinbox_stroke_opacity->setValue(pencolor.alpha());
	spinbox_stroke_opacity->blockSignals(b);

	b = button_strokeColor->blockSignals(true);
	button_strokeColor->setStyleSheet(QString("background-color: %1").arg(pencolor.name()));
	button_strokeColor->blockSignals(b);

	b = slider_fillcolor_red->blockSignals(true);
	slider_fillcolor_red->setValue(brushcolor.red());
	slider_fillcolor_red->blockSignals(b);

	b = slider_fillcolor_green->blockSignals(true);
	slider_fillcolor_green->setValue(brushcolor.green());
	slider_fillcolor_green->blockSignals(b);

	b = slider_fillcolor_blue->blockSignals(true);
	slider_fillcolor_blue->setValue(brushcolor.blue());
	slider_fillcolor_blue->blockSignals(b);

	b = slider_fill_opacity->blockSignals(true);
	slider_fill_opacity->setValue(brushcolor.alpha());
	slider_fill_opacity->blockSignals(b);

	b = spinbox_fillcolor_red->blockSignals(true);
	spinbox_fillcolor_red->setValue(brushcolor.red());
	spinbox_fillcolor_red->blockSignals(b);

	b = spinbox_fillcolor_green->blockSignals(true);
	spinbox_fillcolor_green->setValue(brushcolor.green());
	spinbox_fillcolor_green->blockSignals(b);

	b = spinbox_fillcolor_blue->blockSignals(true);
	spinbox_fillcolor_blue->setValue(brushcolor.blue());
	spinbox_fillcolor_blue->blockSignals(b);

	b = spinbox_fill_opacity->blockSignals(true);
	spinbox_fill_opacity->setValue(brushcolor.alpha());
	spinbox_fill_opacity->blockSignals(b);

	b = button_fillColor->blockSignals(true);
	button_fillColor->setStyleSheet(QString("background-color: %1").arg(brushcolor.name()));
	button_fillColor->blockSignals(b);

	b = combobox_penstyle->blockSignals(true);
	combobox_penstyle->setCurrentIndex(static_cast<int>(pen.style()));
	combobox_penstyle->blockSignals(b);

	b = combobox_brushstyle->blockSignals(true);
	combobox_brushstyle->setCurrentIndex(static_cast<int>(brush.style()));
	combobox_brushstyle->blockSignals(b);

	b = slider_stroke_width->blockSignals(true);
	slider_stroke_width->setValue(pen.width());
	slider_stroke_width->blockSignals(b);

	b = spinbox_stroke_width->blockSignals(true);
	spinbox_stroke_width->setValue(pen.width());
	spinbox_stroke_width->blockSignals(b);

	if (pen.capStyle() == Qt::PenCapStyle::RoundCap)
	{
		b = radiobutton_roundcap->blockSignals(true);
		radiobutton_roundcap->setAutoExclusive(false);
		radiobutton_roundcap->setChecked(true);
		radiobutton_roundcap->setAutoExclusive(true);
		radiobutton_roundcap->blockSignals(b);
	}
	else
	{
		b = radiobutton_flatcap->blockSignals(true);
		radiobutton_flatcap->setAutoExclusive(false);
		radiobutton_flatcap->setChecked(true);
		radiobutton_flatcap->setAutoExclusive(true);
		radiobutton_flatcap->blockSignals(b);
	}

	b = fillColorToolButton->blockSignals(true);
	fillColorToolButton->setIcon(createColorToolButtonIcon(":/paint/images/fill_color_icon.png", brushcolor));
	fillColorToolButton->blockSignals(b);
	b = lineColorToolButton->blockSignals(true);
	lineColorToolButton->setIcon(createColorToolButtonIcon(":/paint/images/line_color_icon.png", pencolor));
	lineColorToolButton->blockSignals(b);

	updateFontWidgetsOnSelection();
	updateArrowWidgetsOnSelection();

	m_pen = pen;
	m_brush = brush;
}
void XrayPaintMainWindow::updateWidgetsOnSelection()
{
	auto area = getCurrentPaintArea();
	if (area)
	{
		auto shape = area->getSelectedShape();
		if (shape) updateWidgetsOnSelection(shape);
	}
}
void XrayPaintMainWindow::on_paintArea_selectionChanged(XrayPaintBaseShape* const shape)
{
	if (shape)
	{
		const auto& name = shape->getName();
		const auto rect = shape->getBoundingRect();
		label_selectedShape->setText(QString(QApplication::translate("XrayPaintMainWindow", "Selected shape") + ": %1").arg(name));
		label_currentSurface->setText(QString(QApplication::translate("XrayPaintMainWindow", "ROI") + ": %1x%2px (%3x%4px)").arg(rect.x).arg(rect.y).arg(rect.width).arg(rect.height));
		updateTotalSurfaceArea();
		updateWidgetsOnSelection(shape);
	}
	else
	{
		label_selectedShape->setText(QString(QApplication::translate("XrayPaintMainWindow", "Selected shape") + ": N/A"));
		label_currentSurface->setText(QString(QApplication::translate("XrayPaintMainWindow", "ROI") + ": N/A"));
		updateTotalSurfaceArea();
	}

	auto area = getCurrentPaintArea();
	if (area)
	{
		// We must block the signals here, because selection shape will call the selectionModel to select the row which calls area->selectShape that calls selectionChanged which calls this function,
		// so it becomes recursive loop.
		auto blocked = area->blockSignals(true);
		shapeListView->selectShape(shape);
		area->blockSignals(blocked);
	}

	p_voidFilter->updateSelectedShapeParameters();
}
void XrayPaintMainWindow::on_paintArea_filenameChanged(const QString& fileName)
{
	auto area = getCurrentPaintArea();
	if (!area)
		return;

	auto oldFileName = documentTab->widget(documentTab->currentIndex())->objectName();

	auto tabText = QString(QFileInfo(fileName).fileName()) + " [" + QString::number(area->width()) + "x" + QString::number(area->height()) + "]";

	documentTab->widget(documentTab->currentIndex())->setObjectName(fileName);
	documentTab->setTabText(documentTab->currentIndex(), tabText);

	auto b = projTreeView->blockSignals(true);
	auto item = projTreeView->treeView()->childItemByData(oldFileName);
	if (item)
	{
		item->setText(tabText);
		item->setData(fileName, Qt::UserRole);
	}
	projTreeView->blockSignals(b);
}
XrayMainPaintWidget* XrayPaintMainWindow::getCurrentPaintArea() const
{
	const auto area = documentTab->currentWidget()->findChild<XrayMainPaintWidget*>("paintArea");
	if (area) return area;
	return nullptr;
}
XrayVoidAnalysisFilter* XrayPaintMainWindow::getVoidFilter() const
{	
	
	if(p_voidFilter)
		return p_voidFilter;
	else return nullptr;
}
QVector<XrayMainPaintWidget*> XrayPaintMainWindow::getPaintAreas() const
{
	QVector<XrayMainPaintWidget*> areas;
	for (auto i = 0; i < documentTab->count(); i++)
	{
		const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (paintArea) 
			areas.push_back(paintArea);
	}

	return areas;
}
void XrayPaintMainWindow::on_buttonGroup_capType_buttonClicked(QAbstractButton* btn)
{
	auto area = getCurrentPaintArea();
	if (!area) return;

	auto shape = area->getSelectedShape();
	if (!shape) return;

	auto pen = shape->getPen();

	if (btn == radiobutton_flatcap)
		pen.setCapStyle(Qt::PenCapStyle::FlatCap);
	else if (btn == radiobutton_roundcap)
		pen.setCapStyle(Qt::PenCapStyle::RoundCap);
	shape->setPen(pen);
	area->update();
}
void XrayPaintMainWindow::on_combobox_penStyle_activated(int index)
{
	auto area = getCurrentPaintArea();
	if (!area) return;

	auto shape = area->getSelectedShape();
	if (!shape) return;
	
	auto pen = shape->getPen();
	pen.setStyle(static_cast<Qt::PenStyle>(index));
	shape->setPen(pen);
	area->update();
	m_pen = pen;
}
void XrayPaintMainWindow::on_combobox_arrowDir_activated(int index)
{
	auto area = getCurrentPaintArea();
	if (!area) return;

	auto shape = dynamic_cast<XrayPaintArrowShape*>(area->getSelectedShape());
	if (!shape) return;

	shape->setArrowDirection(index);
	area->update();

	m_arrow_dir = index;
}
void XrayPaintMainWindow::on_slider_arrowWidth_changed(int _width)
{
	auto area = getCurrentPaintArea();
	if (!area) return;

	auto shape = dynamic_cast<XrayPaintArrowShape*>(area->getSelectedShape());
	if (!shape) return;

	shape->setArrowSize(_width);
	area->update();

	m_arrow_size = _width;
}
void XrayPaintMainWindow::on_combobox_brushStyle_activated(int index)
{
	auto area = getCurrentPaintArea();
	if (!area) return;

	auto shape = area->getSelectedShape();
	if (!shape) return;

	auto brush = shape->getBrush();
	brush.setStyle(static_cast<Qt::BrushStyle>(index));
	shape->setBrush(brush);
	area->update();
	m_brush = brush;
}
void XrayPaintMainWindow::updateTotalSurfaceArea()
{
	auto totalSurfaceArea = 0;
	if (getCurrentPaintArea())
		totalSurfaceArea = getCurrentPaintArea()->calculateTotalSurface();
	label_totalSurface->setText(QString(QApplication::translate("XrayPaintMainWindow", "Total area") + ": %1").arg(totalSurfaceArea));
}
void XrayPaintMainWindow::openProject(const QString& fileName)
{
	if (!QFile::exists(fileName))
	{
		menu_recentProjFiles->removeRecentFile(fileName);
		statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "File doesn't exists!"));
		return;
	}

	// first, close all opened files.
	on_action_closeAllFiles_triggered();
	// if use press no to message box that will appear on closing, 
	// that mean we still have an opened page, so user doesn't want to close the project,
	// so just return from here.
	if (documentTab->count() > 0)
		return;

	// now open the project settings.
	QSettings settings(fileName, QSettings::Format::IniFormat);
	readSettings(settings, fileName);

	XrayGlobal::setLastFileOpenPath(fileName);

	menu_recentProjFiles->addRecentFile(fileName);

	m_projectFilePath = fileName;

	// reset shape tool to Selection
	tool_Select->trigger();

	statusBar()->showMessage(QFileInfo(fileName).fileName());
}
void XrayPaintMainWindow::on_action_OpenProj_triggered()
{
	auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayPaintMainWindow", "Select File"), XrayGlobal::getLastFileOpenPath(), "XPaint Project Type (*.xpaintproj)", nullptr);
	if (fileName.isEmpty())
		return;

	openProject(fileName);
}
bool XrayPaintMainWindow::copyAllOpenedImages(const QString& dirPath)
{
	auto path(dirPath);

	if (!path.endsWith('/'))
		path.append('/');

	auto ok = false;
	for (auto i = 0; i < documentTab->count(); i++)
	{
		const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (paintArea)
		{
			auto fi = QFileInfo(paintArea->getFileName());
			auto dst = path + fi.fileName();
			if (paintArea->getFileName() == dst)
			{
				// file already exisited
			}
			else
			{
				if (QFile::exists(dst))
					QFile::remove(dst);
				if (QFile::copy(paintArea->getFileName(), dst))
					ok = true;
			}
		}
	}

	return ok;
}
void XrayPaintMainWindow::changeImagesLocation(const QString& dirPath)
{
	auto path(dirPath);

	if (!path.endsWith('/'))
		path.append('/');

	for (auto i = 0; i < documentTab->count(); i++)
	{
		const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (paintArea)
		{
			auto fileName = paintArea->getFileName();
			paintArea->setFileName(path + QFileInfo(fileName).fileName());

			// synch tree item data with the changed file locations
			projTreeView->treeView()->setChildData(fileName, paintArea->getFileName());
		}
	}
}
bool XrayPaintMainWindow::saveAsProjectImages(const QString& projectFilePath)
{
	auto fi = QFileInfo(projectFilePath);
	auto path = XrayQFileUtil::getStandardPath(fi.baseName(), fi.absolutePath());

	copyAllOpenedImages(path);
	changeImagesLocation(path);

	return true;
}
bool XrayPaintMainWindow::saveProjectSettings(const QString& fileName)
{
	if (projTreeView->treeView()->hasDuplicateChildItem())
	{
		QMessageBox(QMessageBox::Information, QApplication::applicationName(), QApplication::translate("XrayPaintTabWidget", "Duplicate file names found!\nPlease remove duplicates before saving the project."), QMessageBox::Ok).exec();
		return false;
	}

	saveAsProjectImages(fileName);

	QSettings settings(fileName, QSettings::Format::IniFormat);
	settings.clear();	// settings append values to the existing file, so first, clear the previous settings.
	writeSettings(settings, fileName);

	statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "Project settings have been saved successfully!"), 8000);

	return true;
}
void XrayPaintMainWindow::on_action_SaveProj_triggered()
{
	if(QFile::exists(m_projectFilePath))
	{
		saveProjectSettings(m_projectFilePath);
	}
	else
	{
		on_action_SaveProjAs_triggered();
	}
}
void XrayPaintMainWindow::on_action_SaveProjAs_triggered()
{
	auto area = getCurrentPaintArea();
	if (!area)
		return;

	QString name;
	if (!m_projectFilePath.isEmpty())
		name = QFileInfo(m_projectFilePath).fileName();
	else
		name = "settings.xpaintproj";

	auto fileName = QFileDialog::getSaveFileName(this, QApplication::translate("XrayPaintMainWindow", "Save File As..."), XrayGlobal::getLastFileOpenPath() + '/' + name, "XPaint Project Type (*.xpaintproj)", nullptr);
	if (fileName.isEmpty())
		return;

	auto ext = QFileInfo(fileName).suffix();
	if (ext.isEmpty())
		fileName += ".xpaintproj";

	XrayGlobal::setLastFileOpenPath(fileName);

	if (saveProjectSettings(fileName))
		m_projectFilePath = fileName;
}
void XrayPaintMainWindow::on_action_Reload_triggered()
{
	auto area = getCurrentPaintArea();
	if (!area) return;
	area->reload();
}
void XrayPaintMainWindow::on_action_Undo_triggered()
{
	auto area = getCurrentPaintArea();
	if (!area) return;
	area->undo();
}
void XrayPaintMainWindow::on_action_Redo_triggered()
{
	auto area = getCurrentPaintArea();
	if (!area) return;
	area->redo();
}
void XrayPaintMainWindow::on_action_fileCaption_triggered()
{
	auto caption = QString("");
	auto area = getCurrentPaintArea();
	if (area) caption = area->getFileCaption();

	bool ok;
	auto text = QInputDialog::getMultiLineText(this, QApplication::translate("XrayPaintMainWindow", "Edit Picture Caption"),
		QApplication::translate("XrayPaintMainWindow", "Picture Caption"), caption, &ok);
	if (ok && !text.isEmpty() && area)
	{
		area->setFileCaption(text);
		statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "Picture caption has been recorded successfully."));
	}
}
void XrayPaintMainWindow::on_action_numImages_triggered()
{
	bool ok;
	auto n = QInputDialog::getInt(this, QApplication::translate("XrayPaintMainWindow", "Edit Pictures Per Page"),
		QApplication::translate("XrayPaintMainWindow", "Pictures Per Page"), m_numImagesPerPage, 1, 9, 1, &ok);
	if (ok)
	{
		m_numImagesPerPage = n;
		statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "Pictures Per Page has been recorded successfully."));
	}
}
int XrayPaintMainWindow::getNumImagesPerPage() const
{
	return m_numImagesPerPage;
}
void XrayPaintMainWindow::on_action_newFile_triggered()
{
	on_action_closeAllFiles_triggered();
}
void XrayPaintMainWindow::dragEnterEvent(QDragEnterEvent *e)
{
	if (e->mimeData()->hasUrls())
		e->acceptProposedAction();
}
void XrayPaintMainWindow::dropEvent(QDropEvent* event)
{
	const auto urls = event->mimeData()->urls();
	if (urls.empty())
		return;

	QStringList temp;
	for (auto& url : urls)
		temp.append(url.toLocalFile());

	// check if one of the files are the project file
	for (auto& file : temp)
	{
		if (QFileInfo(file).suffix() == "xpaintproj")
		{
			openProject(file);
			return;
		}
	}

	// if no project file found then load the image files
	openFiles(temp);
	fitToViewportAllViews();
	setCurrentTabIndex(temp);
}
int XrayPaintMainWindow::setCurrentTabIndex(const QStringList& fileNames)
{
	foreach(const auto& fileName, fileNames)
	{
		for (auto i = 0; i < documentTab->count(); i++)
		{
			const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
			if (paintArea)
			{
				if (paintArea->getFileName() == fileName)
				{
					documentTab->setCurrentIndex(i);
					return i;
				}
			}
		}
	}

	return -1;
}
bool XrayPaintMainWindow::openFile(const QString& fileName)
{
	if (!QFile::exists(fileName))
	{
		menu_recentFiles->removeRecentFile(fileName);
		statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "File doesn't exists!"));
		return false;
	}

	for (auto i = 0; i < documentTab->count(); i++)
	{
		const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (paintArea)
		{
			if (paintArea->getFileName() == fileName)
			{
				paintArea->reload();
				documentTab->setCurrentIndex(i);
				return true;
			}
		}
	}

	createNewTabPage(fileName, QString());
	return true;
}
void XrayPaintMainWindow::openFiles(const QStringList& fileNames)
{
	if (fileNames.isEmpty())
		return;

	QString names;
	foreach(const auto& fileName, fileNames)
	{
		const auto nameWithExtenstion = QFileInfo(fileName).fileName();
		auto isAlreadyOpened = false;

		for (auto i = 0; i < documentTab->count(); i++)
		{
			const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
			if (paintArea)
			{
				if (paintArea->getFileName() == fileName)
				{
					documentTab->setCurrentIndex(i);
					names.append(nameWithExtenstion + ", ");
					isAlreadyOpened = true;
					break;
				}
			}
		}

		if (!isAlreadyOpened)
			createNewTabPage(fileName, QString());
	}

	if (!names.isEmpty())
		statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "%1 are already opened!").arg(names), 5000);
}
void XrayPaintMainWindow::openFiles(const QVector<QPair<QString, QString> >& fileNames)
{
	if (fileNames.isEmpty())
		return;

	QString names;
	foreach(const auto& fileName, fileNames)
	{
		const auto nameWithExtenstion = QFileInfo(fileName.first).fileName();
		auto isAlreadyOpened = false;

		for (auto i = 0; i < documentTab->count(); i++)
		{
			const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
			if (paintArea)
			{
				if (paintArea->getFileName() == fileName.first)
				{
					documentTab->setCurrentIndex(i);
					names.append(nameWithExtenstion + ", ");
					isAlreadyOpened = true;
					break;
				}
			}
		}

		if (!isAlreadyOpened)
			createNewTabPage(fileName.first, fileName.second);
	}

	if (!names.isEmpty())
		statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "%1 are already opened!").arg(names));
}
QVector<QPair<QString, QString> > XrayPaintMainWindow::getFileNamesAndCaptions()
{
	QVector<QPair<QString, QString> > images;
	if (documentTab->count() == 0)
		return images;

	images.reserve(documentTab->count());
	for (auto i = 0; i < documentTab->count(); i++)
	{
		const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (paintArea) images.push_back(QPair<QString, QString>(paintArea->getFileName(), paintArea->getFileCaption()));
	}
	return images;
}
QVector<std::tuple<QString, QString, QImage> > XrayPaintMainWindow::getImagesWithCaptionsAndFiles()
{
	QVector<std::tuple<QString, QString, QImage> > images;
	images.reserve(documentTab->count());
	for (auto i = 0; i < documentTab->count(); i++)
	{
		const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (paintArea) images.push_back(std::make_tuple(paintArea->getFileName(), paintArea->getFileCaption(), paintArea->copyImage()));
	}
	return images;
}
void XrayPaintMainWindow::on_action_openFile_triggered()
{
	auto fileNames = QFileDialog::getOpenFileNames(this, QApplication::translate("XrayPaintMainWindow", "Select Files"), XrayGlobal::getLastFileOpenPath(), XrayGlobal::getAllSupportedImageExtensions(QApplication::translate("XrayPaintMainWindow", "Image Files"), {"*.dcm"}), nullptr);
	if (fileNames.isEmpty())
		return;

	openFiles(fileNames);
	fitToViewportAllViews();
	setCurrentTabIndex(fileNames);
	XrayGlobal::setLastFileOpenPath(fileNames);
}
void XrayPaintMainWindow::on_action_openDir_triggered()
{
	auto dir = QFileDialog::getExistingDirectory(this, QApplication::translate("XrayPaintMainWindow", "Open Directory"), XrayGlobal::getLastFileOpenPath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	if (dir.isEmpty()) 
		return;
	QDir directory(dir);
	if (!directory.exists())
		return;
	auto fileNames = directory.entryList(XrayGlobal::getAllSupportedImageExtensions() + QStringList() << "*.dcm", QDir::Files);
	if (fileNames.empty())
		return;

	QStringList temp;
	foreach(QString filename, fileNames)
		temp.append(dir + QDir::separator() + filename);
	openFiles(temp);

	fitToViewportAllViews();

	XrayGlobal::setLastFileOpenPath(fileNames);
}
void XrayPaintMainWindow::on_action_closeFile_triggered()
{
	if (documentTab->count() > 0)
	{
		if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayPaintMainWindow", "Are you sure you want to close?"), QMessageBox::Yes | QMessageBox::No).exec())
			documentTab->deleteTab(documentTab->currentIndex());
	}
}
void XrayPaintMainWindow::closeAllFiles()
{
	if (documentTab->count() > 0)
	{
		for (auto i = documentTab->count() - 1; i >= 0; --i)
			documentTab->deleteTab(i);
		m_projectFilePath.clear();
		projTreeView->treeView()->sourceModel()->clear();
		m_pageId = 0;
	}
}
void XrayPaintMainWindow::on_action_closeAllFiles_triggered()
{
	if (documentTab->count() > 0)
	{
		if (QFile::exists(m_projectFilePath))
		{
			QMessageBox saveMessage;
			const auto save = saveMessage.addButton(QApplication::translate("XrayPaintTabWidget", "Save and Close"), QMessageBox::AcceptRole);
			const auto close = saveMessage.addButton(QApplication::translate("XrayPaintTabWidget", "Close"), QMessageBox::DestructiveRole);
			saveMessage.addButton(QApplication::translate("XrayPaintTabWidget", "Cancel"), QMessageBox::RejectRole);
			saveMessage.setText(QApplication::translate("XrayPaintTabWidget", "You have an opened project. Do you want to save current settings to the project file before you close all pictures?"));
			saveMessage.setIcon(QMessageBox::Question);
			saveMessage.exec();
			if (saveMessage.clickedButton() == save)
			{
				// first, save project settings and images
				if (saveProjectSettings(m_projectFilePath))
					closeAllFiles();
			}
			else if (saveMessage.clickedButton() == close)
			{
				closeAllFiles();
			}
			else
			{
			}
		}
		else
		{
			if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayPaintMainWindow", "Are you sure you want to close all pictures?"), QMessageBox::Yes | QMessageBox::No).exec())
				closeAllFiles();
		}
	}
}
void XrayPaintMainWindow::tabAboutToCloseOnSave(int index)
{
	// close the tab
	tabAboutToClose(index);
}
void XrayPaintMainWindow::tabAboutToClose(int index)
{
	if (index < 0 || index >= documentTab->count())
		return;

	const auto area = documentTab->widget(index)->findChild<XrayMainPaintWidget*>("paintArea");
	if (area)
	{
		// clear the XVoid table
		p_voidFilter->clearTable(area->getFileName());
	}
}
void XrayPaintMainWindow::on_action_save_triggered()
{
	auto area = getCurrentPaintArea();
	if (!area) return;

	if (area->getFileName().isEmpty())
	{
		on_action_saveFileAs_triggered();
	}
	else
	{
		QFileInfo fi(area->getFileName());
		auto coppiedFile = fi.path() + '/' + fi.baseName() + '_' + QDateTime::currentDateTime().toString("MM-dd-yyyy-hh-mm-ss") +  '.' + fi.suffix();

		if(area->saveImage(coppiedFile))
			statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "A copy of the current picture has been saved!"), 8000);
		else
			statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "Couldn't save the picture!"), 8000);
	}
}
void XrayPaintMainWindow::on_action_saveFileAs_triggered()
{
	QString name;
	auto area = getCurrentPaintArea();
	if (area)
	{
		if (!area->getFileName().isEmpty())
			name = QFileInfo(area->getFileName()).completeBaseName();
		else
			name = "picture.jpg";
	}

	auto fileName = QFileDialog::getSaveFileName(this, QApplication::translate("XrayPaintMainWindow", "Save File As..."), XrayGlobal::getLastFileOpenPath() + '/' + name, XrayGlobal::getAllSupportedImageExtensionsForSaveDialog(), nullptr);
	if (!fileName.isEmpty())
	{
		auto ext = QFileInfo(fileName).suffix();
		if (ext.isEmpty())
			fileName += ".jpg";

		auto area = getCurrentPaintArea();
		if (area)
		{
			if (area->saveImage(fileName))
				statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "Picture has been saved successfully!"), 8000);
			else
				statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "Couldn't save the picture!"), 8000);
		}

		XrayGlobal::setLastFileOpenPath(fileName);
	}
}
void XrayPaintMainWindow::on_action_print_triggered()
{
	auto area = getCurrentPaintArea();
	if (!area) return;

	QPrinter printer;
	auto dlg = new QPrintDialog(&printer, nullptr);
	if (dlg->exec() == QDialog::Accepted) 
	{
		QPainter painter(&printer);
		painter.drawImage(QPoint(0, 0), area->copyImage());
		painter.end();
	}
	delete dlg;
}
void XrayPaintMainWindow::on_action_copy_triggered()
{
	auto area = getCurrentPaintArea();
	if (!area) return;
	
	// if there is selected shape then just copy that shape, otherwise copy the whole image.
	if (!area->getSelectedShapes().empty())
	{
		selectedShapes.clear();
		for (auto& shape : area->getSelectedShapes())
		{
			if (shape)
				selectedShapes.append(shape->clone());
		}
		statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "Selected shapes are copied!"), 8000);
	}
	else if (area->getSelectedShape())
	{
		selectedShapes.clear();
		selectedShapes.append(area->getSelectedShape()->clone());
	}
	else
	{
		clipboard->setImage(area->copyImage());
	}
}
void XrayPaintMainWindow::on_action_paste_triggered()
{
	auto area = getCurrentPaintArea();
	if (!area) return;

	// if a shape is already copied then paste that shape, otherwise paste the whole image.
	if (!selectedShapes.empty())
	{
		area->pasteShape(selectedShapes);
		statusBar()->showMessage(QApplication::translate("XrayPaintMainWindow", "Copied shapes are pasted!"), 8000);
	}
	else
	{
		if (!clipboard->image().isNull())
			area->pasteImage(clipboard->image());
	}
}
void XrayPaintMainWindow::on_action_deleteAllShapes_triggered()
{
	auto area = getCurrentPaintArea();
	if (area)
	{
		if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayPaintMainWindow", "Are you sure you want to delete all shapes?"), QMessageBox::Yes | QMessageBox::No).exec())
			shapeListView->on_action_deleteShapes_triggered();
	}
}
void XrayPaintMainWindow::on_action_resizeImage_triggered()
{
	auto area = getCurrentPaintArea();
	if (!area)
		return;

	XrayResizeDialog resizeDialog(this, area->size());
	if (resizeDialog.exec() == 1)
	{
		auto newSize(resizeDialog.getNewSize());
		if (resizeDialog.isKeepAspectRatioChecked())
			newSize = XrayGlobal::getNewSizeKeepAspectRatio(area->size(), newSize);

		// first, reset the zoom 
		zoomWithPercent("100%"); 
		zoomCombo->setCurrentIndex(4);

		// then resize the image and widget area.
		area->resizeImage(newSize);

		// rename the tab text.
		auto name = QFileInfo(area->getFileName()).fileName();
		if (name.isEmpty())
			name = "Untitled";
		auto tabName = name + " [" + QString::number(newSize.width()) + "x" + QString::number(newSize.height()) + "]";
		documentTab->tabBar()->setTabText(documentTab->currentIndex(), tabName);

		zoomWithPercent("Fit");
	}
}
void XrayPaintMainWindow::on_action_ToggleAntiAliasing_triggered()
{
	if (m_use_antialiasing)
		m_use_antialiasing = false;
	else
		m_use_antialiasing = true;

	if (getCurrentPaintArea())
		getCurrentPaintArea()->update();
}
void XrayPaintMainWindow::on_action_ToggleToolBar_triggered()
{
	if (shapesToolBar->isVisible())
		shapesToolBar->setHidden(true);
	else
		shapesToolBar->setVisible(true);
}
void XrayPaintMainWindow::on_action_About_triggered()
{
}
void XrayPaintMainWindow::on_documentTab_currentChanged(int newIndex)
{
	if (newIndex != -1)
	{
		auto area = getCurrentPaintArea();
		if (area)
		{
			shapeListView->setModel(area->getShapeListModel());
			disconnect(shapeListView->selectionModel(), &QItemSelectionModel::selectionChanged, area, &XrayMainPaintWidget::selectShape);
			connect(shapeListView->selectionModel(), &QItemSelectionModel::selectionChanged, area, &XrayMainPaintWidget::selectShape);
		}
		action_Reload->setEnabled(true);
		action_Close->setEnabled(true);
		action_CloseAll->setEnabled(true);

		action_Print->setEnabled(true);
		action_EditText->setEnabled(true);
		action_Caption->setEnabled(true);
		action_NumImages->setEnabled(true);

		zoomCombo->setEnabled(true);
		action_zoomIn->setEnabled(true);
		action_zoomOut->setEnabled(true);
		action_zoomOriginal->setEnabled(true);

		menu_Edit->setEnabled(true);
		menu_View->setEnabled(true);
		shapesToolBar->setEnabled(true);
	}
	else
	{
		action_Reload->setDisabled(true);
		action_Close->setDisabled(true);
		action_CloseAll->setDisabled(true);

		m_projectFilePath.clear();	// clear the project file in case of no picture files

		action_Print->setDisabled(true);
		action_EditText->setDisabled(true);
		action_Caption->setDisabled(true);
		action_NumImages->setDisabled(true);

		zoomCombo->setDisabled(true);
		action_zoomIn->setDisabled(true);
		action_zoomOut->setDisabled(true);
		action_zoomOriginal->setDisabled(true);

		menu_Edit->setDisabled(true);
		menu_View->setDisabled(true);
		shapesToolBar->setDisabled(true);
	}
}
void XrayPaintMainWindow::on_colorChanged(const QString& changed)
{
	auto area = getCurrentPaintArea();
	if (area)
	{
		auto shape = area->getSelectedShape();
		if (shape)
		{
			auto brush = shape->getBrush();
			auto pen = shape->getPen();
			auto color = pen.color();
			auto bcolor = brush.color();

			if (changed == "StrokeRed")
			{
				color.setRed(slider_strokecolor_red->value());
			}
			else if (changed == "StrokeGreen")
			{
				color.setGreen(slider_strokecolor_green->value());
			}
			else if (changed == "StrokeBlue")
			{
				color.setBlue(slider_strokecolor_blue->value());
			}
			else if (changed == "StrokeOpacity")
			{
				color.setAlpha(slider_stroke_opacity->value());
			}
			else if (changed == "StrokeColor")
			{
				color.setRed(slider_strokecolor_red->value());
				color.setGreen(slider_strokecolor_green->value());
				color.setBlue(slider_strokecolor_blue->value());
				color.setAlpha(slider_stroke_opacity->value());
			}
			else if (changed == "ButtonStroke")
			{
				color = QColorDialog::getColor(pen.color(), this, QApplication::translate("XrayPaintMainWindow", "Select Shape Color"));
				if (!color.isValid())
					return;

				color.setAlpha(pen.color().alpha());
				slider_strokecolor_red->setValue(color.red());
				slider_strokecolor_green->setValue(color.green());
				slider_strokecolor_blue->setValue(color.blue());
			}
			else if (changed == "FillRed")
			{
				bcolor.setRed(slider_fillcolor_red->value());
			}
			else if (changed == "FillGreen")
			{
				bcolor.setGreen(slider_fillcolor_green->value());
			}
			else if (changed == "FillBlue")
			{
				bcolor.setBlue(slider_fillcolor_blue->value());
			}
			else if (changed == "FillOpacity")
			{
				bcolor.setAlpha(slider_fill_opacity->value());
			}
			else if (changed == "FillColor")
			{
				bcolor.setRed(slider_fillcolor_red->value());
				bcolor.setGreen(slider_fillcolor_green->value());
				bcolor.setBlue(slider_fillcolor_blue->value());
				bcolor.setAlpha(slider_fill_opacity->value());
			}
			else if (changed == "ButtonFill")
			{
				bcolor = QColorDialog::getColor(brush.color(), this, QApplication::translate("XrayPaintMainWindow", "Select Fill Color"));
				if (!bcolor.isValid())
					return;

				bcolor.setAlpha(brush.color().alpha());
				slider_fillcolor_red->setValue(bcolor.red());
				slider_fillcolor_green->setValue(bcolor.green());
				slider_fillcolor_blue->setValue(bcolor.blue());
			}
			else if (changed == "StrokeWidth")
			{
				pen.setWidth(slider_stroke_width->value());
			}

			button_strokeColor->setStyleSheet(QString("QPushButton { background-color: %1 } QToolTip {""} ").arg(color.name()));
			button_fillColor->setStyleSheet(QString("QPushButton { background-color: %1 } QToolTip {""} ").arg(bcolor.name()));

			pen.setColor(color);
			brush.setColor(bcolor);

			shape->setPen(pen);
			shape->setBrush(brush);
			area->update();
			m_pen = pen;
			m_brush = brush;
		}
		else
		{
			QColor strokeColor(slider_strokecolor_red->value(), slider_strokecolor_green->value(), slider_strokecolor_blue->value(), slider_stroke_opacity->value());
			QColor fillColor(slider_fillcolor_red->value(), slider_fillcolor_green->value(), slider_fillcolor_blue->value(), slider_fill_opacity->value());
			button_strokeColor->setStyleSheet(QString("QPushButton { background-color: %1 } QToolTip {""} ").arg(strokeColor.name()));
			button_fillColor->setStyleSheet(QString("QPushButton { background-color: %1 } QToolTip {""} ").arg(fillColor.name()));
		}
	}
}
void XrayPaintMainWindow::update_mousePosition(const QPoint &mousePos)
{
	label_mousecoordinates->setText(QString("%1, %2px").arg(mousePos.x()).arg(mousePos.y()));
}
bool XrayPaintMainWindow::getUseAntialiasing() const
{
	return m_use_antialiasing;
}
QPen XrayPaintMainWindow::getPen() const
{
	return m_pen;
}
QBrush XrayPaintMainWindow::getBrush() const
{
	return m_brush;
}
PaintDrawModeType XrayPaintMainWindow::getMode() const
{
	return m_mode;
}
QFont XrayPaintMainWindow::getFont() const
{
	return m_font;
}
int XrayPaintMainWindow::getArrowDirection() const
{
	return m_arrow_dir;
}
int XrayPaintMainWindow::getArrowSize() const
{
	return m_arrow_size;
}
void XrayPaintMainWindow::closeEvent(QCloseEvent* _event)
{
	QMessageBox saveMessage;
	const auto save = saveMessage.addButton(QApplication::translate("XrayPaintMainWindow", "Save"), QMessageBox::AcceptRole);
	const auto close = saveMessage.addButton(QApplication::translate("XrayPaintMainWindow", "Close"), QMessageBox::DestructiveRole);
	saveMessage.addButton(QApplication::translate("XrayPaintMainWindow", "Cancel"), QMessageBox::RejectRole);
	int i = 0;

	while (i < documentTab->count())
	{
		const auto currentPaintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>("paintArea");

		if (currentPaintArea->getIsModified())
		{
			documentTab->setCurrentIndex(i);

			saveMessage.setText(QApplication::translate("XrayPaintMainWindow", "You have unsaved changes. Do you want to save them before you exit the application?"));
			saveMessage.setIcon(QMessageBox::Question);
			saveMessage.exec();
			if (saveMessage.clickedButton() == save)
			{
				on_action_saveFileAs_triggered();
				i = i + 1;
			}
			else if (saveMessage.clickedButton() == close)
			{
				documentTab->removeTab(documentTab->currentIndex());
			}
			else
			{
				_event->ignore();
				break;
			}
		}
		else
		{
			break;
		}
	}
}
void XrayPaintMainWindow::doubleClickEvent(QMouseEvent* e)
{
	if(e->button() == Qt::LeftButton)
		updateTextShapeText();
}
void XrayPaintMainWindow::customShapeChanged(const QVector<QPointF>& _points)
{
	p_voidFilter->previewCustomContour(_points);
}
//void XrayPaintMainWindow::wheelEvent(QWheelEvent* event)
//{
//	if (event->orientation() == Qt::Vertical && (event->modifiers() & Qt::AltModifier))
//	{
//		if (documentTab->count() > 0)
//		{
//			auto index = documentTab->currentIndex();
//
//			if (event->delta() > 0)
//			{
//				index++;
//				if (index >= documentTab->count())
//					index = documentTab->count() - 1;
//			}
//			else
//			{
//				index--;
//				if (index < 0)
//					index = 0;
//			}
//			documentTab->setCurrentIndex(index);
//		}
//
//		event->accept();
//	}
//	event->ignore();
//}
void XrayPaintMainWindow::activateTabFromTreeItem(const QString& itemText)
{
	for (auto i = 0; i < documentTab->count(); i++)
	{
		auto pw = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (pw)
		{
			if (pw->getFileName() == itemText)
			{
				auto b = documentTab->blockSignals(true);
				documentTab->setCurrentIndex(i);
				on_documentTab_currentChanged(i);
				documentTab->blockSignals(b);
				break;
			}
		}
	}

}
void XrayPaintMainWindow::activateTreeItemFromTab(int index)
{
	if (index < 0 || index >= documentTab->count())
		return;

	auto pw = documentTab->widget(index)->findChild<XrayMainPaintWidget*>();
	if (!pw)
		return;

	auto b = projTreeView->blockSignals(true);
	projTreeView->treeView()->setCurrentChildIndex(pw->getFileName());
	projTreeView->blockSignals(b);
}
void XrayPaintMainWindow::deleteTabFromTreeItem(const QString& itemText)
{
	for (auto i = 0; i < documentTab->count(); i++)
	{
		auto pw = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (pw)
		{
			if (pw->getFileName() == itemText)
			{
				auto b = documentTab->blockSignals(true);
				documentTab->deleteTab(i);
				documentTab->blockSignals(b);
				break;
			}
		}
	}

	on_documentTab_currentChanged(documentTab->currentIndex());
}
void XrayPaintMainWindow::deleteTreeItemFromTab(int index)
{
	if (index < 0 || index >= documentTab->count())
		return;

	auto pw = documentTab->widget(index)->findChild<XrayMainPaintWidget*>();
	if (!pw)
		return;

	auto b = projTreeView->blockSignals(true);
	projTreeView->treeView()->removeChild(pw->getFileName());
	activateTreeItemFromTab(documentTab->currentIndex());
	projTreeView->blockSignals(b);
}
void XrayPaintMainWindow::syncTabsWithTreeModel()
{
	if (documentTab->count() == 0)
		return;

	QStringList tabs;
	tabs.reserve(documentTab->count());
	for (auto i = 0; i < documentTab->count(); i++)
	{
		auto pw = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (pw) tabs.append(pw->getFileName());
	}

	auto treeItems = projTreeView->treeView()->childsDataText();

	int i, n, from, to;
	n = treeItems.count() - 1;
	for (i = n; i > -1; i--) 
	{
		auto el = treeItems[i];
		to = i;

		for (auto j = 0; j < tabs.size(); j++)
		{
			if (tabs[j] == el)
			{
				from = j;
				break;
			}
		}

		tabs.insert(to, tabs.takeAt(from)); // keep tabs list consistent
		auto b = documentTab->blockSignals(true);
		documentTab->tabBar()->moveTab(from, to);
		documentTab->blockSignals(b);
	}
}
void XrayPaintMainWindow::syncTreeModelWithTabs()
{
	for (auto i = 0; i < documentTab->count(); i++)
	{
		const auto paintArea = documentTab->widget(i)->findChild<XrayMainPaintWidget*>();
		if (paintArea)
		{
			// synch tree item data with the changed file locations
			projTreeView->treeView()->setChildDataMatchFileName(paintArea->getFileName());
		}
	}
}
/****************************************************************************

****************************************************************************/
XrayPaintTabWidget::XrayPaintTabWidget(QWidget* parent) :
	QTabWidget(parent)
{
	setTabsClosable(true);
	setMovable(true);
	setTabShape(QTabWidget::Rounded);
	tabBar()->setObjectName("centralTabBar");
	setStyleSheet("QTabBar::close-button { image: url(:/paint/images/delete_small_icon.png); }");
	connect(this, &QTabWidget::tabCloseRequested, this, &XrayPaintTabWidget::deleteTabWithWarning);
}
void XrayPaintTabWidget::deleteTabWithWarning(int index)
{
	if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, QApplication::applicationName(), QApplication::translate("XrayPaintTabWidget", "Are you sure you want to close?"), QMessageBox::Yes | QMessageBox::No).exec())
		deleteTab(index);
}
void XrayPaintTabWidget::deleteTab(int index)
{
	setCurrentIndex(index);
	emit tabAboutToClose(index);
	delete widget(index);
}
void XrayPaintTabWidget::deleteTab(const QString& tabText)
{
	for (auto i = 0; i < count(); i++)
	{
		if (this->tabText(i) == tabText)
		{
			deleteTab(i);
			break;
		}
	}
}
void XrayPaintMainWindow::setActiveTab(int _index) {

	// Jump to void analysis tab
	optionsTab->setCurrentIndex(_index);
	return;
}
