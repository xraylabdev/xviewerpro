/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XrayPaintFramelessMainWindow.cpp
** file base:	XrayPaintFramelessMainWindow
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a frame less window for the reporting
				wizard widget.
****************************************************************************/

#include "XrayPaintFramelessMainWindow.h"
#include "ui_XrayAboutDialog.h"
#include "XrayGlobal.h"
#include "XrayIconPushButton.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QMenu>
#include <QStyle>
#include <QAction>
#include <QActionGroup>
#include <QWidgetAction>
#include <QDesktopServices>
#include <QMessageBox>
#include <QDebug>

XRAYLAB_USING_NAMESPACE


XrayPaintFramelessMainWindow::XrayPaintFramelessMainWindow(const QString& _title, QWidget* _parent) :
	XrayFramelessWindowWin32(_title, _parent),
	p_mainWindow(new XrayPaintMainWindow(this))
{
	// set window icon as the application icon.
	setWindowIcon(QApplication::windowIcon());

	// create top right drop down menu.
	menuButton()->setVisible(false);	// disable the menu button
	setMenuBar(p_mainWindow->menuBar());

	// set the central widget.
	addCentralWidget(p_mainWindow);

	// set the window size at startup.
	restoreSettings();
	p_mainWindow->readSettings(*p_settings);
}
XrayPaintFramelessMainWindow::~XrayPaintFramelessMainWindow()
{
}
void XrayPaintFramelessMainWindow::closeEvent(QCloseEvent* _event)
{
	p_mainWindow->writeSettings(*p_settings);

	saveSettings();
	_event->accept();
}

void XrayPaintFramelessMainWindow::fitToViewportCurrentView()
{
	p_mainWindow->fitToViewportCurrentView();
}
void XrayPaintFramelessMainWindow::openProject(const QString& fileName)
{
	p_mainWindow->openProject(fileName);
}
bool XrayPaintFramelessMainWindow::openFile(const QString& fileName)
{
	return p_mainWindow->openFile(fileName);
}
void XrayPaintFramelessMainWindow::setEnhancerDirectoriesBoxVisible(bool _b)
{
	p_mainWindow->setEnhancerDirectoriesBoxVisible(_b);
}
void XrayPaintFramelessMainWindow::enableDemoVersion(const QString& _modules, bool _b)
{
	p_mainWindow->enableDemoVersion(_modules, _b);
}