/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/05/20
** filename: 	XrayFlashFilter.cpp
** file base:	XrayFlashFilter
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the GUI for contrast enhancement on 8,
16, 24, 32 bit images.
****************************************************************************/

#include "XrayFlashFilter.h"
#include "ui_XrayFlashFilter.h"

#include "XrayGlobal.h"
#include "XrayQtUtil.h"
#include "XrayCVUtil.h"
#include "XrayQFileUtil.h"

#include "XrayProgressWidget.h"
#include "XrayPaintMainWindow.h"

#include <QMessageBox>
#include <QInputDialog>
#include <QFileDialog>
#include <QDateTime>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayFlashFilter::XrayFlashFilter(QWidget *parent) :
	QWidget(parent),
	p_parent(parent),
	ui(new Ui::XrayFlashFilter)
{
	setWindowTitle("XENHANCER PRO");
	ui->setupUi(this);
	ui->writeTypeCombo->addItems(QStringList() << "jpg" << "jpeg" << "jpe" << "jp2" << "png" << "bmp" << "dib" << "tif" << "tiff" << "pgm" << "pbm" << "ppm" << "ras" << "sr" << "webp");
	ui->writeTypeCombo->setCurrentIndex(0);

	connect(ui->deltaSlider, &QSlider::valueChanged, this, &XrayFlashFilter::preview);
	connect(ui->sigmaSlider, &QSlider::valueChanged, this, &XrayFlashFilter::preview);
	connect(ui->clippingSlider, &QSlider::valueChanged, this, &XrayFlashFilter::preview);
	connect(ui->exposureSlider, &QSlider::valueChanged, this, &XrayFlashFilter::preview);
	connect(ui->gammaSlider, &QSlider::valueChanged, this, &XrayFlashFilter::preview);
	connect(ui->optimizeCheckBox, &QCheckBox::toggled, this, &XrayFlashFilter::preview);

	loadPresets();
	connect(ui->presetComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &XrayFlashFilter::selectPreset);
	connect(ui->presetUpdateBtn, &QPushButton::clicked, this, &XrayFlashFilter::updateWithCurrentPreset);
	connect(ui->presetSaveBtn, &QPushButton::clicked, this, &XrayFlashFilter::savePreset);
	connect(ui->presetRemoveBtn, &QPushButton::clicked, this, &XrayFlashFilter::removePreset);

	connect(ui->applyBtn, &QPushButton::clicked, this, &XrayFlashFilter::apply);

	connect(ui->readBrowse, &QPushButton::clicked, [this]()
	{
		auto dir = QFileDialog::getExistingDirectory(this, QApplication::translate("XrayFlashFilter", "Open Directory"), XrayGlobal::getLastFileOpenPath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
		if (dir.isEmpty())
			return;
		QDir directory(dir);
		if (!directory.exists())
			return;
		ui->readLineEdit->setText(directory.absolutePath());
		XrayGlobal::setLastFileOpenPath(directory.absolutePath());
	});

	connect(ui->writeBrowse, &QPushButton::clicked, [this]()
	{
		auto dir = QFileDialog::getExistingDirectory(this, QApplication::translate("XrayFlashFilter", "Open Directory"), XrayGlobal::getLastFileOpenPath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
		if (dir.isEmpty())
			return;
		QDir directory(dir);
		if (!directory.exists())
			return;
		ui->writeLineEdit->setText(directory.absolutePath());
		XrayGlobal::setLastFileOpenPath(directory.absolutePath());
	});
	connect(ui->processBtn, &QPushButton::clicked, this, &XrayFlashFilter::process);


	connect(ui->rangeCheckBox, &QPushButton::toggled, this, &XrayFlashFilter::startIntensityRange);
	connect(ui->windowSlider, &QSlider::valueChanged, this, &XrayFlashFilter::adjustIntensityRange);
	connect(ui->levelSlider, &QSlider::valueChanged, this, &XrayFlashFilter::adjustIntensityRange);
	ui->histogrmWidget->setBorderEnabled(false);
	setRangeSlidersEnabled(false);
	ui->histogrmWidget->setColor(QColor(52, 52, 52), QColor(52, 52, 52));
}
XrayFlashFilter::~XrayFlashFilter()
{
	delete ui;
}
QGroupBox* XrayFlashFilter::getDirectoriesBox() const
{
	return ui->directoriesGroupBox;
}
QPushButton* XrayFlashFilter::getProcessBtn() const
{
	return ui->processBtn;
}
void XrayFlashFilter::writeSettings(QSettings& _settings)
{
	_settings.beginGroup("XEnhancerPro");

	_settings.setValue("IntensityRange/Window", ui->windowSlider->value());
	_settings.setValue("IntensityRange/Level", ui->levelSlider->value());

	_settings.setValue("Parameters/Delta", ui->deltaSlider->value());
	_settings.setValue("Parameters/Sigma", ui->sigmaSlider->value());
	_settings.setValue("Parameters/Clipping", ui->clippingSlider->value());
	_settings.setValue("Parameters/Exposure", ui->exposureSlider->value());
	_settings.setValue("Parameters/Gamma", ui->gammaSlider->value());
	_settings.setValue("Parameters/Optimize", ui->optimizeCheckBox->isChecked());
	_settings.setValue("Directory/Read", ui->readLineEdit->text());
	_settings.setValue("Directory/Write", ui->writeLineEdit->text());
	_settings.setValue("Directory/WriteType", ui->writeTypeCombo->currentIndex());

	_settings.endGroup();
}
void XrayFlashFilter::readSettings(QSettings& _settings)
{
	_settings.beginGroup("XEnhancerPro");

	ui->windowSlider->setValue(_settings.value("IntensityRange/Window", ui->windowSlider->value()).toInt());
	ui->levelSlider->setValue(_settings.value("IntensityRange/Level", ui->levelSlider->value()).toInt());

	ui->deltaSlider->setValue(_settings.value("Parameters/Delta", ui->deltaSlider->value()).toInt());
	ui->sigmaSlider->setValue(_settings.value("Parameters/Sigma", ui->sigmaSlider->value()).toInt());
	ui->clippingSlider->setValue(_settings.value("Parameters/Clipping", ui->clippingSlider->value()).toInt());
	ui->exposureSlider->setValue(_settings.value("Parameters/Exposure", ui->exposureSlider->value()).toInt());
	ui->gammaSlider->setValue(_settings.value("Parameters/Gamma", ui->gammaSlider->value()).toInt());
	ui->optimizeCheckBox->setChecked(_settings.value("Parameters/Optimize", ui->optimizeCheckBox->isChecked()).toBool());
	ui->readLineEdit->setText(_settings.value("Directory/Read", ui->readLineEdit->text()).toString());
	ui->writeLineEdit->setText(_settings.value("Directory/Write", ui->writeLineEdit->text()).toString());
	ui->writeTypeCombo->setCurrentIndex(_settings.value("Directory/WriteType", ui->writeTypeCombo->currentIndex()).toInt());

	_settings.endGroup();
}
void XrayFlashFilter::setRangeSlidersMinMax(int _min, int _max)
{
	auto b = ui->windowSlider->blockSignals(true);
	ui->windowSlider->setMinimum(_min);
	ui->windowSlider->setMaximum(_max);
	ui->windowSlider->blockSignals(b);

	b = ui->windowSpinBox->blockSignals(true);
	ui->windowSpinBox->setMinimum(_min);
	ui->windowSpinBox->setMaximum(ui->windowSlider->maximum());
	ui->windowSpinBox->blockSignals(b);

	b = ui->levelSlider->blockSignals(true);
	ui->levelSlider->setMinimum(_min);
	ui->levelSlider->setMaximum(_max);
	ui->levelSlider->blockSignals(b);

	b = ui->levelSpinBox->blockSignals(true);
	ui->levelSpinBox->setMinimum(_min);
	ui->levelSpinBox->setMaximum(ui->levelSlider->maximum());
	ui->levelSpinBox->blockSignals(b);
}
void XrayFlashFilter::setRangeSlidersEnabled(bool b)
{
	ui->windowSlider->setEnabled(b);
	ui->windowSpinBox->setEnabled(b);
	ui->levelSlider->setEnabled(b);
	ui->levelSpinBox->setEnabled(b);
	if(b) ui->histogrmWidget->setColor(QColor(0, 0, 0), QColor(220, 220, 220));
	else ui->histogrmWidget->clear(QColor(52, 52, 52));
}
void XrayFlashFilter::startIntensityRange(bool value)
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	if (value)
	{
		cv::Mat src, dst;

		// get the 16 bit image
		auto& mat = area->getImage16();
		if (!mat.empty())
		{
			// convert to 8 bit and unform the intensity distribution
			double min, max;
			cv::minMaxLoc(mat, &min, &max);
			mat.convertTo(dst, CV_8UC1, 255.0 / (max - min), -min * 255.0 / (max - min));

			setRangeSlidersMinMax(1, 65535);
		}
		else
		{
			src = XrayCVUtil::toMat(area->getOriginalImage(), false);
			if (src.channels() > 1)
				cv::cvtColor(src, dst, cv::COLOR_BGR2GRAY);
			else
				dst = src;

			setRangeSlidersMinMax(1, 255);
		}

		// calculate histogram
		int binSize = 256;
		float range[] = { 0, 255 };
		const float* ranges[] = { range };
		cv::MatND hist;
		cv::calcHist(&dst, 1, 0, cv::Mat(), hist, 1, &binSize, ranges, true, false);

		double min, max;
		cv::minMaxLoc(hist, &min, &max);

		QVector<qreal> histogram;
		histogram.resize(binSize);
		for (int i = 0; i < binSize; i++)
			histogram[i] = hist.at<float>(i) / max;
		ui->histogrmWidget->setHistogram(histogram);
		setRangeSlidersEnabled(true);
	}
	else
	{
		setRangeSlidersEnabled(false);
	}
}
void XrayFlashFilter::adjustIntensityRange()
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	if (ui->windowSlider->value() >= ui->levelSlider->value())
	{
		auto b = ui->windowSlider->blockSignals(true);
		ui->windowSlider->setValue(ui->levelSlider->value() - 1);
		ui->windowSlider->blockSignals(b);

		b = ui->windowSpinBox->blockSignals(true);
		ui->windowSpinBox->setValue(ui->windowSlider->value());
		ui->windowSpinBox->blockSignals(b);
	}

	auto& mat = area->getImage16();
	if (!mat.empty())
	{
		auto dst = cv::Mat(mat.size(), mat.type());
		XrayCVUtil::applyWindowLevel16(mat, dst, ui->windowSlider->value(), 0, ui->levelSlider->value(), 65535);

		double min, max;
		cv::minMaxLoc(dst, &min, &max);
		dst.convertTo(dst, CV_8UC1, 255.0 / (max - min));

		area->setImage(XrayCVUtil::toQImage(dst));
		area->setCurrentImageAsOriginalImage();
	}
	else
	{
		auto src = XrayCVUtil::toMat(area->getOriginalImage(), false);
		auto dst = cv::Mat(src.size(), src.type());
		XrayCVUtil::applyWindowLevel(src, dst, ui->windowSlider->value(), 0, ui->levelSlider->value(), 255);

		area->setImage(XrayCVUtil::toQImage(dst));
	}

	area->update();
}
void XrayFlashFilter::setParametersToSliders(R3D::fvkContrastEnhancerUtil::Params _params)
{
	auto b = ui->deltaSlider->blockSignals(true);
	ui->deltaSlider->setValue(_params.Delta);
	ui->deltaSlider->blockSignals(b);

	b = ui->sigmaSlider->blockSignals(true);
	ui->sigmaSlider->setValue(_params.Sigma);
	ui->sigmaSlider->blockSignals(b);

	b = ui->clippingSlider->blockSignals(true);
	ui->clippingSlider->setValue(_params.Clipping);
	ui->clippingSlider->blockSignals(b);

	b = ui->exposureSlider->blockSignals(true);
	ui->exposureSlider->setValue(_params.Exposure);
	ui->exposureSlider->blockSignals(b);

	b = ui->gammaSlider->blockSignals(true);
	ui->gammaSlider->setValue(_params.Gamma);
	ui->gammaSlider->blockSignals(b);

	b = ui->deltaSpinBox->blockSignals(true);
	ui->deltaSpinBox->setValue(_params.Delta);
	ui->deltaSpinBox->blockSignals(b);

	b = ui->sigmaSpinBox->blockSignals(true);
	ui->sigmaSpinBox->setValue(_params.Sigma);
	ui->sigmaSpinBox->blockSignals(b);

	b = ui->clippingSpinBox->blockSignals(true);
	ui->clippingSpinBox->setValue(_params.Clipping);
	ui->clippingSpinBox->blockSignals(b);

	b = ui->exposureSpinBox->blockSignals(true);
	ui->exposureSpinBox->setValue(_params.Exposure);
	ui->exposureSpinBox->blockSignals(b);

	b = ui->gammaSpinBox->blockSignals(true);
	ui->gammaSpinBox->setValue(_params.Gamma);
	ui->gammaSpinBox->blockSignals(b);

	b = ui->optimizeCheckBox->blockSignals(true);
	ui->optimizeCheckBox->setChecked(_params.Optimize == 1 ? true : false);
	ui->optimizeCheckBox->blockSignals(b);
}
R3D::fvkContrastEnhancerUtil::Params XrayFlashFilter::getParametersFromSliders()
{
	R3D::fvkContrastEnhancerUtil::Params params;
	params.Delta = ui->deltaSlider->value();
	params.Sigma = ui->sigmaSlider->value();
	params.Clipping = ui->clippingSlider->value();
	params.Exposure = ui->exposureSlider->value();
	params.Gamma = ui->gammaSlider->value();
	params.Optimize = ui->optimizeCheckBox->isChecked() ? 1 : 0;
	return params;
}
void XrayFlashFilter::reset()
{
	auto b = ui->deltaSlider->blockSignals(true);
	ui->deltaSlider->setValue(40);
	ui->deltaSpinBox->setValue(40);
	ui->deltaSlider->blockSignals(b);

	b = ui->sigmaSlider->blockSignals(true);
	ui->sigmaSlider->setValue(20);
	ui->sigmaSpinBox->setValue(20);
	ui->sigmaSlider->blockSignals(b);

	b = ui->clippingSlider->blockSignals(true);
	ui->clippingSlider->setValue(0);
	ui->clippingSpinBox->setValue(0);
	ui->clippingSlider->blockSignals(b);

	b = ui->exposureSlider->blockSignals(true);
	ui->exposureSlider->setValue(70);
	ui->exposureSpinBox->setValue(70);
	ui->exposureSlider->blockSignals(b);

	b = ui->gammaSlider->blockSignals(true);
	ui->gammaSlider->setValue(0);
	ui->gammaSpinBox->setValue(0);
	ui->gammaSlider->blockSignals(b);

	b = ui->optimizeCheckBox->blockSignals(true);
	ui->optimizeCheckBox->setChecked(true);
	ui->optimizeCheckBox->blockSignals(b);

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	area->resetImage();
}
void XrayFlashFilter::process()
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto b = ui->readLineEdit->blockSignals(true);
	const auto dir = ui->readLineEdit->text();
	ui->readLineEdit->blockSignals(b);

	b = ui->writeLineEdit->blockSignals(true);
	const auto wdir = ui->writeLineEdit->text();
	ui->writeLineEdit->blockSignals(b);

	b = ui->writeTypeCombo->blockSignals(true);
	const auto type = ui->writeTypeCombo->currentText();
	ui->writeTypeCombo->blockSignals(b);

	QDir directory(dir);
	if (!directory.exists())
	{
		QMessageBox::critical(nullptr, "XEnhancer", QString("Couldn't find the specified directory ") + qPrintable(QDir(dir).absolutePath()));
		return;
	}

	auto fileNames = directory.entryList(XrayGlobal::getAllSupportedImageExtensions(), QDir::Files);
	if (fileNames.empty())
	{
		QMessageBox::critical(nullptr, "XEnhancer", QString("Couldn't find any image file at the specified directory ") + qPrintable(QDir(dir).absolutePath()));
		return;
	}

	auto path = dir;
	if (!dir.endsWith('/'))
		path = dir + '/';

	auto wpath = wdir;
	if (!wdir.endsWith('/'))
		wpath = wdir + '/';

	auto folder = wpath + QString("XEnhancer ") + QDateTime::currentDateTime().toString("dd-MM-yyyy hh-mm-ss");

	if (!QDir().mkpath(folder))
	{
		QMessageBox::critical(nullptr, "XEnhancer", QString("Failed to create a directory at ") + qPrintable(QDir(wpath).absolutePath()));
		return;
	}

	QStringList files;
	foreach(QString filename, fileNames)
		files.append(path + filename);

	auto n = 0;
	for (auto i = 0; i < files.size(); i++)
	{
		const auto name = QFileInfo(files[i]).baseName();
		auto img = cv::imread(files[i].toLatin1().toStdString(), cv::IMREAD_UNCHANGED);
		if (img.empty())
		{
			xAppWarn("Couldn't read the image ({})!", name);
			continue;
		}

		// check the depth of the imported image and convert to 8 bit image.
		auto src(img);
		if (src.depth() == CV_16U || src.depth() == CV_16S || src.depth() == CV_16F)
		{
			if (ui->rangeCheckBox->isChecked())
			{
				cv::normalize(src, src, 0, 65535, cv::NORM_MINMAX);
				XrayCVUtil::applyWindowLevel16(src, src, ui->windowSlider->value(), 0, ui->levelSlider->value(), 65535);
			}

			double min, max;
			cv::minMaxLoc(src, &min, &max);
			src.convertTo(src, CV_8UC1, 255.0 / (max - min), -min * 255.0 / (max - min));
		}

		// perform the contrast enhancer filter.
		auto m = getFilteredImage(src);

		// save the enhanced image to the disk.
		std::vector<int> compression_params;
		compression_params.push_back(cv::IMWRITE_PNG_COMPRESSION);
		compression_params.push_back(5);
		if (cv::imwrite((folder + '/' + name + "." + type).toStdString(), m, compression_params))
		{
			xAppInfo("Saved: {}", name + "." + type);
			n++;
		}

		if (n > 0)
		{
			xAppInfo("Current progress: {}%", int(1.0 / (files.size() / static_cast<double>(n)) * 100.0));
		}
	}

	if (n == 0)
		QMessageBox::critical(nullptr, "XEnhancer", QString("Couldn't write any file at the specified directory ") + qPrintable(QDir(folder).absolutePath()));
	else
		QMessageBox::information(nullptr, "XEnhancer", QString::number(n) + QString(" images have been filtered and saved successfully!"));
}
cv::Mat XrayFlashFilter::getFilteredImage(const cv::Mat& _mat)
{
	cv::Mat dest(_mat.size(), _mat.type());
	R3D::fvkContrastEnhancerUtil::performContrastEnhancerFilter(_mat, dest, getParametersFromSliders());
	return dest;

}
void XrayFlashFilter::preview()
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	if (area->getOriginalImage().isNull())
		return;

	auto mat = XrayCVUtil::toMat(area->getOriginalImage(), false);
	
	auto dest = getFilteredImage(mat);
	area->setImage(XrayCVUtil::toQImage(dest));
	area->update();
}
void XrayFlashFilter::apply()
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	if (area->getImage().isNull())
		return;

	area->setCurrentImageAsOriginalImage();
}
static QString getDefaultPresets()
{
	return "[XEnhancerPresets]\n"
		"Preset\\size=3\n"
		"Preset\\1\\Name=1\n"
		"Preset\\1\\Delta=40\n"
		"Preset\\1\\Sigma=20\n"
		"Preset\\1\\Clipping=0\n"
		"Preset\\1\\Exposure=70\n"
		"Preset\\1\\Gamma=0\n"
		"Preset\\1\\Optimize=1\n"
		"Preset\\2\\Name=2\n"
		"Preset\\2\\Delta=20\n"
		"Preset\\2\\Sigma=20\n"
		"Preset\\2\\Clipping=0\n"
		"Preset\\2\\Exposure=60\n"
		"Preset\\2\\Gamma=0\n"
		"Preset\\2\\Optimize=1\n"
		"Preset\\3\\Name=3\n"
		"Preset\\3\\Delta=15\n"
		"Preset\\3\\Sigma=15\n"
		"Preset\\3\\Clipping=0\n"
		"Preset\\3\\Exposure=60\n"
		"Preset\\3\\Gamma=10\n"
		"Preset\\3\\Optimize=1\n";
}
void XrayFlashFilter::loadPresets()
{
	// get the presets file path
	auto docPath = XrayQFileUtil::getStandardPath(QApplication::applicationName(), QStandardPaths::DocumentsLocation);
	auto filePath = XrayQFileUtil::getStandardPath("presets", docPath) + "xenhancer_presets.dat";
	if (!QFile::exists(filePath))	// if file doesn't exists then create with default presets
		XrayQFileUtil::write(getDefaultPresets(), filePath, XrayQFileUtil::Text, QFile::WriteOnly);

	// first clear all presets
	m_presets.clear();
	ui->presetComboBox->clear();

	// load presets from file
	QSettings settings(filePath, QSettings::Format::IniFormat);

	settings.beginGroup("XEnhancerPresets");
	auto totalPresets = settings.beginReadArray("Preset");

	for (auto i = 0; i < totalPresets; i++)
	{
		settings.setArrayIndex(i);

		R3D::fvkContrastEnhancerUtil::Params params;
		params.Delta = settings.value("Delta", 40).toInt();
		params.Sigma = settings.value("Sigma", 20).toInt();
		params.Clipping = settings.value("Clipping", 0).toInt();
		params.Exposure = settings.value("Exposure", 70).toInt();
		params.Gamma = settings.value("Gamma", 0).toInt();
		params.Optimize = settings.value("Optimize", 1).toInt();

		m_presets.append(params);
		ui->presetComboBox->addItem(settings.value("Name", QString::number(i)).toString());
	}

	settings.endArray();
	settings.endGroup();
}
void XrayFlashFilter::savePresets()
{
	auto docPath = XrayQFileUtil::getStandardPath(QApplication::applicationName(), QStandardPaths::DocumentsLocation);
	auto filePath = XrayQFileUtil::getStandardPath("presets", docPath) + "xenhancer_presets.dat";

	QSettings settings(filePath, QSettings::Format::IniFormat);
	settings.clear();	// first, clear the previous settings

	settings.beginGroup("XEnhancerPresets");
	settings.beginWriteArray("Preset");

	for (auto i = 0; i < m_presets.size(); i++)
	{
		settings.setArrayIndex(i);

		auto params = m_presets.at(i);
		settings.setValue("Name", ui->presetComboBox->itemText(i));
		settings.setValue("Delta", static_cast<double>(params.Delta));
		settings.setValue("Sigma", static_cast<double>(params.Sigma));
		settings.setValue("Clipping", static_cast<double>(params.Clipping));
		settings.setValue("Exposure", static_cast<double>(params.Exposure));
		settings.setValue("Gamma", static_cast<double>(params.Gamma));
		settings.setValue("Optimize", static_cast<double>(params.Optimize));
	}

	settings.endArray();
	settings.endGroup();
}
void XrayFlashFilter::savePreset()
{
	QString lastPresetName = "Preset 0";
	if (ui->presetComboBox->count() > 0)
		lastPresetName = ui->presetComboBox->itemText(ui->presetComboBox->count() - 1);

	bool ok;
	auto text = QInputDialog::getText(this, QApplication::translate("XrayFlashFilter", "Enter New Preset Name"),
		QApplication::translate("XrayFlashFilter", "Enter Text"), QLineEdit::Normal, lastPresetName, &ok);
	if (ok && !text.isEmpty())
	{
		// append to the list and then write to file.
		m_presets.append(getParametersFromSliders());

		// add this preset to the combo box and set as current
		ui->presetComboBox->addItem(text);
		auto b = ui->presetComboBox->blockSignals(true);
		ui->presetComboBox->setCurrentText(text);
		ui->presetComboBox->blockSignals(b);

		// save all presets to file
		savePresets();

		// preview to update voids
		preview();
	}
}
void XrayFlashFilter::removePreset()
{
	auto index = ui->presetComboBox->currentIndex();
	if (index >= 0 && index < m_presets.size())
	{
		m_presets.removeAt(index);
		ui->presetComboBox->removeItem(index);
		savePresets();
	}
}
void XrayFlashFilter::selectPreset(int _index)
{
	if (_index >= 0 && _index < m_presets.size())
	{
		setParametersToSliders(m_presets.at(_index));
		preview();
	}
}
void XrayFlashFilter::updateWithCurrentPreset()
{
	selectPreset(ui->presetComboBox->currentIndex());
}
