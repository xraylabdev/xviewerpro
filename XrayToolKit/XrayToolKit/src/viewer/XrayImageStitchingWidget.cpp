/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/11/22
** filename: 	XrayImageStitchingWidget.cpp
** file base:	XrayImageStitchingWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the image stitching widget with UI.
****************************************************************************/

#include "XrayImageStitchingWidget.h"
#include "ui_XrayImageStitchingWidget.h"
#include "XrayImageCropWidget.h"
#include "XrayGlobal.h"
#include "XrayCVUtil.h"
#include "XrayQtUtil.h"
#include "XrayQFileUtil.h"

#include <QApplication>
#include <QFileDialog>
#include <QInputDialog>
#include <QButtonGroup>
#include <QScrollBar>
#include <QTimer>
#include <QDialogButtonBox>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayStitchPaintWidget::XrayStitchPaintWidget(QWidget* _parent) :
	XrayMainPaintWidget(_parent)
{
	auto rectShape = new XrayPaintRectangleShape();
	rectShape->setName("Rect 0");
	rectShape->setRect(QRect(-4, -4, 2, 2));
	rectShape->setBrush(QBrush(QColor(249, 0, 0, 200), Qt::SolidPattern));
	rectShape->setPen(QPen(QBrush(QColor(249, 0, 0, 200), Qt::SolidPattern), 1.0, Qt::SolidLine, Qt::FlatCap));
	addShapeInProgress(rectShape);

	connect(this, &XrayMainPaintWidget::mouseLeftClicked, [this, rectShape](const QPoint& pos)
	{
		if (m_image.isNull())
			return;

		rectShape->setRect(QRect(pos.x() - 1, pos.y() - 1, 2, 2));
		update();
	});
}

XrayImageStitchingWidget::XrayImageStitchingWidget(QWidget* _parent) :
	QWidget(_parent),
	ui(new Ui::XrayImageStitchingWidget),
	m_currentIndex(-1),
	m_demoVersion(true)
{
	ui->setupUi(this);
	setWindowTitle("Stitch Images");

	leftPaint = new XrayStitchPaintWidget(_parent);
	connect(leftPaint, &XrayStitchPaintWidget::mouseLeftClicked, [this](const QPoint& pos)
	{
		if (leftPaint->getImage().isNull())
			return;

		auto pixel = QString::number(pos.x()) + ", " + QString::number(pos.y());
		ui->selectedPixelLabelL->setText(QApplication::translate("XrayImageStitchingWidget", "Selected Pixel: %1").arg(pixel));
		emit statusBarMessage(ui->selectedPixelLabelL->text(), 1000);
	});


	ui->scrollAreaL->setAlignment(Qt::AlignCenter);
	ui->scrollAreaL->setWidget(leftPaint);

	rightPaint = new XrayStitchPaintWidget(_parent);
	connect(rightPaint, &XrayStitchPaintWidget::mouseLeftClicked, [this](const QPoint& pos)
	{
		if (rightPaint->getImage().isNull())
			return;

		auto pixel = QString::number(pos.x()) + ", " + QString::number(pos.y());
		ui->selectedPixelLabelR->setText(QApplication::translate("XrayImageStitchingWidget", "Selected Pixel: %1").arg(pixel));
		emit statusBarMessage(ui->selectedPixelLabelR->text(), 1000);
	});
	ui->scrollAreaR->setAlignment(Qt::AlignCenter);
	ui->scrollAreaR->setWidget(rightPaint);

	connect(ui->browseButtonImages, &QPushButton::clicked, this, &XrayImageStitchingWidget::browseImages);
	connect(ui->browseButtonL, &QPushButton::clicked, this, &XrayImageStitchingWidget::browseLeftImage);
	connect(ui->browseButtonR, &QPushButton::clicked, this, &XrayImageStitchingWidget::browseRightImage);
	connect(ui->applyBtn, &QPushButton::clicked, this, &XrayImageStitchingWidget::apply);
	connect(ui->saveBtn, &QPushButton::clicked, this, &XrayImageStitchingWidget::save);
	connect(ui->xpaintBtn, &QPushButton::clicked, this, &XrayImageStitchingWidget::editInXPaint);

	connect(ui->repeatButton, &QPushButton::clicked, this, &XrayImageStitchingWidget::repeat);
	connect(ui->prevButton, &QPushButton::clicked, this, &XrayImageStitchingWidget::prev);
	connect(ui->nextButton, &QPushButton::clicked, this, &XrayImageStitchingWidget::next);

	createToolButtonsActions(leftPaint, ui->zoomInBtnL, ui->zoomOutBtnL, ui->flipBtnL, ui->rotateBtnL);
	createToolButtonsActions(rightPaint, ui->zoomInBtnR, ui->zoomOutBtnR, ui->flipBtnR, ui->rotateBtnR);
}

void XrayImageStitchingWidget::createToolButtonsActions(XrayMainPaintWidget* paint, QToolButton* zoomIn, QToolButton* zoomOut, QToolButton* flip, QToolButton* rotate)
{
	auto action_zoomIn = new QAction(QIcon(":/paint/images/zoom_in_blue_icon.png"), QApplication::translate("XrayImageStitchingWidget", "Zoom in"), this);
	connect(action_zoomIn, &QAction::triggered, [paint]() { paint->zoomIn(); });
	zoomIn->setDefaultAction(action_zoomIn);
	zoomIn->setIcon(action_zoomIn->icon());

	auto action_zoomOut = new QAction(QIcon(":/paint/images/zoom_out_blue_icon.png"), QApplication::translate("XrayImageStitchingWidget", "Zoom out"), this);
	connect(action_zoomOut, &QAction::triggered, [paint]() { paint->zoomOut(); });
	zoomOut->setDefaultAction(action_zoomOut);
	zoomOut->setIcon(action_zoomOut->icon());

	auto action_FlipHor = new QAction(QIcon(""), QApplication::translate("XrayImageStitchingWidget", "Horizontal"), this);
	connect(action_FlipHor, &QAction::triggered, [paint]() { paint->flip(0); });
	auto action_FlipVert = new QAction(QIcon(""), QApplication::translate("XrayImageStitchingWidget", "Vertical"), this);
	connect(action_FlipVert, &QAction::triggered, [paint]() { paint->flip(1); });
	auto action_FlipBoth = new QAction(QIcon(""), QApplication::translate("XrayImageStitchingWidget", "Both"), this);
	connect(action_FlipBoth, &QAction::triggered, [paint]() { paint->flip(-1); });
	auto flipMenu = new QMenu;
	flipMenu->addAction(action_FlipHor);
	flipMenu->addAction(action_FlipVert);
	flipMenu->addAction(action_FlipBoth);
	flip->setIcon(QIcon(":/paint/images/flip_blue_icon.png"));
	flip->setMenu(flipMenu);

	auto action_Rotate180 = new QAction(QIcon(""), QString::fromLatin1("180�"), this);
	connect(action_Rotate180, &QAction::triggered, [paint]() { paint->rotate(180.0); });
	auto action_Rotate90 = new QAction(QIcon(""), QString::fromLatin1("90�") + ' ' + QApplication::translate("XrayImageStitchingWidget", "Clockwise"), this);
	connect(action_Rotate90, &QAction::triggered, [paint]() { paint->rotate(270.0); });
	auto action_RotateN90 = new QAction(QIcon(""), QString::fromLatin1("90�") + ' ' + QApplication::translate("XrayImageStitchingWidget", "Counter clockwise"), this);
	connect(action_RotateN90, &QAction::triggered, [paint]() { paint->rotate(90.0); });
	auto action_arbitrary = new QAction(QIcon(""), QApplication::translate("XrayImageStitchingWidget", "Arbitrary..."), this);
	connect(action_arbitrary, &QAction::triggered, [this, paint]()
	{
			bool ok;
			auto n = QInputDialog::getDouble(this, QApplication::translate("XrayImageStitchingWidget", "Specify the rotation angle"),
				QApplication::translate("XrayImageStitchingWidget", "Angle (deg)"), 90.0, -180.0, 180.0, 2, &ok);
			if (ok) paint->rotate(n);
	});

	auto rotateMenu = new QMenu;
	rotateMenu->addAction(action_Rotate180);
	rotateMenu->addAction(action_Rotate90);
	rotateMenu->addAction(action_RotateN90);
	rotateMenu->addSeparator();
	rotateMenu->addAction(action_arbitrary);
	rotate->setIcon(QIcon(":/paint/images/rotate_blue_icon.png"));
	rotate->setMenu(rotateMenu);
}
void XrayImageStitchingWidget::browseImages()
{
	auto fileNames = QFileDialog::getOpenFileNames(this, QApplication::translate("XrayImageStitchingWidget", "Select Files"), XrayGlobal::getLastFileOpenPath(), XrayGlobal::getAllSupportedImageExtensions(QApplication::translate("XrayImageStitchingWidget", "Image Files"), { "*.dcm" }), nullptr);
	if (fileNames.isEmpty())
		return;
	
	if (fileNames.size() < 2)
	{
		XrayGlobal::setLastFileOpenPath(fileNames);
		emit statusBarMessage(QApplication::translate("XrayImageStitchingWidget", "Please select at least two images!"), 15000);
		return;
	}

	if (m_demoVersion)
	{
		XrayGlobal::setLastFileOpenPath(fileNames);
		emit statusBarMessage(QApplication::translate("XrayImageStitchingWidget", "Demo version does not allow to work with multiple images!"), 360000);
		return;
	}

	leftPaint->load(fileNames.at(0));
	rightPaint->load(fileNames.at(1));
	m_fileList = fileNames;
	m_currentIndex = 2;

	ui->selectedPixelLabelL->setText("");
	ui->selectedPixelLabelR->setText("");
	ui->groupBoxL->setTitle(QApplication::translate("XrayImageStitchingWidget", "Left Image - ") + QFileInfo(leftPaint->getFileName()).baseName() + " (" + XrayQFileUtil::getHumanReadableFileSize(leftPaint->getFileName()) + ")");
	ui->groupBoxR->setTitle(QApplication::translate("XrayImageStitchingWidget", "Right Image - ") + QFileInfo(rightPaint->getFileName()).baseName() + " (" + XrayQFileUtil::getHumanReadableFileSize(rightPaint->getFileName()) + ")");
	ui->scrollAreaL->horizontalScrollBar()->triggerAction(QAbstractSlider::SliderToMaximum);	// move scroll to most right side

	XrayGlobal::setLastFileOpenPath(fileNames);

	emit statusBarMessage(QApplication::translate("XrayImageStitchingWidget", "Image has been loaded!"), 15000);
}

void XrayImageStitchingWidget::browseLeftImage()
{
	auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayImageStitchingWidget", "Select File"), XrayGlobal::getLastFileOpenPath(), XrayGlobal::getAllSupportedImageExtensions(QApplication::translate("XrayImageStitchingWidget", "Image Files"), { "*.dcm" }), nullptr);
	if (fileName.isEmpty())
		return;

	XrayGlobal::setLastFileOpenPath(fileName);

	if (leftPaint->load(fileName))
	{
		m_fileList.clear();
		m_currentIndex = -1;

		ui->selectedPixelLabelL->setText("");
		ui->selectedPixelLabelR->setText("");
		ui->groupBoxL->setTitle(QApplication::translate("XrayImageStitchingWidget", "Left Image - ") + QFileInfo(leftPaint->getFileName()).baseName() + " (" + XrayQFileUtil::getHumanReadableFileSize(leftPaint->getFileName()) + ")");
		ui->scrollAreaL->horizontalScrollBar()->triggerAction(QAbstractSlider::SliderToMaximum);	// move scroll to most right side

		emit statusBarMessage(QApplication::translate("XrayImageStitchingWidget", "Image has been loaded!"), 15000);
	}
}

void XrayImageStitchingWidget::browseRightImage()
{
	auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayImageStitchingWidget", "Select File"), XrayGlobal::getLastFileOpenPath(), XrayGlobal::getAllSupportedImageExtensions(QApplication::translate("XrayImageStitchingWidget", "Image Files"), { "*.dcm" }), nullptr);
	if (fileName.isEmpty())
		return;

	XrayGlobal::setLastFileOpenPath(fileName);

	if (rightPaint->load(fileName))
	{
		m_fileList.clear();
		m_currentIndex = -1;

		ui->selectedPixelLabelL->setText("");
		ui->selectedPixelLabelR->setText("");
		ui->groupBoxR->setTitle(QApplication::translate("XrayImageStitchingWidget", "Right Image - ") + QFileInfo(rightPaint->getFileName()).baseName() + " (" + XrayQFileUtil::getHumanReadableFileSize(rightPaint->getFileName()) + ")");
	}
}

void XrayImageStitchingWidget::prev()
{
	m_currentIndex--;
	m_currentIndex--;
	if (m_currentIndex < 0)
		m_currentIndex = 0;
	next();
}

void XrayImageStitchingWidget::next()
{
	if (m_fileList.size() > 2 && m_currentIndex >= 0 && m_currentIndex < m_fileList.size())
	{
		rightPaint->load(m_fileList.at(m_currentIndex));
		m_currentIndex++;

		if(m_currentIndex == m_fileList.size())
			ui->groupBoxR->setTitle(QApplication::translate("XrayImageStitchingWidget", "Right Image - ") + QFileInfo(rightPaint->getFileName()).baseName() + " (" + XrayQFileUtil::getHumanReadableFileSize(rightPaint->getFileName()) + ")" + " - Last Image");
		else
			ui->groupBoxR->setTitle(QApplication::translate("XrayImageStitchingWidget", "Right Image - ") + QFileInfo(rightPaint->getFileName()).baseName() + " (" + XrayQFileUtil::getHumanReadableFileSize(rightPaint->getFileName()) + ")");
	}
}

void XrayImageStitchingWidget::repeat()
{
	if (!m_lastImage.isNull())
	{
		leftPaint->resetImage(m_lastImage);
		ui->scrollAreaL->horizontalScrollBar()->triggerAction(QAbstractSlider::SliderToMaximum);	// move scroll to most right side
		emit statusBarMessage(QApplication::translate("XrayImageStitchingWidget", "Left image has been reset to the previous state!"), 15000);
	}
}

void XrayImageStitchingWidget::apply()
{
	auto& imgL = leftPaint->getImage();
	auto& imgR = rightPaint->getImage();

	if (imgL.isNull() || imgR.isNull())
	{
		emit statusBarMessage(QApplication::translate("XrayImageStitchingWidget", "Couldn't find any image!"), 15000);
		return;
	}

	auto p1 = leftPaint->getMouseClickPos();
	auto p2 = rightPaint->getMouseClickPos();

	m_lastImage = imgL.copy();

	auto imageLeft = imgL.copy(0, 0, p1.x(), imgL.height());
	auto imageRight = imgR.copy(p2.x(), 0, imgR.width() - p2.x(), imgR.height());

	// Create the merged image
	auto diffY = std::abs(p1.y() - p2.y());
	auto stitchedImage = QImage(imageLeft.width() + imageRight.width(), std::max(imageLeft.height(), imageRight.height()), imageLeft.format());
	QPainter painter;
	painter.begin(&stitchedImage);
	if (p1.y() > p2.y())
	{
		painter.drawImage(QRect(0, -diffY, imageLeft.width(), imageLeft.height()), imageLeft);
		painter.drawImage(QRect(imageLeft.width(), 0, imageRight.width(), imageRight.height()), imageRight);
	}
	else if (p1.y() < p2.y())
	{
		painter.drawImage(QRect(0, 0, imageLeft.width(), imageLeft.height()), imageLeft);
		painter.drawImage(QRect(imageLeft.width(), -diffY, imageRight.width(), imageRight.height()), imageRight);
	}
	else
	{
		painter.drawImage(QRect(0, 0, imageLeft.width(), imageLeft.height()), imageLeft);
		painter.drawImage(QRect(imageLeft.width(), 0, imageRight.width(), imageRight.height()), imageRight);
	}
	painter.end();

	leftPaint->resetImage(stitchedImage);

	ui->scrollAreaL->horizontalScrollBar()->triggerAction(QAbstractSlider::SliderToMaximum);	// move scroll to most right side
	ui->groupBoxL->setTitle(QApplication::translate("XrayImageStitchingWidget", "Left Image - ") + QFileInfo(leftPaint->getFileName()).baseName() + " + ... + " + QFileInfo(rightPaint->getFileName()).baseName() + " (" + XrayQFileUtil::getHumanReadableSize(stitchedImage.sizeInBytes()) + ")");
	emit statusBarMessage(QApplication::translate("XrayImageStitchingWidget", "Left and right images are stitched successfully!"), 15000);

	if (ui->moveToNextCB->isChecked())
		next();
}

void XrayImageStitchingWidget::save()
{
	auto dialog = new QDialog;
	dialog->setWindowTitle("Image Cropper");
	dialog->setAttribute(Qt::WA_DeleteOnClose);

	auto cropWidget = new XrayImageCropWidget(dialog);
	cropWidget->setROI(QRect(0, 0, leftPaint->getImage().width(), leftPaint->getImage().height()));
	cropWidget->setImage(leftPaint->getImage());

	connect(cropWidget, &XrayImageCropWidget::ok, dialog, &QDialog::accept);
	connect(cropWidget, &XrayImageCropWidget::cancel, dialog, &QDialog::reject);
	connect(cropWidget, &XrayImageCropWidget::statusBarMessage, this, &XrayImageStitchingWidget::statusBarMessage);

	auto vlayout = new QVBoxLayout;
	vlayout->setContentsMargins(0, 0, 0, 0);
	vlayout->addWidget(cropWidget);

	dialog->setLayout(vlayout);
	dialog->resize(800, 600);

	QTimer::singleShot(100, [cropWidget]() { cropWidget->fitImage(); });

	if (dialog->exec() == QDialog::Accepted)
	{

	}
}
void XrayImageStitchingWidget::editInXPaint()
{
	auto& img = leftPaint->getImage();
	if (img.isNull())
	{
		emit statusBarMessage(QApplication::translate("XrayImageStitchingWidget", "Couldn't find any image in the Left Image box!"), 15000);
		return;
	}

	auto path = XrayQFileUtil::getStandardPath("XImage Stitcher Pro", QStandardPaths::DocumentsLocation) + "/stitched-image.jpg";
	if (img.save(path, "JPG", 99))
		emit openInXPaint(path);
	else
		emit statusBarMessage(QApplication::translate("XrayImageStitchingWidget", "Something went wrong! couldn't open."), 15000);
}

void XrayImageStitchingWidget::enableDemoVersion(bool b)
{
	m_demoVersion = b;
}
