/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/22
** filename: 	XrayVoidAnalysisFilter.cpp
** file base:	XrayVoidAnalysisFilter
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a user registration widget with SQLITE
*				database connection.
****************************************************************************/
#include "XrayVoidAnalysisFilter.h"
#include "ui_XrayVoidAnalysisFilter.h"
#include "XrayGlobal.h"
#include "XrayQtUtil.h"
#include "XrayCVUtil.h"
#include "XrayQFileUtil.h"
#include "XrayQDoubleSliderEditor.h"
#include "XrayProgressWidget.h"
#include "XrayPaintMainWindow.h"
#include <QStandardPaths>
#include <QMessageBox>
#include <QInputDialog>
#include <QFileDialog>
#include <QDateTime>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayVoidAnalysisFilter::XrayVoidAnalysisFilter(QWidget *parent) :
	QWidget(parent),
	p_parent(parent),
	p_tableModel(nullptr),
	ui(new Ui::XrayVoidAnalysisFilter),
	p_saveAsExcelDialog(new XraySaveReportAsExcelDialog(nullptr)),
	m_demoLimit(true)
{
	setWindowTitle("XVOID PRO");
	ui->setupUi(this);
	ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->tableView->horizontalHeader()->setStretchLastSection(true);
	connect(ui->tableView, &XrayQTableView::removed, this, &XrayVoidAnalysisFilter::removeTableRows);

	ui->minRepSlider->setValues(3, 1, 0, 1, 10);
	ui->threshStepSlider->setValues(3, 1, 0, 1, 10);
	ui->minThreshSlider->setValues(10, 1, 0, 1, 254);
	ui->maxThreshSlider->setValues(220, 1, 0, 1, 255);
	ui->minAreaSlider->setValues(1, 0.01, 2, 10, 99998);
	ui->maxAreaSlider->setValues(30000.0, 0.01, 2, 11, 99999);
	ui->minCirclSlider->setValues(0.81, 0.01, 2, 0.001, 1.0);
	ui->minConvexSlider->setValues(0.90, 0.01, 2, 0.001, 1.0);
	ui->minIntertiaSlider->setValues(0.13, 0.01, 2, 0.001, 1.0);

	auto spinBoxW = 55;
	ui->minRepSlider->spinBox()->setFixedWidth(spinBoxW);
	ui->threshStepSlider->spinBox()->setFixedWidth(spinBoxW);
	ui->minThreshSlider->spinBox()->setFixedWidth(spinBoxW);
	ui->maxThreshSlider->spinBox()->setFixedWidth(spinBoxW);
	ui->minAreaSlider->spinBox()->setFixedWidth(spinBoxW);
	ui->maxAreaSlider->spinBox()->setFixedWidth(spinBoxW);
	ui->minCirclSlider->spinBox()->setFixedWidth(spinBoxW);
	ui->minConvexSlider->spinBox()->setFixedWidth(spinBoxW);
	ui->minIntertiaSlider->spinBox()->setFixedWidth(spinBoxW);

	connect(ui->minRepSlider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayVoidAnalysisFilter::preview);
	connect(ui->threshStepSlider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayVoidAnalysisFilter::preview);
	connect(ui->minThreshSlider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayVoidAnalysisFilter::preview);
	connect(ui->maxThreshSlider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayVoidAnalysisFilter::preview);
	connect(ui->minAreaSlider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayVoidAnalysisFilter::preview);
	connect(ui->maxAreaSlider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayVoidAnalysisFilter::preview);
	connect(ui->minCirclSlider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayVoidAnalysisFilter::preview);
	connect(ui->minConvexSlider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayVoidAnalysisFilter::preview);
	connect(ui->minIntertiaSlider, &XrayQDoubleSliderEditor::valueChanged, this, &XrayVoidAnalysisFilter::preview);
	connect(ui->lightVoidCBox, &QCheckBox::clicked, this, &XrayVoidAnalysisFilter::preview);
	connect(ui->colorizeCBox, &QCheckBox::clicked, this, &XrayVoidAnalysisFilter::colorize);

	connect(ui->applyBtn, &QPushButton::clicked, this, &XrayVoidAnalysisFilter::apply);
	connect(ui->updateBtn, &QPushButton::clicked, this, &XrayVoidAnalysisFilter::updateTable);
	connect(ui->saveBtn, &QPushButton::clicked, this, &XrayVoidAnalysisFilter::saveTable);

	loadPresets();
	connect(ui->presetComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &XrayVoidAnalysisFilter::selectPreset);
	connect(ui->presetUpdateBtn, &QPushButton::clicked, this, &XrayVoidAnalysisFilter::updateWithCurrentPreset);
	
	auto presetSaveBtn = new QAction(QApplication::translate("XrayVoidAnalysisFilter", "Save current preset"), this);
	connect(presetSaveBtn, &QAction::triggered, this, &XrayVoidAnalysisFilter::savePreset);
	
	auto presetSaveAsBtn = new QAction(QApplication::translate("XrayVoidAnalysisFilter", "Save presets as..."), this);
	connect(presetSaveAsBtn, &QAction::triggered, this, &XrayVoidAnalysisFilter::savePresetsAs);

	auto presetLoadBtn = new QAction(QApplication::translate("XrayVoidAnalysisFilter", "Load presets..."), this);
	connect(presetLoadBtn, &QAction::triggered, this, &XrayVoidAnalysisFilter::loadPresetsAs);

	auto presetRemoveBtn = new QAction(QApplication::translate("XrayVoidAnalysisFilter", "Remove current preset"), this);
	connect(presetRemoveBtn, &QAction::triggered, this, &XrayVoidAnalysisFilter::removePreset);

	auto toolMenu = new QMenu(QApplication::translate("XrayVoidAnalysisFilter", "Preset menu"), this);
	toolMenu->addAction(presetSaveBtn);
	toolMenu->addSeparator();
	toolMenu->addAction(presetSaveAsBtn);
	toolMenu->addAction(presetLoadBtn);
	toolMenu->addSeparator();
	toolMenu->addAction(presetRemoveBtn);
	ui->presetToolButton->setMenu(toolMenu);
	ui->presetToolButton->setStyleSheet("#presetToolButton:menu-indicator{ image: none; }");

	ui->contourColorBtn->setColor(QColor(255, 255, 0, 255));
	ui->contourIdColorBtn->setColor(QColor(0, 255, 0, 255));
	connect(ui->contourColorBtn, &XrayColorPushButton::currentColorChanged, this, &XrayVoidAnalysisFilter::updateContourColor);
	connect(ui->contourIdColorBtn, &XrayColorPushButton::currentColorChanged, this, &XrayVoidAnalysisFilter::updateContourIdColor);

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (par)
	{
		connect(p_saveAsExcelDialog.get(), &XraySaveReportAsExcelDialog::statusBarMessage, [par](const QString& text, int _timeout)
		{
			par->statusBar()->showMessage(text, _timeout);
		});
	}
}
XrayVoidAnalysisFilter::~XrayVoidAnalysisFilter()
{
	delete ui;
}
void XrayVoidAnalysisFilter::setDemoLimitEnabled(bool b)
{
	m_demoLimit = b;
	updateTable();
}
QPushButton* XrayVoidAnalysisFilter::getSaveBtn() const
{
	return ui->saveBtn;
}
void XrayVoidAnalysisFilter::writeSettings(QSettings& _settings)
{
	_settings.beginGroup("XVoidPro");

	_settings.setValue("Parameters/Repeatability", ui->minRepSlider->value());
	_settings.setValue("Parameters/ThresholdStep", ui->threshStepSlider->value());
	_settings.setValue("Parameters/LowerThreshold", ui->minThreshSlider->value());
	_settings.setValue("Parameters/HigherThreshold", ui->maxThreshSlider->value());
	_settings.setValue("Parameters/SmallestVoid", ui->minAreaSlider->value());
	_settings.setValue("Parameters/LargestVoid", ui->maxAreaSlider->value());
	_settings.setValue("Parameters/VoidCircularity", ui->minCirclSlider->value());
	_settings.setValue("Parameters/VoidConvexity", ui->minConvexSlider->value());
	_settings.setValue("Parameters/VoidInertiaRatio", ui->minIntertiaSlider->value());
	_settings.setValue("Parameters/Name", ui->presetComboBox->currentIndex());
	_settings.setValue("Parameters/ColorizedContour", ui->colorizeCBox->isChecked());
	_settings.setValue("Parameters/ContourColor", ui->contourColorBtn->getColor());
	_settings.setValue("Parameters/ContourIdColor", ui->contourIdColorBtn->getColor());

	p_saveAsExcelDialog->writeSettings(_settings);
	_settings.endGroup();
}
void XrayVoidAnalysisFilter::readSettings(QSettings& _settings)
{
	_settings.beginGroup("XVoidPro");

	ui->minRepSlider->setValue(_settings.value("Parameters/Repeatability", ui->minRepSlider->value()).toDouble());
	ui->threshStepSlider->setValue(_settings.value("Parameters/ThresholdStep", ui->threshStepSlider->value()).toDouble());
	ui->minThreshSlider->setValue(_settings.value("Parameters/LowerThreshold", ui->minThreshSlider->value()).toDouble());
	ui->maxThreshSlider->setValue(_settings.value("Parameters/HigherThreshold", ui->maxThreshSlider->value()).toDouble());
	ui->minAreaSlider->setValue(_settings.value("Parameters/SmallestVoid", ui->minAreaSlider->value()).toDouble());
	ui->maxAreaSlider->setValue(_settings.value("Parameters/LargestVoid", ui->maxAreaSlider->value()).toDouble());
	ui->minCirclSlider->setValue(_settings.value("Parameters/VoidCircularity", ui->minCirclSlider->value()).toDouble());
	ui->minConvexSlider->setValue(_settings.value("Parameters/VoidConvexity", ui->minConvexSlider->value()).toDouble());
	ui->minIntertiaSlider->setValue(_settings.value("Parameters/VoidInertiaRatio", ui->minIntertiaSlider->value()).toDouble());

	auto index = _settings.value("Parameters/Name", 0).toInt();
	if (index >= 0 && index < ui->presetComboBox->count())
	{
		auto b = ui->presetComboBox->blockSignals(true);
		ui->presetComboBox->setCurrentIndex(index);
		ui->presetComboBox->blockSignals(b);
	}

	ui->colorizeCBox->setChecked(_settings.value("Parameters/ColorizedContour", ui->colorizeCBox->isChecked()).toBool());
	ui->contourColorBtn->setColor(_settings.value("Parameters/ContourColor", ui->contourColorBtn->getColor()).value<QColor>(), true);
	ui->contourIdColorBtn->setColor(_settings.value("Parameters/ContourIdColor", ui->contourIdColorBtn->getColor()).value<QColor>(), true);

	p_saveAsExcelDialog->readSettings(_settings);
	_settings.endGroup();
}
void XrayVoidAnalysisFilter::colorize(bool b)
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	if (area->getShapeListModel()->getShapeList().empty())
		return;

	auto shape = area->getSelectedShape();
	if (!shape)
		return;

	shape->getBlobDetector().m_colorizeContourOnPercentage = b;
	if (shape->getBlobDetector().draw())
	{
		area->setImage(XrayCVUtil::toQImage(shape->getBlobDetector().m_output));
		area->update();
	}
}
void XrayVoidAnalysisFilter::updateContourColor(const QColor& _color)
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	if (area->getShapeListModel()->getShapeList().empty())
		return;

	auto shape = area->getSelectedShape();
	if (!shape)
		return;

	if (!_color.isValid())
		return;

	shape->getBlobDetector().m_contour_color = cv::Scalar(_color.blue(), _color.green(), _color.red());
	if (shape->getBlobDetector().draw())
	{
		area->setImage(XrayCVUtil::toQImage(shape->getBlobDetector().m_output));
		area->update();
	}
}
void XrayVoidAnalysisFilter::updateContourIdColor(const QColor& _color)
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	if (area->getShapeListModel()->getShapeList().empty())
		return;

	auto shape = area->getSelectedShape();
	if (!shape)
		return;

	if (!_color.isValid())
		return;

	shape->getBlobDetector().m_keypoint_color = cv::Scalar(_color.blue(), _color.green(), _color.red());
	if (shape->getBlobDetector().draw())
	{
		area->setImage(XrayCVUtil::toQImage(shape->getBlobDetector().m_output));
		area->update();
	}
}
cv::SimpleBlobDetector::Params XrayVoidAnalysisFilter::getParametersFromSliders()
{
	cv::SimpleBlobDetector::Params param;

	auto b = ui->minRepSlider->blockSignals(true);
	param.minRepeatability = ui->minRepSlider->value();
	ui->minRepSlider->blockSignals(b);

	b = ui->threshStepSlider->blockSignals(true);
	param.thresholdStep = ui->threshStepSlider->value();
	ui->threshStepSlider->blockSignals(b);

	b = ui->minThreshSlider->blockSignals(true);
	param.minThreshold = ui->minThreshSlider->value();
	ui->minThreshSlider->blockSignals(b);

	b = ui->maxThreshSlider->blockSignals(true);
	param.maxThreshold = ui->maxThreshSlider->value();
	ui->maxThreshSlider->blockSignals(b);

	b = ui->minAreaSlider->blockSignals(true);
	param.minArea = ui->minAreaSlider->value();
	ui->minAreaSlider->blockSignals(b);

	b = ui->maxAreaSlider->blockSignals(true);
	param.maxArea = ui->maxAreaSlider->value();
	ui->maxAreaSlider->blockSignals(b);

	b = ui->minCirclSlider->blockSignals(true);
	param.minCircularity = ui->minCirclSlider->value();
	ui->minCirclSlider->blockSignals(b);

	b = ui->minConvexSlider->blockSignals(true);
	param.minConvexity = ui->minConvexSlider->value();
	ui->minConvexSlider->blockSignals(b);

	b = ui->minIntertiaSlider->blockSignals(true);
	param.minInertiaRatio = ui->minIntertiaSlider->value();
	ui->minIntertiaSlider->blockSignals(b);

	b = ui->lightVoidCBox->blockSignals(true);
	param.blobColor = ui->lightVoidCBox->isChecked() ? 255 : 0;
	ui->lightVoidCBox->blockSignals(b);

	return param;
}
void XrayVoidAnalysisFilter::setParametersToSliders(cv::SimpleBlobDetector::Params param)
{
	auto b = ui->minRepSlider->blockSignals(true);
	ui->minRepSlider->setValue(param.minRepeatability);
	ui->minRepSlider->blockSignals(b);

	b = ui->threshStepSlider->blockSignals(true);
	ui->threshStepSlider->blockSignals(b);

	b = ui->minThreshSlider->blockSignals(true);
	ui->minThreshSlider->setValue(param.minThreshold);
	ui->minThreshSlider->blockSignals(b);

	b = ui->maxThreshSlider->blockSignals(true);
	ui->maxThreshSlider->setValue(param.maxThreshold);
	ui->maxThreshSlider->blockSignals(b);

	b = ui->minAreaSlider->blockSignals(true);
	ui->minAreaSlider->setValue(param.minArea);
	ui->minAreaSlider->blockSignals(b);

	b = ui->maxAreaSlider->blockSignals(true);
	ui->maxAreaSlider->setValue(param.maxArea);
	ui->maxAreaSlider->blockSignals(b);

	b = ui->minCirclSlider->blockSignals(true);
	ui->minCirclSlider->setValue(param.minCircularity);
	ui->minCirclSlider->blockSignals(b);

	b = ui->minConvexSlider->blockSignals(true);
	ui->minConvexSlider->setValue(param.minConvexity);
	ui->minConvexSlider->blockSignals(b);

	b = ui->minIntertiaSlider->blockSignals(true);
	ui->minIntertiaSlider->setValue(param.minInertiaRatio);
	ui->minIntertiaSlider->blockSignals(b);

	b = ui->lightVoidCBox->blockSignals(true);
	ui->lightVoidCBox->setChecked(param.blobColor > 0 ? true : false);
	ui->lightVoidCBox->blockSignals(b);
}
void XrayVoidAnalysisFilter::updateSelectedShapeParameters()
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	// if there is no shape in the list, restore the original image, clear the table and return
	if (area->getShapeListModel()->getShapeList().empty())
	{
		area->setImage(area->getOriginalImage());
		clearTable();
		return;
	}

	// if no selection shape is found, clear the table and return
	auto shape = area->getSelectedShape();
	if (!shape)
	{
		area->setImage(area->getOriginalImage());
		clearTable();
		return;
	}

	// check the shape bounds which should be inside the image.
	auto& image = area->getImage();
	auto r = shape->getBoundingRect();
	if ((r.x < 0) || (r.y < 0) || ((r.x + r.width) > image.width()) || ((r.y + r.height) > image.height()) || (r.width < 2) || (r.height < 2))
	{
		par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Shape ROI is out of range, please try again!"));
		return;
	}

	auto mat = XrayCVUtil::toMat(area->getOriginalImage(), false);

	setParametersToSliders(shape->getBlobDetector().m_params);

	auto c = shape->getBlobDetector().m_contour_color;
	ui->contourColorBtn->setColor(QColor(c[2], c[1], c[0], 255), true);
	c = shape->getBlobDetector().m_keypoint_color;
	ui->contourIdColorBtn->setColor(QColor(c[2], c[1], c[0], 255), true);

	auto b = ui->presetComboBox->blockSignals(true);
	ui->presetComboBox->setCurrentText(QString::fromStdString(shape->getBlobDetector().m_name));
	ui->presetComboBox->blockSignals(b);
	
	//if (par->getMode() == PaintDrawModeType::Selection)	// if you are selecting a shape by select tool then it must update the voids.
	//{
		shape->getBlobDetector().m_image = mat;
		shape->getBlobDetector().m_roi = r;
		shape->getBlobDetector().m_shapeContour = shape->getShapePoints();

		if (shape->getBlobDetector().update())
			area->setImage(XrayCVUtil::toQImage(shape->getBlobDetector().m_output));
		else if (shape->getBlobDetector().m_areas.empty())
			area->setImage(area->getOriginalImage());

		area->update();
		updateTable();
	//}
}
void XrayVoidAnalysisFilter::reset()
{
	auto b = ui->minRepSlider->blockSignals(true);
	ui->minRepSlider->setValue(3);
	ui->minRepSlider->blockSignals(b);

	b = ui->threshStepSlider->blockSignals(true);
	ui->threshStepSlider->setValue(3);
	ui->threshStepSlider->blockSignals(b);

	b = ui->minThreshSlider->blockSignals(true);
	ui->minThreshSlider->setValue(10);
	ui->minThreshSlider->blockSignals(b);

	b = ui->maxThreshSlider->blockSignals(true);
	ui->maxThreshSlider->setValue(220);
	ui->maxThreshSlider->blockSignals(b);
	
	b = ui->minAreaSlider->blockSignals(true);
	ui->minAreaSlider->setValue(10);
	ui->minAreaSlider->blockSignals(b);

	b = ui->maxAreaSlider->blockSignals(true);
	ui->maxAreaSlider->setValue(30000.0);
	ui->maxAreaSlider->blockSignals(b);

	b = ui->minCirclSlider->blockSignals(true);
	ui->minCirclSlider->setValue(0.81);
	ui->minCirclSlider->blockSignals(b);

	b = ui->minConvexSlider->blockSignals(true);
	ui->minConvexSlider->setValue(0.90);
	ui->minConvexSlider->blockSignals(b);

	b = ui->minIntertiaSlider->blockSignals(true);
	ui->minIntertiaSlider->setValue(0.13);
	ui->minIntertiaSlider->blockSignals(b);

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	area->resetImage();
}
bool XrayVoidAnalysisFilter::drawCustomContour(const cv::Mat& _mat, const std::vector<cv::Point>& _contour)
{
	if (_mat.empty())
		return false;

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return false;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return false;

	auto shape = area->getSelectedShape();
	if (!shape)
		return false;

	auto b = ui->presetComboBox->blockSignals(true);
	auto name = ui->presetComboBox->currentText();
	ui->presetComboBox->blockSignals(b);


	shape->getBlobDetector().m_name = name.toStdString();
	shape->getBlobDetector().m_image = _mat;
	shape->getBlobDetector().m_roi = shape->getBoundingRect();
	shape->getBlobDetector().m_shapeContour = shape->getShapePoints();

	// perform detection and accumulate the void areas
	shape->getBlobDetector().addCustomContour(_contour);
	if (shape->getBlobDetector().draw())
		return true;

	// if there no void detected, make sure that the areas are empty
	shape->getBlobDetector().m_areas.clear();

	return false;
}
void XrayVoidAnalysisFilter::previewCustomContour(const QVector<QPointF>& _points)
{
	if (_points.empty())
		return;

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	auto shape = area->getSelectedShape();
	if (!shape)
	{
		par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Couldn't find any selected shape!"));
		return;
	}

	// check the shape bounds which should be inside the image.
	auto& image = area->getImage();
	auto r = shape->getBoundingRect();
	if ((r.x < 0) || (r.y < 0) || ((r.x + r.width) > image.width()) || ((r.y + r.height) > image.height()) || (r.width < 2) || (r.height < 2))
	{
		par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Shape ROI is out of range, please try again!"));
		return;
	}

	// covert points to local shape coordinate system
	std::vector<cv::Point> points(_points.size());
	for (auto i = 0; i < _points.size(); i++)
	{
		points[i].x = _points[i].x() - r.x;
		points[i].y = _points[i].y() - r.y;
	}

	auto mat = XrayCVUtil::toMat(area->getOriginalImage(), false);

	// perform detection
	if (drawCustomContour(mat, points))
	{
		area->setImage(XrayCVUtil::toQImage(shape->getBlobDetector().m_output));
	}
	else if (shape->getBlobDetector().m_areas.empty())
	{
		area->setImage(area->getOriginalImage());
	}

	area->update();
	updateTable();
}
bool XrayVoidAnalysisFilter::detect(const cv::Mat& _mat)
{
	if(_mat.empty())
		return false;

	auto b = ui->minRepSlider->blockSignals(true);
	auto minRep = ui->minRepSlider->value();
	ui->minRepSlider->blockSignals(b);

	b = ui->threshStepSlider->blockSignals(true);
	auto threshStep = ui->threshStepSlider->value();
	ui->threshStepSlider->blockSignals(b);

	b = ui->minThreshSlider->blockSignals(true);
	auto minThresh = ui->minThreshSlider->value();
	ui->minThreshSlider->blockSignals(b);

	b = ui->maxThreshSlider->blockSignals(true);
	auto maxThresh = ui->maxThreshSlider->value();
	ui->maxThreshSlider->blockSignals(b);

	b = ui->minAreaSlider->blockSignals(true);
	auto minArea = ui->minAreaSlider->value();
	ui->minAreaSlider->blockSignals(b);

	b = ui->maxAreaSlider->blockSignals(true);
	auto maxArea = ui->maxAreaSlider->value();
	ui->maxAreaSlider->blockSignals(b);

	b = ui->minCirclSlider->blockSignals(true);
	auto minCircl = ui->minCirclSlider->value();
	ui->minCirclSlider->blockSignals(b);

	b = ui->minConvexSlider->blockSignals(true);
	auto minConvex = ui->minConvexSlider->value();
	ui->minConvexSlider->blockSignals(b);

	b = ui->minIntertiaSlider->blockSignals(true);
	auto minIntertia = ui->minIntertiaSlider->value();
	ui->minIntertiaSlider->blockSignals(b);

	b = ui->lightVoidCBox->blockSignals(true);
	auto isLightBlob = ui->lightVoidCBox->isChecked();
	ui->lightVoidCBox->blockSignals(b);

	b = ui->presetComboBox->blockSignals(true);
	auto name = ui->presetComboBox->currentText();
	ui->presetComboBox->blockSignals(b);

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return false;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return false;

	auto shape = area->getSelectedShape();
	if (!shape)
		return false;

	shape->getBlobDetector().m_name = name.toStdString();
	shape->getBlobDetector().m_image = _mat;
	shape->getBlobDetector().m_roi = shape->getBoundingRect();
	shape->getBlobDetector().m_shapeContour = shape->getShapePoints();

	shape->getBlobDetector().m_params.minRepeatability = minRep;
	shape->getBlobDetector().m_params.thresholdStep = threshStep;
	shape->getBlobDetector().m_params.minThreshold = minThresh;
	shape->getBlobDetector().m_params.maxThreshold = maxThresh;

	shape->getBlobDetector().m_params.filterByArea = true;
	shape->getBlobDetector().m_params.filterByCircularity = true;
	shape->getBlobDetector().m_params.filterByConvexity = true;
	shape->getBlobDetector().m_params.filterByInertia = true;
	shape->getBlobDetector().m_params.filterByColor = true;

	shape->getBlobDetector().m_params.minArea = minArea;
	shape->getBlobDetector().m_params.maxArea = maxArea;
	shape->getBlobDetector().m_params.minCircularity = minCircl;
	shape->getBlobDetector().m_params.minConvexity = minConvex;
	shape->getBlobDetector().m_params.minInertiaRatio = minIntertia;
	shape->getBlobDetector().m_params.blobColor = isLightBlob ? 255 : 0;

	// perform detection and accumulate the void areas
	if (shape->getBlobDetector().detect())
	{
		if (shape->getBlobDetector().draw())
			return true;
	}

	// if there no void detected, make sure that the areas are empty
	shape->getBlobDetector().m_areas.clear();

	return false;
}
void XrayVoidAnalysisFilter::clearTable(const QString& _pictureName)
{
	if (p_tableModel)
	{
		if (p_tableModel->objectName() == _pictureName)
			clearTable();
	}
}
void XrayVoidAnalysisFilter::clearTable()
{
	// clearing the table view works only by creating a new table model and set to table view.
	if (p_tableModel)
		p_tableModel->deleteLater();
	p_tableModel = new XrayTableModel;
	ui->tableView->setModel(p_tableModel);
	ui->tableView->repaint();
}
void XrayVoidAnalysisFilter::updateTable()
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	auto shape = area->getSelectedShape();
	if (!shape)
	{
		par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Couldn't find any selected shape!"));
		return;
	}

	auto& areas = shape->getBlobDetector().m_areas;
	if (areas.empty())
	{
		clearTable();
		par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Couldn't find any void in the selected shape!"));
		return;
	}

	// define the table header
	std::vector<std::string> headers = { "Shape", "Void", "Shape area", "Void area", "%", };

	// reset the memory for table rows
	std::vector<std::vector<std::string> > rows;
	rows.resize(areas.size());

	bool self_selection = false;
	// accumulate the table rows
	for (std::size_t i = 0; i < areas.size(); i++)
	{
		if (100.0 / shape->calculateSurfaceArea() * areas[i] > 85) self_selection = true;
		// QString::arg() uses the default locale to format a number when its position specifier in the format string contains an 'L', e.g. "%L1".
		std::vector<std::string> values = { shape->getName().toStdString(), QString("%1").arg(i).toStdString(), QString("%1").arg(shape->calculateSurfaceArea()).toStdString(), QString("%L1").arg(areas[i], 0, 'g', 6).toStdString(), QString("%L1").arg(100.0 / shape->calculateSurfaceArea() * areas[i], 0, 'g', 6).toStdString() };
		rows[i] = values;
	}

	// set the demo limitation to only 5 voids in the table
	if (m_demoLimit)
	{
		if (rows.size() > 5)
			rows.resize(5);
	}

	// Note: there is no way to reset the table model, clearing the table view works only by creating a new table model and set to table view.
	if (p_tableModel)
		p_tableModel->deleteLater();
	p_tableModel = new XrayTableModel;
	p_tableModel->setObjectName(area->getFileName());
	p_tableModel->createTable(headers, rows);
	// to avoid self detection
	if (self_selection) XrayVoidAnalysisFilter::removeTableRows({ 0 });
	ui->tableView->setModel(p_tableModel);
	ui->tableView->resizeColumnsToContents();
	ui->tableView->resizeRowsToContents();
	ui->tableView->repaint();
	
}
void XrayVoidAnalysisFilter::preview()
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	auto shape = area->getSelectedShape();
	if (!shape)
	{
		par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Couldn't find any selected shape!"));
		return;
	}

	// check the shape bounds which should be inside the image.
	auto& image = area->getImage();
	auto r = shape->getBoundingRect();
	if ((r.x < 0) || (r.y < 0) || ((r.x + r.width) > image.width()) || ((r.y + r.height) > image.height()) || (r.width < 2) || (r.height < 2))
	{
		par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Shape ROI is out of range, please try again!"));
		return;
	}

	auto mat = XrayCVUtil::toMat(area->getOriginalImage(), false);

	// perform detection
	if (detect(mat))
	{
		area->setImage(XrayCVUtil::toQImage(shape->getBlobDetector().m_output));
	}
	else if (shape->getBlobDetector().m_areas.empty())
	{
		area->setImage(area->getOriginalImage());
	}

	area->update();
	updateTable();
}
// old implimentation, not using anymore...
void XrayVoidAnalysisFilter::saveTableCSV(const QString& fileName)
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	if (area->getShapeListModel()->getShapeListConst().empty())
	{
		par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Couldn't find any shape in the Paint area!"));
		return;
	}

	if (QFile::exists(fileName))
	{
		// define the table header
		std::vector<std::string> header = { "Shape", "Void", "Shape area", "Void area", "%" };

		// reset the memory for table rows
		std::vector<std::vector<std::string> > rows;
		std::size_t totalRows = 0;
		for (auto& shape : area->getShapeListModel()->getShapeListConst())
			totalRows += shape->getBlobDetector().m_areas.size();
		rows.reserve(totalRows);

		// accumulate the table rows
		for (auto& shape : area->getShapeListModel()->getShapeListConst())
		{
			auto& areas = shape->getBlobDetector().m_areas;
			for (std::size_t i = 0; i < areas.size(); i++)
			{
				std::vector<std::string> values = { shape->getName().toStdString(), QString("%1").arg(i).toStdString(), QString("%1").arg(shape->calculateSurfaceArea()).toStdString(), QString("%L1").arg(areas[i], 0, 'g', 6).toStdString(), QString("%L1").arg(100.0 / shape->calculateSurfaceArea() * areas[i], 0, 'g', 6).toStdString() };
				rows.push_back(values);
			}
			// for next shape
			rows.push_back(std::vector<std::string>()); // empty line
		}

		// save to comma separated file
		QChar delim = ',';

		// in case of German language or system
		if (XrayGlobal::getCurrentAppLanguage() == "de" || QLocale::system().name() == "de_DE")
			delim = ';';

		if (!p_tableModel)
		{
			par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Couldn't find any void in the table!"));
			return;
		}

		if (p_tableModel->writeCSV(fileName, header, rows, delim))
		{
			auto imgFile = QFileInfo(fileName).absolutePath() + "/" + QFileInfo(fileName).baseName() + ".jpg";
			auto img = area->copyImage();
			if (img.save(imgFile))
			{
				xAppInfo("Voids statistics file has been saved at {}", imgFile);
			}
		}
		else
		{
			xAppError("Couldn't write voids statistics at {}", fileName);
		}
	}
}
void XrayVoidAnalysisFilter::saveTable()
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	if (area->getShapeListModel()->getShapeListConst().empty())
	{
		par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Couldn't find any shape in the Paint area!"));
		return;
	}

	p_saveAsExcelDialog->setCurrentPaintArea(area);
	p_saveAsExcelDialog->setPaintAreas(par->getPaintAreas());
	p_saveAsExcelDialog->exec();
}
void XrayVoidAnalysisFilter::removeTableRows(const std::vector<int>& indexList)
{
	if (!p_tableModel || indexList.empty())
		return;

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	auto shape = area->getSelectedShape();
	if (!shape)
	{
		par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Couldn't find any selected shape!"));
		return;
	}

	shape->getBlobDetector().removeContours(indexList);
	if (shape->getBlobDetector().draw())
	{
		area->setImage(XrayCVUtil::toQImage(shape->getBlobDetector().m_output));
		area->update();
	}

	updateTable();
}
void XrayVoidAnalysisFilter::apply()
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	auto shape = area->getSelectedShape();
	if (!shape)
	{
		par->statusBar()->showMessage(QApplication::translate("XrayVoidAnalysisFilter", "Couldn't find any selected shape!"));
		return;
	}

	area->setCurrentImageAsOriginalImage();
}
static QString getDefaultPresets()
{
	return "[XVoidProPresets]\n"
		"Preset\\size=3\n"
		"Preset\\1\\Name=1\n"
		"Preset\\1\\Repeatability=3\n"
		"Preset\\1\\ThresholdStep=3\n"
		"Preset\\1\\LowerThreshold=10\n"
		"Preset\\1\\HigherThreshold=220\n"
		"Preset\\1\\SmallestVoid=10\n"
		"Preset\\1\\LargestVoid=30000\n"
		"Preset\\1\\VoidCircularity=0.8100000023841858\n"
		"Preset\\1\\VoidConvexity=0.8999999761581421\n"
		"Preset\\1\\VoidInertiaRatio=0.12999999523162842\n"
		"Preset\\1\\VoidColor=255\n"
		"Preset\\2\\Name=2\n"
		"Preset\\2\\Repeatability=3\n"
		"Preset\\2\\ThresholdStep=3\n"
		"Preset\\2\\LowerThreshold=10\n"
		"Preset\\2\\HigherThreshold=220\n"
		"Preset\\2\\SmallestVoid=15\n"
		"Preset\\2\\LargestVoid=30000\n"
		"Preset\\2\\VoidCircularity=0.5\n"
		"Preset\\2\\VoidConvexity=0.699999988079071\n"
		"Preset\\2\\VoidInertiaRatio=0.09000000357627869\n"
		"Preset\\2\\VoidColor=255\n"
		"Preset\\3\\Name=3\n"
		"Preset\\3\\Repeatability=2\n"
		"Preset\\3\\ThresholdStep=2\n"
		"Preset\\3\\LowerThreshold=5\n"
		"Preset\\3\\HigherThreshold=220\n"
		"Preset\\3\\SmallestVoid=10\n"
		"Preset\\3\\LargestVoid=30000\n"
		"Preset\\3\\VoidCircularity=0.5899999737739563\n"
		"Preset\\3\\VoidConvexity=0.6899999976158142\n"
		"Preset\\3\\VoidInertiaRatio=0.05000000074505806\n"
		"Preset\\3\\VoidColor=255\n";
}
void XrayVoidAnalysisFilter::loadPresets()
{
	// get the presets file path
	auto docPath = XrayQFileUtil::getStandardPath(QApplication::applicationName(), QStandardPaths::DocumentsLocation);
	auto filePath = XrayQFileUtil::getStandardPath("presets", docPath) + "xvoid_presets.dat";
	if (!QFile::exists(filePath))	// if file doesn't exists then create with default presets
		XrayQFileUtil::write(getDefaultPresets(), filePath, XrayQFileUtil::Text, QFile::WriteOnly);

	loadPresetFile(filePath);
}
void XrayVoidAnalysisFilter::loadPresetsAs()
{
	auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayVoidAnalysisFilter", "Select File"), XrayGlobal::getLastFileOpenPath(), "XVoid Preset Type (*.dat)", nullptr);
	if (fileName.isEmpty())
		return;

	XrayGlobal::setLastFileOpenPath(fileName);

	if (loadPresetFile(fileName))
		qDebug() << "Loaded XVoid preset.";
	else
		qDebug() << "Couldn't loaded XVoid preset.";
}
bool XrayVoidAnalysisFilter::loadPresetFile(const QString& fileName)
{
	// load presets from file
	QSettings settings(fileName, QSettings::Format::IniFormat);
	auto groups = settings.childGroups();
	if (!groups.contains("XVoidProPresets"))
		return false;

	// first clear all presets
	m_presets.clear();
	ui->presetComboBox->clear();

	settings.beginGroup("XVoidProPresets");
	auto totalPresets = settings.beginReadArray("Preset");

	for (auto i = 0; i < totalPresets; i++)
	{
		settings.setArrayIndex(i);

		cv::SimpleBlobDetector::Params params;
		params.minRepeatability = settings.value("Repeatability", 3.0).toDouble();
		params.thresholdStep = settings.value("ThresholdStep", 3.0).toDouble();
		params.minThreshold = settings.value("LowerThreshold", 10.0).toDouble();
		params.maxThreshold = settings.value("HigherThreshold", 220.0).toDouble();
		params.minArea = settings.value("SmallestVoid", 10.0).toDouble();
		params.maxArea = settings.value("LargestVoid", 220.0).toDouble();
		params.minCircularity = settings.value("VoidCircularity", 0.81).toDouble();
		params.minConvexity = settings.value("VoidConvexity", 0.90).toDouble();
		params.minInertiaRatio = settings.value("VoidInertiaRatio", 0.13).toDouble();
		params.blobColor = settings.value("VoidColor", 255.0).toDouble();

		m_presets.append(params);
		ui->presetComboBox->addItem(settings.value("Name", QString::number(i)).toString());
	}

	settings.endArray();
	settings.endGroup();

	return true;
}
void XrayVoidAnalysisFilter::savePresets()
{
	auto docPath = XrayQFileUtil::getStandardPath(QApplication::applicationName(), QStandardPaths::DocumentsLocation);
	auto filePath = XrayQFileUtil::getStandardPath("presets", docPath) + "xvoid_presets.dat";

	QSettings settings(filePath, QSettings::Format::IniFormat);
	settings.clear();	// first, clear the previous settings

	settings.beginGroup("XVoidProPresets");
	settings.beginWriteArray("Preset");

	for (auto i = 0; i < m_presets.size(); i++)
	{
		settings.setArrayIndex(i);

		auto params = m_presets.at(i);
		settings.setValue("Name", ui->presetComboBox->itemText(i));
		settings.setValue("Repeatability", static_cast<double>(params.minRepeatability));
		settings.setValue("ThresholdStep", static_cast<double>(params.thresholdStep));
		settings.setValue("LowerThreshold", static_cast<double>(params.minThreshold));
		settings.setValue("HigherThreshold", static_cast<double>(params.maxThreshold));
		settings.setValue("SmallestVoid", static_cast<double>(params.minArea));
		settings.setValue("LargestVoid", static_cast<double>(params.maxArea));
		settings.setValue("VoidCircularity", static_cast<double>(params.minCircularity));
		settings.setValue("VoidConvexity", static_cast<double>(params.minConvexity));
		settings.setValue("VoidInertiaRatio", static_cast<double>(params.minInertiaRatio));
		settings.setValue("VoidColor", static_cast<double>(params.blobColor));
	}

	settings.endArray();
	settings.endGroup();
}
void XrayVoidAnalysisFilter::savePresetsAs()
{
	auto fileName = QFileDialog::getSaveFileName(this, QApplication::translate("XrayVoidAnalysisFilter", "Save File As..."), XrayGlobal::getLastFileOpenPath(), "XVoid Preset Type (*.dat)", nullptr);
	if (fileName.isEmpty())
		return;

	XrayGlobal::setLastFileOpenPath(fileName);

	savePresetFile(fileName);
	qDebug() << "Saved XVoid preset.";
}
void XrayVoidAnalysisFilter::savePresetFile(const QString& fileName)
{
	QSettings settings(fileName, QSettings::Format::IniFormat);
	settings.clear();	// first, clear the previous settings

	settings.beginGroup("XVoidProPresets");
	settings.beginWriteArray("Preset");

	for (auto i = 0; i < m_presets.size(); i++)
	{
		settings.setArrayIndex(i);

		auto params = m_presets.at(i);
		settings.setValue("Name", ui->presetComboBox->itemText(i));
		settings.setValue("Repeatability", static_cast<double>(params.minRepeatability));
		settings.setValue("ThresholdStep", static_cast<double>(params.thresholdStep));
		settings.setValue("LowerThreshold", static_cast<double>(params.minThreshold));
		settings.setValue("HigherThreshold", static_cast<double>(params.maxThreshold));
		settings.setValue("SmallestVoid", static_cast<double>(params.minArea));
		settings.setValue("LargestVoid", static_cast<double>(params.maxArea));
		settings.setValue("VoidCircularity", static_cast<double>(params.minCircularity));
		settings.setValue("VoidConvexity", static_cast<double>(params.minConvexity));
		settings.setValue("VoidInertiaRatio", static_cast<double>(params.minInertiaRatio));
		settings.setValue("VoidColor", static_cast<double>(params.blobColor));
	}

	settings.endArray();
	settings.endGroup();
}
void XrayVoidAnalysisFilter::savePreset()
{
	QString lastPresetName = "Preset 0";
	if(ui->presetComboBox->count() > 0)
		lastPresetName = ui->presetComboBox->itemText(ui->presetComboBox->count() - 1);

	bool ok;
	auto text = QInputDialog::getText(this, QApplication::translate("XrayVoidAnalysisFilter", "Enter New Preset Name"),
		QApplication::translate("XrayVoidAnalysisFilter", "Enter Text"), QLineEdit::Normal, lastPresetName, &ok);
	if (ok && !text.isEmpty())
	{
		// append to the list and then write to file.
		m_presets.append(getParametersFromSliders());

		// add this preset to the combo box and set as current
		ui->presetComboBox->addItem(text);
		auto b = ui->presetComboBox->blockSignals(true);
		ui->presetComboBox->setCurrentText(text);
		ui->presetComboBox->blockSignals(b);

		// save all presets to file
		savePresets();

		// preview to update voids
		preview();
	}
}
void XrayVoidAnalysisFilter::removePreset()
{
	auto index = ui->presetComboBox->currentIndex();
	if (index >= 0 && index < m_presets.size())
	{
		m_presets.removeAt(index);
		ui->presetComboBox->removeItem(index);
		savePresets();
	}
}
void XrayVoidAnalysisFilter::selectPreset(int _index)
{
	if (_index >= 0 && _index < m_presets.size())
	{
		setParametersToSliders(m_presets.at(_index));
		preview();
	}
}
void XrayVoidAnalysisFilter::updateWithCurrentPreset()
{
	selectPreset(ui->presetComboBox->currentIndex());
}
void XrayVoidAnalysisFilter::setPreset1()
{
	setParametersToSliders(m_presets.at(0));
	preview();
}
void XrayVoidAnalysisFilter::setPreset2()
{
	setParametersToSliders(m_presets.at(1));
	preview();
}
