/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/05/20
** filename: 	XrayImageFilters.cpp
** file base:	XrayImageFilters
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the GUI for contrast enhancement on 8,
16, 24, 32 bit images.
****************************************************************************/

#include "XrayImageFilters.h"
#include "ui_XrayImageFilters.h"

#include "XrayGlobal.h"
#include "XrayQtUtil.h"
#include "XrayCVUtil.h"
#include "fvkContrastEnhancerUtil.h"

#include "XrayPaintMainWindow.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QDateTime>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayImageFilters::XrayImageFilters(QWidget *parent) :
	QWidget(parent),
	p_parent(parent),
	ui(new Ui::XrayImageFilters)
{
	setWindowTitle("XImage Filters");
	ui->setupUi(this);

	connect(ui->autoAdjustSlider, &QSlider::valueChanged, [this](int value)
	{
		auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
		if (!par)
			return;

		auto area = par->getCurrentPaintArea();
		if (!area)
			return;

		if (area)
		{
			auto src = XrayCVUtil::toMat(area->getOriginalImage(), true);
			cv::Mat m(src.size(), src.type());
			R3D::fvkContrastEnhancerUtil::autoBrightnessAndContrast(src, m, value);
			area->setImage(m);
			area->update();
		}
	});

	connect(ui->resetBtn, &QPushButton::clicked, this, &XrayImageFilters::reset);
	connect(ui->applyBtn, &QPushButton::clicked, this, &XrayImageFilters::apply);
}

XrayImageFilters::~XrayImageFilters()
{
	delete ui;
}

void XrayImageFilters::writeSettings(QSettings& _settings)
{
	_settings.beginGroup("XImageFilters");

	//_settings.setValue("Parameters/Delta", ui->deltaSlider->value());
	//_settings.setValue("Parameters/Sigma", ui->sigmaSlider->value());

	_settings.endGroup();
}
void XrayImageFilters::readSettings(QSettings& _settings)
{
	_settings.beginGroup("XImageFilters");

	//ui->autoAdjustSlider->setValue(_settings.value("Parameters/Delta", ui->deltaSlider->value()).toInt());
	//ui->autoAdjustSpinBox->setValue(_settings.value("Parameters/Sigma", ui->sigmaSlider->value()).toInt());

	_settings.endGroup();
}


void XrayImageFilters::reset()
{
	auto b = ui->autoAdjustSlider->blockSignals(true);
	ui->autoAdjustSlider->setValue(0);
	ui->autoAdjustSlider->blockSignals(b);

	b = ui->autoAdjustSpinBox->blockSignals(true);
	ui->autoAdjustSpinBox->setValue(0);
	ui->autoAdjustSpinBox->blockSignals(b);

	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	area->resetImage();
}

void XrayImageFilters::preview()
{
	update();
}

cv::Mat XrayImageFilters::getFilteredImage(const cv::Mat& _mat)
{
	auto b = ui->autoAdjustSlider->blockSignals(true);
	const auto value = ui->autoAdjustSlider->value();
	ui->autoAdjustSlider->blockSignals(b);

	cv::Mat dest(_mat.size(), _mat.type());
	R3D::fvkContrastEnhancerUtil::autoBrightnessAndContrast(_mat, dest, value);
	return dest;

}

void XrayImageFilters::apply()
{
	auto par = qobject_cast<XrayPaintMainWindow*>(p_parent);
	if (!par)
		return;

	auto area = par->getCurrentPaintArea();
	if (!area)
		return;

	if (area->getImage().isNull())
		return;

	area->setCurrentImageAsOriginalImage();
}