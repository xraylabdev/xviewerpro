/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayPaintUndoCommands.cpp
** file base:	XrayPaintUndoCommands
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides shape factory for the Paint.
****************************************************************************/

#include "XrayPaintUndoCommands.h"

static const int AddShapeCommandId = 1;
static const int ResizeShapeCommandId = 2;
static const int MoveShapeCommandId = 3;

XRAYLAB_USING_NAMESPACE

XrayPaintAddShapeCommand::XrayPaintAddShapeCommand(XrayPaintShapeListModel& _shapeListModel, XrayPaintBaseShape* _newShape, QUndoCommand* _parent) :
	QUndoCommand(_parent),
	shapeListModel(_shapeListModel),
	newShape(_newShape)
{
}
void XrayPaintAddShapeCommand::undo()
{
	newShape.reset(shapeListModel.removeShapeAt(0));
}
void XrayPaintAddShapeCommand::redo()
{
	shapeListModel.insertShapeAt(newShape.take(), 0);
}

XrayPaintDeleteShapeCommand::XrayPaintDeleteShapeCommand(XrayPaintShapeListModel& _shapeListModel, const int& _row, QUndoCommand* _parent) :
	QUndoCommand(_parent),
	shapeListModel(_shapeListModel),
	row(_row),
	deletedShape(nullptr)
{
}
void XrayPaintDeleteShapeCommand::undo()
{
	shapeListModel.insertShapeAt(deletedShape.take(), row);
}
void XrayPaintDeleteShapeCommand::redo()
{
	deletedShape.reset(shapeListModel.removeShapeAt(row));
}



XrayPaintDeleteImageCommand::XrayPaintDeleteImageCommand(QImage& _image, XrayPaintShapeListModel& _model) :
	image(_image),
	oldImage(_image),
	model(_model)
{
}

XrayPaintDeleteImageCommand::~XrayPaintDeleteImageCommand()
{
	if (!storedShapes.isEmpty())
		qDeleteAll(storedShapes);
}

void XrayPaintDeleteImageCommand::undo()
{
	image = oldImage;
	model.swapList(storedShapes);
}

void XrayPaintDeleteImageCommand::redo()
{
	image.fill(Qt::white);
	model.swapList(storedShapes);
}
