﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/06
** filename: 	XrayIconPushButton.cpp
** file base:	XrayIconPushButton
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a customized push button with an icon
*				on it.
****************************************************************************/

#include "XrayIconPushButton.h"

XRAYLAB_USING_NAMESPACE

XrayIconPushButton::XrayIconPushButton(const QIcon& icon, const QString& text, QWidget* parent) :
	QPushButton(icon, text, parent)
{
	setFlat(true);
}
XrayIconPushButton::XrayIconPushButton(const QIcon& icon, QWidget* parent) :
	XrayIconPushButton(icon, "", parent)
{
}

bool XrayIconPushButton::event(QEvent* _event)
{
	if (_event->type() == QEvent::HoverEnter)
	{
		setFlat(false);
	}

	if (_event->type() == QEvent::HoverLeave)
	{
		setFlat(true);
	}

	return QPushButton::event(_event);
}