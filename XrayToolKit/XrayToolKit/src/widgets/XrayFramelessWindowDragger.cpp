/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XrayFramelessWindowDragger.cpp
** file base:	XrayFramelessWindowDragger
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a mouse dragger for a widget.
****************************************************************************/

#include "XrayFramelessWindowDragger.h"
#include <QPainter>
#include <QStyleOption>

XRAYLAB_USING_NAMESPACE

XrayFramelessWindowDragger::XrayFramelessWindowDragger(QWidget* _parent) : 
	QWidget(_parent),
	m_mouse_pressed(false)
{
}

void XrayFramelessWindowDragger::mousePressEvent(QMouseEvent* _event)
{
	m_mouse_pressed = true;
	m_mouse_pos = _event->globalPos();

	auto parent = parentWidget();
	if (parent) parent = parent->parentWidget();

	if (parent) m_wnd_pos = parent->pos();
}

void XrayFramelessWindowDragger::mouseMoveEvent(QMouseEvent* _event)
{
	auto parent = parentWidget();
	if (parent) parent = parent->parentWidget();

	if (parent && m_mouse_pressed)
		parent->move(m_wnd_pos + (_event->globalPos() - m_mouse_pos));
}

void XrayFramelessWindowDragger::mouseReleaseEvent(QMouseEvent* _event)
{
	Q_UNUSED(_event);
	m_mouse_pressed = false;
}

void XrayFramelessWindowDragger::paintEvent(QPaintEvent* _event)
{
	Q_UNUSED(_event);
	QStyleOption sop;
	sop.init(this);
	QPainter painter(this);
	style()->drawPrimitive(QStyle::PE_Widget, &sop, &painter, this);
}

void XrayFramelessWindowDragger::mouseDoubleClickEvent(QMouseEvent* _event)
{
	Q_UNUSED(_event);
	emit doubleClicked();
}
