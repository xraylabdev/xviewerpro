/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayAbstractButtonListModel.cpp
** file base:	XrayAbstractButtonListModel
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides list model for buttons.
****************************************************************************/

#include "XrayAbstractButtonListModel.h"

#include <QVector>
#include <QMap>
#include <QVariant>

XRAYLAB_USING_NAMESPACE

XrayAbstractButtonListModel::XrayAbstractButtonListModel(QObject *parent) :
	QAbstractListModel(parent)
{
}

int XrayAbstractButtonListModel::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid()) 
	{
		return 0;
	}

	return m_buttons.count();
}

QVariant XrayAbstractButtonListModel::data(const QModelIndex &index, int role) const
{
	if (hasIndex(index.row(), index.column(), index.parent())) 
	{
		return m_buttons[index.row()][role];
	}

	return QVariant();
}

Qt::ItemFlags XrayAbstractButtonListModel::flags(const QModelIndex &index) const
{
	return QAbstractListModel::flags(index) | Qt::ItemIsEditable | Qt::ItemIsUserCheckable | Qt::ItemNeverHasChildren;
}

bool XrayAbstractButtonListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (hasIndex(index.row(), index.column(), index.parent()) && role != Qt::EditRole) 
	{
		auto& button = m_buttons[index.row()];
		auto it = button.find(role);

		if (it == button.end()) 
		{
			if (value.isNull())
			{
				return false;
			}

			button[role] = value;
		}
		else {
			if (value.isNull())
			{
				button.erase(it);
			}
			else if ((*it) == value)
			{
				return false;
			}
			else 
			{
				button[role] = value;
			}
		}

		emit dataChanged(index, index, QVector<int>() << role);
		return true;
	}

	return false;
}

bool XrayAbstractButtonListModel::insertRows(int row, int count, const QModelIndex &parent)
{
	if (count < 1 || row < 0 || row > rowCount(parent) || parent.isValid()) 
	{
		return false;
	}

	beginInsertRows(QModelIndex(), row, row + count - 1);
	m_buttons.insert(row, count, {});
	endInsertRows();

	return true;
}

bool XrayAbstractButtonListModel::removeRows(int row, int count, const QModelIndex &parent)
{
	if (count < 1 || row < 0 || (row + count) > rowCount(parent) || parent.isValid()) 
	{
		return false;
	}

	beginRemoveRows(QModelIndex(), row, row + count - 1);
	m_buttons.remove(row, count);
	endRemoveRows();

	return true;
}

void XrayAbstractButtonListModel::clear()
{
	removeRows(0, rowCount(), QModelIndex());
}