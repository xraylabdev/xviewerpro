/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayUpDownButton.cpp
** file base:	XrayUpDownButton
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides up and down buttons.
****************************************************************************/

#include "XrayUpDownButton.h"
#include <QAction>

XRAYLAB_USING_NAMESPACE

XrayUpDownButtonP::XrayUpDownButtonP(XrayUpDownButton* _widget, Qt::Orientation _orientation) :
	QObject(_widget),
	widget(_widget),
	layout(new QBoxLayout(orientationToDirection(_orientation), widget)),
	upButton(new QToolButton(widget)),
	downButton(new QToolButton(widget)),
	autoRaise(false)
{
	Q_ASSERT(widget);

	layout->setMargin(0);
	layout->setSpacing(1);
	layout->addWidget(upButton);
	layout->addWidget(downButton);

	upButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	downButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	widget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	updateButtonArrows();

	connect(upButton, &QToolButton::clicked, widget, &XrayUpDownButton::upClicked);
	connect(downButton, &QToolButton::clicked, widget, &XrayUpDownButton::downClicked);
	connect(upButton, &QToolButton::pressed, widget, &XrayUpDownButton::upPressed);
	connect(downButton, &QToolButton::pressed, widget, &XrayUpDownButton::downPressed);
	connect(upButton, &QToolButton::released, widget, &XrayUpDownButton::upReleased);
	connect(downButton, &QToolButton::released, widget, &XrayUpDownButton::downReleased);
	connect(upButton, &QToolButton::toggled, widget, &XrayUpDownButton::upToggled);
	connect(downButton, &QToolButton::toggled, widget, &XrayUpDownButton::downToggled);
	connect(upButton, &QToolButton::triggered, widget, &XrayUpDownButton::upTriggered);
	connect(downButton, &QToolButton::triggered, widget, &XrayUpDownButton::downTriggered);
}
void XrayUpDownButtonP::updateButtonArrows()
{
	switch (directionToOrientation(layout->direction()))
	{
	case Qt::Horizontal:
		upButton->setArrowType(Qt::LeftArrow);
		downButton->setArrowType(Qt::RightArrow);
		break;
	case Qt::Vertical:
	default:
		upButton->setArrowType(Qt::UpArrow);
		downButton->setArrowType(Qt::DownArrow);
		break;
	}
}

QBoxLayout::Direction XrayUpDownButtonP::orientationToDirection(Qt::Orientation _orientation) const
{
	switch (_orientation)
	{
	case Qt::Horizontal:
		return QBoxLayout::LeftToRight;
	case Qt::Vertical:
	default:
		return QBoxLayout::TopToBottom;
	}
}

Qt::Orientation XrayUpDownButtonP::directionToOrientation(QBoxLayout::Direction _direction) const
{
	switch (_direction)
	{
	case QBoxLayout::LeftToRight:
	case QBoxLayout::RightToLeft:
		return Qt::Horizontal;
	case QBoxLayout::TopToBottom:
	case QBoxLayout::BottomToTop:
	default:
		return Qt::Vertical;
	}
}

XrayUpDownButton::XrayUpDownButton(QWidget *parent) :
	QWidget(parent),
	d(new XrayUpDownButtonP(this, Qt::Vertical))
{
}

XrayUpDownButton::XrayUpDownButton(Qt::Orientation _orientation, QWidget* parent) :
	QWidget(parent),
	d(new XrayUpDownButtonP(this, _orientation))
{
}

Qt::Orientation XrayUpDownButton::orientation() const
{
	return d->directionToOrientation(d->layout->direction());
}

void XrayUpDownButton::setOrientation(Qt::Orientation orientation)
{
	d->layout->setDirection(d->orientationToDirection(orientation));
	d->updateButtonArrows();
}

int XrayUpDownButton::spacing() const
{
	return d->layout->spacing();
}

void XrayUpDownButton::setSpacing(int spacing)
{
	d->layout->setSpacing(spacing);
}

bool XrayUpDownButton::autoRaise() const
{
	return d->upButton->autoRaise() && d->downButton->autoRaise();
}

void XrayUpDownButton::setAutoRaise(bool autoRaise)
{
	d->upButton->setAutoRaise(autoRaise);
	d->downButton->setAutoRaise(autoRaise);
}

QToolButton::ToolButtonPopupMode XrayUpDownButton::popupMode() const
{
	return d->upButton->popupMode();
}

void XrayUpDownButton::setPopupMode(QToolButton::ToolButtonPopupMode mode)
{
	d->upButton->setPopupMode(mode);
	d->downButton->setPopupMode(mode);
}

QKeySequence XrayUpDownButton::upShortcut() const
{
	return d->upButton->shortcut();
}

void XrayUpDownButton::setUpShortcut(const QKeySequence &key)
{
	d->upButton->setShortcut(key);
}

QKeySequence XrayUpDownButton::downShortcut() const
{
	return d->downButton->shortcut();
}

void XrayUpDownButton::setDownShortcut(const QKeySequence &key)
{
	d->downButton->setShortcut(key);
}

QMenu *XrayUpDownButton::upMenu() const
{
	return d->upButton->menu();
}

void XrayUpDownButton::setUpMenu(QMenu *menu)
{
	d->upButton->setMenu(menu);
}

QMenu *XrayUpDownButton::downMenu() const
{
	return d->downButton->menu();
}

void XrayUpDownButton::setDownMenu(QMenu *menu)
{
	d->downButton->setMenu(menu);
}

void XrayUpDownButton::upAnimateClick(int msec)
{
	d->upButton->animateClick(msec);
}

void XrayUpDownButton::downAnimateClick(int msec)
{
	d->downButton->animateClick(msec);
}

void XrayUpDownButton::upClick()
{
	d->upButton->click();
}

void XrayUpDownButton::downClick()
{
	d->downButton->click();
}

void XrayUpDownButton::upToggle()
{
	d->upButton->toggle();
}

void XrayUpDownButton::downToggle()
{
	d->downButton->toggle();
}

void XrayUpDownButton::upShowMenu()
{
	d->upButton->showMenu();
}

void XrayUpDownButton::downShowMenu()
{
	d->downButton->showMenu();
}