/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/14
** filename: 	XrayHoverEventPushButton.cpp
** file base:	XrayHoverEventPushButton
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a push button that emits signals on
				mouse hover events.
****************************************************************************/

#include "XrayHoverEventPushButton.h"

XRAYLAB_USING_NAMESPACE

XrayHoverEventPushButton::XrayHoverEventPushButton(const QIcon& _icon, const QString& _text, QWidget* _parent) :
	QPushButton(_icon, _text, _parent)
{
	setMouseTracking(true);
}

void XrayHoverEventPushButton::enterEvent(QEvent* _e)
{
	emit mouseEnter();
	QWidget::enterEvent(_e);
}
void XrayHoverEventPushButton::leaveEvent(QEvent* _e)
{
	emit mouseLeave();
	QWidget::leaveEvent(_e);
}
void XrayHoverEventPushButton::hoverEnter(QHoverEvent* _e)
{
	emit mouseHoverEnter(_e);
}
void XrayHoverEventPushButton::hoverLeave(QHoverEvent* _e)
{
	emit mouseHoverLeave(_e);
}
void XrayHoverEventPushButton::hoverMove(QHoverEvent* _e)
{
	emit mouseHoverMove(_e);
}

bool XrayHoverEventPushButton::event(QEvent* _ev)
{
	switch (_ev->type())
	{
	case QEvent::HoverEnter:
		hoverEnter(dynamic_cast<QHoverEvent*>(_ev));
		return true;
		break;
	case QEvent::HoverLeave:
		hoverLeave(dynamic_cast<QHoverEvent*>(_ev));
		return true;
		break;
	case QEvent::HoverMove:
		hoverMove(dynamic_cast<QHoverEvent*>(_ev));
		return true;
		break;
	default:
		break;
	}
	return QWidget::event(_ev);
}