/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/13
** filename: 	XrayFramelessWindowWin32.h
** file base:	XrayFramelessWindowWin32
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a frame less main window.
****************************************************************************/

#include "XrayStackedFramelessWindowWin32.h"

XRAYLAB_USING_NAMESPACE

XrayStackedFramelessWindowWin32::XrayStackedFramelessWindowWin32(const QString& _title, QWidget* _parent) :
	XrayFramelessWindowWin32(_title, _parent)
{
	p_stackedWidget = new QStackedWidget(this);
	addCentralWidget(p_stackedWidget);
}

void XrayStackedFramelessWindowWin32::setCurrentIndex(int index)
{
	p_stackedWidget->setCurrentIndex(index);
}
void XrayStackedFramelessWindowWin32::addWidget(QWidget *w)
{
	p_stackedWidget->addWidget(w);
}
void XrayStackedFramelessWindowWin32::removeWidget(QWidget *w)
{
	p_stackedWidget->removeWidget(w);
}
QStackedWidget* XrayStackedFramelessWindowWin32::stackedWidget() const
{
	return p_stackedWidget;
}