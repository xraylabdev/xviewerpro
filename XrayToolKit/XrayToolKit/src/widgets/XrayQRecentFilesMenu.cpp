/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/18
** filename: 	XrayQRecentFilesMenu.cpp
** file base:	XrayQRecentFilesMenu
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a recent files menu with actions.
****************************************************************************/

#include "XrayQRecentFilesMenu.h"

#include <QFileInfo>
#include <QApplication>

XRAYLAB_USING_NAMESPACE

static const qint32 RecentFilesMenuMagic = 0xff;

XrayQRecentFilesMenu::XrayQRecentFilesMenu(QWidget* parent) : 
	QMenu(parent),
	m_maxCount(5),
	m_format(QLatin1String("%d | %s"))
{
	connect(this, &QMenu::triggered, this, &XrayQRecentFilesMenu::menuTriggered);

	setMaxCount(m_maxCount);
}

XrayQRecentFilesMenu::XrayQRecentFilesMenu(const QString& title, QWidget* parent) :
	QMenu(title, parent),
	m_maxCount(5),
	m_format(QLatin1String("%d | %s"))
{
	connect(this, &QMenu::triggered, this, &XrayQRecentFilesMenu::menuTriggered);

	setMaxCount(m_maxCount);
}

void XrayQRecentFilesMenu::addRecentFile(const QString &fileName)
{
	m_projectDir.removeAll(fileName);
	m_projectDir.prepend(fileName);

	while (m_projectDir.size() > maxCount())
		m_projectDir.removeLast();

	updateRecentFileActions();
}
void XrayQRecentFilesMenu::removeRecentFile(const QString &fileName)
{
	m_projectDir.removeOne(fileName);
	updateRecentFileActions();
}
void XrayQRecentFilesMenu::clearMenu()
{
	m_projectDir.clear();

	updateRecentFileActions();
}

int XrayQRecentFilesMenu::maxCount() const
{
	return m_maxCount;
}

void XrayQRecentFilesMenu::setFormat(const QString &format)
{
	if (m_format == format)
		return;

	m_format = format;
	updateRecentFileActions();
}

const QString & XrayQRecentFilesMenu::format() const
{
	return m_format;
}

QByteArray XrayQRecentFilesMenu::saveState() const
{
	auto version = 0;
	QByteArray data;
	QDataStream stream(&data, QIODevice::WriteOnly);

	stream << qint32(RecentFilesMenuMagic);
	stream << qint32(version);
	stream << m_projectDir;

	return data;
}

bool XrayQRecentFilesMenu::restoreState(const QByteArray &state)
{
	auto version = 0;
	QByteArray sd = state;
	QDataStream stream(&sd, QIODevice::ReadOnly);
	qint32 marker;
	qint32 v;

	stream >> marker;
	stream >> v;
	if (marker != RecentFilesMenuMagic || v != version)
		return false;

	stream >> m_projectDir;

	updateRecentFileActions();

	return true;
}

void XrayQRecentFilesMenu::setMaxCount(int count)
{
	m_maxCount = count;

	updateRecentFileActions();
}

void XrayQRecentFilesMenu::menuTriggered(QAction* action)
{
	if (action->data().isValid())
		emit recentFileTriggered(action->data().toString());
}

void XrayQRecentFilesMenu::updateRecentFileActions()
{
	auto numRecentFiles = qMin(m_projectDir.size(), maxCount());

	clear();

	for (auto i = 0; i < numRecentFiles; ++i) 
	{
		auto strippedName = QFileInfo(m_projectDir[i]).fileName();

		auto text = m_format;
		text.replace(QLatin1String("%d"), QString::number(i + 1));
		text.replace(QLatin1String("%s"), strippedName);

		auto recentFileAct = addAction(text);
		recentFileAct->setData(m_projectDir[i]);
	}
	addSeparator();
	addAction(QApplication::translate("XrayQRecentFilesMenu", "Clear Menu"), this, &XrayQRecentFilesMenu::clearMenu);

	setEnabled(numRecentFiles > 0);
}

