/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayResizeDialog.h
** file base:	XrayResizeDialog
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a dialog to get the new size from a user.
****************************************************************************/

#include "XrayResizeDialog.h"

#include <QApplication>

XRAYLAB_USING_NAMESPACE

XrayResizeDialog::XrayResizeDialog(QWidget* parent, const QSize currentSize) :
	QDialog(parent, Qt::WindowTitleHint | Qt::WindowCloseButtonHint),
	button_OK(new QPushButton(tr("&Ok"))),
	button_Cancel(new QPushButton(tr("&Cancel"))),
	spinbox_width(new QSpinBox(this)),
	spinbox_height(new QSpinBox(this)),
	checkbox_aspect(new QCheckBox("Maintain aspect ratio", this)),
	mapper(new QSignalMapper(this)),
	newSize{currentSize}
{
	setWindowTitle(tr("Resize"));
	setWindowFlags(windowFlags() & (~Qt::WindowContextHelpButtonHint | Qt::MSWindowsFixedSizeDialogHint));

	spinbox_width->setMaximum(9999);
	spinbox_height->setMaximum(9999);
	spinbox_width->setButtonSymbols(QAbstractSpinBox::NoButtons);
	spinbox_height->setButtonSymbols(QAbstractSpinBox::NoButtons);
	spinbox_width->setValue(newSize.width());
	spinbox_height->setValue(newSize.height());

	horizontal_label = new QLabel(tr("Horizontal") + ":");
	vertical_label = new QLabel(tr("Vertical") + ":");

	gridLayout = new QGridLayout;
	gridLayout->addWidget(horizontal_label, 0, 0);
	gridLayout->addWidget(spinbox_width, 0, 1);
	gridLayout->addWidget(new QLabel("px"), 0, 2);
	gridLayout->addWidget(vertical_label, 1, 0);
	gridLayout->addWidget(spinbox_height, 1, 1);
	gridLayout->addWidget(new QLabel("px"), 1, 2);
	gridLayout->addWidget(checkbox_aspect, 2, 0);
	gridLayout->setColumnMinimumWidth(0, 130);
	checkbox_aspect->setChecked(true);

	buttonLayout = new QHBoxLayout;
	buttonLayout->addWidget(button_OK);
	buttonLayout->addWidget(button_Cancel);
	buttonLayout->setAlignment(Qt::AlignBottom);

	mainLayout = new QVBoxLayout;
	mainLayout->addLayout(gridLayout);
	mainLayout->addLayout(buttonLayout);

	setLayout(mainLayout);

	connect(button_Cancel, &QPushButton::clicked, mapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	connect(button_OK, &QPushButton::clicked, mapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	mapper->setMapping(button_Cancel, 0);
	mapper->setMapping(button_OK, 1);
	connect(mapper, static_cast<void (QSignalMapper::*)(int)>(&QSignalMapper::mapped), this, &XrayResizeDialog::done);
	connect(mapper, static_cast<void (QSignalMapper::*)(int)>(&QSignalMapper::mapped), this, &XrayResizeDialog::done);

	setFixedSize(260, 140);
	retranslate();
}

bool XrayResizeDialog::isKeepAspectRatioChecked() const
{
	return checkbox_aspect->isChecked();
}

const QSize XrayResizeDialog::getNewSize()
{
	auto width = spinbox_width->text().toInt();
	auto height = spinbox_height->text().toInt();
	return QSize(width, height);
}

void XrayResizeDialog::retranslate()
{
	setWindowTitle(QApplication::translate("XrayResizeDialog", "Resize", nullptr));
	button_OK->setText(QApplication::translate("XrayResizeDialog", "Ok", nullptr));
	button_Cancel->setText(QApplication::translate("XrayResizeDialog", "Cancel", nullptr));
	checkbox_aspect->setText(QApplication::translate("XrayResizeDialog", "Maintain aspect ratio", nullptr));
	horizontal_label->setText(QApplication::translate("XrayResizeDialog", "Horizontal", nullptr) + ":");
	vertical_label->setText(QApplication::translate("XrayResizeDialog", "Vertical", nullptr) + ":");
}
