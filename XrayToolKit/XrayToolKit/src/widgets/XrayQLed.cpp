/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/18
** filename: 	XrayQLed.cpp
** file base:	XrayQLed
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a led widget.
****************************************************************************/

#include "XrayQLed.h"

#include <QColor>
#include <QtGlobal>
#include <QtGui>
#include <QPolygon>
#include <QtSvg>
#include <QSvgRenderer>

XRAYLAB_USING_NAMESPACE

 /*!
   \brief QLed: this is the QLed constructor.
   \param parent: The Parent Widget
 */
XrayQLed::XrayQLed(QWidget *parent) : 
	QWidget(parent)
{
	//setAttribute(Qt::WA_NoSystemBackground, true);
	//setAttribute(Qt::WA_TranslucentBackground);
	//setAutoFillBackground(true);
	//setStyleSheet("background-color: transparent;");

	m_value = false;
	m_onColor = Red;
	m_offColor = Grey;
	m_shape = Circle;
	shapes << ":/res/leds/circle_" << ":/res/leds/square_" << ":/res/leds/triang_" << ":/res/leds/round_";
	colors << "red.svg" << "green.svg" << "yellow.svg" << "grey.svg" << "orange.svg" << "purple.svg" << "blue.svg";

	renderer = new QSvgRenderer();
}
XrayQLed::~XrayQLed() 
{
	delete renderer;
}

void XrayQLed::paintEvent(QPaintEvent *)
{
	QString ledShapeAndColor;
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);

	ledShapeAndColor = shapes[m_shape];

	if (m_value)
		ledShapeAndColor.append(colors[m_onColor]);
	else
		ledShapeAndColor.append(colors[m_offColor]);

	renderer->load(ledShapeAndColor);
	renderer->render(&painter);
}


/*!
  \brief setOnColor: this method allows to change the On color {Red,Green,Yellow,Grey,Orange,Purple,blue}
  \param ledColor newColor
  \return void
*/
void XrayQLed::setOnColor(ledColor newColor)
{
	m_onColor = newColor;
	update();
}


/*!
  \brief setOffColor: this method allows to change the Off color {Red,Green,Yellow,Grey,Orange,Purple,blue}
  \param ledColor newColor
  \return void
*/
void XrayQLed::setOffColor(ledColor newColor)
{
	m_offColor = newColor;
	update();
}


/*!
  \brief setShape: this method allows to change the led shape {Circle,Square,Triangle,Rounded rectangle}
  \param ledColor newColor
  \return void
*/
void XrayQLed::setShape(ledShape newShape)
{
	m_shape = newShape;
	update();
}


/*!
  \brief setValue: this method allows to set the led value {true,false}
  \param ledColor newColor
  \return void
*/
void XrayQLed::setValue(bool value)
{
	m_value = value;
	update();
}


/*!
  \brief toggleValue: this method toggles the led value
  \param ledColor newColor
  \return void
*/
void XrayQLed::toggleValue()
{
	m_value = !m_value;
	update();
	return;
}

XrayQLedBlink::XrayQLedBlink(QWidget *parent) :
	XrayQLed(parent),
	m_index(0)
{
	m_timer.setInterval(2000);
	connect(&m_timer, &QTimer::timeout, [this]()
	{
		auto b = blockSignals(true);
		if (m_index % 2)
		{
			setValue(true);
			m_index--;
		}
		else
		{
			setValue(false);
			m_index++;
		}
		blockSignals(b);
	});
}
XrayQLedBlink::~XrayQLedBlink()
{
}

void XrayQLedBlink::start()
{
	if (m_timer.isActive())
		m_timer.stop();
	m_timer.start();
}
void XrayQLedBlink::stop()
{
	m_timer.stop();
}

void XrayQLedBlink::setInterval(int msec)
{
	m_timer.setInterval(msec);
}
int XrayQLedBlink::interval() const
{
	return m_timer.interval();
}