/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayQThread.cpp
** file base:	XrayQThread
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a thread with pause and delay support.
****************************************************************************/

#include "XraySortFilterProxyModel.h"

#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayProxyModelFilter::XrayProxyModelFilter(const QVariant &value, const int role, const Qt::MatchFlags flags) :
	m_value(value),
	m_role(role),
	m_flags(flags)
{
}
bool XrayProxyModelFilter::acceptsValue(const QVariant & value)
{
	uint matchType = m_flags & 0x0F;
	//if we have no value we accept everything
	if (!m_value.isValid() || !value.isValid())
		return true;

	// QVariant based matching
	if (matchType == Qt::MatchExactly)
	{
		if (m_value == value)
			return true;
	}
	// QString based matching
	else
	{
		Qt::CaseSensitivity cs = m_flags & Qt::MatchCaseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive;
		QString filterText = m_value.toString();
		QString modelText = value.toString();
		switch (matchType) {
		case Qt::MatchRegExp:
			if (QRegExp(filterText, cs).exactMatch(modelText))
				return true;
			break;
		case Qt::MatchWildcard:
			if (QRegExp(filterText, cs, QRegExp::Wildcard).exactMatch(modelText))
				return true;
			break;
		case Qt::MatchStartsWith:
			if (modelText.startsWith(filterText, cs))
				return true;
			break;
		case Qt::MatchEndsWith:
			if (modelText.endsWith(filterText, cs))
				return true;
			break;
		case Qt::MatchFixedString:
			if (modelText.compare(filterText, cs) == 0)
				return true;
			break;
		default:
		case Qt::MatchContains:
			if (modelText.contains(filterText, cs))
				return true;
		}
	}
	return false;
}

XraySortFilterProxyModel::XraySortFilterProxyModel(QObject *parent) : QSortFilterProxyModel(parent)
{
	m_declaringFilter = false;
}
void XraySortFilterProxyModel::setRootIndex(const QModelIndex& _index)
{
	m_rootIndex = _index;
}

void XraySortFilterProxyModel::beginDeclareFilter()
{
	m_declaringFilter = true;
}
void XraySortFilterProxyModel::endDeclareFilter()
{
	if (m_declaringFilter)
	{
		m_declaringFilter = false;
		invalidateFilter();
	}
}

bool XraySortFilterProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
	if (!sourceModel())
		return false;

	if (m_rootIndex == source_parent)
	{
		auto filterColumns = filters.keys();
		foreach(int col, filterColumns)
		{
			auto filter = filters[col];
			// if the column specified by the user is -1 
			// that means all columns need to pass the filter to get into the result
			if (col == -1)
			{
				auto column_count = sourceModel()->columnCount(source_parent);
				for (auto column = 0; column < column_count; ++column)
				{
					auto source_index = sourceModel()->index(source_row, column, source_parent);
					auto key = sourceModel()->data(source_index, filter.m_role);
					if (!filter.acceptsValue(key))
						return false;
				}
				continue;
			}

			auto source_index = sourceModel()->index(source_row, 0, source_parent);
			if (!source_index.isValid()) // the column may not exist
				continue;

			auto key = sourceModel()->data(source_index, filter.m_role);
			if (filter.acceptsValue(key))
				return true;
		}

		return false;
	}

	return true;
}

void XraySortFilterProxyModel::setFilter(const int column, const QVariant &value, const int role, Qt::MatchFlags flags)
{
	if (filters.contains(column))
		filters[column] = XrayProxyModelFilter(value, role, flags);
	else
		filters.insert(column, XrayProxyModelFilter(value, role, flags));

	if (!m_declaringFilter)
		invalidateFilter();
}

void XraySortFilterProxyModel::removeFilter(const int column)
{
	if (filters.contains(column)) {
		filters.remove(column);

		if (!m_declaringFilter)
			invalidateFilter();
	}
}

void XraySortFilterProxyModel::setFilterValue(const int column, const QVariant &value)
{
	if (filters.contains(column))
		filters[column].m_value = value;
	else
		filters.insert(column, XrayProxyModelFilter(value));

	if (!m_declaringFilter)
		invalidateFilter();
}

void XraySortFilterProxyModel::setFilterRole(const int column, const int role)
{
	if (filters.contains(column))
		filters[column].m_role = role;
	else
		filters.insert(column, XrayProxyModelFilter(QVariant(), role));

	if (!m_declaringFilter)
		invalidateFilter();
}

void XraySortFilterProxyModel::setFilterFlags(const int column, const Qt::MatchFlags flags)
{
	if (filters.contains(column))
		filters[column].m_flags = flags;
	else
		filters.insert(column, XrayProxyModelFilter(QVariant(), Qt::DisplayRole, flags));

	if (!m_declaringFilter)
		invalidateFilter();
}

QVariant XraySortFilterProxyModel::filterValue(const int column) const
{
	if (filters.contains(column))
		return filters[column].m_value;
	return QVariant();
}

int XraySortFilterProxyModel::filterRole(const int column) const
{
	if (filters.contains(column))
		return filters[column].m_role;
	return -1;
}

Qt::MatchFlags XraySortFilterProxyModel::filterFlags(const int column) const
{
	if (filters.contains(column))
		return filters[column].m_flags;
	return Qt::MatchContains;
}

bool XraySortFilterProxyModel::isFiltered(const int column)
{
	return filters.contains(column);
}

// have a look at the following accepted row function... to filter out based on levels.
//QFilterTreeProxyModel::QFilterTreeProxyModel(QObject* p)
//	: QSortFilterProxyModel(p)
//{
//	this->TreeLevel = 0;
//}
//
//QFilterTreeProxyModel::~QFilterTreeProxyModel()
//{
//}
//
//void QFilterTreeProxyModel::setFilterTreeLevel(int level)
//{
//	this->TreeLevel = level;
//}
//
//bool QFilterTreeProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
//{
//	int level = 0;
//	QModelIndex pidx = sourceParent;
//	while (pidx != QModelIndex())
//	{
//		pidx = pidx.parent();
//		level++;
//	}
//
//	if (level < this->TreeLevel)
//	{
//		return true;
//	}
//
//	if (level > this->TreeLevel)
//	{
//		return filterAcceptsRow(sourceRow, sourceParent.parent());
//	}
//
//	QModelIndex idx = sourceModel()->index(sourceRow, filterKeyColumn(), sourceParent);
//
//	return (sourceModel()->data(idx).toString().contains(filterRegExp()));
//}
//
//bool QFilterTreeProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
//{
//	QVariant leftData = this->sourceModel()->data(left);
//	QVariant rightData = this->sourceModel()->data(right);
//
//	QString leftString = leftData.toString();
//	QString rightString = rightData.toString();
//
//	return QString::localeAwareCompare(leftString, rightString) < 0;
//}
