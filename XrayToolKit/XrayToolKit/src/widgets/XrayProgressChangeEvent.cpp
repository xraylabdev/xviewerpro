/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayProgressChangeEvent.cpp
** file base:	XrayProgressChangeEvent
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides progress change event.
****************************************************************************/

#include "XrayProgressChangeEvent.h"

XRAYLAB_USING_NAMESPACE

const QEvent::Type XrayProgressChangeEvent::Type = static_cast<QEvent::Type>(QEvent::registerEventType());

XrayProgressChangeEvent::XrayProgressChangeEvent(double _progress) :
	QEvent(Type),
	m_progress(_progress)
{
}

XrayProgressChangeEvent::~XrayProgressChangeEvent()
{
}

double XrayProgressChangeEvent::getProgress() const 
{
	return m_progress;
}