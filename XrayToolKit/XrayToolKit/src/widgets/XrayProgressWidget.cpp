/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayProgressWidget.cpp
** file base:	XrayProgressWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides progress widget.
****************************************************************************/

#include "XrayProgressWidget.h"

XRAYLAB_USING_NAMESPACE

XrayProgressWidget::XrayProgressWidget(QWidget* parent) :
	QWidget(parent),
	m_started(false),
	m_percentTextVisible(true)
{
	resize(706, 23);

	gridLayout = new QGridLayout(this);
	gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
	gridLayout->setHorizontalSpacing(5);
	gridLayout->setContentsMargins(0, 0, 0, 0);
	
	progressBar = new QProgressBar(this);
	progressBar->setObjectName(QString::fromUtf8("progressBar"));
	progressBar->setEnabled(true);
	progressBar->setMinimum(0);
	progressBar->setMaximum(100);
	progressBar->setValue(0);
	progressBar->setTextVisible(false);
	progressBar->setInvertedAppearance(false);

	gridLayout->addWidget(progressBar, 0, 0, 1, 1);

	statusWidget = new XrayPixmapStatusWidget(this);
	statusWidget->setObjectName(QString::fromUtf8("widgetStatus"));
	statusWidget->setMinimumSize(QSize(22, 22));
	statusWidget->setMaximumSize(QSize(22, 22));
	statusWidget->setIcon(XrayPixmapStatusWidget::Okay, QPixmap(":/res/images/okay_icon.png"));
	statusWidget->setIcon(XrayPixmapStatusWidget::Error, QPixmap(":/res/images/error_icon.png"));
	statusWidget->setFrames(XrayPixmapStatusWidget::Busy, QPixmap(":/res/images/busy_icon.png"), 8);

	gridLayout->addWidget(statusWidget, 0, 1, 1, 1);
}

XrayProgressWidget::~XrayProgressWidget()
{
}

void XrayProgressWidget::setCurrentProgress(double progress)
{
	if (m_started)
		progressBar->setValue(progress * 1e2);
}

double XrayProgressWidget::getCurrentProgress() const
{
	if (m_started)
		return progressBar->value() * 1e-2;
	return 0;
}

bool XrayProgressWidget::isStarted() const
{
	return m_started;
}

void XrayProgressWidget::start(const QString& toolTip) 
{
	if (!m_started) 
	{
		statusWidget->setCurrentRole(XrayPixmapStatusWidget::Busy, toolTip);

		progressBar->reset();
		progressBar->setTextVisible(m_percentTextVisible);

		m_started = true;
	}
}
void XrayProgressWidget::onStart()
{
	start("");
}

void XrayProgressWidget::finish(const QString& toolTip) 
{
	if (m_started) 
	{
		statusWidget->setCurrentRole(XrayPixmapStatusWidget::Okay, toolTip);

		progressBar->reset();
		progressBar->setTextVisible(false);

		m_started = false;
	}
}
void XrayProgressWidget::onFinish()
{
	finish("");
}

void XrayProgressWidget::fail(const QString& toolTip)
{
	if (m_started) 
	{
		statusWidget->setCurrentRole(XrayPixmapStatusWidget::Error, toolTip);

		progressBar->reset();
		progressBar->setTextVisible(false);

		m_started = false;
	}
}
void XrayProgressWidget::onFail()
{
	fail("");
}

void XrayProgressWidget::setStatusWidgetVisible(bool _b)
{
	statusWidget->setHidden(!_b);
}
void XrayProgressWidget::setPercentTextVisible(bool _b)
{
	m_percentTextVisible = _b;
}