/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/28
** filename: 	XraySingletonQApplication.cpp
** file base:	XraySingletonQApplication
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a QApplication object with some
				pre-defined attributes.
****************************************************************************/

#include "XraySingletonQApplication.h"

#include <QLocalSocket>

#define G_TIME_OUT 500    // 500ms

XRAYLAB_USING_NAMESPACE

XraySingletonQApplication::XraySingletonQApplication(int &argc, char **argv, const QString &serverName) :
	XrayQApplication(argc, argv), 
	m_isRunning(false),
	p_localServer(nullptr),
	m_serverName(serverName)
{
	initLocalConnection();
}
XraySingletonQApplication::~XraySingletonQApplication()
{
	if(p_localServer)
		p_localServer->deleteLater();
}

bool XraySingletonQApplication::isRunning() 
{
	return m_isRunning;
}

void XraySingletonQApplication::newLocalConnection() 
{
	auto socket = p_localServer->nextPendingConnection();
	if (socket) 
	{
		socket->waitForReadyRead(2 * G_TIME_OUT);
		delete socket;

		// first instance section: when the new/second instance send signal to the first instance when detected.
		emit firstInstanceCalled();
	}
}

void XraySingletonQApplication::initLocalConnection()
{
	m_isRunning = false;

	QLocalSocket socket;
	socket.connectToServer(m_serverName);
	if (socket.waitForConnected(G_TIME_OUT)) 
	{
		m_isRunning = true;

		// current instance section: if the first instance already been running.
		return;
	}

	// create a connection without connecting to the server.
	newLocalServer();
}

void XraySingletonQApplication::newLocalServer() 
{
	p_localServer = new QLocalServer(this);
	connect(p_localServer, &QLocalServer::newConnection, this, &XraySingletonQApplication::newLocalConnection);
	if (!p_localServer->listen(m_serverName)) 
	{
		// At this point, the listener fails, which may be caused by the
		// residual process service when the program crashes.
		if (p_localServer->serverError() == QAbstractSocket::AddressInUseError)
		{
			QLocalServer::removeServer(m_serverName);
			p_localServer->listen(m_serverName); // monitor again
		}
	}
}


void XraySingletonQApplication::bringToFront()
{
	if (m_isRunning)
	{
		XrayQApplication::bringToFront();
		std::exit(0);	// use std::exit instead just exit(), exit is inherited from QApplication.
	}
}