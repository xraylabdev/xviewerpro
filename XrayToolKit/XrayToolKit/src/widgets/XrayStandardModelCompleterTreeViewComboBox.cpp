/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/19/07
** filename: 	XrayStandardModelCompleterTreeViewComboBox.cpp
** file base:	XrayStandardModelCompleterTreeViewComboBox
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a table view.
****************************************************************************/

#include "XrayStandardModelCompleterTreeViewComboBox.h"
#include "XrayStandardModelTreeView.h"

XRAYLAB_USING_NAMESPACE

XrayStandardModelCompleterTreeViewComboBox::XrayStandardModelCompleterTreeViewComboBox(int menuAction, QWidget *p_parent) :
	QComboBox(p_parent),
	p_completerModel(nullptr)
{
	setEditable(true);
	setInsertPolicy(QComboBox::InsertPolicy::NoInsert);	// NoInsert: very important

	p_view = new XrayStandardModelTreeView(menuAction, this);
	p_view->setFocusPolicy(Qt::NoFocus);
	p_view->setDragDropMode(QAbstractItemView::NoDragDrop);
	p_view->setRootIsDecorated(false);	// true will create a down arrow if child exists
	p_view->setParentFlags (Qt::ItemIsSelectable);
	p_view->setChildFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
	p_view->setCustomMenuAction(XrayStandardModelTreeView::None);	// do not show right click menu
	setView(p_view);
	setModel(p_view->sourceModel());

	p_completer = new QCompleter(this);
	p_completer->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
	p_completer->setCaseSensitivity(Qt::CaseInsensitive);
	p_completer->setFilterMode(Qt::MatchContains);
	p_completer->setCompletionColumn(0);
	p_completer->setMaxVisibleItems(10);
	p_completer->setCompletionRole(Qt::DisplayRole);
	p_completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
	setCompleter(p_completer);

	QObject::connect(this, &QComboBox::editTextChanged, [this](const QString&)
	{
		if (p_completerModel)
			delete p_completerModel;
		p_completerModel = new QStringListModel();
		p_completerModel->setStringList(p_view->sourceModel()->findIndicesTexts(false));
		p_completer->setModel(p_completerModel);
	});

	connect(p_completer, QOverload<const QString&>::of(&QCompleter::activated), p_view, &XrayStandardModelTreeView::setCurrentChildIndex);
}

void XrayStandardModelCompleterTreeViewComboBox::showPopup()
{
	setRootModelIndex(QModelIndex());
	QComboBox::showPopup();
}
void XrayStandardModelCompleterTreeViewComboBox::hidePopup()
{
	setRootModelIndex(p_view->currentIndex().parent());
	setCurrentIndex(p_view->currentIndex().row());
	QComboBox::hidePopup();
}
void XrayStandardModelCompleterTreeViewComboBox::selectIndex(const QModelIndex& index)
{
	setRootModelIndex(index.parent());
	setCurrentIndex(index.row());
	p_view->setCurrentIndex(index);
}
