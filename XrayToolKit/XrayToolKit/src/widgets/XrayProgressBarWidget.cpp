/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayProgressBarWidget.cpp
** file base:	XrayProgressBarWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides progress widget.
****************************************************************************/

#include "XrayProgressBarWidget.h"

#include <QApplication>
#include <QHBoxLayout>
#include <QLabel>
#include <QList>
#include <QPainter>
#include <QPair>
#include <QPointer>
#include <QStyle>
#include <QStyleFactory>
#include <QStyleOptionProgressBar>
#include <QTimerEvent>
#include <QtDebug>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayProgressBarWidgetLabel : public QLabel
{
public:
	XrayProgressBarWidgetLabel(QWidget* parentObj) : 
		QLabel(parentObj), 
		m_progressPercentage(0), 
		m_showProgress(false),
		p_style(NULL)
	{
		setFrameShape(QFrame::Panel);
		setFrameShadow(QFrame::Sunken);
		setLineWidth(1);
#ifdef __APPLE__
		// We don't use mac style on apple since it render horrible progress bars.
		Style = QStyleFactory::create("fusion");
		if (!Style)
		{
			Style = QStyleFactory::create("cleanlooks");
		}
#endif
	}

	~XrayProgressBarWidgetLabel() { delete p_style; }

	// returns true if value changed.
	bool setProgressPercentage(int val)
	{
		val = std::min(std::max(0, val), 100);
		if (m_progressPercentage != val)
		{
			m_progressPercentage = val;
			return true;
		}
		return false;
	}

	auto progressPercentage() const { return m_progressPercentage; }
	void setShowProgress(bool val) { m_showProgress = val; }
	auto showProgress() const { return m_showProgress; }

protected:
	QStyle* astyle() { return p_style ? p_style : style(); }

	void paintEvent(QPaintEvent* evt)
	{
		QStyleOptionProgressBar pbstyle;
		pbstyle.initFrom(this);
		pbstyle.minimum = 0;
		pbstyle.progress = m_showProgress ? m_progressPercentage : 0;
		pbstyle.maximum = 100;
		pbstyle.text = m_showProgress ? QString("%1 (%2%)").arg(text()).arg(m_progressPercentage) : text();
		pbstyle.textAlignment = alignment();
		pbstyle.rect = rect();

		if (m_showProgress && m_progressPercentage > 0)
		{
			// let's print the frame border etc.
			// note we're skipping QLabel code to avoid overlapping label text.
			QFrame::paintEvent(evt);

			// we deliberately don't draw the progress bar groove to avoid a dramatic
			// change in the progress bar.
			QPainter painter(this);
			pbstyle.textVisible = false;
			astyle()->drawControl(QStyle::CE_ProgressBarContents, &pbstyle, &painter, this);
			pbstyle.textVisible = true;
			astyle()->drawControl(QStyle::CE_ProgressBarLabel, &pbstyle, &painter, this);
		}
		else
		{
			QLabel::paintEvent(evt);
		}
	}

private:
	Q_DISABLE_COPY(XrayProgressBarWidgetLabel);

	int m_progressPercentage;
	bool m_showProgress;
	QStyle* p_style;
};

//-----------------------------------------------------------------------------
XrayProgressBarWidget::XrayProgressBarWidget(QWidget* _parent /*=0*/) : 
	Superclass(_parent, Qt::FramelessWindowHint), 
	m_busyText(QApplication::translate("XrayProgressBarWidget", "Busy"))
{
	auto hbox = new QHBoxLayout(this);
	hbox->setSpacing(2);
	hbox->setMargin(0);

	p_abortButton = new QToolButton(this);
	p_abortButton->setObjectName("AbortButton");
	p_abortButton->setIcon(QIcon(":/paint/images/delete_icon.png"));
	p_abortButton->setIconSize(QSize(12, 12));
	p_abortButton->setToolTip(tr("Abort"));
	p_abortButton->setEnabled(false);
	connect(p_abortButton, &QToolButton::pressed, this, &XrayProgressBarWidget::abortPressed);
	hbox->addWidget(p_abortButton);

	p_progressLabel = new XrayProgressBarWidgetLabel(this);
	p_progressLabel->setObjectName("ProgressBar");
	p_progressLabel->setAlignment(Qt::AlignCenter);
	p_progressLabel->setText(readyText());
	hbox->addWidget(p_progressLabel, 1);
}

XrayProgressBarWidget::~XrayProgressBarWidget()
{
	delete p_progressLabel;
	delete p_abortButton;
}

void XrayProgressBarWidget::setReadyText(const QString& txt)
{
	m_readyText = txt;
	if (!p_progressLabel->showProgress())
		p_progressLabel->setText(txt);
}

void XrayProgressBarWidget::setBusyText(const QString& txt)
{
	m_busyText = txt;
	if (p_progressLabel->showProgress())
		p_progressLabel->setText(txt);
}

void XrayProgressBarWidget::setProgress(const QString& message, int value)
{
	if (p_progressLabel->showProgress() && (p_progressLabel->progressPercentage() != value || p_progressLabel->text() != message))
	{
		p_progressLabel->setText(message);
		if (p_progressLabel->setProgressPercentage(value))
			updateUI();
	}
}

void XrayProgressBarWidget::enableProgress(bool enabled)
{
	if (p_progressLabel->showProgress() != enabled)
	{
		p_progressLabel->setShowProgress(enabled);
		auto changed = p_progressLabel->setProgressPercentage(0);
		p_progressLabel->setText(enabled ? busyText() : readyText());
		if (changed)
			updateUI();
	}
}

void XrayProgressBarWidget::updateUI()
{
	p_progressLabel->repaint();
	p_abortButton->repaint();
}

void XrayProgressBarWidget::enableAbort(bool enabled)
{
	p_abortButton->setEnabled(enabled);
}

XRAYLAB_END_NAMESPACE
