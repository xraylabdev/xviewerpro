/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/11
** filename: 	XrayImageToolButton.cpp
** file base:	XrayImageToolButton
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a tool button for image and text.
****************************************************************************/

#include "XrayImageToolButton.h"

#include <QLayout>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayImageToolButton::XrayImageToolButton(int _index, const QString& _imgFile, const QString& _imgText, QWidget* _parent) :
	QToolButton(_parent),
	m_hovered(false),
	m_pressed(false),
	m_imgFile(_imgFile),	// do not use std::move here.
	m_imgText(_imgText),
	m_index(_index)
{
	//auto& f = const_cast<QFont&>(font());
	////f.setWeight(QFont::Bold);
	//f.setPointSize(8);

	setStyleSheet("QToolButton { border: 0px; }");
	setMouseTracking(true);
	setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	setIcon(QPixmap(_imgFile));
	setIconSize(QSize(64, 64));
	setText(_imgText);
	connect(this, &QToolButton::pressed, this, &XrayImageToolButton::pressedSlot);
}

void XrayImageToolButton::enterEvent(QEvent* _ev)
{
	emit mouseEnter();
	setToUnpressed(true);
	QWidget::enterEvent(_ev);
}
void XrayImageToolButton::leaveEvent(QEvent* _ev)
{
	emit mouseLeave();
	setToUnpressed(false);
	QWidget::leaveEvent(_ev);
}
void XrayImageToolButton::setToUnpressed(bool _b)
{
	if (_b != m_hovered)
	{
		m_hovered = _b;
		update();
	}
}
void XrayImageToolButton::setToPressed(bool _b)
{
	if (_b != m_pressed)
	{
		m_pressed = _b;
		update();
	}
}

void XrayImageToolButton::pressedSlot()
{
	setToPressed(true);
	emit parentSignal(this);
}

void XrayImageToolButton::paintEvent(QPaintEvent* _ev)
{
	QPainter painter(this);
	if (m_pressed)
	{
		painterinfo(200, 100, &painter);
	}
	else if (m_hovered)
	{
		painterinfo(100, 50, &painter);
	}
	QToolButton::paintEvent(_ev);
}
void XrayImageToolButton::painterinfo(int _topPartOpacity, int _bottomPartOpacity, QPainter *_painter)
{
	auto top = palette().color(QPalette::Highlight);
	top.setAlpha(_topPartOpacity);
	auto bot = palette().color(QPalette::Highlight);
	bot.setAlpha(_bottomPartOpacity);

	const QPen pen(Qt::NoBrush, 1);
	_painter->setPen(pen);
	QLinearGradient g(rect().topLeft(), rect().bottomLeft());
	g.setColorAt(0, top);
	g.setColorAt(1, bot);
	const QBrush brush(g);
	_painter->setBrush(brush);
	_painter->drawRoundedRect(rect(), 5, 5);
}