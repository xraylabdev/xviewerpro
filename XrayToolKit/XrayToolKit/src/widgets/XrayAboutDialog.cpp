/*********************************************************************************
created:	2018/07/09   03:37AM
filename: 	XrayAboutDialog.cpp
file base:	XrayAboutDialog
file ext:	h
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class that creates a about dialog.

/**********************************************************************************
*	Fast Visualization Kit (FVK)
*	Copyright (C) 2017 REAL3D
*
* This file and its content is protected by a software license.
* You should have received a copy of this license with this file.
* If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "XrayAboutDialog.h"
#include "ui_XrayAboutDialog.h"

XRAYLAB_USING_NAMESPACE

XrayAboutDialog::XrayAboutDialog(QWidget* _parent) :
	QDialog(_parent),
	ui(new Ui::XrayAboutDialog)
{
	ui->setupUi(this);
}
XrayAboutDialog::~XrayAboutDialog()
{
	delete ui;
}

void XrayAboutDialog::setImage(const QPixmap& _image)
{
	ui->imageLabel->setPixmap(_image);
}

void XrayAboutDialog::setTitle(const QString& _text)
{
	ui->applicationTitle->setText(_text);
}

void XrayAboutDialog::setVersion(const QString& _text)
{
	ui->version->setText(_text);
}

void XrayAboutDialog::setReleaseDate(const QString& _text)
{
	ui->releaseDate->setText(_text);
}

void XrayAboutDialog::setLicenseType(const QString& _text)
{
	ui->licenseType->setText(_text);
}

void XrayAboutDialog::setModule(const QString& _text)
{
	ui->moduleName->setText(_text);
}

void XrayAboutDialog::setCopyRight(const QString& _text)
{
	ui->copyRight->setText(_text);
}

void XrayAboutDialog::setWebsite(const QString& _text)
{
	ui->website->setText(_text);
}
