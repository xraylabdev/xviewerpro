/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/28
** filename: 	XrayQApplication.cpp
** file base:	XrayQApplication
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a QApplication object with some
				pre-defined attributes.
****************************************************************************/

#include "XrayQApplication.h"
#include "XrayGlobal.h"

#include <QMetaType>
#include <QTextCodec>
#include <QVector>
#include <QPair>
#include <QByteArray>
#include <QDataStream>
#include <QPolygonF>
#include <QPersistentModelIndex>
#include <QSharedMemory>
#include <QFileInfo>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

static QString s_AppBuildDate;			// contains the application build date
static QTranslator s_ApiTranslator;		// contains the translations for XrayToolKit api
static QTranslator s_QtTranslator;		// contains the translations for qt
static QTranslator s_QtBaseTranslator;	// contains the translations for qtbase

Q_DECLARE_METATYPE(QStringList)

typedef QPair<QString, QVector<QPair<QString, QString> > > QPairStrVectorPairDef;
Q_DECLARE_METATYPE(QPairStrVectorPairDef)
typedef QVector<QPairStrVectorPairDef> QVectorPairStrVectorPairDef;
Q_DECLARE_METATYPE(QVectorPairStrVectorPairDef)

typedef QPair<QString, QStringList> QPairStrStrListDef;
Q_DECLARE_METATYPE(QPairStrStrListDef)
typedef QVector<QPairStrStrListDef> QVectorPairStrStrListDef;
Q_DECLARE_METATYPE(QVectorPairStrStrListDef)
typedef QPair<QString, QString> QPairStrStrDef;
Q_DECLARE_METATYPE(QPairStrStrDef)
typedef QVector<QPairStrStrDef> QVectorPairStrStrDef;
Q_DECLARE_METATYPE(QVectorPairStrStrDef)

typedef QPair<QString, QByteArray> QPairStrByteDef;
Q_DECLARE_METATYPE(QPairStrByteDef)
typedef QVector<QPairStrByteDef> QVectorPairStrByteDef;
Q_DECLARE_METATYPE(QVectorPairStrByteDef)

Q_DECLARE_METATYPE(QVector<QByteArray>)

Q_DECLARE_METATYPE(QVector<QPoint>)
Q_DECLARE_METATYPE(QVector<QPointF>)
Q_DECLARE_METATYPE(QPolygonF)
typedef QPair<QPolygonF, QPolygonF> QPairPolygonFDef;
Q_DECLARE_METATYPE(QPairPolygonFDef)

QDataStream &operator << (QDataStream& out, const QVectorPairStrVectorPairDef& vec)
{
	auto s = vec.size();
	out << s;
	if (s)
	{
		for (auto i = 0; i < s; ++i)
		{
			out << vec[i].first;
			auto ls = vec[i].second.size();
			out << ls;
			if (ls)
			{
				for (auto j = 0; j < ls; ++j)
				{
					out << vec[i].second[j].first;
					out << vec[i].second[j].second;
				}
			}
		}
	}
	return out;
}
QDataStream &operator >> (QDataStream& in, QVectorPairStrVectorPairDef& vec)
{
	if (!vec.empty())
		vec.clear();

	int s;
	in >> s;
	if (s)
	{
		vec.reserve(s);
		for (auto i = 0; i < s; ++i)
		{
			QString f;
			in >> f;
			int ls;
			in >> ls;
			QVector<QPair<QString, QString> > list;
			list.reserve(ls);
			if (ls)
			{
				for (auto j = 0; j < ls; ++j)
				{
					QString str, var;
					in >> str;
					in >> var;
					list.push_back({ str, var });
				}
			}
			vec.push_back(QPairStrVectorPairDef(f, list));
		}
	}
	return in;
}

QDataStream &operator << (QDataStream& out, const QVectorPairStrStrListDef& vec)
{
	auto s = vec.size();
	out << s;
	if (s)
	{
		for (auto i = 0; i < s; ++i)
		{
			out << vec[i].first;
			auto ls = vec[i].second.size();
			out << ls;
			if (ls)
			{
				for (auto j = 0; j < ls; ++j)
					out << vec[i].second[j];
			}
		}
	}
	return out;
}
QDataStream &operator >> (QDataStream& in, QVectorPairStrStrListDef& vec)
{
	if (!vec.empty())
		vec.clear();

	int s;
	in >> s;
	if (s)
	{
		vec.reserve(s);
		for (auto i = 0; i < s; ++i)
		{
			QString f;
			in >> f;
			int ls;
			in >> ls;
			QStringList list;
			list.reserve(ls);
			if (ls)
			{
				for (auto j = 0; j < ls; ++j)
				{
					QString str;
					in >> str;
					list.append(str);
				}
			}
			vec.push_back(QPairStrStrListDef(f, list));
		}
	}
	return in;
}
QDataStream &operator << (QDataStream& out, const QVectorPairStrStrDef& vec)
{
	auto s = vec.size();
	out << s;
	if (s)
	{
		for (auto i = 0; i < s; ++i)
			out << vec[i].first << vec[i].second;
	}
	return out;
}
QDataStream &operator >> (QDataStream& in, QVectorPairStrStrDef& vec)
{
	if (!vec.empty())
		vec.clear();

	int s;
	in >> s;
	if (s)
	{
		vec.reserve(s);
		for (auto i = 0; i < s; ++i)
		{
			QString f, sec;
			in >> f >> sec;
			vec.push_back(QPairStrStrDef(f, sec));
		}
	}
	return in;
}

QDataStream &operator << (QDataStream& out, const QVectorPairStrByteDef& vec)
{
	auto s = vec.size();
	out << s;
	if (s)
	{
		for (auto i = 0; i < s; ++i)
			out << vec[i].first << vec[i].second;
	}
	return out;
}
QDataStream &operator >> (QDataStream& in, QVectorPairStrByteDef& vec)
{
	if (!vec.empty())
		vec.clear();

	int s;
	in >> s;
	if (s)
	{
		vec.reserve(s);
		for (auto i = 0; i < s; ++i)
		{
			QString f;
			QByteArray sec;
			in >> f >> sec;
			vec.push_back(QPairStrByteDef(f, sec));
		}
	}
	return in;
}

XrayQApplication::XrayQApplication(int &argc, char **argv) :
	QApplication(argc, argv)
{
	qRegisterMetaType<QStringList>("QStringList");
	qRegisterMetaType<QPairStrVectorPairDef>("QPairStrVectorPairDef");
	qRegisterMetaType<QVectorPairStrVectorPairDef>("QVectorPairStrVectorPairDef");

	qRegisterMetaType<QPairStrStrListDef>("QPairStrStrListDef");
	qRegisterMetaType<QVectorPairStrStrListDef>("QVectorPairStrStrListDef");

	qRegisterMetaType<QPairStrStrDef>("QPairStrStrDef");
	qRegisterMetaType<QVectorPairStrStrDef>("QVectorPairStrStrDef");
	qRegisterMetaType<QVector<int> >("QVector<int>");
	qRegisterMetaType<QVector<QString> >("QVector<QString>");
	qRegisterMetaType<QVector<QByteArray> >("QVector<QByteArray>");
	qRegisterMetaType<QVector<QPoint> >("QVector<QPoint>");
	qRegisterMetaType<QVector<QPointF> >("QVector<QPointF>");
	qRegisterMetaType<QPolygonF>("QPolygonF");
	qRegisterMetaType<QPairPolygonFDef>("QPairPolygonFDef");
	qRegisterMetaType<QList<QPersistentModelIndex> >("QList<QPersistentModelIndex>");
	qRegisterMetaType<QMap<QString, QString> >("QMap<QString, QString>");
	qRegisterMetaType<QMap<int, QString> >("QMap<int, QString>");
	qRegisterMetaType<QPairStrByteDef>("QPairStrByteDef");
	qRegisterMetaType<QVectorPairStrByteDef>("QVectorPairStrByteDef");

	qRegisterMetaTypeStreamOperators<QStringList>("QStringList");
	qRegisterMetaTypeStreamOperators<QPairStrVectorPairDef>("QPairStrVectorPairDef");
	qRegisterMetaTypeStreamOperators<QVectorPairStrVectorPairDef>("QVectorPairStrVectorPairDef");

	qRegisterMetaTypeStreamOperators<QPairStrStrListDef>("QPairStrStrListDef");
	qRegisterMetaTypeStreamOperators<QVectorPairStrStrListDef>("QVectorPairStrStrListDef");

	qRegisterMetaTypeStreamOperators<QPairStrStrDef>("QPairStrStrDef");
	qRegisterMetaTypeStreamOperators<QVectorPairStrStrDef>("QVectorPairStrStrDef");
	qRegisterMetaTypeStreamOperators<QVector<int> >("QVector<int>");
	qRegisterMetaTypeStreamOperators<QVector<QString> >("QVector<QString>");
	qRegisterMetaTypeStreamOperators<QVector<QByteArray> >("QVector<QByteArray>");
	qRegisterMetaTypeStreamOperators<QVector<QPoint> >("QVector<QPoint>");
	qRegisterMetaTypeStreamOperators<QVector<QPointF> >("QVector<QPointF>");
	qRegisterMetaTypeStreamOperators<QPolygonF>("QPolygonF");
	qRegisterMetaTypeStreamOperators<QPairPolygonFDef>("QPairPolygonFDef");
	qRegisterMetaTypeStreamOperators<QMap<QString, QString> >("QMap<QString, QString>");
	qRegisterMetaTypeStreamOperators<QMap<int, QString> >("QMap<int, QString>");
	qRegisterMetaTypeStreamOperators<QPairStrByteDef>("QPairStrByteDef");
	qRegisterMetaTypeStreamOperators<QVectorPairStrByteDef>("QVectorPairStrByteDef");

	connect(this, &QApplication::lastWindowClosed, this, &QApplication::quit);
}

XrayQApplication::~XrayQApplication()
{
}

static void appMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& message)
{
	auto file = context.file ? QFileInfo(context.file).fileName() : "";
	auto func = context.function ? context.function : "";

	auto msg = message + " => at [{}] in [{}]";

	switch (type)
	{
	case QtDebugMsg:
		xAppInfo(message.toLocal8Bit().constData());
		break;

	case QtWarningMsg:
		xAppWarn(msg.toLocal8Bit().constData(), file, func);
		break;

	case QtCriticalMsg:
		xAppCritical(msg.toLocal8Bit().constData(), file, func);
		break;

	case QtFatalMsg:
		xAppError(msg.toLocal8Bit().constData(), file, func);
		break;

	case QtInfoMsg:
		xAppInfo(msg.toLocal8Bit().constData(), file, func);
		break;

	default:
		xAppInfo(msg.toLocal8Bit().constData(), file, func);
		break;
	}
}
void XrayQApplication::installMessageHandler()
{
	qInstallMessageHandler(appMessageHandler);
}

void XrayQApplication::setGlobalAttributes()
{
	QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
	QApplication::setAttribute(Qt::AA_DisableWindowContextHelpButton);	// disable all windows title bar help button globally.
#endif
	QApplication::setAttribute(Qt::AA_DontShowIconsInMenus, false);		// force showing the icons in the menus even if the native OS style discourages it
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
	//   Round:            Round up for .5 and above.
	//   Ceil:             Always round up.
	//   Floor:            Always round down.
	//   RoundPreferFloor: Round up for .75 and above.
	//   PassThrough:      Don't round.
	QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
	QGuiApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::Round);
#endif
}

#ifdef Q_OS_WIN
#include <Windows.h>
#endif

void XrayQApplication::bringToFront()
{
	if (applicationName().isEmpty())
		return;

#ifdef Q_OS_WIN
	auto hWnd = ::FindWindow(NULL, applicationName().toStdWString().c_str());
	if (NULL != hWnd)
	{
		auto hForeWnd = ::GetForegroundWindow();
		auto dwForeID = ::GetWindowThreadProcessId(hForeWnd, NULL);
		auto dwCurID = ::GetCurrentThreadId();

		::AttachThreadInput(dwCurID, dwForeID, TRUE);
		if (::IsZoomed(hWnd))
			SendMessage(hWnd, WM_SYSCOMMAND, SC_MAXIMIZE, 0);
		else
			SendMessage(hWnd, WM_SYSCOMMAND, SC_RESTORE, 0);

		::SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
		::SetWindowPos(hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
		::SetForegroundWindow(hWnd);
		::AttachThreadInput(dwCurID, dwForeID, FALSE);
	}
#endif
}
void XrayQApplication::startAsSingleton(const QString& key)
{
	bool isSingle = false;
	static QSharedMemory lock(key);
	if (lock.attach(QSharedMemory::ReadOnly))
		lock.detach();

	if (lock.create(1))
		isSingle = true;

	if (!isSingle)
	{
		bringToFront();
		std::exit(0);	// use std::exit instead just exit(), exit is inherited from QApplication.
	}
}

void XrayQApplication::setApplicationBuildDate(const QString& date)
{
	s_AppBuildDate = date;
}
QString XrayQApplication::applicationBuildDate()
{
	return s_AppBuildDate;
}

void switchTranslator(QTranslator& _translator, const QString& fileName)
{
	QApplication::removeTranslator(&_translator);	// remove the old translator first
	if (_translator.load(fileName))
		QApplication::installTranslator(&_translator);
	else
		xAppInfo("Couldn't load {}", fileName);
}

void XrayQApplication::loadLanguage(const QString& lang)
{
	const auto locale = QLocale(lang);
	qDebug() << locale;
	QLocale::setDefault(locale);
	qDebug() << QLocale::languageToString(QLocale().language());
	const auto path = ":/language/";
	switchTranslator(s_QtTranslator, path + QString("qt_%1.qm").arg(lang));
	switchTranslator(s_QtBaseTranslator, path + QString("qtbase_%1.qm").arg(lang));
	switchTranslator(s_ApiTranslator, path + QString("xraytoolkit_%1.qm").arg(lang));
}