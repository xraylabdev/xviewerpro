/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/17/07
** filename: 	XrayStandardModelCompleterTreeViewWidget.cpp
** file base:	XrayStandardModelCompleterTreeViewWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a table view.
****************************************************************************/

#include "XrayStandardModelCompleterTreeViewWidget.h"
#include "XrayStandardModelTreeView.h"

#include <QApplication>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayStandardModelCompleterTreeViewWidget::XrayStandardModelCompleterTreeViewWidget(int menuAction, QWidget* parent) :
	QWidget(parent),
	p_completerModel(nullptr)
{
	p_lineEdit = new QLineEdit(this);
	p_lineEdit->setFocus();
	p_lineEdit->setPlaceholderText(QApplication::translate("XrayStandardModelCompleterTreeViewWidget", "Search..."));

	p_view = new XrayStandardModelTreeView(menuAction, this);

	p_completer = new QCompleter(p_lineEdit);
	p_completer->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
	p_completer->setCaseSensitivity(Qt::CaseInsensitive);
	p_completer->setFilterMode(Qt::MatchContains);
	p_completer->setCompletionColumn(0);
	p_completer->setMaxVisibleItems(10);
	p_completer->setCompletionRole(Qt::DisplayRole);
	p_completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
	p_lineEdit->setCompleter(p_completer);

	QObject::connect(p_lineEdit, &QLineEdit::textChanged, [this](const QString&)
	{
		if (p_completerModel) 
			delete p_completerModel;
		p_completerModel = new QStringListModel();
		p_completerModel->setStringList(p_view->sourceModel()->findIndicesTexts(false));
		p_completer->setModel(p_completerModel);
	});

	connect(p_completer, QOverload<const QString&>::of(&QCompleter::activated), p_view, &XrayStandardModelTreeView::setCurrentChildIndex);

	connect(p_view, &XrayStandardModelTreeView::pressed, [this](const QModelIndex &index) { emit pressed(index.data(Qt::UserRole).toString()); });
	connect(p_view, &XrayStandardModelTreeView::activatedItem, [this](const QModelIndex &index) { emit activatedItem(index.data(Qt::UserRole).toString()); });
	connect(p_view, &XrayStandardModelTreeView::removedItem, this, &XrayStandardModelCompleterTreeViewWidget::removedItem);

	vLayout = new QVBoxLayout;
	vLayout->addWidget(p_lineEdit);
	vLayout->addWidget(p_view);
	setLayout(vLayout);
}
