/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/12
** filename: 	XrayQCalendarDialog.cpp
** file base:	XrayQCalendarDialog
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a dialog with calendar widget.
****************************************************************************/

#include "XrayQCalendarDialog.h"

#include <QApplication>
#include <QSettings>
#include <QDialogButtonBox>

XRAYLAB_USING_NAMESPACE

XrayQCalendarDialog::XrayQCalendarDialog(QWidget* _parent, Qt::WindowFlags _flags) :
	QDialog(_parent, _flags)
{
	setWindowTitle("Calendar");

	p_layout = new QGridLayout;

	p_calendar = new QCalendarWidget(this);
	p_calendar->setMinimumDate(QDate(2018, 1, 1));
	p_calendar->setMaximumDate(QDate(2099, 1, 1));
	p_calendar->setGridVisible(true);

	p_layout->addWidget(p_calendar);

	auto buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
	connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

	connect(buttonBox, &QDialogButtonBox::accepted, [this]() { saveSettings(); });
	connect(buttonBox, &QDialogButtonBox::rejected, [this]() { saveSettings(); });

	p_layout->addWidget(buttonBox);

	setLayout(p_layout);

	// restore settings
	QSettings settings(QApplication::organizationName(), windowTitle() + "_Settings");
	restoreGeometry(settings.value("CalendarDialog/geometry", geometry()).toByteArray());
}

void XrayQCalendarDialog::saveSettings()
{
	QSettings settings(QApplication::organizationName(), windowTitle() + "_Settings");
	settings.setValue("CalendarDialog/geometry", saveGeometry());
}

void XrayQCalendarDialog::setMinimumDate(const QDate& _date)
{
	p_calendar->setMinimumDate(_date);
}
void XrayQCalendarDialog::setMaximumDate(const QDate& _date)
{
	p_calendar->setMaximumDate(_date);
}
QDate XrayQCalendarDialog::selectedDate() const
{
	return p_calendar->selectedDate();
}

void XrayQCalendarDialog::closeEvent(QCloseEvent* _event)
{
	saveSettings();
	_event->accept();
}