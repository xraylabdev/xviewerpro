/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/18
** filename: 	XrayQListWidget.cpp
** file base:	XrayQListWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a list widget.
****************************************************************************/

#include "XrayQListWidget.h"

#include <QApplication>
#include <QClipboard>

XRAYLAB_USING_NAMESPACE

XrayQListWidget::XrayQListWidget(QWidget *parent) : 
	QListWidget(parent),
	m_menuAction(static_cast<MenuAction>(MenuAction::SelectAll | MenuAction::InvertSelection | MenuAction::Copy | MenuAction::Paste | MenuAction::RemoveSelected))
{
	setSortingEnabled(false);
	setAlternatingRowColors(true);
	setEditTriggers(QListWidget::EditTrigger::NoEditTriggers);

	setSelectionMode(QAbstractItemView::SelectionMode::MultiSelection);

	createCustomMenu();
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QListWidget::customContextMenuRequested, this, &XrayQListWidget::showContextMenu);
}
XrayQListWidget::~XrayQListWidget() 
{
}

void XrayQListWidget::showContextMenu(const QPoint& _point)
{
	if (!itemAt(_point))
		return;

	m_currentPoint = _point;
	p_contextMenu->exec(viewport()->mapToGlobal(_point));
	//p_contextMenu->exec(QCursor::pos());
}
void XrayQListWidget::createCustomMenu()
{
	p_contextMenu = new QMenu(this);

	if (m_menuAction & MenuAction::SelectAll)
	{
		auto action = new QAction(QIcon(":/paint/images/select_all_blue_icon.png"), QApplication::translate("XrayQListWidget", "Select All"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			selectAll();
		});
		p_contextMenu->addAction(action);
	}

	if (m_menuAction & MenuAction::InvertSelection)
	{
		auto action = new QAction(QIcon(":/paint/images/invert_selection_blue_icon.png"), QApplication::translate("XrayQListWidget", "Invert Selection"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			for (auto i = 0; i < count(); i++)
			{
				if (item(i)->isSelected()) item(i)->setSelected(false);
				else item(i)->setSelected(true);
			}
		});
		p_contextMenu->addAction(action);
		p_contextMenu->addSeparator();
	}

	if (m_menuAction & MenuAction::Copy)
	{
		auto action = new QAction(QIcon(":/paint/images/copy_blue_icon.png"), QApplication::translate("XrayQListWidget", "Copy"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			QString text;
			auto items = selectedItems();
			for (auto& i : items)
				text += i->text() + "\n";
			if (!text.isEmpty())
				QApplication::clipboard()->setText(text);

			emit copy();
		});
		p_contextMenu->addAction(action);
	}

	if (m_menuAction & MenuAction::Paste)
	{
		auto action = new QAction(QIcon(":/paint/images/paste_blue_icon.png"), QApplication::translate("XrayQListWidget", "Paste"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			auto text = QApplication::clipboard()->text();
			if (!text.isEmpty())
			{
				auto list = text.split('\n');
				if (!list.isEmpty())
					addItems(list);
			}
			emit paste();
		});
		p_contextMenu->addAction(action);
		p_contextMenu->addSeparator();
	}

	if (m_menuAction & MenuAction::RemoveSelected)
	{
		auto action = new QAction(QIcon(":/paint/images/delete_small_icon.png"), QApplication::translate("XrayQListWidget", "Remove Selected"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			auto items = selectedItems();

			std::vector<int> rows;
			rows.resize(items.count());
			for (auto i = 0; i < items.count(); i++)
				rows[i] = row(items[i]);

			for (auto& i : items)
				delete takeItem(row(i));

			if (!items.isEmpty())
				emit removed(rows);
		});
		p_contextMenu->addAction(action);
	}
}