/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayQThread.cpp
** file base:	XrayQThread
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a thread with pause and delay support.
****************************************************************************/

#include "XrayQThread.h"

#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayQThread::XrayQThread(QObject* parent) :
	QThread(parent),
	m_ispause(false),
	m_isstop(false),
	m_callback(nullptr),
	m_delay(5000)
{
}

XrayQThread::~XrayQThread()
{
	if (isRunning())
	{
		stop();
		quit();
		wait();
	}
}

void XrayQThread::run()
{
	m_isstop = false;

	while (true)
	{	
		// stop this thread
		m_stopmutex.lock();
		if (m_isstop)
		{
			m_isstop = false;
			m_stopmutex.unlock();
			break;
		}
		m_stopmutex.unlock();

		// pause this thread
		m_pausemutex.lock();
		if (m_ispause)
			m_pausecond.wait(&m_pausemutex);	// in this place, your thread will stop to execute until someone calls resume.
		m_pausemutex.unlock();
		
		msleep(m_delay);

		if(m_callback)
			m_callback();
	}
}

void XrayQThread::stop()
{
	QMutexLocker locker(&m_stopmutex);
	m_isstop = true;
}
void XrayQThread::pause()
{
	QMutexLocker locker(&m_pausemutex);
	m_ispause = true;
}
void XrayQThread::resume()
{
	{
		QMutexLocker locker(&m_pausemutex);
		m_ispause = false;
	}
	m_pausecond.wakeAll();
}
bool XrayQThread::isPause()
{
	QMutexLocker locker(&m_pausemutex);
	return m_ispause;
}
void XrayQThread::setCallback(std::function<void()> _callback)
{
	m_callback = _callback;
}
void XrayQThread::setDelay(unsigned long _delay)
{
	m_delay = _delay;
}
unsigned long XrayQThread::getDelay() const
{
	return m_delay;
}
