/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/13
** filename: 	XrayFramelessWindowWin32.h
** file base:	XrayFramelessWindowWin32
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a frame less main window.
****************************************************************************/

#include "XrayFramelessWindowWin32.h"

#include <dwmapi.h>
#include <Windows.h>
#include <windowsx.h>

#include <QApplication>
#include <QDesktopWidget>
#include <QWindow>
#include <QMenuBar>
#include <QImage>
#include <QLayout>
#include <QSettings>
#include <QDebug>
#pragma comment(lib, "dwmapi.lib")

XRAYLAB_USING_NAMESPACE

namespace
{
	// we cannot just use WS_POPUP style
	// WS_THICKFRAME: without this the window cannot be resized and so aero snap, de-maximizing and minimizing won't work
	// WS_SYSMENU: enables the context menu with the move, close, maximize, minimize... commands (shift + right-click on the task bar item)
	// WS_CAPTION: enables aero minimize animation/transition
	// WS_MAXIMIZEBOX, WS_MINIMIZEBOX: enable minimize/maximize
	enum class Style : DWORD
	{
		windowed = WS_OVERLAPPEDWINDOW | WS_THICKFRAME | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
		aero_borderless = WS_POPUP | WS_THICKFRAME | WS_CAPTION | WS_SYSMENU | WS_MAXIMIZEBOX | WS_MINIMIZEBOX,
		basic_borderless = WS_POPUP | WS_THICKFRAME | WS_SYSMENU | WS_MAXIMIZEBOX | WS_MINIMIZEBOX
	};

	auto maximized(HWND hwnd) -> bool
	{
		WINDOWPLACEMENT placement;
		if (!::GetWindowPlacement(hwnd, &placement))
		{
			return false;
		}

		return placement.showCmd == SW_MAXIMIZE;
	}

	/* Adjust client rect to not spill over monitor edges when maximized.
	* rect(in/out): in: proposed window rect, out: calculated client rect
	* Does nothing if the window is not maximized.
	*/
	auto adjustMaximizedClientRect(HWND _window, RECT& _rect) -> void
	{
		if (!maximized(_window))
		{
			return;
		}

		const auto monitor = ::MonitorFromWindow(_window, MONITOR_DEFAULTTONULL);
		if (!monitor)
		{
			return;
		}

		MONITORINFO monitor_info{};
		monitor_info.cbSize = sizeof(monitor_info);
		if (!::GetMonitorInfoW(monitor, &monitor_info))
		{
			return;
		}

		// when maximized, make the client area fill just the monitor (without task bar) rect,
		// not the whole window rect which extends beyond the monitor.
		_rect = monitor_info.rcWork;
	}

	auto compositionEnabled() -> bool
	{
		auto composition_enabled = FALSE;
		const auto success = ::DwmIsCompositionEnabled(&composition_enabled) == S_OK;
		return composition_enabled && success;
	}

	auto selectBorderlessStyle() -> Style
	{
		return compositionEnabled() ? Style::aero_borderless : Style::basic_borderless;
	}

	auto setShadow(HWND handle, bool enabled) -> void 
	{
		if (compositionEnabled())
		{
			static const MARGINS shadow_state[2]{ { 0,0,0,0 }, { 1,1,1,1 } };
			::DwmExtendFrameIntoClientArea(handle, &shadow_state[enabled]);
		}
	}
}

XrayFramelessWindowWin32::XrayFramelessWindowWin32(const QString& _title, QWidget *parent, Qt::WindowFlags flags) :
	QMainWindow(parent, flags),
	p_minimizeButton(nullptr), 
	p_maximizeButton(nullptr),
	p_restoreButton(nullptr),
	p_closeButton(nullptr),
	p_titleLabel(nullptr),
	p_iconButton(nullptr),
	m_clear_settings(false),
	p_settings(new QSettings(QApplication::organizationName(), _title))
{
	auto horizontalLayout = new QHBoxLayout();
	horizontalLayout->setSpacing(0);
	horizontalLayout->setMargin(0);
	horizontalLayout->setContentsMargins(8, 1, 1, 0);

	//Represent the window title bar.
	p_titleLabel = new QLabel(this);
	p_titleLabel->setFixedHeight(30);
	p_titleLabel->setFocusPolicy(Qt::StrongFocus);
	p_titleLabel->setObjectName("windowTitleBar");
	p_titleLabel->setStyleSheet("#windowTitleBar { border: 0px; font-size: 12px; font-weight: normal; color: rgb(200, 200, 200); }");
	p_titleLabel->setContentsMargins(0, 0, 0, 0);
	p_titleLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	p_titleLabel->setAlignment(Qt::AlignCenter);
	p_titleLabel->setLayout(horizontalLayout);

	setWindowTitle(_title);
	setWindowFlags(Qt::FramelessWindowHint);
	setBorderless(true, true);

	p_iconButton = new QToolButton(p_titleLabel);
	p_iconButton->setIcon(QApplication::windowIcon());
	p_iconButton->setIconSize(QSize(20, 20));
	p_iconButton->setObjectName("titleBarIcon");
	p_iconButton->setFocusPolicy(Qt::NoFocus);
	p_iconButton->setStyleSheet("#titleBarIcon { margin-left: 5px; background-color: transparent; border: none; }");
	connect(p_iconButton, SIGNAL(clicked()), this, SLOT(slot_show_system_menu()));

	//Represent the minimize button.
	p_menuButton = new QToolButton(p_titleLabel);
	p_menuButton->setFixedSize(36, 29);
	p_menuButton->setFocusPolicy(Qt::StrongFocus);
	p_menuButton->setObjectName("menuButton");
	p_menuButton->setToolTip(QApplication::translate("XrayFramelessWindowWin32", "Menu"));
	p_menuButton->setPopupMode(QToolButton::InstantPopup);
	p_menuButton->setStyleSheet(
		"#menuButton{ background-color:none; border:none; image:url(:/res/images/menu_indicator_light.png); }"
		"#menuButton:hover{ background-color: palette(alternate-base); }"
		"#menuButton:pressed{ background-color: palette(highlight); }"
		//"#menuButton:menu-indicator{ width: 8px; height: 8px; subcontrol-position: center center; subcontrol-origin: padding; left: 0; }"
		"#menuButton:menu-indicator{ image: none; }"
	);
	p_menuButton->setVisible(false);

	//Represent the minimize button.
	p_minimizeButton = new QToolButton(p_titleLabel);
	p_minimizeButton->setFixedSize(36, 29);
	p_minimizeButton->setFocusPolicy(Qt::StrongFocus);
	p_minimizeButton->setObjectName("minimizeButton");
	p_minimizeButton->setToolTip(QApplication::translate("XrayFramelessWindowWin32", "Minimize"));
	p_minimizeButton->setStyleSheet(
		"#minimizeButton{ background-color:none; border:none; image:url(:/res/images/window_minimize_light.png); }"
		"#minimizeButton:hover{ background-color: palette(alternate-base); }"
		"#minimizeButton:pressed{ background-color: palette(highlight); }");
	connect(p_minimizeButton, &QToolButton::clicked, this, &XrayFramelessWindowWin32::slot_minimized);

	//Represent the maximize button.
	p_maximizeButton = new QToolButton(p_titleLabel);
	p_maximizeButton->setFixedSize(36, 29);
	p_maximizeButton->setFocusPolicy(Qt::StrongFocus);
	p_maximizeButton->setObjectName("maximizeButton");
	p_maximizeButton->setToolTip(QApplication::translate("XrayFramelessWindowWin32", "Maximize"));
	p_maximizeButton->setVisible(true);
	p_maximizeButton->setStyleSheet(
		"#maximizeButton{ background-color:none; border:none; image:url(:/res/images/window_maximize_light.png); }"
		"#maximizeButton:hover{ background-color: palette(alternate-base); }"
		"#maximizeButton:pressed{ background-color: palette(highlight); }");
	connect(p_maximizeButton, &QToolButton::clicked, this, &XrayFramelessWindowWin32::slot_maximized);

	//Represent the restore button.
	p_restoreButton = new QToolButton(p_titleLabel);
	p_restoreButton->setFixedSize(36, 29);
	p_restoreButton->setFocusPolicy(Qt::StrongFocus);
	p_restoreButton->setObjectName("restoreButton");
	p_restoreButton->setToolTip(QApplication::translate("XrayFramelessWindowWin32", "Restore"));
	p_restoreButton->setVisible(false);
	p_restoreButton->setStyleSheet(
		"#restoreButton{ background-color:none; border:none; image:url(:/res/images/window_restore_light.png); }"
		"#restoreButton:hover{ background-color: palette(alternate-base); }"
		"#restoreButton:pressed{ background-color: palette(highlight); }");
	connect(p_restoreButton, &QToolButton::clicked, this, &XrayFramelessWindowWin32::slot_restored);

	//Represent the close button.
	p_closeButton = new QToolButton(p_titleLabel);
	p_closeButton->setFixedSize(36, 29);
	p_closeButton->setFocusPolicy(Qt::StrongFocus);
	p_closeButton->setObjectName("closeButton");
	p_closeButton->setToolTip(QApplication::translate("XrayFramelessWindowWin32", "Close"));
	p_closeButton->setStyleSheet(
		"#closeButton{ background-color: none; border:none; image:url(:/res/images/window_close_light.png); }"
		"#closeButton:hover{ background-color: rgb(215, 21, 38); }"
		"#closeButton:pressed{ background-color: palette(highlight); }");
	connect(p_closeButton, &QToolButton::clicked, this, &XrayFramelessWindowWin32::slot_closed);

	// add icon on the title bar.
	horizontalLayout->addWidget(p_iconButton);
	horizontalLayout->addSpacing(5);

	// add button command.
	horizontalLayout->addStretch(1);	// add space between the menubar and command icon
	horizontalLayout->addWidget(p_menuButton);
	horizontalLayout->addWidget(p_minimizeButton);
	horizontalLayout->addWidget(p_restoreButton);
	horizontalLayout->addWidget(p_maximizeButton);
	horizontalLayout->addWidget(p_closeButton);

	auto verticalLayout = new QVBoxLayout();
	verticalLayout->setAlignment(Qt::AlignTop);
	verticalLayout->setSpacing(0);
	verticalLayout->setMargin(0);

	// Add the window title bar in the first layout in vertical.
	verticalLayout->addWidget(p_titleLabel, 0, Qt::AlignTop);

	// Add an central widget of this window.
	p_centralWidget = new QWidget(this);
	p_centralWidget->setObjectName("centralWidget");
	p_centralWidget->setStyleSheet("#centralWidget { border:0px; }");
	p_centralWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	p_centralWidget->setLayout(verticalLayout);
	p_centralWidget->setContentsMargins(0, 0, 0, 0);
	setCentralWidget(p_centralWidget);
}
void XrayFramelessWindowWin32::setDarkThemeEnabled(bool _b)
{
	if (_b)
	{
		p_titleLabel->setStyleSheet("#windowTitleBar { border: 0px; font-size: 12px; font-weight: normal; color: rgb(200, 200, 200); }");

		p_menuButton->setStyleSheet(
			"#menuButton{ background-color:none; border:none; image:url(:/res/images/menu_indicator_light.png); }"
			"#menuButton:hover{ background-color: palette(alternate-base); }"
			"#menuButton:pressed{ background-color: palette(highlight); }"
			"#menuButton:menu-indicator{ image: none; }"
		);

		p_minimizeButton->setStyleSheet(
			"#minimizeButton{ background-color:none; border:none; image:url(:/res/images/window_minimize_light.png); }"
			"#minimizeButton:hover{ background-color: palette(alternate-base); }"
			"#minimizeButton:pressed{ background-color: palette(highlight); }");

		p_maximizeButton->setStyleSheet(
			"#maximizeButton{ background-color:none; border:none; image:url(:/res/images/window_maximize_light.png); }"
			"#maximizeButton:hover{ background-color: palette(alternate-base); }"
			"#maximizeButton:pressed{ background-color: palette(highlight); }");

		p_restoreButton->setStyleSheet(
			"#restoreButton{ background-color:none; border:none; image:url(:/res/images/window_restore_light.png); }"
			"#restoreButton:hover{ background-color: palette(alternate-base); }"
			"#restoreButton:pressed{ background-color: palette(highlight); }");

		p_closeButton->setStyleSheet(
			"#closeButton{ background-color: none; border:none; image:url(:/res/images/window_close_light.png); }"
			"#closeButton:hover{ background-color: rgb(215, 21, 38); }"
			"#closeButton:pressed{ background-color: palette(highlight); }");
	}
	else
	{
		p_titleLabel->setStyleSheet("#windowTitleBar { border: 0px; font-size: 12px; font-weight: normal; color: rgb(0, 0, 0); }");

		p_menuButton->setStyleSheet(
			"#menuButton{ background-color:none; border:none; image:url(:/res/images/menu_indicator_dark.png); }"
			"#menuButton:hover{ background-color: palette(alternate-base); }"
			"#menuButton:pressed{ background-color: palette(highlight); }"
			"#menuButton:menu-indicator{ image: none; }"
		);

		p_minimizeButton->setStyleSheet(
			"#minimizeButton{ background-color:none; border:none; image:url(:/res/images/window_minimize_dark.png); }"
			"#minimizeButton:hover{ background-color: palette(alternate-base); }"
			"#minimizeButton:pressed{ background-color: palette(highlight); }");

		p_maximizeButton->setStyleSheet(
			"#maximizeButton{ background-color:none; border:none; image:url(:/res/images/window_maximize_dark.png); }"
			"#maximizeButton:hover{ background-color: palette(alternate-base); }"
			"#maximizeButton:pressed{ background-color: palette(highlight); }");

		p_restoreButton->setStyleSheet(
			"#restoreButton{ background-color:none; border:none; image:url(:/res/images/window_restore_dark.png); }"
			"#restoreButton:hover{ background-color: palette(alternate-base); }"
			"#restoreButton:pressed{ background-color: palette(highlight); }");

		p_closeButton->setStyleSheet(
			"#closeButton{ background-color: none; border:none; image:url(:/res/images/window_close_dark.png); }"
			"#closeButton:hover{ background-color: rgb(215, 21, 38); }"
			"#closeButton:pressed{ background-color: palette(highlight); }");
	}
}
void XrayFramelessWindowWin32::setWindowTitle(const QString& title)
{
	p_titleLabel->setText(title);
	QMainWindow::setWindowTitle(title);
}
void XrayFramelessWindowWin32::setMenuBar(QMenuBar *menubar)
{
	if(menubar)
		qobject_cast<QHBoxLayout*>(p_titleLabel->layout())->insertWidget(2, menubar);
}
void XrayFramelessWindowWin32::addCentralWidget(QWidget* _widget)
{
	if(_widget)
		qobject_cast<QVBoxLayout*>(p_centralWidget->layout())->addWidget(_widget, 1);
}
QToolButton* XrayFramelessWindowWin32::maximumButton() const
{
	return p_maximizeButton;
}
QToolButton* XrayFramelessWindowWin32::restoreButton() const
{
	return p_restoreButton;
}
QToolButton* XrayFramelessWindowWin32::minimumButton() const
{
	return p_minimizeButton;
}
QToolButton* XrayFramelessWindowWin32::menuButton() const
{
	return p_menuButton;
}

bool XrayFramelessWindowWin32::nativeEvent(const QByteArray& eventType, void* message, long* result)
{
	Q_UNUSED(eventType);

	const auto msg = static_cast<MSG*>(message);

	switch (msg->message)
	{
	case WM_NCCREATE: 
	{
		auto userdata = reinterpret_cast<CREATESTRUCTW*>(msg->lParam)->lpCreateParams;
		// store window instance pointer in window user data
		::SetWindowLongPtrW(msg->hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(userdata));
	}

	case WM_ERASEBKGND:
	{
		auto brush = CreateSolidBrush(RGB(48, 48, 48));
		SetClassLongPtr(msg->hwnd, GCLP_HBRBACKGROUND, reinterpret_cast<LONG_PTR>(brush));
	}

	case WM_NCCALCSIZE: 
	{
		if (msg->wParam == TRUE)
		{
			auto& params = *reinterpret_cast<NCCALCSIZE_PARAMS*>(msg->lParam);
			adjustMaximizedClientRect(msg->hwnd, params.rgrc[0]);
		}
		*result = 0;
		return true;
	}

	case WM_NCHITTEST:
	{
		*result = 0;
		const LONG border_width = 8; //in pixels
		RECT winrect;
		GetWindowRect(reinterpret_cast<HWND>(winId()), &winrect);

		long x = GET_X_LPARAM(msg->lParam);
		long y = GET_Y_LPARAM(msg->lParam);

		auto resizeWidth = minimumWidth() != maximumWidth();
		auto resizeHeight = minimumHeight() != maximumHeight();

		if (resizeWidth)
		{
			//left border
			if (x >= winrect.left && x < winrect.left + border_width)
			{
				*result = HTLEFT;
			}
			//right border
			if (x < winrect.right && x >= winrect.right - border_width)
			{
				*result = HTRIGHT;
			}
		}
		if (resizeHeight)
		{
			//bottom border
			if (y < winrect.bottom && y >= winrect.bottom - border_width)
			{
				*result = HTBOTTOM;
			}
			//top border
			if (y >= winrect.top && y < winrect.top + border_width)
			{
				*result = HTTOP;
			}
		}
		if (resizeWidth && resizeHeight)
		{
			//bottom left corner
			if (x >= winrect.left && x < winrect.left + border_width &&
				y < winrect.bottom && y >= winrect.bottom - border_width)
			{
				*result = HTBOTTOMLEFT;
			}
			//bottom right corner
			if (x < winrect.right && x >= winrect.right - border_width &&
				y < winrect.bottom && y >= winrect.bottom - border_width)
			{
				*result = HTBOTTOMRIGHT;
			}
			//top left corner
			if (x >= winrect.left && x < winrect.left + border_width &&
				y >= winrect.top && y < winrect.top + border_width)
			{
				*result = HTTOPLEFT;
			}
			//top right corner
			if (x < winrect.right && x >= winrect.right - border_width &&
				y >= winrect.top && y < winrect.top + border_width)
			{
				*result = HTTOPRIGHT;
			}
		}

		if (*result != 0)
			return true;

		auto action = QApplication::widgetAt(QCursor::pos());
		if (action == p_titleLabel)
		{
			*result = HTCAPTION;
			return true;
		}
		break;
	}

	case WM_GETMINMAXINFO: 
	{
		const auto mmi = reinterpret_cast<MINMAXINFO*>(msg->lParam);

		if (maximized(msg->hwnd))
		{

			RECT window_rect;

			if (!GetWindowRect(msg->hwnd, &window_rect))
			{
				return false;

			}

			auto monitor = ::MonitorFromRect(&window_rect, MONITOR_DEFAULTTONULL);
			if (!monitor)
			{
				return false;
			}

			MONITORINFO monitor_info = { 0 };
			monitor_info.cbSize = sizeof(monitor_info);
			GetMonitorInfo(monitor, &monitor_info);

			auto work_area = monitor_info.rcWork;
			auto monitor_rect = monitor_info.rcMonitor;

			mmi->ptMaxPosition.x = abs(work_area.left - monitor_rect.left);
			mmi->ptMaxPosition.y = abs(work_area.top - monitor_rect.top);

			mmi->ptMaxSize.x = abs(work_area.right - work_area.left);
			mmi->ptMaxSize.y = abs(work_area.bottom - work_area.top);
			mmi->ptMaxTrackSize.x = mmi->ptMaxSize.x;
			mmi->ptMaxTrackSize.y = mmi->ptMaxSize.y;

			*result = 1;
			return true;
		}
	}

	case WM_NCACTIVATE: {
		if (!compositionEnabled())
		{
			// Prevents window frame reappearing on window activation
			// in "basic" theme, where no aero shadow is present.
			*result = 1;
			return true;
		}
		break;
	}

	case WM_SIZE: 
	{
		RECT winrect;
		GetClientRect(msg->hwnd, &winrect);

		WINDOWPLACEMENT wp;
		wp.length = sizeof(WINDOWPLACEMENT);
		GetWindowPlacement(msg->hwnd, &wp);
		if (this)
		{
			if (wp.showCmd == SW_MAXIMIZE)
			{
				::SetWindowPos(reinterpret_cast<HWND>(winId()), Q_NULLPTR, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE);
			}
		}
	}

	default: QWidget::nativeEvent(eventType, message, result);
	}

	return QWidget::nativeEvent(eventType, message, result);
}

void XrayFramelessWindowWin32::mousePressEvent(QMouseEvent* _event)
{
	if (_event->button() == Qt::RightButton)
	{
		if (p_iconButton->underMouse())
		{
			auto hMenu = GetSystemMenu(reinterpret_cast<HWND>(winId()), FALSE);
			if (hMenu)
			{
				MENUITEMINFO mii;
				mii.cbSize = sizeof(MENUITEMINFO);
				mii.fMask = MIIM_STATE;
				mii.fType = 0;

				// update the options
				mii.fState = MF_ENABLED;
				SetMenuItemInfo(hMenu, SC_RESTORE, FALSE, &mii);
				SetMenuItemInfo(hMenu, SC_SIZE, FALSE, &mii);
				SetMenuItemInfo(hMenu, SC_MOVE, FALSE, &mii);
				SetMenuItemInfo(hMenu, SC_MAXIMIZE, FALSE, &mii);
				SetMenuItemInfo(hMenu, SC_MINIMIZE, FALSE, &mii);

				mii.fState = MF_GRAYED;

				WINDOWPLACEMENT wp;
				GetWindowPlacement(reinterpret_cast<HWND>(winId()), &wp);

				switch (wp.showCmd)
				{
				case SW_SHOWMAXIMIZED:
					SetMenuItemInfo(hMenu, SC_SIZE, FALSE, &mii);
					SetMenuItemInfo(hMenu, SC_MOVE, FALSE, &mii);
					SetMenuItemInfo(hMenu, SC_MAXIMIZE, FALSE, &mii);
					SetMenuDefaultItem(hMenu, SC_CLOSE, FALSE);
					break;
				case SW_SHOWMINIMIZED:
					SetMenuItemInfo(hMenu, SC_MINIMIZE, FALSE, &mii);
					SetMenuDefaultItem(hMenu, SC_RESTORE, FALSE);
					break;
				case SW_SHOWNORMAL:
					SetMenuItemInfo(hMenu, SC_RESTORE, FALSE, &mii);
					SetMenuDefaultItem(hMenu, SC_CLOSE, FALSE);
					break;
				default:;
				}

				POINT p;
				if (GetCursorPos(&p))
				{
					const LPARAM cmd = TrackPopupMenu(hMenu, (TPM_RIGHTBUTTON | TPM_NONOTIFY | TPM_RETURNCMD),
						p.x, p.y, NULL, reinterpret_cast<HWND>(winId()), Q_NULLPTR);

					if (cmd) PostMessage(reinterpret_cast<HWND>(winId()), WM_SYSCOMMAND, cmd, 0);
				}
			}
		}
	}
}

void XrayFramelessWindowWin32::changeEvent(QEvent* _event)
{
	QWidget::changeEvent(_event);

	if (_event->type() == QEvent::WindowStateChange)
	{
		const auto ev = dynamic_cast<QWindowStateChangeEvent*>(_event);

		if (!(ev->oldState() & Qt::WindowMaximized) && windowState() & Qt::WindowMaximized)
		{
			p_restoreButton->setVisible(true);
			p_maximizeButton->setVisible(false);
		}
		else
		{
			p_restoreButton->setVisible(false);
			p_maximizeButton->setVisible(true);
		}
	}

	/*if (_event->type() == QEvent::ActivationChange)
	{
	if (this->isActiveWindow())
	{

	}
	else
	{

	}
	}*/
}

void XrayFramelessWindowWin32::slot_show_system_menu() const
{
	const auto hMenu = GetSystemMenu(reinterpret_cast<HWND>(winId()), FALSE);
	if (hMenu)
	{
		MENUITEMINFO mii;
		mii.cbSize = sizeof(MENUITEMINFO);
		mii.fMask = MIIM_STATE;
		mii.fType = 0;

		// update the options
		mii.fState = MF_ENABLED;
		SetMenuItemInfo(hMenu, SC_RESTORE, FALSE, &mii);
		SetMenuItemInfo(hMenu, SC_SIZE, FALSE, &mii);
		SetMenuItemInfo(hMenu, SC_MOVE, FALSE, &mii);
		SetMenuItemInfo(hMenu, SC_MAXIMIZE, FALSE, &mii);
		SetMenuItemInfo(hMenu, SC_MINIMIZE, FALSE, &mii);

		mii.fState = MF_GRAYED;

		WINDOWPLACEMENT wp;
		GetWindowPlacement(reinterpret_cast<HWND>(winId()), &wp);

		switch (wp.showCmd)
		{
		case SW_SHOWMAXIMIZED:
			SetMenuItemInfo(hMenu, SC_SIZE, FALSE, &mii);
			SetMenuItemInfo(hMenu, SC_MOVE, FALSE, &mii);
			SetMenuItemInfo(hMenu, SC_MAXIMIZE, FALSE, &mii);
			SetMenuDefaultItem(hMenu, SC_CLOSE, FALSE);
			break;
		case SW_SHOWMINIMIZED:
			SetMenuItemInfo(hMenu, SC_MINIMIZE, FALSE, &mii);
			SetMenuDefaultItem(hMenu, SC_RESTORE, FALSE);
			break;
		case SW_SHOWNORMAL:
			SetMenuItemInfo(hMenu, SC_RESTORE, FALSE, &mii);
			SetMenuDefaultItem(hMenu, SC_CLOSE, FALSE);
			break;
		default:;
		}

		RECT winrect;
		GetWindowRect(reinterpret_cast<HWND>(winId()), &winrect);

		if (windowState() != Qt::WindowMaximized)
		{
			const LPARAM cmd = TrackPopupMenu(hMenu, (TPM_RIGHTBUTTON | TPM_NONOTIFY | TPM_RETURNCMD),
				winrect.left, winrect.top + 30, NULL, reinterpret_cast<HWND>(winId()), Q_NULLPTR);

			if (cmd) PostMessage(reinterpret_cast<HWND>(winId()), WM_SYSCOMMAND, cmd, 0);
		}
		else
		{
			const auto monitor = MonitorFromWindow(reinterpret_cast<HWND>(winId()), MONITOR_DEFAULTTONULL);
			MONITORINFO monitor_info{};
			monitor_info.cbSize = sizeof(monitor_info);
			GetMonitorInfoW(monitor, &monitor_info);
			const auto rect = monitor_info.rcWork;

			const LPARAM cmd = TrackPopupMenu(hMenu, (TPM_RIGHTBUTTON | TPM_NONOTIFY | TPM_RETURNCMD),
				rect.left, rect.top + 30, NULL, reinterpret_cast<HWND>(winId()), Q_NULLPTR);

			if (cmd) PostMessage(reinterpret_cast<HWND>(winId()), WM_SYSCOMMAND, cmd, 0);
		}

	}
}

void XrayFramelessWindowWin32::slot_minimized()
{
	setWindowState(Qt::WindowMinimized);
}

void XrayFramelessWindowWin32::slot_maximized()
{
	p_restoreButton->setVisible(true);
	p_maximizeButton->setVisible(false);
	setWindowState(Qt::WindowMaximized);
}

void XrayFramelessWindowWin32::slot_restored()
{
	p_restoreButton->setVisible(false);
	p_maximizeButton->setVisible(true);
	setWindowState(Qt::WindowNoState);
}

void XrayFramelessWindowWin32::slot_closed()
{
	close();
}

auto XrayFramelessWindowWin32::setBorderless(bool enabled, bool shadow) const -> void
{
	auto new_style = enabled ? selectBorderlessStyle() : Style::windowed;
	auto old_style = static_cast<Style>(::GetWindowLongPtrW(reinterpret_cast<HWND>(winId()), GWL_STYLE));

	if (new_style != old_style)
	{
		::SetWindowLongPtrW(reinterpret_cast<HWND>(winId()), GWL_STYLE, static_cast<LONG>(new_style));

		// when switching between borderless and windowed, restore appropriate shadow state
		setShadow(reinterpret_cast<HWND>(winId()), shadow && (new_style != Style::windowed));

		// redraw frame
		::SetWindowPos(reinterpret_cast<HWND>(winId()), nullptr, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE);
	}
}

void XrayFramelessWindowWin32::saveSettings()
{
	if (!m_clear_settings)
	{
		p_settings->setValue("Window/Geometry", saveGeometry());
		p_settings->setValue("Window/Maximized", isMaximized());
		p_settings->setValue("Window/State", saveState());
	}
}
void XrayFramelessWindowWin32::restoreSettings()
{
	if (!m_clear_settings)
	{
		// set the initial size of the window.
		const auto geometry = QApplication::desktop()->availableGeometry(this);
		resize(geometry.width() * 0.9, geometry.height() * 0.8);
		move((geometry.width() - width()) / 2, (geometry.height() - height()) / 2);

		// restore previous geometry i.e., position, size, screen.
		restoreGeometry(p_settings->value("Window/Geometry").toByteArray());

		// restore the previous maximized state.
		if (p_settings->value("Window/Maximized", false).toBool())
		{
			showMaximized();
			setGeometry(QApplication::desktop()->availableGeometry(this));
		}
		// restore states of DockWidgets and it's tabify states.
		restoreState(p_settings->value("Window/State", true).toByteArray());
	}
}
void XrayFramelessWindowWin32::clearSettings()
{
	p_settings->clear();
	m_clear_settings = true;
}

bool XrayFramelessWindowWin32::saveScreenShot(const QString& _fileName)
{
	auto capt = QPixmap::grabWindow(QApplication::desktop()->winId()).toImage();

	auto ss = capt.copy(QRect(QWidget::mapToGlobal(contentsRect().topLeft()), QWidget::mapToGlobal(contentsRect().bottomRight())));

	return ss.save(_fileName);
}

//void XrayFramelessWindowWin32::show()
//{
//	setWindowIcon(QApplication::windowIcon());
//	p_iconButton->setIcon(QApplication::windowIcon());
//
//	QSettings settings(QApplication::organizationName(), windowTitle());
//	auto isMax = settings.value("MainWindow/windowState", true).toBool();
//
//	QMainWindow::show();
//
//	if (isMax)
//	{
//		showNormal();
//		showMaximized();
//	}
//	else
//	{
//		showNormal();
//	}
//}