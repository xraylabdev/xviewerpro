/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/18
** filename: 	XrayQTreeWidget.cpp
** file base:	XrayQTreeWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a tree widget.
****************************************************************************/

#include "XrayQTreeWidget.h"

#include <QApplication>
#include <QClipboard>
#include <QHeaderView>

XRAYLAB_USING_NAMESPACE

XrayQTreeWidgetItem::XrayQTreeWidgetItem(int type) :
	QTreeWidgetItem(type)
{

}
XrayQTreeWidgetItem::XrayQTreeWidgetItem(const QStringList &strings, int type) :
	QTreeWidgetItem(strings, type)
{

}
XrayQTreeWidgetItem::XrayQTreeWidgetItem(QTreeWidget *view, int type) :
	QTreeWidgetItem(view, type)
{

}
XrayQTreeWidgetItem::XrayQTreeWidgetItem(QTreeWidget *view, const QStringList &strings, int type) :
	QTreeWidgetItem(view, strings, type)
{

}
XrayQTreeWidgetItem::XrayQTreeWidgetItem(QTreeWidget *view, QTreeWidgetItem *after, int type) :
	QTreeWidgetItem(view, after, type)
{

}
XrayQTreeWidgetItem::XrayQTreeWidgetItem(QTreeWidgetItem *parent, int type) :
	QTreeWidgetItem(parent, type)
{

}
XrayQTreeWidgetItem::XrayQTreeWidgetItem(QTreeWidgetItem *parent, const QStringList &strings, int type) :
	QTreeWidgetItem(parent, strings, type)
{

}
XrayQTreeWidgetItem::XrayQTreeWidgetItem(QTreeWidgetItem *parent, QTreeWidgetItem *after, int type) :
	QTreeWidgetItem(parent, after, type)
{

}
XrayQTreeWidgetItem::XrayQTreeWidgetItem(const QTreeWidgetItem &other) :
	QTreeWidgetItem(other)
{

}
XrayQTreeWidgetItem::~XrayQTreeWidgetItem()
{

}

void XrayQTreeWidgetItem::setData(int column, int role, const QVariant& value)
{
	if (role == Qt::CheckStateRole)
	{
		const Qt::CheckState newState = static_cast<Qt::CheckState>(value.toInt());
		const Qt::CheckState oldState = static_cast<Qt::CheckState>(data(column, role).toInt());

		QTreeWidgetItem::setData(column, role, value);

		if (newState != oldState)
		{
			auto tree = qobject_cast<XrayQTreeWidget*>(treeWidget());
			if (tree)
			{
				emit tree->itemCheckStateChanged(this);
			}
		}
	}
	else
	{
		QTreeWidgetItem::setData(column, role, value);
	}
}
void XrayQTreeWidgetItem::setFlag(Qt::ItemFlag flag, bool enabled)
{
	if (enabled)
		setFlags(flags() | flag);
	else
		setFlags(flags() & ~flag);
}
bool XrayQTreeWidgetItem::testFlag(Qt::ItemFlag flag) const
{
	return (flags() & flag);
}


XrayQTreeWidget::XrayQTreeWidget(QWidget *parent) :
	QTreeWidget(parent),
	m_menuAction(static_cast<MenuAction>(MenuAction::SelectAll | MenuAction::InvertSelection | MenuAction::RemoveSelected))
{
	setAlternatingRowColors(true);
	setAnimated(false);
	setSortingEnabled(false);
	setUniformRowHeights(true);
	setEditTriggers(QTreeWidget::EditTrigger::NoEditTriggers);

	//setRootIsDecorated(false);							// It will remove the root item as childless.
	//setSelectionMode(QAbstractItemView::MultiSelection);	// For enabling multiple items selection.

	//QStringList columns;
	//columns << "1" << "2" << "";
	//setHeaderLabels(columns);
	//header()->setStretchLastSection(true);
	//header()->setSectionResizeMode(QHeaderView::Interactive);
	////header()->setSectionResizeMode(0, QHeaderView::Fixed);
	//setIndentation(14);	// 0 will make the whole tree items expanded by double click without any down and up arrows.
	//setColumnCount(2);

	createCustomMenu();
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QTreeWidget::customContextMenuRequested, this, &XrayQTreeWidget::showContextMenu);

	//QString style = "QHeaderView::section {"
	//	//"background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,"
	//	//"stop:0 #616161, stop: 0.5 #505050,"
	//	//"stop: 0.6 #434343, stop:1 #656565);"
	//	"color: rgb(57, 158, 244);"
	//	"padding: 10px;"
	//	"padding-right: 30px;"
	//	"border-left: 1px solid rgb(128, 85, 0);"
	//	"border-top: 1px solid rgb(128, 85, 0);"
	//	"border-bottom: 1px solid rgb(128, 85, 0);"
	//	"font-size: 16px;"
	//	"font-weight: bold;"
	//	"}";
	//style += "QTreeWidget::item { padding:10px; border-left: 1px solid rgb(128, 85, 0); border-bottom: 1px solid rgb(128, 85, 0); }";
	//////style += "QTreeWidget::item:alternate { background: rgb(128, 128, 128); }";
	////style += "QTreeWidget::item:has-children { color: rgb(255, 255, 255); border-right: 1px solid rgba(128, 85, 0, 255); }";
	//style += "QTreeWidget::item:hover,QTreeWidget::item:hover:selected { background-color: rgb(128, 85, 0); }";
	//style += "QTreeWidget::item:selected, QTreeWidget::item:selected:active, QTreeWidget::item:selected:!active { background-color: rgb(47, 148, 234); }";
	//setStyleSheet(style);
}
XrayQTreeWidget::~XrayQTreeWidget()
{
}

void XrayQTreeWidget::showContextMenu(const QPoint& _point)
{
	if (!itemAt(_point))
		return;

	m_currentPoint = _point;
	p_contextMenu->exec(viewport()->mapToGlobal(_point));
}
void XrayQTreeWidget::createCustomMenu()
{
	p_contextMenu = new QMenu(this);

	if (m_menuAction & MenuAction::SelectAll)
	{
		auto action = new QAction(QIcon(":/paint/images/select_all_blue_icon.png"), QApplication::translate("XrayQTreeWidget", "Select All"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			selectAll();
		});
		p_contextMenu->addAction(action);
	}

	if (m_menuAction & MenuAction::InvertSelection)
	{
		auto action = new QAction(QIcon(":/paint/images/invert_selection_blue_icon.png"), QApplication::translate("XrayQTreeWidget", "Invert Selection"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			for (auto i = 0; i < topLevelItemCount(); i++)
			{
				if (topLevelItem(i)->isSelected()) topLevelItem(i)->setSelected(false);
				else topLevelItem(i)->setSelected(true);
			}
		});
		p_contextMenu->addAction(action);
		p_contextMenu->addSeparator();
	}

	if (m_menuAction & MenuAction::Copy)
	{
		auto action = new QAction(QIcon(":/paint/images/copy_blue_icon.png"), QApplication::translate("XrayQTreeWidget", "Copy"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			QString text;
			auto items = selectedItems();
			for (auto& i : items)
				text += i->text(0) + "\n";
			if (!text.isEmpty())
				QApplication::clipboard()->setText(text);

			emit copy();
		});
		p_contextMenu->addAction(action);
	}

	if (m_menuAction & MenuAction::Paste)
	{
		auto action = new QAction(QIcon(":/paint/images/paste_blue_icon.png"), QApplication::translate("XrayQTreeWidget", "Paste"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			emit paste();
		});
		p_contextMenu->addAction(action);
		p_contextMenu->addSeparator();
	}

	if (m_menuAction & MenuAction::RemoveSelected)
	{
		auto action = new QAction(QIcon(":/paint/images/delete_small_icon.png"), QApplication::translate("XrayQTreeWidget", "Remove Selected"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			auto items = selectedItems();

			std::vector<int> rows;
			rows.resize(items.count());
			for (auto i = 0; i < items.count(); i++)
				rows[i] = indexOfTopLevelItem(items[i]);

			for (auto& i : items)
				delete takeTopLevelItem(indexOfTopLevelItem(i));

			if (!items.isEmpty())
				emit removed(rows);
		});
		p_contextMenu->addAction(action);
	}
}

QTreeWidgetItem* XrayQTreeWidget::searchItem(int _column, const QString& _text, QTreeWidgetItemIterator::IteratorFlags _flags)
{
	if (_text.isEmpty())
		return nullptr;

	QTreeWidgetItemIterator it(this, _flags);
	while (*it)
	{
		if ((*it)->text(_column) == _text)
			return *it;
		++it;
	}

	return nullptr;
}
QTreeWidgetItem* XrayQTreeWidget::searchItem(QTreeWidgetItem* _item, int _column, const QString& _text)
{
	for (auto i = 0; i < _item->childCount(); i++)
	{
		if (_item->child(i)->text(_column) == _text)
			return _item->child(i);
	}

	return nullptr;
}

void XrayQTreeWidget::selectItem(int _column, const QString& _text, bool _setCurrentItem)
{
	auto item = searchItem(_column, _text);
	if (item)
	{
		item->setSelected(true);
		if(_setCurrentItem)
			setCurrentItem(item);
	}
}

void XrayQTreeWidget::allChecked()
{
	int i, end = topLevelItemCount();
	for (i = 0; i < end; i++)
		topLevelItem(i)->setCheckState(0, Qt::Checked);
}

void XrayQTreeWidget::allUnchecked()
{
	int i, end = topLevelItemCount();
	for (i = 0; i < end; i++)
		topLevelItem(i)->setCheckState(0, Qt::Unchecked);
}

void XrayQTreeWidget::doToggleCheckState(int _column)
{
	if (_column == 0)
	{
		auto convert = false;
		auto cs = headerItem()->data(0, Qt::CheckStateRole).toInt(&convert);
		if (convert)
		{
			if (cs == Qt::Checked)
				allUnchecked();
			else
				allChecked(); // both unchecked and partial checked go here

		}
	}
}