/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/06/08
** filename: 	XrayQListWidgetGroupBoxWithAddText.cpp
** file base:	XrayQListWidgetGroupBoxWithAddText
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a list widget.
****************************************************************************/

#include "XrayQListWidgetGroupBoxWithAddText.h"
#include "XrayGlobal.h"

#include <QApplication>
#include <QInputDialog>

XRAYLAB_USING_NAMESPACE

XrayQListWidgetGroupBoxWithAddText::XrayQListWidgetGroupBoxWithAddText(const QString &title, const QStringList &list, QWidget *parent) :
	QGroupBox(title, parent)
{
	listWidget = new XrayQListWidget(this);
	listWidget->addItems(list);
	listWidget->setDragDropMode(QAbstractItemView::InternalMove);

	addButton = new QPushButton(QApplication::translate("XrayQListWidgetGroupBoxWithAddText", "Add..."));
	addButton->setToolTip(QApplication::translate("XrayQListWidgetGroupBoxWithAddText", "Add text to the list."));
	connect(addButton, &QPushButton::clicked, this, &XrayQListWidgetGroupBoxWithAddText::addText);

	hLayout = new QHBoxLayout;
	hLayout->addStretch();
	hLayout->addWidget(addButton);

	vLayout = new QVBoxLayout;
	vLayout->addWidget(listWidget);
	vLayout->addLayout(hLayout);
	setLayout(vLayout);
}
XrayQListWidgetGroupBoxWithAddText::~XrayQListWidgetGroupBoxWithAddText()
{
}

XrayQListWidget* XrayQListWidgetGroupBoxWithAddText::getListWidget() const
{
	return listWidget;
}

void XrayQListWidgetGroupBoxWithAddText::addListTexts(const QStringList &list)
{
	// no duplicates are allowed.
	QStringList lines;
	lines.reserve(listWidget->count() + list.size());

	for (auto i = 0; i < listWidget->count(); i++)
		lines.append(listWidget->item(i)->text());

	lines.append(list);

	listWidget->clear();
	listWidget->addItems(lines);
	listWidget->setDragDropMode(QAbstractItemView::InternalMove);
	setSelectedListTexts(list);
}
void XrayQListWidgetGroupBoxWithAddText::setListTexts(const QStringList &list)
{
	listWidget->clear();
	listWidget->addItems(list);
	listWidget->setDragDropMode(QAbstractItemView::InternalMove);
}
QStringList XrayQListWidgetGroupBoxWithAddText::getListTexts() const
{
	QStringList lines;
	lines.reserve(listWidget->count());

	for (auto i = 0; i < listWidget->count(); i++)
		lines.append(listWidget->item(i)->text());

	return lines;
}

void XrayQListWidgetGroupBoxWithAddText::setSelectedListTexts(const QStringList& texts)
{
	for (auto& i : texts)
	{
		auto selected = listWidget->findItems(i, Qt::MatchExactly);
		if (!selected.empty())
		{
			for (auto& item : selected)
				listWidget->setItemSelected(item, true);
		}
	}
}
QStringList XrayQListWidgetGroupBoxWithAddText::getSelectedListTexts() const
{
	auto selected = listWidget->selectedItems();

	QStringList lines;
	lines.reserve(selected.size());

	for (auto i = 0; i < selected.size(); i++)
		lines.append(selected[i]->text());

	return lines;
}

void XrayQListWidgetGroupBoxWithAddText::addText()
{
	QString lastLine;
	if (listWidget->count() > 0)
		lastLine = listWidget->item(listWidget->count() - 1)->text();

	bool ok;
	auto text = QInputDialog::getMultiLineText(this, QApplication::translate("XrayQListWidgetGroupBoxWithAddText", "Edit Text"),
		QApplication::translate("XrayQListWidgetGroupBoxWithAddText", "Enter Text"), lastLine, &ok);
	if (ok && !text.isEmpty())
	{
		addListTexts({ text });
	}
}