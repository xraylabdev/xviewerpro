/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayButtonGroupBox.cpp
** file base:	XrayButtonGroupBox
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides list model based group box for buttons.
****************************************************************************/

#include "XrayButtonGroupBox.h"

#include <QButtonGroup>
#include <QBoxLayout>
#include <QCheckBox>
#include <QRadioButton>
#include <QStyle>

XRAYLAB_USING_NAMESPACE

XrayButtonGroupBox::XrayButtonGroupBox(QWidget *parent) :
	QGroupBox(parent),
	layout(new QBoxLayout(QBoxLayout::TopToBottom, this)),
	group(new QButtonGroup(this)),
	model(new XrayAbstractButtonListModel(this)),
	m_type(XrayButtonGroupBox::CheckBox)
{
	const int hint = style()->pixelMetric(QStyle::PM_ButtonIconSize);
	m_iconSize = QSize(hint, hint);

	connect(model, &XrayAbstractButtonListModel::rowsInserted, this, [this](const QModelIndex &parent, int first, int last)
	{
		Q_UNUSED(parent);

		for (int i = first; i <= last; ++i)
		{
			createButton_(i);
		}
	});
	connect(model, &XrayAbstractButtonListModel::dataChanged, this, [this](const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
	{
		for (int i = topLeft.row(); i <= bottomRight.row(); ++i)
		{
			updateButton_(i, roles);
		}
	});
	connect(model, &XrayAbstractButtonListModel::rowsRemoved, this, [this](const QModelIndex &parent, int first, int last)
	{
		Q_UNUSED(parent);

		for (auto i = last; i >= first; --i)
			removeButton_(i);
	});

	connect(group, QOverload<QAbstractButton *>::of(&QButtonGroup::buttonClicked), this, [this](QAbstractButton *button) 
	{
		emit buttonClicked(layout->indexOf(button), group->id(button));
	});
	connect(group, QOverload<QAbstractButton *>::of(&QButtonGroup::buttonPressed), this, [this](QAbstractButton *button) 
	{
		emit buttonPressed(layout->indexOf(button), group->id(button));
	});
	connect(group, QOverload<QAbstractButton *>::of(&QButtonGroup::buttonReleased), this, [this](QAbstractButton *button)
	{
		emit buttonReleased(layout->indexOf(button), group->id(button));
	});
	connect(group, QOverload<QAbstractButton *, bool>::of(&QButtonGroup::buttonToggled), this, [this](QAbstractButton *button, bool checked)
	{
		const auto index = layout->indexOf(button);
		model->setData(this->model->index(index), checked ? Qt::Checked : Qt::Unchecked, Qt::CheckStateRole);
		emit buttonToggled(index, group->id(button), checked);
	});
}

Qt::Orientation XrayButtonGroupBox::orientation() const
{
	return layout->direction() == QBoxLayout::LeftToRight ? Qt::Horizontal : Qt::Vertical;
}

void XrayButtonGroupBox::setOrientation(Qt::Orientation orientation)
{
	layout->setDirection(orientation == Qt::Horizontal ? QBoxLayout::LeftToRight : QBoxLayout::TopToBottom);
}

XrayButtonGroupBox::Type XrayButtonGroupBox::type() const
{
	return m_type;
}

void XrayButtonGroupBox::setType(XrayButtonGroupBox::Type _type)
{
	if (m_type == _type)
	{
		return;
	}

	m_type = _type;
	resetButtons_();
}

QSize XrayButtonGroupBox::iconSize() const
{
	return m_iconSize;
}

void XrayButtonGroupBox::setIconSize(const QSize& _iconSize)
{
	if (m_iconSize == _iconSize)
	{
		return;
	}

	m_iconSize = _iconSize;

	if (!layout->isEmpty())
	{
		const auto buttons = group->buttons();
		for (auto& button : buttons)
		{
			button->setIconSize(_iconSize);
		}
	}
}

bool XrayButtonGroupBox::exclusive() const
{
	return group->exclusive();
}

void XrayButtonGroupBox::setExclusive(bool exclusive)
{
	group->setExclusive(exclusive);
}

void XrayButtonGroupBox::insertButton(int index, const XrayButtonGroupBox::Button &button)
{
	if (model->insertRow(index))
	{
		model->setItemData(model->index(index), button);
	}
}

void XrayButtonGroupBox::addButton(const XrayButtonGroupBox::Button &button)
{
	insertButton(count(), button);
}

void XrayButtonGroupBox::removeButton(int index)
{
	model->removeRow(index);
}

QVariant XrayButtonGroupBox::buttonData(int index, int role) const
{
	return model->data(model->index(index), role);
}

void XrayButtonGroupBox::setButtonData(int index, int role, const QVariant &data)
{
	model->setData(model->index(index), data, role);
}

void XrayButtonGroupBox::clear()
{
	model->clear();
}

int XrayButtonGroupBox::count() const
{
	return layout->count();
}

bool XrayButtonGroupBox::isEmpty() const
{
	return layout->isEmpty();
}

int XrayButtonGroupBox::checkedIndex() const
{
	return layout->indexOf(group->checkedButton());
}

int XrayButtonGroupBox::checkedId() const
{
	return group->checkedId();
}

QStringList XrayButtonGroupBox::items() const
{
	QStringList items;

	for (auto i = 0; i < count(); ++i)
	{
		items << model->index(i).data(Qt::DisplayRole).toString();
	}

	return items;
}

void XrayButtonGroupBox::setItems(const QStringList &items)
{
	clear();

	for (const auto& item : items)
	{
		XrayButtonGroupBox::Button button;
		button[Qt::DisplayRole] = item;
		addButton(button);
	}
}

QAbstractButton *XrayButtonGroupBox::createButton() const
{
	switch (m_type) 
	{
	case XrayButtonGroupBox::CheckBox:
		return new QCheckBox;
	case XrayButtonGroupBox::RadioBox:
		return new QRadioButton;
	default:
		Q_UNREACHABLE();
	}

	return nullptr;
}

void XrayButtonGroupBox::removeButton_(int index)
{
	auto button = qobject_cast<QAbstractButton *>(layout->itemAt(index)->widget());
	Q_ASSERT(button);

	layout->removeWidget(button);
	group->removeButton(button);
	button->deleteLater();
}

void XrayButtonGroupBox::updateButton_(int index, const QVector<int> &roles /*= {}*/)
{
	Q_UNUSED(roles);

	const auto data = model->itemData(model->index(index));
	auto button = qobject_cast<QAbstractButton *>(layout->itemAt(index)->widget());
	Q_ASSERT(button);

	group->setId(button, data.value(XrayAbstractButtonListModel::IdRole).toInt());

	button->setChecked(data.value(Qt::CheckStateRole, Qt::Unchecked).value<Qt::CheckState>() == Qt::Checked);
	button->setIcon(data.value(Qt::DecorationRole).value<QIcon>());
	button->setText(data.value(Qt::DisplayRole).toString());
	button->setToolTip(data.value(Qt::ToolTipRole).toString());
	button->setWhatsThis(data.value(Qt::WhatsThisRole).toString());
	button->setStatusTip(data.value(Qt::StatusTipRole).toString());
	button->setAccessibleName(data.value(Qt::AccessibleTextRole).toString());
	button->setAccessibleDescription(data.value(Qt::AccessibleDescriptionRole).toString());
	button->setShortcut(data.value(XrayAbstractButtonListModel::ShortcutRole).value<QKeySequence>());

	const auto font = data.value(Qt::FontRole);
	if (font.isValid())
	{
		button->setFont(font.value<QFont>());
	}

	// Exclusive group would not undeck the checked button
	if (button->isChecked() != (data.value(Qt::CheckStateRole, Qt::Unchecked).value<Qt::CheckState>() == Qt::Checked))
	{
		model->setData(model->index(index), button->isChecked() ? Qt::Checked : Qt::Unchecked, Qt::CheckStateRole);
	}
}

void XrayButtonGroupBox::createButton_(int index)
{
	auto button = createButton();
	Q_ASSERT(button);

	button->setCheckable(true);
	button->setIconSize(m_iconSize);

	group->addButton(button, model->data(model->index(index), XrayAbstractButtonListModel::IdRole).toInt());
	layout->insertWidget(index, button);

	updateButton_(index);
}

void XrayButtonGroupBox::resetButtons_()
{
	for (int i = 0; i < model->rowCount(); ++i)
	{
		removeButton_(i);
		createButton_(i);
	}
}