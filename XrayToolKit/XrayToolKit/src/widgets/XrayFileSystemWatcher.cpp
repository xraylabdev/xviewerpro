/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayFileSystemWatcher.cpp
** file base:	XrayFileSystemWatcher
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a file system watcher.
****************************************************************************/

#include "XrayFileSystemWatcher.h"

#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayFileSystemWatcher::XrayFileSystemWatcher(int _timerInterval) : 
	m_callback(nullptr)
{
	connect(&m_watcher, &QFileSystemWatcher::directoryChanged, this, &XrayFileSystemWatcher::directoryChanged);
	connect(&m_watcher, &QFileSystemWatcher::fileChanged, this, &XrayFileSystemWatcher::fileChanged);

	// configure the timer to signal the changes to the callback.
	m_timer.setInterval(_timerInterval);
	m_timer.setSingleShot(true);

	m_dir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot | QDir::Modified);
	m_dir.setNameFilters({"*"});
}
XrayFileSystemWatcher::~XrayFileSystemWatcher()
{
}
void XrayFileSystemWatcher::clear()
{
	if (!m_watcher.directories().isEmpty())
		m_watcher.removePaths(m_watcher.directories());
}
void XrayFileSystemWatcher::setCallback(std::function<void()> _callback)
{
	if (_callback)
	{
		m_callback = _callback;
		disconnect(&m_timer, &QTimer::timeout, nullptr, nullptr);
		connect(&m_timer, &QTimer::timeout, m_callback);
	}
}
QDir& XrayFileSystemWatcher::getDir()
{ 
	return m_dir; 
}
QStringList XrayFileSystemWatcher::getFiles() const
{
	return m_watcher.files();
}
QStringList XrayFileSystemWatcher::getDirectories() const
{
	return m_watcher.directories();
}
QList<QDateTime> XrayFileSystemWatcher::getModifiedDateTime() const
{
	QList<QDateTime> list;
	for (const auto& info : m_watcher.directories())
		list << QFileInfo(info).lastModified()/*.toString(Qt::SystemLocaleDate)*/;
	return list;
}
void XrayFileSystemWatcher::addPaths()
{
	const auto path = m_dir.absolutePath();
	if (!QFile::exists(path))
		return;

	const auto paths = m_dir.entryList().replaceInStrings(QRegExp("^"), path);

	clear();
	m_watcher.addPaths(paths);
	m_timer.start();
}
void XrayFileSystemWatcher::setDirectory(const QString& path)
{
	if (!m_watcher.addPath(path))
	{
		qWarning() << "Couldn't add the specified path: " << path;
	}
	else
	{
		m_dir.setPath(path);
		addPaths();
	}
}
void XrayFileSystemWatcher::remove(const QString& _path)
{
	for(const auto& dir : m_watcher.directories())
	{
		if (dir.startsWith(_path, Qt::CaseInsensitive))
		{
			m_watcher.removePath(dir);
		}
	}
}
void XrayFileSystemWatcher::directoryChanged(const QString& path)
{
	Q_UNUSED(path);
	addPaths();
}
void XrayFileSystemWatcher::fileChanged(const QString& path)
{
	Q_UNUSED(path);
	m_timer.start();
}