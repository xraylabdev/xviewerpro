/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/05/23
** filename: 	XrayTextEditor.h
** file base:	XrayTextEditor
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a basic text editor.
****************************************************************************/

#include "XrayTextEditor.h"
#include "XrayGlobal.h"

#include <QtWidgets>

XRAYLAB_USING_NAMESPACE

XrayTextEditor::XrayTextEditor() : 
	textEdit(new QPlainTextEdit)
{
	setCentralWidget(textEdit);

	createActions();
	createStatusBar();

	readSettings();

	connect(textEdit->document(), &QTextDocument::contentsChanged, this, &XrayTextEditor::documentWasModified);

#ifndef QT_NO_SESSIONMANAGER
	QGuiApplication::setFallbackSessionManagementEnabled(false);
	connect(qApp, &QGuiApplication::commitDataRequest, this, &XrayTextEditor::commitData);
#endif

	setCurrentFile(QString());
	setUnifiedTitleAndToolBarOnMac(true);
}

void XrayTextEditor::closeEvent(QCloseEvent *event)
{
	if (maybeSave()) 
	{
		writeSettings();
		event->accept();
	} 
	else 
	{
		event->ignore();
	}
}

void XrayTextEditor::newFile()
{
	if (maybeSave()) 
	{
		textEdit->clear();
		setCurrentFile(QString());
	}
}

void XrayTextEditor::open()
{
	if (maybeSave()) 
	{
		auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayTextEditor", "Select File"), XrayGlobal::getLastFileOpenPath(), QString(), nullptr);
		if (!fileName.isEmpty())
			loadFile(fileName);
	}
}

bool XrayTextEditor::save()
{
	if (curFile.isEmpty()) 
	{
		return saveAs();
	} 
	else 
	{
		return saveFile(curFile);
	}
}

bool XrayTextEditor::saveAs()
{
	auto fileName = QFileDialog::getSaveFileName(this, QApplication::translate("XrayTextEditor", "Save File As..."), XrayGlobal::getLastFileOpenPath(), QString(), nullptr);
	if (!fileName.isEmpty())
	{
		return saveFile(fileName);
	}
	return false;
}

void XrayTextEditor::about()
{
}

void XrayTextEditor::documentWasModified()
{
	setWindowModified(textEdit->document()->isModified());
}

void XrayTextEditor::createActions()
{
	auto fileMenu = menuBar()->addMenu(QApplication::translate("XrayTextEditor", "&File"));
	auto fileToolBar = addToolBar(QApplication::translate("XrayTextEditor", "File"));
	const auto newIcon = QIcon::fromTheme("document-new", QIcon(":/images/new.png"));
	auto newAct = new QAction(newIcon, QApplication::translate("XrayTextEditor", "&New"), this);
	newAct->setShortcuts(QKeySequence::New);
	newAct->setStatusTip(QApplication::translate("XrayTextEditor", "Create a new file"));
	connect(newAct, &QAction::triggered, this, &XrayTextEditor::newFile);
	fileMenu->addAction(newAct);
	fileToolBar->addAction(newAct);

	const auto openIcon = QIcon::fromTheme("document-open", QIcon(":/images/open.png"));
	auto openAct = new QAction(openIcon, QApplication::translate("XrayTextEditor", "&Open..."), this);
	openAct->setShortcuts(QKeySequence::Open);
	openAct->setStatusTip(QApplication::translate("XrayTextEditor", "Open an existing file"));
	connect(openAct, &QAction::triggered, this, &XrayTextEditor::open);
	fileMenu->addAction(openAct);
	fileToolBar->addAction(openAct);

	const auto saveIcon = QIcon::fromTheme("document-save", QIcon(":/images/save.png"));
	auto saveAct = new QAction(saveIcon, QApplication::translate("XrayTextEditor", "&Save"), this);
	saveAct->setShortcuts(QKeySequence::Save);
	saveAct->setStatusTip(QApplication::translate("XrayTextEditor", "Save the document to disk"));
	connect(saveAct, &QAction::triggered, this, &XrayTextEditor::save);
	fileMenu->addAction(saveAct);
	fileToolBar->addAction(saveAct);

	const auto saveAsIcon = QIcon::fromTheme("document-save-as");
	auto saveAsAct = fileMenu->addAction(saveAsIcon, QApplication::translate("XrayTextEditor", "Save &As..."), this, &XrayTextEditor::saveAs);
	saveAsAct->setShortcuts(QKeySequence::SaveAs);
	saveAsAct->setStatusTip(QApplication::translate("XrayTextEditor", "Save the document under a new name"));

	fileMenu->addSeparator();

	const auto exitIcon = QIcon::fromTheme("application-exit");
	auto exitAct = fileMenu->addAction(exitIcon, QApplication::translate("XrayTextEditor", "E&xit"), this, &QWidget::close);
	exitAct->setShortcuts(QKeySequence::Quit);
	exitAct->setStatusTip(QApplication::translate("XrayTextEditor", "Exit the application"));

	auto editMenu = menuBar()->addMenu(QApplication::translate("XrayTextEditor", "&Edit"));
	auto editToolBar = addToolBar(QApplication::translate("XrayTextEditor", "Edit"));
#ifndef QT_NO_CLIPBOARD
	const auto cutIcon = QIcon::fromTheme("edit-cut", QIcon(":/images/cut.png"));
	auto cutAct = new QAction(cutIcon, QApplication::translate("XrayTextEditor", "Cu&t"), this);

	cutAct->setShortcuts(QKeySequence::Cut);
	cutAct->setStatusTip(QApplication::translate("XrayTextEditor", "Cut the current selection's contents to the "
							"clipboard"));
	connect(cutAct, &QAction::triggered, textEdit, &QPlainTextEdit::cut);
	editMenu->addAction(cutAct);
	editToolBar->addAction(cutAct);

	const auto copyIcon = QIcon::fromTheme("edit-copy", QIcon(":/images/copy.png"));
	auto copyAct = new QAction(copyIcon, QApplication::translate("XrayTextEditor", "&Copy"), this);
	copyAct->setShortcuts(QKeySequence::Copy);
	copyAct->setStatusTip(QApplication::translate("XrayTextEditor", "Copy the current selection's contents to the "
							 "clipboard"));
	connect(copyAct, &QAction::triggered, textEdit, &QPlainTextEdit::copy);
	editMenu->addAction(copyAct);
	editToolBar->addAction(copyAct);

	const auto pasteIcon = QIcon::fromTheme("edit-paste", QIcon(":/images/paste.png"));
	auto pasteAct = new QAction(pasteIcon, QApplication::translate("XrayTextEditor", "&Paste"), this);
	pasteAct->setShortcuts(QKeySequence::Paste);
	pasteAct->setStatusTip(QApplication::translate("XrayTextEditor", "Paste the clipboard's contents into the current "
							  "selection"));
	connect(pasteAct, &QAction::triggered, textEdit, &QPlainTextEdit::paste);
	editMenu->addAction(pasteAct);
	editToolBar->addAction(pasteAct);

	menuBar()->addSeparator();
#endif // !QT_NO_CLIPBOARD

	//auto helpMenu = menuBar()->addMenu(QApplication::translate("XrayTextEditor", "&Help"));
	//auto aboutAct = helpMenu->addAction(QApplication::translate("XrayTextEditor", "&About"), this, &XrayTextEditor::about);

#ifndef QT_NO_CLIPBOARD
	cutAct->setEnabled(false);
	copyAct->setEnabled(false);
	connect(textEdit, &QPlainTextEdit::copyAvailable, cutAct, &QAction::setEnabled);
	connect(textEdit, &QPlainTextEdit::copyAvailable, copyAct, &QAction::setEnabled);
#endif // !QT_NO_CLIPBOARD
}

void XrayTextEditor::createStatusBar()
{
	statusBar()->showMessage(QApplication::translate("XrayTextEditor", "Ready"));
}

void XrayTextEditor::readSettings()
{
	QSettings settings(QApplication::organizationName(), QApplication::applicationName());
	const auto geometry = settings.value("TextEditor/Geometry", QByteArray()).toByteArray();
	if (geometry.isEmpty()) 
	{
		const auto availableGeometry = QApplication::desktop()->availableGeometry(this);
		resize(availableGeometry.width() / 3, availableGeometry.height() / 2);
		move((availableGeometry.width() - width()) / 2, (availableGeometry.height() - height()) / 2);
	} 
	else 
	{
		restoreGeometry(geometry);
	}
}

void XrayTextEditor::writeSettings()
{
	QSettings settings(QApplication::organizationName(), QApplication::applicationName());
	settings.setValue("TextEditor/Geometry", saveGeometry());
}

bool XrayTextEditor::maybeSave()
{
	if (!textEdit->document()->isModified())
		return true;

	const auto ret = QMessageBox::warning(this, windowTitle(), QApplication::translate("XrayTextEditor", "The document has been modified.\nDo you want to save your changes?"), QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
	switch (ret)
	{
	case QMessageBox::Save:
		return save();
	case QMessageBox::Cancel:
		return false;
	default:
		break;
	}
	return true;
}

void XrayTextEditor::loadFile(const QString &fileName)
{
	QFile file(fileName);
	if (!file.open(QFile::ReadOnly | QFile::Text)) 
	{
		QMessageBox::warning(this, windowTitle(), QApplication::translate("XrayTextEditor", "Cannot read file %1:\n%2.").arg(QDir::toNativeSeparators(fileName), file.errorString()));
		return;
	}

	QTextStream in(&file);
#ifndef QT_NO_CURSOR
	QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
	textEdit->setPlainText(in.readAll());
#ifndef QT_NO_CURSOR
	QApplication::restoreOverrideCursor();
#endif

	setCurrentFile(fileName);
	statusBar()->showMessage(QApplication::translate("XrayTextEditor", "File loaded"), 2000);
}

bool XrayTextEditor::saveFile(const QString &fileName)
{
	QFile file(fileName);
	if (!file.open(QFile::WriteOnly | QFile::Text)) 
	{
		QMessageBox::warning(this, QApplication::translate("XrayTextEditor", "Application"), QApplication::translate("XrayTextEditor", "Cannot write file %1:\n%2.").arg(QDir::toNativeSeparators(fileName), file.errorString()));
		return false;
	}

	QTextStream out(&file);
#ifndef QT_NO_CURSOR
	QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
	out << textEdit->toPlainText();
#ifndef QT_NO_CURSOR
	QApplication::restoreOverrideCursor();
#endif

	setCurrentFile(fileName);
	statusBar()->showMessage(QApplication::translate("XrayTextEditor", "File saved"), 2000);
	return true;
}

void XrayTextEditor::setCurrentFile(const QString &fileName)
{
	curFile = fileName;
	textEdit->document()->setModified(false);
	setWindowModified(false);

	auto shownName = curFile;
	if (curFile.isEmpty())
		shownName = "untitled.txt";
	setWindowFilePath(shownName);
}

QString XrayTextEditor::strippedName(const QString &fullFileName)
{
	return QFileInfo(fullFileName).fileName();
}

#ifndef QT_NO_SESSIONMANAGER
void XrayTextEditor::commitData(QSessionManager &manager)
{
	if (manager.allowsInteraction())
	{
		if (!maybeSave())
			manager.cancel();
	} 
	else
	{
		// save without asking
		if (textEdit->document()->isModified())
			save();
	}
}
#endif
