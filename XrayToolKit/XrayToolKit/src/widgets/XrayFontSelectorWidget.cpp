/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/05
** filename: 	XrayFontSelectorWidget.cpp
** file base:	XrayFontSelectorWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a widget that contains font family, size
				bold, italic, underline, and color widgets.
****************************************************************************/

#include "XrayFontSelectorWidget.h"

#include <QApplication>
#include <QPainter>
#include <QPaintEvent>
#include <QColorDialog>

XRAYLAB_USING_NAMESPACE

XrayFontSelectorWidget::XrayFontSelectorWidget(QWidget* pParent) :
	QWidget(pParent)
{
	// shape settings toolbar
	fontCombo = new QFontComboBox();
	fontCombo->setCurrentFont(QFont("Arial", 10, -1, false));
	fontCombo->setToolTip(QApplication::translate("XrayFontSelectorWidget", "Change the font family."));
	connect(fontCombo, &QFontComboBox::currentFontChanged, this, &XrayFontSelectorWidget::currentFontChanged);

	fontSizeCombo = new QComboBox;
	fontSizeCombo->setFixedWidth(70);
	fontSizeCombo->setEditable(true);
	for (int i = 8; i < 64; i = i + 2)
		fontSizeCombo->addItem(QString().setNum(i));
	auto validator = new QIntValidator(2, 64, this);
	fontSizeCombo->setValidator(validator);
	fontSizeCombo->setCurrentText("10");
	fontSizeCombo->setToolTip(QApplication::translate("XrayFontSelectorWidget", "Change the font size."));
	connect(fontSizeCombo, static_cast<void (QComboBox::*)(const QString&)>(&QComboBox::currentIndexChanged), this, &XrayFontSelectorWidget::fontSizeChanged);

	boldAction = new QPushButton(QIcon(":/paint/images/bold_icon.png"), QApplication::translate("XrayFontSelectorWidget", "Bold"), this);
	boldAction->setFixedWidth(30);
	boldAction->setCheckable(true);
	boldAction->setToolTip(QApplication::translate("XrayFontSelectorWidget", "Change to the heavier font."));
	connect(boldAction, &QPushButton::clicked, this, &XrayFontSelectorWidget::handleFontChange);
	boldAction->setText("");

	italicAction = new QPushButton(QIcon(":/paint/images/italic_icon.png"), QApplication::translate("XrayFontSelectorWidget", "Italic"), this);
	italicAction->setFixedWidth(30);
	italicAction->setCheckable(true);
	italicAction->setToolTip(QApplication::translate("XrayFontSelectorWidget", "Change to an italic font."));
	connect(italicAction, &QPushButton::clicked, this, &XrayFontSelectorWidget::handleFontChange);
	italicAction->setText("");

	underlineAction = new QPushButton(QIcon(":/paint/images/underline_icon.png"), QApplication::translate("XrayFontSelectorWidget", "Underline"), this);
	underlineAction->setFixedWidth(30);
	underlineAction->setCheckable(true);
	underlineAction->setToolTip(QApplication::translate("XrayFontSelectorWidget", "Draw a line below the text."));
	connect(underlineAction, &QPushButton::clicked, this, &XrayFontSelectorWidget::handleFontChange);
	underlineAction->setText("");

	p_colorDialog = new XrayColorFrame(this);
	p_colorDialog->setToolTip(QApplication::translate("XrayFontSelectorWidget", "Select a color from the color palette."));

	p_layout = new QHBoxLayout;
	p_layout->addWidget(fontCombo);
	p_layout->addWidget(fontSizeCombo);
	p_layout->addWidget(boldAction);
	p_layout->addWidget(italicAction);
	p_layout->addWidget(underlineAction);
	p_layout->addWidget(p_colorDialog);
	setLayout(p_layout);
}

void XrayFontSelectorWidget::setFont(const QFont& _font)
{
	fontCombo->setCurrentFont(_font);
	fontSizeCombo->setCurrentText(QString::number(_font.pointSize()));
	boldAction->setChecked(_font.bold());
	italicAction->setChecked(_font.italic());
	underlineAction->setChecked(_font.underline());
}
QFont XrayFontSelectorWidget::getFont() const
{
	auto font = fontCombo->currentFont();
	font.setPointSize(fontSizeCombo->currentText().toInt());
	font.setWeight(boldAction->isChecked() ? QFont::Bold : QFont::Normal);
	font.setItalic(italicAction->isChecked());
	font.setUnderline(underlineAction->isChecked());
	return font;
}

void XrayFontSelectorWidget::setColor(const QColor& _color)
{
	p_colorDialog->setColor(_color);
}
QColor XrayFontSelectorWidget::getColor() const
{
	return p_colorDialog->getColor();
}

void XrayFontSelectorWidget::fontSizeChanged(const QString&)
{
	handleFontChange();
}
void XrayFontSelectorWidget::currentFontChanged(const QFont&)
{
	handleFontChange();
}
void XrayFontSelectorWidget::handleFontChange()
{
	auto font = fontCombo->currentFont();
	font.setPointSize(fontSizeCombo->currentText().toInt());
	font.setWeight(boldAction->isChecked() ? QFont::Bold : QFont::Normal);
	font.setItalic(italicAction->isChecked());
	font.setUnderline(underlineAction->isChecked());

	emit fontChanged(font);
}