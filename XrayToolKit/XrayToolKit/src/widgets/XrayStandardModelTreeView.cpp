/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/17/07
** filename: 	XrayStandardModelTreeView.cpp
** file base:	XrayStandardModelTreeView
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a table view.
****************************************************************************/

#include "XrayStandardModelTreeView.h"

#include <QApplication>
#include <QHeaderView>
#include <QClipboard>
#include <QInputDialog>
#include <QMessageBox>
#include <QDesktopServices>
#include <QFileInfo>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayStandardModelTreeView::XrayStandardModelTreeView(int menuAction, QWidget* parent) :
	QTreeView(parent),
	m_menuAction(static_cast<MenuAction>(menuAction)),
	p_sourceModel(new TreeViewSortSourceModel{ this }),
	p_contextMenu(nullptr),
	m_changeOnlyParentText(true),
	p_groupMenu(nullptr),
	m_parentFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDropEnabled),
	m_childFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled),
	m_showWarningMsgBoxOnRemove(false)
{
	setModel(p_sourceModel);

	// m_view->setFrameShape(QFrame::NoFrame);
	sortByColumn(0);
	setSortingEnabled(false);
	setDragDropMode(QAbstractItemView::InternalMove);
	setDropIndicatorShown(true);
	//setIndentation(20);
	setRootIsDecorated(true);	// true will create a down arrow if child exists
	setWordWrap(true);
	setAllColumnsShowFocus(true);
	setEditTriggers(QTreeView::NoEditTriggers);
	setAlternatingRowColors(true);
	setSelectionBehavior(QTreeView::SelectRows);
	setDragDropOverwriteMode(false);

	setSelectionMode(QAbstractItemView::ExtendedSelection);

	header()->resizeSections(QHeaderView::ResizeToContents);
	header()->setSortIndicator(0, Qt::AscendingOrder);
	header()->setHidden(true);

	createCustomMenu();
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QTreeView::customContextMenuRequested, this, &XrayStandardModelTreeView::showContextMenu);

	// sync checkable with the item selection model
	connect(selectionModel(), &QItemSelectionModel::selectionChanged, [this](const QItemSelection &selected, const QItemSelection &deselected)
	{
		for (const auto& index : selected.indexes())
		{
			auto item = p_sourceModel->itemFromIndex(index);
			if (item->isCheckable())
				item->setCheckState(Qt::Checked);
		}
		for (const auto& index : deselected.indexes())
		{
			auto item = p_sourceModel->itemFromIndex(index);
			if (item->isCheckable())
				item->setCheckState(Qt::Unchecked);
		}
	});
}

void XrayStandardModelTreeView::setSourceModel(TreeViewSortSourceModel* model)
{ 
	p_sourceModel = model;
	setModel(p_sourceModel);
}
TreeViewSortSourceModel* XrayStandardModelTreeView::sourceModel() const
{ 
	return p_sourceModel; 
}
void XrayStandardModelTreeView::setShowWarningMessageBoxOnRemove(bool b)
{
	m_showWarningMsgBoxOnRemove = b;
}
bool XrayStandardModelTreeView::showWarningMessageBoxOnRemove() const
{
	return m_showWarningMsgBoxOnRemove;
}

void XrayStandardModelTreeView::showContextMenu(const QPoint& point)
{
	if (m_menuAction == MenuAction::None)
		return;

	if (!indexAt(point).isValid())
		return;

	m_currentPoint = point;
	if (p_contextMenu)
		p_contextMenu->exec(viewport()->mapToGlobal(point));
}

void XrayStandardModelTreeView::checkableClicked(const QModelIndex& index)
{
	if (!index.isValid())
		return;

	auto item = p_sourceModel->itemFromIndex(index);
	if (!item)
		return;

	if (!item->rowCount())
		return;

	// check the current item
	//if (item->isCheckable())
	//	item->setCheckState(item->checkState() == Qt::Checked ? Qt::Unchecked : Qt::Checked);

	// check the group items
	for (int i = 0; i < item->rowCount(); ++i)
	{
		auto ch = item->child(i);
		auto checked = ch->checkState() == Qt::Checked;

		if (ch->isCheckable())
			ch->setCheckState(checked ? Qt::Unchecked : Qt::Checked);
	}
}

void XrayStandardModelTreeView::createCustomMenu()
{
	p_contextMenu = new QMenu(this);

	if (m_menuAction & MenuAction::SelectAll)
	{
		auto action = new QAction(QIcon(":/paint/images/select_all_blue_icon.png"), QApplication::translate("XrayStandardModelTreeView", "Select All"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			selectAll();

			// following code only select the parent items
			//for (auto i = 0; i < p_sourceModel->rowCount(); i++)
			//{
			//	auto item = p_sourceModel->item(i);
			//	auto index = p_sourceModel->indexFromItem(item);
			//	selectionModel()->select(index, QItemSelectionModel::Select | QItemSelectionModel::Rows);
			//}
		});
		p_contextMenu->addAction(action);
	}

	if (m_menuAction & MenuAction::InvertSelection)
	{
		auto action = new QAction(QIcon(":/paint/images/.png"), QApplication::translate("XrayStandardModelTreeView", "Select All Files"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			selectAll();

			// following code only togglles the parent items
			for (auto i = 0; i < p_sourceModel->rowCount(); i++)
			{
				auto item = p_sourceModel->item(i);
				auto index = p_sourceModel->indexFromItem(item);
				selectionModel()->select(index, QItemSelectionModel::Toggle | QItemSelectionModel::Rows);
			}
		});
		p_contextMenu->addAction(action);
		p_contextMenu->addSeparator();
	}

	if (m_menuAction & MenuAction::Sort)
	{
		auto sortMenu = p_contextMenu->addMenu(QApplication::translate("XrayStandardModelTreeView", "Sort Groups"));
		auto action = new QAction(QIcon(":/paint/images/.png"), QApplication::translate("XrayStandardModelTreeView", "Ascending Order"), this);
		connect(action, &QAction::triggered, [this]() { p_sourceModel->sort(0, Qt::AscendingOrder); emit itemsSorted(0, Qt::AscendingOrder); });
		sortMenu->addAction(action);
		action = new QAction(QIcon(":/paint/images/.png"), QApplication::translate("XrayStandardModelTreeView", "Descending Order"), this);
		connect(action, &QAction::triggered, [this]() { p_sourceModel->sort(0, Qt::DescendingOrder); emit itemsSorted(0, Qt::DescendingOrder); });
		sortMenu->addAction(action);

		sortMenu = p_contextMenu->addMenu(QApplication::translate("XrayStandardModelTreeView", "Sort Files"));
		action = new QAction(QIcon(":/paint/images/.png"), QApplication::translate("XrayStandardModelTreeView", "Ascending Order"), this);
		connect(action, &QAction::triggered, [this]() { sortChildren(0, Qt::AscendingOrder); emit itemsSorted(0, Qt::AscendingOrder); });
		sortMenu->addAction(action);
		action = new QAction(QIcon(":/paint/images/.png"), QApplication::translate("XrayStandardModelTreeView", "Descending Order"), this);
		connect(action, &QAction::triggered, [this]() { sortChildren(0, Qt::DescendingOrder); emit itemsSorted(0, Qt::DescendingOrder); });
		sortMenu->addAction(action);
	}

	if (m_menuAction & MenuAction::MoveTo)
	{
		p_groupMenu = p_contextMenu->addMenu(QApplication::translate("XrayStandardModelTreeView", "Move To"));
		p_contextMenu->addSeparator();
	}

	if (m_menuAction & MenuAction::AddParent)
	{
		auto action = new QAction(QIcon(":/paint/images/plus_icon.png"), QApplication::translate("XrayStandardModelTreeView", "Add New Group"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			bool ok;
			auto text = QInputDialog::getText(this, QApplication::translate("XrayStandardModelTreeView", "Add New Group"),
				QApplication::translate("XrayStandardModelTreeView", "Enter Text"), QLineEdit::Normal, QApplication::translate("XrayStandardModelTreeView", "Group 1"), &ok);
			if (ok && !text.isEmpty())
			{
				addParent(text, true, false);
			}
		});
		p_contextMenu->addAction(action);
	}

	if (m_menuAction & MenuAction::ChangeText)
	{
		QString actionText;
		if (m_changeOnlyParentText)
			actionText = QApplication::translate("XrayStandardModelTreeView", "Rename Group");
		else
			actionText = QApplication::translate("XrayStandardModelTreeView", "Rename");

		auto action = new QAction(QIcon(":/paint/images/font_icon.png"), actionText, this);
		connect(action, &QAction::triggered, this, [this]()
		{
			bool ok;
			auto text = QInputDialog::getText(this, QApplication::translate("XrayStandardModelTreeView", "Change Text"),
				QApplication::translate("XrayStandardModelTreeView", "Enter Text"), QLineEdit::Normal, QApplication::translate("XrayStandardModelTreeView", "Text"), &ok);
			if (ok && !text.isEmpty())
			{
				auto item = p_sourceModel->itemFromIndex(currentIndex());

				if (m_changeOnlyParentText)
				{
					if (isParent(currentIndex()))
					{
						item->setText(text);
						emit itemTextChanged(item);
					}
					else if (isChild(currentIndex()))
					{
						item->parent()->setText(text);
						emit itemTextChanged(item->parent());
					}

					// update MoveTo menu of the context menu
					updateMoveToMenu();
				}
				else
				{
					item->setText(text);

					emit itemTextChanged(item);
				}
			}
		});
		p_contextMenu->addAction(action);
		p_contextMenu->addSeparator();
	}

	if (m_menuAction & MenuAction::Copy)
	{
		auto action = new QAction(QIcon(":/paint/images/copy_blue_icon.png"), QApplication::translate("XrayStandardModelTreeView", "Copy Path"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			QString text;
			auto items = selectionModel()->selectedIndexes();
			for (auto& i : items)
				text += i.data(Qt::UserRole).toString() + '\n';
			if (!text.isEmpty())
				QApplication::clipboard()->setText(text);

			emit copy(text);
		});
		p_contextMenu->addAction(action);
	}

	if (m_menuAction & MenuAction::OpenInExplorer)
	{
		auto action = new QAction(QIcon(":/paint/images/.png"), QApplication::translate("XrayStandardModelTreeView", "Reveal In File Explorer"), this);
		connect(action, &QAction::triggered, [this]()
		{
			auto index = currentIndex();
			if (index.isValid())
			{
				auto text = index.data(Qt::UserRole).toString();
				QDesktopServices::openUrl(QUrl::fromLocalFile(QFileInfo(text).path()));
			}
		});
		p_contextMenu->addAction(action);
		p_contextMenu->addSeparator();
	}

	if (m_menuAction & MenuAction::RemoveSelected)
	{
		auto action = new QAction(QIcon(":/paint/images/remove_icon.png"), QApplication::translate("XrayStandardModelTreeView", "Remove Selected"), this);
		connect(action, &QAction::triggered, this, &XrayStandardModelTreeView::removeSelected);
		p_contextMenu->addAction(action);
		p_contextMenu->addSeparator();
	}

	if (m_menuAction & MenuAction::ExpandAll)
	{
		auto action = new QAction(QIcon(":/paint/images/.png"), QApplication::translate("XrayStandardModelTreeView", "Expand All"), this);
		connect(action, &QAction::triggered, this, &XrayStandardModelTreeView::expandAll);
		p_contextMenu->addAction(action);
	}

	if (m_menuAction & MenuAction::CollapseAll)
	{
		auto action = new QAction(QIcon(":/paint/images/.png"), QApplication::translate("XrayStandardModelTreeView", "Collapse All"), this);
		connect(action, &QAction::triggered, this, &XrayStandardModelTreeView::collapseAll);
		p_contextMenu->addAction(action);
	}
}

bool XrayStandardModelTreeView::isParent(const QModelIndex& index)
{
	return index.data(Qt::AccessibleDescriptionRole).toString() == "parent";
}
bool XrayStandardModelTreeView::isChild(const QModelIndex& index)
{
	return index.data(Qt::AccessibleDescriptionRole).toString() == "child";
}

QStandardItem* XrayStandardModelTreeView::addParent(const QString& text, bool bold, bool italic)
{
	auto item = new QStandardItem(text);
	item->setFlags(m_parentFlags);
	item->setData("parent", Qt::AccessibleDescriptionRole);
	item->setCheckable(false);
	auto font = item->font();
	font.setBold(bold);
	font.setItalic(italic);
	item->setFont(font);
	p_sourceModel->appendRow(item);
	updateMoveToMenu();	// update MoveTo menu of the context menu
	return item;
}

QStandardItem* XrayStandardModelTreeView::addChild(QStandardItem* parent, const QString& text, const QVariant& data, bool checkable)
{
	auto item = new QStandardItem(text);
	item->setFlags(m_childFlags);
	item->setData(data, Qt::UserRole);
	item->setData("child", Qt::AccessibleDescriptionRole);
	item->setCheckable(checkable);
	if (parent) parent->appendRow(item);
	return item;
}
QStandardItem* XrayStandardModelTreeView::addChild(const QString& parentText, const QString& text, const QVariant& data, bool checkable)
{
	if (p_sourceModel->rowCount() == 0)
	{
		auto p = addParent(parentText, true, false);
		return addChild(p, text, data, checkable);
	}
	else
	{
		auto p = p_sourceModel->item(p_sourceModel->rowCount() - 1, 0);
		return addChild(p, text, data, checkable);
	}
}
QStandardItem* XrayStandardModelTreeView::childItemByData(const QVariant& data)
{
	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i);
		if (item)
		{
			for (auto j = 0; j < item->rowCount(); j++)
			{
				auto ch = item->child(j);
				if (ch)
				{
					if (ch->index().data(Qt::UserRole) == data)
					{
						return ch;
					}
				}
			}
		}
	}

	return nullptr;
}
void XrayStandardModelTreeView::setChildData(const QVariant& data, const QVariant& newData)
{
	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i);
		if (item)
		{
			for (auto j = 0; j < item->rowCount(); j++)
			{
				auto ch = item->child(j);
				if (ch)
				{
					if (ch->index().data(Qt::UserRole) == data)
					{
						ch->setData(newData, Qt::UserRole);
						return;
					}
				}
			}
		}
	}
}
void XrayStandardModelTreeView::setChildDataMatchFileName(const QString& newData)
{
	auto newName = QFileInfo(newData).fileName();

	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i);
		if (item)
		{
			for (auto j = 0; j < item->rowCount(); j++)
			{
				auto ch = item->child(j);
				if (ch)
				{
					auto oldName = QFileInfo(ch->index().data(Qt::UserRole).toString()).fileName();

					if (oldName == newName)
					{
						ch->setData(newData, Qt::UserRole);
						return;
					}
				}
			}
		}
	}
}

void XrayStandardModelTreeView::setParentFlags(Qt::ItemFlags flags)
{
	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i);
		if (item) item->setFlags(flags);
	}
	m_parentFlags = flags;
}
void XrayStandardModelTreeView::setChildFlags(Qt::ItemFlags flags)
{
	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i);
		if (item)
		{
			for (auto j = 0; j < item->rowCount(); j++)
			{
				auto ch = item->child(j);
				if (ch)
				{
					ch->setFlags(flags);
				}
			}
		}
	}
	m_childFlags = flags;
}
void XrayStandardModelTreeView::setCheckState(Qt::CheckState state)
{
	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i);
		if (item)
		{
			for (auto j = 0; j < item->rowCount(); j++)
			{
				auto ch = item->child(j);
				if (ch)
				{
					if (ch->isCheckable())
						ch->setCheckState(state);
				}
			}

			if (item->isCheckable())
				item->setCheckState(state);
		}
	}
}

QList<QStandardItem*> XrayStandardModelTreeView::parentItems() const
{
	QList<QStandardItem*> list;
	list.reserve(p_sourceModel->rowCount());
	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
		list.append(p_sourceModel->item(i));
	return list;
}
QList<QStandardItem*> XrayStandardModelTreeView::childItems(QStandardItem* parent) const
{
	QList<QStandardItem*> list;
	if (!parent)
		return list;

	list.reserve(parent->rowCount());
	for (auto i = 0; i < parent->rowCount(); i++)
		list.append(parent->child(i));
	return list;
}
bool XrayStandardModelTreeView::hasDuplicateChildItem() const
{
	QSet<QString> s;
	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i);
		if (item)
		{
			for (auto j = 0; j < item->rowCount(); j++)
			{
				auto ch = item->child(j);
				if (ch)
				{
					if (s.contains(ch->text()))
						return true;
					s.insert(ch->text());
				}
			}
		}
	}

	return false;
}
QStringList XrayStandardModelTreeView::childsDataText() const
{
	QStringList list;
	list.reserve(p_sourceModel->rowCount());
	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i);
		if (item)
		{
			for (auto j = 0; j < item->rowCount(); j++)
			{
				auto ch = item->child(j);
				if (ch)
				{
					list.append(ch->index().data(Qt::UserRole).toString());
				}
			}
		}
	}

	return list;
}
QStringList XrayStandardModelTreeView::childsDataText(QStandardItem* parent) const
{
	QStringList list;
	if (parent)
	{
		list.reserve(parent->rowCount());
		for (auto j = 0; j < parent->rowCount(); j++)
		{
			auto ch = parent->child(j);
			if (ch)
			{
				list.append(ch->index().data(Qt::UserRole).toString());
			}
		}
	}

	return list;
}

bool XrayStandardModelTreeView::removeParent(const QString& text, int column)
{
	auto list = p_sourceModel->findItems(text, Qt::MatchExactly, column);
	if (!list.empty())
	{
		auto ok = false;
		while (!list.isEmpty())
		{
			auto idx = list.takeLast()->index();
			ok = p_sourceModel->removeRow(idx.row(), idx.parent());
		}

		return ok;
	}
	return false;
}
void XrayStandardModelTreeView::removeChilds(const QString& text, bool removeAllSimilar)
{
	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i, 0);
		if (item)
		{
			auto n = item->rowCount();
			for (int j = n - 1; j >= 0; j--)
			{
				auto ch = item->child(j);
				if (ch)
				{
					if (ch->index().data(Qt::UserRole).toString() == text)
					{
						item->removeRow(ch->row());
						if (!removeAllSimilar)
							return;
					}
				}
			}
		}
	}
}
void XrayStandardModelTreeView::removeChild(const QString& text)
{
	removeChilds(text, false);
}
void XrayStandardModelTreeView::removeChilds(QStandardItem* parent, const QString& text, bool removeAllSimilar)
{
	if (!parent)
		return;

	if (parent->hasChildren())
	{
		auto n = parent->rowCount();
		for (int j = n - 1; j >= 0; j--)
		{
			auto ch = parent->child(j);
			if (ch)
			{
				if (ch->index().data(Qt::UserRole).toString() == text)
				{
					parent->removeRow(ch->row());
					if (!removeAllSimilar)
						return;
				}
			}
		}
	}
}
void XrayStandardModelTreeView::removeChild(QStandardItem* parent, const QString& text)
{
	removeChilds(parent, text, false);
}

void XrayStandardModelTreeView::removeSelected()
{
	if (m_showWarningMsgBoxOnRemove)
	{
		if (!selectionModel()->selectedIndexes().empty())
		{
			if (QMessageBox::No == QMessageBox(QMessageBox::Information, QApplication::applicationName(), QApplication::translate("XrayStandardModelTreeView", "Are you sure you want to close?"), QMessageBox::Yes | QMessageBox::No).exec())
				return;
		}
	}

	auto mode = selectionMode();
	if (mode == QAbstractItemView::MultiSelection || mode == QAbstractItemView::ExtendedSelection || mode == QAbstractItemView::ContiguousSelection)
	{
		while (!selectionModel()->selectedIndexes().isEmpty())
		{
			auto index = selectionModel()->selectedIndexes().first();

			QStringList list;
			if (isParent(index))
				list = childsDataText(p_sourceModel->itemFromIndex(index));

			auto text = index.data(Qt::UserRole).toString();
			if (p_sourceModel->removeRow(index.row(), index.parent()))
			{
				// if index is a parent then emit a signal that all of its childs will also be removed
				for (const auto& i : list)
					emit removedItem(i);

				emit removedItem(text);
			}
		}
	}
	else if (mode == QAbstractItemView::SingleSelection)
	{
		auto index = currentIndex();

		QStringList list;
		if (isParent(index))
			list = childsDataText(p_sourceModel->itemFromIndex(index));

		auto text = index.data(Qt::UserRole).toString();
		if (p_sourceModel->removeRow(index.row(), index.parent()))
		{
			// if index is a parent then emit a signal that all of its childs will also be removed
			for (const auto& i : list)
				emit removedItem(i);

			emit removedItem(text);
		}
	}

	// update MoveTo menu of the context menu
	updateMoveToMenu();
}

void XrayStandardModelTreeView::sortChildren(int column, Qt::SortOrder order)
{
	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i);
		if (item)
		{
			if (item->hasChildren())
				item->sortChildren(column, order);
		}
	}
}

void XrayStandardModelTreeView::setCurrentChildIndex(const QString& text)
{
	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i);
		if (item)
		{
			for (auto j = 0; j < item->rowCount(); j++)
			{
				auto ch = item->child(j);
				if (ch)
				{
					if (ch->index().data(Qt::UserRole).toString() == text)
					{
						setCurrentIndex(ch->index());
						emit activatedItem(ch->index());
						return;
					}
				}
			}
		}
	}
}

void XrayStandardModelTreeView::updateTreeData()
{
	m_treeStruct.clear();
	m_treeStruct.reserve(p_sourceModel->rowCount());

	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i);
		if (item)
		{
			QVector<QPair<QString, QString> > childs;
			childs.reserve(item->rowCount());

			for (auto j = 0; j < item->rowCount(); j++)
			{
				auto ch = item->child(j);
				if (ch)
				{
					childs.append({ ch->text(), ch->index().data(Qt::UserRole).toString() });
				}
			}

			m_treeStruct.append({ item->text(), childs });
		}
	}
}
void XrayStandardModelTreeView::setTreeData(const QVector<QPair<QString, QVector<QPair<QString, QString> > > >& parentAndChildItems)
{
	m_treeStruct = parentAndChildItems;
	buildFromTreeData();
	updateMoveToMenu();
	expandAll();
}
QVector<QPair<QString, QVector<QPair<QString, QString> > > >& XrayStandardModelTreeView::treeData()
{
	updateTreeData();
	return m_treeStruct; 
}
void XrayStandardModelTreeView::buildFromTreeData()
{
	p_sourceModel->clear();
	for (const auto& i : m_treeStruct)
	{
		auto p = addParent(i.first, true, false);
		qDebug() << p->text();
		for (const auto& j : i.second)
		{
			qDebug() << "   " << j.second;

			addChild(p, j.first, j.second, true);
		}
	}
}

void XrayStandardModelTreeView::updateMoveToMenu()
{
	if (!p_groupMenu)
		return;

	p_groupMenu->clear();

	for (auto i = 0; i < p_sourceModel->rowCount(); i++)
	{
		auto item = p_sourceModel->item(i);
		if (item)
		{
			auto action = new QAction(item->text(), this);
			connect(action, &QAction::triggered, [this, action]() { moveChildsTo(action->text()); });
			p_groupMenu->addAction(action);
		}
	}
}

void XrayStandardModelTreeView::moveChildsTo(const QString& groupName)
{
	auto list = p_sourceModel->findItems(groupName, Qt::MatchExactly, 0);
	if (list.empty() || p_sourceModel->rowCount() <= 1)
		return;

	auto dst = list.first();

	auto b = false;
	while (!selectionModel()->selectedIndexes().isEmpty())
	{
		auto index = selectionModel()->selectedIndexes().first();

		auto name = index.data(Qt::DisplayRole).toString();
		auto data = index.data(Qt::UserRole).toString();

		b = p_sourceModel->removeRow(index.row(), index.parent());

		addChild(dst, name, data, true);
	}

	if (b)
	{
		expand(dst->index());

		emit itemsMoved();
	}
}

void XrayStandardModelTreeView::moveCurrentRow(bool isUp)
{
	auto index = currentIndex();
	if (index.isValid())
	{
		if (isParent(index))
		{
			const int row = index.row();

			// No selection (selectionModel()->setCurrentIndex(indexBelow(currentIndex()), QItemSelectionModel::NoUpdate);)
			// is needed when we move an item upward. It is only needed when we move downward
			if (isUp)
			{
				auto tar = row - 1;
				if (tar < 0)
					return;

				auto rowItems = p_sourceModel->takeRow(row);
				p_sourceModel->insertRow(tar, rowItems);
			}
			else
			{
				auto tar = row + 1;
				if (tar >= p_sourceModel->rowCount())
					return;

				auto rowItems = p_sourceModel->takeRow(row);
				p_sourceModel->insertRow(tar, rowItems);

				// at row = 0, selection automatically occurs at item 1, and after that we have to manually set it
				if (row != 0)
					selectionModel()->setCurrentIndex(indexBelow(currentIndex()), QItemSelectionModel::NoUpdate); // must be currentIndex()
			}
		}
		else if (isChild(index))
		{
			auto item = p_sourceModel->itemFromIndex(index)->parent();

			const int row = index.row();

			// No selection (selectionModel()->setCurrentIndex(indexBelow(currentIndex()), QItemSelectionModel::NoUpdate);)
			// is needed when we move an item upward. It is only needed when we move downward
			if (isUp)
			{
				auto tar = row - 1;
				if (tar < 0)
					return;

				auto rowItems = item->takeRow(row);
				item->insertRow(tar, rowItems);
			}
			else
			{
				auto tar = row + 1;
				if (tar >= item->rowCount())
					return;

				auto rowItems = item->takeRow(row);
				item->insertRow(tar, rowItems);

				// at row = 0, selection automatically occurs at item 1, and after that we have to manually set it
				if (row != 0)
					selectionModel()->setCurrentIndex(indexBelow(currentIndex()), QItemSelectionModel::NoUpdate); // must be currentIndex()
			}

		}

		emit itemsMoved();
	}
}
void XrayStandardModelTreeView::moveUp()
{
	moveCurrentRow(true);
}
void XrayStandardModelTreeView::moveDown()
{
	moveCurrentRow(false);
}

void XrayStandardModelTreeView::keyPressEvent(QKeyEvent* event)
{
	if (event->modifiers() & Qt::AltModifier && event->key() == Qt::Key_Up)
	{
		moveUp();
	}
	else if (event->modifiers() & Qt::AltModifier && event->key() == Qt::Key_Down)
	{
		moveDown();
	}

	QTreeView::keyPressEvent(event);

	// emit activation when an item is selected by key up and down.
	if (event->key() == Qt::Key_Up)
	{
		emit activatedItem(currentIndex());
	}
	else if (event->key() == Qt::Key_Down)
	{
		emit activatedItem(currentIndex());
	}
}
void XrayStandardModelTreeView::dropEvent(QDropEvent *event)
{
	QTreeView::dropEvent(event);

	// must uncheck after drag and drop, otherwise items remaine checked.
	setCheckState(Qt::Unchecked);

	expandAll();

	emit itemsMoved();
}

//class CustomTreeWidget : public QTreeWidget
//{
//public:
//	CustomTreeWidget(QWidget *parent = nullptr);
//protected:
//	void dragEnterEvent(QDragEnterEvent *event);
//	void dropEvent(QDropEvent *event);
//private:
//	QTreeWidgetItem* draggedItem;
//};
//void CustomTreeWidget::dragEnterEvent(QDragEnterEvent *event){
//    draggedItem = currentItem();
//    QTreeWidget::dragEnterEvent(event);
//}
//
//void CustomTreeWidget::dropEvent(QDropEvent *event){
//    QModelIndex droppedIndex = indexAt(event->pos());
//    if( !droppedIndex.isValid() )
//        return;
//
//    if(draggedItem){
//        QTreeWidgetItem* dParent = draggedItem->parent();
//        if(dParent){
//            if(itemFromIndex(droppedIndex.parent()) != dParent)
//                return;
//            dParent->removeChild(draggedItem);
//            dParent->insertChild(droppedIndex.row(), draggedItem);
//        }
//    }
//}