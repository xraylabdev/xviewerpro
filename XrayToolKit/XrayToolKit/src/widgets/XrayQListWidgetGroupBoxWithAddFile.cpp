/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2020/06/08
** filename: 	XrayQListWidgetGroupBoxWithAddFile.cpp
** file base:	XrayQListWidgetGroupBoxWithAddFile
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a list widget.
****************************************************************************/

#include "XrayQListWidgetGroupBoxWithAddFile.h"
#include "XrayGlobal.h"

#include <QApplication>
#include <QFileDialog>

XRAYLAB_USING_NAMESPACE

XrayQListWidgetGroupBoxWithAddFile::XrayQListWidgetGroupBoxWithAddFile(const QString &title, QWidget *parent) :
	QGroupBox(title, parent)
{
	listWidget = new XrayQListWidget(this);

	browseButton = new QPushButton(QApplication::translate("XrayQListWidgetGroupBoxWithAddFile", "Browse..."));
	browseButton->setToolTip(QApplication::translate("XrayQListWidgetGroupBoxWithAddFile", "Browse for files to add to the list."));
	connect(browseButton, &QPushButton::clicked, this, &XrayQListWidgetGroupBoxWithAddFile::browseFile);

	hLayout = new QHBoxLayout;
	hLayout->addStretch();
	hLayout->addWidget(browseButton);

	vLayout = new QVBoxLayout;
	vLayout->addWidget(listWidget);
	vLayout->addLayout(hLayout);
	setLayout(vLayout);
}
XrayQListWidgetGroupBoxWithAddFile::~XrayQListWidgetGroupBoxWithAddFile() 
{
}

void XrayQListWidgetGroupBoxWithAddFile::addListTexts(const QStringList &list)
{
	// no duplicates are allowed.
	QSet<QString> lines;
	lines.reserve(listWidget->count() + list.size());

	for (auto i = 0; i < listWidget->count(); i++)
		lines.insert(listWidget->item(i)->text());

	for (const auto& i : list)
		lines.insert(i);

	listWidget->clear();
	listWidget->addItems(lines.toList());
	listWidget->setDragDropMode(QAbstractItemView::InternalMove);
	setSelectedListTexts(list);
}
void XrayQListWidgetGroupBoxWithAddFile::setListTexts(const QStringList &list)
{
	listWidget->clear();
	listWidget->addItems(list);
	listWidget->setDragDropMode(QAbstractItemView::InternalMove);
}
QStringList XrayQListWidgetGroupBoxWithAddFile::getListTexts() const
{
	QStringList lines;
	lines.reserve(listWidget->count());

	for (auto i = 0; i < listWidget->count(); i++)
		lines.append(listWidget->item(i)->text());

	return lines;
}

void XrayQListWidgetGroupBoxWithAddFile::setSelectedListTexts(const QStringList& texts)
{
	for (auto& i : texts)
	{
		auto selected = listWidget->findItems(i, Qt::MatchExactly);
		if (!selected.empty())
		{
			for (auto& item : selected)
				listWidget->setItemSelected(item, true);
		}
	}
}
QStringList XrayQListWidgetGroupBoxWithAddFile::getSelectedListTexts() const
{
	auto selected = listWidget->selectedItems();

	QStringList lines;
	lines.reserve(selected.size());

	for (auto i = 0; i < selected.size(); i++)
		lines.append(selected[i]->text());

	return lines;
}

XrayQListWidget* XrayQListWidgetGroupBoxWithAddFile::getListWidget() const
{
	return listWidget;
}

void XrayQListWidgetGroupBoxWithAddFile::setBrowseExtensionFilters(const QString& filters)
{
	extensionFilters = filters;
}
QString XrayQListWidgetGroupBoxWithAddFile::getBrowseExtensionFilters() const
{
	return extensionFilters;
}

void XrayQListWidgetGroupBoxWithAddFile::browseFile()
{
	auto fileNames = QFileDialog::getOpenFileNames(this, QApplication::translate("XrayQListWidgetGroupBoxWithAddFile", "Select Files"), XrayGlobal::getLastFileOpenPath(), extensionFilters, nullptr);
	if (fileNames.isEmpty())
		return;

	addListTexts(fileNames);

	XrayGlobal::setLastFileOpenPath(fileNames);
}