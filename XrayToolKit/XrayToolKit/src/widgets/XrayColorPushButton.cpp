/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/15
** filename: 	XrayColorPushButton.h
** file base:	XrayColorPushButton
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a dialog to get the new size from a user.
****************************************************************************/

#include "XrayColorPushButton.h"

#include <QApplication>
#include <QPainter>
#include <QPaintEvent>
#include <QColorDialog>

XRAYLAB_USING_NAMESPACE

XrayColorPushButton::XrayColorPushButton(QWidget* pParent) :
	QPushButton(pParent),
	m_margin(5),
	m_radius(4),
	m_color(Qt::black)
{
	setText("");
}

void XrayColorPushButton::paintEvent(QPaintEvent* _paintEvent)
{
	setText("");

	QPushButton::paintEvent(_paintEvent);

	QPainter painter(this);
	QRect c = _paintEvent->rect();
	c.adjust(m_margin, m_margin, -m_margin, -m_margin);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.setBrush(QBrush(isEnabled() ? m_color : Qt::lightGray));
	painter.setPen(QPen(isEnabled() ? QColor(25, 25, 25) : Qt::darkGray, 0.5));
	painter.drawRoundedRect(c, m_radius, Qt::AbsoluteSize);
}

void XrayColorPushButton::mousePressEvent(QMouseEvent* _event)
{
	Q_UNUSED(_event);

	m_lastColor = m_color;
	QColorDialog d(this);

	connect(&d, &QColorDialog::currentColorChanged, this, &XrayColorPushButton::onCurrentColorChanged);

	d.setWindowIcon(QApplication::windowIcon());
	d.setCurrentColor(m_color);
	if (d.exec() == 0)
	{
		setColor(m_lastColor, true);
		emit currentColorChanged(m_lastColor);
	}

	disconnect(&d, &QColorDialog::currentColorChanged, this, &XrayColorPushButton::onCurrentColorChanged);
}

int XrayColorPushButton::getMargin(void) const
{
	return m_margin;
}

void XrayColorPushButton::setMargin(const int& _margin)
{
	m_margin = _margin;
	update();
}

int XrayColorPushButton::getRadius(void) const
{
	return m_radius;
}

void XrayColorPushButton::setRadius(const int& _radius)
{
	m_radius = _radius;
	update();
}

QColor XrayColorPushButton::getColor(void) const
{
	return m_color;
}

void XrayColorPushButton::setColor(const QColor& _color, bool _blockSignals)
{
	auto b = blockSignals(_blockSignals);

	m_color = _color;
	update();

	blockSignals(b);
}

void XrayColorPushButton::onCurrentColorChanged(const QColor& _color)
{
	setColor(_color);

	emit currentColorChanged(m_color);
}

XrayColorFrame::XrayColorFrame(QWidget* _parent) :
	QFrame(_parent),
	m_colorButton(),
	m_colorCombo()
{
	setLayout(&m_mainLayout);

	m_mainLayout.addWidget(&m_colorButton, 0, 0, Qt::AlignLeft);
	//m_mainLayout.addWidget(&m_colorCombo, 0, 1);

	m_mainLayout.setContentsMargins(0, 0, 0, 0);

	m_colorButton.setFixedWidth(30);

	connect(&m_colorButton, &XrayColorPushButton::currentColorChanged, this, &XrayColorFrame::onCurrentColorChanged);
}

QColor XrayColorFrame::getColor(void) const
{
	return m_colorButton.getColor();
}

void XrayColorFrame::setColor(const QColor& _color, bool _blockSignals)
{
	m_colorButton.setColor(_color, _blockSignals);
}

void XrayColorFrame::onCurrentColorChanged(const QColor& _color)
{
	emit currentColorChanged(_color);
}
