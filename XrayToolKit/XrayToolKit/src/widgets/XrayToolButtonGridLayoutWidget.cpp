/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XrayToolButtonGridLayoutWidget.h
** file base:	XrayToolButtonGridLayoutWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that creates a widget with a grid of tool buttons.
****************************************************************************/

#include "XrayToolButtonGridLayoutWidget.h"
#include "XrayQtUtil.h"
#include <QtGui>

XRAYLAB_USING_NAMESPACE

XrayToolButtonGridLayoutWidget::XrayToolButtonGridLayoutWidget(const QList<QPair<QString, QString> >& _button_texts, int _ncols, QWidget* _parent) :
	QWidget(_parent),
	m_button_texts(_button_texts)
{
	setStyleSheet("QLabel { border: 0px; }");
	m_layout.setContentsMargins(5, 5, 5, 5);
	addButtons(_button_texts, _ncols);
	setLayout(&m_layout);
}
XrayToolButtonGridLayoutWidget::XrayToolButtonGridLayoutWidget(QWidget* _parent) :
	XrayToolButtonGridLayoutWidget({}, 0, _parent)
{
}

void XrayToolButtonGridLayoutWidget::addButtons(const QList<QPair<QString, QString> >& _button_texts, int _ncols)
{
	if (_button_texts.empty())
		return;

	auto rows = 0;
	auto cols = 0;
	for (auto i = 0; i < _button_texts.size(); i++)
	{
		auto btn = new XrayImageToolButton(i, _button_texts[i].first, _button_texts[i].second, 0);
		connect(btn, &XrayImageToolButton::parentSignal, this, &XrayToolButtonGridLayoutWidget::setToUnpressed);
		m_layout.addWidget(btn, rows, cols++);
		m_buttons.append(btn);
		if(cols > _ncols - 1)
		{
			rows++;
			cols = 0;
		}
	}
}

XrayImageToolButton* XrayToolButtonGridLayoutWidget::getButton(const QString& _text)
{
	for (auto& i : m_buttons)
	{
		if (i->text() == _text)
			return i;
	}
	return nullptr;
}

void XrayToolButtonGridLayoutWidget::removeAll()
{
	XrayQtUtil::deleteChildWidgets(&m_layout);
	m_buttons.clear();
}

void XrayToolButtonGridLayoutWidget::resizeEvent(QResizeEvent* _ev)
{
	QWidget::resizeEvent(_ev);
}

void XrayToolButtonGridLayoutWidget::setToUnpressed(XrayImageToolButton* _btn)
{
	for (auto button : m_buttons)
		if (button != _btn)
			button->setToPressed(false);
}