/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XrayFramelessWindow.cpp
** file base:	XrayFramelessWindow
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a frame less widget.
****************************************************************************/

#include "XrayFramelessWindow.h"
#include "ui_XrayFramelessWindow.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QGraphicsDropShadowEffect>
#include <QMenu>
#include <QStyle>
#include <QSettings>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayFramelessWindow::XrayFramelessWindow(QWidget *parent)
	: QWidget(parent),
	ui(new Ui::XrayFramelessWindow),
	m_mouse_pressed(false),
	m_drag_top(false),
	m_drag_left(false),
	m_drag_right(false),
	m_drag_bottom(false),
	m_clear_settings(false)
{
	setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint);
	// append minimize button flag in case of windows,
	// for correct windows native handling of minimize function
	#if defined(Q_OS_WIN)
	setWindowFlags(windowFlags() | Qt::WindowMinimizeButtonHint);
	#endif
	setAttribute(Qt::WA_NoSystemBackground, true);
	setAttribute(Qt::WA_TranslucentBackground);

	ui->setupUi(this);
	ui->restoreButton->setVisible(false);

	//ui->windowTitlebar->layout()->setSpacing(10);
	//ui->windowTitlebar->layout()->setContentsMargins(0, 5, 5, 5);
	//ui->windowTitlebar->setFixedHeight(30);

	QObject::connect(qApp, &QGuiApplication::applicationStateChanged, this, &XrayFramelessWindow::on_applicationStateChanged);
	setMouseTracking(true);

	// important to watch mouse move from all child widgets
	QApplication::instance()->installEventFilter(this);
}
XrayFramelessWindow::~XrayFramelessWindow()
{
	delete ui;
}
void XrayFramelessWindow::saveSettings()
{
	if (!m_clear_settings)
	{
		QSettings settings(QApplication::organizationName(), windowTitle());
		settings.setValue("MainWindow/geometry", saveGeometry());
	}
}
void XrayFramelessWindow::restoreSettings()
{
	if (!m_clear_settings)
	{
		QSettings settings(QApplication::organizationName(), windowTitle());
		restoreGeometry(settings.value("MainWindow/geometry").toByteArray());
	}
}
void XrayFramelessWindow::clearSettings()
{
	QSettings settings(QApplication::organizationName(), windowTitle());
	settings.clear();
	m_clear_settings = true;
}

QToolButton* XrayFramelessWindow::maximumButton() const
{
	return ui->maximizeButton;
}
QToolButton* XrayFramelessWindow::minimumButton() const
{
	return ui->minimizeButton;
}
QToolButton* XrayFramelessWindow::menuButton() const
{
	return ui->menuButton;
}
QLabel* XrayFramelessWindow::titleLabel() const
{
	return ui->titleText;
}
XrayFramelessWindowDragger* XrayFramelessWindow::titleBar() const
{
	return ui->windowTitlebar;
}

void XrayFramelessWindow::on_restoreButton_clicked()
{
	if (ui->maximizeButton->isEnabled())
	{
		ui->restoreButton->setVisible(false);

		ui->maximizeButton->setVisible(true);
		setWindowState(Qt::WindowNoState);
		// on MacOS this hack makes sure the
		// background window is repaint correctly
		hide();
		show();
	}
}

void XrayFramelessWindow::on_maximizeButton_clicked()
{
	if (ui->maximizeButton->isEnabled())
	{
		ui->restoreButton->setVisible(true);
		ui->maximizeButton->setVisible(false);
		this->setWindowState(Qt::WindowMaximized);
		this->showMaximized();
		styleWindow(true, false);
	}
}

void XrayFramelessWindow::changeEvent(QEvent* _event)
{
	if (_event->type() == QEvent::WindowStateChange)
	{
		if (windowState().testFlag(Qt::WindowNoState))
		{
			if (ui->maximizeButton->isEnabled())
			{
				ui->restoreButton->setVisible(false);
				ui->maximizeButton->setVisible(true);
				styleWindow(true, true);
			}
			_event->ignore();
		}
		else if (windowState().testFlag(Qt::WindowMaximized))
		{
			if (ui->maximizeButton->isEnabled())
			{
				ui->restoreButton->setVisible(true);
				ui->maximizeButton->setVisible(false);
				styleWindow(true, false);
			}
			_event->ignore();
		}
	}
	_event->accept();
}

void XrayFramelessWindow::setCurrentIndex(int index)
{
	ui->stackedWidget->setCurrentIndex(index);
}
void XrayFramelessWindow::addWidget(QWidget *w)
{
	ui->stackedWidget->addWidget(w);
}
void XrayFramelessWindow::removeWidget(QWidget *w)
{
	ui->stackedWidget->removeWidget(w);
}

void XrayFramelessWindow::setWindowTitle(const QString& _text)
{
	ui->titleText->setText(_text);
	QWidget::setWindowTitle(_text);
}

void XrayFramelessWindow::setWindowTitleIcon(const QIcon& _ico, const QSize& _size)
{
	ui->icon->setPixmap(_ico.pixmap(_size));
}
void XrayFramelessWindow::setWindowIcon(const QIcon& _ico)
{
	QWidget::setWindowIcon(_ico);
}

void XrayFramelessWindow::styleWindow(bool _active, bool _no_state)
{
	if (_active)
	{
		if (_no_state)
		{
			layout()->setMargin(15);
			ui->windowTitlebar->setStyleSheet(QStringLiteral(
				"#windowTitlebar{border: 0px none palette(shadow); "
				"border-top-left-radius:5px; border-top-right-radius:5px; "
				"background-color:palette(shadow); height:20px;}"));
			ui->windowFrame->setStyleSheet(QStringLiteral(
				"#windowFrame{border:1px solid palette(highlight); border-radius:5px "
				"5px 5px 5px; background-color:palette(Window);}"));
		}
		else
		{
			layout()->setMargin(0);
			ui->windowTitlebar->setStyleSheet(QStringLiteral(
				"#windowTitlebar{border: 0px none palette(shadow); "
				"border-top-left-radius:0px; border-top-right-radius:0px; "
				"background-color:palette(shadow); height:20px;}"));
			ui->windowFrame->setStyleSheet(QStringLiteral(
				"#windowFrame{border:1px solid palette(dark); border-radius:0px 0px "
				"0px 0px; background-color:palette(Window);}"));
		}  // if (_no_state) else maximize
	}
	else
	{
		if (_no_state)
		{
			layout()->setMargin(15);
			ui->windowTitlebar->setStyleSheet(QStringLiteral(
				"#windowTitlebar{border: 0px none palette(shadow); "
				"border-top-left-radius:5px; border-top-right-radius:5px; "
				"background-color:palette(dark); height:20px;}"));
			ui->windowFrame->setStyleSheet(QStringLiteral(
				"#windowFrame{border:1px solid #000000; border-radius:5px 5px 5px "
				"5px; background-color:palette(Window);}"));
		}
		else
		{
			layout()->setMargin(0);
			ui->windowTitlebar->setStyleSheet(QStringLiteral(
				"#titlebarWidget{border: 0px none palette(shadow); "
				"border-top-left-radius:0px; border-top-right-radius:0px; "
				"background-color:palette(dark); height:20px;}"));
			ui->windowFrame->setStyleSheet(QStringLiteral(
				"#windowFrame{border:1px solid palette(shadow); border-radius:0px "
				"0px 0px 0px; background-color:palette(Window);}"));
		}  // if (_no_state) { else maximize
	}    // if (_active) { else no focus
}

void XrayFramelessWindow::on_applicationStateChanged(Qt::ApplicationState state)
{
	if (windowState().testFlag(Qt::WindowNoState))
	{
		if (state == Qt::ApplicationActive)
			styleWindow(true, true);
		else
			styleWindow(false, true);
	}
	else if (windowState().testFlag(Qt::WindowFullScreen))
	{
		if (state == Qt::ApplicationActive)
			styleWindow(true, false);
		else
			styleWindow(false, false);
	}
}

void XrayFramelessWindow::on_minimizeButton_clicked()
{
	setWindowState(Qt::WindowMinimized);
}

void XrayFramelessWindow::on_closeButton_clicked()
{
	close();
}

void XrayFramelessWindow::on_windowTitlebar_doubleClicked()
{
	if (windowState().testFlag(Qt::WindowNoState))
		on_maximizeButton_clicked();
	else if (windowState().testFlag(Qt::WindowFullScreen))
		on_restoreButton_clicked();
}

void XrayFramelessWindow::mouseDoubleClickEvent(QMouseEvent* _event)
{
	Q_UNUSED(_event);
}

void XrayFramelessWindow::checkBorderDragging(QMouseEvent* _event)
{
	if (isMaximized())
		return;

	auto globalMousePos = _event->globalPos();
	if (m_mouse_pressed)
	{
		// available geometry excludes taskbar
		auto availGeometry = QApplication::desktop()->availableGeometry();
		auto h = availGeometry.height();
		auto w = availGeometry.width();
		if (QApplication::desktop()->isVirtualDesktop())
		{
			auto sz = QApplication::desktop()->size();
			h = sz.height();
			w = sz.width();
		}

		// top right corner
		if (m_drag_top && m_drag_right)
		{
			int diff = globalMousePos.x() - (m_start_geometry.x() + m_start_geometry.width());
			auto neww = m_start_geometry.width() + diff;
			diff = globalMousePos.y() - m_start_geometry.y();
			auto newy = m_start_geometry.y() + diff;
			if (neww > 0 && newy > 0 && newy < h - 50)
			{
				auto newg = m_start_geometry;
				newg.setWidth(neww);
				newg.setX(m_start_geometry.x());
				newg.setY(newy);
				setGeometry(newg);
			}
		}
		// top left corner
		else if (m_drag_top && m_drag_left)
		{
			auto diff = globalMousePos.y() - m_start_geometry.y();
			auto newy = m_start_geometry.y() + diff;
			diff = globalMousePos.x() - m_start_geometry.x();
			auto newx = m_start_geometry.x() + diff;
			if (newy > 0 && newx > 0)
			{
				auto newg = m_start_geometry;
				newg.setY(newy);
				newg.setX(newx);
				setGeometry(newg);
			}
		}
		// bottom right corner
		else if (m_drag_bottom && m_drag_left)
		{
			auto diff = globalMousePos.y() - (m_start_geometry.y() + m_start_geometry.height());
			auto newh = m_start_geometry.height() + diff;
			diff = globalMousePos.x() - m_start_geometry.x();
			auto newx = m_start_geometry.x() + diff;
			if (newh > 0 && newx > 0)
			{
				auto newg = m_start_geometry;
				newg.setX(newx);
				newg.setHeight(newh);
				setGeometry(newg);
			}
		}
		else if (m_drag_top)
		{
			auto diff = globalMousePos.y() - m_start_geometry.y();
			auto newy = m_start_geometry.y() + diff;
			if (newy > 0 && newy < h - 50)
			{
				auto newg = m_start_geometry;
				newg.setY(newy);
				setGeometry(newg);
			}
		}
		else if (m_drag_left)
		{
			auto diff = globalMousePos.x() - m_start_geometry.x();
			auto newx = m_start_geometry.x() + diff;
			if (newx > 0 && newx < w - 50)
			{
				auto newg = m_start_geometry;
				newg.setX(newx);
				setGeometry(newg);
			}
		}
		else if (m_drag_right)
		{
			auto diff = globalMousePos.x() - (m_start_geometry.x() + m_start_geometry.width());
			auto neww = m_start_geometry.width() + diff;
			if (neww > 0)
			{
				auto newg = m_start_geometry;
				newg.setWidth(neww);
				newg.setX(m_start_geometry.x());
				setGeometry(newg);
			}
		}
		else if (m_drag_bottom)
		{
			auto diff = globalMousePos.y() - (m_start_geometry.y() + m_start_geometry.height());
			auto newh = m_start_geometry.height() + diff;
			if (newh > 0)
			{
				auto newg = m_start_geometry;
				newg.setHeight(newh);
				newg.setY(m_start_geometry.y());
				setGeometry(newg);
			}
		}
	}
	else
	{
		// no mouse pressed
		if (leftBorderHit(globalMousePos) && topBorderHit(globalMousePos))
		{
			setCursor(Qt::SizeFDiagCursor);
		}
		else if (rightBorderHit(globalMousePos) && topBorderHit(globalMousePos))
		{
			setCursor(Qt::SizeBDiagCursor);
		}
		else if (leftBorderHit(globalMousePos) &&
			bottomBorderHit(globalMousePos))
		{
			setCursor(Qt::SizeBDiagCursor);
		}
		else
		{
			if (topBorderHit(globalMousePos))
			{
				setCursor(Qt::SizeVerCursor);
			}
			else if (leftBorderHit(globalMousePos))
			{
				setCursor(Qt::SizeHorCursor);
			}
			else if (rightBorderHit(globalMousePos))
			{
				setCursor(Qt::SizeHorCursor);
			}
			else if (bottomBorderHit(globalMousePos))
			{
				setCursor(Qt::SizeVerCursor);
			}
			else
			{
				m_drag_top = false;
				m_drag_left = false;
				m_drag_right = false;
				m_drag_bottom = false;
				setCursor(Qt::ArrowCursor);
			}
		}
	}
}

// pos in global virtual desktop coordinates
bool XrayFramelessWindow::leftBorderHit(const QPoint& _pos)
{
	const auto& rect = this->geometry();
	return _pos.x() >= rect.x() && _pos.x() <= rect.x() + CONST_DRAG_BORDER_SIZE;
}

bool XrayFramelessWindow::rightBorderHit(const QPoint& _pos)
{
	const auto& rect = this->geometry();
	auto tmp = rect.x() + rect.width();
	return _pos.x() <= tmp && _pos.x() >= (tmp - CONST_DRAG_BORDER_SIZE);
}

bool XrayFramelessWindow::topBorderHit(const QPoint& _pos)
{
	const auto& rect = this->geometry();
	return _pos.y() >= rect.y() && _pos.y() <= rect.y() + CONST_DRAG_BORDER_SIZE;
}

bool XrayFramelessWindow::bottomBorderHit(const QPoint& _pos)
{
	const auto& rect = this->geometry();
	auto tmp = rect.y() + rect.height();
	return _pos.y() <= tmp && _pos.y() >= (tmp - CONST_DRAG_BORDER_SIZE);
}

void XrayFramelessWindow::mousePressEvent(QMouseEvent* _event)
{
	if (isMaximized())
		return;

	m_mouse_pressed = true;
	m_start_geometry = this->geometry();

	auto globalMousePos = mapToGlobal(QPoint(_event->x(), _event->y()));

	if (leftBorderHit(globalMousePos) && topBorderHit(globalMousePos))
	{
		m_drag_top = true;
		m_drag_left = true;
		setCursor(Qt::SizeFDiagCursor);
	}
	else if (rightBorderHit(globalMousePos) && topBorderHit(globalMousePos))
	{
		m_drag_right = true;
		m_drag_top = true;
		setCursor(Qt::SizeBDiagCursor);
	}
	else if (leftBorderHit(globalMousePos) && bottomBorderHit(globalMousePos))
	{
		m_drag_left = true;
		m_drag_bottom = true;
		setCursor(Qt::SizeBDiagCursor);
	}
	else
	{
		if (topBorderHit(globalMousePos))
		{
			m_drag_top = true;
			setCursor(Qt::SizeVerCursor);
		}
		else if (leftBorderHit(globalMousePos))
		{
			m_drag_left = true;
			setCursor(Qt::SizeHorCursor);
		}
		else if (rightBorderHit(globalMousePos))
		{
			m_drag_right = true;
			setCursor(Qt::SizeHorCursor);
		}
		else if (bottomBorderHit(globalMousePos))
		{
			m_drag_bottom = true;
			setCursor(Qt::SizeVerCursor);
		}
	}
}

void XrayFramelessWindow::mouseReleaseEvent(QMouseEvent* _event)
{
	Q_UNUSED(_event);
	if (isMaximized())
		return;

	m_mouse_pressed = false;
	bool bSwitchBackCursorNeeded = m_drag_top || m_drag_left || m_drag_right || m_drag_bottom;
	m_drag_top = false;
	m_drag_left = false;
	m_drag_right = false;
	m_drag_bottom = false;
	if (bSwitchBackCursorNeeded)
		setCursor(Qt::ArrowCursor);
}

bool XrayFramelessWindow::eventFilter(QObject* _obj, QEvent* _event)
{
	if (isMaximized())
	{
		return QWidget::eventFilter(_obj, _event);
	}

	// check mouse move event when mouse is moved on any object
	if (_event->type() == QEvent::MouseMove)
	{
		auto m = dynamic_cast<QMouseEvent*>(_event);
		if (m) checkBorderDragging(m);
	}
	// press is triggered only on frame window
	else if (_event->type() == QEvent::MouseButtonPress && _obj == this)
	{
		auto m = dynamic_cast<QMouseEvent*>(_event);
		if (m) mousePressEvent(m);
	}
	else if (_event->type() == QEvent::MouseButtonRelease)
	{
		if (m_mouse_pressed)
		{
			auto m = dynamic_cast<QMouseEvent*>(_event);
			if (m) mouseReleaseEvent(m);
		}
	}

	return QWidget::eventFilter(_obj, _event);
}