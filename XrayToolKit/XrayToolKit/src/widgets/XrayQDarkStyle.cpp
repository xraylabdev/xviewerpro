/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XrayQDarkStyle.cpp
** file base:	XrayQDarkStyle
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a dark style for Qt applications.
****************************************************************************/

#include "XrayQDarkStyle.h"

#include <QStyleFactory>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayQDarkStyle::XrayQDarkStyle() : 
	XrayQDarkStyle(styleBase())
{
	
}
XrayQDarkStyle::XrayQDarkStyle(QStyle* _style) : 
	QProxyStyle(_style)
{
	
}

QStyle* XrayQDarkStyle::styleBase(QStyle* _style)
{
	static auto base = !_style ? QStyleFactory::create(QStringLiteral("Fusion")) : _style;
	return base;
}
QStyle* XrayQDarkStyle::baseStyle()
{
	return styleBase();
}

void XrayQDarkStyle::polish(QPalette& _palette)
{
	_palette.setColor(QPalette::Window, QColor(52, 52, 52));
	_palette.setColor(QPalette::WindowText, Qt::white);
	_palette.setColor(QPalette::Disabled, QPalette::WindowText, QColor(127, 127, 127));
	_palette.setColor(QPalette::Base, QColor(42, 42, 42));
	_palette.setColor(QPalette::AlternateBase, QColor(66, 66, 66));
	_palette.setColor(QPalette::ToolTipBase, Qt::white);
	_palette.setColor(QPalette::ToolTipText, QColor(52, 52, 52));
	_palette.setColor(QPalette::Text, Qt::white);
	_palette.setColor(QPalette::Disabled, QPalette::Text, QColor(127, 127, 127));
	_palette.setColor(QPalette::Dark, QColor(35, 35, 35));
	_palette.setColor(QPalette::Shadow, QColor(20, 20, 20));
	_palette.setColor(QPalette::Button, QColor(52, 52, 52));
	_palette.setColor(QPalette::ButtonText, Qt::white);
	_palette.setColor(QPalette::Disabled, QPalette::ButtonText, QColor(127, 127, 127));
	_palette.setColor(QPalette::BrightText, Qt::red);
	_palette.setColor(QPalette::Link, QColor(42, 130, 218));
	_palette.setColor(QPalette::Highlight, QColor(42, 130, 218)); // 42, 130, 218
	_palette.setColor(QPalette::Disabled, QPalette::Highlight, QColor(80, 80, 80));
	_palette.setColor(QPalette::HighlightedText, Qt::white);
	_palette.setColor(QPalette::Disabled, QPalette::HighlightedText, QColor(127, 127, 127));
}

void XrayQDarkStyle::polish(QApplication* _app)
{
	if (!_app) return;

	// increase font size for better reading,
	// setPointSize was reduced from +2 because when applied this way in Qt5, the
	// font is larger than intended for some reason
	//auto defaultFont = QApplication::font();
	//defaultFont.setPointSize(defaultFont.pointSize() + 1);
	//_app->setFont(defaultFont);

	QFile file(QStringLiteral(":/darkstyle/darkstyle.qss"));
	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		_app->setStyleSheet(QString::fromLatin1(file.readAll()));
		file.close();
	}
}
