/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/11
** filename: 	XrayImageSlideShowWidget.cpp
** file base:	XrayImageSlideShowWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides slide show widget with next, previous,
				play and pause buttons..
****************************************************************************/

#include "XrayImageSlideShowWidget.h"
#include "XrayIconPushButton.h"

#include <QGridLayout>
#include <QPropertyAnimation>
#include <QGraphicsDropShadowEffect>
#include <QDebug>
#include <QFileInfo>
#include <utility>
#include <QPushButton>

XRAYLAB_USING_NAMESPACE

XrayImageSlideShowWidget::XrayImageSlideShowWidget(const QStringList& _imageFiles, QWidget* _parent) :
	QLabel(_parent),
	m_currentIndex(0),
	m_lastIndex(0),
	m_slideShowDuration(2000),
	m_imageShowDuration(500),
	m_pauseAnimation(false),
	m_transitionType(FadeIn)
{
	setScaledContents(true);
	m_frontFrame.setScaledContents(true);

	QIcon icon;
	icon.addFile(":/res/images/pause_icon.png", QSize(22, 22), QIcon::Normal, QIcon::Off);
	icon.addFile(":/res/images/run_icon.png", QSize(22, 22), QIcon::Normal, QIcon::On);
	auto play = new QPushButton(icon, "", this);
	play->setFlat(true);
	play->setCheckable(true);
	play->setChecked(true);
	play->setStyleSheet("QPushButton { border: 0px; } QPushButton:pressed { background-color: rgba(52, 52, 52, 0); }");
	connect(play, &QPushButton::clicked, [this, play]()
	{
		m_transitionType = None;
		if (play->isChecked())
			stop();
		else
			start(100);
	});
	m_statusBar.addWidget(play);

	auto btn = new XrayIconPushButton(QIcon(":/res/images/arrow_left_icon.png"), this);
	btn->setFlat(true);
	btn->setStyleSheet("QPushButton:pressed { background-color: rgba(142, 21, 21, 20); } ");
	connect(btn, &QPushButton::clicked, [this, play]()
	{
		play->setChecked(true);
		previous(); 
	});
	m_statusBar.addPermanentWidget(btn);
	btn = new XrayIconPushButton(QIcon(":/res/images/arrow_right_icon.png"), this);
	btn->setFlat(true);
	btn->setStyleSheet("QPushButton:pressed { background-color: rgba(142, 21, 21, 20); } ");
	connect(btn, &QPushButton::clicked, [this, play]()
	{
		play->setChecked(true);
		next();
	});
	m_statusBar.addPermanentWidget(btn);

	auto g = new QVBoxLayout(this);
	g->setAlignment(Qt::AlignBottom);
	g->setContentsMargins(0, 0, 0, 0);
	g->addWidget(&m_frontFrame);
	g->addWidget(&m_statusBar);
	m_statusBar.setFixedHeight(26);
	m_statusBar.setSizeGripEnabled(false);
	m_statusBar.setStyleSheet("QStatusBar { background-color: transparent; }");
	m_statusBar.setHidden(true);

	connect(&m_timer, &QTimer::timeout, [&]()
	{
		if (!m_pauseAnimation)
			nextSlide();
	});

	setImageFiles(_imageFiles);
}
XrayImageSlideShowWidget::~XrayImageSlideShowWidget()
{
	m_timer.stop();
	disconnect(&m_timer, &QTimer::timeout, nullptr, nullptr);
}
void XrayImageSlideShowWidget::setTransitionType(TransitionType _type)
{
	if(_type == None)
		m_frontFrame.hide();	// first hide to solve the flickering effect because of QPropertyAnimation duration.
	m_transitionType = _type;
}
XrayImageSlideShowWidget::TransitionType XrayImageSlideShowWidget::getTransitionType() const
{
	return m_transitionType;
}
void XrayImageSlideShowWidget::setSlideShowTransitionDuration(int _msec)
{
	m_slideShowDuration = _msec;
}
int XrayImageSlideShowWidget::getSlideShowTransitionDuration() const
{
	return m_slideShowDuration;
}
void XrayImageSlideShowWidget::setTransitionDuration(int _msec)
{
	m_imageShowDuration = _msec;
}
int XrayImageSlideShowWidget::getTransitionDuration() const
{
	return m_imageShowDuration;
}
void XrayImageSlideShowWidget::pause(bool _b)
{
	m_pauseAnimation = _b;
}
bool XrayImageSlideShowWidget::pause() const
{
	return m_pauseAnimation;
}
QStatusBar& XrayImageSlideShowWidget::getStatusBar()
{
	return m_statusBar;
}

void XrayImageSlideShowWidget::setScaledContents(bool _scaled)
{
	QLabel::setScaledContents(_scaled);
	m_frontFrame.setScaledContents(_scaled);
}
void XrayImageSlideShowWidget::start(int _delay)
{
	if (m_timer.isActive())
		m_timer.stop();

	if (m_currentIndex < 0)
		m_currentIndex = m_imageFiles.size() - 1;
	if (m_currentIndex >= m_imageFiles.size())
		m_currentIndex = 0;
	showImage(m_currentIndex, m_slideShowDuration);

	m_timer.start(_delay);
}
void XrayImageSlideShowWidget::stop()
{
	m_timer.stop();
}

void XrayImageSlideShowWidget::setImageFiles(const QStringList& _fileNames)
{
	if (_fileNames.empty())
		return;

	m_imageFiles = _fileNames;
	m_currentIndex = 0;
	m_lastIndex = m_imageFiles.size() - 1;
	m_frontFrame.setPixmap(QPixmap(m_imageFiles[m_currentIndex]).scaled(width(), height(), Qt::KeepAspectRatio));
	setPixmap(QPixmap(m_imageFiles[m_lastIndex]).scaled(width(), height(), Qt::KeepAspectRatio));			/**< last image */
}
QStringList XrayImageSlideShowWidget::getImageFiles() const
{
	return m_imageFiles;
}

void XrayImageSlideShowWidget::showImage(int _index)
{
	showImage(_index, m_imageShowDuration);
}
void XrayImageSlideShowWidget::showImage(int _index, int _transitionDuration)
{
	if (_index < 0 || _index >= m_imageFiles.size() || m_imageFiles.empty())
		return;

	if (m_lastIndex == _index)
		return;

	if (m_transitionType != None)
	{
		m_frontFrame.hide();	// first hide to solve the flickering effect because of QPropertyAnimation duration.
		m_frontFrame.setPixmap(QPixmap(m_imageFiles[_index]).scaled(width(), height(), Qt::KeepAspectRatio));
		setPixmap(QPixmap(m_imageFiles[m_lastIndex]).scaled(width(), height(), Qt::KeepAspectRatio));			/**< last image */
		m_lastIndex = _index;
		m_frontFrame.show();	// now show it again after setting the back image.

		if(m_transitionType == FadeIn || m_transitionType == FadeOut)
		{
			const auto eff = new QGraphicsOpacityEffect(this);
			m_frontFrame.setGraphicsEffect(eff);
			auto a = new QPropertyAnimation(eff, "opacity");
			a->setDuration(_transitionDuration);
			a->setStartValue(0);
			a->setEndValue(1);
			if (m_transitionType == FadeIn)
				a->setEasingCurve(QEasingCurve::InBack);
			else if (m_transitionType == FadeOut)
				a->setEasingCurve(QEasingCurve::OutBack);
			a->start(QPropertyAnimation::DeleteWhenStopped);
			connect(a, &QPropertyAnimation::finished, [this, _index]()
			{
				setPixmap(QPixmap(m_imageFiles[_index]).scaled(width(), height(), Qt::KeepAspectRatio));			/**< last image */
			});	// to know when the transition is finished.
		}
		else
		{
			auto a = new QPropertyAnimation(&m_frontFrame, "pos");
			a->setDuration(_transitionDuration);
			if (m_transitionType == LeftToRight)
			{
				a->setStartValue(QPoint(0, 0));
				a->setEndValue(QPoint(width(), 0));
			}
			else if (m_transitionType == RightToLeft)
			{
				a->setStartValue(QPoint(width(), 0));
				a->setEndValue(QPoint(0, 0));
			}
			else if (m_transitionType == TopToBottom)
			{
				a->setStartValue(QPoint(0, height()));
				a->setEndValue(QPoint(0, 0));
			}
			else if (m_transitionType == BottomToTop)
			{
				a->setStartValue(QPoint(0, 0));
				a->setEndValue(QPoint(height(), 0));
			}
			a->setEasingCurve(QEasingCurve::InQuad);
			a->start(QPropertyAnimation::DeleteWhenStopped);
			connect(a, &QPropertyAnimation::finished, [this, _index]()
			{
				setPixmap(QPixmap(m_imageFiles[_index]).scaled(width(), height(), Qt::KeepAspectRatio));			/**< last image */
			});	// to know when the transition is finished.
		}
	}
	else
	{
		m_frontFrame.hide();	// first hide to solve the flickering effect because of QPropertyAnimation duration.
		m_frontFrame.setPixmap(QPixmap(m_imageFiles[_index]).scaled(width(), height(), Qt::KeepAspectRatio));
		setPixmap(QPixmap(m_imageFiles[_index]).scaled(width(), height(), Qt::KeepAspectRatio));			/**< last image */
		m_lastIndex = _index;
	}
}
void XrayImageSlideShowWidget::showImage(const QString& _fileName)
{
	if (m_imageFiles.empty())
		return;

	auto index = -1;
	for (auto i = 0; i < m_imageFiles.size(); i++)
	{
		if (m_imageFiles[i].endsWith(_fileName))
		{
			index = i;
			break;
		}
	}
	if(index >= 0)
		showImage(index);
}

void XrayImageSlideShowWidget::next()
{
	if (m_currentIndex < 0)
		m_currentIndex = m_imageFiles.size() - 1;

	if (m_currentIndex >= m_imageFiles.size())
		m_currentIndex = 0;

	showImage(m_currentIndex++);
}
void XrayImageSlideShowWidget::previous()
{
	if (m_currentIndex < 0)
		m_currentIndex = m_imageFiles.size() - 1;

	if (m_currentIndex >= m_imageFiles.size())
		m_currentIndex = 0;

	showImage(m_currentIndex--);
}
void XrayImageSlideShowWidget::nextSlide()
{
	if (m_currentIndex < 0)
		m_currentIndex = m_imageFiles.size() - 1;

	if (m_currentIndex >= m_imageFiles.size())
		m_currentIndex = 0;

	showImage(m_currentIndex++, m_slideShowDuration);
}

void XrayImageSlideShowWidget::enterEvent(QEvent* _e)
{
	pause(true);
	QWidget::enterEvent(_e);
}
void XrayImageSlideShowWidget::leaveEvent(QEvent* _e)
{
	pause(false);
	QWidget::leaveEvent(_e);
}
