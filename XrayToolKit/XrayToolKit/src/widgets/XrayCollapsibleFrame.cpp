/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayCollapsibleFrame.cpp
** file base:	XrayCollapsibleFrame
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a collapsible frame widget.
****************************************************************************/

#include "XrayCollapsibleFrame.h"

XRAYLAB_USING_NAMESPACE

XrayCollapsibleFrame::XrayCollapsibleFrame(QWidget *parent) :
	QFrame(parent),
	d(new XrayCollapsibleFrameP(this))
{
	d->button_toggled(d->button->isChecked());
}

QSize XrayCollapsibleFrame::minimumSizeHint() const
{
	const QMargins margins = d->verticalLayout->contentsMargins();
	QSize sh(d->button->minimumSizeHint());

	sh.rwidth() += margins.left() + margins.right();
	sh.rheight() += margins.top() + margins.bottom();

	if (d->collapsed)
	{
	}
	else if (d->widget)
	{
		int spacing = d->verticalLayout->spacing();

		if (spacing == -1)
		{
			spacing = style()->pixelMetric(QStyle::PM_LayoutVerticalSpacing);
		}

		if (spacing == -1)
		{
			spacing = style()->layoutSpacing(d->button->sizePolicy().controlType(),
				d->widget->sizePolicy().controlType(),
				Qt::Vertical,
				0,
				this);
		}

		sh.rwidth() = qMax(sh.width(), d->widget->minimumSizeHint().width());
		sh.rheight() += d->widget->minimumSizeHint().height();
		sh.rheight() += spacing;
	}

	return sh;
}

XrayCollapsibleFrameP::RestoreSizeBehavior XrayCollapsibleFrame::restoreSizeBehavior() const
{
	return d->resizeBehavior;
}

void XrayCollapsibleFrame::setRestoreSizeBehavior(XrayCollapsibleFrameP::RestoreSizeBehavior behavior)
{
	if (d->resizeBehavior == behavior)
	{
		return;
	}

	d->resizeBehavior = behavior;
	d->button_toggled(d->button->isChecked());
}

QWidget *XrayCollapsibleFrame::title() const
{
	return d->title;
}

QWidget *XrayCollapsibleFrame::takeTitle()
{
	if (!d->title)
	{
		return nullptr;
	}

	QWidget *title = d->title;
	d->title = nullptr;
	d->horizontalLayout->removeWidget(title);
	title->setParent(nullptr);

	d->button_toggled(d->button->isChecked());

	return title;
}

void XrayCollapsibleFrame::setTitle(QWidget *title)
{
	if (d->title == title)
	{
		return;
	}

	delete takeTitle();

	if (title) {
		d->title = title;
		d->horizontalLayout->addWidget(title);
		d->button_toggled(d->button->isChecked());
	}

	emit titleChanged();
}

QWidget *XrayCollapsibleFrame::widget() const
{
	return d->widget;
}

QWidget *XrayCollapsibleFrame::takeWidget()
{
	if (!d->widget) {
		return 0;
	}

	QWidget *widget = d->widget;
	d->widget = 0;
	d->verticalLayout->removeWidget(widget);
	widget->setParent(0);

	d->button_toggled(d->button->isChecked());

	return widget;
}

void XrayCollapsibleFrame::setWidget(QWidget *widget)
{
	if (d->widget == widget)
	{
		return;
	}

	delete takeWidget();

	if (widget) {
		d->widget = widget;
		d->verticalLayout->addWidget(widget);
		d->button_toggled(d->button->isChecked());
	}

	emit widgetChanged();
}

QString XrayCollapsibleFrame::collapseToolTip() const
{
	return d->button->toolTip();
}

QIcon XrayCollapsibleFrame::collapsedIcon() const
{
	return d->collapsedIcon.isNull() ? style()->standardIcon(QStyle::SP_ArrowRight)
		: d->collapsedIcon;
}

QIcon XrayCollapsibleFrame::expandedIcon() const
{
	return d->expandedIcon.isNull() ? style()->standardIcon(QStyle::SP_ArrowDown) : d->expandedIcon;
}

bool XrayCollapsibleFrame::isCollapsed() const
{
	return d->collapsed;
}

bool XrayCollapsibleFrame::isExpanded() const
{
	return !isCollapsed();
}

int XrayCollapsibleFrame::indentation() const
{
	return d->verticalLayout->contentsMargins().left();
}

void XrayCollapsibleFrame::setCollapseToolTip(const QString &toolTip)
{
	d->button->setToolTip(toolTip);
}

void XrayCollapsibleFrame::setCollapsedIcon(const QIcon &icon)
{
	d->collapsedIcon = icon;

	if (d->collapsed)
	{
		d->button->setIcon(collapsedIcon());
	}
}

void XrayCollapsibleFrame::setExpandedIcon(const QIcon &icon)
{
	d->expandedIcon = icon;

	if (!d->collapsed)
	{
		d->button->setIcon(expandedIcon());
	}
}

void XrayCollapsibleFrame::setIcon(const QIcon &icon)
{
	setCollapsedIcon(icon);
	setExpandedIcon(icon);
}

void XrayCollapsibleFrame::setCollapsed(bool collapsed)
{
	if (d->collapsed == collapsed)
	{
		return;
	}

	d->button->setChecked(!collapsed);
}

void XrayCollapsibleFrame::setExpanded(bool expanded)
{
	setCollapsed(!expanded);
}

void XrayCollapsibleFrame::setIndentation(int indentation)
{
	QMargins margins = d->verticalLayout->contentsMargins();
	margins.setLeft(indentation);
	d->verticalLayout->setContentsMargins(margins);
}

XrayCollapsibleFrameP::XrayCollapsibleFrameP(XrayCollapsibleFrame *_editor) : 
	QObject(_editor),
	editor(_editor), 
	horizontalLayout(new QHBoxLayout), 
	verticalLayout(new QVBoxLayout(editor)), 
	button(new XrayCollapsibleToolButton(editor)),
	collapsed(false)
{
	button->setToolButtonStyle(Qt::ToolButtonIconOnly);
	button->setCheckable(true);

	horizontalLayout->addWidget(button, 0, Qt::AlignLeft);

	verticalLayout->setContentsMargins(0, 3, 0, 0);
	verticalLayout->addLayout(horizontalLayout);
	verticalLayout->setAlignment(horizontalLayout, Qt::AlignTop);

	connect(button, SIGNAL(toggled(bool)), this, SLOT(button_toggled(bool)));
}
void XrayCollapsibleToolButton::paintEvent(QPaintEvent *event)
{
	Q_UNUSED(event);

	if (!icon().isNull())
	{
		const QIcon::Mode mode = isEnabled() ? QIcon::Normal : QIcon::Disabled;
		const QIcon::State state = QIcon::Off;
		QPainter painter(this);
		QRect r(QPoint(), iconSize());
		r.moveCenter(contentsRect().center());
		icon().paint(&painter, r, Qt::AlignCenter, mode, state);
	}
}

void XrayCollapsibleFrameP::restoreSize(QWidget *widget)
{
	if (widget->isHidden())
	{
		return;
	}

	if (collapsed)
	{
		if (oldCollapsedSize.isValid())
		{
			const auto layouts = widget->findChildren<QLayout *>();
			for (auto& layout : layouts)
			{
				layout->invalidate();
				layout->activate();
			}

			widget->resize(oldCollapsedSize);
		}
		emit editor->collapsed();
	}
	else {
		if (oldExpandedSize.isValid())
		{
			const auto layouts = widget->findChildren<QLayout *>();
			for (auto& layout : layouts)
			{
				layout->invalidate();
				layout->activate();
			}

			widget->resize(oldExpandedSize);
		}
		emit editor->expanded();
	}
}

void XrayCollapsibleFrameP::storeSizes(QWidget *widget)
{
	if (widget->isHidden() || !widget->size().isValid())
	{
		return;
	}

	oldCollapsedSize = collapsed ? widget->size() : oldCollapsedSize;
	oldExpandedSize = collapsed ? oldExpandedSize : widget->size();
}

void XrayCollapsibleFrameP::button_toggled(bool toggled)
{
	switch (resizeBehavior)
	{
	case XrayCollapsibleFrameP::RestoreWindowSizeBehavior:
		storeSizes(editor->window());
		break;
	case XrayCollapsibleFrameP::NoRestoreSizeBehavior:
		break;
	}

	collapsed = !toggled;

	if (collapsed)
	{
		emit editor->aboutToCollapse();
	}
	else
	{
		emit editor->aboutToExpand();
	}

	button->setIcon(collapsed ? editor->collapsedIcon() : editor->expandedIcon());
	button->setChecked(toggled);

	if (widget)
	{
		widget->setVisible(toggled);
	}

	switch (resizeBehavior)
	{
	case XrayCollapsibleFrameP::RestoreWindowSizeBehavior:
		QMetaObject::invokeMethod(this, [this]() { restoreSize(editor->window()); }, Qt::QueuedConnection);
		break;
	case XrayCollapsibleFrameP::NoRestoreSizeBehavior:
		break;
	}
}
