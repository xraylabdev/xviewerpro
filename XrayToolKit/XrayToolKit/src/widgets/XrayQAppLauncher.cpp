/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/28
** filename: 	XrayQApplication.cpp
** file base:	XrayQApplication
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a QApplication object with some
				pre-defined attributes.
****************************************************************************/

#include "XrayQAppLauncher.h"

XRAYLAB_USING_NAMESPACE

XrayQAppLauncher::XrayQAppLauncher(QObject *parent) : 
	QObject(parent), 
	m_process(new QProcess(this))
{
	connect(m_process, &QProcess::errorOccurred, this, &XrayQAppLauncher::onErrorOccurred, Qt::UniqueConnection);
}

void XrayQAppLauncher::lauch(const QString &program)
{
	m_process->start("\"" + program + "\"");
}

void XrayQAppLauncher::lauch(const QString &program, const QStringList &arguments)
{
	m_process->start("\"" + program + "\"", arguments);
}

bool XrayQAppLauncher::lauchDetached(const QString &program)
{
	return m_process->startDetached("\"" + program + "\"");
}

bool XrayQAppLauncher::isRunning()
{
	return m_process->state() == QProcess::Running ? true : false;
}

void XrayQAppLauncher::onErrorOccurred(QProcess::ProcessError error)
{
	Q_UNUSED(error);
#ifdef DEBUG
	qDebug() << QString("Error (%1) occurred while launching app.").arg(error);
#endif // DEBUG
}