/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/015
** filename: 	XrayWizardWidget.h
** file base:	XrayWizardWidget
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that creates a wizard of the added pages.
****************************************************************************/

#include "XrayStatusBar.h"
#include <QPainter>
#include <QStyleOption>
#include <QDebug>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayLabel : public QLabel
{
public:
	explicit XrayLabel(QWidget *parent = 0, Qt::WindowFlags f = Qt::WindowFlags());

protected:
	void paintEvent(QPaintEvent *event) override;

private:
	QString m_elidedText;
	QString m_lastText;
	int m_lastWidth;
};

XRAYLAB_END_NAMESPACE

XRAYLAB_USING_NAMESPACE

XrayLabel::XrayLabel(QWidget* parent, Qt::WindowFlags f) :
	QLabel(parent, f),
	m_lastWidth(0) 
{
	setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	setMinimumWidth(fontMetrics().averageCharWidth() * 10);	// set a min width to prevent the window from widening with long texts
}

// A simplified version of QLabel::paintEvent()
// without pixmap or shortcut but with eliding.
void XrayLabel::paintEvent(QPaintEvent* /*event*/) 
{
	auto cr = contentsRect().adjusted(margin(), margin(), -margin(), -margin());
	auto txt = text();
	// if the text is changed or its rect is resized (due to window resizing),
	// find whether it needs to be elided...
	if (txt != m_lastText || cr.width() != m_lastWidth)
	{
		m_lastText = txt;
		m_lastWidth = cr.width();
		m_elidedText = fontMetrics().elidedText(txt, Qt::ElideMiddle, cr.width());
	}
	// ... then, draw the (elided) text
	if (!m_elidedText.isEmpty()) 
	{
		QPainter painter(this);
		QStyleOption opt;
		opt.initFrom(this);
		style()->drawItemText(&painter, cr, alignment(), opt.palette, isEnabled(), m_elidedText, foregroundRole());
	}
}

XrayStatusBar::XrayStatusBar(QWidget* parent) :
	QStatusBar(parent),
	m_lastTimeOut(8000),
	m_messageDelay(250)
{
	p_statusLabel = new XrayLabel();
	p_statusLabel->setFrameShape(QFrame::NoFrame);
	p_statusLabel->setContentsMargins(4, 0, 4, 0);	// 4px space on both sides (not to be mixed with the permanent widget)
	addWidget(p_statusLabel);

	p_messageTimer = new QTimer(this);
	p_messageTimer->setSingleShot(true);
	p_messageTimer->setInterval(m_messageDelay);
	connect(p_messageTimer, &QTimer::timeout, this, &XrayStatusBar::reallyShowMessage);
}

XrayStatusBar::~XrayStatusBar()
{
	if (p_messageTimer) 
	{
		p_messageTimer->stop();
		delete p_messageTimer;
	}
}

void XrayStatusBar::setDefaultMessage(const QString& _message)
{
	// set the text on the label to prevent its disappearance on focusing menubar items
	// and also ensure that it contains no newline (because file names may contain it)
	p_statusLabel->setText(QString(_message).replace(QLatin1Char('\n'), QLatin1Char(' ')));
}

void XrayStatusBar::showMessage(const QString& _message, int _timeout)
{
	// don't show the message immediately
	m_lastMessage = _message;
	if(_timeout != 0)
		m_lastTimeOut = _timeout;
	if (!p_messageTimer->isActive()) 
		p_messageTimer->start();
}

void XrayStatusBar::reallyShowMessage()
{
	QStatusBar::showMessage(m_lastMessage, m_lastTimeOut);
}