/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/28
** filename: 	XraySplashScreen.cpp
** file base:	XraySplashScreen
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a splash screen with a progress bar.
****************************************************************************/

#include "XraySplashScreen.h"

#include <QApplication>
#include <QStyleOptionProgressBar>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XraySplashScreen::XraySplashScreen(const QPixmap &pixmap, int progressValue, Qt::WindowFlags f) :
	QSplashScreen(pixmap, f),
	m_progress(0),
	m_incremental(0),
	m_incrementalValue(progressValue),
	m_barTextAlign(Qt::AlignCenter),
	m_barColor(QColor(0, 200, 0)),
	m_textColor(QColor(0, 0, 0)),
	m_barRect(QRect(16, height() - 38, width() - 32, 22))
{
	QFont splashFont;
	splashFont.setFamily("Arial");
	splashFont.setBold(true);
	splashFont.setPixelSize(10);
	setFont(splashFont);
	setCursor(Qt::BusyCursor);
	setEnabled(false);	// disable click event on splash screen

	timerThread = new QThread;
	timer = new QTimer;
	timer->setInterval(100);
	timer->moveToThread(timerThread);
	connect(timerThread, &QThread::started, timer, static_cast<void (QTimer::*)()>(&QTimer::start));
	connect(timerThread, &QThread::finished, timer, &QTimer::deleteLater);
	connect(this, &XraySplashScreen::finished, timerThread, &QThread::quit);

	connect(timer, &QTimer::timeout, [this]()
	{
		m_incremental += m_incrementalValue;
		if (m_incremental >= 100)
			m_incremental = 100;

		setProgress(m_incremental);

		if (m_incremental >= 100)
			QTimer::singleShot(100, this, &XraySplashScreen::finished);
	});
}
XraySplashScreen::~XraySplashScreen()
{
	timerThread->quit();
	timerThread->wait();
	timerThread->deleteLater();
}
void XraySplashScreen::start()
{
	timerThread->start();

	show();
	raise();
	QApplication::processEvents(); // make sure splash screen gets drawn ASAP

	QEventLoop loop;
	connect(timerThread, &QThread::finished, &loop, &QEventLoop::quit);
	loop.exec(); // do event processing until the thread has finished!
}
void XraySplashScreen::setProgress(int value)
{
	value = std::min(std::max(0, value), 100);
	if (m_progress != value)
	{
		m_progress = value;
		update();
	}
}
void XraySplashScreen::drawContents(QPainter *painter)
{
	QSplashScreen::drawContents(painter);

	// draw progress bar
	QStyleOptionProgressBar pbstyle;
	pbstyle.initFrom(this);
	pbstyle.state = QStyle::State_Enabled;
	pbstyle.textVisible = false;
	pbstyle.minimum = 0;
	pbstyle.maximum = 100;
	pbstyle.progress = m_progress;
	pbstyle.invertedAppearance = false;
	auto pal = pbstyle.palette;
	pal.setColor(QPalette::Highlight, m_barColor);
	pbstyle.palette = pal;
	pbstyle.rect = m_barRect;

	style()->drawControl(QStyle::CE_ProgressBarContents, &pbstyle, painter, this);
	//style()->drawControl(QStyle::CE_ProgressBar, &pbstyle, painter, this);

	// draw texts
	painter->save();
	painter->setRenderHint(QPainter::Antialiasing);
	painter->drawText(m_barRect, m_barTextAlign, m_barText.isEmpty() ? QString::number(m_progress) + "%" : m_barText);
	for (const auto& t : m_texts)
	{
		painter->setFont(t.m_font);
		painter->setPen(t.m_textColor);
		painter->drawText(t.m_rect, t.m_textAlign, t.m_text);
	}
	painter->restore();
}
