/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/18
** filename: 	XrayQTableView.cpp
** file base:	XrayQTableView
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a table view.
****************************************************************************/

#include "XrayQTableView.h"
#include "XrayTableModel.h"

#include <QApplication>
#include <QClipboard>

#include <QAbstractItemModel>
#include <QEvent>
#include <QHeaderView>
#include <QLayout>
#include <QScrollBar>

XRAYLAB_USING_NAMESPACE

XrayQTableView::XrayQTableView(QWidget *parent) :
	QTableView(parent),
	m_menuAction(static_cast<MenuAction>(MenuAction::SelectAll | MenuAction::InvertSelection | MenuAction::Copy | MenuAction::RemoveSelected))
{
	setSortingEnabled(false);
	setAlternatingRowColors(true);
	setEditTriggers(QTableView::EditTrigger::NoEditTriggers);

	setSelectionMode(QAbstractItemView::SelectionMode::MultiSelection);

	// listen for show/hide events on the horizontal scroll bar.
	horizontalScrollBar()->installEventFilter(this);
	setFocusPolicy(Qt::StrongFocus);

	createCustomMenu();
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QTableView::customContextMenuRequested, this, &XrayQTableView::showContextMenu);
}
XrayQTableView::~XrayQTableView() 
{
}

void XrayQTableView::showContextMenu(const QPoint& _point)
{
	if (!indexAt(_point).isValid())
		return;

	m_currentPoint = _point;
	p_contextMenu->exec(viewport()->mapToGlobal(_point));
	//p_contextMenu->exec(QCursor::pos());
}
void XrayQTableView::createCustomMenu()
{
	p_contextMenu = new QMenu(this);

	if (m_menuAction & MenuAction::SelectAll)
	{
		auto action = new QAction(QIcon(":/paint/images/select_all_blue_icon.png"), QApplication::translate("XrayQTableView", "Select All"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			selectAll();
		});
		p_contextMenu->addAction(action);
	}

	if (m_menuAction & MenuAction::InvertSelection)
	{
		auto action = new QAction(QIcon(":/paint/images/invert_selection_blue_icon.png"), QApplication::translate("XrayQTableView", "Invert Selection"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			for (auto i = 0; i < model()->rowCount(); i++)
			{
				auto index = model()->index(i, 0);
				if (!selectionModel()->isRowSelected(i, index))
					selectRow(i);
			}
		});
		p_contextMenu->addAction(action);
		p_contextMenu->addSeparator();
	}

	if (m_menuAction & MenuAction::Copy)
	{
		auto action = new QAction(QIcon(":/paint/images/copy_blue_icon.png"), QApplication::translate("XrayQTableView", "Copy"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			auto m = qobject_cast<XrayTableModel*>(model());
			if (m)
			{
				auto s = m->writeToString(',');
				if(!s.empty())
					QApplication::clipboard()->setText(s.join(' '));
			}
			emit copy();
		});
		p_contextMenu->addAction(action);
	}

	if (m_menuAction & MenuAction::Paste)
	{
		auto action = new QAction(QIcon(":/paint/images/paste_blue_icon.png"), QApplication::translate("XrayQTableView", "Paste"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			emit paste();
		});
		p_contextMenu->addAction(action);
		p_contextMenu->addSeparator();
	}

	if (m_menuAction & MenuAction::RemoveSelected)
	{
		auto action = new QAction(QIcon(":/paint/images/delete_small_icon.png"), QApplication::translate("XrayQTableView", "Remove Selected"), this);
		connect(action, &QAction::triggered, this, [this]()
		{
			auto indexes = selectionModel()->selectedRows();

			std::vector<int> rows;
			rows.resize(indexes.count());
			for (auto i = 0; i < indexes.count(); i++)
				rows[i] = indexes[i].row();
			std::sort(rows.begin(), rows.end());
			std::reverse(rows.begin(), rows.end());	// important to reverse the container.

			for (const auto& i : rows)
				model()->removeRow(i, QModelIndex());

			if (!rows.empty())
				emit removed(rows);
		});
		p_contextMenu->addAction(action);
	}
}

void XrayQTableView::wheelEvent(QWheelEvent* _evt)
{
	// don't handle wheel events unless widget had focus.
	// this improves scrolling when scrollable widgets are nested
	// together with setFocusPolicy(Qt::StrongFocus).
	//if (hasFocus())
		QTableView::wheelEvent(_evt);
}

void XrayQTableView::setModel(QAbstractItemModel* mdl)
{
	auto curModel = this->model();
	if (curModel)
		curModel->disconnect(this, SLOT(invalidateLayout())); // disconnect all connections on model that connect to invalidateLayout();

	QTableView::setModel(mdl);
	if (mdl)
	{
		connect(mdl, &QAbstractItemModel::rowsInserted, this, &XrayQTableView::invalidateLayout);
		connect(mdl, &QAbstractItemModel::rowsRemoved, this, &XrayQTableView::invalidateLayout);
		connect(mdl, &QAbstractItemModel::modelReset, this, &XrayQTableView::invalidateLayout);
	}
	invalidateLayout();
}

void XrayQTableView::setRootIndex(const QModelIndex& idx)
{
	QTableView::setRootIndex(idx);
	invalidateLayout();
}

void XrayQTableView::invalidateLayout()
{
	for (auto w = parentWidget(); w && w->layout(); w = w->parentWidget())
		w->layout()->invalidate();

	updateGeometry();
}