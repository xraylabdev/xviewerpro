/****************************************************************************
** Copyright (C) 2007-2018 Klaralvdalens Datakonsult AB.  All rights reserved.
**
** This file is part of the KD Reports library.
**
** Licensees holding valid commercial KD Reports licenses may use this file in
** accordance with the KD Reports Commercial License Agreement provided with
** the Software.
**
**
** This file may be distributed and/or modified under the terms of the
** GNU Lesser General Public License version 2.1 and version 3 as published by the
** Free Software Foundation and appearing in the file LICENSE.LGPL.txt included.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Contact info@kdab.com if any conditions of this licensing are not
** clear to you.
**
**********************************************************************/

#include "XrayTableModel.h"
#include "XrayCSVHandler.h"
#include "XrayGlobal.h"

#include <QtGlobal>
#include <QtDebug>
#include <QFile>
#include <QByteArray>
#include <QString>
#include <QStringList>

XRAYLAB_USING_NAMESPACE

XrayTableModel::XrayTableModel(QObject * parent) :
	QAbstractTableModel(parent),
	m_dataHasHorizontalHeaders(true),
	m_dataHasVerticalHeaders(false),
	m_supplyHeaderData(true)
{
}

XrayTableModel::~XrayTableModel()
{
}

int XrayTableModel::rowCount(const QModelIndex &) const
{
	return static_cast<int>(m_rows.size());
}

int XrayTableModel::columnCount(const QModelIndex &) const
{
	if (m_rows.empty())
	{
		return 0;
	}
	else
	{
		return static_cast<int>(m_rows[0].size());
	}
}

QVariant XrayTableModel::data(const QModelIndex& index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (index.row() == -1 || index.column() == -1)
		return QVariant();

	Q_ASSERT(index.row() >= 0 && index.row() < rowCount());
	Q_ASSERT(index.column() >= 0 && index.column() < columnCount());

	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		try
		{
			return m_rows.at(index.row()).at(index.column());
		}
		catch (const std::out_of_range& e)
		{
			qDebug() << "Error: exception at TableModel. " << e.what();
		}
		catch (const std::runtime_error& e)
		{
			qDebug() << "Error: exception at TableModel. " << e.what();
		}
	}

	return QVariant();
}

QVariant XrayTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant result;

	switch (role)
	{
	case Qt::DisplayRole:
	case Qt::EditRole:
		if (m_supplyHeaderData)
		{
			if (orientation == Qt::Horizontal)  // column header data:
			{
				if (section < m_horizontalHeaderData.count())
					result = m_horizontalHeaderData[section];
			}
			else	// row header data:
			{
				if (section < m_verticalHeaderData.count())
					result = m_verticalHeaderData[section];
			}
		}
		break;
	case Qt::TextAlignmentRole:
		//        result = QVariant ( Qt::AlignHCenter | Qt::AlignHCenter );
		break;
	case Qt::DecorationRole:
	case Qt::ToolTipRole:
		break;
	default:
		break;
	}
	return result;
}

bool XrayTableModel::setData(const QModelIndex& index, const QVariant& value, int role/* = Qt::EditRole */)
{
	if (!index.isValid())
		return false;

	Q_ASSERT(index.row() >= 0 && index.row() < rowCount());
	Q_ASSERT(index.column() >= 0 && index.column() < columnCount());

	if (role == Qt::EditRole)
	{
		m_rows[index.row()][index.column()] = value;
		emit dataChanged(index, index);
		return true;
	}

	return false;
}

bool XrayTableModel::insertRows(int position, int rows, const QModelIndex &parent)
{
	auto columns = columnCount();
	beginInsertRows(parent, position, position + rows - 1);

	for (auto row = 0; row < rows; ++row)
	{
		QList<QVariant> items;
		for (auto column = 0; column < columns; ++column)
			items.append("");
		m_rows.insert(position, items);
	}

	endInsertRows();
	return true;
}
bool XrayTableModel::insertColumns(int position, int columns, const QModelIndex &parent)
{
	auto rows = rowCount();
	beginInsertColumns(parent, position, position + columns - 1);

	for (auto row = 0; row < rows; ++row)
	{
		for (auto column = position; column < columns; ++column)
			m_rows[row].insert(position, "");
	}

	endInsertColumns();
	return true;
}
bool XrayTableModel::removeRows(int position, int rows, const QModelIndex &parent)
{
	beginRemoveRows(parent, position, position + rows - 1);

	for (auto row = 0; row < rows; ++row)
		m_rows.removeAt(position);

	endRemoveRows();
	return true;
}
bool XrayTableModel::removeColumns(int position, int columns, const QModelIndex &parent)
{
	auto rows = rowCount();
	beginRemoveColumns(parent, position, position + columns - 1);

	for (auto row = 0; row < rows; ++row)
	{
		for (auto column = 0; column < columns; ++column)
			m_rows[row].removeAt(position);
	}

	endRemoveColumns();
	return true;
}
bool XrayTableModel::createTable(const QVector<QString>& headers, const QVector<QVector<QString> >& rows)
{
	std::vector<std::string> h;
	h.resize(static_cast<std::size_t>(headers.size()));
	for (auto i = 0; i < headers.size(); i++)
		h[i] = headers[i].toStdString();

	std::vector<std::vector<std::string> > ro;
	ro.resize(static_cast<std::size_t>(rows.size()));

	for (auto i = 0; i < rows.size(); i++)
	{
		const auto& r = rows[i];

		std::vector<std::string> row;
		row.resize(static_cast<std::size_t>(r.size()));

		for (auto j = 0; j < r.size(); j++)
			row[j] = r[j].toStdString();

		ro[i] = row;
	}

	return createTable(h, ro);
}
bool XrayTableModel::createTable(const std::vector<std::string>& headers, const std::vector<std::vector<std::string> >& rows)
{
	if (!headers.empty() && m_dataHasHorizontalHeaders)
	{
		m_horizontalHeaderData.clear();
		m_horizontalHeaderData.reserve(static_cast<int>(headers.size()));
		for (const auto& i : headers)
			m_horizontalHeaderData.push_back(QString::fromStdString(i));
	}

	if (!rows.empty())
	{
		m_rows.clear();
		m_rows.reserve(static_cast<int>(rows.size()));

		for (std::size_t i = 0; i < rows.size(); i++)
		{
			QList<QVariant> values;
			values.reserve(static_cast<int>(rows[i].size()));

			for (std::size_t j = 0; j < rows[i].size(); j++)
			{
				auto cell = QString::fromStdString(rows[i][j]);

				if (j == 0 && m_dataHasVerticalHeaders)   // interpret first column as row headers
				{
					m_verticalHeaderData.push_back(cell);
				}
				else
				{
					auto ok = false;
					const auto value = cell.toDouble(&ok);
					values.push_back(ok ? QVariant(value) : QVariant(cell));
				}
			}

			m_rows.push_back(values);
		}
	}
	else
	{
		m_rows.reserve(0);
	}

	resetInternalData();

	if (!m_rows.empty())
		xAppInfo("Table loaded with {} rows and {} columns.", rowCount(), columnCount());
	else
		xAppInfo("Table loaded but no model data found.");

	return true;
}

bool XrayTableModel::readCSV(const QString& filename, QChar delimiter)
{
	XrayCSVHandler reader;
	if (!reader.read(filename.toStdString(), delimiter.toLatin1()))
		return false;

	return createTable(reader.getHeaders(), reader.getData());
}

void XrayTableModel::clear()
{
	m_rows.clear();
	resetInternalData();
}
bool XrayTableModel::writeCSV(const QString& filename, QChar delimiter)
{
	QFile file(filename);
	if (file.open(QIODevice::WriteOnly)) 
	{
		QTextStream stream(&file);

		if (!m_horizontalHeaderData.empty())
		{
			stream << m_horizontalHeaderData[0];
			for (auto i = 1; i < m_horizontalHeaderData.size(); i++)
				stream << delimiter << m_horizontalHeaderData[i];
			stream << endl;
		}

		for (const auto& i : m_rows)
		{
			const auto& cols = i;
			if (!cols.empty())
			{
				stream << cols[0].toString();
				for (auto j = 1; j < cols.size(); j++)
					stream << delimiter << cols[j].toString();
				stream << endl;
			}
		}

		file.close();
		return true;
	}

	return false;
}
bool XrayTableModel::writeCSV(const QString& filename, const std::vector<std::string>& header, const std::vector<std::vector<std::string> >& rows, QChar delimiter)
{
	QFile file(filename);
	if (file.open(QIODevice::WriteOnly))
	{
		QTextStream stream(&file);

		if (!header.empty())
		{
			stream << header[0].c_str();
			for (auto i = 1; i < header.size(); i++)
				stream << delimiter << header[i].c_str();
			stream << endl;
		}

		for (const auto& i : rows)
		{
			const auto& cols = i;
			if (!cols.empty())
			{
				stream << cols[0].c_str();
				for (auto j = 1; j < cols.size(); j++)
					stream << delimiter << cols[j].c_str();
				stream << endl;
			}
		}

		file.close();
		return true;
	}

	return false;
}

QStringList XrayTableModel::writeToString(QChar delimiter)
{
	QStringList list;

	if (!m_horizontalHeaderData.empty())
	{
		QString s;
		s += m_horizontalHeaderData[0];
		for (auto i = 1; i < m_horizontalHeaderData.size(); i++)
			s += (delimiter + m_horizontalHeaderData[i]);
		list.append(s);
	}

	for (const auto& i : m_rows)
	{
		const auto& cols = i;
		if (!cols.empty())
		{
			QString s;
			s += cols[0].toString();
			for (auto j = 1; j < cols.size(); j++)
				s += (delimiter + cols[j].toString());
			list.append(s);
		}
	}

	return list;
}
