/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/04
** filename: 	XrayFindFilesWidget.cpp
** file base:	XrayFindFilesWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a widget for searching files that
*				contain the given text.
****************************************************************************/

#include "XrayFindFilesWidget.h"

#include <QtWidgets>
#include "XrayGlobal.h"

XRAYLAB_USING_NAMESPACE

enum { absoluteFileNameRole = Qt::UserRole + 1 };

static inline void openFile(const QString &fileName)
{
	QDesktopServices::openUrl(QUrl::fromLocalFile(fileName));
}

XrayFindFilesWidget::XrayFindFilesWidget(QWidget *parent) : 
	QWidget(parent)
{
	setWindowTitle("Find Existing Files");

	browseBtn = new QPushButton(QApplication::translate("XrayFindFilesWidget", "&Browse..."), this);
	//browseBtn->setStyleSheet("QPushButton { Padding: 1px; height: 26px; width: 120px; Border-radius: 0px; Background: rgb(90, 90, 90); Color: #fefefe; } QPushButton:pressed { background-color: rgba(100, 100, 100, 50); }QPushButton:hover { background-color: rgba(100, 100, 100, 100); border: 1px solid rgba(170, 170, 0, 255); } QPushButton:focus { border: 1px solid rgba(170, 170, 0, 255); font-weight: bold; outline: none; }");
	connect(browseBtn, &QAbstractButton::clicked, this, &XrayFindFilesWidget::browse);
	
	findButton = new QPushButton(QApplication::translate("XrayFindFilesWidget", "&Find"), this);
	//findButton->setStyleSheet("QPushButton { Padding: 1px; height: 26px; width: 120px; Border-radius: 0px; Background: rgb(90, 90, 90); Color: #fefefe; } QPushButton:pressed { background-color: rgba(100, 100, 100, 50); }QPushButton:hover { background-color: rgba(100, 100, 100, 100); border: 1px solid rgba(170, 170, 0, 255); } QPushButton:focus { border: 1px solid rgba(170, 170, 0, 255); font-weight: bold; outline: none; }");
	connect(findButton, &QAbstractButton::clicked, this, &XrayFindFilesWidget::find);

	fileComboBox = createComboBox({ { tr("*.ini") } });
	fileComboBox->setCurrentText(QApplication::translate("XrayFindFilesWidget", "*.ini"));
	connect(fileComboBox->lineEdit(), &QLineEdit::returnPressed, this, &XrayFindFilesWidget::animateFindClick);
	
	textComboBox = createComboBox({ { tr("2D") }, { tr("3D") }, { tr("Porosity") } });
	textComboBox->setCurrentText(QApplication::translate("XrayFindFilesWidget", "2D"));
	connect(textComboBox->lineEdit(), &QLineEdit::returnPressed, this, &XrayFindFilesWidget::animateFindClick);

	directoryComboBox = createComboBox({ QDir::toNativeSeparators(QDir::currentPath()) });
	directoryComboBox->setCurrentText({ QDir::toNativeSeparators(QDir::currentPath()) });
	connect(directoryComboBox->lineEdit(), &QLineEdit::returnPressed, this, &XrayFindFilesWidget::animateFindClick);

	filesFoundLabel = new QLabel(this);

	createFilesTable();

	fileTypeLabel = new QLabel(QApplication::translate("XrayFindFilesWidget", "File Type:"));
	tagLabel = new QLabel(QApplication::translate("XrayFindFilesWidget", "Tag:"));
	lookInLabel = new QLabel(QApplication::translate("XrayFindFilesWidget", "Look In:"));

	auto mainLayout = new QGridLayout(this);
	mainLayout->addWidget(fileTypeLabel, 0, 0);
	mainLayout->addWidget(fileComboBox, 0, 1, 1, 2);
	mainLayout->addWidget(tagLabel, 1, 0);
	mainLayout->addWidget(textComboBox, 1, 1, 1, 2);
	mainLayout->addWidget(lookInLabel, 2, 0);
	mainLayout->addWidget(directoryComboBox, 2, 1);
	mainLayout->addWidget(browseBtn, 2, 2);
	mainLayout->addWidget(filesTable, 3, 0, 1, 3);
	mainLayout->addWidget(filesFoundLabel, 4, 0, 1, 2);
	mainLayout->addWidget(findButton, 4, 2);
}

XrayFindFilesWidget::~XrayFindFilesWidget()
{
}
void XrayFindFilesWidget::writeSettings(QSettings& _settings)
{
	_settings.beginGroup("XFindFiles");

	_settings.setValue("FileTypes", getFileTypes());
	_settings.setValue("Tags", getTexts());
	_settings.setValue("Directories", getDirectories());

	_settings.setValue("ActiveFileType", fileComboBox->currentText());
	_settings.setValue("ActiveTag", textComboBox->currentText());
	_settings.setValue("ActiveDirectory", directoryComboBox->currentText());

	_settings.endGroup();
}
void XrayFindFilesWidget::readSettings(QSettings& _settings)
{
	_settings.beginGroup("XFindFiles");

	auto variant = _settings.value("FileTypes", getFileTypes()); setFileTypes(variant.toStringList());
	variant = _settings.value("Tags", getTexts()); setTexts(variant.toStringList());
	variant = _settings.value("Directories", getDirectories()); setDirectories(variant.toStringList());
	
	variant = _settings.value("ActiveFileType", fileComboBox->currentText()); if(variant.isValid()) fileComboBox->setCurrentText(variant.toString());
	variant = _settings.value("ActiveTag", textComboBox->currentText()); if (variant.isValid()) textComboBox->setCurrentText(variant.toString());
	variant = _settings.value("ActiveDirectory", directoryComboBox->currentText()); if (variant.isValid()) directoryComboBox->setCurrentText(variant.toString());
	
	_settings.endGroup();
}

void XrayFindFilesWidget::setFileTypes(const QStringList& _fileTypes)
{
	fileComboBox->clear();
	fileComboBox->addItems(_fileTypes);
}
QStringList XrayFindFilesWidget::getFileTypes() const
{
	QStringList t;
	for (auto i = 0; i < fileComboBox->count(); i++)
		t.append(fileComboBox->itemText(i));
	return t;
}

void XrayFindFilesWidget::setTexts(const QStringList& _texts)
{
	textComboBox->clear();
	textComboBox->addItems(_texts);
}
QStringList XrayFindFilesWidget::getTexts() const
{
	QStringList t;
	for (auto i = 0; i < textComboBox->count(); i++)
		t.append(textComboBox->itemText(i));
	return t;
}

void XrayFindFilesWidget::setDirectories(const QStringList& _dir)
{
	directoryComboBox->clear();
	directoryComboBox->addItems(_dir);
}
QStringList XrayFindFilesWidget::getDirectories() const
{
	QStringList t;
	for (auto i = 0; i < directoryComboBox->count(); i++)
		t.append(directoryComboBox->itemText(i));
	return t;
}

QTableWidget* XrayFindFilesWidget::getTableWidget() const
{
	return filesTable;
}
QString XrayFindFilesWidget::getFilename(const QTableWidgetItem *item)
{
	return item->data(absoluteFileNameRole).toString();
}

void XrayFindFilesWidget::browse()
{
	auto directory = QDir::toNativeSeparators(QFileDialog::getExistingDirectory(this, QApplication::translate("XrayFindFilesWidget", "Find Files"), XrayGlobal::getLastFileOpenPath()));
	if (!directory.isEmpty())
	{
		if (directoryComboBox->findText(directory) == -1)
			directoryComboBox->addItem(directory);
		directoryComboBox->setCurrentIndex(directoryComboBox->findText(directory));

		XrayGlobal::setLastFileOpenPath(directory);
	}
}

static void updateComboBox(QComboBox *comboBox)
{
	if (comboBox->findText(comboBox->currentText()) == -1)
		comboBox->addItem(comboBox->currentText());
}

void XrayFindFilesWidget::find()
{
	filesTable->setRowCount(0);

	auto fileName = fileComboBox->currentText();
	auto text = textComboBox->currentText();
	auto path = QDir::cleanPath(directoryComboBox->currentText());
	currentDir = QDir(path);

	updateComboBox(fileComboBox);
	updateComboBox(textComboBox);
	updateComboBox(directoryComboBox);

	QStringList filter;
	if (!fileName.isEmpty())
		filter << fileName;
	QDirIterator it(path, filter, QDir::AllEntries | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);
	QStringList files;
	while (it.hasNext())
		files << it.next();
	if (!text.isEmpty())
		files = findFiles(files, text);
	files.sort();
	showFiles(files);
}

void XrayFindFilesWidget::animateFindClick()
{
	findButton->animateClick();
}

QStringList XrayFindFilesWidget::findFiles(const QStringList &files, const QString &text)
{
	QProgressDialog progressDialog(this);
	progressDialog.setCancelButtonText(QApplication::translate("XrayFindFilesWidget", "&Cancel"));
	progressDialog.setRange(0, files.size());
	progressDialog.setWindowTitle(QApplication::translate("XrayFindFilesWidget", "Find Files"));

	QMimeDatabase mimeDatabase;
	QStringList foundFiles;

	for (auto i = 0; i < files.size(); ++i)
	{
		progressDialog.setValue(i);
		progressDialog.setLabelText(QApplication::translate("XrayFindFilesWidget", "Searching file number %1 of %n...").arg(files.size()).arg(i));
		QCoreApplication::processEvents();

		if (progressDialog.wasCanceled())
			break;

		const auto fileName = files.at(i);
		const auto mimeType = mimeDatabase.mimeTypeForFile(fileName);
		if (mimeType.isValid() && !mimeType.inherits(QStringLiteral("text/plain")))	// do not search for binary files.
			continue;

		QFile file(fileName);
		if (file.open(QIODevice::ReadOnly))
		{
			QString line;
			QTextStream in(&file);
			while (!in.atEnd())
			{
				if (progressDialog.wasCanceled())
					break;
				line = in.readLine();
				if (line.contains(text, Qt::CaseInsensitive))
				{
					foundFiles << files[i];
					break;
				}
			}
		}
	}
	return foundFiles;
}

void XrayFindFilesWidget::showFiles(const QStringList &paths)
{
	for (const auto& filePath : paths)
	{
		const auto toolTip = QDir::toNativeSeparators(filePath);
		const auto relativePath = QDir::toNativeSeparators(currentDir.relativeFilePath(filePath));

		const auto size = QFileInfo(filePath).size();
		auto fileNameItem = new QTableWidgetItem(relativePath);
		fileNameItem->setData(absoluteFileNameRole, QVariant(filePath));
		fileNameItem->setToolTip(toolTip);
		fileNameItem->setFlags(fileNameItem->flags() ^ Qt::ItemIsEditable);

		auto sizeItem = new QTableWidgetItem(QLocale().formattedDataSize(size));
		sizeItem->setData(absoluteFileNameRole, QVariant(filePath));
		sizeItem->setToolTip(toolTip);
		sizeItem->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
		sizeItem->setFlags(sizeItem->flags() ^ Qt::ItemIsEditable);

		auto row = filesTable->rowCount();
		filesTable->insertRow(row);
		filesTable->setItem(row, 0, fileNameItem);
		filesTable->setItem(row, 1, sizeItem);
	}

	if (paths.empty())
		setMessageText(QApplication::translate("XrayFindFilesWidget", "Couldn't find any file with this tag!"));
	else
		setMessageText(QString::number(paths.size()) + " " + QApplication::translate("XrayFindFilesWidget", "file(s) found! (Double click on a file to restore it)"));
}
void XrayFindFilesWidget::setMessageText(const QString& _text)
{
	filesFoundLabel->setText(_text);
	filesFoundLabel->setWordWrap(true);
}

QComboBox* XrayFindFilesWidget::createComboBox(const QStringList& _items)
{
	auto comboBox = new QComboBox(this);
	comboBox->setEditable(true);
	comboBox->addItems(_items);
	comboBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	return comboBox;
}

void XrayFindFilesWidget::createFilesTable()
{
	filesTable = new QTableWidget(0, 2);
	filesTable->setSelectionBehavior(QAbstractItemView::SelectRows);

	QStringList labels;
	labels << QApplication::translate("XrayFindFilesWidget", "File Name") << QApplication::translate("XrayFindFilesWidget", "Size");
	filesTable->setHorizontalHeaderLabels(labels);
	filesTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
	filesTable->verticalHeader()->hide();
	filesTable->setShowGrid(false);
	filesTable->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(filesTable, &QTableWidget::customContextMenuRequested, this, &XrayFindFilesWidget::contextMenu);
	connect(filesTable, &QTableWidget::cellActivated, [this](int row, int column) 
	{ 
		emit tableCellActivated(row, column);
		emit firstColumnActivated(getFilename(filesTable->item(row, 0)));
	});
}

void XrayFindFilesWidget::openFileOfItem(int row, int /* column */)
{
	const auto item = filesTable->item(row, 0);
	openFile(getFilename(item));
}

void XrayFindFilesWidget::contextMenu(const QPoint &pos)
{
	const auto item = filesTable->itemAt(pos);
	if (!item)
		return;
	QMenu menu;
#ifndef QT_NO_CLIPBOARD
	copyNameAction = menu.addAction(QIcon(":/paint/images/copy_blue_icon.png"), tr("Copy Name"));
	copyNameAction->setText(QApplication::translate("XrayFindFilesWidget", "Copy Name"));
#endif
	openAction = menu.addAction(QIcon(":/res/images/document_blue_icon.png"), tr("Open"));
	openAction->setText(QApplication::translate("XrayFindFilesWidget", "Open File"));
	auto action = menu.exec(filesTable->mapToGlobal(pos));
	if (!action)
		return;
	const auto fileName = getFilename(item);
	if (action == openAction)
		openFile(fileName);
#ifndef QT_NO_CLIPBOARD
	else if (action == copyNameAction)
		QGuiApplication::clipboard()->setText(QDir::toNativeSeparators(fileName));
	#endif
}