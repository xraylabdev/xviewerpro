/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayPixmapStatusWidget.cpp
** file base:	XrayPixmapStatusWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a timer widget that changes the pixmaps
				for a slideshow.
****************************************************************************/

#include "XrayPixmapStatusWidget.h"

XRAYLAB_USING_NAMESPACE

XrayPixmapStatusWidget::XrayPixmapStatusWidget(QWidget* parent, Role role) :
	QWidget(parent),
	p_layout(new QGridLayout(this)),
	p_labelIcon(new QLabel(this)),
	p_timer(new QTimer(this)),
	m_currentRole(role),
	m_currentFrame(0)
{
	setLayout(p_layout);

	p_layout->setContentsMargins(0, 0, 0, 0);
	p_layout->addWidget(p_labelIcon, 0, 0);

	m_frames[Okay] = QList<QPixmap>();
	m_frames[Error] = QList<QPixmap>();
	m_frames[Busy] = QList<QPixmap>();

	m_frameRates[Okay] = 0.0;
	m_frameRates[Error] = 0.0;
	m_frameRates[Busy] = 0.0;

	connect(p_timer, &QTimer::timeout, this, &XrayPixmapStatusWidget::timerTimeout);
}

XrayPixmapStatusWidget::~XrayPixmapStatusWidget() 
{
}

void XrayPixmapStatusWidget::setIcon(Role role, const QPixmap& icon)
{
	setFrames(role, icon, 1, 0.0);
}

const QPixmap& XrayPixmapStatusWidget::getIcon(Role role) const
{
	if (m_frames[role].isEmpty())
	{
		static QPixmap icon;
		return icon;
	}
	else
		return m_frames[role].front();
}

void XrayPixmapStatusWidget::setFrames(Role role, const QPixmap& frames, int numFrames, double frameRate)
{
	QList<QPixmap> frameList;
	auto frameHeight = frames.height() / numFrames;

	for (auto i = 0; i < numFrames; ++i)
	{
		auto frame = frames.copy(0, i * frameHeight, frames.width(), frameHeight);
		frameList.append(frame);
	}

	setFrames(role, frameList, frameRate);
}

void XrayPixmapStatusWidget::setFrames(Role role, const QList<QPixmap>& frameList, double frameRate) 
{
	bool wasStarted = false;

	if (role == m_currentRole)
	{
		wasStarted = true;
		stop();
	}

	m_frames[role] = frameList;
	m_frameRates[role] = frameRate;

	if (wasStarted)
		start();
}

const QList<QPixmap>& XrayPixmapStatusWidget::getFrames(Role role) const 
{
	QMap<Role, QList<QPixmap> >::const_iterator it = m_frames.find(role);

	if (it == m_frames.end())
	{
		static QList<QPixmap> frames;
		return frames;
	}
	else
		return it.value();
}

void XrayPixmapStatusWidget::setFrameRate(Role role, double frameRate)
{
	if (frameRate != m_frameRates[role])
	{
		m_frameRates[role] = frameRate;

		if ((role == m_currentRole) && p_timer->isActive()) 
		{
			if (frameRate > 0.0)
				p_timer->setInterval(1.0 / frameRate * 1e3);
			else
				p_timer->stop();
		}
	}
}

double XrayPixmapStatusWidget::getFrameRate(Role role) const 
{
	return m_frameRates[role];
}

void XrayPixmapStatusWidget::setCurrentRole(Role role, const QString& toolTip) 
{
	if (role != m_currentRole)
	{
		stop();

		m_currentRole = role;
		setToolTip(toolTip);

		start();

		emit currentRoleChanged(role);
	}
	else
		setToolTip(toolTip);
}

XrayPixmapStatusWidget::Role XrayPixmapStatusWidget::getCurrentRole() const
{
	return m_currentRole;
}

void XrayPixmapStatusWidget::pushCurrentRole()
{
	m_roleStack.append(m_currentRole);
	m_toolTipStack.append(toolTip());
}

bool XrayPixmapStatusWidget::popCurrentRole()
{
	if (!m_roleStack.isEmpty()) 
	{
		setCurrentRole(m_roleStack.last(), m_toolTipStack.last());

		m_roleStack.removeLast();
		m_toolTipStack.removeLast();

		return true;
	}
	else
		return false;
}

void XrayPixmapStatusWidget::start() 
{
	if (p_timer->isActive())
		p_timer->stop();

	m_currentFrame = 0;

	if (!m_frames[m_currentRole].isEmpty()) 
	{
		p_labelIcon->setPixmap(m_frames[m_currentRole].front());

		if (m_frameRates[m_currentRole] > 0.0)
			p_timer->start(1.0 / m_frameRates[m_currentRole] * 1e3);
	}
}

void XrayPixmapStatusWidget::step()
{
	++m_currentFrame;

	if (m_currentFrame >= m_frames[m_currentRole].length())
		m_currentFrame = 0;

	if (!m_frames[m_currentRole].isEmpty())
		p_labelIcon->setPixmap(m_frames[m_currentRole].at(m_currentFrame));
}

void XrayPixmapStatusWidget::stop() 
{
	if (p_timer->isActive())
		p_timer->stop();
}

void XrayPixmapStatusWidget::timerTimeout() 
{
	step();
}