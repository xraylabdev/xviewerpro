/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/08
** filename: 	XrayHistogramViewWidget.cpp
** file base:	XrayHistogramViewWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a double value slider.
****************************************************************************/

#include "XrayHistogramViewWidget.h"

#include <QPainter>
#include <QPaintEvent>

XRAYLAB_USING_NAMESPACE

XrayHistogramViewWidget::XrayHistogramViewWidget(QWidget* _parent) : 
	QWidget(_parent),
	m_color1(QColor(0, 0, 0)),
	m_color2(QColor(220, 220, 220)),
	m_penColor(QColor(10, 147, 205)),
	m_drawBorder(true)
{
	setAttribute(Qt::WA_NoBackground);

	setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	setMinimumWidth(50);
	setMinimumHeight(50);
}

void XrayHistogramViewWidget::paintEvent(QPaintEvent* event)
{
	// generate shade, if necessary
	if (m_shade.isNull() || m_shade.size() != size())
		generateShade();

	// draw shade
	QPainter p(this);
	p.drawImage(0, 0, m_shade);
	p.setPen(m_penColor);
	if(m_drawBorder)
		p.drawRect(0, 0, width() - 1, height() - 1);

	// draw histogram
	if (m_histogram.size() > 0)
	{
		qreal x_range = m_histogram.size();

		QVector<QPointF> listOfPoints;
		listOfPoints.reserve(x_range * 2);

		for (int i = 0; i < x_range; ++i)
		{
			QPointF point(i * (width() - 1) / x_range, (height() - 1) - m_histogram[i] * (height() - 1));
			QPointF point2(i * (width() - 1) / x_range, (height() - 1));
			listOfPoints.push_back(point2);
			listOfPoints.push_back(point);
		}
		p.drawLines(listOfPoints);
	}
}

void XrayHistogramViewWidget::generateShade()
{
	m_shade = QImage(size(), QImage::Format_RGB32);
	QLinearGradient shade(0, 0, 0, height());
	
	shade.setColorAt(1, m_color1);
	shade.setColorAt(0, m_color2);

	QPainter p(&m_shade);
	p.fillRect(rect(), shade);
}

void XrayHistogramViewWidget::setHistogram(const QVector<qreal>& _histogram)
{
	m_histogram = _histogram;
}

void XrayHistogramViewWidget::setColor1(const QColor& _color)
{
	m_color1 = _color;
	generateShade();
	update();
}
void XrayHistogramViewWidget::setColor2(const QColor& _color)
{
	m_color2 = _color;
	generateShade();
	update();
}
void XrayHistogramViewWidget::setColor(const QColor& _color1, const QColor& _color2)
{
	m_color1 = _color1;
	m_color2 = _color2;
	generateShade();
	update();
}
void XrayHistogramViewWidget::setLineColor(const QColor& _color)
{
	m_penColor = _color;
	update();
}
void XrayHistogramViewWidget::setBorderEnabled(bool b)
{
	m_drawBorder = b;
}
bool XrayHistogramViewWidget::isBorderEnabled() const
{
	return m_drawBorder;
}

void XrayHistogramViewWidget::clear(const QColor& _color)
{
	m_histogram.clear();

	m_shade = QImage(size(), QImage::Format_RGB32);
	QLinearGradient shade(0, 0, 0, height());
	
	shade.setColorAt(1, _color);
	shade.setColorAt(0, _color);

	QPainter p(&m_shade);
	p.fillRect(rect(), shade);
	update();
}
