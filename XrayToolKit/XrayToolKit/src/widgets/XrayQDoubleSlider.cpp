/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/08
** filename: 	XrayQDoubleSlider.cpp
** file base:	XrayQDoubleSlider
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a double value slider.
****************************************************************************/

#include "XrayQDoubleSlider.h"

XRAYLAB_USING_NAMESPACE

XrayQDoubleSlider::XrayQDoubleSlider(QWidget *parent) :
	QSlider(parent)
{
	m_DoubleMin = 0.0;
	m_DoubleMax = 1.0;
	m_DoubleStep = 0.01;
	updateRange();
}

void XrayQDoubleSlider::updateRange()
{
	auto mymax = static_cast<int>(std::ceil((m_DoubleMax - m_DoubleMin) / m_DoubleStep));
	this->setMinimum(0);
	this->setMaximum(mymax);
	this->setSingleStep(1);

	this->setDoubleValue(m_DoubleValue);
}

void XrayQDoubleSlider::setDoubleValue(double _value)
{
	m_DoubleValue = _value;
	auto t = (m_DoubleValue - m_DoubleMin) / (m_DoubleMax - m_DoubleMin);
	t = std::max(0.0, std::min(1.0, t));
	auto p = static_cast<int>(0.5 + this->maximum() * t);
	if (this->value() != p)
	{
		this->setValue(p);
	}
	m_CorrespondingIntValue = p;
}

double XrayQDoubleSlider::doubleValue()
{
	if (this->value() != m_CorrespondingIntValue)
	{
		auto t = this->value() * 1.0 / static_cast<double>(this->maximum());
		m_DoubleValue = m_DoubleMin + t * (m_DoubleMax - m_DoubleMin);
		m_CorrespondingIntValue = this->value();
	}

	return m_DoubleValue;
}

