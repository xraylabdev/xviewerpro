/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/18
** filename: 	XrayQTabWidget.cpp
** file base:	XrayQTabWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a tab widget with actions and animation.
****************************************************************************/

#include "XrayQTabWidget.h"

#include <QContextMenuEvent>
#include <QApplication>
#include <QTabBar>
#include <QAction>
#include <QMovie>
#include <QMenu>

typedef QList<QAction*> Actions;

XRAYLAB_USING_NAMESPACE

XrayQTabWidget::XrayQTabWidget(QWidget* parent) : 
	QTabWidget(parent),
	m_always(true), 
	m_policy(Qt::DefaultContextMenu)
{
}

XrayQTabWidget::~XrayQTabWidget()
{
}

void XrayQTabWidget::setAlwaysShowTabBar(bool _always)
{
	m_always = _always;
	tabBar()->setVisible(m_always || count() > 1);
}
bool XrayQTabWidget::alwaysShowTabBar() const
{
	return m_always;
}

void XrayQTabWidget::setTabContextMenuPolicy(Qt::ContextMenuPolicy _policy)
{
	m_policy = _policy;
}
Qt::ContextMenuPolicy XrayQTabWidget::tabContextMenuPolicy() const
{
	return m_policy;
}

void XrayQTabWidget::addTabAction(int index, QAction* action)
{
	insertTabAction(index, 0, action);
}
QAction* XrayQTabWidget::addTabAction(int index, const QString& text)
{
	return addTabAction(index, QIcon(), text, 0, 0);
}
QAction* XrayQTabWidget::addTabAction(int index, const QIcon& icon, const QString& text)
{
	return addTabAction(index, icon, text, 0, 0);
}

QAction* XrayQTabWidget::addTabAction(int index, const QString& text, const QObject* receiver, const char* member, const QKeySequence& shortcut)
{
	return addTabAction(index, QIcon(), text, receiver, member, shortcut);
}
QAction* XrayQTabWidget::addTabAction(int index, const QIcon& icon, const QString& text, const QObject* receiver, const char* member, const QKeySequence& shortcut)
{
	auto action = new QAction(icon, text, this);
	addTabAction(index, action);
	if (receiver && member)
		connect(action, SIGNAL(triggered()), receiver, member);
	action->setShortcut(shortcut);
	return action;
}

void XrayQTabWidget::addTabActions(int index, QList<QAction*> actions)
{
	foreach(QAction* action, actions)
	{
		insertTabAction(index, 0, action);
	}
}

void XrayQTabWidget::clearTabActions(int index)
{
	Q_ASSERT(index >= 0 && index < m_actions.count());

	while (m_actions[index].count())
	{
		auto action = m_actions[index].last();
		removeTabAction(index, action);
		if (action->parent() == this)
			delete action;
	}
}

void XrayQTabWidget::insertTabAction(int index, QAction* before, QAction* action)
{
	Q_ASSERT(index >= 0 && index < m_actions.count());

	if (!action)
	{
		qWarning("XrayQTabWidget::insertTabAction: Attempt to insert a null action");
		return;
	}

	const auto& actions = m_actions.at(index);
	if (actions.contains(action))
		removeTabAction(index, action);

	int pos = actions.indexOf(before);
	if (pos < 0)
	{
		before = 0;
		pos = actions.count();
	}
	m_actions[index].insert(pos, action);

	QActionEvent e(QEvent::ActionAdded, action, before);
	QApplication::sendEvent(this, &e);
}

void XrayQTabWidget::insertTabActions(int index, QAction* before, QList<QAction*> actions)
{
	foreach(QAction* action, actions)
	{
		insertTabAction(index, before, action);
	}
}

void XrayQTabWidget::removeTabAction(int index, QAction* action)
{
	Q_ASSERT(index >= 0 && index < m_actions.count());

	if (!action)
	{
		qWarning("XrayQTabWidget::removeTabAction: Attempt to remove a null action");
		return;
	}

	if (m_actions[index].removeAll(action))
	{
		QActionEvent e(QEvent::ActionRemoved, action);
		QApplication::sendEvent(this, &e);
	}
}

QList<QAction*> XrayQTabWidget::tabActions(int index) const
{
	Q_ASSERT(index >= 0 && index < m_actions.count());
	return m_actions.at(index);
}

QMovie* XrayQTabWidget::tabAnimation(int index) const
{
	Q_ASSERT(index >= 0 && index < m_animations.count());
	return m_animations.at(index);
}
void XrayQTabWidget::setTabAnimation(int index, QMovie* animation, bool start)
{
	Q_ASSERT(index >= 0 && index < m_animations.count());
	delete takeTabAnimation(index);
	m_animations[index] = animation;
	if (animation)
	{
		connect(animation, &QMovie::frameChanged, this, &XrayQTabWidget::setMovieFrame);
		if (start)
			animation->start();
	}
}
void XrayQTabWidget::setTabAnimation(int index, const QString& fileName, bool start)
{
	setTabAnimation(index, new QMovie(fileName, QByteArray(), this), start);
}
QMovie* XrayQTabWidget::takeTabAnimation(int index)
{
	Q_ASSERT(index >= 0 && index < m_animations.count());
	auto animation = m_animations.at(index);
	m_animations[index] = 0;
	return animation;
}

void XrayQTabWidget::tabInserted(int index)
{
	Q_ASSERT(index >= 0);
	Q_ASSERT(index <= m_actions.count());
	Q_ASSERT(index <= m_animations.count());
	m_actions.insert(index, Actions());
	m_animations.insert(index, 0);
	tabBar()->setVisible(m_always || count() > 1);
}
void XrayQTabWidget::tabRemoved(int index)
{
	Q_ASSERT(index >= 0);
	Q_ASSERT(index < m_actions.count());
	Q_ASSERT(index < m_animations.count());
	m_actions.removeAt(index);
	m_animations.removeAt(index);
	tabBar()->setVisible(m_always || count() > 1);
}

void XrayQTabWidget::contextMenuEvent(QContextMenuEvent* event)
{
	const auto& pos = event->pos();
	if (!tabBar()->geometry().contains(pos))
		return QTabWidget::contextMenuEvent(event);

	const auto index = tabIndexAt(event->pos());
	switch (m_policy)
	{
	case Qt::NoContextMenu:
		event->ignore();
		break;

	case Qt::PreventContextMenu:
		event->accept();
		break;

	case Qt::ActionsContextMenu:
		if (index != -1 && m_actions.at(index).count())
		{
			QMenu::exec(m_actions.at(index), event->globalPos());
		}
		break;

	case Qt::CustomContextMenu:
		if (index != -1)
		{
			emit tabContextMenuRequested(index, event->globalPos());
		}
		break;

	case Qt::DefaultContextMenu:
	default:
		if (index != -1)
		{
			tabContextMenuEvent(index, event);
		}
		break;
	}
}

void XrayQTabWidget::tabContextMenuEvent(int index, QContextMenuEvent* event)
{
	Q_UNUSED(index);
	event->ignore();
}

int XrayQTabWidget::tabIndexAt(const QPoint& pos) const
{
	const auto n = count();
	const auto tb = tabBar();
	for (auto i = 0; i < n; ++i)
		if (tb->tabRect(i).contains(pos))
			return i;
	return -1;
}

void XrayQTabWidget::setMovieFrame(int frame)
{
	Q_UNUSED(frame);
	auto movie = static_cast<QMovie*>(sender());
	if (movie)
	{
		auto index = m_animations.indexOf(movie);
		if (index != -1)
			setTabIcon(index, movie->currentPixmap());
	}
}
