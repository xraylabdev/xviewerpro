/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/08
** filename: 	XrayQDoubleSliderEditor.cpp
** file base:	XrayQDoubleSliderEditor
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a double value slider with an editor.
****************************************************************************/

#include "XrayQDoubleSliderEditor.h"
#include "ui_XrayQDoubleSliderEditor.h"

XRAYLAB_USING_NAMESPACE

XrayQDoubleSliderEditor::XrayQDoubleSliderEditor(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::XrayQDoubleSliderEditor)
{
	ui->setupUi(this);

	ui->slider->setMinimum(0);
	ui->slider->setMaximum(1000000);
	ui->slider->setSingleStep(0);

	// When the value in the slider changes, we want to update the spinbox
	connect(ui->slider, &QSlider::valueChanged, this, &XrayQDoubleSliderEditor::sliderValueChanged);
	connect(ui->spinbox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, &XrayQDoubleSliderEditor::spinnerValueChanged);

	m_IgnoreSliderEvent = false;
	m_IgnoreSpinnerEvent = false;
	m_ForceDiscreteSteps = true;
}

XrayQDoubleSliderEditor::~XrayQDoubleSliderEditor()
{
	delete ui;
}

QDoubleSpinBox* XrayQDoubleSliderEditor::spinBox() const
{
	return ui->spinbox;
}

void XrayQDoubleSliderEditor::setValue(double newval)
{
	// set the value in the spin box
	if (newval != ui->spinbox->value())
	{
		m_IgnoreSpinnerEvent = true;
		ui->spinbox->setValue(newval);
		ui->spinbox->setSpecialValueText("");
		m_IgnoreSpinnerEvent = false;
		this->updateSliderFromSpinner();
	}
}

double XrayQDoubleSliderEditor::value()
{
	return ui->spinbox->value();
}

void XrayQDoubleSliderEditor::updateSliderFromSpinner()
{
	// Set the value of the slider proportionally to the range of the spinner
	auto a = ui->spinbox->minimum();
	auto b = ui->spinbox->maximum();
	auto v = ui->spinbox->value();

	auto r = (v - a) / (b - a);
	auto vs = static_cast<int>(ui->slider->maximum() * r);

	m_IgnoreSliderEvent = true;
	if (vs != ui->slider->value())
		ui->slider->setValue(vs);
	m_IgnoreSliderEvent = false;
}

double XrayQDoubleSliderEditor::minimum()
{
	return ui->spinbox->minimum();
}

double XrayQDoubleSliderEditor::maximum()
{
	return ui->spinbox->maximum();
}

double XrayQDoubleSliderEditor::singleStep()
{
	return ui->spinbox->singleStep();
}
int XrayQDoubleSliderEditor::decimals()
{
	return ui->spinbox->decimals();
}

void XrayQDoubleSliderEditor::setMinimum(double x)
{
	ui->spinbox->setMinimum(x);
	this->updateSliderFromSpinner();
}

void XrayQDoubleSliderEditor::setMaximum(double x)
{
	ui->spinbox->setMaximum(x);
	this->updateSliderFromSpinner();
}

void XrayQDoubleSliderEditor::setSingleStep(double x)
{
	ui->spinbox->setSingleStep(x);
}
void XrayQDoubleSliderEditor::setDecimals(int x)
{
	ui->spinbox->setDecimals(x);
}

void XrayQDoubleSliderEditor::setForceDiscreteSteps(bool useDiscreteSteps)
{
	m_ForceDiscreteSteps = useDiscreteSteps;
}

void XrayQDoubleSliderEditor::sliderValueChanged(int valslider)
{
	if (!m_IgnoreSliderEvent)
	{
		// We need to map the integer value into the closest acceptable by the spinbox
		auto r = valslider * 1.0 / static_cast<double>(ui->slider->maximum());
		auto a = ui->spinbox->minimum();
		auto b = ui->spinbox->maximum();
		auto v = 0.0;

		// If necessary, round the value using singleStep
		if (m_ForceDiscreteSteps)
		{
			auto step = ui->spinbox->singleStep();
			auto t = 0.5 * step + (b - a) * r;
			v = a + (t - std::fmod(t, step));
		}
		else
		{
			v = a + (b - a) * r;
		}

		// Set the value in the spinner and slider
		this->setValue(v);

		// Invoke the event
		emit valueChanged(ui->spinbox->value());
	}
}

void XrayQDoubleSliderEditor::spinnerValueChanged(double value)
{
	// This is very simple, we just stick the value into the slider
	if (!m_IgnoreSpinnerEvent)
	{
		this->updateSliderFromSpinner();
		emit valueChanged(value);
	}
}

void XrayQDoubleSliderEditor::stepUp()
{
	ui->spinbox->stepUp();
}

void XrayQDoubleSliderEditor::stepDown()
{
	ui->spinbox->stepDown();
}

void XrayQDoubleSliderEditor::setValueToNull()
{
	// First, set the value to minimum
	m_IgnoreSliderEvent = true;
	m_IgnoreSpinnerEvent = true;
	ui->slider->setValue(ui->slider->minimum());
	ui->spinbox->setValue(ui->spinbox->minimum());
	ui->spinbox->setSpecialValueText(" ");
	m_IgnoreSliderEvent = false;
	m_IgnoreSpinnerEvent = false;
}

void XrayQDoubleSliderEditor::setValues(double _value, double _step, int _decimals, double _min, double _max)
{
	ui->spinbox->setMinimum(_min);
	ui->spinbox->setMaximum(_max);
	ui->spinbox->setSingleStep(_step);
	ui->spinbox->setDecimals(_decimals);
	ui->spinbox->setValue(_value);
	ui->spinbox->setSpecialValueText("");
	this->updateSliderFromSpinner();
}