/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/19
** filename: 	XrayQToleranceBar.cpp
** file base:	XrayQToleranceBar
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a colorized (rainbow) tolerance bar.
****************************************************************************/

#include "XrayQToleranceBar.h"

XRAYLAB_USING_NAMESPACE

XrayQToleranceBar::XrayQToleranceBar(QWidget *parent) :
	QWidget(parent),
	m_value(0),
	m_totalBars(10),
	m_pressed(false),
	m_painterSize(500, 500),
	m_pos(1, 1)
{
}

void XrayQToleranceBar::setPainterSize(const QSize& _size)
{
	m_painterSize = _size;
}
QSize XrayQToleranceBar::painterSize() const
{
	return m_painterSize;
}
void XrayQToleranceBar::setValue(int _value)
{
	m_pos = QPoint(_value, height() - 10);
	if (calcValue(m_pos))
		update();
}
int XrayQToleranceBar::value() const
{
	return m_pos.x();
}
void XrayQToleranceBar::setTotalBars(int _value)
{
	m_totalBars = _value;
	if (calcValue(m_pos))
		update();
}
int XrayQToleranceBar::totalBars() const
{
	return m_totalBars;
}

void XrayQToleranceBar::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.setWindow(0, 0, m_painterSize.width(), m_painterSize.height());
	painter.setRenderHint(QPainter::Antialiasing);
	painter.setBrush(QColor(40, 40, 40));
	painter.setPen(QPen(Qt::black, 5));
	QPolygon poly1;
	poly1 << QPoint(0, m_painterSize.height() - 10) << QPoint((m_painterSize.width() - 10), m_painterSize.height() - 10) << QPoint((m_painterSize.width() - 10), 0);
	painter.drawPolygon(poly1);
	painter.setPen(QPen(Qt::black, 5));

	QLinearGradient linGrad(0, 50, (m_painterSize.width() - 10), 50);
	linGrad.setColorAt(0, Qt::green);
	linGrad.setColorAt(1, Qt::red);
	linGrad.setSpread(QGradient::PadSpread);
	painter.setBrush(linGrad);

	QPolygon poly2;
	poly2 << QPoint(0, m_painterSize.height() - 10) << QPoint((m_painterSize.width() - 10) * m_value / m_totalBars, m_painterSize.height() - 10) << QPoint((m_painterSize.width() - 10) * m_value / m_totalBars, ((m_painterSize.height() - 10)*(m_totalBars - m_value) / m_totalBars));
	painter.drawPolygon(poly2);

	painter.setBrush(QColor(40, 40, 40));
	painter.setPen(QPen(Qt::black, 5));
	QFont font = painter.font();
	font.setPointSize(18);
	painter.setFont(font);

	for (auto i = 1; i < m_totalBars; i++)
	{
		painter.setPen(QPen(Qt::black, 5));
		painter.drawLine((m_painterSize.width() - 10) * i / m_totalBars, (m_painterSize.height() + 1 - 10) * (m_totalBars - i) / m_totalBars, (m_painterSize.width() - 10) * i / m_totalBars, (m_painterSize.height() - 10));
		painter.setPen(QPen(Qt::red, 5));
	}
}

bool XrayQToleranceBar::calcValue(const QPoint& _pos)
{
	auto w = width() / static_cast<float>(m_painterSize.width());
	auto h = height() / static_cast<float>(m_painterSize.height());
	auto x = _pos.x() / w;
	auto y = _pos.y() / h;
	auto p = ((m_painterSize.height() - 10) * ((m_painterSize.width() - 10) - x) / (m_painterSize.width() - 10));
	if (y > p && y < m_painterSize.height() - 10 && x < m_painterSize.width() - 10)
	{
		m_value = ((x * m_totalBars) / (m_painterSize.width() - 10)) + 1;
		return true;
	}
	return false;
}

void XrayQToleranceBar::mousePressEvent(QMouseEvent* _event)
{
	if (_event->button() == Qt::LeftButton)
	{
		if (calcValue(_event->pos()))
		{
			m_pos = _event->pos();
			m_pressed = true;
			update();
		}
	}
}

void XrayQToleranceBar::mouseMoveEvent(QMouseEvent* _event)
{
	if (m_pressed)
	{
		if (calcValue(_event->pos()))
		{
			m_pos = _event->pos();
			update();
		}
	}
}

void XrayQToleranceBar::mouseReleaseEvent(QMouseEvent* _event)
{
	Q_UNUSED(_event);
	m_pressed = false;
}
