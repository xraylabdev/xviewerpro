/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayThreadedTimer.cpp
** file base:	XrayThreadedTimer
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a thread with a timer in it.
****************************************************************************/

#include "XrayThreadedTimer.h"

#include <QApplication>
#include <QTimerEvent>

XRAYLAB_USING_NAMESPACE

XrayThreadedTimer::XrayThreadedTimer(QObject* parent) :
	QThread(parent),
	p_timer(new QTimer()) 
{
	p_timer->setInterval(5000);
	p_timer->moveToThread(this);

	connect(p_timer, &QTimer::timeout, this, &XrayThreadedTimer::timerTimeout);
}

XrayThreadedTimer::~XrayThreadedTimer()
{
	p_timer->stop();

	quit();
	wait();

	delete p_timer;
}

int XrayThreadedTimer::getTimerId() const 
{
	return p_timer->timerId();
}

void XrayThreadedTimer::setInterval(int _interval)
{
	p_timer->setInterval(_interval);
}

int XrayThreadedTimer::getInterval() const
{
	return p_timer->interval();
}

void XrayThreadedTimer::run() 
{
	p_timer->start();

	QThread::exec();
}

void XrayThreadedTimer::timerTimeout()
{
	if (parent())
		QApplication::postEvent(parent(), new QTimerEvent(p_timer->timerId()));
}