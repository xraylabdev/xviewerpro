﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/015
** filename: 	XrayReportWizardPages.cpp
** file base:	XrayReportWizardPages
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		classes that provide wizard pages for the reporting widget.
****************************************************************************/

#include "XrayReportWizardPages.h"
#include "XrayReportWizardWidget.h"
#include "XrayGlobal.h"
#include "XrayTableModel.h"
#include "XrayXmlSettings.h"
#include "XrayQtUtil.h"
#include "XrayQCalendarDialog.h"

#include <QApplication>
#include <QDesktopServices>
#include <QPushButton>
#include <QMessageBox>
#include <QRegExpValidator>
#include <QDir>
#include <QFileDialog>
#include <QDebug>
#include <QDate>
#include <QTableWidget>
#include <QSpacerItem>

XRAYLAB_USING_NAMESPACE

XrayFindFilesWizardPage::XrayFindFilesWizardPage(QWidget* parent) :
	QWizardPage(parent)
{
	setTitle(tr("Find Existing Report Document"));
	setSubTitle(" ");

	p_findFilesGroupBox = new QGroupBox(tr("Find Existing Reports"), this);
	p_findFilesWidget = new XrayFindFilesWidget(this);
	connect(p_findFilesWidget, &XrayFindFilesWidget::firstColumnActivated, this, &XrayFindFilesWizardPage::firstColumnActivated);

	auto layout = new QGridLayout;
	layout->addWidget(p_findFilesWidget);
	p_findFilesGroupBox->setLayout(layout);

	p_restorePreviousGroupBox = new QGroupBox(tr("Restore Previous Report"), this);

	p_docRestoreLabel = new QLabel(tr(""));
	p_docRestore = new QLineEdit(tr(""));
	connect(p_docRestore, &QLineEdit::returnPressed, [this]() { emit lineEditReturnPressed(p_docRestore->text()); });
	p_docRestoreLabel->setBuddy(p_docRestore);
	p_browseBtn = new QPushButton(tr("&Browse..."));
	//p_browseBtn->setStyleSheet("QPushButton { Padding: 1px; height: 26px; width: 120px; Border-radius: 0px; Background: rgb(90, 90, 90); Color: #fefefe; } QPushButton:pressed { background-color: rgba(100, 100, 100, 50); }QPushButton:hover { background-color: rgba(100, 100, 100, 100); border: 1px solid rgba(170, 170, 0, 255); } QPushButton:focus { border: 1px solid rgba(170, 170, 0, 255); font-weight: bold; outline: none; }");
	connect(p_browseBtn, &QPushButton::clicked, this, &XrayFindFilesWizardPage::clickedBrowse);

	layout = new QGridLayout;
	layout->addWidget(p_docRestoreLabel, 0, 0);
	layout->addWidget(p_docRestore, 0, 1);
	layout->addWidget(p_browseBtn, 0, 2);
	p_restorePreviousGroupBox->setLayout(layout);

	layout = new QGridLayout;
	layout->addWidget(p_restorePreviousGroupBox);
	layout->addWidget(p_findFilesGroupBox);

	setLayout(layout);
	retranslate();
}
void XrayFindFilesWizardPage::writeSettings(QSettings& _settings)
{
	_settings.beginGroup("XFindFiles");
	p_docRestore->setText(_settings.value("PreviousReport", p_docRestore->text()).toString());
	_settings.endGroup();
	p_findFilesWidget->writeSettings(_settings);
}
void XrayFindFilesWizardPage::readSettings(QSettings& _settings)
{
	_settings.beginGroup("XFindFiles");
	_settings.setValue("PreviousReport", p_docRestore->text());
	_settings.endGroup();
	p_findFilesWidget->readSettings(_settings);
}
void XrayFindFilesWizardPage::setMessageText(const QString& _text)
{
	p_findFilesWidget->setMessageText(_text);
}
int XrayFindFilesWizardPage::nextId() const
{
	return ReportWizardPage::DocTitle;
}
void XrayFindFilesWizardPage::setRestoreFilePath(const QString& _text)
{
	p_docRestore->setText(_text);
}
QString XrayFindFilesWizardPage::getRestoreFilePath() const
{
	return p_docRestore->text();
}
void XrayFindFilesWizardPage::retranslate()
{
	setTitle(QApplication::translate("XrayFindFilesWizardPage", "Find Existing Report Document"));
	p_findFilesGroupBox->setTitle(QApplication::translate("XrayFindFilesWizardPage", "Find Existing Reports"));
	p_restorePreviousGroupBox->setTitle(QApplication::translate("XrayFindFilesWizardPage", "Restore Previous Report", nullptr));
	p_browseBtn->setText(QApplication::translate("XrayFindFilesWizardPage", "&Browse..."));
}

XrayDocTitleWizardPage::XrayDocTitleWizardPage(QWidget* parent) :
	QWizardPage(parent)
{
	setTitle(tr("Report Document Information"));
	setSubTitle(" ");

	p_groupBox = new QGroupBox(tr("Report Document"), this);

	setupComboBoxes();

	p_docNameLabel = new QLabel(tr("Name:"));
	p_docName = new QLineEdit(tr("Ins_Report_24404"));
	p_docNameLabel->setBuddy(p_docName);

	p_docTitleLabel = new QLabel(tr("Title:"));
	p_docTitle = new QLineEdit(tr("Inspection Report"));
	p_docTitleLabel->setBuddy(p_docTitle);

	p_docTypeLabel = new QLabel(tr("Tag:"));
	p_docType = new QLineEdit(tr("2D"));
	p_docType->setToolTip(tr("Assign related tag(s)/keyword(s) to this report document."));
	p_docTypeLabel->setBuddy(p_docType);

	p_docHeadingFontLabel = new QLabel(tr("Heading Text:"));
	p_docBodyFontLabel = new QLabel(tr("Body Text:"));

	p_textFontSelector = new XrayFontSelectorWidget(p_groupBox);
	p_textFontSelector->setAutoFillBackground(true);
	auto p = p_textFontSelector->palette();
	p.setColor(QPalette::Background, QColor(80, 80, 80, 255));
	//p_textFontSelector->setPalette(p);
	p_headingFontSelector = new XrayFontSelectorWidget(p_groupBox);
	p_headingFontSelector->setAutoFillBackground(true);
	//p_headingFontSelector->setPalette(p);

	auto layout = new QGridLayout;
	layout->addWidget(p_docNameLabel, 0, 0);
	layout->addWidget(p_docName, 0, 1);
	layout->addWidget(p_docTitleLabel, 1, 0);
	layout->addWidget(p_docTitle, 1, 1);
	layout->addWidget(p_docTypeLabel, 2, 0);
	layout->addWidget(p_docType, 2, 1);
	layout->addWidget(new QLabel(), 3, 0);
	layout->addWidget(new QLabel(), 4, 0);
	layout->addWidget(p_docHeadingFontLabel, 5, 0);
	layout->addWidget(p_headingFontSelector, 5, 1);
	layout->addWidget(p_docBodyFontLabel, 6, 0);
	layout->addWidget(p_textFontSelector, 6, 1);

	layout->addWidget(paperSizeComboLabel, 7, 0);
	layout->addWidget(paperSizeCombo, 7, 1);

	layout->addWidget(paperOrientationCombLabel, 8, 0);
	layout->addWidget(paperOrientationCombo, 8, 1);
	p_groupBox->setLayout(layout);

	layout = new QGridLayout;
	layout->addWidget(p_groupBox);

	setLayout(layout);
	retranslate();
}
int XrayDocTitleWizardPage::nextId() const
{
	return ReportWizardPage::DocHeader;
}
void XrayDocTitleWizardPage::retranslate()
{
	setTitle(QApplication::translate("XrayDocTitleWizardPage", "Report Document Information", nullptr));
	p_groupBox->setTitle(QApplication::translate("XrayDocTitleWizardPage", "Report Document", nullptr));
	p_docNameLabel->setText(QApplication::translate("XrayDocTitleWizardPage", "Name:", nullptr));
	p_docTitleLabel->setText(QApplication::translate("XrayDocTitleWizardPage", "Title:", nullptr));
	p_docTypeLabel->setText(QApplication::translate("XrayDocTitleWizardPage", "Tag:", nullptr));
	p_docType->setText(QApplication::translate("XrayDocTitleWizardPage", "2D", nullptr));
	p_docType->setToolTip(QApplication::translate("XrayDocTitleWizardPage", "Assign related tag(s)/keyword(s) to this report document.", nullptr));
	p_docHeadingFontLabel->setText(QApplication::translate("XrayDocTitleWizardPage", "Heading Text:", nullptr));
	p_docBodyFontLabel->setText(QApplication::translate("XrayDocTitleWizardPage", "Body Text:", nullptr));
	paperSizeComboLabel->setText(QApplication::translate("XrayDocTitleWizardPage", "Paper Size:", nullptr));
	paperOrientationCombLabel->setText(QApplication::translate("XrayDocTitleWizardPage", "Paper Orientation:", nullptr));
}

QString XrayDocTitleWizardPage::getGroupBoxTitle() const
{
	return p_groupBox->title();
}
QString XrayDocTitleWizardPage::getDocNameLabel() const
{
	return p_docNameLabel->text();
}
QString XrayDocTitleWizardPage::getDocName() const
{
	return p_docName->text();
}
QString XrayDocTitleWizardPage::getDocTitle() const
{
	return p_docTitle->text();
}
QString XrayDocTitleWizardPage::getDocTitleLabel() const
{
	return p_docTitleLabel->text();
}
void XrayDocTitleWizardPage::setDocType(const QString& _type)
{
	p_docType->setText(_type);
}
QString XrayDocTitleWizardPage::getDocTypeLabel() const
{
	return p_docTypeLabel->text();
}
QString XrayDocTitleWizardPage::getDocType() const
{
	return p_docType->text();
}
XrayFontSelectorWidget* XrayDocTitleWizardPage::getHeadingFontSelector() const
{
	return p_headingFontSelector;
}
XrayFontSelectorWidget* XrayDocTitleWizardPage::getBodyFontSelector() const
{
	return p_textFontSelector;
}

void XrayDocTitleWizardPage::setupComboBoxes()
{
	paperSizeComboLabel = new QLabel(tr("Paper Size:"));
	paperSizeCombo = new QComboBox(this);
	paperSizeCombo->addItem(tr("A0 (841 x 1189 mm)"), QPrinter::A0);
	paperSizeCombo->addItem(tr("A1 (594 x 841 mm)"), QPrinter::A1);
	paperSizeCombo->addItem(tr("A2 (420 x 594 mm)"), QPrinter::A2);
	paperSizeCombo->addItem(tr("A3 (297 x 420 mm)"), QPrinter::A3);
	paperSizeCombo->addItem(tr("A4 (210 x 297 mm)"), QPrinter::A4);
	paperSizeCombo->addItem(tr("A5 (148 x 210 mm)"), QPrinter::A5);
	paperSizeCombo->addItem(tr("A6 (105 x 148 mm)"), QPrinter::A6);
	paperSizeCombo->addItem(tr("A7 (74 x 105 mm)"), QPrinter::A7);
	paperSizeCombo->addItem(tr("A8 (52 x 74 mm)"), QPrinter::A8);
	//paperSizeCombo->addItem(q->tr("A9 (37 x 52 mm)"), QPrinter::A9);
	paperSizeCombo->addItem(tr("B0 (1000 x 1414 mm)"), QPrinter::B0);
	paperSizeCombo->addItem(tr("B1 (707 x 1000 mm)"), QPrinter::B1);
	paperSizeCombo->addItem(tr("B2 (500 x 707 mm)"), QPrinter::B2);
	paperSizeCombo->addItem(tr("B3 (353 x 500 mm)"), QPrinter::B3);
	paperSizeCombo->addItem(tr("B4 (250 x 353 mm)"), QPrinter::B4);
	paperSizeCombo->addItem(tr("B5 (176 x 250 mm)"), QPrinter::B5);
	paperSizeCombo->addItem(tr("B6 (125 x 176 mm)"), QPrinter::B6);
	paperSizeCombo->addItem(tr("B7 (88 x 125 mm)"), QPrinter::B7);
	paperSizeCombo->addItem(tr("B8 (62 x 88 mm)"), QPrinter::B8);
	//paperSizeCombo->addItem(q->tr("B9 (44 x 62 mm)"), QPrinter::B9);
	//paperSizeCombo->addItem(q->tr("B10 (31 x 44 mm)"), QPrinter::B10);
	paperSizeCombo->addItem(tr("C5E (163 x 229 mm)"), QPrinter::C5E);
	paperSizeCombo->addItem(tr("DLE (110 x 220 mm)"), QPrinter::DLE);
	paperSizeCombo->addItem(tr("Executive (7.5 x 10 inches)"), QPrinter::Executive);
	paperSizeCombo->addItem(tr("Folio (210 x 330 mm)"), QPrinter::Folio);
	paperSizeCombo->addItem(tr("Ledger (432 x 279 mm)"), QPrinter::Ledger);
	paperSizeCombo->addItem(tr("Legal (8.5 x 14 inches)"), QPrinter::Legal);
	paperSizeCombo->addItem(tr("Letter (8.5 x 11 inches)"), QPrinter::Letter);
	paperSizeCombo->addItem(tr("Tabloid (279 x 432 mm)"), QPrinter::Tabloid);
	paperSizeCombo->addItem(tr("US #10 Envelope (105 x 241 mm)"), QPrinter::Comm10E);
	connect(paperSizeCombo, static_cast<void (QComboBox::*)(int)>(&QComboBox::activated), [this](int index)
	{
		m_pageSize = QPrinter::PageSize(paperSizeCombo->itemData(index).toInt());
	});
	paperSizeCombo->setCurrentIndex(4);
	m_pageSize = QPrinter::PageSize::A4;

	paperOrientationCombLabel = new QLabel(tr("Paper Orientation:"));
	paperOrientationCombo = new QComboBox(this);
	paperOrientationCombo->addItem(tr("Portrait"), QPrinter::Portrait);
	paperOrientationCombo->addItem(tr("Landscape"), QPrinter::Landscape);
	connect(paperOrientationCombo, static_cast<void (QComboBox::*)(int)>(&QComboBox::activated), [this](int index)
	{
		m_orientation = QPrinter::Orientation(paperOrientationCombo->itemData(index).toInt());
	});
	m_orientation = QPrinter::Orientation::Portrait;
}
void XrayDocTitleWizardPage::setPaperSizeCurrentIndex(int _index)
{
	paperSizeCombo->setCurrentIndex(_index);
	m_pageSize = QPrinter::PageSize(paperSizeCombo->itemData(_index).toInt());
}
int XrayDocTitleWizardPage::getPaperSizeCurrentIndex() const
{
	return paperSizeCombo->currentIndex();
}
void XrayDocTitleWizardPage::setPaperOrientationCurrentIndex(int _index)
{
	paperOrientationCombo->setCurrentIndex(_index);
	m_orientation = QPrinter::Orientation(paperOrientationCombo->itemData(_index).toInt());
}
int XrayDocTitleWizardPage::getPaperOrientationCurrentIndex() const
{
	return paperOrientationCombo->currentIndex();
}
QPrinter::PageSize XrayDocTitleWizardPage::getPageSize()
{
	return m_pageSize;
}
QPrinter::Orientation XrayDocTitleWizardPage::getPageOrientation()
{
	return m_orientation;
}

XrayDocHeaderWizardPage::XrayDocHeaderWizardPage(QWidget* parent) :
	QWizardPage(parent)
{
	setTitle(tr("Header of the Report Document"));
	setSubTitle(" ");

	p_groupBox = new QGroupBox(tr("Test Engineer"), this);

	p_contactPersonNameLabel = new QLabel(tr("Name:"));
	p_contactPersonName = new QLineEdit("Gerd-Hendrik Greiwe");
	p_contactPersonNameLabel->setBuddy(p_contactPersonName);

	p_contactPersonPhoneLabel = new QLabel(tr("Phone:"));
	p_contactPersonPhone = new QLineEdit("+49 (0) 173 6567366");
	p_contactPersonPhone->setValidator(new QRegExpValidator(QRegExp("^[0-9\\-\\+()/ ]+$"), this));	// it allows only numbers, dash, plus, small brackets, space, and forward slash.
	p_contactPersonPhoneLabel->setBuddy(p_contactPersonPhone);

	p_contactPersonEmailLabel = new QLabel(tr("Email:"));
	p_contactPersonEmail = new QLineEdit("gerd-hendrik.greiwe@xray-lab.com");
	p_contactPersonEmail->setValidator(new QRegExpValidator(QRegExp(".*@.*"), this));
	p_contactPersonEmailLabel->setBuddy(p_contactPersonEmail);

	//registerField("header.name*", p_contactPersonName);
	//registerField("header.phone", p_contactPersonPhone);
	//registerField("header.email", p_contactPersonEmail);

	auto layout = new QGridLayout;
	layout->addWidget(p_contactPersonNameLabel, 0, 0);
	layout->addWidget(p_contactPersonName, 0, 1);
	layout->addWidget(p_contactPersonPhoneLabel, 1, 0);
	layout->addWidget(p_contactPersonPhone, 1, 1);
	layout->addWidget(p_contactPersonEmailLabel, 2, 0);
	layout->addWidget(p_contactPersonEmail, 2, 1);
	p_groupBox->setLayout(layout);

	layout = new QGridLayout;
	layout->addWidget(p_groupBox);

	setLayout(layout);
	retranslate();
}
int XrayDocHeaderWizardPage::nextId() const
{
	return ReportWizardPage::DocFooter;
}
void XrayDocHeaderWizardPage::retranslate()
{
	setTitle(QApplication::translate("XrayDocHeaderWizardPage", "Header of the Report Document", nullptr));
	p_groupBox->setTitle(QApplication::translate("XrayDocHeaderWizardPage", "Test Engineer", nullptr));
	p_contactPersonNameLabel->setText(QApplication::translate("XrayDocHeaderWizardPage", "Name:", nullptr));
	p_contactPersonPhoneLabel->setText(QApplication::translate("XrayDocHeaderWizardPage", "Phone:", nullptr));
	p_contactPersonEmailLabel->setText(QApplication::translate("XrayDocHeaderWizardPage", "Email:", nullptr));
}

QString XrayDocHeaderWizardPage::getGroupBoxTitle() const
{
	return p_groupBox->title();
}
QString XrayDocHeaderWizardPage::getContactPersonName() const
{
	return p_contactPersonName->text();
}
QString XrayDocHeaderWizardPage::getContactPersonNameLabel() const
{
	return p_contactPersonNameLabel->text();
}
QString XrayDocHeaderWizardPage::getContactPersonPhone() const
{
	return p_contactPersonPhone->text();
}
QString XrayDocHeaderWizardPage::getContactPersonPhoneLabel() const
{
	return p_contactPersonPhoneLabel->text();
}
QString XrayDocHeaderWizardPage::getContactPersonEmail() const
{
	return p_contactPersonEmail->text();
}
QString XrayDocHeaderWizardPage::getContactPersonEmailLabel() const
{
	return p_contactPersonEmailLabel->text();
}

XrayDocFooterWizardPage::XrayDocFooterWizardPage(QWidget* parent) :
	QWizardPage(parent)
{
	setTitle(tr("Footer of the Report Document"));
	setSubTitle(" ");

	p_groupBox = new QGroupBox(tr("My Company Information"), this);

	p_companyNameLabel = new QLabel(tr("Name:"));
	p_companyName = new QLineEdit("XRAY-LAB GmbH & Co. KG");
	p_companyNameLabel->setBuddy(p_companyName);

	p_companyLocationLabel = new QLabel(tr("Location:"));
	p_companyLocation = new QLineEdit("Theodor-Schweitzer-Str. 1+3 75447 Sternenfels");
	p_companyLocationLabel->setBuddy(p_companyLocation);

	p_companyPhoneLabel = new QLabel(tr("Phone:"));
	p_companyPhone = new QLineEdit("+49 (0) 7045 / 20445-0");
	p_companyPhone->setValidator(new QRegExpValidator(QRegExp("^[0-9\\-\\+()/ ]+$"), this));
	p_companyPhoneLabel->setBuddy(p_companyPhone);

	p_companyFaxLabel = new QLabel(tr("Fax:"));
	p_companyFax = new QLineEdit("+49 (0) 7045 / 2044555");
	p_companyFax->setValidator(new QRegExpValidator(QRegExp("^[0-9\\-\\+()/ ]+$"), this));
	p_companyFaxLabel->setBuddy(p_companyFax);

	p_logoImageLabel = new QLabel(tr("Company Logo:"));
	p_logoFile = new QLineEdit("");
	p_logoImageLabel->setBuddy(p_logoFile);
	auto btn = new QPushButton(QApplication::translate("XrayDocFooterWizardPage", "Browse..."), this);
	btn->setToolTip(QApplication::translate("XrayDocFooterWizardPage", "Browse for a company logo image."));
	connect(btn, &QPushButton::clicked, [this]()
	{
		auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayDocFooterWizardPage", "Select File"), XrayGlobal::getLastFileOpenPath(), XrayGlobal::getAllSupportedImageExtensions(QApplication::translate("XrayPaintMainWindow", "Image Files")), nullptr);
		if (fileName.isEmpty())
			return;
		setLogoFile(fileName);
		XrayGlobal::setLastFileOpenPath(fileName);
	});

	p_logoImage = new QLabel("");
	p_logoImage->setMaximumHeight(300);

	//registerField("footer.companyname*", p_companyName);
	//registerField("footer.companylocation", p_companyLocation);
	//registerField("footer.companyphone", p_companyPhone);
	//registerField("footer.companyfax", p_companyFax);

	auto layout = new QGridLayout;
	layout->addWidget(p_companyNameLabel,			0, 0);
	layout->addWidget(p_companyName,				0, 1, 1, 2);
	layout->addWidget(p_companyLocationLabel,		1, 0);
	layout->addWidget(p_companyLocation,			1, 1, 1, 2);
	layout->addWidget(p_companyPhoneLabel,			2, 0);
	layout->addWidget(p_companyPhone,				2, 1, 1, 2);
	layout->addWidget(p_companyFaxLabel,			3, 0);
	layout->addWidget(p_companyFax,					3, 1, 1, 2);
	layout->addWidget(p_logoImageLabel,				4, 0);
	layout->addWidget(p_logoFile,					4, 1);
	layout->addWidget(btn,							4, 2);
	layout->addWidget(p_logoImage,					5, 1);
	p_groupBox->setLayout(layout);

	layout = new QGridLayout;
	layout->addWidget(p_groupBox);

	setLayout(layout);
	retranslate();
}
int XrayDocFooterWizardPage::nextId() const
{
	return ReportWizardPage::ClientInfo;
}
void XrayDocFooterWizardPage::retranslate()
{
	setTitle(QApplication::translate("XrayDocFooterWizardPage", "Footer of the Report Document", nullptr));
	p_groupBox->setTitle(QApplication::translate("XrayDocFooterWizardPage", "My Company Information", nullptr));
	p_companyNameLabel->setText(QApplication::translate("XrayDocFooterWizardPage", "Name:", nullptr));
	p_companyLocationLabel->setText(QApplication::translate("XrayDocFooterWizardPage", "Location:", nullptr));
	p_companyPhoneLabel->setText(QApplication::translate("XrayDocFooterWizardPage", "Phone:", nullptr));
	p_companyFaxLabel->setText(QApplication::translate("XrayDocFooterWizardPage", "Fax:", nullptr));
	p_logoImageLabel->setText(QApplication::translate("XrayDocFooterWizardPage", "Company Logo:", nullptr));
}

QString XrayDocFooterWizardPage::getGroupBoxTitle() const
{
	return p_groupBox->title();
}
QString XrayDocFooterWizardPage::getCompanyName() const
{
	return p_companyName->text();
}
QString XrayDocFooterWizardPage::getCompanyLocation() const
{
	return p_companyLocation->text();
}
QString XrayDocFooterWizardPage::getCompanyPhone() const
{
	return p_companyPhone->text();
}
QString XrayDocFooterWizardPage::getCompanyFax() const
{
	return p_companyFax->text();
}
void XrayDocFooterWizardPage::setLogoFile(const QString& fileName)
{
	auto img = QPixmap(fileName).scaled(QSize(300, 300), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	p_logoFile->setText(fileName);
	p_logoImage->setPixmap(img);
	p_logoImage->update();
}
QString XrayDocFooterWizardPage::getLogoFile() const
{
	return p_logoFile->text();
}

XrayClientInfoWizardPage::XrayClientInfoWizardPage(QWidget* parent) :
	QWizardPage(parent)
{
	setTitle(tr("Client/Order Information"));
	setSubTitle(" ");

	p_groupBox = new QGroupBox(tr("Order Information"), this);

	p_orderNumberLabel = new QLabel(tr("Order Number:"));
	p_orderNumber = new QLineEdit("22340");
	p_orderNumber->setValidator(new QRegExpValidator(QRegExp("^[0-9]+$"), this));
	p_orderNumberLabel->setBuddy(p_orderNumber);

	p_dateLabel = new QLabel(tr("Date:"));
	p_date = new QLineEdit(QDate::currentDate().toString("dd.MM.yyyy"));
	p_date->setValidator(new QRegExpValidator(QRegExp("^[0-9.]+$"), this));
	p_dateLabel->setBuddy(p_date);

	p_calendarBtn = new QPushButton(tr("Calendar"), this);
	connect(p_calendarBtn, &QPushButton::clicked, [this]()
	{
		auto calendar = new XrayQCalendarDialog(this);
		if (calendar->exec())
		{
			p_date->setText(calendar->selectedDate().toString("dd.MM.yyyy"));
			calendar->deleteLater();
		}
	});

	p_clientNameLabel = new QLabel(tr("Client Name:"));
	p_clientName = new QLineEdit("xxx GmbH & Co. KG");
	p_clientNameLabel->setBuddy(p_clientName);

	p_sampleNameLabel = new QLabel(tr("Sample:"));
	p_sampleName = new QLineEdit(tr("Electronic Equipments"));
	p_sampleNameLabel->setBuddy(p_sampleName);

	p_quantityLabel = new QLabel(tr("Quantity:"));
	p_quantity = new QLineEdit("1");
	p_quantity->setValidator(new QRegExpValidator(QRegExp("^[0-9]+$"), this));
	p_quantityLabel->setBuddy(p_sampleName);

	//registerField("clientinfo.companyname*", p_orderNumber);
	//registerField("clientinfo.companylocation", p_date);
	//registerField("clientinfo.companyphone", p_clientName);
	//registerField("clientinfo.companyfax", p_sampleName);
	//registerField("clientinfo.companyfax", p_quantity);

	auto layout = new QGridLayout;
	layout->addWidget(p_orderNumberLabel,	0, 0);
	layout->addWidget(p_orderNumber,		0, 1, 1, 2);
	layout->addWidget(p_dateLabel,			1, 0);
	layout->addWidget(p_date,				1, 1);
	layout->addWidget(p_calendarBtn,		1, 2);
	layout->addWidget(p_clientNameLabel,	2, 0);
	layout->addWidget(p_clientName,			2, 1, 1, 2);
	layout->addWidget(p_sampleNameLabel,	3, 0);
	layout->addWidget(p_sampleName,			3, 1, 1, 2);
	layout->addWidget(p_quantityLabel,		4, 0);
	layout->addWidget(p_quantity,			4, 1, 1, 2);
	p_groupBox->setLayout(layout);

	layout = new QGridLayout;
	layout->addWidget(p_groupBox);

	setLayout(layout);
	retranslate();
}
int XrayClientInfoWizardPage::nextId() const
{
	return ReportWizardPage::Investigation;
}
void XrayClientInfoWizardPage::retranslate()
{
	setTitle(QApplication::translate("XrayClientInfoWizardPage", "Client/Order Information", nullptr));
	p_groupBox->setTitle(QApplication::translate("XrayClientInfoWizardPage", "Order Information", nullptr));
	p_orderNumberLabel->setText(QApplication::translate("XrayClientInfoWizardPage", "Order Number:", nullptr));
	p_dateLabel->setText(QApplication::translate("XrayClientInfoWizardPage", "Date:", nullptr));
	p_calendarBtn->setText(QApplication::translate("XrayClientInfoWizardPage", "Calendar", nullptr));
	p_clientNameLabel->setText(QApplication::translate("XrayClientInfoWizardPage", "Client Name:", nullptr));
	p_sampleNameLabel->setText(QApplication::translate("XrayClientInfoWizardPage", "Sample:", nullptr));
	p_quantityLabel->setText(QApplication::translate("XrayClientInfoWizardPage", "Quantity:", nullptr));
}

QString XrayClientInfoWizardPage::getGroupBoxTitle() const
{
	return p_groupBox->title();
}
QString XrayClientInfoWizardPage::getOrderNumberLabel() const
{
	return p_orderNumberLabel->text();
}
QString XrayClientInfoWizardPage::getDateLabel() const
{
	return p_dateLabel->text();
}
QString XrayClientInfoWizardPage::getClientNameLabel() const
{
	return p_clientNameLabel->text();
}
QString XrayClientInfoWizardPage::getSampleNameLabel() const
{
	return p_sampleNameLabel->text();
}
QString XrayClientInfoWizardPage::getQuantityLabel() const
{
	return p_quantityLabel->text();
}

QString XrayClientInfoWizardPage::getOrderNumber() const
{
	return p_orderNumber->text();
}
QString XrayClientInfoWizardPage::getDate() const
{
	return p_date->text();
}
QString XrayClientInfoWizardPage::getClientName() const
{
	return p_clientName->text();
}
QString XrayClientInfoWizardPage::getSampleName() const
{
	return p_sampleName->text();
}
QString XrayClientInfoWizardPage::getQuantity() const
{
	return p_quantity->text();
}


XrayInvestigationWizardPage::XrayInvestigationWizardPage(QWidget* parent) :
	QWizardPage(parent)
{
	setTitle(tr("Investigation Sections of the Report Document"));
	setSubTitle(" ");

	p_investigationLabel = new QLabel(tr("Analysis:"));
	p_investigation = new QTextEdit;
	p_investigationLabel->setBuddy(p_investigation);

	p_problemLabel = new QLabel(tr("Problem:"));
	p_problem = new QTextEdit;
	p_problemLabel->setBuddy(p_problem);

	p_hypotheticalErrorLabel = new QLabel(tr("Hypothetical Error:"));
	p_hypotheticalfactor = new QTextEdit;
	p_hypotheticalErrorLabel->setBuddy(p_hypotheticalfactor);

	p_equipmentLabel = new QLabel(tr("Equipment:"));
	p_equipmentDescription = new QTextEdit(tr(""));
	p_equipmentLabel->setBuddy(p_equipmentDescription);

	QStringList machines;
	machines.append(QApplication::translate("XrayInvestigationWizardPage", "Micromex with Max. power 20W at 180kV."));
	machines.append(QApplication::translate("XrayInvestigationWizardPage", "Vtomex M  with Max. power 300W at 300kV."));
	machines.append(QApplication::translate("XrayInvestigationWizardPage", "Vtomex S  with Max. power 222W at 225kV."));
	listWidget = new XrayQListWidgetGroupBoxWithAddText("", machines, this);
	listWidget->layout()->setMargin(0);

	p_voltageUsedLabel = new QLabel(tr("Voltage Used:"));
	p_voltageUsed = new QLineEdit;
	p_voltageUsed->setValidator(new QRegExpValidator(QRegExp("^[0-9]+$"), this));
	p_voltageUsed->setPlaceholderText(tr("Voltage used in kV..."));
	p_voltageUsedLabel->setBuddy(p_voltageUsed);

	p_currentUsedLabel = new QLabel(tr("Current Used:"));
	p_currentUsed = new QLineEdit;
	p_currentUsed->setValidator(new QRegExpValidator(QRegExp("^[0-9]+$"), this));
	p_currentUsed->setPlaceholderText(tr("Current used in mA..."));
	p_currentUsedLabel->setBuddy(p_currentUsed);

	//registerField("investigation.analysis*", p_investigation);
	//registerField("investigation.problem*", p_problem);
	//registerField("investigation.facilities*", p_hypotheticalfactor);

	auto layout = new QGridLayout;
	layout->addWidget(p_investigationLabel, 0, 0, Qt::AlignTop);
	layout->addWidget(p_investigation, 0, 1);
	layout->addWidget(p_problemLabel, 1, 0, Qt::AlignTop);
	layout->addWidget(p_problem, 1, 1);
	layout->addWidget(p_hypotheticalErrorLabel, 2, 0, Qt::AlignTop);
	layout->addWidget(p_hypotheticalfactor, 2, 1);
	layout->addWidget(p_equipmentLabel, 3, 0, Qt::AlignTop);
	layout->addWidget(p_equipmentDescription, 3, 1);

	layout->addWidget(new QLabel(QApplication::translate("XrayInvestigationWizardPage", "Machines:")), 4, 0, Qt::AlignTop);
	layout->addWidget(listWidget, 4, 1, Qt::AlignTop);

	layout->addWidget(p_voltageUsedLabel, 5, 0);
	layout->addWidget(p_voltageUsed, 5, 1);
	layout->addWidget(p_currentUsedLabel, 6, 0);
	layout->addWidget(p_currentUsed, 6, 1);

	setLayout(layout);
	retranslate();
}
int XrayInvestigationWizardPage::nextId() const
{
	return ReportWizardPage::CustomizedSections;
}
void XrayInvestigationWizardPage::retranslate()
{
	setTitle(QApplication::translate("XrayInvestigationWizardPage", "Investigation Sections of the Report Document", nullptr));
	p_investigationLabel->setText(QApplication::translate("XrayInvestigationWizardPage", "Analysis:", nullptr));
	p_problemLabel->setText(QApplication::translate("XrayInvestigationWizardPage", "Problem:", nullptr));
	p_hypotheticalErrorLabel->setText(QApplication::translate("XrayInvestigationWizardPage", "Hypothetical Error:", nullptr));
	p_equipmentLabel->setText(QApplication::translate("XrayInvestigationWizardPage", "Equipment:", nullptr));
	p_equipmentDescription->setText(QApplication::translate("XrayInvestigationWizardPage", "", nullptr));
	p_voltageUsedLabel->setText(QApplication::translate("XrayInvestigationWizardPage", "Voltage Used:", nullptr));
	p_currentUsedLabel->setText(QApplication::translate("XrayInvestigationWizardPage", "Current Used:", nullptr));
	p_voltageUsed->setPlaceholderText(QApplication::translate("XrayInvestigationWizardPage", "Voltage used in kV...", nullptr));
	p_currentUsed->setPlaceholderText(QApplication::translate("XrayInvestigationWizardPage", "Current used in mA...", nullptr));
}

QString XrayInvestigationWizardPage::getInvestigationAnalysisLabel() const
{
	return p_investigationLabel->text();
}
QString XrayInvestigationWizardPage::getProblemStatementLabel() const
{
	return p_problemLabel->text();
}
QString XrayInvestigationWizardPage::getHypotheticalFactorLabel() const
{
	return p_hypotheticalErrorLabel->text();
}
QString XrayInvestigationWizardPage::getEquipmentLabel() const
{
	return p_equipmentLabel->text();
}

QString XrayInvestigationWizardPage::getInvestigationAnalysis() const
{
	return p_investigation->toPlainText();
}
QString XrayInvestigationWizardPage::getProblemStatement() const
{
	return p_problem->toPlainText();
}
QString XrayInvestigationWizardPage::getHypotheticalFactor() const
{
	return p_hypotheticalfactor->toPlainText();
}
QString XrayInvestigationWizardPage::getEquipmentDescription() const
{
	return p_equipmentDescription->toPlainText();
}
int XrayInvestigationWizardPage::getVoltages() const
{
	return p_voltageUsed->text().toInt();
}
int XrayInvestigationWizardPage::getCurrent() const
{
	return p_currentUsed->text().toInt();
}

void XrayInvestigationWizardPage::setMachinesList(const QStringList& list)
{
	return listWidget->setListTexts(list);
}
QStringList XrayInvestigationWizardPage::getMachinesList() const
{
	return listWidget->getListTexts();
}
void XrayInvestigationWizardPage::setSelectedMachinesList(const QStringList& list)
{
	listWidget->setSelectedListTexts(list);
}
QStringList XrayInvestigationWizardPage::getSelectedMachinesList() const
{
	return listWidget->getSelectedListTexts();
}

QString XrayInvestigationWizardPage::getEquipmentsAsString() const
{
	QString s;
	
	auto list = listWidget->getSelectedListTexts();
	for (const auto& i : list)
		s.append(i + "\n");

	if (!s.isEmpty())
		s.append("\n");

	if(!p_voltageUsed->text().isEmpty() || !p_currentUsed->text().isEmpty())
		s.append(QApplication::translate("XrayInvestigationWizardPage", "Range of inspection: ") + p_voltageUsed->text() + "kV" + "/" + p_currentUsed->text() + QString::fromUtf8("\302\265") + "A. ");

	s.append(QApplication::translate("XrayInvestigationWizardPage", "This is nondestructive for all parts because the radiated power is very low."));

	return s;
}

XrayCustomizedSectionsWizardPage::XrayCustomizedSectionsWizardPage(QWidget* parent) :
	QWizardPage(parent)
{
	setTitle(tr("Customized Sections of the Report Document"));
	setSubTitle(" ");

	p_sectionGroupBox = new QGroupBox(tr("Section"), this);
	p_layout = new QVBoxLayout;

	p_sectionGroupBox->setLayout(p_layout);

	auto mainLayout = new QGridLayout;
	mainLayout->addWidget(p_sectionGroupBox, 0, 0);
	mainLayout->addItem(new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding), 1, 0);
	setLayout(mainLayout);
	retranslate();
}
int XrayCustomizedSectionsWizardPage::nextId() const
{
	return ReportWizardPage::Results;
}
void XrayCustomizedSectionsWizardPage::retranslate()
{
	setTitle(QApplication::translate("XrayCustomizedSectionsWizardPage", "Customized Sections of the Report Document", nullptr));
	p_sectionGroupBox->setTitle(QApplication::translate("XrayCustomizedSectionsWizardPage", "Section", nullptr));
}

void XrayCustomizedSectionsWizardPage::resetLayout()
{
	p_sections.clear();
	XrayQtUtil::clearLayout(p_layout);
	p_layout = new QVBoxLayout;
	p_sectionGroupBox->setLayout(p_layout);
}
XrayCustomizedSectionsWizardPage::Section XrayCustomizedSectionsWizardPage::createSection()
{
	auto label = new QLineEdit;
	label->setFixedWidth(200);
	label->setPlaceholderText(QApplication::translate("XrayCustomizedSectionsWizardPage", "Section heading..."));
	
	auto text = new QTextEdit;
	text->setPlaceholderText(QApplication::translate("XrayCustomizedSectionsWizardPage", "Section heading description..."));
	
	auto btn = new QPushButton;
	btn->setIcon(QIcon(":/paint/images/remove_icon.png"));
	btn->setFixedWidth(30);
	btn->setToolTip(QApplication::translate("XrayCustomizedSectionsWizardPage", "Remove Section"));
	connect(btn, &QPushButton::clicked, [this, btn]()
	{
		auto it = std::find_if(p_sections.begin(), p_sections.end(),
			[btn](const Section o)
		{
			return o.button == btn;
		});

		if (it != p_sections.end())
		{
			for (auto i = 0; i < p_sections.size(); i++)
			{
				if (p_sections[i].button == (*it).button)
				{
					p_layout->removeItem(p_sections[i].layout);
					p_layout->removeWidget(p_sections[i].lineEdit);
					p_layout->removeWidget(p_sections[i].textEdit);
					p_layout->removeWidget(p_sections[i].button);

					p_sections[i].layout->deleteLater();
					p_sections[i].button->deleteLater();
					p_sections[i].textEdit->deleteLater();
					p_sections[i].lineEdit->deleteLater();
					p_sections.removeAt(i);
					break;
				}
			}
		}
	});

	auto hLayout = new QHBoxLayout;
	hLayout->addWidget(label, 0, Qt::AlignTop);
	hLayout->addWidget(text, 1, Qt::AlignTop);
	hLayout->addWidget(btn, 0, Qt::AlignTop);
	hLayout->setAlignment(Qt::AlignTop);

	p_layout->addLayout(hLayout);

	p_sections.push_back({ hLayout, label, text, btn });

	return { hLayout, label, text, btn };
}

XrayResultsWizardPage::XrayResultsWizardPage(QWidget* parent) :
	QWizardPage(parent)
{
	setTitle(tr("Results Sections of the Report Document"));
	setSubTitle(" ");

	p_resultsDescriptionLabel = new QLabel(tr("Results:"));
	p_resultsDescription = new QTextEdit;
	p_resultsDescriptionLabel->setBuddy(p_resultsDescription);

	p_resultsDataLabel = new QLabel(tr("Data Files:"));
	listWidget = new XrayQListWidgetGroupBoxWithAddFile("", this);
	listWidget->layout()->setMargin(0);
	listWidget->setBrowseExtensionFilters("CSV File (*.csv)");

	p_csv_delimeter = new QLineEdit(",");
	p_csv_delimeter->setFixedWidth(20);
	p_csv_delimeter->setToolTip(tr("Specify the separator delimiter that could be comma(,) or semicolon(;) or bar(|) or just space( ), etc."));

	p_imageDescriptionLabel = new QLabel(tr("Example Images Description:"));
	p_imageDescription = new QTextEdit;
	p_imageDescriptionLabel->setBuddy(p_imageDescription);

	//registerField("results.result*", p_results);
	//registerField("results.datafilepath*", p_resultsTable);
	//registerField("results.imagedescription*", p_imageDescription);
	//registerField("results.imagefilepath*", p_imagePath);

	auto layout = new QGridLayout;
	layout->addWidget(p_resultsDescriptionLabel, 0, 0, Qt::AlignTop);
	layout->addWidget(p_resultsDescription, 0, 1, 1, 3);
	layout->addWidget(p_resultsDataLabel, 1, 0, Qt::AlignTop);
	layout->addWidget(listWidget, 1, 1);
	layout->addWidget(p_csv_delimeter, 1, 2);
	layout->addWidget(p_imageDescriptionLabel, 2, 0, Qt::AlignTop);
	layout->addWidget(p_imageDescription, 2, 1, 1, 3);

	setLayout(layout);
	retranslate();
}
int XrayResultsWizardPage::nextId() const
{
	return ReportWizardPage::Paint;
}
void XrayResultsWizardPage::retranslate()
{
	setTitle(QApplication::translate("XrayResultsWizardPage", "Results Sections of the Report Document"));
	p_resultsDescriptionLabel->setText(QApplication::translate("XrayResultsWizardPage", "Results:"));
	p_resultsDataLabel->setText(QApplication::translate("XrayResultsWizardPage", "Data Files:"));
	p_imageDescriptionLabel->setText(QApplication::translate("XrayResultsWizardPage", "Example Images Description:"));
	p_csv_delimeter->setToolTip(QApplication::translate("XrayResultsWizardPage", "Specify the separator delimiter that could be comma(,) or semicolon(;) or bar(|) or just space( ), etc."));
}

QString XrayResultsWizardPage::getResultDescriptionLabel() const
{
	return p_resultsDescriptionLabel->text();
}
QString XrayResultsWizardPage::getResultDataLabel() const
{
	return p_resultsDataLabel->text();
}
QString XrayResultsWizardPage::getImageDescriptionLabel() const
{
	return p_imageDescriptionLabel->text();
}

QString XrayResultsWizardPage::getResultDescription() const
{
	return p_resultsDescription->toPlainText();
}
QStringList XrayResultsWizardPage::getDataFiles() const
{
	return listWidget->getListTexts();
}
void XrayResultsWizardPage::setDataFiles(const QStringList& fileNames)
{
	listWidget->setListTexts(fileNames);
}
void XrayResultsWizardPage::setSelectedDataFiles(const QStringList& fileNames)
{
	listWidget->setSelectedListTexts(fileNames);
}
QStringList XrayResultsWizardPage::getSelectedDataFiles() const
{
	return listWidget->getSelectedListTexts();
}
QString XrayResultsWizardPage::getCsvDelimeter() const
{
	return p_csv_delimeter->text();
}
QString XrayResultsWizardPage::getImageDescription() const
{
	return p_imageDescription->toPlainText();
}

XrayPaintWizardPage::XrayPaintWizardPage(QWidget* parent) :
	QWizardPage(parent)
{
	setTitle(tr("Paint Section of the Report Document"));
	setSubTitle(" ");

	p_paintWidget = new XrayPaintMainWindow;
	p_paintWidget->setReportingToolBarVisible(true);
	auto layout = new QGridLayout;
	layout->addWidget(p_paintWidget);
	setLayout(layout);
	retranslate();
}
int XrayPaintWizardPage::nextId() const
{
	return ReportWizardPage::CustomizedSections2;
}
void XrayPaintWizardPage::retranslate()
{
	setTitle(QApplication::translate("XrayPaintWizardPage", "Paint Section of the Report Document", nullptr));
}
XrayPaintMainWindow* XrayPaintWizardPage::getPaintWidget() const
{
	return p_paintWidget;
}


XrayCustomizedSections2WizardPage::XrayCustomizedSections2WizardPage(QWidget* parent) :
	XrayCustomizedSectionsWizardPage(parent)
{
}
int XrayCustomizedSections2WizardPage::nextId() const
{
	return ReportWizardPage::Finish;
}

XrayFinishWizardPage::XrayFinishWizardPage(QWidget* parent) :
	QWizardPage(parent)
{
	setTitle(tr("Save Generated Report As"));
	setSubTitle(" ");
	setFinalPage(false);

	p_bestRegardsLabel = new QLabel(QApplication::translate("XrayFinishWizardPage", ""), this);
	p_bestRegardsDescription = new QTextEdit(QApplication::translate("XrayFinishWizardPage", "We will send you all x-ray inspection results in a USB-Stick. If you need the results faster, we can establish a FTP-account for you.<br/><br/>In case of any question, please feel free to contact me.<br/><br/>Mit freundlichen Gruessen / With best regards,"));
	p_bestRegardsLabel->setBuddy(p_bestRegardsDescription);

	p_signatureLabel = new QLabel(QApplication::translate("XrayFinishWizardPage", ""), this);
	p_signatureFile = new QLineEdit(this);
	p_signatureFile->setPlaceholderText(QApplication::translate("XrayFinishWizardPage", "Signature image file..."));
	p_signatureLabel->setBuddy(p_signatureFile);

	p_signatureFileBrowseBtn = new QPushButton(QApplication::translate("XrayFinishWizardPage", "&Browse..."), this);
	//p_signatureFileBrowseBtn->setStyleSheet("QPushButton { Padding: 1px; height: 26px; width: 120px; Border-radius: 0px; Background: rgb(90, 90, 90); Color: #fefefe; } QPushButton:pressed { background-color: rgba(100, 100, 100, 50); }QPushButton:hover { background-color: rgba(100, 100, 100, 100); border: 1px solid rgba(170, 170, 0, 255); } QPushButton:focus { border: 1px solid rgba(170, 170, 0, 255); font-weight: bold; outline: none; }");
	p_signatureFileBrowseBtn->setToolTip(QApplication::translate("XrayFinishWizardPage", "Select a signature image file."));
	connect(p_signatureFileBrowseBtn, &QPushButton::clicked, [this]()
	{
		auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayFinishWizardPage", "Select File"), XrayGlobal::getLastFileOpenPath(), XrayGlobal::getAllSupportedImageExtensions(QApplication::translate("XrayFinishWizardPage", "Image File")), nullptr);
		if (!fileName.isEmpty())
		{
			p_signatureFile->setText(fileName);
			XrayGlobal::setLastFileOpenPath(fileName);
		}
	});

	auto layout = new QGridLayout;
	layout->addWidget(p_bestRegardsLabel, 0, 0, Qt::AlignTop);
	layout->addWidget(p_bestRegardsDescription, 0, 1, 1, 2);
	layout->addWidget(p_signatureLabel, 1, 0, Qt::AlignTop);
	layout->addWidget(p_signatureFile, 1, 1);
	layout->addWidget(p_signatureFileBrowseBtn, 1, 2);
	setLayout(layout);
	retranslate();
}
int XrayFinishWizardPage::nextId() const
{
	return 1;
}
void XrayFinishWizardPage::retranslate()
{
	setTitle(QApplication::translate("XrayFinishWizardPage", "Save Generated Report As"));
}

void XrayFinishWizardPage::setSignatureFile(const QString& _filePath)
{
	p_signatureFile->setText(_filePath);
}
QString XrayFinishWizardPage::getSignatureFile() const
{
	return p_signatureFile->text();
}
void XrayFinishWizardPage::setRegardsDescription(const QString& _text)
{
	p_bestRegardsDescription->setText(_text);
}
QString XrayFinishWizardPage::getRegardsDescription() const
{
	return p_bestRegardsDescription->toPlainText();
}