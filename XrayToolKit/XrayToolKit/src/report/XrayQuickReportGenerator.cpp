﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/015
** filename: 	XrayQuickReportGenerator.cpp
** file base:	XrayQuickReportGenerator
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		classes that provide wizard pages for the reporting widget.
****************************************************************************/

#include "XrayQuickReportGenerator.h"
#include "XrayGlobal.h"
#include "XrayQtUtil.h"

#include <QApplication>
#include <QDesktopServices>
#include <QPushButton>
#include <QMessageBox>
#include <QDir>
#include <QFileDialog>
#include <QTextCodec>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayQuickReportGenerator::XrayQuickReportGenerator()
{
}
void XrayQuickReportGenerator::clear()
{
	m_images.clear();
	m_map.clear();
	m_reportType = ReportType::None;
}

QStringList XrayQuickReportGenerator::parseSrcImages(const QString &html)
{
	QRegExp expression("src=[\"\'](.*)[\"\']", Qt::CaseInsensitive);
	expression.setMinimal(true);
	auto ipos = 0;
	QStringList images;
	while ((ipos = expression.indexIn(html, ipos)) != -1)
	{
		auto src = expression.cap(1);
		images.append(src);
		ipos += expression.matchedLength();
	}

	return images;
}

QString XrayQuickReportGenerator::parseValue(const QString &html, const QString &str, const QString &splitStr, Qt::CaseSensitivity cs)
{
	QRegExp expression("\\b" + str + "\\b", cs);
	auto ipos = 0;
	QStringList images;
	while ((ipos = expression.indexIn(html, ipos)) != -1)
	{
		ipos += expression.matchedLength();
		break;
	}

	if (ipos > 0)
	{
		QString value;
		while (html[ipos] != '<')
			value.append(html[ipos++]);
		value.remove(' ');
		auto values = value.split(splitStr);
		return !values.isEmpty() ? values.back() : "";
	}

	return "";
}
QString XrayQuickReportGenerator::parseValue(const QString &html, const QString &str, Qt::CaseSensitivity cs)
{
	QRegExp expression("\\b" + str + "\\b", cs);
	auto ipos = 0;
	QStringList images;
	while ((ipos = expression.indexIn(html, ipos)) != -1)
	{
		ipos += expression.matchedLength();
		break;
	}

	if (ipos > 0)
	{
		QString value;
		while (html[ipos] != '<')
			value.append(html[ipos++]);
		return value.trimmed();
	}

	return "";
}
bool XrayQuickReportGenerator::containValue(const QString &html, const QString &str, Qt::CaseSensitivity cs)
{
	QRegExp expression("\\b" + str + "\\b", cs);
	auto ipos = 0;
	while ((ipos = expression.indexIn(html, ipos)) != -1)
	{
		ipos += expression.matchedLength();
		break;
	}

	if (ipos > 0)
		return true;

	return false;
}

QMap<QString, QString> XrayQuickReportGenerator::readNominalComparison(const QString& str)
{
	QMap<QString, QString> values;

	values.insert("Actual object", parseValue(str, "Actual object"));
	values.insert("Nominal object", parseValue(str, "Nominal object"));
	values.insert("Max. distance [mm]", parseValue(str, "Max. distance", "[mm]"));
	values.insert("Precision", parseValue(str, "Precision"));
	values.insert("Swap sign of deviation", parseValue(str, "Swap sign of deviation"));
	values.insert("Consider surface orientation", parseValue(str, "Consider surface orientation"));
	values.insert("Compensate for mesh problems", parseValue(str, "Compensate for mesh problems"));
	values.insert("Memory optimized", parseValue(str, "Memory optimized"));
	values.insert("Deviation interval [mm]", parseValue(str, "Deviation interval", "[mm]"));
	values.insert("Interval mode", parseValue(str, "Interval mode"));
	values.insert(QString::fromUtf8("Min. surface [mm²]"), parseValue(str, "Min. surface", QString::fromUtf8("[mm²]")));
	values.insert("Tolerancing", parseValue(str, "Tolerancing"));

	values.insert("Min. deviation [mm]", parseValue(str, "Min. deviation", "[mm]"));
	values.insert("Max. deviation [mm]", parseValue(str, "Max. deviation", "[mm]"));

	values.insert("Cumulated absolute 90 [mm]", parseValue(str, "Cumulated absolute 90", "[mm]"));
	values.insert("Cumulated absolute 95 [mm]", parseValue(str, "Cumulated absolute 95", "[mm]"));
	values.insert("Cumulated absolute 98 [mm]", parseValue(str, "Cumulated absolute 98", "[mm]"));

	return values;
}

QString XrayQuickReportGenerator::nominalComparisonImageText(int index)
{
	QString caption;
	if (index == 0)
	{
		caption = 
			QString("Min. deviation [mm]") + ": " + m_map.value("Min. deviation [mm]") + '\n' +
			QString("Max. deviation [mm]") + ": " + m_map.value("Max. deviation [mm]");
	}
	else if (index == 1)
	{
		caption =
			QString("Cumulated absolute 90 [mm]") + ": " + m_map.value("Cumulated absolute 90 [mm]") + '\n' +
			QString("Cumulated absolute 95 [mm]") + ": " + m_map.value("Cumulated absolute 95 [mm]") + '\n' +
			QString("Cumulated absolute 98 [mm]") + ": " + m_map.value("Cumulated absolute 98 [mm]");
	}

	return caption.toUtf8();
}

QMap<QString, QString> XrayQuickReportGenerator::readPorosityAnalysis(const QString& str)
{
	QMap<QString, QString> values;

	values.insert("Contrast", parseValue(str, "Contrast"));
	values.insert("Local area size [vox]", parseValue(str, "Local area size", "[vox]"));
	values.insert("Analysis area", parseValue(str, "Analysis area"));
	values.insert("Closing radius [voxel]", parseValue(str, "Closing radius", "[voxel]"));
	values.insert("Surface distance [vox]", parseValue(str, "Surface distance", "[vox]"));
	values.insert("Preview", parseValue(str, "Preview"));
	values.insert("Probability threshold", parseValue(str, "Probability threshold"));
	values.insert("Min. Volume [vox]", parseValue(str, "Min. Volume", "[vox]"));
	values.insert("Max. Diameter [mm]", parseValue(str, "Max. Diameter", "[mm]"));
	values.insert("Compactness range", parseValue(str, "Compactness range"));
	values.insert("Sphericity range", parseValue(str, "Sphericity range"));
	values.insert("Classification", parseValue(str, "Classification"));
	values.insert("Edge distance range [mm]", parseValue(str, "Edge distance range", "[mm]"));
	values.insert(QString::fromUtf8("Cut face range [mm²]"), parseValue(str, "Cut face range", QString::fromUtf8("[mm²]")));
	values.insert("Edge distance", parseValue(str, "Edge distance"));
	values.insert("Ignore defects caused by CT artifacts mode", parseValue(str, "Ignore defects caused by CT artifacts mode"));
	values.insert("Ignore defects caused by CT artifacts center", parseValue(str, "Ignore defects caused by CT artifacts center"));
	values.insert("Ignore defects caused by CT artifacts normal", parseValue(str, "Ignore defects caused by CT artifacts normal"));
	values.insert("Check neighborhood", parseValue(str, "Check neighborhood"));
	values.insert("Ignore streak artifacts", parseValue(str, "Ignore streak artifacts"));
	values.insert("Wall thickness analysis", parseValue(str, "Wall thickness analysis"));
	values.insert("Area size [mm]", parseValue(str, "Area size", "[mm]"));
	values.insert("Hot spot threshold [%]", parseValue(str, "Hot spot threshold", "[%]"));
	values.insert("Direction", parseValue(str, "Direction"));
	values.insert("Slice thickness [vox]", parseValue(str, "Slice thickness", "[vox]"));
	values.insert("Integration mesh", parseValue(str, "Integration mesh"));
	values.insert("Tolerancing", parseValue(str, "Tolerancing"));

	values.insert(QString::fromUtf8("Σ Voxel"), parseValue(str, QString::fromUtf8("Σ Voxel")));
	values.insert(QString::fromUtf8("Σ PX [mm²]"), parseValue(str, QString::fromUtf8("Σ PX"), QString::fromUtf8("[mm²]")));
	values.insert(QString::fromUtf8("Σ Volume [mm³]"), parseValue(str, QString::fromUtf8("Σ Volume"), QString::fromUtf8("[mm³]")));
	values.insert(QString::fromUtf8("Σ PY [mm²]"), parseValue(str, QString::fromUtf8("Σ PY"), QString::fromUtf8("[mm²]")));
	values.insert(QString::fromUtf8("Σ Surface [mm²]"), parseValue(str, QString::fromUtf8("Σ Surface"), QString::fromUtf8("[mm²]")));
	values.insert(QString::fromUtf8("Σ PZ [mm²]"), parseValue(str, QString::fromUtf8("Σ PZ"), QString::fromUtf8("[mm²]")));
	values.insert(QString::fromUtf8("Material volume [mm³]"), parseValue(str, "Material volume", QString::fromUtf8("[mm³]")));
	values.insert(QString::fromUtf8("Defect volume [mm³]"), parseValue(str, "Defect volume", QString::fromUtf8("[mm³]")));
	values.insert(QString::fromUtf8("Defect volume ratio [%]"), parseValue(str, "Defect volume ratio", QString::fromUtf8("[%]")));

	values.insert("Ordinate feature", parseValue(str, "Ordinate feature"));
	values.insert("Abscissa feature", parseValue(str, "Abscissa feature"));

	return values;
}

QString XrayQuickReportGenerator::porosityAnalysisImageText(int index)
{
	QString caption;
	if (index == 0)
	{
		caption =
			QString::fromUtf8("Σ Voxel") + ": " + m_map.value(QString::fromUtf8("Σ Voxel")) + '\n' +
			QString::fromUtf8("Σ PX [mm²]") + ": " + m_map.value(QString::fromUtf8("Σ PX [mm²]")) + '\n' +
			QString::fromUtf8("Σ Volume [mm³]") + ": " + m_map.value(QString::fromUtf8("Σ Volume [mm³]")) + '\n' +
			QString::fromUtf8("Σ PY [mm²]") + ": " + m_map.value(QString::fromUtf8("Σ PY [mm²]")) + '\n' +
			QString::fromUtf8("Σ Surface [mm²]") + ": " + m_map.value(QString::fromUtf8("Σ Surface [mm²]")) + '\n' +
			QString::fromUtf8("Σ PZ [mm²]") + ": " + m_map.value(QString::fromUtf8("Σ PZ [mm²]")) + '\n' +
			QString::fromUtf8("Material volume [mm³]") + ": " + m_map.value(QString::fromUtf8("Material volume [mm³]")) + '\n' +
			QString::fromUtf8("Defect volume [mm³]") + ": " + m_map.value(QString::fromUtf8("Defect volume [mm³]")) + '\n' +
			QString::fromUtf8("Defect volume ratio [%]") + ": " + m_map.value(QString::fromUtf8("Defect volume ratio [%]"));
	}
	else if (index == 1)
	{
		caption =
			QString("Ordinate feature") + ": " + m_map.value("Ordinate feature") + '\n' +
			QString("Abscissa feature") + ": " + m_map.value("Abscissa feature");
	}

	return caption.toUtf8();
}

QMap<QString, QString> XrayQuickReportGenerator::readWallThicknessAnalysisRayMethod(const QString& str)
{
	QMap<QString, QString> values;

	values.insert("Surface", parseValue(str, "Surface"));
	values.insert("Analysis mode", parseValue(str, "Analysis mode"));
	values.insert("Thickness range [mm]", parseValue(str, "Thickness range", "[mm]"));
	values.insert("Search angle [deg]", parseValue(str, "Search angle", "[deg]"));
	values.insert(QString::fromUtf8("Min. volume [mm³]"), parseValue(str, "Min. volume", QString::fromUtf8("[mm³]")));
	values.insert("Create closed surface", parseValue(str, "Create closed surface"));
	values.insert("Use advanced mode", parseValue(str, "Use advanced mode", ")"));
	values.insert("High encoding precision", parseValue(str, "High encoding precision"));
	values.insert("Position property", parseValue(str, "Position property"));
	values.insert("Encoding precision [mm]", parseValue(str, "Encoding precision", "[mm]"));
	values.insert("Tolerancing", parseValue(str, "Tolerancing"));

	values.insert("Min. wall thickness [mm]", parseValue(str, "Min. wall thickness", "[mm]"));
	values.insert("Max. wall thickness [mm]", parseValue(str, "Max. wall thickness", "[mm]"));
	values.insert("Mean wall thickness [mm]", parseValue(str, "Mean wall thickness", "[mm]"));
	values.insert("Wall thickness deviation [mm]", parseValue(str, "Wall thickness deviation", "[mm]"));
	values.insert(QString::fromUtf8("Analyzed volume [mm³]"), parseValue(str, "Analyzed volume", QString::fromUtf8("[mm³]")));

	return values;
}

QString XrayQuickReportGenerator::wallThicknessAnalysisRayMethodImageText(int index)
{
	QString caption;
	if (index == 0)
	{
		caption =
			QString("Min. wall thickness [mm]") + ": " + m_map.value("Min. wall thickness [mm]") + '\n' +
			QString("Max. wall thickness [mm]") + ": " + m_map.value("Max. wall thickness [mm]") + '\n' +
			QString("Mean wall thickness [mm]") + ": " + m_map.value("Mean wall thickness [mm]") + '\n' +
			QString("Wall thickness deviation [mm]") + ": " + m_map.value("Wall thickness deviation [mm]") + '\n' +
			QString::fromUtf8("Analyzed volume [mm³]") + ": " + m_map.value(QString::fromUtf8("Analyzed volume [mm³]"));
	}
	else if (index == 1)
	{
	}

	return caption.toUtf8();
}

QMap<QString, QString> XrayQuickReportGenerator::readWallThicknessAnalysisSphereMethod(const QString& str)
{
	QMap<QString, QString> values;

	values.insert("Surface", parseValue(str, "Surface"));
	values.insert("Analysis mode", parseValue(str, "Analysis mode"));
	values.insert("Thickness range [mm]", parseValue(str, "Thickness range", "[mm]"));
	values.insert("Min. volume [vox]", parseValue(str, "Min. volume", "[vox]"));
	values.insert("Tolerancing", parseValue(str, "Tolerancing"));

	values.insert("Min. wall thickness [mm]", parseValue(str, "Min. wall thickness", "[mm]"));
	values.insert("Max. wall thickness [mm]", parseValue(str, "Max. wall thickness", "[mm]"));
	values.insert("Mean wall thickness [mm]", parseValue(str, "Mean wall thickness", "[mm]"));
	values.insert("Standard deviation [mm]", parseValue(str, "Standard deviation", "[mm]"));

	return values;
}
QString XrayQuickReportGenerator::wallThicknessAnalysisSphereMethodImageText(int index)
{
	QString caption;
	if (index == 0)
	{
		caption =
			QString("Min. wall thickness [mm]") + ": " + m_map.value("Min. wall thickness [mm]") + '\n' +
			QString("Max. wall thickness [mm]") + ": " + m_map.value("Max. wall thickness [mm]") + '\n' +
			QString("Mean wall thickness [mm]") + ": " + m_map.value("Mean wall thickness [mm]") + '\n' +
			QString("Standard deviation [mm]") + ": " + m_map.value("Standard deviation [mm]");
	}
	else if (index == 1)
	{
	}

	return caption.toUtf8();
}
QString XrayQuickReportGenerator::getImageCaption(int index)
{
	if (m_reportType == ReportType::Nominal)
		return nominalComparisonImageText(index);
	else if (m_reportType == ReportType::Porosity)
		return porosityAnalysisImageText(index);
	else if (m_reportType == ReportType::WallThicknessRay)
		return wallThicknessAnalysisRayMethodImageText(index);
	else if (m_reportType == ReportType::WallThicknessSphere)
		return wallThicknessAnalysisSphereMethodImageText(index);
	return "";
}

QMap<QString, QString> XrayQuickReportGenerator::readProjectIniFile(const QString &fileName)
{
	QMap<QString, QString> values;

	QSettings settings(fileName, QSettings::IniFormat);

	values.insert("Project Name", settings.value("Project/name", "n/a").toString());
	values.insert("Project CreatedDate", settings.value("Project/createdDate", "n/a").toString());
	values.insert("Project ContactPerson", settings.value("Project/contactPerson", "n/a").toString());
	values.insert("Project Location", settings.value("Project/location", "n/a").toString());
	values.insert("Project Machine", settings.value("Project/machine", "n/a").toString());
	values.insert("Project Component", settings.value("Project/component", "n/a").toString());
	values.insert("Project Quantity", settings.value("Project/quantity", "n/a").toString());
	values.insert("Project Analysis", settings.value("Project/analysis", "n/a").toString());
	values.insert("Project Remarks", settings.value("Project/remarks", "n/a").toString());

	values.insert("Customer Name", settings.value("CustomerInfo/name", "n/a").toString());
	values.insert("Customer Address1", settings.value("CustomerInfo/address1", "n/a").toString());
	values.insert("Customer Address2", settings.value("CustomerInfo/address2", "n/a").toString());
	values.insert("Customer Country", settings.value("CustomerInfo/country", "n/a").toString());

	values.insert("CustomerContactPerson Name", settings.value("CustomerContactPerson/name", "n/a").toString());
	values.insert("CustomerContactPerson Email", settings.value("CustomerContactPerson/email", "n/a").toString());
	values.insert("CustomerContactPerson Phone", settings.value("CustomerContactPerson/phone", "n/a").toString());

	return values;
}

bool XrayQuickReportGenerator::parseIniFile(const QString &fileName)
{
	auto values = readProjectIniFile(fileName);
	if (values.empty())
		return false;

	QMapIterator<QString, QString> i(values);
	while (i.hasNext())
	{
		i.next();
		m_map.insert(i.key(), i.value());
	};

	return true;
}

int XrayQuickReportGenerator::parseHtmlFile(const QString &fileName)
{
	QFile file(fileName);

	if (!file.open(QIODevice::ReadOnly))
		return 0;

	auto str = file.readAll();
	if (str.isEmpty())
		return -1;

	if (containValue(str, "Nominal") && containValue(str, "actual comparison"))
	{
		m_map = readNominalComparison(str);
		m_reportType = ReportType::Nominal;
	}
	else if (containValue(str, "Porosity") && containValue(str, "inclusion analysis"))
	{
		m_map = readPorosityAnalysis(str);
		m_reportType = ReportType::Porosity;
	}
	else if (containValue(str, "Wall thickness analysis") && containValue(str, "Ray method"))
	{
		m_map = readWallThicknessAnalysisRayMethod(str);
		m_reportType = ReportType::WallThicknessRay;
	}
	else if (containValue(str, "Wall thickness analysis") && containValue(str, "Sphere method"))
	{
		m_map = readWallThicknessAnalysisSphereMethod(str);
		m_reportType = ReportType::WallThicknessSphere;
	}
	else
	{
		m_reportType = ReportType::None;
		return -2;
	}

	m_images = parseSrcImages(str);
	for (auto& i : m_images)
		i.prepend(QFileInfo(file).path() + '/');

	return 1;
}

void XrayQuickReportGenerator::print()
{
	qDebug().noquote() << "Images";
	for (auto& i : m_images)
		qDebug().noquote() << i;

	qDebug().noquote() << "Values";
	QMapIterator<QString, QString> i(m_map);
	while (i.hasNext())
	{
		i.next();
		qDebug().noquote() << i.key() << ":" << i.value();
	};
}