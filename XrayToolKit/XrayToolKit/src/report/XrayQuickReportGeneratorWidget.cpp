﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/015
** filename: 	XrayQuickReportGeneratorWidget.cpp
** file base:	XrayQuickReportGeneratorWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		classes that provide wizard pages for the reporting widget.
****************************************************************************/

#include "XrayQuickReportGeneratorWidget.h"
#include "ui_XrayQuickReportGeneratorWidget.h"

#include "XrayGlobal.h"
#include "XrayLicenseGlobals.h"
#include "XrayCVUtil.h"
#include "XrayQtUtil.h"
#include "XrayQFileUtil.h"

#include <QApplication>
#include <QFileDialog>
#include <QInputDialog>
#include <QTimer>
#include <QMessageBox>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayQuickReportGeneratorWidget::XrayQuickReportGeneratorWidget(QWidget* parent) :
	QWidget(parent),
	ui(new Ui::XrayQuickReportGeneratorWidget),
	m_isDemo(true)
{
	ui->setupUi(this);
	setWindowTitle("XQuick Report Generator");

	connect(ui->projBrowseBtn, &QPushButton::clicked, this, &XrayQuickReportGeneratorWidget::browseIniFile);
	connect(ui->htmlBrowseBtn, &QPushButton::clicked, this, &XrayQuickReportGeneratorWidget::browseHtml);
	connect(ui->signatureBrowseBtn, &QPushButton::clicked, this, &XrayQuickReportGeneratorWidget::browseSignature);
	connect(ui->imgsBrowseBtn, &QPushButton::clicked, this, &XrayQuickReportGeneratorWidget::browseImages);

	auto save = ui->buttonBox->button(QDialogButtonBox::Save);
	save->setToolTip(QApplication::translate("XrayQuickReportGeneratorWidget", "Save generated report to the disk."));
	auto reset = ui->buttonBox->button(QDialogButtonBox::Reset);
	reset->setText(QApplication::translate("XrayQuickReportGeneratorWidget", "Preview"));
	reset->setToolTip(QApplication::translate("XrayQuickReportGeneratorWidget", "Report preview."));
	connect(save, &QPushButton::clicked, this, &XrayQuickReportGeneratorWidget::save);
	connect(reset, &QPushButton::clicked, this, &XrayQuickReportGeneratorWidget::preview);

	auto headingFont = QFont("Arial");
	headingFont.setPointSize(12);
	headingFont.setBold(true);
	headingFont.setUnderline(true);

	auto textFont = QFont("Arial");
	textFont.setPointSize(12);

	p_docHeadingFontLabel = new QLabel(QApplication::translate("XrayQuickReportGeneratorWidget", "Heading Text:"));
	p_docBodyFontLabel = new QLabel(QApplication::translate("XrayQuickReportGeneratorWidget", "Body Text:"));
	p_textFontSelector = new XrayFontSelectorWidget;
	p_textFontSelector->setAutoFillBackground(true);
	p_textFontSelector->setFont(textFont);
	p_headingFontSelector = new XrayFontSelectorWidget;
	p_headingFontSelector->setAutoFillBackground(true);
	p_headingFontSelector->setFont(headingFont);
	auto glayout = new QGridLayout;
	glayout->addWidget(p_docHeadingFontLabel, 0, 0);
	glayout->addWidget(p_headingFontSelector, 0, 1);
	glayout->addWidget(p_docBodyFontLabel, 1, 0);
	glayout->addWidget(p_textFontSelector, 1, 1);
	ui->fontGroupBox->setLayout(glayout);
}

void XrayQuickReportGeneratorWidget::writeSettings(QSettings& _settings)
{
	_settings.beginGroup("XQuickReportGenerator");
	_settings.setValue("Input/IniFile", ui->projLineEdit->text());
	_settings.setValue("Input/HtmlFile", ui->htmlLineEdit->text());
	_settings.setValue("Input/SignatureFile", ui->signatureLineEdit->text());

	_settings.value("Font/HeadingFont", p_headingFontSelector->getFont());
	_settings.value("Font/BodyFont", p_textFontSelector->getFont());
	_settings.value("Font/HeadingColor", p_headingFontSelector->getColor());
	_settings.value("Font/BodyColor", p_textFontSelector->getColor());

	_settings.endGroup();
}
void XrayQuickReportGeneratorWidget::readSettings(QSettings& _settings)
{
	_settings.beginGroup("XQuickReportGenerator");
	ui->projLineEdit->setText(_settings.value("Input/IniFile").toString());
	ui->htmlLineEdit->setText(_settings.value("Input/HtmlFile").toString());
	ui->signatureLineEdit->setText(_settings.value("Input/SignatureFile").toString());

	auto headingFont = QFont("Arial");
	headingFont.setPointSize(12);
	headingFont.setBold(true);
	headingFont.setUnderline(true);

	auto textFont = QFont("Arial");
	textFont.setPointSize(12);

	p_headingFontSelector->setFont(_settings.value("Font/HeadingFont", headingFont).value<QFont>());
	p_textFontSelector->setFont(_settings.value("Font/BodyFont", textFont).value<QFont>());
	p_headingFontSelector->setColor(_settings.value("Font/HeadingColor", QColor(0, 0, 0)).value<QColor>());
	p_textFontSelector->setColor(_settings.value("Font/BodyColor", QColor(0, 0, 0)).value<QColor>());

	_settings.endGroup();
}

void XrayQuickReportGeneratorWidget::enableDemoVersion(bool _b)
{
	m_isDemo = _b;
}

void XrayQuickReportGeneratorWidget::browseIniFile()
{
	auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayQuickReportGeneratorWidget", "Select File"), ui->projLineEdit->text(), "INI File (*.ini)", nullptr);
	if (fileName.isEmpty())
		return;

	ui->projLineEdit->setText(fileName);

	XrayGlobal::setLastFileOpenPath(fileName);
}
void XrayQuickReportGeneratorWidget::browseHtml()
{
	auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayQuickReportGeneratorWidget", "Select File"), ui->htmlLineEdit->text(), "HTML File (*.html)", nullptr);
	if (fileName.isEmpty())
		return;

	ui->htmlLineEdit->setText(fileName);

	XrayGlobal::setLastFileOpenPath(fileName);
}
void XrayQuickReportGeneratorWidget::browseSignature()
{
	auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayQuickReportGeneratorWidget", "Select File"), ui->signatureLineEdit->text(), XrayGlobal::getAllSupportedImageExtensions(QApplication::translate("XrayQuickReportGeneratorWidget", "Image File")), nullptr);
	if (fileName.isEmpty())
		return;

	ui->signatureLineEdit->setText(fileName);

	XrayGlobal::setLastFileOpenPath(fileName);
}

void XrayQuickReportGeneratorWidget::browseImages()
{
	auto fileNames = QFileDialog::getOpenFileNames(this, QApplication::translate("XrayQuickReportGeneratorWidget", "Select Files"), XrayGlobal::getLastFileOpenPath(), XrayGlobal::getAllSupportedImageExtensions(QApplication::translate("XrayQuickReportGeneratorWidget", "Image Files")), nullptr);
	if (fileNames.isEmpty())
		return;

	// no duplicates are allowed.
	QSet<QString> files;
	files.reserve(fileNames.size() + ui->listWidget->count());

	for (auto i = 0; i < ui->listWidget->count(); i++)
		files.insert(ui->listWidget->item(i)->text());

	for (const auto& i : fileNames)
		files.insert(i);

	ui->listWidget->clear();
	ui->listWidget->addItems(files.toList());
	ui->listWidget->setDragDropMode(QAbstractItemView::InternalMove);

	XrayGlobal::setLastFileOpenPath(fileNames);
}

static void addImageElement(const QString& _imagePath, KDReports::TableElement& _tableElement, int _row, int _col, int _wPercent, int _hPercent, const QColor& headerColor, const QColor& headingColor, const QColor& bodyColor, const QFont& bodyFont)
{
	QImage img(_imagePath);

	auto& tableCell = _tableElement.cell(_row, _col);
	tableCell.setBackground(headerColor);

	// add image
	KDReports::ImageElement imgElement(img);
	if (_wPercent > 0)
		imgElement.setWidth(_wPercent, KDReports::Percent);
	if (_hPercent > 0)
		imgElement.setHeight(_hPercent, KDReports::Percent);

	tableCell.addElement(imgElement, Qt::AlignCenter);
}

void XrayQuickReportGeneratorWidget::generate(KDReports::Report& report)
{
	auto headingFont = p_headingFontSelector->getFont();
	auto bodyFont = p_textFontSelector->getFont();
	auto headingColor = p_headingFontSelector->getColor();
	auto bodyColor = p_textFontSelector->getColor();

	auto docTitle = QApplication::translate("XrayQuickReportGeneratorWidget", "Inspection Report");

	auto projectName = m_filesReader.value("Project Name");
	auto projectCreatedDate = m_filesReader.value("Project CreatedDate");
	auto projectComponent = m_filesReader.value("Project Component");
	auto projectQuantity = m_filesReader.value("Project Quantity");

	auto docName = "Ins_Report_" + XrayGlobal::getCurrentAppLanguage() + '_' + projectName;

	auto customerName = m_filesReader.value("Customer Name");
	auto customerAddress1 = m_filesReader.value("Customer Address1");
	auto customerAddress2 = m_filesReader.value("Customer Address2");
	auto customerCountry = m_filesReader.value("Customer Country");

	auto customerContactPersonName = m_filesReader.value("CustomerContactPerson Name");
	auto customerContactPersonEmail = m_filesReader.value("CustomerContactPerson Email");
	auto customerContactPersonPhone = m_filesReader.value("CustomerContactPerson Phone");

	auto projectContactTitle = QApplication::translate("XrayQuickReportGeneratorWidget", "Contact Person");
	auto projectContactPersonName = m_filesReader.value("Project ContactPerson");
	auto projectContactPersonEmail = QString("gerd-hendrik.greiwe@xray-lab.com");
	auto projectContactPersonPhone = QString("+49 (0) 173 6567366");

	auto investigationTitleText = QApplication::translate("XrayQuickReportGeneratorWidget", "Investigation Analysis");
	auto investigationText = QString::fromLocal8Bit("Maßhaltigkeit der Diagnosebuchsen soll im Vergleich mit dem CAD-Modell durch einen Soll-Ist-Vergleich untersucht werden. Diese Untersuchung wird in drei unterschiedlichen Ausrichtungen an das CAD-Modell durchgeführt.  Dazu wird jeweils ein lokales Bestfit zur Ausrichtung herangezogen.");
	
	auto equipmentTitleText = QApplication::translate("XrayQuickReportGeneratorWidget", "Equipment");
	auto equipmentText = QString::fromUtf8(
		"Der gesamte Bereich ist ESD-geschützt auch die Ausrüstung und die Bekleidung."
		"GE Micro focus X-ray Maschine mit speziellem digitalen Detektor."
		"Maximale Leistung ist 500W bei 300kV."
		"Spektrum der Untersuchung: 120kV/100µA. Dies ist eine für alle Teile zerstörungsfreie Untersuchung auf Grund der geringen Strahlendosis.");

	auto analysisParaTitleText = QApplication::translate("XrayQuickReportGeneratorWidget", "Analysis Parameter");
	auto analysisParaText = QApplication::translate("XrayQuickReportGeneratorWidget", "Analysis Parameter text");

	auto resultTitleText = QApplication::translate("XrayQuickReportGeneratorWidget", "Result");
	auto resultText = QString::fromUtf8("Die Abweichungen des Bauteils zum CAD-Modell beträgt für 90 % der Oberfläche weniger als ±XX1 mm und für 98 % weniger als ±XX2 mm. Im Folgenden werden die Ergebnisse aus unterschiedlichen Ansichten dargestellt.");

	auto regardsTitleText = QApplication::translate("XrayQuickReportGeneratorWidget", "Result");
	auto regardsText = QString::fromUtf8(
		"Alle Ergebnisse der X-ray Untersuchung erhalten Sie auf einem Datentraeger. Für Rückfragen stehe ich Ihnen gerne zur Verfügung."
		"\n"
		"\n"
		"\n"
		"\n"
		"Mit freundlichen Grüßen / With best regards,");


	report.setDocumentName("Inspection_Report_" + projectName);
	report.setDefaultFont(bodyFont);

	if (XrayLicenseGlobals::isDemoVersion())
		report.setWatermarkPixmap(QPixmap(":/res/images/xray-lab_watermark.png"));

	report.setHeaderBodySpacing(1); // mm
	report.setFooterBodySpacing(1); // mm

	report.setFirstPageNumber(1);
	auto& header = report.header(/*KDReports::FirstPage*/);	// not just for first page but all.

	KDReports::TableElement tableElement;
	tableElement.setHeaderRowCount(0);
	tableElement.setHeaderColumnCount(0);
	tableElement.setBorder(0);
	tableElement.setPadding(1);
	tableElement.setWidth(100, KDReports::Percent);
	QColor headerColor(255, 255, 255, 255);
	auto& titleCell = tableElement.cell(0, 0);
	titleCell.setBackground(headerColor);
	auto& logoCell = tableElement.cell(0, 2);
	logoCell.setBackground(headerColor);

	// doc title
	if (!docTitle.isEmpty())
	{
		KDReports::TextElement titleElement;
		titleElement << docTitle + "\n";
		titleElement.setBold(true);
		titleElement.setPointSize(20);
		titleElement.setTextColor(Qt::black);
		titleCell.addElement(titleElement, Qt::AlignLeft);
	}

	// header contact person
	if (!projectContactPersonName.isEmpty())
	{
		KDReports::TextElement contactElement;
		contactElement << projectContactTitle + "\n";
		contactElement.setBold(true);
		contactElement.setPointSize(8);
		contactElement.setTextColor(Qt::black);
		contactElement.setItalic(true);
		titleCell.addElement(contactElement);

		KDReports::TextElement contactInfoElement;
		contactInfoElement << QApplication::translate("XrayQuickReportGeneratorWidget", "Name:") + " " + projectContactPersonName + "\n";
		contactInfoElement << QApplication::translate("XrayQuickReportGeneratorWidget", "Phone:") + " " + projectContactPersonEmail + "\n";
		contactInfoElement << QApplication::translate("XrayQuickReportGeneratorWidget", "Email:") + " " + projectContactPersonPhone;
		contactInfoElement.setBold(true);
		contactInfoElement.setPointSize(8);
		contactInfoElement.setTextColor(Qt::black);
		contactInfoElement.setItalic(false);
		titleCell.addInlineElement(contactInfoElement);
	}

	QPixmap kdab(":/res/images/xray-lab_logo.png");
	KDReports::ImageElement imageElement(kdab);
	imageElement.setWidth(30, KDReports::Percent);
	logoCell.addElement(imageElement, Qt::AlignRight);

	header.addElement(tableElement);

	KDReports::HLineElement line;
	line.setThickness(1);
	line.setMargin(1);
	header.addInlineElement(line);

	if (!projectName.isEmpty())
	{
		KDReports::TextElement docInfoElement;
		docInfoElement << QApplication::translate("XrayQuickReportGeneratorWidget", "Order Number:") + QString("\t") + projectName + "\n";
		docInfoElement << QApplication::translate("XrayQuickReportGeneratorWidget", "Date:") + QString("\t") + projectCreatedDate + "\n";
		docInfoElement << QApplication::translate("XrayQuickReportGeneratorWidget", "Customer Name:") + QString("\t") + customerName + "\n";
		docInfoElement << QApplication::translate("XrayQuickReportGeneratorWidget", "Contact Person:") + QString("\t") + customerContactPersonName + "\n";
		docInfoElement << QApplication::translate("XrayQuickReportGeneratorWidget", "Component:") + QString("\t") + projectComponent + "\n";
		docInfoElement << QApplication::translate("XrayQuickReportGeneratorWidget", "Quantity:") + QString("\t") + projectQuantity + "\n";
		docInfoElement.setFont(bodyFont);
		docInfoElement.setTextColor(bodyColor);

		// spacing of a tab (used in above text), it perfectly align the tabbed text.
		QList<QTextOption::Tab> tabs;
		QTextOption::Tab tab;
		tab.position = 50; // in mm
		tab.type = QTextOption::LeftTab;
		tabs.append(tab);
		report.setTabPositions(tabs);

		report.addElement(docInfoElement, Qt::AlignLeft);
	}

	if (!investigationText.isEmpty())
	{
		KDReports::TextElement heading;
		heading << investigationTitleText + "\n";
		heading.setFont(headingFont);
		heading.setTextColor(headingColor);
		report.addElement(heading);

		KDReports::TextElement body;
		body << investigationText + "\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addInlineElement(body);
	}

	if (!equipmentText.isEmpty())
	{
		KDReports::TextElement heading;
		heading << equipmentTitleText + "\n";
		heading.setFont(headingFont);
		heading.setTextColor(headingColor);
		report.addElement(heading);

		KDReports::TextElement body;
		body << equipmentText + "\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addInlineElement(body);
	}

	if (!analysisParaText.isEmpty())
	{
		KDReports::TextElement heading;
		heading << analysisParaTitleText + "\n";
		heading.setFont(headingFont);
		heading.setTextColor(headingColor);
		report.addElement(heading);

		KDReports::TextElement body;
		body << analysisParaText + "\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addInlineElement(body);
	}

	report.addPageBreak();

	if (!resultText.isEmpty())
	{
		KDReports::TextElement heading;
		heading << resultTitleText + "\n";
		heading.setFont(headingFont);
		heading.setTextColor(headingColor);
		report.addElement(heading);

		KDReports::TextElement body;
		body << resultText + "\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addInlineElement(body);
	}
	
	auto imageFiles = getAllImages();
	if (!imageFiles.empty())
	{
		auto imagesPerPage = 1;
		if (imagesPerPage == 1)		// 1 image per page
		{
			auto counter = 0;
			auto firstTime = true;
			for (const auto& filename : imageFiles)
			{
				KDReports::TableElement tableElement;
				tableElement.setHeaderRowCount(0);
				tableElement.setHeaderColumnCount(0);
				tableElement.setBorder(0);
				tableElement.setPadding(1);
				tableElement.setWidth(100, KDReports::Percent);
				QColor headerColor(255, 255, 255, 255);

				if (firstTime && !resultText.isEmpty())
				{
					auto space = bodyFont.pointSize() * 2;
					if (space > 50)
						space = 50;

					addImageElement(filename, tableElement, 0, 0, 100 - space, 0, headerColor, headingColor, bodyColor, bodyFont);
					firstTime = false;
				}
				else
				{
					addImageElement(filename, tableElement, 0, 0, 100, 0, headerColor, headingColor, bodyColor, bodyFont);
				}

				// add caption
				auto caption = m_filesReader.getImageCaption(counter++);
				if (!caption.isEmpty())
				{
					auto& tableCell = tableElement.cell(0, 0);
					tableCell.setBackground(headerColor);
					KDReports::TextElement body;
					body << caption;
					body.setFont(bodyFont);
					body.setTextColor(bodyColor);
					tableCell.addElement(body, Qt::AlignLeft);
				}

				report.addElement(tableElement, Qt::AlignLeft);
				report.addPageBreak();
			}
		}
	}

	report.addPageBreak();

	if (!regardsText.isEmpty())
	{
		KDReports::TextElement body;
		body << regardsText + "\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addElement(body);
	}

	auto file = ui->signatureLineEdit->text();
	if (QFile::exists(file))
	{
		QImage img(file);
		KDReports::ImageElement imgElement(img.scaled(QSize(200, 150), Qt::AspectRatioMode::KeepAspectRatio, Qt::SmoothTransformation));
		report.addElement(imgElement, Qt::AlignLeft);

		KDReports::TextElement body;
		body << "\n\n[" + projectContactPersonName + "]\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addInlineElement(body);
	}

	// add footer with contact information:
	auto& footer = report.footer();
	footer.addElement(line);

	auto companyName = QString("XRAY-LAB GmbH & Co. KG");
	auto companyLocation = QString("Theodor-Schweitzer-Str. 1+3, 75447 Sternenfels");
	auto companyPhone = QString("+49 (0) 7045/20 445-0");
	auto companyFax = QString("+49 (0) 7046/8808-20");

	KDReports::TextElement footerInfoElement;
	footerInfoElement << "File: " + docName + "\n";
	auto companyname = companyName + "     |     " + companyLocation + "     |     " + "Phone: " + companyPhone + "     |     " + "Fax: " + companyFax;
	footerInfoElement << companyname + "\n\n";
	footerInfoElement.setBold(false);
	footerInfoElement.setPointSize(7);
	footerInfoElement.setTextColor(Qt::black);
	footerInfoElement.setItalic(false);
	footer.addElement(footerInfoElement, Qt::AlignLeft);

	KDReports::TextElement ftab("\tPage: ");
	ftab.setPointSize(7);
	KDReports::TextElement fslash(" / ");
	fslash.setPointSize(7);
	KDReports::TextElement date(tr(", Date: "));
	date.setPointSize(7);
	KDReports::TextElement time(tr(", Time: "));
	time.setPointSize(7);
	footer.addElement(ftab, Qt::AlignRight);
	footer.addVariable(KDReports::PageNumber);
	footer.addInlineElement(fslash);
	footer.addVariable(KDReports::PageCount);
	footer.addInlineElement(date);
	footer.addVariable(KDReports::TextDate);
	footer.addInlineElement(time);
	footer.addVariable(KDReports::TextTime);

	report.setOrientation(QPrinter::Orientation::Landscape);
	report.setPageSize(QPrinter::PageSize::A4);
}

QStringList XrayQuickReportGeneratorWidget::getAllImages()
{
	QStringList imageFiles;
	imageFiles.reserve(ui->listWidget->count() + m_filesReader.images().size());
	for (auto i : m_filesReader.images())
		imageFiles.append(i);
	for (auto i = 0; i < ui->listWidget->count(); i++)
		imageFiles.append(ui->listWidget->item(i)->text());
	return imageFiles;
}

bool XrayQuickReportGeneratorWidget::parseFiles()
{
	auto iniFile = ui->projLineEdit->text();
	auto htmlFile = ui->htmlLineEdit->text();

	if (!QFile::exists(iniFile) || !QFile::exists(htmlFile))
	{
		emit statusBarMessage(QApplication::translate("XrayQuickReportGeneratorWidget", "File doesn't exists!"), 15000);
		return false;
	}

	m_filesReader.clear();

	if (m_filesReader.parseHtmlFile(htmlFile) < 1)
	{
		emit statusBarMessage(QApplication::translate("XrayQuickReportGeneratorWidget", "Couldn't open HTML file!"), 15000);
		return false;
	}

	if (!m_filesReader.parseIniFile(iniFile))
	{
		emit statusBarMessage(QApplication::translate("XrayQuickReportGeneratorWidget", "Couldn't open INI file!"), 15000);
		return false;
	}

	m_filesReader.print();

	return true;
}

void XrayQuickReportGeneratorWidget::preview()
{
	if (!parseFiles())
		return;

	KDReports::Report report;
	generate(report);

	KDReports::PreviewDialog preview(&report);
	preview.setDefaultSaveDirectory(XrayGlobal::getLastFileOpenPath());
	if (preview.exec())
	{
		switch (preview.result())
		{
		case KDReports::PreviewDialog::SavedSuccessfully:
			QMessageBox::information(nullptr, QString(QApplication::translate("XrayQuickReportGeneratorWidget", "Report saved")), QString(QApplication::translate("XrayQuickReportGeneratorWidget", "Success saving to %1")).arg(preview.savedFileName()));
			break;
		case KDReports::PreviewDialog::SaveError:
			QMessageBox::information(nullptr, QString(QApplication::translate("XrayQuickReportGeneratorWidget", "Error")), QString(QApplication::translate("XrayQuickReportGeneratorWidget", "Error while saving to %1")).arg(preview.savedFileName()));
			break;
		default:
			break;
		}
	}
}

void XrayQuickReportGeneratorWidget::save()
{
	if (!parseFiles())
		return;

	auto fileName = QFileDialog::getSaveFileName(this, QApplication::translate("XrayQuickReportGeneratorWidget", "Save File As..."), XrayGlobal::getLastFileOpenPath() + '/' + "Ins_Report_" + ".pdf", tr("All Supported Files (*.pdf *.html *.png *.jpg *.bmp)"), nullptr);
	if (fileName.isEmpty())
		return;

	KDReports::Report report;
	generate(report);

	if (fileName.endsWith(".jpg", Qt::CaseInsensitive) && report.numberOfPages() == 1)
		report.exportToImage(QSize(1920, 1080), fileName, "JPG");
	else if (fileName.endsWith(".png", Qt::CaseInsensitive) && report.numberOfPages() == 1)
		report.exportToImage(QSize(1920, 1080), fileName, "PNG");
	else if (fileName.endsWith(".bmp", Qt::CaseInsensitive) && report.numberOfPages() == 1)
		report.exportToImage(QSize(1920, 1080), fileName, "BMP");
	else if (fileName.endsWith(".html", Qt::CaseInsensitive))
		report.exportToHtml(fileName);
	else if (fileName.endsWith(".pdf", Qt::CaseInsensitive))
		report.exportToFile(fileName);

	XrayGlobal::setLastFileOpenPath(fileName);
}