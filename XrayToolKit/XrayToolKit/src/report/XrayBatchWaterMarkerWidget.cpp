﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/015
** filename: 	XrayBatchWaterMarkerWidget.cpp
** file base:	XrayBatchWaterMarkerWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		classes that provide wizard pages for the reporting widget.
****************************************************************************/

#include "XrayBatchWaterMarkerWidget.h"
#include "ui_XrayBatchWaterMarkerWidget.h"

#include "XrayGlobal.h"
#include "XrayCVUtil.h"
#include "XrayQtUtil.h"

#include <QApplication>
#include <QFileDialog>
#include <QInputDialog>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayBatchWaterMarkerWidget::XrayBatchWaterMarkerWidget(QWidget* parent) :
	QWidget(parent),
	ui(new Ui::XrayBatchWaterMarkerWidget),
	m_isDemo(true)
{
	ui->setupUi(this);
	setWindowTitle("XWatermark Pro");

	connect(ui->wmBrowseBtn, &QPushButton::clicked, this, &XrayBatchWaterMarkerWidget::browseWatermark);
	connect(ui->imgsBrowseBtn, &QPushButton::clicked, this, &XrayBatchWaterMarkerWidget::browseImages);
	connect(ui->saveBrowseBtn, &QPushButton::clicked, this, &XrayBatchWaterMarkerWidget::browseSave);
	connect(ui->opacitySlider, &QSlider::valueChanged, this, &XrayBatchWaterMarkerWidget::preview);
	connect(ui->rgbOnGrayCBox, &QCheckBox::toggled, this, &XrayBatchWaterMarkerWidget::rgbOnGray);

	auto apply = ui->buttonBox->button(QDialogButtonBox::Apply);
	connect(apply, &QPushButton::clicked, this, &XrayBatchWaterMarkerWidget::apply);
}

void XrayBatchWaterMarkerWidget::writeSettings(QSettings& _settings)
{
	_settings.beginGroup("XWatermarkPro");

	_settings.setValue("Input/WaterMarkFile", ui->wmLineEdit->text());
	_settings.setValue("Input/Opacity", ui->opacitySlider->value());
	_settings.setValue("Input/RGBonGray", ui->rgbOnGrayCBox->isChecked());
	_settings.setValue("Output/SaveDir", ui->saveLineEdit->text());

	_settings.endGroup();
}
void XrayBatchWaterMarkerWidget::readSettings(QSettings& _settings)
{
	_settings.beginGroup("XWatermarkPro");

	ui->wmLineEdit->setText(_settings.value("Input/WaterMarkFile").toString());
	if (ui->wmPaintWidget->load(ui->wmLineEdit->text()))
		ui->wmPaintWidget->fitToViewSize(ui->wmPaintWidget->size());

	ui->opacitySlider->setValue(_settings.value("Input/Opacity", 30).toInt());
	ui->rgbOnGrayCBox->setChecked(_settings.value("Input/RGBonGray", true).toBool());
	ui->saveLineEdit->setText(_settings.value("Output/SaveDir").toString());

	_settings.endGroup();
}

void XrayBatchWaterMarkerWidget::enableDemoVersion(bool _b)
{
	m_isDemo = _b;
}

void XrayBatchWaterMarkerWidget::browseWatermark()
{
	auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayBatchWaterMarkerWidget", "Select File"), XrayGlobal::getLastFileOpenPath(), XrayGlobal::getAllSupportedImageExtensions(QApplication::translate("XrayBatchWaterMarkerWidget", "Image File")), nullptr);
	if (fileName.isEmpty())
		return;

	ui->wmLineEdit->setText(fileName);
	if (ui->wmPaintWidget->load(ui->wmLineEdit->text()))
	{
		ui->wmPaintWidget->fitToViewSize(ui->wmPaintWidget->size());
		preview(ui->opacitySlider->value());
	}

	XrayGlobal::setLastFileOpenPath(fileName);
}

void XrayBatchWaterMarkerWidget::browseImages()
{
	auto fileNames = QFileDialog::getOpenFileNames(this, QApplication::translate("XrayBatchWaterMarkerWidget", "Select Files"), XrayGlobal::getLastFileOpenPath(), XrayGlobal::getAllSupportedImageExtensions(QApplication::translate("XrayBatchWaterMarkerWidget", "Image Files")), nullptr);
	if (fileNames.isEmpty())
		return;

	// no duplicates are allowed.
	QSet<QString> files;
	files.reserve(fileNames.size() + ui->listWidget->count());

	for (auto i = 0; i < ui->listWidget->count(); i++)
		files.insert(ui->listWidget->item(i)->text());

	for (const auto& i : fileNames)
		files.insert(i);

	auto list = files.toList();

	ui->listWidget->clear();
	ui->listWidget->addItems(list);
	ui->listWidget->setDragDropMode(QAbstractItemView::InternalMove);

	if (ui->previewPaintWidget->load(fileNames.last()))
	{
		ui->previewPaintWidget->fitToViewSize(ui->previewPaintWidget->size());
		preview(ui->opacitySlider->value());
	}

	XrayGlobal::setLastFileOpenPath(fileNames);
}

QStringList XrayBatchWaterMarkerWidget::getAllImages()
{
	QStringList imageFiles;
	imageFiles.reserve(ui->listWidget->count());
	for (auto i = 0; i < ui->listWidget->count(); i++)
		imageFiles.append(ui->listWidget->item(i)->text());
	return imageFiles;
}

void XrayBatchWaterMarkerWidget::browseSave()
{
	auto dir = QFileDialog::getExistingDirectory(this, QApplication::translate("XrayBatchWaterMarkerWidget", "Open Directory"), XrayGlobal::getLastFileOpenPath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	if (dir.isEmpty())
		return;
	QDir directory(dir);
	if (!directory.exists())
		return;

	ui->saveLineEdit->setText(directory.path());

	XrayGlobal::setLastFileOpenPath(directory.path());
}

void XrayBatchWaterMarkerWidget::rgbOnGray(bool _b)
{
	Q_UNUSED(_b);
	preview(ui->opacitySlider->value());
}

void XrayBatchWaterMarkerWidget::preview(int _value)
{
	if (ui->previewPaintWidget->getFileName().isEmpty() || ui->wmPaintWidget->getFileName().isEmpty())
		return;

	auto opacity = _value / 99.0;
	auto isRGB = ui->rgbOnGrayCBox->isChecked();

	auto img = ui->previewPaintWidget->getWaterMarkedOriginalImage(ui->wmPaintWidget->getImage(), opacity, isRGB);
	ui->previewPaintWidget->setImage(img);
	ui->previewPaintWidget->update();
}

void XrayBatchWaterMarkerWidget::apply()
{
	if(m_isDemo)
	{
		emit statusBarMessage(QApplication::translate("XrayBatchWaterMarkerWidget", "Demo version doesn't allow to save the images!"), 30000);
		return;
	}

	if (!QFile::exists(ui->wmLineEdit->text()))
	{
		emit statusBarMessage(QApplication::translate("XrayBatchWaterMarkerWidget", "Couldn't save! Watermark image is missing!"), 8000);
		return;
	}

	if (!QFile::exists(ui->saveLineEdit->text()))
	{
		emit statusBarMessage(QApplication::translate("XrayBatchWaterMarkerWidget", "Couldn't save! Output images directory is missing!"), 8000);
		return;
	}

	auto files = getAllImages();
	if (files.isEmpty())
	{
		emit statusBarMessage(QApplication::translate("XrayBatchWaterMarkerWidget", "Couldn't find input images!"), 8000);
		return;
	}

	auto opacity = ui->opacitySlider->value() / 99.0;
	auto isRGB = ui->rgbOnGrayCBox->isChecked();

	QImage temp;
	for (auto i : files)
	{
		if (ui->previewPaintWidget->load(i))
		{
			ui->previewPaintWidget->fitToViewSize(ui->previewPaintWidget->size());

			auto fileName = QFileInfo(i).fileName();
			auto path = ui->saveLineEdit->text() + '/' + fileName;

			temp = ui->previewPaintWidget->getWaterMarkedOriginalImage(ui->wmPaintWidget->getImage(), opacity, isRGB);
			if (temp.save(path))
			{
				emit statusBarMessage(QApplication::translate("XrayBatchWaterMarkerWidget", "Saved file: %1").arg(fileName), 5000);
				qApp->processEvents();
			}
		}
	}

	ui->previewPaintWidget->setImage(temp);
	ui->previewPaintWidget->update();

	emit statusBarMessage(QApplication::translate("XrayBatchWaterMarkerWidget", "%1 images has been processed and saved successfully!").arg(files.size()), 30000);
}