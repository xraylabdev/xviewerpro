﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	XrayReportWizardWindow.cpp
** file base:	XrayReportWizardWindow
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a frame less window for the reporting
				wizard widget.
****************************************************************************/

#include "XrayReportWizardWindow.h"
#include "ui_XrayAboutDialog.h"
#include "XrayGlobal.h"
#include "XrayIconPushButton.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QMenu>
#include <QStyle>
#include <QAction>
#include <QActionGroup>
#include <QWidgetAction>
#include <QDesktopServices>
#include <QMessageBox>
#include <QDebug>

XRAYLAB_USING_NAMESPACE


XrayReportWizardWindow::XrayReportWizardWindow(const QString& _title, QWidget* _parent) :
	XrayFramelessWindowWin32(_title, _parent),
	p_wizardWidget(new XrayReportWizardWidget(this))
{
	// set window icon as the application icon.
	setWindowIcon(QApplication::windowIcon());

	// create top right drop down menu.
	createMenu();

	// set the central widget.
	addCentralWidget(p_wizardWidget);

	// restore window settings
	restoreSettings();
}
XrayReportWizardWindow::~XrayReportWizardWindow()
{
}
void XrayReportWizardWindow::closeEvent(QCloseEvent* _event)
{
	saveSettings();
	_event->accept();
}

QMenu* XrayReportWizardWindow::createMenu()
{
	auto menu = new QMenu;

	p_actionSaveBackup = new QAction(QIcon(":/paint/images/save_blue_icon.png"), QApplication::translate("XrayReportWizardWindow", "Save Current Changes"), this);
	connect(p_actionSaveBackup, &QAction::triggered, [this]() { p_wizardWidget->saveBackup(); });
	menu->addAction(p_actionSaveBackup);
	menu->addSeparator();

	p_actionResetSettings = new QAction(QIcon(":/res/images/settings_blue_icon.png"), QApplication::translate("XrayReportWizardWindow", "Clear All Changes"), this);
	connect(p_actionResetSettings, &QAction::triggered, [this]()
	{
		if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayReportWizardWindow", "Are you sure you want to clear all changes?"), QMessageBox::Yes | QMessageBox::No).exec())
		{
			QSettings settings;
			p_wizardWidget->clearSettings();
			p_wizardWidget->restoreSettings(settings);
		}
	});
	menu->addAction(p_actionResetSettings);
	menu->addSeparator();

	menuButton()->setMenu(menu);
	menuButton()->setVisible(true);
	p_menu = menu;

	return menu;
}

void XrayReportWizardWindow::setEnhancerDirectoriesBoxVisible(bool _b)
{
	p_wizardWidget->setEnhancerDirectoriesBoxVisible(_b);
}
void XrayReportWizardWindow::enableDemoVersion(const QString& _modules, bool _b)
{
	p_wizardWidget->enableDemoVersion(_modules, _b);
}