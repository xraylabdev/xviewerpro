﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/015
** filename: 	XrayReportWizardWidget.cpp
** file base:	XrayReportWizardWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		classes that create a wizard of the report sections.
****************************************************************************/

#include "XrayReportWizardWidget.h"
#include "XrayGlobal.h"
#include "XrayLicenseGlobals.h"
#include "XrayTableModel.h"
#include "XrayXmlSettings.h"
#include "XrayQtUtil.h"

#include <QApplication>
#include <QDesktopServices>
#include <QPushButton>
#include <QMessageBox>
#include <QRegExpValidator>
#include <QDir>
#include <QFileDialog>
#include <QDebug>
#include <QDate>
#include <QTableWidget>
#include <QSpacerItem>

XRAYLAB_USING_NAMESPACE

/*!
	Class that create a wizard for the report generator tool.
*/
XrayReportWizardWidget::XrayReportWizardWidget(QWidget* parent, Qt::WindowFlags flags) :
	XrayWizardWidget(parent, flags)
{
	setWindowTitle("XReport Pro");
	setWindowFlags(windowFlags() | 
		Qt::CustomizeWindowHint |
		Qt::WindowMinimizeButtonHint |
		Qt::WindowMaximizeButtonHint |
		Qt::WindowCloseButtonHint);

	p_findFilesPage = new XrayFindFilesWizardPage;
	p_titlePage = new XrayDocTitleWizardPage;
	p_headerPage = new XrayDocHeaderWizardPage;
	p_footerPage = new XrayDocFooterWizardPage;
	p_clientInfoPage = new XrayClientInfoWizardPage;
	p_investigationPage = new XrayInvestigationWizardPage;
	p_customizedSectionsPage = new XrayCustomizedSectionsWizardPage;
	p_resultsPage = new XrayResultsWizardPage;
	p_paintPage = new XrayPaintWizardPage;
	p_customizedSections2Page = new XrayCustomizedSections2WizardPage;
	p_finishPage = new XrayFinishWizardPage;

	setPage(ReportWizardPage::FindFiles, p_findFilesPage);
	setPage(ReportWizardPage::DocTitle, p_titlePage);
	setPage(ReportWizardPage::DocHeader, p_headerPage);
	setPage(ReportWizardPage::DocFooter, p_footerPage);
	setPage(ReportWizardPage::ClientInfo, p_clientInfoPage);
	setPage(ReportWizardPage::Investigation, p_investigationPage);
	setPage(ReportWizardPage::CustomizedSections, p_customizedSectionsPage);
	setPage(ReportWizardPage::Results, p_resultsPage);
	setPage(ReportWizardPage::Paint, p_paintPage);
	setPage(ReportWizardPage::CustomizedSections2, p_customizedSections2Page);
	setPage(ReportWizardPage::Finish, p_finishPage);

	setStartId(ReportWizardPage::FindFiles);
	setWizardStyle(QWizard::ModernStyle);
	//QPixmap pix(QSize(1000, 100));
	//pix.fill(QColor(53, 53, 53));
	//setPixmap(QWizard::BannerPixmap, pix);
	setPixmap(QWizard::LogoPixmap, QPixmap(":/res/images/xraylab_logo-slogan.png"));
	setTitleFormat(Qt::TextFormat::AutoText);
	connect(this, &XrayReportWizardWidget::currentIdChanged, this, &XrayReportWizardWidget::idChanged);
	
	setOption(HaveHelpButton, true);
	setOption(QWizard::NoCancelButton, true);
	setOption(QWizard::HaveCustomButton1, true);
	setOption(QWizard::HaveCustomButton2, true);
	setButtonText(QWizard::HelpButton, QApplication::translate("XrayReportWizardWidget", "Preview"));
	setButtonText(QWizard::CustomButton1, tr(""));
	setButtonText(QWizard::CustomButton2, tr(""));
	//connect(this, &XrayReportWizardWidget::helpRequested, this, &XrayReportWizardWidget::showHelp);

	// this is how we can rearrange the buttons.
	QList<QWizard::WizardButton> button_layout;
	button_layout << QWizard::HelpButton << QWizard::Stretch << QWizard::CustomButton2 << QWizard::CustomButton1 << QWizard::Stretch << QWizard::BackButton << QWizard::NextButton;
	setButtonLayout(button_layout);

	auto btn = qobject_cast<QPushButton*>(button(QWizard::NextButton));
	btn->setShortcut(Qt::Key_Right);
	//btn->setStyleSheet("QPushButton { Padding: 1px; height: 26px; width: 170px; Border-radius: 13px; Background: rgb(0, 100, 150); Color: #fefefe; } QPushButton:pressed { 	background-color: rgba(0, 100, 150, 50); }QPushButton:hover { 	background-color: rgba(0, 100, 150, 100); 	border: 1px solid rgba(170, 170, 0, 255); } QPushButton:focus { 	border: 1px solid rgba(170, 170, 0, 255); 	font-weight: bold; 	outline: none; }");
	
	btn = qobject_cast<QPushButton*>(button(QWizard::BackButton));
	btn->setShortcut(Qt::Key_Left);
	//btn->setStyleSheet("QPushButton { Padding: 1px; height: 26px; width: 170px; Border-radius: 13px; Background: rgb(0, 100, 150); Color: #fefefe; } QPushButton:pressed { 	background-color: rgba(0, 100, 150, 50); }QPushButton:hover { 	background-color: rgba(0, 100, 150, 100); 	border: 1px solid rgba(170, 170, 0, 255); } QPushButton:focus { 	border: 1px solid rgba(170, 170, 0, 255); 	font-weight: bold; 	outline: none; }");
	
	btn = qobject_cast<QPushButton*>(button(QWizard::HelpButton));
	btn->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_P);
	//btn->setStyleSheet("QPushButton { Padding: 1px; height: 26px; width: 170px; Border-radius: 13px; Background: rgb(21, 143, 21); Color: #fefefe; } QPushButton:pressed { 	background-color: rgba(21, 143, 21, 50); }QPushButton:hover { 	background-color: rgba(21, 143, 21, 100); 	border: 1px solid rgba(170, 170, 0, 255); } QPushButton:focus { 	border: 1px solid rgba(170, 170, 0, 255); 	font-weight: bold; 	outline: none; }");
	connect(btn, &QPushButton::clicked, this, &XrayReportWizardWidget::preview);
	
	btn = qobject_cast<QPushButton*>(button(QWizard::CancelButton));
	//btn->setStyleSheet("QPushButton { Padding: 1px; height: 26px; width: 170px; Border-radius: 13px; Background: rgb(143, 21, 21); Color: #fefefe; } QPushButton:pressed { 	background-color: rgba(143, 21, 21, 50); }QPushButton:hover { 	background-color: rgba(143, 21, 21, 100); 	border: 1px solid rgba(170, 170, 0, 255); } QPushButton:focus { 	border: 1px solid rgba(170, 170, 0, 255); 	font-weight: bold; 	outline: none; }");

	btn = qobject_cast<QPushButton*>(button(QWizard::CustomButton1));
	btn->setIcon(QIcon(":/paint/images/plus_icon.png"));
	btn->setToolTip(QApplication::translate("XrayReportWizardWidget", "Add a new section to the document."));
	//btn->setStyleSheet("QPushButton { Padding: 1px; height: 26px; width: 170px; Border-radius: 13px; Background: rgb(150, 100, 150); Color: #fefefe; } QPushButton:pressed { 	background-color: rgba(150, 100, 150, 50); }QPushButton:hover { 	background-color: rgba(150, 100, 150, 100); 	border: 1px solid rgba(170, 170, 0, 255); } QPushButton:focus { 	border: 1px solid rgba(170, 170, 0, 255); 	font-weight: bold; 	outline: none; }");
	connect(btn, &QPushButton::clicked, [this]() { p_customizedSectionsPage->createSection(); });
	auto pal = btn->palette();
	pal.setColor(QPalette::Button, QColor(0, 153, 0));
	btn->setAutoFillBackground(false);	// must be false, true will remove button style and sets the background like a flat button.
	btn->setPalette(pal);
	btn->update();
	btn->hide();

	btn = qobject_cast<QPushButton*>(button(QWizard::CustomButton2));
	btn->setIcon(QIcon(":/res/images/xreportpro_icon_40x40.png"));
	btn->setToolTip(QApplication::translate("XrayReportWizardWidget", "Generate report and save to the disk."));
	btn->setShortcut(Qt::CTRL + Qt::Key_S);
	//btn->setStyleSheet("QPushButton { Padding: 1px; height: 26px; width: 170px; Border-radius: 13px; Background: rgb(150, 100, 150); Color: #fefefe; } QPushButton:pressed { 	background-color: rgba(150, 100, 150, 50); }QPushButton:hover { 	background-color: rgba(150, 100, 150, 100); 	border: 1px solid rgba(170, 170, 0, 255); } QPushButton:focus { 	border: 1px solid rgba(170, 170, 0, 255); 	font-weight: bold; 	outline: none; }");
	connect(btn, &QPushButton::clicked, this, &XrayReportWizardWidget::saveAs);
	pal = btn->palette();
	pal.setColor(QPalette::Button, QColor(0, 153, 0));
	btn->setAutoFillBackground(false);	// must be false, true will remove button style and sets the background like a flat button.
	btn->setPalette(pal);
	btn->update();
	btn->hide();

	connect(p_findFilesPage, &XrayFindFilesWizardPage::clickedBrowse, this, &XrayReportWizardWidget::restoreBackupByBrowse);
	connect(p_findFilesPage, &XrayFindFilesWizardPage::lineEditReturnPressed, this, &XrayReportWizardWidget::restoreBackup);
	connect(p_findFilesPage, &XrayFindFilesWizardPage::firstColumnActivated, this, &XrayReportWizardWidget::restoreBackup);
	//connect(this, &XrayReportWizardWidget::setMessageText, p_findFilesPage, &XrayFindFilesWizardPage::setMessageText);

	setAutoFillBackground(true);
	//setStyleSheet(
	//	"QWizard { background: rgb(53, 53, 53); background-color: rgb(53, 53, 53);}"
	//	"QLineEdit { background: rgb(200, 200, 200); color: rgb(0, 0, 0); border: 1px solid rgba(143, 21, 21, 255); padding: 4px;}"
	//	"QTextEdit { background : rgb(200, 200, 200); color: rgb(0, 0, 0); border: 1px solid rgba(143, 21, 21, 255); }"
	//	"QLabel { color : rgb(255, 255, 255); }"
	//	"QCheckBox { color : rgb(255, 255, 255); }");
		//"QGroupBox { color: rgb(255, 255, 255); font-weight: bold; border: 3px; padding: 20px;}");
		//"QPushButton { padding: 1px; height: 26px; width: 180px; border-radius: 13px; background: rgb(143, 21, 21); color: #fefefe; } QPushButton:pressed { background-color: rgba(21, 143, 21, 50); } QPushButton:hover { background-color: rgba(21, 143, 21, 100); border: 1px solid rgba(170, 170, 0, 255); } QPushButton:focus { border: 1px solid rgba(170, 170, 0, 255); font-weight: bold; outline: none; }");

	resize(QSize(1000, 775));

	setMaximumWidth(1677721);

	QSettings settings(QApplication::organizationName(), windowTitle());
	readSettings(settings);
	p_findFilesPage->readSettings(settings);
}

XrayReportWizardWidget::~XrayReportWizardWidget()
{
	saveBackup();
}
void XrayReportWizardWidget::closeEvent(QCloseEvent *event)
{
	saveBackup();
	XrayWizardWidget::closeEvent(event);
}

void XrayReportWizardWidget::writeSettings(QSettings& settings)
{
	settings.beginGroup("XRAY-LAB-Report");

	settings.setValue("/DocumentPage/docName", p_titlePage->getDocName());
	settings.setValue("/DocumentPage/docTitle", p_titlePage->getDocTitle());
	settings.setValue("/DocumentPage/docType", p_titlePage->getDocType());
	settings.setValue("/DocumentPage/docHeadingFont", p_titlePage->getHeadingFontSelector()->getFont());
	settings.setValue("/DocumentPage/docBodyFont", p_titlePage->getBodyFontSelector()->getFont());
	settings.setValue("/DocumentPage/docHeadingColor", p_titlePage->getHeadingFontSelector()->getColor());
	settings.setValue("/DocumentPage/docBodyColor", p_titlePage->getBodyFontSelector()->getColor());
	settings.setValue("/DocumentPage/paperSize", p_titlePage->getPaperSizeCurrentIndex());
	settings.setValue("/DocumentPage/paperOrientation", p_titlePage->getPaperOrientationCurrentIndex());

	settings.setValue("/HeaderPage/contactPersonName", p_headerPage->getContactPersonName());
	settings.setValue("/HeaderPage/contactPersonPhone", p_headerPage->getContactPersonPhone());
	settings.setValue("/HeaderPage/contactPersonEmail", p_headerPage->getContactPersonEmail());

	settings.setValue("/FooterPage/companyName", p_footerPage->getCompanyName());
	settings.setValue("/FooterPage/companyLocation", p_footerPage->getCompanyLocation());
	settings.setValue("/FooterPage/companyPhone", p_footerPage->getCompanyPhone());
	settings.setValue("/FooterPage/companyFax", p_footerPage->getCompanyFax());
	settings.setValue("/FooterPage/logoFile", p_footerPage->getLogoFile());

	settings.setValue("/ClientInfoPage/orderNumber", p_clientInfoPage->getOrderNumber());
	settings.setValue("/ClientInfoPage/date", p_clientInfoPage->getDate());
	settings.setValue("/ClientInfoPage/clientName", p_clientInfoPage->getClientName());
	settings.setValue("/ClientInfoPage/sample", p_clientInfoPage->getSampleName());
	settings.setValue("/ClientInfoPage/quantity", p_clientInfoPage->getQuantity());

	settings.setValue("/AnalysisPage/analysis", p_investigationPage->getInvestigationAnalysis());
	settings.setValue("/AnalysisPage/problem", p_investigationPage->getProblemStatement());
	settings.setValue("/AnalysisPage/hypothetical_error", p_investigationPage->getHypotheticalFactor());
	settings.setValue("/AnalysisPage/equipment", p_investigationPage->getEquipmentDescription());
	settings.setValue("/AnalysisPage/machines", p_investigationPage->getMachinesList());
	settings.setValue("/AnalysisPage/selectedMachines", p_investigationPage->getSelectedMachinesList());
	settings.setValue("/AnalysisPage/voltages", p_investigationPage->getVoltages());
	settings.setValue("/AnalysisPage/current", p_investigationPage->getCurrent());

	// iterate through all the customized sections and save the heading and text description.
	auto& customizedSections = p_customizedSectionsPage->getSections();
	if (!customizedSections.empty())
	{
		settings.beginGroup("CustomizedSections");
		for (auto i = 0; i < customizedSections.size(); i++)
		{
			auto text = customizedSections[i].textEdit->toPlainText();
			if (!text.isEmpty())
				settings.setValue(customizedSections[i].lineEdit->text(), text);
		}
		settings.endGroup();
	}

	settings.setValue("/ResultsPage/results", p_resultsPage->getResultDescription());
	settings.setValue("/ResultsPage/data_files", p_resultsPage->getDataFiles());
	settings.setValue("/ResultsPage/selected_data_files", p_resultsPage->getSelectedDataFiles());
	settings.setValue("/ResultsPage/data_files_delimiter", p_resultsPage->getCsvDelimeter());
	settings.setValue("/ResultsPage/example_images", p_resultsPage->getImageDescription());

	auto& customizedSections2 = p_customizedSections2Page->getSections();
	if (!customizedSections2.empty())
	{
		settings.beginGroup("CustomizedSections2");
		for (auto i = 0; i < customizedSections2.size(); i++)
		{
			auto text = customizedSections2[i].textEdit->toPlainText();
			if (!text.isEmpty())
				settings.setValue(customizedSections2[i].lineEdit->text(), text);
		}
		settings.endGroup();
	}

	settings.setValue("/FinishPage/regards", p_finishPage->getRegardsDescription());
	settings.setValue("/FinishPage/signatureFile", p_finishPage->getSignatureFile());

	settings.endGroup();

	p_paintPage->getPaintWidget()->writeSettings(settings);
	p_findFilesPage->writeSettings(settings);
}
void XrayReportWizardWidget::readSettings(QSettings& settings)
{
	settings.beginGroup("XRAY-LAB-Report");

	auto variant = settings.value("/DocumentPage/docName", p_titlePage->getDocName()); p_titlePage->p_docName->setText(variant.toString());
	variant = settings.value("/DocumentPage/docTitle", p_titlePage->getDocTitle()); p_titlePage->p_docTitle->setText(variant.toString());
	variant = settings.value("/DocumentPage/docType", p_titlePage->getDocType()); p_titlePage->setDocType(variant.toString());
	variant = settings.value("/DocumentPage/docHeadingFont", p_titlePage->getHeadingFontSelector()->getFont()); p_titlePage->getHeadingFontSelector()->setFont(variant.value<QFont>());
	variant = settings.value("/DocumentPage/docBodyFont", p_titlePage->getBodyFontSelector()->getFont()); p_titlePage->getBodyFontSelector()->setFont(variant.value<QFont>());
	variant = settings.value("/DocumentPage/docHeadingColor", p_titlePage->getHeadingFontSelector()->getColor()); p_titlePage->getHeadingFontSelector()->setColor(variant.value<QColor>());
	variant = settings.value("/DocumentPage/docBodyColor", p_titlePage->getBodyFontSelector()->getColor()); p_titlePage->getBodyFontSelector()->setColor(variant.value<QColor>());
	variant = settings.value("/DocumentPage/paperSize", p_titlePage->getPaperSizeCurrentIndex()); p_titlePage->setPaperSizeCurrentIndex(variant.toInt());
	variant = settings.value("/DocumentPage/paperOrientation", p_titlePage->getPaperOrientationCurrentIndex()); p_titlePage->setPaperOrientationCurrentIndex(variant.toInt());

	variant = settings.value("/HeaderPage/contactPersonName", p_headerPage->getContactPersonName()); p_headerPage->p_contactPersonName->setText(variant.toString());
	variant = settings.value("/HeaderPage/contactPersonPhone", p_headerPage->getContactPersonPhone()); p_headerPage->p_contactPersonPhone->setText(variant.toString());
	variant = settings.value("/HeaderPage/contactPersonEmail", p_headerPage->getContactPersonEmail()); p_headerPage->p_contactPersonEmail->setText(variant.toString());

	variant = settings.value("/FooterPage/companyName", p_footerPage->getCompanyName()); p_footerPage->p_companyName->setText(variant.toString());
	variant = settings.value("/FooterPage/companyLocation", p_footerPage->getCompanyLocation()); p_footerPage->p_companyLocation->setText(variant.toString());
	variant = settings.value("/FooterPage/companyPhone", p_footerPage->getCompanyPhone()); p_footerPage->p_companyPhone->setText(variant.toString());
	variant = settings.value("/FooterPage/companyFax", p_footerPage->getCompanyFax()); p_footerPage->p_companyFax->setText(variant.toString());
	variant = settings.value("/FooterPage/logoFile", p_footerPage->getLogoFile()); p_footerPage->setLogoFile(variant.toString());

	variant = settings.value("/ClientInfoPage/orderNumber", p_clientInfoPage->getOrderNumber()); p_clientInfoPage->p_orderNumber->setText(variant.toString());
	variant = settings.value("/ClientInfoPage/date", p_clientInfoPage->getDate()); p_clientInfoPage->p_date->setText(variant.toString());
	variant = settings.value("/ClientInfoPage/clientName", p_clientInfoPage->getClientName()); p_clientInfoPage->p_clientName->setText(variant.toString());
	variant = settings.value("/ClientInfoPage/sample", p_clientInfoPage->getSampleName()); p_clientInfoPage->p_sampleName->setText(variant.toString());
	variant = settings.value("/ClientInfoPage/quantity", p_clientInfoPage->getQuantity()); p_clientInfoPage->p_quantity->setText(variant.toString());

	variant = settings.value("/AnalysisPage/analysis", p_investigationPage->getInvestigationAnalysis()); p_investigationPage->p_investigation->setText(variant.toString());
	variant = settings.value("/AnalysisPage/problem", p_investigationPage->getProblemStatement()); p_investigationPage->p_problem->setText(variant.toString());
	variant = settings.value("/AnalysisPage/hypothetical_error", p_investigationPage->getHypotheticalFactor()); p_investigationPage->p_hypotheticalfactor->setText(variant.toString());
	variant = settings.value("/AnalysisPage/equipment", p_investigationPage->getEquipmentDescription()); p_investigationPage->p_equipmentDescription->setText(variant.toString());
	variant = settings.value("/AnalysisPage/machines", p_investigationPage->getMachinesList()); p_investigationPage->setMachinesList(variant.toStringList());
	variant = settings.value("/AnalysisPage/selectedMachines", p_investigationPage->getSelectedMachinesList()); p_investigationPage->setSelectedMachinesList(variant.toStringList());
	variant = settings.value("/AnalysisPage/voltages", p_investigationPage->getVoltages()); p_investigationPage->p_voltageUsed->setText(variant.toString() == "0" ? "" : variant.toString());
	variant = settings.value("/AnalysisPage/current", p_investigationPage->getCurrent()); p_investigationPage->p_currentUsed->setText(variant.toString() == "0" ? "" : variant.toString());

	// iterate through all the customized sections and read the heading and text description.
	settings.beginGroup("CustomizedSections");
	auto childKeys = settings.childKeys();
	if (!childKeys.empty())
		p_customizedSectionsPage->resetLayout();
	foreach(const QString& key, childKeys)
	{
		auto pair = p_customizedSectionsPage->createSection();
		pair.lineEdit->setText(key);
		pair.textEdit->setText(settings.value(key).toString());
	}
	settings.endGroup();

	variant = settings.value("/ResultsPage/results"); p_resultsPage->p_resultsDescription->setText(variant.toString());
	variant = settings.value("/ResultsPage/data_files"); p_resultsPage->setDataFiles(variant.toStringList());
	variant = settings.value("/ResultsPage/selected_data_files"); p_resultsPage->setSelectedDataFiles(variant.toStringList());
	variant = settings.value("/ResultsPage/data_files_delimiter"); p_resultsPage->p_csv_delimeter->setText(variant.toString());
	variant = settings.value("/ResultsPage/example_images"); p_resultsPage->p_imageDescription->setText(variant.toString());

	settings.beginGroup("CustomizedSections2");
	childKeys = settings.childKeys();
	if (!childKeys.empty())
		p_customizedSections2Page->resetLayout();
	foreach(const QString& key, childKeys)
	{
		auto pair = p_customizedSections2Page->createSection();
		pair.lineEdit->setText(key);
		pair.textEdit->setText(settings.value(key).toString());
	}
	settings.endGroup();

	variant = settings.value("/FinishPage/regards", p_finishPage->getRegardsDescription()); p_finishPage->setRegardsDescription(variant.toString());
	variant = settings.value("/FinishPage/signatureFile", p_finishPage->getSignatureFile()); p_finishPage->setSignatureFile(variant.toString());

	settings.endGroup();

	p_paintPage->getPaintWidget()->readSettings(settings);
}
void XrayReportWizardWidget::restoreSettings(QSettings& settings)
{
	settings.beginGroup("XRAY-LAB-Report");

	auto variant = settings.value("/DocumentPage/docName"); p_titlePage->p_docName->setText(variant.toString());
	variant = settings.value("/DocumentPage/docTitle"); p_titlePage->p_docTitle->setText(variant.toString());
	variant = settings.value("/DocumentPage/docType"); p_titlePage->setDocType(variant.toString());
	variant = settings.value("/DocumentPage/docHeadingFont", QFont("Arial", 10, QFont::Bold)); p_titlePage->getHeadingFontSelector()->setFont(variant.value<QFont>());
	variant = settings.value("/DocumentPage/docBodyFont", QFont("Arial", 10)); p_titlePage->getBodyFontSelector()->setFont(variant.value<QFont>());
	variant = settings.value("/DocumentPage/docHeadingColor", QColor(0, 0, 0)); p_titlePage->getHeadingFontSelector()->setColor(variant.value<QColor>());
	variant = settings.value("/DocumentPage/docBodyColor", QColor(0, 0, 0)); p_titlePage->getBodyFontSelector()->setColor(variant.value<QColor>());
	variant = settings.value("/DocumentPage/paperSize", 4); p_titlePage->setPaperSizeCurrentIndex(variant.toInt());
	variant = settings.value("/DocumentPage/paperOrientation", 0); p_titlePage->setPaperOrientationCurrentIndex(variant.toInt());


	variant = settings.value("/HeaderPage/contactPersonName"); p_headerPage->p_contactPersonName->setText(variant.toString());
	variant = settings.value("/HeaderPage/contactPersonPhone"); p_headerPage->p_contactPersonPhone->setText(variant.toString());
	variant = settings.value("/HeaderPage/contactPersonEmail"); p_headerPage->p_contactPersonEmail->setText(variant.toString());

	variant = settings.value("/FooterPage/companyName"); p_footerPage->p_companyName->setText(variant.toString());
	variant = settings.value("/FooterPage/companyLocation"); p_footerPage->p_companyLocation->setText(variant.toString());
	variant = settings.value("/FooterPage/companyPhone"); p_footerPage->p_companyPhone->setText(variant.toString());
	variant = settings.value("/FooterPage/companyFax"); p_footerPage->p_companyFax->setText(variant.toString());
	variant = settings.value("/FooterPage/logoFile"); p_footerPage->setLogoFile(variant.toString());

	variant = settings.value("/ClientInfoPage/orderNumber"); p_clientInfoPage->p_orderNumber->setText(variant.toString());
	variant = settings.value("/ClientInfoPage/date"); p_clientInfoPage->p_date->setText(variant.toString());
	variant = settings.value("/ClientInfoPage/clientName"); p_clientInfoPage->p_clientName->setText(variant.toString());
	variant = settings.value("/ClientInfoPage/sample"); p_clientInfoPage->p_sampleName->setText(variant.toString());
	variant = settings.value("/ClientInfoPage/quantity"); p_clientInfoPage->p_quantity->setText(variant.toString());

	variant = settings.value("/AnalysisPage/analysis"); p_investigationPage->p_investigation->setText(variant.toString());
	variant = settings.value("/AnalysisPage/problem"); p_investigationPage->p_problem->setText(variant.toString());
	variant = settings.value("/AnalysisPage/hypothetical_error"); p_investigationPage->p_hypotheticalfactor->setText(variant.toString());
	variant = settings.value("/AnalysisPage/equipment"); p_investigationPage->p_equipmentDescription->setText(variant.toString());
	variant = settings.value("/AnalysisPage/machines", p_investigationPage->getMachinesList()); p_investigationPage->setMachinesList(variant.toStringList());
	variant = settings.value("/AnalysisPage/selectedMachines", p_investigationPage->getSelectedMachinesList()); p_investigationPage->setSelectedMachinesList(variant.toStringList());
	variant = settings.value("/AnalysisPage/voltages"); p_investigationPage->p_voltageUsed->setText(variant.toString() == "0" ? "" : variant.toString());
	variant = settings.value("/AnalysisPage/current"); p_investigationPage->p_currentUsed->setText(variant.toString() == "0" ? "" : variant.toString());

	// iterate through all the customized sections and read the heading and text description.
	p_customizedSectionsPage->resetLayout();
	settings.beginGroup("CustomizedSections");
	auto childKeys = settings.childKeys();
	foreach(const QString& key, childKeys)
	{
		auto pair = p_customizedSectionsPage->createSection();
		pair.lineEdit->setText(key);
		pair.textEdit->setText(settings.value(key).toString());
	}
	settings.endGroup();

	variant = settings.value("/ResultsPage/results"); p_resultsPage->p_resultsDescription->setText(variant.toString());
	variant = settings.value("/ResultsPage/data_files"); p_resultsPage->setDataFiles(variant.toStringList());
	variant = settings.value("/ResultsPage/selected_data_files"); p_resultsPage->setSelectedDataFiles(variant.toStringList());
	variant = settings.value("/ResultsPage/data_files_delimiter", ","); p_resultsPage->p_csv_delimeter->setText(variant.toString());
	variant = settings.value("/ResultsPage/example_images"); p_resultsPage->p_imageDescription->setText(variant.toString());

	p_customizedSections2Page->resetLayout();
	settings.beginGroup("CustomizedSections2");
	childKeys = settings.childKeys();
	foreach(const QString& key, childKeys)
	{
		auto pair = p_customizedSections2Page->createSection();
		pair.lineEdit->setText(key);
		pair.textEdit->setText(settings.value(key).toString());
	}
	settings.endGroup();

	variant = settings.value("/FinishPage/regards"); p_finishPage->setRegardsDescription(variant.toString());
	variant = settings.value("/FinishPage/signatureFile"); p_finishPage->setSignatureFile(variant.toString());

	settings.endGroup();

	p_paintPage->getPaintWidget()->closeAllFiles();	// make sure all previous pictures are closed.
	p_paintPage->getPaintWidget()->readSettings(settings);
}

void XrayReportWizardWidget::clearSettings()
{
	QSettings settings(QApplication::organizationName(), windowTitle());
	settings.clear();
}

void XrayReportWizardWidget::idChanged(int id)
{
	if (id == static_cast<int>(ReportWizardPage::CustomizedSections))
	{
		auto btn = qobject_cast<QPushButton*>(button(QWizard::WizardButton::CustomButton1));
		disconnect(btn, &QPushButton::clicked, nullptr, nullptr);
		connect(btn, &QPushButton::clicked, [this]() { p_customizedSectionsPage->createSection(); });
		if (btn) btn->show();
	}
	else if (id == static_cast<int>(ReportWizardPage::CustomizedSections2))
	{
		auto btn = qobject_cast<QPushButton*>(button(QWizard::WizardButton::CustomButton1));
		disconnect(btn, &QPushButton::clicked, nullptr, nullptr);
		connect(btn, &QPushButton::clicked, [this]() { p_customizedSections2Page->createSection(); });
		if (btn) btn->show();
	}
	else
	{
		auto btn = qobject_cast<QPushButton*>(button(QWizard::WizardButton::CustomButton1));
		if (btn) btn->hide();
	}

	if (id == static_cast<int>(ReportWizardPage::Finish))
	{
		auto btn = qobject_cast<QPushButton*>(button(QWizard::WizardButton::NextButton));
		if (btn) btn->hide();
		btn = qobject_cast<QPushButton*>(button(QWizard::WizardButton::CustomButton2));
		if (btn) btn->show();
	}
	else
	{
		auto btn = qobject_cast<QPushButton*>(button(QWizard::WizardButton::CustomButton2));
		if (btn) btn->hide();
	}
}

void XrayReportWizardWidget::preview()
{
	KDReports::Report report;
	generate(report);
	KDReports::PreviewDialog preview(&report);
	preview.setDefaultSaveDirectory(XrayGlobal::getLastFileOpenPath());
	//preview.resize(1200, 900);
	if (preview.exec())
	{
		switch (preview.result())
		{
		case KDReports::PreviewDialog::SavedSuccessfully:
			QMessageBox::information(0, QString(QApplication::translate("XrayReportWizardWidget", "Report saved")), QString(QApplication::translate("XrayReportWizardWidget", "Success saving to %1")).arg(preview.savedFileName()));
			break;
		case KDReports::PreviewDialog::SaveError:
			QMessageBox::information(0, QString(QApplication::translate("XrayReportWizardWidget", "Error")), QString(QApplication::translate("XrayReportWizardWidget", "Error while saving to %1")).arg(preview.savedFileName()));
			break;
		default:
			break;
		}
	}
}
void XrayReportWizardWidget::saveAs()
{
	static auto filters =
		"PDF (*.pdf);;"
		"HTML (*.html)";

	auto fileName = QFileDialog::getSaveFileName(this, QApplication::translate("XrayReportWizardWidget", "Save File As..."), XrayGlobal::getLastFileOpenPath() + QDir::separator() + p_titlePage->getDocName() + ".pdf", filters, nullptr);
	if (!fileName.isEmpty())
	{
		KDReports::Report report;
		generate(report);

		if (fileName.endsWith(".jpg", Qt::CaseInsensitive) && report.numberOfPages() == 1)
			report.exportToImage(QSize(1920, 1080), fileName, "JPG");
		else if (fileName.endsWith(".png", Qt::CaseInsensitive) && report.numberOfPages() == 1)
			report.exportToImage(QSize(1920, 1080), fileName, "PNG");
		else if (fileName.endsWith(".bmp", Qt::CaseInsensitive) && report.numberOfPages() == 1)
			report.exportToImage(QSize(1920, 1080), fileName, "BMP");
		else if (fileName.endsWith(".html", Qt::CaseInsensitive))
			report.exportToHtml(fileName);
		else if (fileName.endsWith(".pdf", Qt::CaseInsensitive))
			report.exportToFile(fileName);

		QSettings settingsToIniFile(QFileInfo(fileName).absolutePath() + '/' + QFileInfo(fileName).baseName() + ".ini", QSettings::IniFormat);
		settingsToIniFile.clear(); // settings append values to the existing file, so first, clear the previous settings.
		writeSettings(settingsToIniFile);

		XrayGlobal::setLastFileOpenPath(fileName);
	}
}

void XrayReportWizardWidget::saveBackup()
{
	QSettings settingsToRegistry(QApplication::organizationName(), windowTitle());
	writeSettings(settingsToRegistry);
	//QString settingsPath = QFileInfo(settingsToRegistry.fileName()).absolutePath();
	//qDebug() << settingsPath;
	//
	////QSettings settings1(XrayXmlSettings::format, QSettings::UserScope, "XRay-Lab", "ReportGenerator");
	////settings1.setDefaultFormat(XrayXmlSettings::format);
	////settings1.setPath(XrayXmlSettings::format, QSettings::UserScope, QDir::currentPath() + "//settings.xml");
	////QSettings settings1(QApplication::applicationDirPath() + QDir::separator() + "settings.xml", XrayXmlSettings::format);

	//const auto dir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
	//if (QDir().mkpath(dir))
	//{
	//	const auto file = dir + QDir::separator() + p_titlePage->getDocName() + " " + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss_zzz") + ".ini";
	//	QSettings settingsToIniFile(file, QSettings::IniFormat);
	//	writeSettings(settingsToIniFile);
	//}
}
void XrayReportWizardWidget::saveBackupAs()
{
	auto fileName = QFileDialog::getSaveFileName(this, QApplication::translate("XrayReportWizardWidget", "Save File As..."), XrayGlobal::getLastFileOpenPath() + QDir::separator() + p_titlePage->getDocName() + ".ini", "INI File (*.ini)", nullptr);
	if (!fileName.isEmpty())
	{
		QSettings settingsToIniFile(fileName, QSettings::IniFormat);
		settingsToIniFile.clear();
		writeSettings(settingsToIniFile);
	}
}
bool XrayReportWizardWidget::restoreBackup(const QString& _filePath)
{
	if (!_filePath.endsWith(".ini", Qt::CaseInsensitive))
		return false;

	QSettings settings(_filePath, QSettings::IniFormat);
	if (!settings.childGroups().contains("XRAY-LAB-Report"))
	{
		p_findFilesPage->setMessageText(QApplication::translate("XrayReportWizardWidget", "Couldn't restored! Invalid report document."));
		return false;
	}

	restoreSettings(settings);
	p_findFilesPage->setRestoreFilePath(_filePath);
	p_findFilesPage->setMessageText(QApplication::translate("XrayReportWizardWidget", "The selected report has been restored!"));

	return true;
}
void XrayReportWizardWidget::restoreBackupByBrowse()
{
	auto fileName = QFileDialog::getOpenFileName(this, QApplication::translate("XrayReportWizardWidget", "Select File"), XrayGlobal::getLastFileOpenPath(), "INI File (*.ini)", nullptr);
	if (!fileName.isEmpty())
	{
		restoreBackup(fileName);
		XrayGlobal::setLastFileOpenPath(fileName);
	}
}

void XrayReportWizardWidget::showHelp()
{
	static QString lastHelpMessage;

	QString message;

	switch (currentId())
	{
	case ReportWizardPage::DocTitle:
		message = QApplication::translate("XrayReportWizardWidget", "Specify the document name and it's title.");
		break;
	case ReportWizardPage::DocHeader:
		message = QApplication::translate("XrayReportWizardWidget", "Specify the document header information.");
		break;
	case ReportWizardPage::DocFooter:
		message = QApplication::translate("XrayReportWizardWidget", "Specify the document footer information.");
		break;
	case ReportWizardPage::ClientInfo:
		message = QApplication::translate("XrayReportWizardWidget", "Specify the order information.");
		break;
	case ReportWizardPage::Investigation:
		message = QApplication::translate("XrayReportWizardWidget", "Specify the investigation analysis.");
		break;
	case ReportWizardPage::CustomizedSections:
		message = QApplication::translate("XrayReportWizardWidget", "Create new sections in the report by pressing the Create New button.");
		break;
	case ReportWizardPage::Results:
		message = QApplication::translate("XrayReportWizardWidget", "Specify the results, data, and related images.");
		break;
	case ReportWizardPage::Finish:
		message = QApplication::translate("XrayReportWizardWidget", "Preview the report by pressing the Generate button.");
		break;
	default:
		message = QApplication::translate("XrayReportWizardWidget", "This help is likely not to be of any help.");
	}

	if (lastHelpMessage == message)
		message = QApplication::translate("XrayReportWizardWidget", "Couldn't find any help for this page.");

	QMessageBox::information(nullptr, windowTitle(), message);

	lastHelpMessage = message;
}

void XrayReportWizardWidget::setEnhancerDirectoriesBoxVisible(bool _b)
{
	p_paintPage->getPaintWidget()->setEnhancerDirectoriesBoxVisible(_b);
}
void XrayReportWizardWidget::enableDemoVersion(const QString& _modules, bool _b)
{
	p_paintPage->getPaintWidget()->enableDemoVersion(_modules, _b);
	if (_b)
	{
		auto btn = qobject_cast<QPushButton*>(button(QWizard::WizardButton::CustomButton2));	// Save Report As...
		disconnect(btn, &QPushButton::clicked, nullptr, nullptr);
	}
	else
	{
		auto btn = qobject_cast<QPushButton*>(button(QWizard::WizardButton::CustomButton2));
		disconnect(btn, &QPushButton::clicked, nullptr, nullptr);
		connect(btn, &QPushButton::clicked, this, &XrayReportWizardWidget::saveAs);
	}
}

static void addImageElement(const std::tuple<QString, QString, QImage>& _tuple, KDReports::TableElement& _tableElement, int _row, int _col, int _wPercent, int _hPercent, const QColor& headerColor, const QColor& headingColor, const QColor& bodyColor, const QFont& bodyFont)
{
	auto filename = std::get<0>(_tuple);
	auto caption = std::get<1>(_tuple);
	QImage img(std::get<2>(_tuple));

	auto& tableCell = _tableElement.cell(_row, _col);
	tableCell.setBackground(headerColor);

	// add image heading
	KDReports::TextElement heading;
	heading << QFileInfo(filename).baseName();
	heading.setFont(bodyFont);	// use the body font here.
	heading.setTextColor(headingColor);
	tableCell.addElement(heading, Qt::AlignLeft);

	// add image
	KDReports::ImageElement imgElement(img);
	if(_wPercent > 0)
		imgElement.setWidth(_wPercent, KDReports::Percent);
	if (_hPercent > 0)
		imgElement.setHeight(_hPercent, KDReports::Percent);

	tableCell.addElement(imgElement, Qt::AlignCenter);

	// add caption
	if (!caption.isEmpty())
	{
		KDReports::TextElement body;
		body << caption;
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		tableCell.addElement(body, Qt::AlignCenter);
	}
}
void XrayReportWizardWidget::generate(KDReports::Report& report)
{
	auto headingFont = p_titlePage->getHeadingFontSelector()->getFont();
	auto bodyFont = p_titlePage->getBodyFontSelector()->getFont();
	auto headingColor = p_titlePage->getHeadingFontSelector()->getColor();
	auto bodyColor = p_titlePage->getBodyFontSelector()->getColor();

	report.setDocumentName(p_titlePage->getDocName());
	report.setDefaultFont(bodyFont);

	if(XrayLicenseGlobals::isDemoVersion())
		report.setWatermarkPixmap(QPixmap(":/res/images/xray-lab_watermark.png"));

	report.setHeaderBodySpacing(2); // mm
	report.setFooterBodySpacing(2); // mm

	report.setFirstPageNumber(1);
	auto& header = report.header(/*KDReports::FirstPage*/);	// not just for first page but all.

	KDReports::TableElement tableElement;
	tableElement.setHeaderRowCount(0);
	tableElement.setHeaderColumnCount(0);
	tableElement.setBorder(0);
	tableElement.setPadding(1);
	tableElement.setWidth(100, KDReports::Percent);
	QColor headerColor(255, 255, 255, 255);
	auto& titleCell = tableElement.cell(0, 0);
	titleCell.setBackground(headerColor);
	auto& logoCell = tableElement.cell(0, 2);
	logoCell.setBackground(headerColor);

	if (!p_titlePage->getDocTitle().isEmpty())
	{
		KDReports::TextElement titleElement;
		titleElement << p_titlePage->getDocTitle() + "\n";
		titleElement.setBold(true);
		titleElement.setPointSize(20);
		titleElement.setTextColor(Qt::black);
		titleCell.addElement(titleElement, Qt::AlignLeft);
	}

	if (!p_headerPage->getContactPersonName().isEmpty())
	{
		KDReports::TextElement contactElement;
		contactElement << p_headerPage->getGroupBoxTitle() + "\n";
		contactElement.setBold(true);
		contactElement.setPointSize(8);
		contactElement.setTextColor(Qt::black);
		contactElement.setItalic(true);
		titleCell.addElement(contactElement);

		KDReports::TextElement contactInfoElement;
		contactInfoElement << p_headerPage->getContactPersonNameLabel() + " " + p_headerPage->getContactPersonName() + "\n";
		contactInfoElement << p_headerPage->getContactPersonPhoneLabel() + " " + p_headerPage->getContactPersonPhone() + "\n";
		contactInfoElement << p_headerPage->getContactPersonEmailLabel() + " " + p_headerPage->getContactPersonEmail();
		contactInfoElement.setBold(true);
		contactInfoElement.setPointSize(8);
		contactInfoElement.setTextColor(Qt::black);
		contactInfoElement.setItalic(false);
		titleCell.addInlineElement(contactInfoElement);
	}

	QPixmap kdab(p_footerPage->getLogoFile());
	KDReports::ImageElement imageElement(kdab);
	imageElement.setWidth(40, KDReports::Percent);
	logoCell.addElement(imageElement, Qt::AlignRight);

	header.addElement(tableElement);

	KDReports::HLineElement line;
	line.setThickness(1);
	line.setMargin(1);
	header.addInlineElement(line);

	if (!p_clientInfoPage->getOrderNumber().isEmpty())
	{
		KDReports::TextElement docInfoElement;
		docInfoElement << p_clientInfoPage->getOrderNumberLabel() + QString("\t") + p_clientInfoPage->getOrderNumber() + "\n";
		docInfoElement << p_clientInfoPage->getDateLabel() + QString("\t") + p_clientInfoPage->getDate() + "\n";
		docInfoElement << p_clientInfoPage->getClientNameLabel() + QString("\t") + p_clientInfoPage->getClientName() + "\n";
		docInfoElement << p_clientInfoPage->getSampleNameLabel() + QString("\t") + p_clientInfoPage->getSampleName() + "\n";
		docInfoElement << p_clientInfoPage->getQuantityLabel() + QString("\t") + p_clientInfoPage->getQuantity() + "\n";
		docInfoElement.setFont(bodyFont);
		docInfoElement.setTextColor(bodyColor);

		// spacing of a tab (used in above text), it perfectly align the tabbed text.
		QList<QTextOption::Tab> tabs;
		QTextOption::Tab tab;
		tab.position = 50; // in mm
		tab.type = QTextOption::LeftTab;
		tabs.append(tab);
		report.setTabPositions(tabs);

		report.addElement(docInfoElement, Qt::AlignLeft);
	}

	if (!p_investigationPage->getInvestigationAnalysis().isEmpty())
	{
		KDReports::TextElement heading;
		heading << p_investigationPage->getInvestigationAnalysisLabel() + "\n";
		heading.setFont(headingFont);
		heading.setTextColor(headingColor);
		report.addElement(heading);

		KDReports::TextElement body;
		body << p_investigationPage->getInvestigationAnalysis() + "\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addInlineElement(body);
	}

	if (!p_investigationPage->getProblemStatement().isEmpty())
	{
		KDReports::TextElement heading;
		heading << p_investigationPage->getProblemStatementLabel() + "\n";
		heading.setFont(headingFont);
		heading.setTextColor(headingColor);
		report.addElement(heading);

		KDReports::TextElement body;
		body << p_investigationPage->getProblemStatement() + "\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addInlineElement(body);
	}

	if (!p_investigationPage->getHypotheticalFactor().isEmpty())
	{
		KDReports::TextElement heading;
		heading << p_investigationPage->getHypotheticalFactorLabel() + "\n";
		heading.setFont(headingFont);
		heading.setTextColor(headingColor);
		report.addElement(heading);

		KDReports::TextElement body;
		body << p_investigationPage->getHypotheticalFactor() + "\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addInlineElement(body);
	}

	if (!p_investigationPage->getEquipmentDescription().isEmpty())
	{
		KDReports::TextElement heading;
		heading << p_investigationPage->getEquipmentLabel() + "\n";
		heading.setFont(headingFont);
		heading.setTextColor(headingColor);
		report.addElement(heading);

		KDReports::TextElement body;
		body << p_investigationPage->getEquipmentDescription() + "\n";
		body << p_investigationPage->getEquipmentsAsString() + "\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addInlineElement(body);
	}

	// Customized Sections WizardPage
	auto& sections = p_customizedSectionsPage->getSections();
	for (auto& s : sections)
	{
		auto text = s.textEdit->toPlainText();
		if (!text.isEmpty())
		{
			KDReports::TextElement heading;
			heading << s.lineEdit->text() + "\n";
			heading.setFont(headingFont);
			heading.setTextColor(headingColor);
			report.addElement(heading);

			KDReports::TextElement body;
			body << text + "\n";
			body.setFont(bodyFont);
			body.setTextColor(bodyColor);
			report.addInlineElement(body);
		}
	}

	if (!p_resultsPage->getResultDescription().isEmpty())
	{
		KDReports::TextElement heading;
		heading << p_resultsPage->getResultDescriptionLabel() + "\n";
		heading.setFont(headingFont);
		heading.setTextColor(headingColor);
		report.addElement(heading);

		KDReports::TextElement body;
		body << p_resultsPage->getResultDescription() + "\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addInlineElement(body);
	}

	auto resultsDataFiles = p_resultsPage->getSelectedDataFiles();
	foreach (const QString& file, resultsDataFiles)
	{
		QFile f(file);
		if (f.exists())
		{
			XrayTableModel tableModel;
			tableModel.setDataHasVerticalHeaders(false);
			if (tableModel.readCSV(file, p_resultsPage->getCsvDelimeter().at(0).toLatin1()))
			{
				const QColor titleElementColor(240, 240, 240, 255);
				// A text element with the title above a table
				// Note that the background color is not just behind the text (as setBackground() would do),
				// but behind the whole paragraph, up until the right margin of the page.
				//auto str = QFileInfo(file).completeBaseName();
				//if (!str.contains("_nh", Qt::CaseInsensitive))	// if a file name contains _nh which means "No Heading" then no heading of the table will be added.
				//{
					KDReports::TextElement heading(QFileInfo(file).completeBaseName());
					heading.setFont(bodyFont);	// use the body font here.
					heading.setTextColor(headingColor);
					report.addElement(heading, Qt::AlignLeft, titleElementColor);
				//}

				KDReports::AutoTableElement autoTableElement(&tableModel);
				//autoTableElement.setHeaderBackground(QColor(0, 255, 0, 70));
				autoTableElement.setVerticalHeaderVisible(false);
				autoTableElement.setWidth(100, KDReports::Percent);
				report.addElement(autoTableElement);
				report.addElement(KDReports::TextElement("\n"));
			}
		}
	}

	report.addPageBreak();

	auto imageSectionDescription = p_resultsPage->getImageDescription();
	if (!imageSectionDescription.isEmpty())
	{
		KDReports::TextElement heading;
		heading << p_resultsPage->getImageDescriptionLabel() + "\n";
		heading.setFont(headingFont);
		heading.setTextColor(headingColor);
		report.addElement(heading);

		KDReports::TextElement body;
		body << imageSectionDescription + "\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addInlineElement(body);
	}

	auto imageFiles = p_paintPage->getPaintWidget()->getImagesWithCaptionsAndFiles();
	if (!imageFiles.empty())
	{
		auto imagesPerPage = p_paintPage->getPaintWidget()->getNumImagesPerPage();
		if (imagesPerPage == 1)		// 1 image per page
		{
			auto firstTime = true;
			for (const auto& filename : imageFiles)
			{
				KDReports::TableElement tableElement;
				tableElement.setHeaderRowCount(0);
				tableElement.setHeaderColumnCount(0);
				tableElement.setBorder(0);
				tableElement.setPadding(1);
				tableElement.setWidth(100, KDReports::Percent);
				QColor headerColor(255, 255, 255, 255);

				if (firstTime && !imageSectionDescription.isEmpty())
				{
					auto space = bodyFont.pointSize() * 2;
					if (space > 50)
						space = 50;

					if (p_titlePage->getPageOrientation() == QPrinter::Orientation::Landscape)
						addImageElement(filename, tableElement, 0, 0, 0, 80 - space, headerColor, headingColor, bodyColor, bodyFont);
					else
						addImageElement(filename, tableElement, 0, 0, 100 - space, 0, headerColor, headingColor, bodyColor, bodyFont);

					firstTime = false;
				}
				else
				{
					if(p_titlePage->getPageOrientation() == QPrinter::Orientation::Landscape)
						addImageElement(filename, tableElement, 0, 0, 0, 80, headerColor, headingColor, bodyColor, bodyFont);
					else
						addImageElement(filename, tableElement, 0, 0, 100, 0, headerColor, headingColor, bodyColor, bodyFont);
				}

				report.addElement(tableElement, Qt::AlignLeft);
				report.addPageBreak();
			}
		}
		else if (imagesPerPage == 2)		// 2 images per page
		{
			for (auto i = 0; i < imageFiles.size(); i += 2)
			{
				KDReports::TableElement tableElement;
				tableElement.setHeaderRowCount(0);
				tableElement.setHeaderColumnCount(0);
				tableElement.setBorder(0);
				tableElement.setPadding(1);
				tableElement.setWidth(100, KDReports::Percent);
				QColor headerColor(255, 255, 255, 255);

				auto n = i;

				auto tuple = imageFiles.at(n);
				addImageElement(tuple, tableElement, 0, 0, 0, 40, headerColor, headingColor, bodyColor, bodyFont);

				n = i + 1;
				if(n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 1, 0, 0, 40, headerColor, headingColor, bodyColor, bodyFont);
				}

				report.addElement(tableElement, Qt::AlignLeft);
				report.addPageBreak();
			}
		}
		else if (imagesPerPage > 2 && imagesPerPage < 5)		// 4 images per page
		{
			for (auto i = 0; i < imageFiles.size(); i += 4)
			{
				KDReports::TableElement tableElement;
				tableElement.setHeaderRowCount(0);
				tableElement.setHeaderColumnCount(0);
				tableElement.setBorder(0);
				tableElement.setPadding(1);
				tableElement.setWidth(100, KDReports::Percent);
				QColor headerColor(255, 255, 255, 255);

				auto n = i;

				auto tuple = imageFiles.at(n);
				addImageElement(tuple, tableElement, 0, 0, 0, 40, headerColor, headingColor, bodyColor, bodyFont);
				
				n = i + 1;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 0, 1, 0, 40, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 2;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 1, 0, 0, 40, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 3;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 1, 1, 0, 40, headerColor, headingColor, bodyColor, bodyFont);
				}

				report.addElement(tableElement, Qt::AlignLeft);
				report.addPageBreak();
			}
		}
		else if (imagesPerPage > 4 && imagesPerPage < 7)		// 6 images per page
		{
			for (auto i = 0; i < imageFiles.size(); i += 6)
			{
				KDReports::TableElement tableElement;
				tableElement.setHeaderRowCount(0);
				tableElement.setHeaderColumnCount(0);
				tableElement.setBorder(0);
				tableElement.setPadding(1);
				tableElement.setWidth(100, KDReports::Percent);
				QColor headerColor(255, 255, 255, 255);

				auto n = i;

				auto tuple = imageFiles.at(n);
				addImageElement(tuple, tableElement, 0, 0, 0, 25, headerColor, headingColor, bodyColor, bodyFont);

				n = i + 1;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 0, 1, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 2;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 1, 0, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 3;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 1, 1, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 4;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 2, 0, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 5;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 2, 1, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				report.addElement(tableElement, Qt::AlignLeft);
				report.addPageBreak();
			}
		}
		else if (imagesPerPage > 6 && imagesPerPage < 10)		// 9 images per page
		{
			for (auto i = 0; i < imageFiles.size(); i += 9)
			{
				KDReports::TableElement tableElement;
				tableElement.setHeaderRowCount(0);
				tableElement.setHeaderColumnCount(0);
				tableElement.setBorder(0);
				tableElement.setPadding(1);
				tableElement.setWidth(100, KDReports::Percent);
				QColor headerColor(255, 255, 255, 255);

				auto n = i;

				auto tuple = imageFiles.at(n);
				addImageElement(tuple, tableElement, 0, 0, 0, 25, headerColor, headingColor, bodyColor, bodyFont);

				n = i + 1;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 0, 1, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 2;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 0, 2, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 3;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 1, 0, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 4;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 1, 1, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 5;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 1, 2, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 6;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 2, 0, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 7;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 2, 1, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				n = i + 8;
				if (n < imageFiles.size())
				{
					auto tuple = imageFiles.at(n);
					addImageElement(tuple, tableElement, 2, 2, 0, 25, headerColor, headingColor, bodyColor, bodyFont);
				}

				report.addElement(tableElement, Qt::AlignLeft);
				report.addPageBreak();
			}
		}
	}

	// Customized Sections WizardPage
	auto& sections2 = p_customizedSections2Page->getSections();
	for (auto& s : sections2)
	{
		auto text = s.textEdit->toPlainText();
		if (!text.isEmpty())
		{
			KDReports::TextElement heading;
			heading << s.lineEdit->text() + "\n";
			heading.setFont(headingFont);
			heading.setTextColor(headingColor);
			report.addElement(heading);

			KDReports::TextElement body;
			body << text + "\n";
			body.setFont(bodyFont);
			body.setTextColor(bodyColor);
			report.addInlineElement(body);
		}
	}

	if (!p_finishPage->getRegardsDescription().isEmpty())
	{
		KDReports::TextElement body;
		body << p_finishPage->getRegardsDescription() + "\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addElement(body);
	}

	auto file = p_finishPage->getSignatureFile();
	if (QFile::exists(file))
	{
		QImage img(file);
		KDReports::ImageElement imgElement(img);
		report.addElement(imgElement, Qt::AlignLeft);

		KDReports::TextElement body;
		body << "\n\n[" + p_headerPage->getContactPersonName() + "]\n";
		body.setFont(bodyFont);
		body.setTextColor(bodyColor);
		report.addInlineElement(body);
	}

	// add footer with contact information:
	auto& footer = report.footer();
	footer.addElement(line);

	KDReports::TextElement footerInfoElement;
	footerInfoElement << "File: " + p_titlePage->getDocName() + "\n";
	auto companyname = p_footerPage->getCompanyName() + "     |     " + p_footerPage->getCompanyLocation() + "     |     " + "Phone: " + p_footerPage->getCompanyPhone() + "     |     " + "Fax: " + p_footerPage->getCompanyFax();
	footerInfoElement << companyname + "\n\n";
	footerInfoElement.setBold(false);
	footerInfoElement.setPointSize(7);
	footerInfoElement.setTextColor(Qt::black);
	footerInfoElement.setItalic(false);
	footer.addElement(footerInfoElement, Qt::AlignLeft);

	KDReports::TextElement ftab("\tPage: ");
	ftab.setPointSize(7);
	KDReports::TextElement fslash(" / ");
	fslash.setPointSize(7);
	KDReports::TextElement date(tr(", Date: "));
	date.setPointSize(7);
	KDReports::TextElement time(tr(", Time: "));
	time.setPointSize(7);
	footer.addElement(ftab, Qt::AlignRight);
	footer.addVariable(KDReports::PageNumber);
	footer.addInlineElement(fslash);
	footer.addVariable(KDReports::PageCount);
	footer.addInlineElement(date);
	footer.addVariable(KDReports::TextDate);
	footer.addInlineElement(time);
	footer.addVariable(KDReports::TextTime);

	report.setOrientation(p_titlePage->getPageOrientation());
	report.setPageSize(p_titlePage->getPageSize());
}

void XrayReportWizardWidget::keyPressEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Escape)
	{
		// handle it
	}
	else
	{
		return XrayWizardWidget::keyPressEvent(event);
	}
}