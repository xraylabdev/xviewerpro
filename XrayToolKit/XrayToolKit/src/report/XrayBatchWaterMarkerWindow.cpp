﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/11/22
** filename: 	XrayBatchWaterMarkerWindow.cpp
** file base:	XrayBatchWaterMarkerWindow
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the main window for image stitcher.
****************************************************************************/

#include "XrayBatchWaterMarkerWindow.h"
#include "XrayAboutDialog.h"
#include "XrayQApplication.h"
#include "XrayIconPushButton.h"
#include "XrayGlobal.h"
#include "XrayLicenseGlobals.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QDesktopServices>
#include <QMessageBox>
#include <QSettings>
#include <QDateTime>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayBatchWaterMarkerWindow::XrayBatchWaterMarkerWindow(const QString& _title, QWidget* _parent) :
	XrayFramelessWindowWin32(_title, _parent),
	p_widget(new XrayBatchWaterMarkerWidget(this))
{
	setWindowIcon(QApplication::windowIcon());

	p_menu = createMenu();
	menuButton()->setMenu(p_menu);
	menuButton()->setVisible(false);

	addCentralWidget(p_widget);

	p_statusBar = new XrayStatusBar(this);
	p_statusBar->setSizeGripEnabled(false);
	p_statusBar->setContentsMargins(5, 0, 5, 0);
	connect(p_widget, &XrayBatchWaterMarkerWidget::statusBarMessage, [this](const QString& text, int timeout)
	{
		p_statusBar->showMessage(text, timeout);
	});
	setStatusBar(p_statusBar);

	p_widget->readSettings(*p_settings);
	restoreSettings();
}
XrayBatchWaterMarkerWindow::~XrayBatchWaterMarkerWindow()
{
	p_widget->deleteLater();
}

void XrayBatchWaterMarkerWindow::enableDemoVersion(const QString& _modules, bool b)
{
	if (_modules.contains("XWATERMARK PRO", Qt::CaseInsensitive))
		p_widget->enableDemoVersion(b);
	else
		p_widget->enableDemoVersion(true);
}

void XrayBatchWaterMarkerWindow::closeEvent(QCloseEvent* _event)
{
	p_widget->writeSettings(*p_settings);
	saveSettings();

	_event->accept();
}

QMenu* XrayBatchWaterMarkerWindow::createMenu()
{
	auto menu = new QMenu;

	p_actionViewHelp = new QAction(QIcon(":/res/images/help_blue_icon.png"), QApplication::translate("XrayBatchWaterMarkerWindow", "View Help"), this);
	connect(p_actionViewHelp, &QAction::triggered, [this]() {});
	menu->addAction(p_actionViewHelp);

	p_actionWebsite = new QAction(QIcon(":/res/images/website_blue_icon.png"), QApplication::translate("XrayBatchWaterMarkerWindow", "Website"), this);
	connect(p_actionWebsite, &QAction::triggered, [this]() { QDesktopServices::openUrl(QUrl("https://xray-lab.com/")); });
	menu->addAction(p_actionWebsite);

	menu->addSeparator();
	p_actionAbout = new QAction(QIcon(":/res/images/about_blue_icon.png"), QApplication::translate("XrayBatchWaterMarkerWindow", "About"), this);
	connect(p_actionAbout, &QAction::triggered, [this]()
	{
		XrayAboutDialog dialog;
		dialog.setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint);
		dialog.setWindowIcon(QApplication::windowIcon());
		dialog.setFixedSize(650, 480);

		dialog.setImage(QPixmap(QString::fromUtf8(":/res/images/XrayLab_Logo-green-white_SloganEN_RGB.png")));
#ifdef _WIN64
		dialog.setTitle(QApplication::applicationName() + " (64 bit)");
#else
		dialog.setTitle(QApplication::applicationName() + " (32 bit)");
#endif
		dialog.setVersion(QApplication::applicationVersion());
		dialog.setReleaseDate(XrayQApplication::applicationBuildDate());
		dialog.setModule(XrayLicenseGlobals::getModules());
		if (XrayLicenseGlobals::isForCommercialUse())
			dialog.setLicenseType(QApplication::translate("XrayMainWindow", "Licensed for commercial use <a href=\"https://xray-lab.com/us/eula-english\">(License terms)</a>"));
		else
			dialog.setLicenseType(QApplication::translate("XrayMainWindow", "Unlicensed for commercial use <a href=\"https://xray-lab.com/us/eula-english\">(License terms)</a>"));
		dialog.setCopyRight("2020 XRAY-LAB GmbH & Co. KG");
		dialog.setWebsite("<html><head/><body><p><a href=\"https://xray-lab.com\"><span style=\" color:#55aaff;\">www.xray-lab.com</span></a></span></a></p></body></html>");

		dialog.setStyleSheet(QString::fromUtf8("QDialog { background: rgb(70, 70, 70); border:1px solid rgb(21, 142, 21); } QLabel { color: rgb(240, 240, 240); }"));
		dialog.exec();
	});
	menu->addAction(p_actionAbout);

	return menu;
}