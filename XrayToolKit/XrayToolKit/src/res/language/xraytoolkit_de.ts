<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>XrayAboutDialog</name>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="59"/>
        <source>Warning: this computer program is protected by copyright law and international treaties. Unauthorized reproduction or distribution of this program. or any portion of it, may result in severe civil and criminal penalties, and will be prosecuted to the maximum extent possible under the law.</source>
        <translation>Warnung: Dieses Computerprogramm ist weltweit urheberrechtlich geschützt.Die unautorisierte Reproduktion oder der Weitervertrieb dieses Programms oder von Teilen dieses Programms werden zivil- und strafrechtlich verfolgt.</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="77"/>
        <source>XApps Manager v1.1.0 (64 bit)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="97"/>
        <source>Release date:</source>
        <translation>Veröffentlichungsdatum:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="109"/>
        <source>09.04.2019</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="136"/>
        <source>Licensed module(s):</source>
        <translation>Lizensierte Modul(e):</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="251"/>
        <source>Version:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="263"/>
        <source>1.0120</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="121"/>
        <source>XRAY-LAB Report</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="148"/>
        <source>2020 XRAY-LAB GmbH &amp; Co. KG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="221"/>
        <source>License type:</source>
        <translation>Lizenztyp:</translation>
    </message>
    <message>
        <source>Licensed for commercial use &lt;a href=&quot;http://xray-lab.com&quot;&gt;(License terms)&lt;/a&gt;</source>
        <translation type="vanished">Lizensiert für die kommerzielle Benutzung &lt;a href=&quot;https://xray-lab.com&quot;&gt;(Lizenzbedingungen)&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Licensed modules:</source>
        <translation type="vanished">Lizenzmodule:</translation>
    </message>
    <message>
        <source>XRay-Lab report</source>
        <translation type="vanished">XRay-Lab Bericht</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="179"/>
        <source>Licensed for commercial use &lt;a href=&quot;https://xray-lab.com&quot;&gt;(License terms)&lt;/a&gt;</source>
        <translation>Lizensiert für die kommerzielle Nutzung &lt;a href=&quot;https://xray-lab.com&quot;&gt;(Lizenzbedingungen)&lt;/a&gt;</translation>
    </message>
    <message>
        <source>XRay-Lab Report</source>
        <translation type="vanished">XRay-Lab Bericht</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="160"/>
        <source>Copyright(C):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="197"/>
        <source>All rights reserved.</source>
        <translation>Alle Rechte vorbehalten.</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="209"/>
        <source>Website:</source>
        <translation>Webseite:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayAboutDialog.ui" line="233"/>
        <source>&lt;a href=&quot;https://xray-lab.com&quot;&gt;www.xray-lab.com&lt;/a&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>XrayAppsManagementWidget</name>
    <message>
        <source>This module will be added soon.</source>
        <translation type="vanished">Dieses Modul wird bald hinzugefügt.</translation>
    </message>
    <message>
        <source>Reports</source>
        <translation type="vanished">Berichte</translation>
    </message>
</context>
<context>
    <name>XrayBatchWaterMarkerWidget</name>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="14"/>
        <source>BatchWaterMarkerWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="20"/>
        <source>Watermark</source>
        <translation>Wasserzeichen</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="28"/>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="51"/>
        <source>Opacity of watermark</source>
        <translation>Durchlässigkeit des Wasserzeichens</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="44"/>
        <location filename="../../report/XrayBatchWaterMarkerWidget.cpp" line="91"/>
        <source>Image File</source>
        <translation>Bilddatei</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="61"/>
        <source>Opacity</source>
        <translation>Durchlässigkeit</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="68"/>
        <source>Watermark file path</source>
        <translation>Dateipfad Wasserzeichen</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="75"/>
        <source>Select a watermark image.</source>
        <translation>Bild als Wasserzeichen auswählen.</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="78"/>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="137"/>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="174"/>
        <source>Browse...</source>
        <translation>Durchsuchen...</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="85"/>
        <source>RGB on Gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="92"/>
        <source>If it is checked, colored watermark will be blended on Gray images.</source>
        <translation>Wenn ausgewählt, wird ein farbiges Wasserzeichen in Schwarz-Weiß-Bildern eingeblendet.</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="110"/>
        <source>Input Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="134"/>
        <source>Select single or multiple image(s).</source>
        <translation>Ein oder mehrere Bilder auswählen.</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="149"/>
        <source>Output Images</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="157"/>
        <source>Directory</source>
        <translation>Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="164"/>
        <source>Directory path where watermarked images will be saved.</source>
        <translation>Verzeichnis, in dem Bilder mit Wasserzeichen gespeichert werden sollen.</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="171"/>
        <source>Select directory where watermarked images will be saved.</source>
        <translation>Verzeichnis auswählen, in dem Bilder mit Wasserzeichen gespeichert werden sollen.</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="186"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="204"/>
        <source>Preview of watermark</source>
        <translation>Vorschau des Wasserzeichens</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="223"/>
        <source>Preview of watermarked image</source>
        <translation>Vorschau von Bild mit Wassezeichen</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.ui" line="246"/>
        <source>Apply watermark to the input images and save at the specified location.</source>
        <translation>Eingelesene Bilder mit Wasserzeichen versehen und im angegebenen Verzeichnis speichern.</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.cpp" line="91"/>
        <source>Select File</source>
        <translation>Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.cpp" line="107"/>
        <source>Select Files</source>
        <translation>Dateeni auswählen</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.cpp" line="107"/>
        <source>Image Files</source>
        <translation>Bilddateien</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.cpp" line="147"/>
        <source>Open Directory</source>
        <translation>Verzeichnis öffnen</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.cpp" line="182"/>
        <source>Demo version doesn&apos;t allow to save the images!</source>
        <translation>Speichern von Bildern in Demo-Version nicht möglich!</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.cpp" line="188"/>
        <source>Couldn&apos;t save! Watermark image is missing!</source>
        <translation>Speichern nicht möglich! Bild mit Wasserzeichen fehlt!</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.cpp" line="194"/>
        <source>Couldn&apos;t save! Output images directory is missing!</source>
        <translation>Speichern nicht möglich! Output-Verzeichnis für Bilder fehlt!</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.cpp" line="201"/>
        <source>Couldn&apos;t find input images!</source>
        <translation>Bild nicht gefunden!</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.cpp" line="221"/>
        <source>Saved file: %1</source>
        <translation>Datei speichern: %1</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWidget.cpp" line="230"/>
        <source>%1 images has been processed and saved successfully!</source>
        <translation>%1 Bild wurde erfolgreich bearbeitet und gespeichert!</translation>
    </message>
</context>
<context>
    <name>XrayBatchWaterMarkerWindow</name>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWindow.cpp" line="92"/>
        <source>View Help</source>
        <translation>Hilfe anzeigen</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWindow.cpp" line="96"/>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location filename="../../report/XrayBatchWaterMarkerWindow.cpp" line="101"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
</context>
<context>
    <name>XrayBodyTextWizardPage</name>
    <message>
        <source>Analysis:</source>
        <translation type="vanished">Analyse:</translation>
    </message>
    <message>
        <source>Hypothetical Error:</source>
        <translation type="vanished">Hypothetischer Fehler:</translation>
    </message>
    <message>
        <source>Equipment:</source>
        <translation type="vanished">Ausrüstung:</translation>
    </message>
    <message>
        <source>The whole room is ESD-protected that includes all equipments and the clothes.&lt;br/&gt;</source>
        <translation type="vanished">Der ganze Raum ist ESD geschützt das beinhaltet Schutzausrüstung und Schutzkleidung.</translation>
    </message>
    <message>
        <source>Micromex with Max. power 20W at 180kV.</source>
        <translation type="vanished">Micromex mit max. Leistung 20W bei 180kV</translation>
    </message>
    <message>
        <source>Vtomex M  with Max. power 300W at 300kV.</source>
        <translation type="vanished">Vtomex M  mit max. Leistung 300W bei 300kV</translation>
    </message>
    <message>
        <source>Vtomex S  with Max. power 222W at 225kV.</source>
        <translation type="vanished">Vtomex S  mit max. Leistung 225W bei 225kV</translation>
    </message>
    <message>
        <source> This is nondestructive for all parts because the radiated power is very low.</source>
        <translation type="vanished">Dies ist zerstörungsfrei für alle Teile, weil die abgestrahlte Leistung sehr gering ist.</translation>
    </message>
</context>
<context>
    <name>XrayCTMachinesStatusWidget</name>
    <message>
        <location filename="../../watcher/XrayCTMachinesStatusWidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>XrayCheckForUpdate</name>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="47"/>
        <source>Software Update</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="49"/>
        <source>Release Notes</source>
        <translation>Release-Anmerkungen</translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="56"/>
        <source>Update is available</source>
        <translation>Update ist verfügbar</translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="65"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="67"/>
        <source>Installed</source>
        <translation>Installiert</translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="69"/>
        <source>Available</source>
        <translation>Verfügbar</translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="71"/>
        <source>Release Date</source>
        <translation>Release-Datum</translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="73"/>
        <source>Release Status</source>
        <translation>Release-Status</translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="259"/>
        <source>Do you want to close the application for the update process to complete?</source>
        <translation>Wollen Sie das Programm schließen, um den Update-Prozeß abzuschließen?</translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="80"/>
        <source>Install Update</source>
        <translation>Update installieren</translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="87"/>
        <source>Remind Me Later</source>
        <translation>Erinnere mich später</translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="102"/>
        <source>Download</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="107"/>
        <source>Always check on startup</source>
        <translation>Beim Starten immer prüfen</translation>
    </message>
    <message>
        <location filename="../../network/XrayCheckForUpdate.cpp" line="214"/>
        <source>You are up to date!

%1 %2 is currently the newest version
available. Please check back again for updates at
a later time.</source>
        <translation>Die Software ist aktuell! 
%1 %2 ist aktuell als die neueste Version
verfügbar. Bitte prüfen Sie später wieder, 
ob ein Update möglich ist.</translation>
    </message>
</context>
<context>
    <name>XrayClientInfoWizardPage</name>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="525"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="596"/>
        <source>Client/Order Information</source>
        <translation>Kunden-/Bestellinformation</translation>
    </message>
    <message>
        <source>Order Info</source>
        <translation type="vanished">Auftragsinfo</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="528"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="597"/>
        <source>Order Information</source>
        <translation>Auftragsinformationen</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="530"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="598"/>
        <source>Order Number:</source>
        <translation>Auftragsnummer:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="535"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="599"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="540"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="600"/>
        <source>Calendar</source>
        <translation>Kalender</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="551"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="601"/>
        <source>Client Name:</source>
        <translation>Kundenname:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="555"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="602"/>
        <source>Sample:</source>
        <translation>Probe:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="556"/>
        <source>Electronic Equipments</source>
        <translation>Elektronische Geräte</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="559"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="603"/>
        <source>Quantity:</source>
        <translation>Menge:</translation>
    </message>
</context>
<context>
    <name>XrayCustomizedSectionsWizardPage</name>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="818"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="838"/>
        <source>Customized Sections of the Report Document</source>
        <translation>Kunden- Abschnitte des Berichts</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="821"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="839"/>
        <source>Section</source>
        <translation>Abschnitt</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="853"/>
        <source>Section heading...</source>
        <translation>Überschrift...</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="856"/>
        <source>Section heading description...</source>
        <translation>Beschreibung der Überschrift...</translation>
    </message>
    <message>
        <source>Specify the heading of the section that should be removed.</source>
        <translation type="vanished">Spezifiziere die Kopfzeile, welche gelöscht werden soll.</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation type="vanished">&amp;Löschen</translation>
    </message>
    <message>
        <source>Press to remove the section.</source>
        <translation type="vanished">Zum Löschen des Bereichs drücken.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="861"/>
        <source>Remove Section</source>
        <translation>Bereich löschen</translation>
    </message>
</context>
<context>
    <name>XrayDeleteProjectFilesWidget</name>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="14"/>
        <source>XProject Files Cleaner</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="23"/>
        <source>Root Directory</source>
        <translation>Hauptverzeichnis</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="31"/>
        <source>Root path</source>
        <translation>Hauptpfad</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="38"/>
        <source>Specify the root directory path. (Q:/)</source>
        <translation>Hauptverzeichnispfad spezifizieren. (Q:/)</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="41"/>
        <source>Q:/</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="48"/>
        <source>Browse for the root directory.</source>
        <translation>Nach Hauptverzeichnis suchen.</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="51"/>
        <source>Browse</source>
        <translation>Durchsuchen</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="58"/>
        <source>Filter for root directories</source>
        <translation>Filter für Hauptverzeichnisse</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="65"/>
        <source>Specify the regular expression filter for the sub-directories. (Q:/23451/)</source>
        <translation>Unterverzeichnisse spezifizieren. (Q:/23451/)</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="68"/>
        <source>\d*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="75"/>
        <source>Sub-directory that need to be cleaned</source>
        <translation>Unterverzeichnis, welches bereinigt werden soll</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="82"/>
        <source>Specify name of the directory to be scanned in the sub-directories. (Q:/23451/CT/)</source>
        <translation>Verzeichnisname spezifizieren, welcher in den Unterverzeichnissen gescannt werden soll. (Q:/23451/CT/)</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="85"/>
        <source>CT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="92"/>
        <source>File extension to be filtered</source>
        <translation>Dateierweiterung zum Filtern</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="99"/>
        <source>Specify file extensions to be considered for cleaning. (Q:/23451/CT/abc/name00012.tif)
Note: extension must start with dot(.) and put space for multiple extensions.</source>
        <translation>Spezifiziere Dateierweiterungen für mögliche Bereinigungen. (Q:/23451/CT/abc/name00012.tif)
Anmerk.: Erweiterung muß mit einem Punkt (.) beginnen und setze ein Leerzeichen
für mehrfache Erweiterungen.</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="103"/>
        <source>.tif .tiff</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="110"/>
        <source>Skip files that contains these names</source>
        <translation>Überspringe Dateien mit diesen Namen</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="117"/>
        <source>Skip those files that contains specified names.
For example: If you specify &quot;dark&quot;, then this file &quot;CT_XX4_01_Dark_500.tif&quot; will be skipped for cleaning.
Note: Put single space for multiple names.</source>
        <translation>Überspringe diese Dateien mit den spezifizierten Namen. Als Beispiel: Wenn Sie &quot;dark&quot;
spezifizieren, dann wird diese Datei &quot;CT_XX4_01_Dark_500.tif&quot; von der Bereinigung ausgenommen.
Anmerk.: Setze ein Leerzeichen für mehrere Namen.</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="122"/>
        <source>dark bright</source>
        <translation>dunkel hell</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="129"/>
        <source>Sort files numerically</source>
        <translation>Sortiere Dateien numerisch</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="136"/>
        <source>If it is checked, files will be sorted by considering numeric numbers in the file name. (name00012.tif)
It also checks the last character of the file name if that&apos;s a numeric digit.</source>
        <translation>Wenn der Haken gesetzt wurde, werden Dateien bzgl. den Zahlen im Dateinamen sortiert. (name00012.tif)
Es wird auch geprüft, ob am Ende des Dateinamens Zahlen sind.</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="150"/>
        <source>Skip files from start of the sorted files</source>
        <translation>Überspringe Dateien vom Start der sortierten Dateien</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="157"/>
        <source>How many files must be skipped from the sorted list of files (from start).</source>
        <translation>Wieviele Dateien müssen in der sortierten Liste übersprungen werden (vom Start).</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="170"/>
        <source>Skip files from end of the sorted files</source>
        <translation>Überspringe Dateien vom Ende der sortierten Dateien</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="177"/>
        <source>How many files must be skipped from the sorted list of files (from end).</source>
        <translation>Wieviele Dateien müssen in der sortierten Liste übersprungen werden (vom Ende).</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="190"/>
        <source>Update interval for rescan the root path (sec)</source>
        <translation>Update-Interval für Neuscann des Hauptpfades (sec)</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="197"/>
        <source>Specify the update interval in seconds to rescan the root path and update the directory structure.</source>
        <translation>Spezifiziere das Update-Interval zum Neuscannen des Stammpfades und Update der Verzeichnisstruktur.</translation>
    </message>
    <message>
        <source>Update interval for directory structure (sec)</source>
        <translation type="vanished">Update-Interval für Verzeichnisstruktur (sec)</translation>
    </message>
    <message>
        <source>Specify update interval to update the directory structure in seconds.</source>
        <translation type="vanished">Spezifiziere Update-Interval zum Aktualisieren der Verzeichnisstruktur in Sekunden.</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="215"/>
        <source>Directory Structure</source>
        <translation>Verzeichnisstruktur</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="221"/>
        <source>Scan the root directory for sub-directories and files.</source>
        <translation>Scannen des Hauptverzeichnisses für Unterverzeichnisse und Dateien.</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="224"/>
        <source>Scan</source>
        <translation>Scannen</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="231"/>
        <source>Cancel the scanning process.</source>
        <translation>Abbrechen des Scann-Prozesses.</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="234"/>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="505"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="267"/>
        <source>Check to rescan the root path and update the directory structure on every start.</source>
        <translation>Abhaken zum erneuten Scannen des Stammpfades und Updaten der Verzeichnisstruktur bei jedem Start.</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="270"/>
        <source>Scan on startup</source>
        <translation>Scannen bei Inbetriebnahme</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="286"/>
        <source>Properties</source>
        <translation>Eigenschaften</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="294"/>
        <source>Total files:</source>
        <translation>Gesamtmenge Dateien:</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="301"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="308"/>
        <source>Total size:</source>
        <translation>Gesamtgröße:</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="315"/>
        <source>0.0 MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="337"/>
        <source>Update the total number of scanned files and their size.</source>
        <translation>Update der Gesamtzahl an gescannten Dateien und Ihre Größe.</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="340"/>
        <source>Update</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="352"/>
        <source>Ready...</source>
        <translation>Bereit...</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="378"/>
        <source>Delete files from the selected directories.</source>
        <translation>Lösche Dateien von den ausgewählten Verzeichnissen.</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.ui" line="381"/>
        <source>Start Cleaning</source>
        <translation>Start Bereinigung</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="336"/>
        <source>Select Root Directory</source>
        <translation>Wähle Hauptverzeichnis</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="359"/>
        <source>Couldn&apos;t find any project directory!</source>
        <translation>Konnte nicht irgendein Projektverzeichnis finden!</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="394"/>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="491"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="446"/>
        <source>Please select directories that you want to clean!</source>
        <translation>Wählen Sie bitte die zu bereinigenden Verzeichnisse aus!</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="496"/>
        <source>Couldn&apos;t find any cleanable file in the selected directory!</source>
        <translation>Konnte nicht irgendeine zu bereinigende Datei im ausgewählten Verzeichnis finden!</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="503"/>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="575"/>
        <source>Clean</source>
        <translation>Bereinigen</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="504"/>
        <source>Show me the list</source>
        <translation>Zeigen Sie mir die Liste</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="506"/>
        <source>The detected files are more than 50 thousands. Do you want to see the list of cleanable files?

Press &apos;Clean&apos; if you do not want to see the list of cleanable files.</source>
        <translation>Es wurden mehr als 50 taused Dateien erkannt. Wollen Sie die Liste der zu bereinigenden Dateien sehen?

Drücken Sie &apos;Bereinigen&apos;, wenn Sie die Liste der zu bereinigenden Dateien sehen wollen.</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="548"/>
        <source>Remove Selected</source>
        <translation>Ausgewähltes löschen</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="580"/>
        <source>Following %1 files are about to be deleted.
You can remove files from the list that shouldn&apos;t be deleted.
(Right click to see the menu actions.)
</source>
        <translation>Folgende %1 Dateien werden gelöscht.
Sie können Dateien aus der Liste entfernenn, welche nicht gelöscht werden sollen.
(Rechtsklick, um Menu zu sehen.)
</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="593"/>
        <source>Are you sure you want to clean?</source>
        <translation>Sind Sie sicher, daß Sie Löschen wollen?</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="621"/>
        <source>Files in the selected directories have been cleaned successfully!</source>
        <translation>Dateien in dem ausgewählten Verzeichnis wurden erfolgreich gelöscht!</translation>
    </message>
    <message>
        <source>Are you sure you want to clean the selected directories?</source>
        <translation type="vanished">Sind Sie sich sicher, daß Sie die ausgewählten Verzeichnisse bereinigen möchten?</translation>
    </message>
    <message>
        <source>Please select directories you want to clean!</source>
        <translation type="vanished">Bitte wähle die zu bereinigenden Verzeichnisse aus!</translation>
    </message>
    <message>
        <source>Deleted file: </source>
        <translation type="vanished">Gelöschte Datei:</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWidget.cpp" line="610"/>
        <source>You are using a demo version which has limited features.</source>
        <translation>Sie benutzen eine Demo-Version mit eingeschränkten Funktionsumfang.</translation>
    </message>
    <message>
        <source>Files in the selected directories have been successfully cleaned!</source>
        <translation type="vanished">Dateien in den ausgewählten Verzeichnissen sind erfolgreich bereinigt worden!</translation>
    </message>
</context>
<context>
    <name>XrayDeleteProjectFilesWindow</name>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWindow.cpp" line="72"/>
        <source>Scanning in progress...</source>
        <translation>Scannen im Fortschreiten...</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWindow.cpp" line="76"/>
        <source>Last updated: </source>
        <translation>Letztes Update: </translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWindow.cpp" line="110"/>
        <source>View Help</source>
        <translation>Zeige Hilfe</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWindow.cpp" line="114"/>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayDeleteProjectFilesWindow.cpp" line="119"/>
        <source>About</source>
        <translation>Info</translation>
    </message>
</context>
<context>
    <name>XrayDocFooterWizardPage</name>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="409"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="481"/>
        <source>Footer of the Report Document</source>
        <translation>Fußzeile des Berichtdokuments</translation>
    </message>
    <message>
        <source>Company Info</source>
        <translation type="vanished">Firmeninfo</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="412"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="482"/>
        <source>My Company Information</source>
        <translation>Eigene Unternehmensinformationen</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="414"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="483"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="418"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="484"/>
        <source>Location:</source>
        <translation>Ort:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="422"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="485"/>
        <source>Phone:</source>
        <translation>Telefon:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="427"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="486"/>
        <source>Fax:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="432"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="487"/>
        <source>Company Logo:</source>
        <translation>Unternehmenslogo:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="435"/>
        <source>Browse...</source>
        <translation>Durchsuchen...</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="436"/>
        <source>Browse for a company logo image.</source>
        <translation>Nach Unternehmnsloge suchen.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="439"/>
        <source>Select File</source>
        <translation>Datei auswählen</translation>
    </message>
</context>
<context>
    <name>XrayDocHeaderWizardPage</name>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="326"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="370"/>
        <source>Header of the Report Document</source>
        <translation>Kopfzeilen des Berichtdokuments</translation>
    </message>
    <message>
        <source>Contact Person</source>
        <translation type="vanished">Ansprechpartner</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="329"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="371"/>
        <source>Test Engineer</source>
        <translation>Prüfer</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="331"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="372"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="335"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="373"/>
        <source>Phone:</source>
        <translation>Telefon:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="340"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="374"/>
        <source>Email:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>XrayDocTitleWizardPage</name>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="126"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="191"/>
        <source>Report Document Information</source>
        <translation>Berichtdokument-Information</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="129"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="192"/>
        <source>Report Document</source>
        <translation>Berichtdokument</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="133"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="193"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="134"/>
        <source>Ins_Report_24404</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="137"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="194"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="138"/>
        <source>Inspection Report</source>
        <translation>Untersuchungsbericht</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="141"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="195"/>
        <source>Tag:</source>
        <translation>Schlagwort:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="142"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="196"/>
        <source>2D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="143"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="197"/>
        <source>Assign related tag(s)/keyword(s) to this report document.</source>
        <translation>Spezifikation Schlagwort(e) zu diesem Berichtdokument.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="146"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="198"/>
        <source>Heading Text:</source>
        <translation>Kopfzeilen - Text:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="147"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="199"/>
        <source>Body Text:</source>
        <translation>Hauptbereich-Text:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="200"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="247"/>
        <source>Paper Size:</source>
        <translation>Papiergröße:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="201"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="286"/>
        <source>Paper Orientation:</source>
        <translation>Papier- Blattausrichtung:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="249"/>
        <source>A0 (841 x 1189 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="250"/>
        <source>A1 (594 x 841 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="251"/>
        <source>A2 (420 x 594 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="252"/>
        <source>A3 (297 x 420 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="253"/>
        <source>A4 (210 x 297 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="254"/>
        <source>A5 (148 x 210 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="255"/>
        <source>A6 (105 x 148 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="256"/>
        <source>A7 (74 x 105 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="257"/>
        <source>A8 (52 x 74 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="259"/>
        <source>B0 (1000 x 1414 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="260"/>
        <source>B1 (707 x 1000 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="261"/>
        <source>B2 (500 x 707 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="262"/>
        <source>B3 (353 x 500 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="263"/>
        <source>B4 (250 x 353 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="264"/>
        <source>B5 (176 x 250 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="265"/>
        <source>B6 (125 x 176 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="266"/>
        <source>B7 (88 x 125 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="267"/>
        <source>B8 (62 x 88 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="270"/>
        <source>C5E (163 x 229 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="271"/>
        <source>DLE (110 x 220 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="272"/>
        <source>Executive (7.5 x 10 inches)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="273"/>
        <source>Folio (210 x 330 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="274"/>
        <source>Ledger (432 x 279 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="275"/>
        <source>Legal (8.5 x 14 inches)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="276"/>
        <source>Letter (8.5 x 11 inches)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="277"/>
        <source>Tabloid (279 x 432 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="278"/>
        <source>US #10 Envelope (105 x 241 mm)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="288"/>
        <source>Portrait</source>
        <translation>Hochformat</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="289"/>
        <source>Landscape</source>
        <translation>Querformat</translation>
    </message>
</context>
<context>
    <name>XrayDocumentTitleWizardPage</name>
    <message>
        <source>Report Document Information</source>
        <translation type="vanished">Berichtdokument-Information</translation>
    </message>
    <message>
        <source>Report Document</source>
        <translation type="vanished">Berichtdokument</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="vanished">Name:</translation>
    </message>
    <message>
        <source>Title:</source>
        <translation type="vanished">Titel:</translation>
    </message>
    <message>
        <source>Inspection Report</source>
        <translation type="vanished">Untersuchungsbericht</translation>
    </message>
    <message>
        <source>Tag:</source>
        <translation type="vanished">Schlagwort:</translation>
    </message>
    <message>
        <source>Assign related tag(s)/keyword(s) to this report document.</source>
        <translation type="vanished">Spezifikation Schlagwort(e) zu diesem Berichtdokument.</translation>
    </message>
</context>
<context>
    <name>XrayFileSystemWatcherMainWidget</name>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWidget.cpp" line="57"/>
        <source>Project:</source>
        <translation>Projekt:</translation>
    </message>
</context>
<context>
    <name>XrayFileSystemWatcherTreeWidget</name>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherTreeWidget.cpp" line="545"/>
        <source>Open in file explorer...</source>
        <translation>Öffnen in Exolorer...</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherTreeWidget.cpp" line="551"/>
        <source>Copy path</source>
        <translation>Kopiere Pfad</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherTreeWidget.cpp" line="650"/>
        <source>Start time: </source>
        <translation>Startzeit: </translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherTreeWidget.cpp" line="653"/>
        <source>End time: </source>
        <translation>Endzeit: </translation>
    </message>
</context>
<context>
    <name>XrayFileSystemWatcherWidget</name>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWidget.cpp" line="98"/>
        <source>PCA Info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWidget.cpp" line="121"/>
        <source>Image Box</source>
        <translation>Bild-Box</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWidget.cpp" line="151"/>
        <source>Last updated: </source>
        <translation>Letztes Update: </translation>
    </message>
</context>
<context>
    <name>XrayFileSystemWatcherWindow</name>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWindow.cpp" line="87"/>
        <source>View Help</source>
        <translation>Zeige Hilfe</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWindow.cpp" line="91"/>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWindow.cpp" line="95"/>
        <source>About</source>
        <translation>Über uns</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWindow.cpp" line="112"/>
        <source>Restore Last Projects</source>
        <translation>Letzte Projekte wiederherstellen</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWindow.cpp" line="123"/>
        <source>Set Root Path...</source>
        <translation>Setze Verzeichnispfad...</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWindow.cpp" line="131"/>
        <source>Root Drive:</source>
        <translation>Verzeichnis-Laufwerk:</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWindow.cpp" line="136"/>
        <source>Update Interval: </source>
        <translation>Update-Interval: </translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWindow.cpp" line="166"/>
        <source>Please enter a valid drive path!</source>
        <translation>Setze einen gültigen Laufwerkspfad!</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWindow.cpp" line="173"/>
        <source>Update</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWindow.cpp" line="181"/>
        <location filename="../../watcher/XrayFileSystemWatcherWindow.cpp" line="190"/>
        <source>Hide Dock</source>
        <translation>Verberge Dock</translation>
    </message>
    <message>
        <location filename="../../watcher/XrayFileSystemWatcherWindow.cpp" line="192"/>
        <source>Show Dock</source>
        <translation>Zeige Dock</translation>
    </message>
</context>
<context>
    <name>XrayFindFilesWidget</name>
    <message>
        <source>XRay-Lab Report Generator =&gt; Find Existing Reports</source>
        <translation type="vanished">XRay-Lab- Berichtersteller=&gt;Finde existierende Berichte</translation>
    </message>
    <message>
        <source>Find Existing Files</source>
        <translation type="vanished">Finde existierende Berichte</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="46"/>
        <source>&amp;Browse...</source>
        <translation>&amp;Durchsuchen...</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="50"/>
        <source>&amp;Find</source>
        <translation>&amp;Finde</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="54"/>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="55"/>
        <source>*.ini</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="58"/>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="59"/>
        <source>2D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="58"/>
        <source>3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="58"/>
        <source>Porosity</source>
        <translation>Porosität</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="70"/>
        <source>File Type:</source>
        <translation>Dateityp:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="71"/>
        <source>Tag:</source>
        <translation>Schlagwort:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="72"/>
        <source>Look In:</source>
        <translation>Hineinschauen:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="169"/>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="222"/>
        <source>Find Files</source>
        <translation>Finde Dateien</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="220"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="230"/>
        <source>Searching file number %1 of %n...</source>
        <translation>Suche Dateinummer von %1 bis %n...</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="288"/>
        <source>Couldn&apos;t find any file with this tag!</source>
        <translation>Konnte nicht irgendeine Datei mit diesem Schlüsselwort finden!</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="290"/>
        <source>file(s) found! (Double click on a file to restore it)</source>
        <translation>Dateie(n) gefunden! (Doppelclick auf die Datei zur Wiederherstellung dieser)</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="344"/>
        <source>Open File</source>
        <translation>Öffne Datei</translation>
    </message>
    <message>
        <source>%n file(s) found! (Double click on a file to restore it)</source>
        <translation type="vanished">%n Dateie(n) gefunden! (Doppelclick auf die Datei zur Wiederherstellung dieser)</translation>
    </message>
    <message>
        <source>Filename</source>
        <translation type="vanished">Dateiname</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="313"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="313"/>
        <source>File Name</source>
        <translation>Datei Name</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="340"/>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="341"/>
        <source>Copy Name</source>
        <translation>Kopie Name</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFindFilesWidget.cpp" line="343"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
</context>
<context>
    <name>XrayFindFilesWizardPage</name>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="51"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="117"/>
        <source>Find Existing Report Document</source>
        <translation>Suche vorhandes Berichtdokument</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="54"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="118"/>
        <source>Find Existing Reports</source>
        <translation>Suche vorhandene Berichte</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="62"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="119"/>
        <source>Restore Previous Report</source>
        <translation>Vorherigen Bericht wiederherstellen</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="68"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="120"/>
        <source>&amp;Browse...</source>
        <translation>&amp;Durchsuchen...</translation>
    </message>
    <message>
        <source>Browse...</source>
        <translation type="vanished">Browse de...</translation>
    </message>
</context>
<context>
    <name>XrayFinishWizardPage</name>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="1039"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="1080"/>
        <source>Save Generated Report As</source>
        <translation>Speichern erzeugten Bericht unter</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="1044"/>
        <source>We will send you all x-ray inspection results in a USB-Stick. If you need the results faster, we can establish a FTP-account for you.&lt;br/&gt;&lt;br/&gt;In case of any question, please feel free to contact me.&lt;br/&gt;&lt;br/&gt;Mit freundlichen Gruessen / With best regards,</source>
        <translation>Alle X-Ray -Analyseergebnisse werden Ihnen auf einem USB-Stick zugesandt. Wenn Sie die Ergebnisse schneller benötigen, kann ein FTP-Zugang für Sie eingerichtet werden. &lt;br/&gt;&lt;br/&gt;Bitte kontaktieren Sie mich bei Fragen.&lt;br/&gt;&lt;br/&gt;Mit freundlichen Gruessen / With best regards,</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="1049"/>
        <source>Signature image file...</source>
        <translation>Signatur Bilddatei...</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="1052"/>
        <source>&amp;Browse...</source>
        <translation>&amp;Durchsuchen...</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="1054"/>
        <source>Select a signature image file.</source>
        <translation>Auswahl einer Signatur der Bilddatei.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="1057"/>
        <source>Select File</source>
        <translation>Wähle Datei</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="1057"/>
        <source>Image File</source>
        <translation>Bilddatei</translation>
    </message>
    <message>
        <source>We will send you all x-ray inspection results in a USB-Stick. If you need the results faster, we can establish a FTP-account for you.&lt;br/&gt;&lt;br/&gt;In case of any question, please feel free to contact me.</source>
        <translation type="vanished">Wir werden Ihnen alle Ergebnisse der Untersuchung auf einem USB-Stick senden. Wenn Sie die Ergebnisse schneller benötigen,können wir einen FTP-Zugang für Sie einrichten.&lt;br/&gt;&lt;br/&gt;Für  Rückfragen stehe ich Ihnen gerne zur Verfügung.</translation>
    </message>
</context>
<context>
    <name>XrayFlashFilter</name>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="20"/>
        <source>Adjust Intensity Range</source>
        <translation>Intensität anpassen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="28"/>
        <source>If it is checked, histogram can be viewed and
range of the image intensity can be adjusted.</source>
        <translation>Wenn ausgewählt, kann das Histogram betrachtet und
die Spanne und Intensität angepasst werden.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="32"/>
        <source>On/Off</source>
        <translation>Ein/Aus</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="42"/>
        <location filename="../../paint/XrayFlashFilter.ui" line="61"/>
        <source>Window range or contrast of the image.</source>
        <translation>Spanne bzw. Kontrast des Bildes.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="77"/>
        <location filename="../../paint/XrayFlashFilter.ui" line="93"/>
        <source>Level range or brightness of the image.</source>
        <translation>Spanne bzw. Helligkeit des Bildes.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="114"/>
        <source>Parameters</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="122"/>
        <source>Delta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="129"/>
        <location filename="../../paint/XrayFlashFilter.ui" line="146"/>
        <source>Adjust the contrast of the image.
Lower the value higher the contrast.</source>
        <translation>Bildkontrasteinstellung. Je niedriger der Wert, um so stärker der Kontrast.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="160"/>
        <source>Sigma</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="167"/>
        <location filename="../../paint/XrayFlashFilter.ui" line="184"/>
        <source>Adjust the smoothness of the image.
Lower the value lower the smoothness.</source>
        <translation>Bildglättungseinstellung. Je niedriger der Wert, um so schwächer die Glättung.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="198"/>
        <source>Clipping</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="205"/>
        <location filename="../../paint/XrayFlashFilter.ui" line="219"/>
        <source>Adjust the histogram clipping.
Lower the value lower the contrast.</source>
        <translation>Einstellung für Histogramm-Clipping. Je niedriger der Wert, um so niedriger der Kontrast.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="230"/>
        <source>Exposure</source>
        <translation>Belichtung</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="237"/>
        <location filename="../../paint/XrayFlashFilter.ui" line="253"/>
        <source>Adjust the exposure of the image.</source>
        <translation>Belichtungseinstellung für das Bild.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="266"/>
        <source>Gamma</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="273"/>
        <location filename="../../paint/XrayFlashFilter.ui" line="286"/>
        <source>Adjust the gamma of the image.</source>
        <translation>Gamma-Wert-Einstellung für das Bild.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="296"/>
        <source>Optimize</source>
        <translation>Optimiere</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="303"/>
        <source>If it is checked, the resulting contrast will be optimized.</source>
        <translation>Optimierung des Kontrastes bei gesetztem Haken.</translation>
    </message>
    <message>
        <source>Reset image contrast and the parameters.</source>
        <translation type="vanished">Bildkontrast und Parameter zurücksetzen.</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Zurücksetzen</translation>
    </message>
    <message>
        <source>Update contrast by applying the current parameters.</source>
        <translation type="vanished">Kontrast-Update durch Übernahme der aktuellen Parameter. </translation>
    </message>
    <message>
        <source>Preview</source>
        <translation type="vanished">Vorschau</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="321"/>
        <source>Presets</source>
        <translation>Voreinstellungen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="327"/>
        <source>Select a preset for the selected picture.</source>
        <translation>Voreinstellungsauswahl für ausgewähltes Bild.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="340"/>
        <source>Save current settings as a preset.</source>
        <translation>Speichern aktueller Einstellungen als Vorauswahl.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="343"/>
        <source>Save</source>
        <translation>Speicher</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="356"/>
        <source>Update the selected picture with the current preset.</source>
        <translation>Aktualisierung des ausgewählten Bildes mit der aktuellen Voreinstellung.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="359"/>
        <source>Update</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="372"/>
        <source>Remove current preset.</source>
        <translation>Löschen aktueller Voreinstellung.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="375"/>
        <source>Remove</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="385"/>
        <source>Apply current changes to the original image.</source>
        <translation>Übernahme aktueller Änderungen an das Originalbild.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="388"/>
        <source>Apply</source>
        <translation>Übernahme</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="395"/>
        <source>Directories</source>
        <translation>Verzeichnisse</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="401"/>
        <source>Read</source>
        <translation>Lese</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="408"/>
        <source>Directory of the images to be filtered.</source>
        <translation>Verzeichnis der Bilder wird gefiltert.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="421"/>
        <source>Browse for the images directory.</source>
        <translation>Durchsuchen für das Verzeichnis der Bilder.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="424"/>
        <location filename="../../paint/XrayFlashFilter.ui" line="470"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="431"/>
        <source>Write</source>
        <translation>Schreibe</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="438"/>
        <source>Directory of the filtered/saved images.</source>
        <translation>Verzeichnis der gefilterten/gespeicherten Bilder.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="451"/>
        <source>Saved image file type.</source>
        <translation>Gespeichert Bilddatei-Typ.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="467"/>
        <source>Browse for the saved images directory.</source>
        <translation>Durchsuche das Verzeichnis für die gespeicherten Bilder.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="477"/>
        <source>Process all images at the Read directory
location and saved at Write directory.</source>
        <translation>Verarbeite alle Bilder im Leseverzeichnis und gespeichert im Schreibverzeichnis.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.ui" line="481"/>
        <source>Process</source>
        <translation>Verarbeite</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.cpp" line="73"/>
        <location filename="../../paint/XrayFlashFilter.cpp" line="85"/>
        <source>Open Directory</source>
        <translation>Öffne Verzeichnis</translation>
    </message>
    <message>
        <source>Couldn&apos;t find any Paint area!</source>
        <translation type="vanished">Konnte nicht irgendeine Zeichnungsfläche finden!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.cpp" line="641"/>
        <source>Enter New Preset Name</source>
        <translation>Eintragen neuer Voreinstellungsbezeichnung</translation>
    </message>
    <message>
        <location filename="../../paint/XrayFlashFilter.cpp" line="642"/>
        <source>Enter Text</source>
        <translation>Eintragen Text</translation>
    </message>
</context>
<context>
    <name>XrayFontSelectorWidget</name>
    <message>
        <location filename="../../widgets/XrayFontSelectorWidget.cpp" line="42"/>
        <source>Change the font family.</source>
        <translation>Wechselt die Schriftfamilie.</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFontSelectorWidget.cpp" line="53"/>
        <source>Change the font size.</source>
        <translation>Ändert die Schriftgröße.</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFontSelectorWidget.cpp" line="56"/>
        <source>Bold</source>
        <translation>Fett</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFontSelectorWidget.cpp" line="59"/>
        <source>Change to the heavier font.</source>
        <translation>Änderung zu stärkerer Schrift.</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFontSelectorWidget.cpp" line="63"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFontSelectorWidget.cpp" line="66"/>
        <source>Change to an italic font.</source>
        <translation>Änderung zu kursiver Schrift.</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFontSelectorWidget.cpp" line="70"/>
        <source>Underline</source>
        <translation>Unterstrichen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFontSelectorWidget.cpp" line="73"/>
        <source>Draw a line below the text.</source>
        <translation>Zeichne eine Linie unter den Text.</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFontSelectorWidget.cpp" line="78"/>
        <source>Select a color from the color palette.</source>
        <translation>Farbauswahl von der Farbpalette.</translation>
    </message>
</context>
<context>
    <name>XrayFooterWizardPage</name>
    <message>
        <source>Footer of the Report Document</source>
        <translation type="vanished">Fußzeile des Berichtdokuments</translation>
    </message>
    <message>
        <source>Company Info</source>
        <translation type="vanished">Firmeninfo</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="vanished">Name:</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="vanished">Ort:</translation>
    </message>
    <message>
        <source>Phone:</source>
        <translation type="vanished">Telefon:</translation>
    </message>
</context>
<context>
    <name>XrayFramelessWindow</name>
    <message>
        <location filename="../../widgets/XrayFramelessWindow.ui" line="141"/>
        <source>XRay-Lab App Manager</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>XrayFramelessWindowWin32</name>
    <message>
        <location filename="../../widgets/XrayFramelessWindowWin32.cpp" line="164"/>
        <source>Menu</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFramelessWindowWin32.cpp" line="180"/>
        <source>Minimize</source>
        <translation>Minimieren</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFramelessWindowWin32.cpp" line="192"/>
        <source>Maximize</source>
        <translation>Maximieren</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFramelessWindowWin32.cpp" line="205"/>
        <source>Restore</source>
        <translation>Wiederherstellen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayFramelessWindowWin32.cpp" line="218"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>XrayHeaderWizardPage</name>
    <message>
        <source>Header of the Report Document</source>
        <translation type="vanished">Kopfzeilen des Berichtdokuments</translation>
    </message>
    <message>
        <source>Contact Person</source>
        <translation type="vanished">Ansprechpartner</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="vanished">Name:</translation>
    </message>
    <message>
        <source>Phone:</source>
        <translation type="vanished">Telefon:</translation>
    </message>
</context>
<context>
    <name>XrayImageCropWidget</name>
    <message>
        <location filename="../../paint/XrayImageCropWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageCropWidget.ui" line="20"/>
        <source>Image Box</source>
        <translation>Bild-Box</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageCropWidget.ui" line="46"/>
        <source>Crop ROI</source>
        <translation>Beschneiden ROI</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageCropWidget.ui" line="54"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageCropWidget.ui" line="64"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageCropWidget.ui" line="74"/>
        <source>W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageCropWidget.ui" line="84"/>
        <source>H</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageCropWidget.cpp" line="95"/>
        <source>Save the cropped image to the disk.</source>
        <translation>Speichern des beschnittenen Bildes auf die Festplatte.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageCropWidget.cpp" line="97"/>
        <source>Reset ROI to image size.</source>
        <translation>Zurücksetzen ROI auf Bildgrösse.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageCropWidget.cpp" line="149"/>
        <source>Couldn&apos;t find any image or invalid ROI!</source>
        <translation>Konnte nicht irgendein Bild finden oder ROI ist ungültig!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageCropWidget.cpp" line="153"/>
        <source>Save File As...</source>
        <translation>Speichern Datei unter...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageCropWidget.cpp" line="161"/>
        <source>Image has been saved successfully!</source>
        <translation>Bild wurde erfolgreich gespeichert!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageCropWidget.cpp" line="163"/>
        <source>Couldn&apos;t save the image!</source>
        <translation>Speichern des Bildes nicht möglich!</translation>
    </message>
</context>
<context>
    <name>XrayImageFilters</name>
    <message>
        <location filename="../../paint/XrayImageFilters.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageFilters.ui" line="20"/>
        <source>Auto Adjustment</source>
        <translation>Auto-Abgleich


Auto-Anpassung</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageFilters.ui" line="28"/>
        <source>Percentage</source>
        <translation>Prozent</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageFilters.ui" line="78"/>
        <source>Reset image contrast and the parameters.</source>
        <translation>Zurücksetzen von Bildkontrast und Parametern.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageFilters.ui" line="81"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageFilters.ui" line="88"/>
        <source>Apply current changes to the original image.</source>
        <translation>Übernahme aktueller Änderungen auf das Originalbild.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageFilters.ui" line="91"/>
        <source>Apply</source>
        <translation>Übernahme</translation>
    </message>
    <message>
        <source>Couldn&apos;t find any Paint area!</source>
        <translation type="vanished">Konnte keinen Zeichenbereich vorfinden!</translation>
    </message>
</context>
<context>
    <name>XrayImageStitchingWidget</name>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="22"/>
        <source>Select Images</source>
        <translation>Bilderauswahl</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="28"/>
        <source>Browse multiple images and select two pixels that you want to align. Select a pixel in the left image and a pixel in the right image and then press Apply.</source>
        <translation>Wählen Sie mehrere Bilder aus und dann jeweils 2 Pixel zur Verbindung. Wählen Sie ein Pixel im linken und dann im rechten Bild. Dann drücken Sie Anwenden.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="38"/>
        <source>Browse for multiple images</source>
        <translation>Auwählen mehrerer Bilder</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="41"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="178"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="314"/>
        <source>Browse...</source>
        <translation>Auswählen...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="64"/>
        <source>Left Image</source>
        <translation>Linkes Bild</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="88"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="224"/>
        <source>Zoom in on the current image</source>
        <translation>Auf das aktuelle Bild heranzoomen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="91"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="101"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="111"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="124"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="227"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="237"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="247"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="260"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="98"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="234"/>
        <source>Zoom out on the current image</source>
        <translation>Aus dem aktuellen Bild herauszommen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="108"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="244"/>
        <source>Flip current image</source>
        <translation>Aktuelles Bild spiegeln</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="121"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="257"/>
        <source>Rotate current image</source>
        <translation>Aktuelles Bild drehen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="155"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="291"/>
        <source>Selected Pixel:</source>
        <translation>Ausgewählte Pixel:</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="175"/>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="311"/>
        <source>Browse for a image file</source>
        <translation>Auswählen einer Bilddatei</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="185"/>
        <source>Undo the last step</source>
        <translation>Letztes rückgängig machen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="188"/>
        <source>Undo</source>
        <translation>Rückgängig machen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="200"/>
        <source>Right Image</source>
        <translation>Rechtes Bild</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="333"/>
        <source>Move to previous image</source>
        <translation>Gehe zum vorherigen Bild</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="336"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="355"/>
        <source>Move to next image</source>
        <translation>Gehe zum nächsten Bild</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="358"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="372"/>
        <source>Save the current left image to the disk</source>
        <translation>Das aktuelle linke Bild af der Festplatte speichern</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="375"/>
        <source>Save...</source>
        <translation>Speichern...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="382"/>
        <source>Open the left image in XPaint Pro for editing.</source>
        <translation>Öffen des linken Bildes in XPaintPro zur Bearbeitung.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="385"/>
        <source>Edit in XPaint Pro</source>
        <translation>Bearbeiten in XPaint Pro</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="405"/>
        <source>Next right image will be displayed on Apply. </source>
        <translation>Nächstes rechte Bild wird angezeigt mit Übernahme.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="408"/>
        <source>Move to next image on apply</source>
        <translation>Gehe zum nächsten Bild mit Übernahme</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="418"/>
        <source>Stitch left and right image</source>
        <translation>Füge linkes und rechtes Bild zusammen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.ui" line="421"/>
        <source>Apply</source>
        <translation>Übernahme</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="81"/>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="96"/>
        <source>Selected Pixel: %1</source>
        <translation>Ausgewählte Pixel: %1</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="119"/>
        <source>Zoom in</source>
        <translation>Vergrössern</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="124"/>
        <source>Zoom out</source>
        <translation>Verkleinern</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="129"/>
        <source>Horizontal</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="131"/>
        <source>Vertical</source>
        <translation>Vertikal</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="133"/>
        <source>Both</source>
        <translation>Beide</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="144"/>
        <source>Clockwise</source>
        <translation>Im Uhrzeigersinn</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="146"/>
        <source>Counter clockwise</source>
        <translation>Entgegen Uhrzeigersinn</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="148"/>
        <source>Arbitrary...</source>
        <translation>Frei wählbar...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="152"/>
        <source>Specify the rotation angle</source>
        <translation>Spezifizieren des Rotationswinkel</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="153"/>
        <source>Angle (deg)</source>
        <translation>Winkel (grad)</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="168"/>
        <source>Select Files</source>
        <translation>Auswahl Dateien</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="168"/>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="204"/>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="226"/>
        <source>Image Files</source>
        <translation>Bild-Dateien</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="175"/>
        <source>Please select at least two images!</source>
        <translation>Bitte wählen Sie zuletzt 2 Bilder aus!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="182"/>
        <source>Demo version does not allow to work with multiple images!</source>
        <translation>Das Arbeiten mit vielen Bildern ist in der Demo-Version nicht erlaubt!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="193"/>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="217"/>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="320"/>
        <source>Left Image - </source>
        <translation>Linkes Bild - </translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="194"/>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="239"/>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="260"/>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="262"/>
        <source>Right Image - </source>
        <translation>Rechtes Bild -</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="199"/>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="220"/>
        <source>Image has been loaded!</source>
        <translation>Bild wurde hgeladen!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="204"/>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="226"/>
        <source>Select File</source>
        <translation>Wähle Datei</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="272"/>
        <source>Left image has been reset to the previous state!</source>
        <translation>Linkes Bild wurde auf vorherigen Zustand zurückgesetzt!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="283"/>
        <source>Couldn&apos;t find any image!</source>
        <translation>Konnte nicht irgendein Bild finden!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="321"/>
        <source>Left and right images are stitched successfully!</source>
        <translation>Linke und rechte Bilder erfolgreich zusammengefügt!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="360"/>
        <source>Couldn&apos;t find any image in the Left Image box!</source>
        <translation>Konnte nicht irgendein Bild in der linken Bildbox finden!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWidget.cpp" line="368"/>
        <source>Something went wrong! couldn&apos;t open.</source>
        <translation>Irgendetwas ging schief! Öffnen nicht möglich.</translation>
    </message>
    <message>
        <source>Stitched both images!</source>
        <translation type="vanished">Beide Bilder zusammengefügt!</translation>
    </message>
    <message>
        <source>Image has been saved successfully!</source>
        <translation type="vanished">Bild wurde erfolgreich gespeichert!</translation>
    </message>
    <message>
        <source>Couldn&apos;t save the image!</source>
        <translation type="vanished">Speichern des Bildes nicht möglich!</translation>
    </message>
</context>
<context>
    <name>XrayImageStitchingWindow</name>
    <message>
        <location filename="../../paint/XrayImageStitchingWindow.cpp" line="92"/>
        <source>View Help</source>
        <translation>Zeige Hilfe</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWindow.cpp" line="96"/>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWindow.cpp" line="101"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
</context>
<context>
    <name>XrayIntroWizardPage</name>
    <message>
        <source>Title:</source>
        <translation type="vanished">Titel:</translation>
    </message>
    <message>
        <source>Inspection Report</source>
        <translation type="vanished">Untersuchungsbericht</translation>
    </message>
</context>
<context>
    <name>XrayInvestigationWizardPage</name>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="656"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="725"/>
        <source>Investigation Sections of the Report Document</source>
        <translation>Untersuchungsbereiche des Berichtdokuments</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="659"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="726"/>
        <source>Analysis:</source>
        <translation>Analyse:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="663"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="727"/>
        <source>Problem:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="667"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="728"/>
        <source>Hypothetical Error:</source>
        <translation>Hypothetischer Faktor fuer die Analyse:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="671"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="729"/>
        <source>Equipment:</source>
        <translation>Ausstattung:</translation>
    </message>
    <message>
        <source>The whole room is ESD-protected that includes all equipments and the clothes.&lt;br/&gt;</source>
        <translation type="vanished">Der gesamte Bereich ist ESD-geschuetzt auch die Ausruestung und die Bekleidung.&lt;br/&gt;</translation>
    </message>
    <message>
        <source>The whole room is ESD-protected that includes all equipments and the clothes.</source>
        <translation type="vanished">Der gesamte Bereich ist ESD-geschützt auch die Ausrüstung und die Bekleidung.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="676"/>
        <source>Micromex with Max. power 20W at 180kV.</source>
        <translation>Micromex mit max. Leistung 20W bei 180kV.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="677"/>
        <source>Vtomex M  with Max. power 300W at 300kV.</source>
        <translation>Vtomex M  mit max. Leistung 300W bei 300kV.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="678"/>
        <source>Vtomex S  with Max. power 222W at 225kV.</source>
        <translation>Vtomex S  mit max. Leistung 225W bei 225kV.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="682"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="731"/>
        <source>Voltage Used:</source>
        <translation>Genutzte Spannung:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="685"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="733"/>
        <source>Voltage used in kV...</source>
        <translation>Benutzte Spannung in kV...</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="688"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="732"/>
        <source>Current Used:</source>
        <translation>Genutzte Stromstärke:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="691"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="734"/>
        <source>Current used in mA...</source>
        <translation>Stromstärke in mA...</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="708"/>
        <source>Machines:</source>
        <translation>Anlage:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="808"/>
        <source>Range of inspection: </source>
        <translation>Spektrum der Untersuchung: </translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="810"/>
        <source>This is nondestructive for all parts because the radiated power is very low.</source>
        <translation>Dies ist eine für alle Teile zerstörungsfreie Untersuchung aufgrund der geringen Leistung.</translation>
    </message>
    <message>
        <source> This is nondestructive for all parts because the radiated power is very low.</source>
        <translation type="vanished">Dies ist eine für alle Teile zerstörungsfreie Untersuchung aufgrund der geringen Leistung.</translation>
    </message>
</context>
<context>
    <name>XrayLicenseFormWidget</name>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="51"/>
        <source>Current license status</source>
        <translation>Aktueller Lizenzstatus</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="57"/>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="266"/>
        <source>License not available</source>
        <translation>Lizenz ist nicht verfügbar</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="70"/>
        <source>Demo mode</source>
        <translation>Demo-Modus</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="76"/>
        <source>Start demo version</source>
        <translation>Starte Demo-Version</translation>
    </message>
    <message>
        <source>You may start the application is demo mode. In demo mode, the application will run for 10 minutes and some of the features will be disabled.</source>
        <translation type="vanished">Sie können die Software im Demo-Modus starten. Im Demo-Modus läuft die Software für 10 Minuten und ein paar Möglichkeiten sind ausgeschaltet.</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="99"/>
        <source>Apply for a new license</source>
        <translation>Übernahme einer neuen Lizenz</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="105"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To apply for a new license, please create registeration form, fill out form, and send the fully filled out form to the email address: &lt;span style=&quot; font-weight:600;&quot;&gt;request@xray-lab.com&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Übernahme einer neuen Lizenz, bitte führe eine Registrierung durch, fülle dazu das Format aus und sende das vollausgefüllte Format zu der e-mail-Adresse:&lt;span style=&quot; font-weight:600;&quot;&gt;request@xray-lab.com&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="115"/>
        <source>Create registration form</source>
        <translation>Erzeuge das Registrierungsformat</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="128"/>
        <source>Exisiting license</source>
        <translation>Vorhandene Lizenz</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="134"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you already have a license file, you can check the status of the license.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wenn Sie über eine Lizenzdatei verfügen, können Sie den Status der Lizenz überprüfen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="144"/>
        <source>Check the exisiting license</source>
        <translation>Überprüfung der vorhandenen Lizenz</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="163"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;In case that you are not interested in one of the above options, please click Exit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Für den Fall, daß Sie nicht an einer der darüber angegebenen Optionen interessiert sind, klicken Sie bitte auf auf Abbruch.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="204"/>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="678"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="211"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="224"/>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="438"/>
        <source>Information</source>
        <translation></translation>
    </message>
    <message>
        <source>You need a license file to be able to use the software. You can request this license by filling out the following form and email the saved file to us. With the information you give us we will generate the license file which we will send to you via email.</source>
        <translation type="vanished">Sie benötigen eine Lizenzdatei, um die Software benutzen zu können. Sie können diese Lizenz mit dem Ausfüllen des folgenden Formats und schicken einer e-mail zu uns anfragen. Mit der Information ermöglichen Sie uns, eine Lizenzdatei für Sie zu erzeugen, welche...</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="86"/>
        <source>You may start the application in demo mode. In demo mode, some of the features will be disabled.</source>
        <translation>Sie können die Software im Demo-Modus starten. Im Demo-Modus sind ein paar Möglichkeiten sind ausgeschaltet.</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="230"/>
        <source>You need a license file to be able to use the disabled features of the software. You can request this license by filling out the following form and email the saved file to us. With the information you give us we will generate the license file which we will send to you via email.</source>
        <translation>Sie benötigen eine Lizenzdatei, um ausgeschaltete Module der Software benutzen zu können. Sie können diese Lizenz mit dem Ausfüllen des folgenden Formats und Schicken einer e-mail zu uns anfragen. Mit der Information ermöglichen Sie uns, eine Lizenzdatei für Sie zu erzeugen, welche wir Ihnen per E-mail zurückschicken.</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="243"/>
        <source>* : Required fields</source>
        <translation>*: Erforderliche Felder</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="251"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Full name: &lt;font color= &quot;red&quot;&gt;*&lt;/font&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Vollständiger Name: &lt;font color=&quot;red&quot;&gt;*&lt;/font&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="261"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="288"/>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="479"/>
        <source>Company:</source>
        <translation>Firma:</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="298"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="308"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Email: &lt;font color= &quot;red&quot;&gt;*&lt;/font&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="318"/>
        <source>Phone:</source>
        <translation>Telefon:</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="339"/>
        <source>Hardware key</source>
        <translation>Hardwareschlüssel</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="364"/>
        <source>Comments</source>
        <translation>Kommentare</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="411"/>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="671"/>
        <source>Go back</source>
        <translation>Gehe zurück</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="418"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="425"/>
        <source>Send by email</source>
        <translation>Sende mit e-mail</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="444"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ⓒ Copyright 2019 by XRAY-LAB GmbH &amp;amp; Co. Kg. All content, including but not limited to graphics and source code are protected by law. All rights reserved, including but not limited to: reproduction, publication, adaptation, and translation in any other language.&lt;br/&gt;&lt;br/&gt;Please browse and select the file that you have recieved from us.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ⓒ Copyright 2019 by XRAY-LAB GmbH &amp;amp; Co.Kg. Der gesamte Inhalt inkl. - aber nicht limitiert bzgl. - Graphik und Quellcode sind durch Gesetz geschützt. Alle Rechte vorbehalten inkl. - aber nicht limitiert bzgl.:- Reproduktion, Veröffentlichung, Anpassung und Übersetzung in eine andere Sprache.&lt;br/&gt;&lt;br/&gt;Bitte wählen Sie mit Durchsuchen die Datei aus, welche Sie von uns erhalten haben.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="457"/>
        <source>Customer</source>
        <translation>Kunde</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="465"/>
        <source>Full name:</source>
        <translation>Vollständiger Name:</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="472"/>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="486"/>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="500"/>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="527"/>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="541"/>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="555"/>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="574"/>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="588"/>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="602"/>
        <source>n/a</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="493"/>
        <source>Email:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="512"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="520"/>
        <source>License-id:</source>
        <translation>Lizenz-ID:</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="534"/>
        <source>Serial:</source>
        <translation>Serie:</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="548"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="562"/>
        <source>Expire:</source>
        <translation>Ablauf:</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="581"/>
        <source>Issue:</source>
        <translation>Angelegenheit:</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="595"/>
        <source>Modules:</source>
        <translation>Module:</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="617"/>
        <source>License file</source>
        <translation>Lizenzdatei</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="623"/>
        <source>Browse...</source>
        <translation>Durchsuchen...</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.ui" line="630"/>
        <source>Path:</source>
        <translation>Pfad:</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="195"/>
        <source>License available but not registered with this hardware</source>
        <translation>Lizenz verfügbar, aber nicht auf diese Hardware registriert</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="209"/>
        <source>License available but not registered for this application/version</source>
        <translation>Lizenz verfügbar, aber nicht auf diese Anwendung/Version registriert</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="238"/>
        <source>Never</source>
        <translation>Unbegrenzt</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="240"/>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="256"/>
        <source>License available</source>
        <translation>Lizenz verfügbar</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="249"/>
        <source>License available but expired</source>
        <translation>Lizenz verfügbar, aber beendet</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="283"/>
        <source>Select File</source>
        <translation>Wähle Datei</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="283"/>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="348"/>
        <source>All Supported Files (*.dat)</source>
        <translation>Alle unterstützte Dateien (*dat)</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="296"/>
        <source>Please fill in required fields (Full name: *)!</source>
        <translation>Bitte in erforderliche Felder eingeben (Vollständiger Name:*)!</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="301"/>
        <source>Please fill in required fields (Email: *)!</source>
        <translation>Bitte in erforderliche Felder eingeben (Email:*)!</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="348"/>
        <source>Save File As...</source>
        <translation>Speichern Datei unter...</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="328"/>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="330"/>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="332"/>
        <source>Something went wrong!</source>
        <translation>Irgendetwas ist schief gelaufen!</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="52"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To apply for a new license, please create registration form, fill out form, and send the fully filled out form to the email address: &lt;span style=&quot; font-weight:600;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Übernahme einer neuen Lizenz, bitte führe eine Registrierung durch, fülle dazu das Format aus und sende das vollausgefüllte Format zu der e-mail-Adresse: &lt;span style=&quot; font-weight:600;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="53"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ⓒ Copyright %1. All content, including but not limited to graphics and source code are protected by law. All rights reserved, including but not limited to: reproduction, publication, adaptation, and translation in any other language.&lt;br/&gt;&lt;br/&gt;Please browse and select the file that you have recieved from us.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ⓒ Copyright %1. Der gesamte Inhalt inkl. - aber nicht limitiert bzgl. - Graphik und Quellcode sind durch Gesetz geschützt. Alle Rechte vorbehalten inkl. - aber nicht limitiert bzgl.:- Reproduktion, Veröffentlichung, Anpassung und Übersetzung in eine andere Sprache.&lt;br/&gt;&lt;br/&gt;Bitte wählen Sie mit Durchsuchen die Datei aus, welche Sie von uns erhalten haben.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="338"/>
        <source>Couldn&apos;t save hardware key file to the disk!</source>
        <translation>Konnte nicht die Datei mit dem Hardwareschlüssel auf der Festplatte speichern!</translation>
    </message>
    <message>
        <location filename="../../license/XrayLicenseFormWidget.cpp" line="340"/>
        <source>Hardware key file has been saved successfully!</source>
        <translation>Datei mit dem Hardwareschlüssel wurde erfolgreich gespeichert!</translation>
    </message>
</context>
<context>
    <name>XrayMainPaintWidget</name>
    <message>
        <location filename="../../paint/XrayMainPaintWidget.cpp" line="1189"/>
        <source>Brush</source>
        <translation>Pinsel</translation>
    </message>
    <message>
        <source>Curve</source>
        <translation type="vanished">Kurve</translation>
    </message>
    <message>
        <location filename="../../paint/XrayMainPaintWidget.cpp" line="1198"/>
        <source>Rectangle</source>
        <translation>Rechteck</translation>
    </message>
    <message>
        <location filename="../../paint/XrayMainPaintWidget.cpp" line="1201"/>
        <source>Circle</source>
        <translation>Kreis</translation>
    </message>
    <message>
        <location filename="../../paint/XrayMainPaintWidget.cpp" line="1204"/>
        <source>Oval</source>
        <translation>Oval</translation>
    </message>
    <message>
        <location filename="../../paint/XrayMainPaintWidget.cpp" line="1207"/>
        <source>Polygon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayMainPaintWidget.cpp" line="1192"/>
        <source>Line</source>
        <translation>Linie</translation>
    </message>
    <message>
        <location filename="../../paint/XrayMainPaintWidget.cpp" line="1195"/>
        <source>Arrow</source>
        <translation>Pfeil</translation>
    </message>
    <message>
        <location filename="../../paint/XrayMainPaintWidget.cpp" line="1210"/>
        <source>Text</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>XrayMainWindow</name>
    <message>
        <source>Are you sure you want to quit?</source>
        <translation type="vanished">Willst du wirklich abbrechen?</translation>
    </message>
    <message>
        <source>Log out</source>
        <translation type="vanished">Ausloggen</translation>
    </message>
    <message>
        <source>User Profile</source>
        <translation type="vanished">Benutzer Profil</translation>
    </message>
    <message>
        <source>View Help</source>
        <translation type="vanished">Zeige Hilfe</translation>
    </message>
    <message>
        <source>Licenses...</source>
        <translation type="vanished">Lizensen...</translation>
    </message>
    <message>
        <source>Check for Updates</source>
        <translation type="vanished">Nach Aktualisierungen suchen</translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="vanished">Webseite</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Über uns</translation>
    </message>
    <message>
        <source>Clear Settings</source>
        <translation type="vanished">Lösche Einstellungen</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">Englisch</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="vanished">Deutsch</translation>
    </message>
    <message>
        <source>Please restart the application to see the change.</source>
        <translation type="vanished">Bitte starte das Programm neu, um die Änderung zu sehen.</translation>
    </message>
    <message>
        <source>You will need to restart the application to see the %1 language.</source>
        <translation type="vanished">Bitte starte das Programm neu, um die Sprachänderungen zu sehen</translation>
    </message>
    <message>
        <source>Japanese</source>
        <translation type="vanished">Japanisch</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="vanished">Chinesisch</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Sprache</translation>
    </message>
    <message>
        <source>Current Language changed to %1</source>
        <translation type="vanished">Derzeitige Sprache geändert in %1</translation>
    </message>
    <message>
        <source>Licensed for commercial use &lt;a href=&quot;https://xray-lab.com&quot;&gt;(License terms)&lt;/a&gt;</source>
        <translation type="vanished">Lizensiert für die kommerzielle Benutzung &lt;a href=&quot;https://xray-lab.com&quot;&gt;(Lizenzbedingungen)&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Unlicensed for commercial use &lt;a href=&quot;https://xray-lab.com&quot;&gt;(License terms)&lt;/a&gt;</source>
        <translation type="vanished">Nicht lizensiert für die kommerzielle Benutzung &lt;a href=&quot;https://xray-lab.com&quot;&gt;(Lizenzbedingungen)&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWindow.cpp" line="119"/>
        <location filename="../../report/XrayBatchWaterMarkerWindow.cpp" line="119"/>
        <location filename="../../report/XrayQuickReportGeneratorWindow.cpp" line="121"/>
        <location filename="../../watcher/XrayDeleteProjectFilesWindow.cpp" line="137"/>
        <source>Licensed for commercial use &lt;a href=&quot;https://xray-lab.com/us/eula-english&quot;&gt;(License terms)&lt;/a&gt;</source>
        <translation>Lizensiert für die kommerzielle Benutzung &lt;a href=&quot;https://xray-lab.com/us/eula-german&quot;&gt;(Lizenzbedingungen) &lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../paint/XrayImageStitchingWindow.cpp" line="121"/>
        <location filename="../../report/XrayBatchWaterMarkerWindow.cpp" line="121"/>
        <location filename="../../report/XrayQuickReportGeneratorWindow.cpp" line="123"/>
        <location filename="../../watcher/XrayDeleteProjectFilesWindow.cpp" line="139"/>
        <source>Unlicensed for commercial use &lt;a href=&quot;https://xray-lab.com/us/eula-english&quot;&gt;(License terms)&lt;/a&gt;</source>
        <translation>Nicht lizensiert für die kommerzielle Benutzung &lt;a href=&quot;https://xray-lab.com/us/eula-german&quot;&gt;(Lizenzbedingungen) &lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>XrayPaintMainWindow</name>
    <message>
        <source>Options</source>
        <translation type="vanished">Optionen</translation>
    </message>
    <message>
        <source>File Caption</source>
        <translation type="vanished">File Caption -de</translation>
    </message>
    <message>
        <source>Open a dialog to specify the image caption.</source>
        <translation type="vanished">Dialog zur Bildbeschriftung öffnen.</translation>
    </message>
    <message>
        <source>New File</source>
        <translation type="vanished">New File de</translation>
    </message>
    <message>
        <source>Open an existing file.</source>
        <translation type="vanished">Öffne eine vorhandene Datei.</translation>
    </message>
    <message>
        <source>Save As...</source>
        <translation type="vanished">Speichern unter...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1162"/>
        <source>Pictures Per Page...</source>
        <translation>Bilder pro Seite...</translation>
    </message>
    <message>
        <source>How many pictures you want in one page? Open a dialog to specify the number.</source>
        <translation type="vanished">Wieviele Bilder wollen Sie auf 1 Seite haben? Öffnet einen Dialog um die Anzahl zu spezifizieren.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="654"/>
        <source>Print...</source>
        <translation>Drucken...</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Beenden</translation>
    </message>
    <message>
        <source>Close the main application.</source>
        <translation type="vanished">Schließe das Programm.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="662"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="696"/>
        <source>Undo</source>
        <translation>Rückgängig machen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="700"/>
        <source>Redo</source>
        <translation>Wiederherstellen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="704"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="708"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Löschen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="781"/>
        <source>Resize...</source>
        <translation>Größe ändern...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="785"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="813"/>
        <source>Smooth Edges</source>
        <translation>Abgerundete Ecken</translation>
    </message>
    <message>
        <source>Apply anti-aliasing to smooth edges.)</source>
        <translation type="vanished">Anwendung Anti-Aliiasing zur Eck-Abrundung.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="811"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="921"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1108"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1120"/>
        <source>Empty</source>
        <translation>Leer</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1109"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1121"/>
        <source>Solid</source>
        <translation>Durchgezogen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1110"/>
        <source>Dash</source>
        <translation>Gestrichelt</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1111"/>
        <source>Dot</source>
        <translation>Gepunktet</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1112"/>
        <source>Dash dot</source>
        <translation>Gestrichelt und gepunktet</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1113"/>
        <source>Dash dot dot</source>
        <translation>Gestrichelt gepunktet gepunktet</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1168"/>
        <source>Picture Caption and Number</source>
        <translation>Bildbeschreibung und -nummer</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1436"/>
        <source>Fill color</source>
        <translation>Fülle mit Farbe</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1206"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2164"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2171"/>
        <source>Selected shape</source>
        <translation>Ausgewählte Form</translation>
    </message>
    <message>
        <source>Outline</source>
        <translation type="vanished">Konturlinie</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Farbe</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Einstellungen</translation>
    </message>
    <message>
        <source>Style</source>
        <translation type="vanished">Stil</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation type="vanished">Fülle</translation>
    </message>
    <message>
        <source>Pattern</source>
        <translation type="vanished">Muster</translation>
    </message>
    <message>
        <source>Untitled - Paint</source>
        <translation type="vanished">Unbetitelt - Paint</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1207"/>
        <source>Area</source>
        <translation>Fläche</translation>
    </message>
    <message>
        <source>Caption</source>
        <translation type="vanished">Beschriftung</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="603"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <source>Create a new file.</source>
        <translation type="vanished">Erzeugen einer neue Datei.</translation>
    </message>
    <message>
        <source>Open...</source>
        <translation type="vanished">Öffnen...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="646"/>
        <source>Save changes on the original file.</source>
        <translation>Speichern Änderungen an der Originaldatei.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="651"/>
        <source>Save the selected file as another name or format.</source>
        <translation>Speichern der ausgewählte Datei mit anderen Namen oder Format.</translation>
    </message>
    <message>
        <source>Images Per Page...</source>
        <translation type="vanished">Bilder pro Seite...</translation>
    </message>
    <message>
        <source>How many images you want per page? Open a dialog to specify in number.</source>
        <translation type="vanished">Wieviele Bilderwollen Sie auf 1 Seite haben? Ein Dialog zur Eingabe wird geöffnet.</translation>
    </message>
    <message>
        <source>Outline shape color</source>
        <translation type="vanished">Konturlinienfarbe</translation>
    </message>
    <message>
        <source>Size (1)</source>
        <translation type="vanished">Größe (1)</translation>
    </message>
    <message>
        <source>Opacity (255)</source>
        <translation type="vanished">Durchlässigkeit (255)</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1315"/>
        <source>Round Cap</source>
        <translation>Abrundung</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1319"/>
        <source>Flat Cap</source>
        <translation>Gerademachen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1122"/>
        <source>Pattern #1</source>
        <translation>Muster #1</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="72"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="109"/>
        <source>Modules</source>
        <translation>Modul</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="604"/>
        <source>Create a new project.</source>
        <translation>Neues Projekt erstellen.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="608"/>
        <source>Open Project...</source>
        <translation>Öffnen Projekt...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="609"/>
        <source>Open an existing project.</source>
        <translation>Öffnen eines vorhandenen Projekts.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="613"/>
        <source>Close Project</source>
        <translation>Projekt schließen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="614"/>
        <source>Close the project with all pictures.</source>
        <translation>Projekt und alle Bilder schließen.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="617"/>
        <source>Save Project</source>
        <translation>Speichern Projekt</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="618"/>
        <source>Save current settings to the last saved project.</source>
        <translation>Speichern aktueller Einstellungen zu dem zuletzt gespeicherten Projekts.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="622"/>
        <source>Save Project As...</source>
        <translation>Speichern Projekt unter...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="623"/>
        <source>Save current settings as a project.</source>
        <translation>Speichern aktueller Einstellungen als ein Projekt.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="626"/>
        <source>Open Pictures...</source>
        <translation>Öffnen Bilder...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="627"/>
        <source>Open the existing file(s).</source>
        <translation>Öffnen der vorhandenen Datei(en).</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="631"/>
        <source>Open Directory...</source>
        <translation>Öffnen Verzeichnis...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="632"/>
        <source>Open the existing file(s) from the directory.</source>
        <translation>Öffnen der vorhandenen Datei(en) von dem Verzeichnis.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="636"/>
        <source>Reload Current Picture</source>
        <translation>Neuladen aktuelles Bild</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="637"/>
        <source>Reload the current picture from disk.</source>
        <translation>Neuladen aktuelles Bild von Festplatte.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="642"/>
        <source>Close the current picture tab.</source>
        <translation>Schließen des aktuellen Bild-Tabs.</translation>
    </message>
    <message>
        <source>Close All</source>
        <translation type="vanished">Schließen alles</translation>
    </message>
    <message>
        <source>Close all pictures tabs.</source>
        <translation type="vanished">Schließen aller Bild-Tabs.</translation>
    </message>
    <message>
        <source>Save Picture</source>
        <translation type="vanished">Speichern Bild</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="650"/>
        <source>Save Picture As...</source>
        <translation>Speichern Bild unter...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="665"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="686"/>
        <source>Recent Project Files</source>
        <translation>Letzte Projektdateien</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="691"/>
        <source>Recent Picture Files</source>
        <translation>Letzte Bilddateien</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="712"/>
        <source>Delete All Shapes</source>
        <translation>Löschen aller Formen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="716"/>
        <source>Delete Selected Shape</source>
        <translation>Löschen ausgewählte Form</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="782"/>
        <source>Resize the picture.</source>
        <translation>Größenanpassung des Bildes.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="835"/>
        <source>Fit to screen. [Ctrl+0]</source>
        <translation>Einpassen. [Strg+O]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="841"/>
        <source>Fit all to screen</source>
        <translation>Alles einpassen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="842"/>
        <source>Fit all to screen. [Ctrl+Shift+0]</source>
        <translation>Alles einpassen. [Strg+Shift+O]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="851"/>
        <source>Transparent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="852"/>
        <source>Set transparent background.</source>
        <translation>Setzen transparenten Hintergrund.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="866"/>
        <source>White</source>
        <translation>Weiß</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="867"/>
        <source>Set white background.</source>
        <translation>Setzen weißen Hintergrung.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="882"/>
        <source>Set grid as background.</source>
        <translation>Setzen Raster als Hintergrund.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="849"/>
        <source>Background</source>
        <translation>Hintergrund</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="820"/>
        <source>Zoom in</source>
        <translation>Vergrössern</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="821"/>
        <source>Zoom in on the current picture. [Ctrl+PgUp]</source>
        <translation>Vergrössern des aktuelle Bildes. [Strg+Bild-Auf)</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="827"/>
        <source>Zoom out</source>
        <translation>Verkleinern</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="828"/>
        <source>Zoom out on the current picture. [Ctrl+PgDn]</source>
        <translation>Verkleinern des aktuellen Bildes. [Strg+Bild-Hinunter]</translation>
    </message>
    <message>
        <source>Zoom to 100%</source>
        <translation type="vanished">Ansicht zu 100%</translation>
    </message>
    <message>
        <source>Zoom to 100%.</source>
        <translation type="vanished">Ansicht zu 100%.</translation>
    </message>
    <message>
        <source>File toolbar</source>
        <translation type="vanished">Datei Werkzeugleiste</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="936"/>
        <source>Shapes</source>
        <translation>Formen</translation>
    </message>
    <message>
        <source>Click here for shape selection.</source>
        <translation type="vanished">Klicke hier für die Formauswahl.</translation>
    </message>
    <message>
        <source>Pick a color from the picture and use it for drawing.</source>
        <translation type="vanished">Entnehme eine Farbe vom Bild für das Zeichnen.</translation>
    </message>
    <message>
        <source>Insert line into the picture.</source>
        <translation type="vanished">Setze Linie auf das Bild.</translation>
    </message>
    <message>
        <source>Insert rectangle into the picture.</source>
        <translation type="vanished">Setze Rechteck auf das Bild.</translation>
    </message>
    <message>
        <source>Insert oval into the picture.</source>
        <translation type="vanished">Setze Oval auf das Bild.</translation>
    </message>
    <message>
        <source>Insert polygon into the picture.</source>
        <translation type="vanished">Setze Polygon auf das Bild.</translation>
    </message>
    <message>
        <source>Insert arrow into the picture.</source>
        <translation type="vanished">Setze Pfeil auf das Bild.</translation>
    </message>
    <message>
        <source>Insert text into the picture.</source>
        <translation type="vanished">Setze Text auf das Bild.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1045"/>
        <source>Change the font family.</source>
        <translation>Ändern der Schriftfamilie.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1055"/>
        <source>Change the font size.</source>
        <translation>Ändern der Schriftgröße.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1058"/>
        <source>Bold</source>
        <translation>Fett</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1063"/>
        <source>Change to the heavier font.</source>
        <translation>Formatieren Sie Text fett.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1066"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1069"/>
        <source>Change to an italic font.</source>
        <translation>Formatieren Sie Text kursiv.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1072"/>
        <source>Underline</source>
        <translation>Unterstreichen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1075"/>
        <source>Draw a line below the text.</source>
        <translation>Unterstreichen Sie den Text.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1078"/>
        <source>Font</source>
        <translation>Schrift</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1092"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1368"/>
        <source>Select the medium for the shape fill.</source>
        <translation>Auswählen der Farbe für die Füllung der Form aus.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1101"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1236"/>
        <source>Select the medium for the shape outline/text.</source>
        <translation>Auswählen der Farbe für die Formkontur /Text.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1106"/>
        <source>Select the outline style.</source>
        <translation>Auswählen des Konturstils.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1118"/>
        <source>Select the shape fill pattern.</source>
        <translation>Auswählen des Musters zur Füllung der Form.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1123"/>
        <source>Pattern #2</source>
        <translation>Muster #2</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1124"/>
        <source>Pattern #3</source>
        <translation>Muster #3</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1125"/>
        <source>Pattern #4</source>
        <translation>Muster #4</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1126"/>
        <source>Pattern #5</source>
        <translation>Muster #5</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1127"/>
        <source>Pattern #6</source>
        <translation>Muster #6</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1128"/>
        <source>Pattern #7</source>
        <translation>Muster #7</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1129"/>
        <source>Horizontal lines</source>
        <translation>Horizontal-Linien</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1130"/>
        <source>Vertical lines</source>
        <translation>Vertikal-Linien</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="881"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="894"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1131"/>
        <source>Grid</source>
        <translation>Raster</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="641"/>
        <source>Close Current Picture</source>
        <translation>Aktuelles Bild schließen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="645"/>
        <source>Save a Copy</source>
        <translation>Speichern einer Kopie</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="926"/>
        <source>File Toolbar</source>
        <translation>Datei Toolbar</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="973"/>
        <source>Insert circle into the picture. [Key 6]</source>
        <translation>Einfügen Kreis in das Bild. [Taste 6]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="979"/>
        <source>Insert oval into the picture. [Key 7]</source>
        <translation>Einfügen Ellipse in das Bild. [Taste 7]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="985"/>
        <source>Insert polygon into the picture. [Key 8]
(Right mouse click to close it!)</source>
        <translation>Einfügen Polygon in das Bild. [Taste 8]
(Rechte Maustaste zum Schließen!)</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="991"/>
        <source>Insert text into the picture. [Key 9]</source>
        <translation>Einfügen Text in das Bild. [Taste 9]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="997"/>
        <source>Insert void into the selected shape with curve selection. [Key 0]</source>
        <translation>Einfügen Fehlstelle in die ausgewählte Form mit Krümmungsauswahl. [Taste 0]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1003"/>
        <source>Insert void into the selected shape with polygon selection. [Key -]
(Right mouse click to close it!)</source>
        <translation>Einfügen Fehlstelle in die ausgewählte Form mit Polygonauswahl. [Taste -]
(Rechte Maustaste zum Schließen!)</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1009"/>
        <source>Insert void into the selected shape with circular selection. [Key =]</source>
        <translation>Einfügen Fehlstelle in die ausgewählte Form mit Kreisauswahl. [Taste =]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1132"/>
        <source>Diagonal line #1</source>
        <translation>Diagonal-Linie #1</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1133"/>
        <source>Diagonal line #2</source>
        <translation>Diagonal-Linie #2</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1134"/>
        <source>Diagonal grid</source>
        <translation>Diagonal-Raster</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1139"/>
        <source>Select the arrow direction.</source>
        <translation>Auswählen der Peilrichtung.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1141"/>
        <source>Left arrow</source>
        <translation>Linker Pfeil</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1142"/>
        <source>Right arrow</source>
        <translation>Rechter Pfeil</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1143"/>
        <source>Both arrows</source>
        <translation>Beidseitiger Pfeil</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1147"/>
        <source>Shape Color and Style</source>
        <translation>Form- Farbe und -Stil</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1015"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1847"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1869"/>
        <source>Edit Text</source>
        <translation>Bearbeite Text</translation>
    </message>
    <message>
        <source>Open a dialog to edit the text of the selected text-box. [ShiftKey + T]</source>
        <translation type="vanished">Öffne einen Dialog zur Textbearbeitung von der ausgewählten Text-Box.[Shift-Taste+T]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1156"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2389"/>
        <source>Picture Caption</source>
        <translation>Bildbeschriftung</translation>
    </message>
    <message>
        <source>Open a dialog to edit the picture caption. [AltKey + C]</source>
        <translation type="vanished">Öffne einen Dialog zur Bildbeschriftung.[Alt-Taste+C]</translation>
    </message>
    <message>
        <source>Picture Text</source>
        <translation type="vanished">Bild Text</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1277"/>
        <source>Adjust the opacity for the shape outline/text.</source>
        <translation>Anpassen der Durchlässigkeit für die Konturlinie/Text.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1291"/>
        <source>Adjust the width for the shape outline.</source>
        <translation>Anpassen der Breite für die Konturlinie.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1305"/>
        <source>Adjust the size for the shape arrow(s).</source>
        <translation>Anpassen der Größe für Pfeil(e)der Form.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1208"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2280"/>
        <source>Total area</source>
        <translation>Gesamtfläche</translation>
    </message>
    <message>
        <source>Area of all shapes</source>
        <translation type="vanished">Fläche alle Formen</translation>
    </message>
    <message>
        <source>Image Caption</source>
        <translation type="vanished">Bildbeschriftung</translation>
    </message>
    <message>
        <source>Caption:</source>
        <translation type="vanished">Beschriftung:</translation>
    </message>
    <message>
        <source>Image caption has been recorded successfully.</source>
        <translation type="vanished">Bildbeschriftung erfolgreich vollzogen.</translation>
    </message>
    <message>
        <source>Images In One Page</source>
        <translation type="vanished">Bilder auf 1 Seite</translation>
    </message>
    <message>
        <source>Images Per Page:</source>
        <translation type="vanished">Bilder pro Seite:</translation>
    </message>
    <message>
        <source>Image Per Page has been recorded successfully.</source>
        <translation type="vanished">Eingabe der Bilder pro Seite war erfolgreich.</translation>
    </message>
    <message>
        <source>Information: %1File(s) are already opened!</source>
        <translation type="vanished">Information:%1Datei(n) sind bereits geöffnet!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2623"/>
        <source>Select Files</source>
        <translation>Auswahl Dateien</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2623"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="439"/>
        <source>Image Files</source>
        <translation>Bild-Dateien</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2348"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2756"/>
        <source>Save File As...</source>
        <translation>Datei speichern unter...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2998"/>
        <source>Select Shape Color</source>
        <translation>Auswahl Formfarbe</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="3032"/>
        <source>Select Fill Color</source>
        <translation>Auswahl Füllfarbe</translation>
    </message>
    <message>
        <source>Opacity</source>
        <translation type="vanished">Durchlässigkeit</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Größe</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="814"/>
        <source>Apply anti-aliasing to smooth edges.</source>
        <translation>Anwenden Anti-Aliasing zur Eckabrundung.</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Wähle</translation>
    </message>
    <message>
        <source>Brush</source>
        <translation type="vanished">Pinsel</translation>
    </message>
    <message>
        <source>Line</source>
        <translation type="vanished">Linie</translation>
    </message>
    <message>
        <source>Rectangle</source>
        <translation type="vanished">Rechteck</translation>
    </message>
    <message>
        <source>Oval</source>
        <translation type="vanished">Ellipse</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="3105"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Recent Files</source>
        <translation type="vanished">Letzte Dateien</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="721"/>
        <source>Clockwise</source>
        <translation>Im Uhrzeigersinn</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="724"/>
        <source>Counter clockwise</source>
        <translation>Entgegen Uhrzeigersinn</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="727"/>
        <source>Arbitrary...</source>
        <translation>Frei wählbar...</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="734"/>
        <source>Specify the rotation angle</source>
        <translation>Spezifizieren des Rotationswinkel</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="735"/>
        <source>Angle (deg)</source>
        <translation>Winkel (grad)</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="740"/>
        <source>Horizontal</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="742"/>
        <source>Vertical</source>
        <translation>Vertikal</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="744"/>
        <source>Both</source>
        <translation>Beide</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="747"/>
        <source>Invert</source>
        <translation>Invertieren</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="760"/>
        <source>Crop</source>
        <translation>Zuschneiden</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="762"/>
        <source>Crop the picture so it only contains the current selection.</source>
        <translation>Bild so zuschneiden, daß es nur die aktuelle Auswahl enthält.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="794"/>
        <source>Rotate</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="800"/>
        <source>Flip</source>
        <translation>Spiegeln</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="834"/>
        <source>Fit to screen</source>
        <translation>Einpassen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="887"/>
        <source>Crosshair</source>
        <translation>Fadenkreuz</translation>
    </message>
    <message>
        <source>Toggle cross-hair in the paint area.</source>
        <translation type="vanished">Einschalten Fadenkreuz in Zeichenbereich </translation>
    </message>
    <message>
        <source>Select a shape.</source>
        <translation type="vanished">Wählen Sie eine Form aus.</translation>
    </message>
    <message>
        <source>Insert a curve into the picture.</source>
        <translation type="vanished">Einfügen einer Freihandlinie in das Bild.</translation>
    </message>
    <message>
        <source>Insert polygon into the picture. (Right mouse click to close it!)</source>
        <translation type="vanished">Einfügen Polygon in das Bild. (Rechte Maustaste zum Schließen!)</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1017"/>
        <source>Open a dialog to edit the text of the selected text-box. [Shift+T]</source>
        <translation>Öffne einen Dialog zur Textbearbeitung von der ausgewählten Text-Box.[Shift+T]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1157"/>
        <source>Open a dialog to edit the picture caption. [Alt+C]</source>
        <translation>Öffne einen Dialog zur Bildbeschriftung.[Alt+C]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1163"/>
        <source>How many pictures you want in one page? Open a dialog to specify the number. [Alt+I]</source>
        <translation>Wieviele Bilderwollen Sie auf 1 Seite haben? Ein Dialog zur Eingabe wird geöffnet. [Alt+I]</translation>
    </message>
    <message>
        <source>Input Dialogs</source>
        <translation type="vanished">Eingabe Dialoge</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1180"/>
        <source>Zoom in/out on the current picture.</source>
        <translation>Hinein-/Herauszoomen in/ aus das aktuelle Bild.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1184"/>
        <source>Zoom</source>
        <translation>Zoomen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1212"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1350"/>
        <source>Outline color</source>
        <translation>Konturfarbe</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1362"/>
        <source>Outline settings</source>
        <translation>Kontureinstellungen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1448"/>
        <source>Shape</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1451"/>
        <source>XEnhancer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1454"/>
        <source>XVoid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>black</source>
        <translation>schwarz</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>white</source>
        <translation>weiß</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>darkGray</source>
        <translation>dunkelgrau</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>gray</source>
        <translation>grau</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>lightGray</source>
        <translation>hellgrau</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>red</source>
        <translation>rot</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>green</source>
        <translation>grün</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>blue</source>
        <translation>blau</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>cyan</source>
        <translation>zyan</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>magenta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>yellow</source>
        <translation>gelb</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>darkRed</source>
        <translation>dunkelrot</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>darkGreen</source>
        <translation>dunkelgrün</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>darkBlue</source>
        <translation>dunkelblau</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>darkCyan</source>
        <translation>dunkelzyan</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>darkMagenta</source>
        <translation>dunkelmagenta</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1706"/>
        <source>darkYellow</source>
        <translation>dunkelgelb</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1848"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1870"/>
        <source>Enter Text</source>
        <translation>Eintragen Text</translation>
    </message>
    <message>
        <source>Please select a text box first!</source>
        <translation type="vanished">Bitte wählen Sie eine Text-Box zuerst aus!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1898"/>
        <source>Untitled</source>
        <translation>Unbetitelt</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2165"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2172"/>
        <source>ROI</source>
        <translation>Interessensregion</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2314"/>
        <source>Select File</source>
        <translation>Auswählen Datei</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2329"/>
        <source>Project settings have been saved successfully!</source>
        <translation>Projekteinstellungen erfolgreich gespeichert!</translation>
    </message>
    <message>
        <source>Couldn&apos;t save project settings!</source>
        <translation type="vanished">Konnte aktuelle Projekteinstellungen nicht speichern!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2388"/>
        <source>Edit Picture Caption</source>
        <translation>Bearbeiten Bildbeschriftung</translation>
    </message>
    <message>
        <source>Edit Images Per Page</source>
        <translation type="vanished">Bearbeiten Bilder pro Seite</translation>
    </message>
    <message>
        <source>Images Per Page</source>
        <translation type="vanished">Bilder pro Seite</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2633"/>
        <source>Open Directory</source>
        <translation>Öffnen Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="3106"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="889"/>
        <source>Toggle crosshair in the paint area.</source>
        <translation>Einschalten von Fadenkreuz in den Zeichenbereich.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="896"/>
        <source>Toggle grid in the paint area.</source>
        <translation>Einschalten von Raster in den Zeichenbereich.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="941"/>
        <source>Click here for selection shapes. [Key 1]</source>
        <translation>Klick hier für Formenauswahl.[Taste 1]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="949"/>
        <source>Insert curve into the picture. [Key 2]</source>
        <translation>Einfügen Kurve in das Bild. [Taste 2]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="955"/>
        <source>Insert line into the picture. [Key 3]</source>
        <translation>Einfügen Linie in das Bild. [Taste 3]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="961"/>
        <source>Insert arrow into the picture. [Key 4]</source>
        <translation>Einfügen Pfeil in das Bild. [Taste 4]</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="967"/>
        <source>Insert rectangle into the picture. [Key 5]</source>
        <translation>Einfügen Rechteck in das Bild. [Taste 5]</translation>
    </message>
    <message>
        <source>Insert circle into the picture. [Key 5]</source>
        <translation type="vanished">Einfügen Kreis in das Bild. [Taste 5]</translation>
    </message>
    <message>
        <source>Insert oval into the picture. [Key 6]</source>
        <translation type="vanished">Einfügen Ellipse in das Bild. [Taste 6]</translation>
    </message>
    <message>
        <source>Insert polygon into the picture. [Key 7]
(Right mouse click to close it!)</source>
        <translation type="vanished">Einfügen Polygon in das Bild. [Taste 7]
(Rechte Maustaste zum Schließen!)</translation>
    </message>
    <message>
        <source>Insert text into the picture. [Key 8]</source>
        <translation type="vanished">Einfügen Text in das Bild. [Taste 8]</translation>
    </message>
    <message>
        <source>Insert void into the selected shape with curve selection. [Key 9]</source>
        <translation type="vanished">Einfügen Fehlstellen in die ausgewählte Fläche mit Krümmungsauswahl. [Taste 9]</translation>
    </message>
    <message>
        <source>Insert void into the selected shape with polygon selection. [Key 0]
(Right mouse click to close it!)</source>
        <translation type="vanished">Einfügen Fehlstellen in die ausgewählte Fläche mit Polygonauswahl [Taste 0]
(Rechte Maustaste zum Schließen!)</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2393"/>
        <source>Picture caption has been recorded successfully.</source>
        <translation>Bildbeschriftung wurde erfolgreich aufgenommen.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2399"/>
        <source>Edit Pictures Per Page</source>
        <translation>Bearbeiten Bilder pro Seite</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2400"/>
        <source>Pictures Per Page</source>
        <translation>Bilder pro Seite</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2404"/>
        <source>Pictures Per Page has been recorded successfully.</source>
        <translation>Bilder pro Seite wurden erfolgreich aufgenommen.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2288"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2451"/>
        <source>File doesn&apos;t exists!</source>
        <translation>Datei existiert nicht!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2503"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2536"/>
        <source>%1 are already opened!</source>
        <translation>%1 sind bereits geöffnet!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2656"/>
        <source>Are you sure you want to close?</source>
        <translation>Sind Sie sicher, daß Sie schließen wollen?</translation>
    </message>
    <message>
        <source>Are you sure you want to close all?</source>
        <translation type="vanished">Sind Sie sicher, daß Sie alles schließen wollen?</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2739"/>
        <source>A copy of the current picture has been saved!</source>
        <translation>Eine Kopie des vorhandenen Bildes ist gespeichert worden!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2767"/>
        <source>Picture has been saved successfully!</source>
        <translation>Bild erfolgreich gespeichert!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2741"/>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2769"/>
        <source>Couldn&apos;t save the picture!</source>
        <translation>Speicherung des Bildes nicht möglich!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1880"/>
        <source>Please first select a text box!</source>
        <translation>Bitte zunächt ein Textfeld wählen!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="1925"/>
        <source>File not existed-</source>
        <translation>Datei niccht vorhanden-</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2700"/>
        <source>Are you sure you want to close all pictures?</source>
        <translation>Wollen Sie alle Bilder schließen?</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2806"/>
        <source>Selected shapes are copied!</source>
        <translation>Ausgewählte Formen werden kopiert!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2827"/>
        <source>Copied shapes are pasted!</source>
        <translation>Kopierte Formen werden eingefügt!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2840"/>
        <source>Are you sure you want to delete all shapes?</source>
        <translation>Sind Sie sicher, daß Sie alle Formen löschen wollen?</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="3107"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="3118"/>
        <source>You have unsaved changes. Do you want to save them before you exit the application?</source>
        <translation>Du hast ungespeicherte Änderungen. Wollen Sie die Änderungen vor dem Schließen speichern?</translation>
    </message>
</context>
<context>
    <name>XrayPaintShapeListModel</name>
    <message>
        <location filename="../../paint/XrayPaintShapeListModel.cpp" line="75"/>
        <source>Column</source>
        <translation>Spalte</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintShapeListModel.cpp" line="77"/>
        <source>Row</source>
        <translation>Zeile</translation>
    </message>
</context>
<context>
    <name>XrayPaintShapeListView</name>
    <message>
        <location filename="../../paint/XrayPaintShapeListView.cpp" line="44"/>
        <source>Delete</source>
        <translation>Lösche</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintShapeListView.cpp" line="46"/>
        <source>Move Up</source>
        <translation>Hinaufschieben</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintShapeListView.cpp" line="47"/>
        <source>Move this shape up.</source>
        <translation>Schiebe diese Form nach oben.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintShapeListView.cpp" line="48"/>
        <source>Move Down</source>
        <translation>Herunterschieben</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintShapeListView.cpp" line="49"/>
        <source>Move this shape down.</source>
        <translation>Schiebe diese Form nach unten.</translation>
    </message>
    <message>
        <source>Copy Roi</source>
        <translation type="vanished">Kopiere Interessensregion (ROI)</translation>
    </message>
    <message>
        <source>Copy the current state (ROI) of the shape.</source>
        <translation type="vanished">Kopiere den aktuellen Status (ROI) der Form.</translation>
    </message>
    <message>
        <source>Paste Roi</source>
        <translation type="vanished">Füge Interessensregion (ROI) ein</translation>
    </message>
    <message>
        <source>Paste the last copied state (ROI).</source>
        <translation type="vanished">Füge den zuletzt kopierten Status (ROI) ein.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintShapeListView.cpp" line="45"/>
        <source>Delete this shape.</source>
        <translation>Lösche diese Form.</translation>
    </message>
</context>
<context>
    <name>XrayPaintTabWidget</name>
    <message>
        <source>Save</source>
        <translation type="vanished">Speichern</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2677"/>
        <source>Save and Close</source>
        <translation>Speichern und schließen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2678"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2679"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="2680"/>
        <source>You have an opened project. Do you want to save current settings to the project file before you close all pictures?</source>
        <translation>Sie haben ein offenes Projekt. Wollen Sie die aktuellen Einstellungen in der Projektdatei speichern bevor alle Bilder geschlossen werden?</translation>
    </message>
    <message>
        <location filename="../../paint/XrayPaintMainWindow.cpp" line="3301"/>
        <source>Are you sure you want to close?</source>
        <translation>Sind Sie sicher, daß Sie schließen wollen?</translation>
    </message>
    <message>
        <source>You have unsaved changes. Do you want to save them before you close this tab?</source>
        <translation type="vanished">Sie haben Änderungen nicht gespeichert. Wollen Sie diese vor dem Schließen dieses Tabs speichern?</translation>
    </message>
    <message>
        <source>You have unsaved changes. Do you want to save them before you exit the application?</source>
        <translation type="vanished">Du hast ungespeicherte Änderungen. Wollen Sie die Änderungen vor dem Schließen speichern?</translation>
    </message>
    <message>
        <source>Save File As...</source>
        <translation type="vanished">Datei speichern als...</translation>
    </message>
</context>
<context>
    <name>XrayPaintWizardPage</name>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="1003"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="1019"/>
        <source>Paint Section of the Report Document</source>
        <translation>Zeichenbereich des Berichtsdokuments</translation>
    </message>
</context>
<context>
    <name>XrayPartsWizardPage</name>
    <message>
        <source>Results:</source>
        <translation type="vanished">Ergebnisse:</translation>
    </message>
    <message>
        <source>Data File:</source>
        <translation type="vanished">Datei:</translation>
    </message>
    <message>
        <source>&amp;Browse...</source>
        <translation type="obsolete">&amp;Durchsuchen...</translation>
    </message>
    <message>
        <source>Browse...</source>
        <translation type="vanished">Browse de...</translation>
    </message>
    <message>
        <source>Select Images</source>
        <translation type="vanished">Bildauswahl</translation>
    </message>
    <message>
        <source>CSV File (*.csv)</source>
        <translation type="vanished">CSV Datei (*.csv)</translation>
    </message>
    <message>
        <source>Example Images:</source>
        <translation type="vanished">Beispielbilder:</translation>
    </message>
    <message>
        <source>Image File(s):</source>
        <translation type="vanished">Bilddatei(n):</translation>
    </message>
    <message>
        <source>Image Files (*.jpg *.jpeg *.png *.bmp) ;; All files (*.*)</source>
        <translation type="vanished">Bilddateien (*.jpg *.jpeg *.png *.bmp) ;; Alle Dateien (*.*)</translation>
    </message>
    <message>
        <source>Images Per Page:</source>
        <translation type="vanished">Bilder pro Seite:</translation>
    </message>
</context>
<context>
    <name>XrayProgressBarWidget</name>
    <message>
        <location filename="../../widgets/XrayProgressBarWidget.cpp" line="128"/>
        <source>Busy</source>
        <translation>Beschäftigt</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayProgressBarWidget.cpp" line="138"/>
        <source>Abort</source>
        <translation>Abbruch</translation>
    </message>
</context>
<context>
    <name>XrayQCalendarWidget</name>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="51"/>
        <source>Calendar Widget</source>
        <translation>Kalender Widget</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="124"/>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="353"/>
        <source>Bold</source>
        <translation>Fett</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="126"/>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="354"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="128"/>
        <source>Green</source>
        <translation>Gruen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="174"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="190"/>
        <source>General Options</source>
        <translation>Allgemeine Optionen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="214"/>
        <source>&amp;Locale</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="218"/>
        <source>Sunday</source>
        <translation>Sonntag</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="219"/>
        <source>Monday</source>
        <translation>Montag</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="220"/>
        <source>Tuesday</source>
        <translation>Dienstag</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="221"/>
        <source>Wednesday</source>
        <translation>Mittwoch</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="222"/>
        <source>Thursday</source>
        <translation>Donnerstag</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="223"/>
        <source>Friday</source>
        <translation>Freitag</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="224"/>
        <source>Saturday</source>
        <translation>Samstag</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="226"/>
        <source>Wee&amp;k starts on:</source>
        <translation>Wochenstartanfang&amp;k:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="230"/>
        <source>Single selection</source>
        <translation>Einzelauswahl</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="231"/>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="245"/>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="253"/>
        <source>None</source>
        <translation>Kein</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="233"/>
        <source>&amp;Selection mode:</source>
        <translation>&amp;Auswahlmodus:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="236"/>
        <source>&amp;Grid</source>
        <translation>&amp;Raster</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="239"/>
        <source>&amp;Navigation bar</source>
        <translation>&amp;Navigationsleiste</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="243"/>
        <source>Single letter day names</source>
        <translation>Einzelschreiben Tag Namen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="244"/>
        <source>Short day names</source>
        <translation>Kurz Tagnamen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="248"/>
        <source>&amp;Horizontal header:</source>
        <translation>&amp;Horizontale Kopfzeile:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="252"/>
        <source>ISO week numbers</source>
        <translation>ISO Wochenzahlen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="255"/>
        <source>&amp;Vertical header:</source>
        <translation>&amp;Vertikale Kopfzeile:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="293"/>
        <source>Dates</source>
        <translation>Datumsangaben</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="300"/>
        <source>&amp;Minimum Date:</source>
        <translation>&amp;Minimales Datum:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="308"/>
        <source>&amp;Current Date:</source>
        <translation>&amp;Aktueles datum:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="316"/>
        <source>Ma&amp;ximum Date:</source>
        <translation>Ma&amp;ximales Datum:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="338"/>
        <source>Text Formats</source>
        <translation>Text Formate</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="341"/>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="399"/>
        <source>Black</source>
        <translation>Schwarz</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="343"/>
        <source>&amp;Weekday color:</source>
        <translation>&amp;Wochentagfarbe:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="347"/>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="397"/>
        <source>Red</source>
        <translation>Rot</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="349"/>
        <source>Week&amp;end color:</source>
        <translation>Wochen&amp;end Farbe:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="355"/>
        <source>Plain</source>
        <translation>Ebene</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="357"/>
        <source>&amp;Header text:</source>
        <translation>&amp;Kopfzeilentext:</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="360"/>
        <source>&amp;First Friday in blue</source>
        <translation>&amp;Erster Freitag in blau</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="362"/>
        <source>May &amp;1 in red</source>
        <translation>Mai &amp;1 in rot</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="398"/>
        <source>Blue</source>
        <translation>Blau</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQCalendarWidget.cpp" line="400"/>
        <source>Magenta</source>
        <translation>Gelb</translation>
    </message>
</context>
<context>
    <name>XrayQDoubleSliderEditor</name>
    <message>
        <location filename="../../widgets/XrayQDoubleSliderEditor.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>XrayQFileDownloaderWidget</name>
    <message>
        <location filename="../../network/XrayQFileDownloaderWidget.cpp" line="48"/>
        <source>File size</source>
        <translation>Dateigrösse</translation>
    </message>
    <message>
        <location filename="../../network/XrayQFileDownloaderWidget.cpp" line="51"/>
        <source>Downloaded</source>
        <translation>Heruntergeladen</translation>
    </message>
    <message>
        <location filename="../../network/XrayQFileDownloaderWidget.cpp" line="54"/>
        <source>Transfer rate</source>
        <translation>Transferrate</translation>
    </message>
    <message>
        <location filename="../../network/XrayQFileDownloaderWidget.cpp" line="57"/>
        <source>Time left</source>
        <translation>Verbleibende Zeit</translation>
    </message>
    <message>
        <location filename="../../network/XrayQFileDownloaderWidget.cpp" line="64"/>
        <source>Download to: </source>
        <translation>Herunterladen zu: </translation>
    </message>
    <message>
        <location filename="../../network/XrayQFileDownloaderWidget.cpp" line="69"/>
        <source>&amp;Browse...</source>
        <translation>&amp;Durchsuchen...</translation>
    </message>
    <message>
        <location filename="../../network/XrayQFileDownloaderWidget.cpp" line="70"/>
        <source>Select download directory</source>
        <translation>Auswählen Verzeichnis zum Herunterladen</translation>
    </message>
    <message>
        <location filename="../../network/XrayQFileDownloaderWidget.cpp" line="73"/>
        <source>Open Directory</source>
        <translation>Öffnen Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../network/XrayQFileDownloaderWidget.cpp" line="81"/>
        <source>Abort</source>
        <translation>Abbruch</translation>
    </message>
    <message>
        <location filename="../../network/XrayQFileDownloaderWidget.cpp" line="82"/>
        <source>Abort download</source>
        <translation>Abbruch des Herunterladens</translation>
    </message>
    <message>
        <location filename="../../network/XrayQFileDownloaderWidget.cpp" line="125"/>
        <source>Downloading...</source>
        <translation>Herunterladen...</translation>
    </message>
    <message>
        <location filename="../../network/XrayQFileDownloaderWidget.cpp" line="177"/>
        <source>Temporary network failure.</source>
        <translation></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Fehler</translation>
    </message>
</context>
<context>
    <name>XrayQListWidget</name>
    <message>
        <location filename="../../widgets/XrayQListWidget.cpp" line="66"/>
        <source>Select All</source>
        <translation>Alle auswählen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQListWidget.cpp" line="76"/>
        <source>Invert Selection</source>
        <translation>Invertierte Auswahl</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQListWidget.cpp" line="91"/>
        <source>Copy</source>
        <translation>Kopie</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQListWidget.cpp" line="108"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQListWidget.cpp" line="126"/>
        <source>Remove Selected</source>
        <translation>Ausgewähltes löschen</translation>
    </message>
</context>
<context>
    <name>XrayQListWidgetGroupBoxWithAddFile</name>
    <message>
        <location filename="../../widgets/XrayQListWidgetGroupBoxWithAddFile.cpp" line="39"/>
        <source>Browse...</source>
        <translation>Durchsuchen...</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQListWidgetGroupBoxWithAddFile.cpp" line="40"/>
        <source>Browse for files to add to the list.</source>
        <translation>Nach hinzuzufügenden Dateien suchen.</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQListWidgetGroupBoxWithAddFile.cpp" line="131"/>
        <source>Select Files</source>
        <translation>Datei auswählen</translation>
    </message>
</context>
<context>
    <name>XrayQListWidgetGroupBoxWithAddText</name>
    <message>
        <location filename="../../widgets/XrayQListWidgetGroupBoxWithAddText.cpp" line="41"/>
        <source>Add...</source>
        <translation>Hinzufügen...</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQListWidgetGroupBoxWithAddText.cpp" line="42"/>
        <source>Add text to the list.</source>
        <translation>Text zur Liste hinzufügen.</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQListWidgetGroupBoxWithAddText.cpp" line="128"/>
        <source>Edit Text</source>
        <translation>Text bearbeiten</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQListWidgetGroupBoxWithAddText.cpp" line="129"/>
        <source>Enter Text</source>
        <translation>Text einfügen</translation>
    </message>
</context>
<context>
    <name>XrayQRecentFilesMenu</name>
    <message>
        <location filename="../../widgets/XrayQRecentFilesMenu.cpp" line="160"/>
        <source>Clear Menu</source>
        <translation>Menu leeren</translation>
    </message>
</context>
<context>
    <name>XrayQTableView</name>
    <message>
        <location filename="../../widgets/XrayQTableView.cpp" line="77"/>
        <source>Select All</source>
        <translation>Alle auswählen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQTableView.cpp" line="87"/>
        <source>Invert Selection</source>
        <translation>Invertierte Auswahl</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQTableView.cpp" line="103"/>
        <source>Copy</source>
        <translation>Kopie</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQTableView.cpp" line="120"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQTableView.cpp" line="131"/>
        <source>Remove Selected</source>
        <translation>Ausgewähltes löschen</translation>
    </message>
</context>
<context>
    <name>XrayQTreeWidget</name>
    <message>
        <location filename="../../widgets/XrayQTreeWidget.cpp" line="184"/>
        <source>Select All</source>
        <translation>Alle auswählen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQTreeWidget.cpp" line="194"/>
        <source>Invert Selection</source>
        <translation>Invertierte Auswahl</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQTreeWidget.cpp" line="209"/>
        <source>Copy</source>
        <translation>Kopie</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQTreeWidget.cpp" line="226"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayQTreeWidget.cpp" line="237"/>
        <source>Remove Selected</source>
        <translation>Ausgewähltes löschen</translation>
    </message>
</context>
<context>
    <name>XrayQuickReportGeneratorWidget</name>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.ui" line="14"/>
        <source>XQuickReportWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.ui" line="20"/>
        <source>Input Files</source>
        <translation>Eingangsdateien</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.ui" line="28"/>
        <source>Project File</source>
        <translation>Projektdatei</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.ui" line="38"/>
        <location filename="../../report/XrayQuickReportGeneratorWidget.ui" line="55"/>
        <location filename="../../report/XrayQuickReportGeneratorWidget.ui" line="72"/>
        <location filename="../../report/XrayQuickReportGeneratorWidget.ui" line="108"/>
        <source>Browse...</source>
        <translation>Durchsuchen...</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.ui" line="45"/>
        <source>Html File</source>
        <translation>HTML-Datei</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.ui" line="62"/>
        <source>Signature File</source>
        <translation>Unterschriftsdatei</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.ui" line="84"/>
        <source>Additional Images</source>
        <translation>Zusätzliche Bilder</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.ui" line="120"/>
        <source>Report Font</source>
        <translation>Schriftart des Berichts</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="58"/>
        <source>Save generated report to the disk.</source>
        <translation>Bericht speichern.</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="60"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="61"/>
        <source>Report preview.</source>
        <translation>Vorschau Bericht.</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="73"/>
        <source>Heading Text:</source>
        <translation>Text überschrift:</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="74"/>
        <source>Body Text:</source>
        <translation>Beschreibungstext:</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="133"/>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="143"/>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="153"/>
        <source>Select File</source>
        <translation>Datei auswählen</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="153"/>
        <source>Image File</source>
        <translation>Bilddatei</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="164"/>
        <source>Select Files</source>
        <translation>Dateien auswählen</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="164"/>
        <source>Image Files</source>
        <translation>Bilddateien</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="209"/>
        <source>Inspection Report</source>
        <translation>Untersuchungsbericht</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="227"/>
        <source>Contact Person</source>
        <translation>Ansprechpartner</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="232"/>
        <source>Investigation Analysis</source>
        <translation>Untersuchung</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="235"/>
        <source>Equipment</source>
        <translation>Ausstattung</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="242"/>
        <source>Analysis Parameter</source>
        <translation>UNtersuchungsparameter</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="243"/>
        <source>Analysis Parameter text</source>
        <translation>Beschreibung Untersuchungsparameter</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="245"/>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="248"/>
        <source>Result</source>
        <translation>Ergebnis</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="305"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="306"/>
        <source>Phone:</source>
        <translation>Telefon:</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="307"/>
        <source>Email:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="330"/>
        <source>Order Number:</source>
        <translation>Auftragsnummer:</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="331"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="332"/>
        <source>Customer Name:</source>
        <translation>Kunde:</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="333"/>
        <source>Contact Person:</source>
        <translation>Kontaktperson (Kunde):</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="334"/>
        <source>Component:</source>
        <translation>Bauteil:</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="335"/>
        <source>Quantity:</source>
        <translation>Menge:</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="511"/>
        <source>, Date: </source>
        <translation>, Datum: </translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="513"/>
        <source>, Time: </source>
        <translation>, Zeit: </translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="546"/>
        <source>File doesn&apos;t exists!</source>
        <translation>Datei existiert nicht!</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="554"/>
        <source>Couldn&apos;t open HTML file!</source>
        <translation>HTML-Datei konnte nicht geöffnet werden!</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="560"/>
        <source>Couldn&apos;t open INI file!</source>
        <translation>INI-Datei konnte nicht geöffnet werden!</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="584"/>
        <source>Report saved</source>
        <translation>Bericht gespeichert</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="584"/>
        <source>Success saving to %1</source>
        <translation>%1 erfolgreich gespeichert</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="587"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="587"/>
        <source>Error while saving to %1</source>
        <translation>Fehler beim Speichern von %1</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="600"/>
        <source>Save File As...</source>
        <translation>Datei speichern als...</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWidget.cpp" line="600"/>
        <source>All Supported Files (*.pdf *.html *.png *.jpg *.bmp)</source>
        <translation>Alle unterstützte Dateien (*.pdf*.html*.png*.jpg*.bmp)</translation>
    </message>
</context>
<context>
    <name>XrayQuickReportGeneratorWindow</name>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWindow.cpp" line="94"/>
        <source>View Help</source>
        <translation>Hilfe anzeigen</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWindow.cpp" line="98"/>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location filename="../../report/XrayQuickReportGeneratorWindow.cpp" line="103"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
</context>
<context>
    <name>XrayReportGenerator</name>
    <message>
        <source>Contact Person:
</source>
        <translation type="vanished">Ansprechpartner:</translation>
    </message>
    <message>
        <source>Mobile:</source>
        <translation type="vanished">Handy:</translation>
    </message>
    <message>
        <source>Preview</source>
        <translation type="vanished">Vorschau</translation>
    </message>
    <message>
        <source>Create New Section</source>
        <translation type="vanished">Erzeuge neuen Bereich</translation>
    </message>
    <message>
        <source>Save Report As...</source>
        <translation type="vanished">Speichern Bericht unter...</translation>
    </message>
    <message>
        <source>Backup As...</source>
        <translation type="vanished">Sicherung unter...</translation>
    </message>
    <message>
        <source>, Date: </source>
        <translation type="vanished">, Datum: </translation>
    </message>
    <message>
        <source>, Time: </source>
        <translation type="vanished">, Zeit: </translation>
    </message>
    <message>
        <source>Report saved</source>
        <translation type="vanished">Bericht gespeichert</translation>
    </message>
    <message>
        <source>Create New</source>
        <translation type="vanished">Erzeuge neuen Bereich</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Fehler</translation>
    </message>
    <message>
        <source>Save File As...</source>
        <translation type="vanished">Datei speichern unter...</translation>
    </message>
    <message>
        <source>All Supported Files (*.pdf *.html *.png *.jpg *.bmp)</source>
        <translation type="vanished">Alle unterstützte Dateien (*.pdf*.html*.png*.jpg*.bmp)</translation>
    </message>
    <message>
        <source>Couldn&apos;t restored! Invalid report document.</source>
        <translation type="vanished">Konnte nicht wiederhergestellt werden! Ungültiges Berichtdokument.</translation>
    </message>
    <message>
        <source>The selected report has been restored!</source>
        <translation type="vanished">Der ausgewählte Bericht wurde wiederhergestellt!</translation>
    </message>
    <message>
        <source>Select File</source>
        <translation type="vanished">Wähle Datei</translation>
    </message>
    <message>
        <source>Specify the document name and it&apos;s title.</source>
        <translation type="vanished">Spezifiziere den Dokumentenname und sein Titel.</translation>
    </message>
    <message>
        <source>Specify the document header information.</source>
        <translation type="vanished">Spezifiziere die Information im Dokumentenkopf.</translation>
    </message>
    <message>
        <source>Specify the document footer information.</source>
        <translation type="vanished">Spezifiziere die Information in der Dokumentenfußzeile.</translation>
    </message>
    <message>
        <source>Specify the order information.</source>
        <translation type="vanished">Spezifiziere dieAuftrags-Information.</translation>
    </message>
    <message>
        <source>Specify the investigation analysis.</source>
        <translation type="vanished">Spezifiziere die Untersuchungsanalyse.</translation>
    </message>
    <message>
        <source>Create new sections in the report by pressing the Create New button.</source>
        <translation type="vanished">Erstelle einen neuen Berichtsbereich durch Drücken des Create New-Button.</translation>
    </message>
    <message>
        <source>Specify the results, data, and related images.</source>
        <translation type="vanished">Spezifiziere die Ergebnisse, Daten und Bilder.</translation>
    </message>
    <message>
        <source>Preview the report by pressing the Generate button.</source>
        <translation type="vanished">Vorschau des Berichts durch Drücken des Generate Buttons.</translation>
    </message>
    <message>
        <source>This help is likely not to be of any help.</source>
        <translation type="vanished">Diese Hilfe wird voraussichtlich nicht behilflich sein.</translation>
    </message>
    <message>
        <source>Couldn&apos;t find any help for this page.</source>
        <translation type="vanished">Konnte nicht irgendeine Hilfe für diese Seite finden.</translation>
    </message>
</context>
<context>
    <name>XrayReportWizardWidget</name>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="97"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <source>Create New Section</source>
        <translation type="vanished">Erzeuge neuen Bereich</translation>
    </message>
    <message>
        <source>Save Report As...</source>
        <translation type="vanished">Speichern Bericht unter...</translation>
    </message>
    <message>
        <source>Backup As...</source>
        <translation type="vanished">Sicherung unter...</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="471"/>
        <source>Report saved</source>
        <translation>Bericht gespeichert</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="471"/>
        <source>Success saving to %1</source>
        <translation>%1 erfolgreich gespeichert</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="474"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="474"/>
        <source>Error while saving to %1</source>
        <translation>Fehler bei Speicherung von %1</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="487"/>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="534"/>
        <source>Save File As...</source>
        <translation>Speichere Datei als...</translation>
    </message>
    <message>
        <source>All Supported Files (*.pdf *.html *.png *.jpg *.bmp)</source>
        <translation type="vanished">Alle unterstützte Dateien (*.pdf*.html*.png*.jpg*.bmp)</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="125"/>
        <source>Add a new section to the document.</source>
        <translation>Neuen Abschnitt zum Dokument hinzufügen.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="137"/>
        <source>Generate report and save to the disk.</source>
        <translation>Bericht erstellen und speichern.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="550"/>
        <source>Couldn&apos;t restored! Invalid report document.</source>
        <translation>Konnte nicht wiederhergestellt werden! Ungültiges Berichtdokument.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="556"/>
        <source>The selected report has been restored!</source>
        <translation>Der ausgewählte Bericht wurde wiederhergestellt!</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="562"/>
        <source>Select File</source>
        <translation>Wähle Datei</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="579"/>
        <source>Specify the document name and it&apos;s title.</source>
        <translation>Spezifiziere den Dokumentenname und sein Titel.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="582"/>
        <source>Specify the document header information.</source>
        <translation>Spezifiziere die Information im Dokumentenkopf.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="585"/>
        <source>Specify the document footer information.</source>
        <translation>Spezifiziere die Information in der Dokumentenfußzeile.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="588"/>
        <source>Specify the order information.</source>
        <translation>Spezifiziere die Auftrags-Information.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="591"/>
        <source>Specify the investigation analysis.</source>
        <translation>Spezifiziere die Untersuchungsanalyse.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="594"/>
        <source>Create new sections in the report by pressing the Create New button.</source>
        <translation>Erstelle einen neuen Berichtsbereich durch Drücken des Create New-Button.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="597"/>
        <source>Specify the results, data, and related images.</source>
        <translation>Spezifiziere die Ergebnisse, Daten und Bilder.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="600"/>
        <source>Preview the report by pressing the Generate button.</source>
        <translation>Vorschau des Berichts durch Drücken des Buttons.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="603"/>
        <source>This help is likely not to be of any help.</source>
        <translation>Diese Hilfe wird voraussichtlich nicht behilflich sein.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="607"/>
        <source>Couldn&apos;t find any help for this page.</source>
        <translation>Konnte nicht irgendeine Hilfe für diese Seite finden.</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="1222"/>
        <source>, Date: </source>
        <translation>, Datum: </translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWidget.cpp" line="1224"/>
        <source>, Time: </source>
        <translation>, Zeit: </translation>
    </message>
</context>
<context>
    <name>XrayReportWizardWindow</name>
    <message>
        <location filename="../../report/XrayReportWizardWindow.cpp" line="75"/>
        <source>Save Current Changes</source>
        <translation>Speichere aktuelle Änderungen</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWindow.cpp" line="80"/>
        <source>Clear All Changes</source>
        <translation>Lösche aktuelle Änderungen</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardWindow.cpp" line="83"/>
        <source>Are you sure you want to clear all changes?</source>
        <translation>Sind Sie sicher, daß sie alle aktuelle Änderungen löschen wollen?</translation>
    </message>
    <message>
        <source>View Help</source>
        <translation type="vanished">Zeige Hilfe</translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="vanished">Webseite</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Über uns</translation>
    </message>
</context>
<context>
    <name>XrayResizeDialog</name>
    <message>
        <location filename="../../widgets/XrayResizeDialog.cpp" line="103"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayResizeDialog.cpp" line="104"/>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayResizeDialog.cpp" line="42"/>
        <location filename="../../widgets/XrayResizeDialog.cpp" line="102"/>
        <source>Resize</source>
        <translation>Größenänderung</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayResizeDialog.cpp" line="34"/>
        <source>&amp;Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayResizeDialog.cpp" line="35"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayResizeDialog.cpp" line="52"/>
        <location filename="../../widgets/XrayResizeDialog.cpp" line="106"/>
        <source>Horizontal</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayResizeDialog.cpp" line="53"/>
        <location filename="../../widgets/XrayResizeDialog.cpp" line="107"/>
        <source>Vertical</source>
        <translation>Vertikal</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayResizeDialog.cpp" line="105"/>
        <source>Maintain aspect ratio</source>
        <translation>Ansichtverhältnis beibehalten</translation>
    </message>
</context>
<context>
    <name>XrayResultsWizardPage</name>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="908"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="951"/>
        <source>Results Sections of the Report Document</source>
        <translation>Ergebnisbereiche des Berichtdokuments</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="911"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="952"/>
        <source>Results:</source>
        <translation>Ergebnisse:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="915"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="953"/>
        <source>Data Files:</source>
        <translation>Dateien:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="924"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="954"/>
        <source>Example Images Description:</source>
        <translation>Beschreibung zu den Beispielbildern:</translation>
    </message>
    <message>
        <source>Data File:</source>
        <translation type="vanished">Datendatei:</translation>
    </message>
    <message>
        <location filename="../../report/XrayReportWizardPages.cpp" line="922"/>
        <location filename="../../report/XrayReportWizardPages.cpp" line="955"/>
        <source>Specify the separator delimiter that could be comma(,) or semicolon(;) or bar(|) or just space( ), etc.</source>
        <translation>Spezifiziere das Trennzeichen, welches ein Komma (,) oder  Semikolon (;) oder Strich (|) oder nur ein Leerzeichen (), etc. sein kann.</translation>
    </message>
    <message>
        <source>&amp;Browse...</source>
        <translation type="vanished">&amp;Durchsuchen...</translation>
    </message>
    <message>
        <source>Select the data file(s) you want to embed in the report.</source>
        <translation type="vanished">Wähle die Dateien aus, welche Sie in den Report einbetten wollen.</translation>
    </message>
    <message>
        <source>Select the data file(s) you want to embed in the report. If a file name ends with &apos;_nh&apos; then No Heading will be added before the table, otherwise file name will be the heading of the table.</source>
        <translation type="vanished">Wähle die Dateien aus, welche Sie in den Report einbetten wollen.</translation>
    </message>
    <message>
        <source>Select File(s)</source>
        <translation type="vanished">Wähle Dateie(n)</translation>
    </message>
    <message>
        <source>Select File</source>
        <translation type="vanished">Wähle Datei</translation>
    </message>
    <message>
        <source>Select Files</source>
        <translation type="vanished">Wähle Dateiene aus</translation>
    </message>
    <message>
        <source>Example Images:</source>
        <translation type="vanished">Beispiel Bilder:</translation>
    </message>
    <message>
        <source>Image File(s):</source>
        <translation type="vanished">Bilddatei(en):</translation>
    </message>
    <message>
        <source>Select Images</source>
        <translation type="vanished">Bilderauswahl</translation>
    </message>
    <message>
        <source>Images Per Page:</source>
        <translation type="vanished">Bilder pro Seite:</translation>
    </message>
</context>
<context>
    <name>XraySaveReportAsExcelDialog</name>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="14"/>
        <source>Form</source>
        <translation>Geometrie</translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="32"/>
        <source>Project: </source>
        <translation>Projekt: </translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="39"/>
        <source>12345</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="46"/>
        <source>Part: </source>
        <translation>Bauteil: </translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="53"/>
        <source>HL_12345</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="60"/>
        <source>Image W</source>
        <translation>Bildbreite</translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="67"/>
        <source>Width of picture added to the excel sheet or saved for CSV.</source>
        <translation>Breite des Bildes, das der Exceldatei hinzugefügt oder in der CSV-Datei gespeichert wird.</translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="70"/>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="93"/>
        <source>px</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="83"/>
        <source>Image H</source>
        <translation>Bildhöhe</translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="90"/>
        <source>Height of picture added to the excel sheet or saved for CSV.</source>
        <translation>Höhe des Bildes, das der Exceldatei hinzugefügt oder in der CSV-Datei gespeichert wird.</translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="121"/>
        <source>If it is checked, only current picture will be exported, otherwise all.</source>
        <translation>Wenn ausgewählt, wird nur das aktuelle Bild exportiert. Andernfalls alle.</translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.ui" line="124"/>
        <source>Save only current picture</source>
        <translation>Nur aktuelles Bild speichern</translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.cpp" line="408"/>
        <source>Couldn&apos;t find any picture!</source>
        <translation>Kein Bild gefunden!</translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.cpp" line="416"/>
        <source>Save File As...</source>
        <translation>Datei speichern unter...</translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.cpp" line="444"/>
        <location filename="../../paint/XraySaveReportAsExcelDialog.cpp" line="454"/>
        <source>Report has been saved successfully!</source>
        <translation>Bericht wurde erfolgreich gespeichert!</translation>
    </message>
    <message>
        <location filename="../../paint/XraySaveReportAsExcelDialog.cpp" line="446"/>
        <location filename="../../paint/XraySaveReportAsExcelDialog.cpp" line="456"/>
        <source>Couldn&apos;t save the report!</source>
        <translation>Bericht konnte nicht gespeichert werden!</translation>
    </message>
</context>
<context>
    <name>XraySqlUserManagementWidget</name>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="20"/>
        <source>XUser Panel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="384"/>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="1023"/>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="3026"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="277"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="455"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="702"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="736"/>
        <source>User Name</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="630"/>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="1269"/>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="3272"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="283"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="461"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="703"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="737"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="669"/>
        <source>Log In</source>
        <translation>Einloggen</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="672"/>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="1311"/>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="3314"/>
        <source>Return</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="714"/>
        <source>Register</source>
        <translation>Registrierung</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="1308"/>
        <source>Complete Registration</source>
        <translation>Schließe die Registrierung ab</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="1557"/>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="3560"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="289"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="467"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="708"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="742"/>
        <source>Email</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="1803"/>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="3806"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="295"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="473"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="704"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="738"/>
        <source>First Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="2049"/>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="4052"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="301"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="479"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="707"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="741"/>
        <source>Phone Number</source>
        <translation>Telefonnummer</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="2295"/>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="4298"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="307"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="485"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="705"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="739"/>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="2334"/>
        <source>&lt; Back to Login Page</source>
        <translation>&lt; Zurück zum Einloggen</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="2417"/>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="4392"/>
        <source>Upload Picture</source>
        <translation>Bild hochladen</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="2474"/>
        <source>Log Out</source>
        <translation>Ausloggen</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="2491"/>
        <source>Name:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="2508"/>
        <source>Rank:</source>
        <translation>Rang:</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="2579"/>
        <source>Edit My Profile</source>
        <translation>Mein Profil editieren</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="2618"/>
        <source>Delete My Acount</source>
        <translation>Lösche mein Konto</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="2635"/>
        <source>Email:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="2761"/>
        <source>Enter to
Modules</source>
        <translation>Zu Modulen</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="2690"/>
        <source>Admin Panel</source>
        <translation>Admin-Bereich</translation>
    </message>
    <message>
        <source>Enter to
Application</source>
        <translation type="vanished">zu der Applikation</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="3311"/>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="4482"/>
        <source>Save Changes</source>
        <translation>Speicher Änderungen</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="4337"/>
        <source>&lt; Back to My Page</source>
        <translation>&lt; Zurück zu meiner Seite</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="4521"/>
        <source>Delete All Users</source>
        <translation>Lösche alle Benutzer</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="4731"/>
        <source>Rollback All Changes</source>
        <translation>Alle Änderungen rückgängig</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="4770"/>
        <source>Goto My Page</source>
        <translation>Gehe zu meine Seite</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="4809"/>
        <source>All Admins</source>
        <translation>Alle Admins</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="4848"/>
        <source>Delete All Admins</source>
        <translation>Lösche alle Admins</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="4864"/>
        <source>Warning: There are no auto backups! Make sure you know what you are doing.</source>
        <translation>Warnung: Es wird keine Sicherstellung selbstständig gemacht! Vergewissern Sie sich was Sie tun.</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.ui" line="4903"/>
        <source>All Users</source>
        <translation>Alle Benutzer</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="199"/>
        <source>Login Failed: The user name or password entered is incorrect.</source>
        <translation>Einloggen gescheitert: Der Benutzername oder das Passwort ist nicht korrekt.</translation>
    </message>
    <message>
        <source>Are you sure you want to logout?</source>
        <translation type="vanished">Sind Sie sicher, daß Sie sich ausloggen wollen?</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="175"/>
        <source>Do you want User Manager to remember your name and password?</source>
        <translation>Wollen Sie, daß der User Manager Ihren Namen und Ihr Passwort merkt?</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="253"/>
        <source>You have been logged out.</source>
        <translation>Sie sind ausgeloggt.</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="267"/>
        <source>Demo version allows only 2 users to register.</source>
        <translation>Demo-Version erlaubt das Registrieren von nur von 2 Benutzern.</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="320"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="498"/>
        <source>Choose a different User Name!</source>
        <translation>Wähle einen anderen Benutzernamen!</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="335"/>
        <source>Use another Email address!</source>
        <translation>Benutze eine andere Email Addresse!</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="520"/>
        <source>Please use valid entries.</source>
        <translation>Bitte benutze einen gültigen Zugang.</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="382"/>
        <source>Registration Successful! You can now login.</source>
        <translation>Erfolgreiche Registrierung! Sie können sich jetzt einloggen.</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="344"/>
        <source>Please fill all entries.</source>
        <translation>Bitte alle Eingabefelder ausfüllen.</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="429"/>
        <source>Are you sure you want to delete your account?</source>
        <translation>Sind Sie sicher, daß Sie Ihr Konto löschen wollen?</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="443"/>
        <source>Your account has been deleted.</source>
        <translation>Ihr Konto wurde gelöscht.</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="512"/>
        <source>Use another Email!</source>
        <translation>Benutze eine andere Email!</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="619"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="628"/>
        <source>Open Image</source>
        <translation>Öffne Bild</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="619"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="628"/>
        <source>Image Files (*.png *.jpg *.bmp)</source>
        <translation>Bilddateien (*.png *.jpg *bmp)</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="650"/>
        <source>Saved! database has been updated successfully.</source>
        <translation>Gespeichert! Datenbank wurde erfolgreich aktualisiert.</translation>
    </message>
    <message>
        <source>Saved to database!</source>
        <translation type="vanished">In der Datenbank gespeichert!</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="701"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="735"/>
        <source>Id</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="706"/>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="740"/>
        <source>Rank</source>
        <translation>Rang</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="751"/>
        <source>Are you sure you want to delete all user accounts?</source>
        <translation>Sind Sie sicher, daß Sie alle Ihre Konten löschen wollen?</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="757"/>
        <source>All users have been deleted. You must save the changes if you are sure about this deletion.</source>
        <translation>Alle Benutzer wurden gelöscht. Sie sollten ihre Änderungen vorher sichern wenn Sie die Löschung wollen.</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="765"/>
        <source>Are you sure you want to delete all admins?
(This won&apos;t delete regular users and you.)</source>
        <translation>Wollen Sie wirklich alle Administratoren löschen? (Dieses löscht nicht den normalen Benutzer und Sie.)</translation>
    </message>
    <message>
        <location filename="../../sql/XraySqlUserManagementWidget.cpp" line="771"/>
        <source>All other admins have been deleted. You must save the changes if you are sure about this deletion.</source>
        <translation>Alle anderen Administratoren sind gelöscht. Sie sollten die Änderungen speichern, wenn Sie über diese Löschung sicher sind.</translation>
    </message>
</context>
<context>
    <name>XrayStandardModelCompleterTreeViewWidget</name>
    <message>
        <location filename="../../widgets/XrayStandardModelCompleterTreeViewWidget.cpp" line="40"/>
        <source>Search...</source>
        <translation>Suchen...</translation>
    </message>
</context>
<context>
    <name>XrayStandardModelTreeView</name>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="158"/>
        <source>Select All</source>
        <translation>Alle auswählen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="176"/>
        <source>Select All Files</source>
        <translation>Alle Dateien auswählen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="195"/>
        <source>Sort Groups</source>
        <translation>Gruppen sortieren</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="196"/>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="204"/>
        <source>Ascending Order</source>
        <translation>Aufsteigend</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="199"/>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="207"/>
        <source>Descending Order</source>
        <translation>Absteigend</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="203"/>
        <source>Sort Files</source>
        <translation>Dateien sortieren</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="214"/>
        <source>Move To</source>
        <translation>Gehe zu</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="220"/>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="224"/>
        <source>Add New Group</source>
        <translation>Neue Gruppe hinzufügen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="225"/>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="247"/>
        <source>Enter Text</source>
        <translation>Text eintragen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="225"/>
        <source>Group 1</source>
        <translation>Gruppe 1</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="238"/>
        <source>Rename Group</source>
        <translation>Gruppe umbenennen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="240"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="246"/>
        <source>Change Text</source>
        <translation>Text ändern</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="247"/>
        <source>Text</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="282"/>
        <source>Copy Path</source>
        <translation>Dateipfad kopieren</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="299"/>
        <source>Reveal In File Explorer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="315"/>
        <source>Remove Selected</source>
        <translation>Ausgewahl löschen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="323"/>
        <source>Expand All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="330"/>
        <source>Collapse All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widgets/XrayStandardModelTreeView.cpp" line="595"/>
        <source>Are you sure you want to close?</source>
        <translation>Sind Sie sicher, dass Sie schließen wollen?</translation>
    </message>
</context>
<context>
    <name>XrayTcpClientMainWidget</name>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="57"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="61"/>
        <source>Port:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="69"/>
        <source>Run</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="73"/>
        <source>Server</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="81"/>
        <source>Update interval:</source>
        <translation>Update Interval:</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="83"/>
        <source>Specify the update time interval in seconds.
Timer will be disabled on value 0.</source>
        <translation>Spezifiziere das Update- Interval in Sekunden. Der Timer wird auf den Wert 0 ausgeschaltet.</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="100"/>
        <source>Timer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="121"/>
        <source>Browse directory</source>
        <translation>Durchsuche Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="122"/>
        <source>Select directory for the received files.</source>
        <translation>Wähle das Verzeichnis für die zu erhaltenenen Dateien aus.</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="125"/>
        <source>Specify directory for the received files.</source>
        <translation>Spezifiziere das Verzeichnis für die zu erhaltenenen Dateien aus.</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="127"/>
        <source>Received files</source>
        <translation>Erhaltenenen Dateien</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="147"/>
        <source>Reading to receive!</source>
        <translation>Lesen zum Erhalten!</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="155"/>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="253"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="182"/>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="185"/>
        <source>Receiving...</source>
        <translation>Erhalten...</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="189"/>
        <source>Specified directory doesn&apos;t exists!</source>
        <translation>Spezifiziertes Verzeichnis existiert nicht!</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="198"/>
        <source>Select Directory</source>
        <translation>Wähle Verzeichnis aus</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="232"/>
        <source>Host was not found. Make sure the host name and port are correct.</source>
        <translation>Host wurde nicht gefunden. Versichere dich, daß der Host-Name und der Port richtig sind.</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="235"/>
        <source>The connection was refused by the peer. Make sure the server is running and host name and port are correct.</source>
        <translation>Die Verbindung wurde vom Peer zurückgewiesen. Versichern Sie sich, daß der Server läuft und der Host-Name und Port richtig sind.</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="238"/>
        <source>Following error occurred: %1.</source>
        <translation>Es ist ein Fehler aufgetreten: %1.</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="242"/>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="246"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="256"/>
        <source>Ready to receive!</source>
        <translation>Empfangsbereit!</translation>
    </message>
</context>
<context>
    <name>XrayTcpClientServerMainWindow</name>
    <message>
        <location filename="../../network/XrayTcpClientServerMainWindow.cpp" line="53"/>
        <source>Client</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientServerMainWindow.cpp" line="54"/>
        <source>Server</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientServerMainWindow.cpp" line="85"/>
        <source>View Help</source>
        <translation>Zeige Hilfe</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientServerMainWindow.cpp" line="89"/>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpClientServerMainWindow.cpp" line="94"/>
        <source>About</source>
        <translation>Über uns</translation>
    </message>
</context>
<context>
    <name>XrayTcpServerMainWidget</name>
    <message>
        <location filename="../../network/XrayTcpClientMainWidget.cpp" line="107"/>
        <source>Last received time</source>
        <translation>Zuletzt erhaltene Zeit</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="56"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="60"/>
        <source>Port:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="65"/>
        <source>Run</source>
        <translation>Lauf</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="67"/>
        <source>Stop</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="70"/>
        <source>Last sent time</source>
        <translation>Zuletzt versandte Zeit</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="76"/>
        <source>Host</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="94"/>
        <source>Add Files</source>
        <translation>Hizufügen von Dateien</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="95"/>
        <source>Browse and select files that needs to be sent.</source>
        <translation>Durchsuchen und auswählen von Dateien, welche versandt werden.</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="98"/>
        <source>Sent files</source>
        <translation>Versandte Dateien</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="117"/>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="125"/>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="154"/>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="167"/>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="169"/>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="191"/>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="193"/>
        <source>Not running</source>
        <translation>Nicht laufend</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="152"/>
        <source>Couldn&apos;t find any file in the list.</source>
        <translation>Konnte nicht irgendeine Datei in der Liste finden.</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="166"/>
        <source>Unable to start the server: %1.</source>
        <translation>Unmöglich den server zu starten: %1.</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="180"/>
        <source>The server is running on IP: %1 port: %2</source>
        <translation>Der Server läuft on IP: %1 Port:%2</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="182"/>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="241"/>
        <source>Running</source>
        <translation>Läuft</translation>
    </message>
    <message>
        <location filename="../../network/XrayTcpServerMainWidget.cpp" line="208"/>
        <source>Select Files</source>
        <translation>Wähle Dateien</translation>
    </message>
</context>
<context>
    <name>XrayTextEditor</name>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="80"/>
        <source>Select File</source>
        <translation>Auswählen Datei</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="100"/>
        <source>Save File As...</source>
        <translation>Speichern Datei unter...</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="119"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="120"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="122"/>
        <source>&amp;New</source>
        <translation>&amp;Neu</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="124"/>
        <source>Create a new file</source>
        <translation>Erzeugen eine neue Datei</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="130"/>
        <source>&amp;Open...</source>
        <translation>&amp;Öffnen...</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="132"/>
        <source>Open an existing file</source>
        <translation>Öffnen einer vorhandene Datei</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="138"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="140"/>
        <source>Save the document to disk</source>
        <translation>Speichern des Dokuments auf der Festplatte</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="146"/>
        <source>Save &amp;As...</source>
        <translation>Speichern &amp;unter...</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="148"/>
        <source>Save the document under a new name</source>
        <translation>Speichern des Dokuments unter einem neuen Namen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="153"/>
        <source>E&amp;xit</source>
        <translation>B&amp;eenden</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="155"/>
        <source>Exit the application</source>
        <translation>Beenden der Anwendung</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="157"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="158"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="161"/>
        <source>Cu&amp;t</source>
        <translation>Aus&amp;schneiden</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="164"/>
        <source>Cut the current selection&apos;s contents to the clipboard</source>
        <translation>Ausschneiden der Inhalte der aktuellen Auswahl auf das Klemmbrett</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="171"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="173"/>
        <source>Copy the current selection&apos;s contents to the clipboard</source>
        <translation>Kopieren der Inhalte der aktuellen Auswahl auf das Klemmbrett</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="180"/>
        <source>&amp;Paste</source>
        <translation>&amp;Einfügen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="182"/>
        <source>Paste the clipboard&apos;s contents into the current selection</source>
        <translation>Einfügen der Inhalte des Klemmbrettes auf die aktuelle Auswahl</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="vanished">&amp;Hilfe</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="vanished">&amp;Info</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="204"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="234"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>Das Dokument ist modifiziert worden. Wollen Sie Ihre Änderungen speichern?</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="252"/>
        <source>Cannot read file %1:
%2.</source>
        <translation>Kann nicht lesen die Datei %1: %2.</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="266"/>
        <source>File loaded</source>
        <translation>Datei geladen</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="274"/>
        <source>Application</source>
        <translation>Anwendung</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="274"/>
        <source>Cannot write file %1:
%2.</source>
        <translation>Kann nicht schreiben die Datei %1: %2.</translation>
    </message>
    <message>
        <location filename="../../widgets/XrayTextEditor.cpp" line="288"/>
        <source>File saved</source>
        <translation>Datei gespeichert</translation>
    </message>
</context>
<context>
    <name>XrayVoidAnalysisFilter</name>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="30"/>
        <source>Parameters</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="76"/>
        <source>Select the contour color for the selected shape.</source>
        <translation>Wähle die Konturfarbe für die ausgewählte Form aus.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="63"/>
        <source>Select the contour id color for the selected shape.</source>
        <translation>Wähle die Kontur-ID-Farbe für die ausgewählte Form aus.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="86"/>
        <source>If it is checked, voids will be colorized based percentage.
Each color represent 25% of total detected voids in a single shape.</source>
        <translation type="unfinished">Wenn ausgewählt, werden die Poren nach Anteil eingefärbt.
Jede Farbe beschreibt 25% der Gesamtzahl der Poren in einer Geometrie.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="90"/>
        <source>Colorize</source>
        <translation>Einfärben</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="102"/>
        <source>Repeatability</source>
        <translation>Wiederholbarkeit</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="109"/>
        <source>Repeatability threshold
(lower the value higher the detection rate and vice versa)</source>
        <translation>Wiederholbarkeitsgrenzwert (je niedriger der Wert, um so höher ist die Erkennbarkeitsrate und umgekehrt)</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="117"/>
        <source>Threshold step</source>
        <translation>Schwellwertschritt</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="124"/>
        <source>Threshold step
(lower the value higher the detection rate and vice versa)</source>
        <translation>Schwellwertschritt (je niedriger der Wert, um so höher ist die Erkennbarkeitsrate und umgekehrt)</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="132"/>
        <source>Lower threshold</source>
        <translation>Untere Schwellwert</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="139"/>
        <source>Minimum range of intensity histogram</source>
        <translation>Minimaler Bereich des Intensitätshistogramms</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="146"/>
        <source>Higher threshold</source>
        <translation>Oberer Schwellwert</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="153"/>
        <source>Maximum range of intensity histogram</source>
        <translation>Maximaler Bereich des Intensitätshistogramms</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="160"/>
        <source>Smallest void</source>
        <translation>Kleinste Fehlstelle</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="167"/>
        <source>Minimum range of void area</source>
        <translation>Kleinster Bereich von Fehlstellenflächen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="174"/>
        <source>Largest void</source>
        <translation>Größte Fehlstelle</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="181"/>
        <source>Maximum range of void area</source>
        <translation>Grösster Bereich von Fehlstellenflächen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="188"/>
        <source>Void circularity</source>
        <translation>Rundheit</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="195"/>
        <source>Minimum range of void circularity</source>
        <translation>Kleinster Bereich der Fehlstellen-Rundheit</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="202"/>
        <source>Void convexity</source>
        <translation>Konvexität</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="209"/>
        <source>Minimum range of void convexity</source>
        <translation>Kleinster Bereich der Fehlstellen-Konvexität</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="216"/>
        <source>Void inertia ratio</source>
        <translation>Größenverhältnis</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="223"/>
        <source>Minimum range of void inertia ratio</source>
        <translation>Kleinster Bereich des breiten-Höhenverhältnisses</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="270"/>
        <source>Presets</source>
        <translation>Voreinstellungen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="276"/>
        <source>Select a preset for the selected shape.</source>
        <translation>Wähle eine Voreinstellung für die ausgewählte Form aus.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="289"/>
        <source>Save current settings as a preset.</source>
        <translation>Speichere aktuelle Einstellungen als eine Voreinstellung.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="305"/>
        <source>Preview voids in the selected shape using the current preset.</source>
        <translation>Voransicht von Fehlstellen in der ausgwählten Fläche mit der aktuellen Voreinstellung.</translation>
    </message>
    <message>
        <source>Update voids in the selected shape with the current preset.</source>
        <translation type="vanished">Aktualisiere Fehlstellen in der ausgwählten Fläche mit der aktuellen Voreinstellung.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="321"/>
        <source>Remove current preset.</source>
        <translation>Lösche aktuelle Voreinstellung.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="324"/>
        <source>Remove</source>
        <translation>Lösche</translation>
    </message>
    <message>
        <source>Reset image and the parameters.</source>
        <translation type="vanished">Zurücksetzen von Bild und Paramter.</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Zurücksetzen</translation>
    </message>
    <message>
        <source>Preview of detected voids in the selected shape.</source>
        <translation type="vanished">Vorschau detektierter Fehlstellen in der ausgewählten Form.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="308"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="20"/>
        <source>Apply current changes to the original image.</source>
        <translation>Übernahme laufender Änderungen auf das Originalbild.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="23"/>
        <source>Apply</source>
        <translation>Übernahme</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="43"/>
        <source>If it is checked, light (white) intensity voids
will be detected, otherwise dark (black).</source>
        <translation>Wenn angeschaltet, helle (weiße) Fehlstellen werden detektiert, andernfalls dunkle (schwarze).</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="47"/>
        <source>Detect light void</source>
        <translation>Detektiere helle Fehlstellen</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="235"/>
        <source>Statistics</source>
        <translation>Statistik</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="247"/>
        <source>Update table with the voids of the selected shape.</source>
        <translation>Aktualisieren der Tabelle mit Fehlstellen der ausgewählten Form.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="250"/>
        <source>Update</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="257"/>
        <source>Save all voids to the disk as CSV or ASCII text format.</source>
        <translation>Speichere alle Fehlstellen auf die Festplatte als CSV- oder ASCII- Textformat.</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="260"/>
        <location filename="../../paint/XrayVoidAnalysisFilter.ui" line="292"/>
        <source>Save</source>
        <translation>Speicher</translation>
    </message>
    <message>
        <source>Couldn&apos;t find any Paint area!</source>
        <translation type="vanished">Konnte keine Zeichenfläche finden!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="506"/>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="670"/>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="722"/>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="859"/>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="886"/>
        <source>Couldn&apos;t find any selected shape!</source>
        <translation>Konnte keine ausgewählte Form finden!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="678"/>
        <source>Couldn&apos;t find any void in the selected shape!</source>
        <translation>Konnte keine Fehlstelle in der ausgewählten Form finden!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="802"/>
        <source>Couldn&apos;t find any void in the table!</source>
        <translation>Keine Poren in der Tabelle gefunden!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="371"/>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="515"/>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="731"/>
        <source>Shape ROI is out of range, please try again!</source>
        <translation>Form als ROI ist ausserhalb, bitte versuche es nochmals!</translation>
    </message>
    <message>
        <source>Couldn&apos;t find any void!</source>
        <translation type="vanished">Konnte nicht irgendeine Fehlstelle finden!</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="764"/>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="834"/>
        <source>Couldn&apos;t find any shape in the Paint area!</source>
        <translation>Konnte nicht irgendeine Form im Zeichenbereich finden!</translation>
    </message>
    <message>
        <source>Save File As...</source>
        <translation type="vanished">Speichere Datei unter...</translation>
    </message>
    <message>
        <source>All Supported Files (*.csv *.txt)</source>
        <translation type="vanished">Alle unterstützten Dateien (*.csv *.txt)</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="1012"/>
        <source>Enter New Preset Name</source>
        <translation>Eintragen neuer Voreinstellungsbezeichnung</translation>
    </message>
    <message>
        <location filename="../../paint/XrayVoidAnalysisFilter.cpp" line="1013"/>
        <source>Enter Text</source>
        <translation>Eintragen Text</translation>
    </message>
</context>
</TS>
