/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/28
** filename: 	XrayGlobalResources.cpp
** file base:	XrayGlobalResources
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a QApplication object with some
				pre-defined attributes.
****************************************************************************/

#include "XrayGlobalResources.h"

#include <QDebug>

XRAYLAB_USING_NAMESPACE

void XrayGlobalResources::initResources()
{
	// important: initialize the resources of this library.
	Q_INIT_RESOURCE(darkstyle);
	Q_INIT_RESOURCE(images);
	Q_INIT_RESOURCE(languages);
	Q_INIT_RESOURCE(leds);
}
