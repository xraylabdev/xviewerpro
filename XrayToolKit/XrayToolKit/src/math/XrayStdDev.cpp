/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/04/04
** filename: 	XrayStdDev.cpp
** file base:	XrayStdDev
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the standard deviation of the given data.
****************************************************************************/

#include "XrayStdDev.h"

#include <cmath>

XRAYLAB_USING_NAMESPACE

XrayStdDev::XrayStdDev(int _sample_count)  :
	m_sampleCount(0)
{
	m_data.resize(_sample_count);
}

void XrayStdDev::reset() 
{
	m_sampleCount = 0;
}

void XrayStdDev::addValue(float _v) 
{
	m_data[m_sampleCount++] = _v;
	if (m_sampleCount == m_data.size())
		m_sampleCount = 0;
}

float XrayStdDev::getAverage() 
{
	auto average = 0.f;
	for (auto i = 0; i < m_sampleCount; i++)
		average += m_data[i];

	if (m_sampleCount > 0)
		return average / static_cast<float>(m_sampleCount);

	return 0;
}

float XrayStdDev::getSD() 
{
	auto average = getAverage();
	auto XrayStdDev = 0.f;
	for (auto i = 0; i < m_sampleCount; i++)
		XrayStdDev += std::powf(m_data[i] - average, 2);

	if (m_sampleCount > 1)
		return std::sqrt(XrayStdDev / static_cast<float>(m_sampleCount - 1.0));

	return 0;
}