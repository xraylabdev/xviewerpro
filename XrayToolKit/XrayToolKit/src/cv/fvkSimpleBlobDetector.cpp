/*********************************************************************************
created:	2017/08/05   02:02AM
filename: 	fvkSimpleBlobDetector.cpp
file base:	fvkSimpleBlobDetector
file ext:	cpp
author:		Furqan Ullah (Post-docs, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	fvkSimpleBlobDetector class extends the cv::SimpleBlobDetector
to detect the blobs in an gray scale image.
This class is a part of Fast Visualization Kit (FVK) which is developed and
maintained by Dr. Furqan Ullah.

/**********************************************************************************
FL-ESSENTIALS (FLE) - FLTK Utility Widgets
Copyright (C) 2017-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkSimpleBlobDetector.h"
#include "XrayCVUtil.h"

#include <vector>
#include <utility>

using namespace R3D;
XRAYLAB_USING_NAMESPACE

fvkSimpleBlobDetector::fvkSimpleBlobDetector(const cv::SimpleBlobDetector::Params& parameters) :
	params(parameters)
{
}

static std::vector < std::vector<cv::Point> > _contours;

std::vector<std::vector<cv::Point>> fvkSimpleBlobDetector::getContours()
{
	return _contours;
}

cv::Ptr<fvkSimpleBlobDetector> fvkSimpleBlobDetector::create(const SimpleBlobDetector::Params& params)
{
	return cv::makePtr<fvkSimpleBlobDetector>(params);
}

void fvkSimpleBlobDetector::findBlobs(const cv::Mat& _image, const cv::Mat& _binaryImage, std::vector<Center>& centers, std::vector<std::vector<cv::Point>>& curContours) const
{
	CV_UNUSED(_image);

	centers.clear();
	curContours.clear();

	cv::Mat binaryImage = _binaryImage.clone();
	std::vector <std::vector<cv::Point>> contours;
	cv::findContours(binaryImage, contours, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);

#ifdef DEBUG_BLOB_DETECTOR
	cv::Mat keypointsImage;
	cv::cvtColor(binaryImage, keypointsImage, cv::COLOR_GRAY2RGB);

	cv::Mat contoursImage;
	cv::cvtColor(binaryImage, contoursImage, cv::COLOR_GRAY2RGB);
	cv::drawContours(contoursImage, contours, -1, cv::Scalar(0, 255, 0));
	cv::imshow("contours", contoursImage);
#endif

	for (std::size_t contourIdx = 0; contourIdx < contours.size(); contourIdx++)
	{
		Center center;
		center.confidence = 1;
		cv::Moments moms = cv::moments(contours[contourIdx]);
		if (params.filterByArea)
		{
			double area = moms.m00;
			if (area < params.minArea || area >= params.maxArea)
				continue;
		}

		if (params.filterByCircularity)
		{
			double area = moms.m00;
			double perimeter = cv::arcLength(contours[contourIdx], true);
			double ratio = 4 * CV_PI * area / (perimeter * perimeter);
			if (ratio < params.minCircularity || ratio >= params.maxCircularity)
				continue;
		}

		if (params.filterByInertia)
		{
			double denominator = std::sqrt(std::pow(2 * moms.mu11, 2) + std::pow(moms.mu20 - moms.mu02, 2));
			const double eps = 1e-2;
			double ratio;
			if (denominator > eps)
			{
				double cosmin = (moms.mu20 - moms.mu02) / denominator;
				double sinmin = 2 * moms.mu11 / denominator;
				double cosmax = -cosmin;
				double sinmax = -sinmin;

				double imin = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cosmin - moms.mu11 * sinmin;
				double imax = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cosmax - moms.mu11 * sinmax;
				ratio = imin / imax;
			}
			else
			{
				ratio = 1;
			}

			if (ratio < params.minInertiaRatio || ratio >= params.maxInertiaRatio)
				continue;

			center.confidence = ratio * ratio;
		}

		if (params.filterByConvexity)
		{
			std::vector < cv::Point > hull;
			cv::convexHull(contours[contourIdx], hull);
			double area = cv::contourArea(contours[contourIdx]);
			double hullArea = cv::contourArea(hull);
			if (std::fabs(hullArea) < DBL_EPSILON)
				continue;
			double ratio = area / hullArea;
			if (ratio < params.minConvexity || ratio >= params.maxConvexity)
				continue;
		}

		if (moms.m00 == 0.0)
			continue;
		center.location = cv::Point2d(moms.m10 / moms.m00, moms.m01 / moms.m00);

		if (params.filterByColor)
		{
			if (binaryImage.at<uchar>(cvRound(center.location.y), cvRound(center.location.x)) != params.blobColor)
				continue;
		}

		//compute blob radius
		{
			std::vector<double> dists;
			dists.reserve(contours[contourIdx].size());
			for (std::size_t pointIdx = 0; pointIdx < contours[contourIdx].size(); pointIdx++)
			{
				cv::Point2d pt = contours[contourIdx][pointIdx];
				dists.emplace_back(cv::norm(center.location - pt));
			}
			std::sort(dists.begin(), dists.end());
			center.radius = (dists[(dists.size() - 1) / 2] + dists[dists.size() / 2]) / 2.;
		}

		centers.emplace_back(center);
		curContours.emplace_back(contours[contourIdx]);


#ifdef DEBUG_BLOB_DETECTOR
		cv::circle(keypointsImage, center.location, 1, cv::Scalar(0, 0, 255), 1);
#endif
	}
#ifdef DEBUG_BLOB_DETECTOR
	cv::imshow("bk", keypointsImage);
	cv::waitKey();
#endif
}

void fvkSimpleBlobDetector::detect(const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, const cv::Mat& mask) const
{
	_contours.clear();
	keypoints.clear();

	CV_Assert(params.minRepeatability != 0);

	cv::Mat grayscaleImage;
	if (image.channels() == 3 || image.channels() == 4)
		cv::cvtColor(image, grayscaleImage, cv::COLOR_BGR2GRAY);
	else
		grayscaleImage = image.clone();

	if (grayscaleImage.type() != CV_8UC1) 
	{
		throw std::invalid_argument("Blob detector only supports 8-bit images!");
	}

	std::vector < std::vector<cv::Point> > contours;
	std::vector < std::vector<Center> > centers;
	for (double thresh = params.minThreshold; thresh < params.maxThreshold; thresh += params.thresholdStep)
	{
		cv::Mat binarizedImage;
		cv::threshold(grayscaleImage, binarizedImage, thresh, 255, cv::THRESH_BINARY);

		std::vector < Center > curCenters;
		std::vector < std::vector<cv::Point> > curContours, newContours;
		findBlobs(grayscaleImage, binarizedImage, curCenters, curContours);
		std::vector < std::vector<Center> > newCenters;
		for (std::size_t i = 0; i < curCenters.size(); i++)
		{
			bool isNew = true;
			for (std::size_t j = 0; j < centers.size(); j++)
			{
				double dist = cv::norm(centers[j][centers[j].size() / 2].location - curCenters[i].location);
				isNew = dist >= params.minDistBetweenBlobs && dist >= centers[j][centers[j].size() / 2].radius && dist >= curCenters[i].radius;
				if (!isNew)
				{
					centers[j].push_back(curCenters[i]);

					std::size_t k = centers[j].size() - 1;
					while (k > 0 && centers[j][k].radius < centers[j][k - 1].radius)
					{
						centers[j][k] = centers[j][k - 1];
						k--;
					}
					centers[j][k] = curCenters[i];

					break;
				}
			}
			if (isNew)
			{
				newCenters.emplace_back(std::vector<Center>(1, curCenters[i]));
				newContours.emplace_back(curContours[i]);
			}
		}
		std::copy(newCenters.begin(), newCenters.end(), std::back_inserter(centers));
		std::copy(newContours.begin(), newContours.end(), std::back_inserter(contours));
	}

	keypoints.reserve(centers.size());
	_contours.reserve(centers.size());

	for (std::size_t i = 0; i < centers.size(); i++)
	{
		if (centers[i].size() < params.minRepeatability)
			continue;
		cv::Point2d sumPoint(0, 0);
		double normalizer = 0;
		for (std::size_t j = 0; j < centers[i].size(); j++)
		{
			sumPoint += centers[i][j].confidence * centers[i][j].location;
			normalizer += centers[i][j].confidence;
		}
		sumPoint *= (1. / normalizer);
		cv::KeyPoint kpt(sumPoint, (float)(centers[i][centers[i].size() / 2].radius) * 2.0f);
		keypoints.emplace_back(kpt);
		_contours.emplace_back(contours[i]);
	}

	if (!mask.empty())
	{
		cv::KeyPointsFilter::runByPixelsMask(keypoints, mask.clone());
	}
}

/**********************************************************************************
/* Static functions implementation
/**********************************************************************************/
fvkBlobDetector::fvkBlobDetector() :
	m_smoothness(5),
	m_sharpness(11),
	m_colorizeContourOnPercentage(true)
{
	// Filter by distance
	m_params.minDistBetweenBlobs = 3.f;
	// Filter by Area
	m_params.filterByArea = true;
	// Filter by Circularity
	m_params.filterByCircularity = true;
	// Filter by Convexity
	m_params.filterByConvexity = true;
	m_params.maxConvexity = 2.f;
	// Filter by Inertia
	m_params.filterByInertia = true;
	// Filter by Color
	m_params.filterByColor = true;
	m_params.blobColor = 255;

	m_params.minRepeatability = 3;
	m_params.thresholdStep = 3.f;
	m_params.minThreshold = 10.f;
	m_params.maxThreshold = 220.f;
	m_params.minArea = 10.f;
	m_params.maxArea = 30000.f;
	m_params.minCircularity = 0.81f;
	m_params.minConvexity = 0.90f;
	m_params.minInertiaRatio = 0.13f;

	m_contour_color = cv::Scalar(0, 255, 255);
	m_keypoint_color = cv::Scalar(0, 255, 0);
}

void fvkBlobDetector::sortContours()
{
	struct Contour
	{
		cv::KeyPoint keypoint;
		std::vector<cv::Point> contours;
		double area;
	};
	std::vector<Contour> sortedContours(m_contours.size());
	for (std::size_t i = 0; i < sortedContours.size(); i++)
	{
		sortedContours[i].keypoint = m_keypoints[i];
		sortedContours[i].contours = m_contours[i];
		sortedContours[i].area = m_areas[i];
	}
	std::sort(sortedContours.begin(), sortedContours.end(), [](const Contour& a, const Contour& b) -> bool
	{
		return a.area > b.area;
	});

	for (std::size_t i = 0; i < sortedContours.size(); i++)
	{
		m_keypoints[i] = sortedContours[i].keypoint;
		m_contours[i] = sortedContours[i].contours;
		m_areas[i] = sortedContours[i].area;
	}
}

bool fvkBlobDetector::update()
{
	if (m_image.empty())
		return false;

	if (m_image.channels() == 3 || m_image.channels() == 4)
		cv::cvtColor(m_image, m_grayscale, cv::COLOR_BGR2GRAY);
	else
		m_grayscale = m_image.clone();

	if (m_grayscale.type() != CV_8UC1)
	{
		throw std::invalid_argument("Blob detector only supports 8-bit images!");
	}

	m_cropped = cv::Mat(m_grayscale, m_roi).clone();
	cv::cvtColor(m_cropped, m_cropped, cv::COLOR_GRAY2BGR);

	return draw();
}
bool fvkBlobDetector::detect()
{
	if (m_image.empty())
		return false;

	if (m_image.channels() == 3 || m_image.channels() == 4)
		cv::cvtColor(m_image, m_grayscale, cv::COLOR_BGR2GRAY);
	else
		m_grayscale = m_image.clone();

	if (m_grayscale.type() != CV_8UC1)
	{
		throw std::invalid_argument("Blob detector only supports 8-bit images!");
	}

	m_cropped = cv::Mat(m_grayscale, m_roi).clone();
	cv::cvtColor(m_cropped, m_cropped, cv::COLOR_GRAY2BGR);

	cv::Mat frame(m_cropped.size(), m_cropped.type());
	cv::medianBlur(m_cropped, frame, m_smoothness);

	// sharpen with weights
	cv::Mat gaussianBlurred(frame.size(), frame.type());
	cv::GaussianBlur(frame, gaussianBlurred, cv::Size(0, 0), static_cast<double>(m_sharpness));
	cv::addWeighted(frame, 1.5, gaussianBlurred, -0.5, 0, frame);

	auto detector = fvkSimpleBlobDetector::create(m_params);
	std::vector<cv::KeyPoint> tempKeyPoints;

	detector->detect(frame, tempKeyPoints);
	auto tempContours = detector->getContours();

	// key points and contours size must be the same.
	if (tempKeyPoints.size() != tempContours.size())
		return false;

	m_keypoints.clear();
	m_keypoints.reserve(tempContours.size());

	m_contours.clear();
	m_contours.reserve(tempContours.size());
	
	m_areas.clear();
	m_areas.reserve(tempContours.size());

	for (std::size_t i = 0; i < tempContours.size(); i++)
	{
		//auto area = cv::contourArea(tempContours[i], false);
		//// sometimes the contours are not open, and give wrong values,
		//// to discard these contours, I am checking the percentage of pixels and
		//// if it's higher than 95% then just discard those contours.
		//auto percent = 100.0 / frameSize * area;
		//if (percent < 95.0)
		//{
			if (XrayCVUtil::isContourInsideContour(tempContours[i], m_shapeContour))
			{
				auto insidePixels = XrayCVUtil::getNumberOfPixelsInsidePolygon(m_cropped.size(), tempContours[i]);

				//std::cout << "inside contour: " << i << "\n";
				//std::cout << "inside contour area: " << insidePixels << "\n";

				m_keypoints.emplace_back(tempKeyPoints[i]);
				m_contours.emplace_back(tempContours[i]);
				m_areas.emplace_back(insidePixels/*area*/);
			}
			else
			{
				//std::cout << "not inside contour: " << i << "\n";
			}
		//}
	}

	for (std::size_t i = 0; i < m_custom_contours.size(); i++)
	{
		m_contours.emplace_back(m_custom_contours[i]);
		m_keypoints.emplace_back(m_custom_keypoints[i]);
		m_areas.emplace_back(m_custom_areas[i]);
	}

	// sort contours by their area, decremented to lower area
	sortContours();

	return !m_contours.empty();
}

bool fvkBlobDetector::draw()
{
	if (m_grayscale.empty() || m_roi.size() == cv::Size2i(0, 0))
		return false;

	m_cropped = cv::Mat(m_grayscale, m_roi).clone();
	cv::cvtColor(m_cropped, m_cropped, cv::COLOR_GRAY2BGR);

	if (!m_contours.empty())
	{
		if (m_colorizeContourOnPercentage)
		{
			for (std::size_t i = 0; i < m_contours.size(); i++)
			{
				auto color = cv::Scalar(255, 0, 0);

				// colorize based on percentage
				auto perc = static_cast<int>(i * 100.0 / m_contours.size());
				if (perc <= 25)
					color = cv::Scalar(0, 0, 255);
				if (perc > 25 && perc <= 50)
					color = cv::Scalar(0, 255, 0);
				if (perc > 50 && perc <= 75)
					color = cv::Scalar(0, 255, 255);
				if (perc > 75)
					color = cv::Scalar(255, 0, 0);
				
				if (perc < 95) {
				
					std::vector<std::vector<cv::Point> > contour = { m_contours[i] };
					cv::drawContours(m_cropped, contour, -1, color, 1, cv::LINE_AA);
					cv::putText(m_cropped, std::to_string(i), m_keypoints[i].pt, cv::FONT_HERSHEY_PLAIN, 0.9, m_keypoint_color, 1, cv::LINE_AA);
				}

			}
		}
		else
		{
			for (std::size_t i = 0; i < m_contours.size(); i++)
			{
				std::vector<std::vector<cv::Point> > contour = { m_contours[i] };
				cv::drawContours(m_cropped, contour, -1, m_contour_color, 1, cv::LINE_AA);

				cv::putText(m_cropped, std::to_string(i), m_keypoints[i].pt, cv::FONT_HERSHEY_PLAIN, 0.9, m_keypoint_color, 1, cv::LINE_AA);
			}
		}
	}

	if (m_image.channels() == 3)
	{
		m_output = m_image.clone();
	}
	else if (m_image.channels() == 4)
	{
		m_output = m_image.clone();
		cv::cvtColor(m_output, m_output, cv::COLOR_BGRA2BGR);
	}
	else
	{
		cv::cvtColor(m_grayscale, m_output, cv::COLOR_GRAY2BGR);
	}

	m_cropped.copyTo(m_output(m_roi));

	return true;
}

void fvkBlobDetector::addCustomContour(const std::vector<cv::Point>& contour)
{
	if (contour.empty())
		return;

	if (m_image.empty())
		return;

	// prepare to only consider a grayscale image
	if (m_image.channels() == 3 || m_image.channels() == 4)
		cv::cvtColor(m_image, m_grayscale, cv::COLOR_BGR2GRAY);
	else
		m_grayscale = m_image.clone();

	if (m_grayscale.type() != CV_8UC1)
	{
		throw std::invalid_argument("Blob detector only supports 8-bit images!");
	}

	m_cropped = cv::Mat(m_grayscale, m_roi).clone();
	cv::cvtColor(m_cropped, m_cropped, cv::COLOR_GRAY2BGR);

	// compute center of the blob
	fvkSimpleBlobDetector::Center center;
	center.confidence = 1;
	auto moms = cv::moments(contour);
	if (moms.m00 == 0.0)
		return;

	center.location = cv::Point2d(moms.m10 / moms.m00, moms.m01 / moms.m00);

	// compute blob radius
	std::vector<double> dists;
	dists.reserve(contour.size());
	for (std::size_t pointIdx = 0; pointIdx < contour.size(); pointIdx++)
	{
		cv::Point2d pt = contour[pointIdx];
		dists.emplace_back(cv::norm(center.location - pt));
	}
	std::sort(dists.begin(), dists.end());
	center.radius = (dists[(dists.size() - 1) / 2] + dists[dists.size() / 2]) / 2.;

	// compute blob center
	cv::KeyPoint kpt(center.location, center.radius * 2.0f);

	//// compute blob area
	//auto area = cv::contourArea(contour, false);
	//// sometimes the contours are not open, and give wrong values,
	//// to discard these contours, I am checking the percentage of pixels and
	//// if it's higher than 95% then just discard those contours.
	//auto frameSize = m_cropped.rows * m_cropped.cols;
	//auto percent = 100.0 / frameSize * area;
	//std::cout << percent << "  percent\n";
	//if (percent < 95.0)
	//{
		auto insidePixels = XrayCVUtil::getNumberOfPixelsInsidePolygon(m_cropped.size(), contour);

		m_custom_contours.emplace_back(contour);
		m_custom_keypoints.emplace_back(kpt);
		m_custom_areas.emplace_back(insidePixels/*area*/);

		m_contours.emplace_back(contour);
		m_keypoints.emplace_back(kpt);
		m_areas.emplace_back(insidePixels/*area*/);

		// sort contours by their area, decremented to lower area
		sortContours();
	//}
}

void fvkBlobDetector::removeContours(const std::vector<int>& _indexList)
{
	if (_indexList.empty())
		return;

	// size of the keyPoint and contours array is always the same.
	std::vector<cv::KeyPoint> tempK;
	std::vector<std::vector<cv::Point> > tempC;
	std::vector<double> tempA;
	tempC.reserve(m_contours.size());
	tempK.reserve(m_contours.size());
	tempA.reserve(m_contours.size());
	for (std::size_t i = 0; i < m_contours.size(); i++)
	{
		auto r = std::find(_indexList.begin(), _indexList.end(), i);
		if (r == _indexList.end())	// if we don't find i'th index in the filtered indices 
		{
			tempC.emplace_back(m_contours[i]);
			tempK.emplace_back(m_keypoints[i]);
			tempA.emplace_back(m_areas[i]);
		}
		else // i'th contour need to be removed
		{
			auto kp = m_keypoints[i];

			// check if the removed contour is from the custom draw
			for (std::size_t j = 0; j < m_custom_contours.size(); j++)
			{
				// compare the center of the contour
				if (m_custom_keypoints[j].pt == kp.pt)
				{
					m_custom_contours.erase(m_custom_contours.begin() + j);
					m_custom_keypoints.erase(m_custom_keypoints.begin() + j);
					m_custom_areas.erase(m_custom_areas.begin() + j);
				}
			}
		}
	}

	m_contours = tempC;
	m_keypoints = tempK;
	m_areas = tempA;
}

void fvkBlobDetector::clear()
{
	m_contours.clear();
	m_keypoints.clear();
	m_areas.clear();

	m_custom_contours.clear();
	m_custom_keypoints.clear();
	m_custom_areas.clear();
}

void fvkBlobDetector::resize(int size)
{
	m_contours.resize(size);
	m_keypoints.resize(size);
	m_areas.resize(size);
}

/**********************************************************************************
/* Static functions implementation
/**********************************************************************************/
std::vector<double> fvkBlobDetector::detect(cv::Mat& _img, cv::SimpleBlobDetector::Params params, int _smoothness, int _sharpness, std::vector<int> _filtered_indices)
{
	std::vector<double> area;
	if (_img.empty())
		return area;

	cv::Mat normalize(_img);
	//auto p = cv::createCLAHE(10, cv::Size(8, 8)); // adaptive histogram equalization
	//p->apply(_img, normalize);
	//cv::normalize(_img, normalize, 0, 255, cv::NORM_MINMAX);

	// smoothing with median filter
	cv::Mat frame(normalize.size(), normalize.type());
	cv::medianBlur(normalize, frame, _smoothness);

	// sharpen with weights
	cv::Mat m(frame.size(), frame.type());
	cv::GaussianBlur(frame, m, cv::Size(0, 0), static_cast<double>(_sharpness));
	cv::addWeighted(frame, 1.5, m, -0.5, 0, frame);

	std::vector<cv::KeyPoint> keypoints;
	auto detector = fvkSimpleBlobDetector::create(params);
	detector->detect(frame, keypoints);

	auto contours = detector->getContours();
	if (contours.empty())
		return area;

	if (_img.channels() == 1)
		cv::cvtColor(_img, _img, cv::COLOR_GRAY2BGR);

	// compute the area of contours.
	area.resize(contours.size());
	for (std::size_t i = 0; i < contours.size(); i++)
	{
		std::vector<cv::Point> approx;
		cv::approxPolyDP(contours[i], approx, 2, true);
		area[i] = cv::contourArea(approx, false);
	}

	if (!_filtered_indices.empty())
	{
		// size of the keyPoint and contours array is always the same.
		std::vector<cv::KeyPoint> tempK;
		std::vector<std::vector<cv::Point> > tempC;
		std::vector<double> tempA;
		tempC.reserve(contours.size());
		tempK.reserve(contours.size());
		tempA.reserve(contours.size());
		for (std::size_t i = 0; i < contours.size(); i++)
		{
			auto r = std::find(_filtered_indices.begin(), _filtered_indices.end(), i);
			if (r == _filtered_indices.end())	// if we don't find i'th index in the filtered indices
			{
				tempC.push_back(contours[i]);
				tempK.push_back(keypoints[i]);
				tempA.push_back(area[i]);
			}
		}

		cv::drawContours(_img, tempC, -1, cv::Scalar(0, 255, 255), 1, cv::LINE_AA);

		for (std::size_t i = 0; i < tempK.size(); i++)
			cv::putText(_img, std::to_string(i), tempK[i].pt, cv::FONT_HERSHEY_PLAIN, 0.9, cv::Scalar(0, 255, 0, 255), 1, cv::LINE_AA);

		return tempA;
	}
	else
	{
		cv::drawContours(_img, contours, -1, cv::Scalar(0, 255, 255), 1, cv::LINE_AA);

		for (std::size_t i = 0; i < keypoints.size(); i++)
			cv::putText(_img, std::to_string(i), keypoints[i].pt, cv::FONT_HERSHEY_PLAIN, 0.9, cv::Scalar(0, 255, 0, 255), 1, cv::LINE_AA);

		return area;
	}
}

cv::Mat fvkBlobDetector::execute(const cv::Mat& _img, const cv::Rect& roi, std::vector<double>& _areas, std::vector<int> _filtered_indices, std::size_t _minRep, float _threshStep, float _minThresh, float _maxThresh, float _minArea, float _maxArea, float _minCir, float _minConv, float _minIner)
{
	cv::Mat grayscaleImage;
	if (_img.channels() == 3 || _img.channels() == 4)
		cv::cvtColor(_img, grayscaleImage, cv::COLOR_BGR2GRAY);
	else
		grayscaleImage = _img.clone();

	if (grayscaleImage.type() != CV_8UC1)
	{
		throw std::invalid_argument("Blob detector only supports 8-bit images!");
	}

	auto cropped = cv::Mat(grayscaleImage, roi).clone();

	cv::SimpleBlobDetector::Params params;
	// Filter by distance
	params.minDistBetweenBlobs = 3.f;
	// Filter by Area
	params.filterByArea = true;
	// Filter by Circularity
	params.filterByCircularity = true;
	// Filter by Convexity
	params.filterByConvexity = true;
	params.maxConvexity = 2.f;
	// Filter by Inertia
	params.filterByInertia = true;
	// Filter by Color
	params.filterByColor = true;
	params.blobColor = 255;

	params.minRepeatability = _minRep;
	params.thresholdStep = _threshStep;
	params.minThreshold = _minThresh;
	params.maxThreshold = _maxThresh;
	params.minArea = _minArea;
	params.maxArea = _maxArea;
	params.minCircularity = _minCir;
	params.minConvexity = _minConv;
	params.minInertiaRatio = _minIner;

	cv::Mat bgr;
	_areas = detect(cropped, params, 5, 11, _filtered_indices);
	if (!_areas.empty())
	{
		if (_img.channels() == 3 || _img.channels() == 4)
			bgr = _img.clone();
		else
			cv::cvtColor(grayscaleImage, bgr, cv::COLOR_GRAY2BGR);

		cropped.copyTo(bgr(roi));
	}

	return bgr;
}