/*********************************************************************************
created:	2018/08/18   02:02AM
filename: 	fvkContrastEnhancerUtil.cpp
file base:	fvkContrastEnhancerUtil
file ext:	cpp
author:		Furqan Ullah (Post-docs, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	This class provides algorithms for contrast enhancements for 8, 16,
24, 32 bit images.
This class is a part of Fast Visualization Kit (FVK) which is developed and
maintained by Dr. Furqan Ullah.

/**********************************************************************************
*	Fast Visualization Kit (FVK)
*	Copyright (C) 2017-2019 REAL3D
*
* This file and its content is protected by a software license.
* You should have received a copy of this license with this file.
* If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkContrastEnhancerUtil.h"

using namespace R3D;

bool fvkContrastEnhancerUtil::performSUACE(const cv::Mat& _src, cv::Mat& _dst, int _distance, double _sigma)
{
	if (_src.type() != CV_8UC1)
		return false;

	if (_distance <= 0 || _sigma <= 0)
		return false;

	_dst = cv::Mat(_src.size(), CV_8UC1);
	cv::Mat smoothed;
	int val;
	int a, b;
	int adjuster;
	int half_distance = _distance / 2;
	double distance_d = _distance;

	cv::GaussianBlur(_src, smoothed, cv::Size(0, 0), _sigma);

	for (auto x = 0; x < _src.cols; x++)
	{
		for (auto y = 0; y < _src.rows; y++)
		{
			val = _src.at<uchar>(y, x);
			adjuster = smoothed.at<uchar>(y, x);

			if (val - adjuster > distance_d)
			{
				adjuster += (val - adjuster) * 0.5;
			}

			adjuster = adjuster < half_distance ? half_distance : adjuster;

			b = adjuster + half_distance;
			b = b > 255 ? 255 : b;
			a = b - _distance;
			a = a < 0 ? 0 : a;

			if (val >= a && val <= b)
			{
				_dst.at<uchar>(y, x) = static_cast<int>(((val - a) / distance_d) * 255);
			}
			else if (val < a)
			{
				_dst.at<uchar>(y, x) = 0;
			}
			else if (val > b)
			{
				_dst.at<uchar>(y, x) = 255;
			}
		}
	}
	return true;
}
void fvkContrastEnhancerUtil::performCLAHE(cv::Mat& _img, double _cliplimit, cv::Size _tile_grid_size)
{
	if (_img.empty() || _cliplimit == 0) 
		return;

	if (_img.channels() == 1)
	{
		cv::Mat m;
		auto p = cv::createCLAHE(_cliplimit, _tile_grid_size); // adaptive histogram equalization
		p->apply(_img, m);
		_img = m;
	}
	else if (_img.channels() == 3)
	{
		cv::Mat m;
		std::vector<cv::Mat> channels(3);
		cv::cvtColor(_img, m, cv::COLOR_BGR2YCrCb);	//change to YCrCb format
		cv::split(m, channels);					// split the image into channels
		auto p = cv::createCLAHE(_cliplimit, _tile_grid_size);
		p->apply(channels[0], channels[0]);
		cv::merge(channels, m);					// merge 3 channels including the modified one
		cv::cvtColor(m, m, cv::COLOR_YCrCb2BGR);		// change to BGR format
		_img = m;
	}
	else if (_img.channels() == 4)
	{
		cv::Mat m;
		std::vector<cv::Mat> channels(4);
		cv::split(_img, channels);

		std::vector<cv::Mat> channels3(3);
		channels3[0] = channels[0];
		channels3[1] = channels[1];
		channels3[2] = channels[2];
		cv::merge(channels3, m);

		cv::cvtColor(m, m, cv::COLOR_BGR2YCrCb);
		cv::split(m, channels3);
		auto p = cv::createCLAHE(_cliplimit, _tile_grid_size);
		p->apply(channels3[0], channels3[0]);
		cv::merge(channels3, m);
		cv::cvtColor(m, m, cv::COLOR_YCrCb2BGR);

		cv::Mat dst(_img.size(), _img.type());
		for (auto y = 0; y < m.rows; y++)
		{
			for (auto x = 0; x < m.cols; x++)
			{
				auto pixel = m.at<cv::Vec3b>(cv::Point(x, y));
				auto p = cv::Vec4b(pixel.val[0], pixel.val[1], pixel.val[2], channels[3].at<uchar>(cv::Point(x, y)));
				dst.at<cv::Vec4b>(cv::Point(x, y)) = p;

			}
		}

		_img = dst;
	}
}

void fvkContrastEnhancerUtil::rescaleGrayLevelMat(const cv::Mat& _input_mat, cv::Mat& _output_mat, const float _histogram_clipping_limit)
{
	// rescale between 0-255, keeping floating point values
	cv::normalize(_input_mat, _output_mat, 0.0, 255.0, cv::NORM_MINMAX);

	// extract a 8bit image that will be used for histogram edge cut
	cv::Mat intGrayImage;
	if (_output_mat.channels() == 1)
	{
		_output_mat.convertTo(intGrayImage, CV_8U);
	}
	else
	{
		cv::Mat rgbIntImg;
		_output_mat.convertTo(rgbIntImg, CV_8UC3);
		cv::cvtColor(rgbIntImg, intGrayImage, cv::COLOR_BGR2GRAY);
	}

	// get histogram density probability in order to cut values under above edges limits (here 5-95%)... usefull for HDR pixel errors cancellation
	cv::Mat dst, hist;
	auto histSize = 256;
	cv::calcHist(&intGrayImage, 1, nullptr, cv::Mat(), hist, 1, &histSize, nullptr);
	cv::Mat normalizedHist;
	cv::normalize(hist, normalizedHist, 1, 0, cv::NORM_L1, CV_32F); // normalize histogram so that its sum equals 1

	double min_val, max_val;
	cv::minMaxLoc(normalizedHist, &min_val, &max_val);
	//std::cout << "Hist max, min = " << max_val << ", " << min_val << std::endl;

	// compute density probability
	cv::Mat denseProb = cv::Mat::zeros(normalizedHist.size(), CV_32F);
	denseProb.at<float>(0) = normalizedHist.at<float>(0);
	auto histLowerLimit = 0, histUpperLimit = 0;
	for (auto i = 1; i < normalizedHist.size().height; ++i)
	{
		denseProb.at<float>(i) = denseProb.at<float>(i - 1) + normalizedHist.at<float>(i);
		if (denseProb.at<float>(i) < _histogram_clipping_limit)
			histLowerLimit = i;
		if (denseProb.at<float>(i) < 1 - _histogram_clipping_limit)
			histUpperLimit = i;
	}
	// deduce min and max admitted gray levels
	auto minInputValue = static_cast<float>(histLowerLimit) / histSize * 255;
	auto maxInputValue = static_cast<float>(histUpperLimit) / histSize * 255;

	// rescale image range [minInputValue-maxInputValue] to [0-255]
	_output_mat -= minInputValue;
	_output_mat *= 255.0 / (maxInputValue - minInputValue);
	// cut original histogram and back project to original image
	cv::threshold(_output_mat, _output_mat, 255.0, 255.0, 2);	// THRESH_TRUNC, clips values above 255
	cv::threshold(_output_mat, _output_mat, 0.0, 0.0, 3);		// THRESH_TOZERO, clips values under 0
}

void fvkContrastEnhancerUtil::performGammaLUT(cv::Mat& _img, const int _value)
{
	if (_img.empty() || _value == 0) 
		return;

	const auto value = 1.f - static_cast<double>(_value) / 100.f;

	cv::Mat lut_matrix(1, 256, CV_8UC1);
	auto ptr = lut_matrix.ptr();
	for (auto i = 0; i < 256; i++)
		ptr[i] = static_cast<int>(std::pow(static_cast<double>(i) / 255.0, value) * 255.0);

	cv::Mat m(_img.size(), _img.type());
	cv::LUT(_img, lut_matrix, m);
	_img = m;
}
void fvkContrastEnhancerUtil::performGammaFilter(cv::Mat& _img, int _value)
{
	if (_img.empty() || _value == 0) 
		return;

	const auto value = 1.f - static_cast<float>(_value) / 100.f;

	if (_img.channels() == 1)
	{
		cv::Mat m(_img.size(), _img.type());
		for (auto y = 0; y < _img.rows; y++)
		{
			for (auto x = 0; x < _img.cols; x++)
			{
				m.at<uchar>(y, x) = std::pow(_img.at<uchar>(y, x) / 255.f, value) * 255.f;
			}
		}
		_img = m;
	}
	else if (_img.channels() == 3)
	{
		cv::Mat m(_img.size(), _img.type());
		for (auto y = 0; y < _img.rows; y++)
		{
			for (auto x = 0; x < _img.cols; x++)
			{
				cv::Vec3f pixel = _img.at<cv::Vec3b>(cv::Point(x, y));

				pixel.val[0] = std::pow(pixel.val[0] / 255.f, value) * 255.f;
				pixel.val[1] = std::pow(pixel.val[1] / 255.f, value) * 255.f;
				pixel.val[2] = std::pow(pixel.val[2] / 255.f, value) * 255.f;

				m.at<cv::Vec3b>(cv::Point(x, y)) = pixel;
			}
		}
		_img = m;
	}
	else if (_img.channels() == 4)
	{
		cv::Mat m(_img.size(), _img.type());
		for (auto y = 0; y < _img.rows; y++)
		{
			for (auto x = 0; x < _img.cols; x++)
			{
				cv::Vec4f pixel = _img.at<cv::Vec4b>(cv::Point(x, y));

				pixel.val[0] = std::pow(pixel.val[0] / 255.f, value) * 255.f;
				pixel.val[1] = std::pow(pixel.val[1] / 255.f, value) * 255.f;
				pixel.val[2] = std::pow(pixel.val[2] / 255.f, value) * 255.f;
				pixel.val[3] = std::pow(pixel.val[3] / 255.f, value) * 255.f;

				m.at<cv::Vec4b>(cv::Point(x, y)) = pixel;
			}
		}
		_img = m;
	}
}

void fvkContrastEnhancerUtil::performLevelsGray(unsigned char* buffer, const unsigned int width, const unsigned int height, const int exposure, const float brightness, const float contrast, const float saturation)
{
	if (!buffer || exposure == 0)
		return;

	const auto value = 1.f - static_cast<float>(exposure) / 50.f;
	const auto exposureFactor = std::pow(2.0f, -value);
	const auto brightnessFactor = brightness / 10.0f;
	const auto contrastFactor = contrast > 0.0f ? contrast : 0.0f;
	const auto saturationFactor = 1.0f - saturation;

	const auto limit = height * width;
	for (auto pixelIndex = 0; pixelIndex < limit; pixelIndex++)
	{
		const auto pixelValue = buffer[pixelIndex];

		auto R = pixelValue / 255.0f;

		// clamp values + exposure factor + contrast
		R = (((R > 1.0f ? exposureFactor : R < 0.0f ? 0.0f : exposureFactor * R) - 0.5f) * contrastFactor) + 0.5f;

		// saturation + Brightness
		const auto graySaturFactorPlusBrightness = brightnessFactor + R * saturationFactor;
		R = graySaturFactorPlusBrightness + R * saturation;

		// clamp values
		R = R > 1.0f ? 255.0f : R < 0.0f ? 0.0f : 255.0f * R;

		buffer[pixelIndex] = static_cast<unsigned char>(R);
	}
}
void fvkContrastEnhancerUtil::performLevelsRGB(unsigned char* buffer, const unsigned int width, const unsigned int height, const int exposure, const float brightness, const float contrast, const float saturation)
{
	if (!buffer || exposure == 0)
		return;

	const auto value = 1.f - static_cast<float>(exposure) / 50.f;
	const auto exposureFactor = std::pow(2.0f, -value);
	const float brightnessFactor = brightness / 10.0f;
	const float contrastFactor = contrast > 0.0f ? contrast : 0.0f;
	const float saturationFactor = 1.0f - saturation;

	const unsigned int limit = height * width;
	for (unsigned int pixelIndex = 0; pixelIndex < limit; pixelIndex++)
	{
		const int pixelValue = buffer[pixelIndex];

		float R = ((pixelValue & 0xff0000) >> 16) / 255.0f;
		float G = ((pixelValue & 0xff00) >> 8) / 255.0f;
		float B = (pixelValue & 0xff) / 255.0f;

		// Clamp values + exposure factor + contrast
		R = (((R > 1.0f ? exposureFactor : R < 0.0f ? 0.0f : exposureFactor * R) - 0.5f) * contrastFactor) + 0.5f;
		G = (((G > 1.0f ? exposureFactor : G < 0.0f ? 0.0f : exposureFactor * G) - 0.5f) * contrastFactor) + 0.5f;
		B = (((B > 1.0f ? exposureFactor : B < 0.0f ? 0.0f : exposureFactor * B) - 0.5f) * contrastFactor) + 0.5f;

		// Saturation + Brightness
		const float graySaturFactorPlusBrightness = brightnessFactor + ((R * 0.3f) + (G * 0.59f) + (B * 0.11f)) * saturationFactor;
		R = graySaturFactorPlusBrightness + R * saturation;
		G = graySaturFactorPlusBrightness + G * saturation;
		B = graySaturFactorPlusBrightness + B * saturation;

		// Clamp values
		R = R > 1.0f ? 255.0f : R < 0.0f ? 0.0f : 255.0f*R;
		G = G > 1.0f ? 255.0f : G < 0.0f ? 0.0f : 255.0f*G;
		B = B > 1.0f ? 255.0f : B < 0.0f ? 0.0f : 255.0f*B;

		buffer[pixelIndex] = ((int)R << 16) | ((int)G << 8) | (int)B;
	}
}
void fvkContrastEnhancerUtil::performLevels(unsigned char* buffer, const unsigned int width, const unsigned int height, int channels, const int exposure, const float brightness, const float contrast, const float saturation)
{
	if (channels == 1)
		performLevelsGray(buffer, width, height, exposure, brightness, contrast, saturation);
	//else if (channels == 3 || channels == 4)
	//	performLevelsRGB(buffer, width, height, exposure, brightness, contrast, saturation);
}
void fvkContrastEnhancerUtil::performExposure(cv::Mat& _img, const int exposure)
{
	performLevels(_img.data, _img.cols, _img.rows, _img.channels(), exposure, 0.f, 1.f, 0.f);
}

void fvkContrastEnhancerUtil::performContrastEnhancerFilter(const cv::Mat& _src, cv::Mat& _dest, int _delta, int _sigma, int _clip, int _exposure, int _gamma, int _normalize)
{
	auto rescaled = _src.clone();

	if (_clip > 0)
	{
		rescaleGrayLevelMat(rescaled, rescaled, static_cast<float>(_clip) / 100.f);
		cv::normalize(rescaled, rescaled, 0.0, 255.0, cv::NORM_MINMAX);
	}

	if (_delta > 0 && _sigma > 0)
	{
		if (performSUACE(rescaled, _dest, _delta, (_sigma + 1) / 8.0))	// only works with 1 channel.
		{
			if (_normalize)
				performCLAHE(_dest, 8, cv::Size(4, 4));
		}
		else // works with all channels.
		{
			_dest = rescaled.clone();
			performCLAHE(_dest, _delta, cv::Size(_sigma, _sigma));
		}
	}
	else
	{
		_dest = rescaled.clone();
	}

	performExposure(_dest, _exposure);
	performGammaLUT(_dest, _gamma);

	// sharpening
	cv::Mat m(_dest.size(), _dest.type());
	cv::GaussianBlur(_dest, m, cv::Size(0, 0), 21);
	cv::addWeighted(_dest, 1.5, m, -0.5, 0, _dest);
	cv::normalize(_dest, _dest, 0.0, 255.0, cv::NORM_MINMAX);
}
void fvkContrastEnhancerUtil::performContrastEnhancerFilter(const cv::Mat& _src, cv::Mat& _dest, Params _param)
{
	performContrastEnhancerFilter(_src, _dest, _param.Delta, _param.Sigma, _param.Clipping, _param.Exposure, _param.Gamma, _param.Optimize);
}

void fvkContrastEnhancerUtil::autoBrightnessAndContrast(const cv::Mat &src, cv::Mat &dst, float clipHistPercent)
{
	CV_Assert(clipHistPercent >= 0);
	CV_Assert((src.type() == CV_8UC1) || (src.type() == CV_8UC3) || (src.type() == CV_8UC4));

	int histSize = 256;
	float alpha, beta;
	double minGray = 0, maxGray = 0;

	//to calculate grayscale histogram
	cv::Mat gray;
	if (src.type() == CV_8UC1) gray = src;
	else if (src.type() == CV_8UC3) cv::cvtColor(src, gray, cv::COLOR_BGR2GRAY);
	else if (src.type() == CV_8UC4) cv::cvtColor(src, gray, cv::COLOR_BGRA2GRAY);

	if (clipHistPercent == 0)
	{
		// keep full available range
		cv::minMaxLoc(gray, &minGray, &maxGray);
	}
	else
	{
		cv::Mat hist; //the grayscale histogram

		float range[] = { 0, 256 };
		const float* histRange = { range };
		bool uniform = true;
		bool accumulate = false;
		calcHist(&gray, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);

		// calculate cumulative distribution from the histogram
		std::vector<float> accumulator(histSize);
		accumulator[0] = hist.at<float>(0);
		for (int i = 1; i < histSize; i++)
		{
			accumulator[i] = accumulator[i - 1] + hist.at<float>(i);
		}

		// locate points that cuts at required value
		float max = accumulator.back();
		clipHistPercent *= (max / 100.0); //make percent as absolute
		clipHistPercent /= 2.0; // left and right wings
		// locate left cut
		minGray = 0;
		while (accumulator[minGray] < clipHistPercent)
			minGray++;

		// locate right cut
		maxGray = histSize - 1;
		while (accumulator[maxGray] >= (max - clipHistPercent))
			maxGray--;
	}

	// current range
	float inputRange = maxGray - minGray;

	alpha = (histSize - 1) / inputRange;   // alpha expands current range to histsize range
	beta = -minGray * alpha;             // beta shifts current range so that minGray will go to 0

	// Apply brightness and contrast normalization
	// convertTo operates with saurate_cast
	src.convertTo(dst, -1, alpha, beta);

	// restore alpha channel from source 
	if (dst.type() == CV_8UC4)
	{
		int from_to[] = { 3, 3 };
		cv::mixChannels(&src, 4, &dst, 1, from_to, 1);
	}
	return;
}