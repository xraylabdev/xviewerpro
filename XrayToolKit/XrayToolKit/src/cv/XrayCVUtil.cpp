/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/05/20
** filename: 	XrayCVUtil.cpp
** file base:	XrayCVUtil
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides utility function for cv::Mat.
****************************************************************************/

#include "XrayCVUtil.h"

XRAYLAB_USING_NAMESPACE

QImage XrayCVUtil::setBrightness(const QImage& _img, int _value)
{
	if (_img.isNull() || _value == 0)
		return QImage();

	auto mat = toMat(_img, true);
	auto value = cvFloor(255.f * (static_cast<float>(_value) / 100.f));
	auto m = cv::Mat(cv::Mat::zeros(mat.size(), mat.type()));
	mat.convertTo(m, -1, 1.0, static_cast<double>(value));
	return toQImage(mat);
}

void XrayCVUtil::setAlpha(QImage& _img, int _value)
{
	for (auto i = 0; i < _img.width(); i++)
	{
		for (auto j = 0; j < _img.height(); j++)
		{
			auto color = _img.pixelColor(i, j);
			auto hue = color.hue();

			color.setAlpha(_value);

			color.setHsv(hue, color.saturation(), color.value(), color.alpha());
			_img.setPixelColor(i, j, color);
		}
	}
}

int XrayCVUtil::getWindowLevelValue(int x, int r1, int s1, int r2, int s2, int typeMaxValue)
{
	float result;
	if (0 <= x && x <= r1)
		result = s1 / r1 * x;
	else if (r1 < x && x <= r2)
		result = ((s2 - s1) / (r2 - r1)) * (x - r1) + s1;
	else if (r2 < x && x <= typeMaxValue)
		result = ((typeMaxValue - s2) / (typeMaxValue - r2)) * (x - r2) + s2;

	return static_cast<int>(result);
}
void XrayCVUtil::applyWindowLevel(const cv::Mat& image, cv::Mat& outImage, int r1, int s1, int r2, int s2)
{
	if (image.channels() == 1)
	{
		for (auto y = 0; y < image.rows; y++)
		{
			for (auto x = 0; x < image.cols; x++)
			{
				auto output = getWindowLevelValue(image.at<uchar>(y, x), r1, s1, r2, s2, 255);
				outImage.at<uchar>(y, x) = static_cast<uchar>(output);
			}
		}
	}
	else if (image.channels() == 3)
	{
		for (auto y = 0; y < image.rows; y++)
		{
			for (auto x = 0; x < image.cols; x++)
			{
				for (auto c = 0; c < 3; c++)
				{
					auto output = getWindowLevelValue(image.at<cv::Vec3b>(y, x)[c], r1, s1, r2, s2, 255);
					outImage.at<cv::Vec3b>(y, x)[c] = cv::saturate_cast<uchar>(output);
				}
			}
		}
	}
	else if (image.channels() == 4)
	{
		for (auto y = 0; y < image.rows; y++)
		{
			for (auto x = 0; x < image.cols; x++)
			{
				auto bgra = image.at<cv::Vec4b>(y, x);
				auto b = getWindowLevelValue(bgra[0], r1, s1, r2, s2, 255);
				auto g = getWindowLevelValue(bgra[1], r1, s1, r2, s2, 255);
				auto r = getWindowLevelValue(bgra[2], r1, s1, r2, s2, 255);
				bgra[0] = cv::saturate_cast<uchar>(b);
				bgra[1] = cv::saturate_cast<uchar>(g);
				bgra[2] = cv::saturate_cast<uchar>(r);
				outImage.at<cv::Vec4b>(y, x) = bgra;
			}
		}
	}
}
void XrayCVUtil::applyWindowLevel16(const cv::Mat& image, cv::Mat& outImage, int r1, int s1, int r2, int s2)
{
	if (image.channels() == 1)
	{
		for (auto y = 0; y < image.rows; y++)
		{
			for (auto x = 0; x < image.cols; x++)
			{
				auto output = getWindowLevelValue(image.at<unsigned short>(y, x), r1, s1, r2, s2, 65535);
				outImage.at<unsigned short>(y, x) = static_cast<unsigned short>(output);
			}
		}
	}
}

const int XrayCVUtil::getAreaUsingShoelaceFormula(const std::vector<cv::Point>& points)
{
	if (points.empty())
		return 0;

	std::vector<int> pointsX(points.size());
	std::vector<int> pointsY(points.size());

	auto n = 0;
	for (std::size_t i = points.size(); i-- != 0; )
		pointsX[n++] = points[i].x;

	n = 0;
	for (std::size_t i = points.size(); i-- != 0; )
		pointsY[n++] = points[i].y;

	auto area = 0;
	auto j = points.size() - 1;
	for (std::size_t i = 0; i < points.size(); i++)
	{
		area = area + (pointsX[j] + pointsX[i]) * (pointsY[j] - pointsY[i]);
		j = i;
	}

	return std::abs(area / 2);
}
cv::Mat XrayCVUtil::fillContours(const cv::Size& size, const std::vector<std::vector<cv::Point> >& contours, const cv::Scalar& color)
{
	cv::Mat filledImage = cv::Mat::zeros(size, CV_8UC1);
	cv::fillPoly(filledImage, contours, color);

	return filledImage;
}
cv::Mat XrayCVUtil::fillContour(const cv::Size& size, const std::vector<cv::Point>& contour, const cv::Scalar& color)
{
	if (size.empty() || contour.empty())
		return {};

	std::vector<std::vector<cv::Point> > contours = { contour };
	cv::Mat filledImage = cv::Mat::zeros(size, CV_8UC1);
	cv::fillPoly(filledImage, contours, color);

	return filledImage;
}
std::vector<cv::Point> XrayCVUtil::getNonZeroPixels(cv::Mat& mat)
{
	std::vector<cv::Point> nonZeroPixels;
	if (mat.empty() || mat.channels() != 1)
		return nonZeroPixels;

	cv::findNonZero(mat, nonZeroPixels);
	return nonZeroPixels;
}
int XrayCVUtil::getNumberOfPixelsInsidePolygon(const cv::Size& size, const std::vector<cv::Point>& contour)
{
	auto filledImage = fillContour(size, contour, cv::Scalar(255, 255, 255));
	if (filledImage.empty())
		return 0;

	//cv::imshow("filled image ", filledImage);
	auto nonZeroPixels = getNonZeroPixels(filledImage);

	return static_cast<int>(nonZeroPixels.size());
}
bool XrayCVUtil::isPixelInside(const std::vector<cv::Point>& contour, const cv::Point& point)
{
	auto isInside = cv::pointPolygonTest(contour, point, false);
	if (isInside >= 0)	// inside or on edge
		return true;
	return false;
}
bool XrayCVUtil::isContourInsideContour(const std::vector<cv::Point>& insideContour, const std::vector<cv::Point>& boundContour)
{
	for (auto& i : insideContour)
	{
		if (!isPixelInside(boundContour, i))
			return false;
	}

	return true;
}


bool isDICOM(const char* _fileName)
{
	auto fp = fopen(_fileName, "rb");
	if (!fp) return false;

	char buf[128 + 4];
	auto rc = fread(buf, 1, 128 + 4, fp);
	if (rc != 128 + 4)
	{
		fclose(fp);
		return false;
	}
	auto is_dicom = 
		buf[128 + 0] == 'D' && buf[128 + 1] == 'I' && 
		buf[128 + 2] == 'C' && buf[128 + 3] == 'M';
	fclose(fp);
	return is_dicom;
}

//int Fle_ImageUtil::batchInvert(const std::string& _directory_path, bool _hasDICOM, Fle_StatusBar* _sb)
//{
//	if (_directory_path.empty()) return 0;
//
//	auto files = Fle_ImageUtil::getDirectoryImageFiles(_directory_path, _hasDICOM);
//	if (files.empty()) return 0;
//
//	if (_sb) _sb->showMessage("Inverting! please wait...", 100000);
//
//	auto t = Fle_Timer::getLocalTime("%Y-%m-%d %X");
//	auto folder = "Inverted-" + t;
//	std::replace(folder.begin() + folder.size() - t.size(), folder.end(), ':', '-');
//	auto dir = _directory_path + Fle_StringUtil::separator() + folder;
//
//	bool r;
//#if (_MSC_VER >= 1900)
//	namespace fs = std::experimental::filesystem;
//	r = fs::create_directories(dir);
//#else
//	r = Fle_WindowsUtil::create_directory(dir);
//#endif
//
//	if (r)
//	{
//		if (_sb) _sb->showMessage("Inverting and saving! please wait...", 100000);
//
//		auto n = 0;
//		for (std::size_t i = 0; i < files.size(); i++)
//		{
//			auto name = Fle_StringUtil::extractFileNameWithoutExt(files[i]);
//			auto img = cv::imread(files[i], cv::IMREAD_UNCHANGED);
//			if (img.empty())
//			{
//				if (_sb) _sb->showMessage("Couldn't read the image (" + name + ")!", 10);
//				continue;
//			}
//
//			// opencv doesn't support DCM writing, so just save into jpg.
//			auto ext = Fle_StringUtil::extractFileExt(files[i]);
//			if (ext == "dcm")
//				name += ".jpg";
//
//			// check the depth of the imported image and convert to 8 bit image.
//			auto src(img);
//			if (src.depth() == CV_16U || src.depth() == CV_16S)
//			{
//				double min, max;
//				cv::minMaxLoc(src, &min, &max);
//				src.convertTo(src, CV_8UC1, 255.0 / (max - min));
//			}
//
//			cv::Mat m;
//			cv::bitwise_not(src, m);
//			if (cv::imwrite(dir + Fle_StringUtil::separator() + name, m))
//			{
//				if (_sb) _sb->showMessage(Fle_StringUtil::to_string(n) + " inverted and saved!", 8);
//				n++;
//			}
//		}
//
//		return n;
//	}
//
//	return 0;
//}
