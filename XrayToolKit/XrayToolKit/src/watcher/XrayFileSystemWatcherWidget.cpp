﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/02
** filename: 	XrayFileSystemWatcherWidget.cpp
** file base:	XrayFileSystemWatcherWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the file system monitoring widget for
				specified folders.
****************************************************************************/

#include "XrayFileSystemWatcherWidget.h"
#include "XrayCTMachinesStatusWidget.h"
#include "XrayIconPushButton.h"
#include "XrayGlobal.h"
#include "XrayQtUtil.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QDesktopServices>
#include <QDateTime>
#include <QLayout>
#include <QDir>
#include <QDebug>
#include <QPushButton>
#include <QMessageBox>
#include <QSettings>
#include <QInputDialog>
#include <QGroupBox>
#include <QScrollArea>

XRAYLAB_USING_NAMESPACE

XrayFileSystemWatcherWidget::XrayFileSystemWatcherWidget(QWidget* parent) :
	QMainWindow(parent),
	p_centeralTabWidget(new QTabWidget(this))
{
	setWindowTitle(QApplication::applicationName() + " - XFileSys Pro");
	setWindowIcon(QApplication::windowIcon());
	setCentralWidget(p_centeralTabWidget);

	dockWidget = new QDockWidget(QApplication::translate("XrayFileSystemWatcherMainWidget", "Project:"), this);
	dockWidget->setMinimumSize(QSize(320, 10));
	dockWidget->setMaximumSize(QSize(320, 99999));
	dockWidget->setAutoFillBackground(false);
	dockWidget->setEnabled(true);
	dockWidget->setFeatures(QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetMovable);
	dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	//auto dockWidgetTitle = new QLabel("Info", dockWidget);
	//dockWidgetTitle->setStyleSheet("QLabel { font-size: 11px; font-weight: bold; color: rgb(255, 255, 255); }");
	//dockWidget->setTitleBarWidget(dockWidgetTitle);
	dockWidget->setStyleSheet("QDockWidget { font-size: 12px; border: 1px; }");
	connect(dockWidget, &QDockWidget::visibilityChanged, [this](bool _visibility) { emit dockVisibilityChanged(_visibility); });

	p_progressWidget = new XrayProgressWidget(this);
	p_progressWidget->setStyleSheet("QWidget { background-color: transparent; }");
	p_progressWidget->setMaximumWidth(320);

	// setup of file system manager tab
	p_fileSysTree = new XrayFileSystemWatcherTreeWidget(this);
	connect(p_fileSysTree, &XrayFileSystemWatcherTreeWidget::onStart, p_progressWidget, &XrayProgressWidget::onStart);
	connect(p_fileSysTree, &XrayFileSystemWatcherTreeWidget::onProgress, p_progressWidget, &XrayProgressWidget::setCurrentProgress);
	connect(p_fileSysTree, &XrayFileSystemWatcherTreeWidget::onFinished, p_progressWidget, &XrayProgressWidget::onFinish);
	connect(p_fileSysTree, &XrayFileSystemWatcherTreeWidget::onClicked_onPcaFileItem, this, static_cast<void (XrayFileSystemWatcherWidget::*)(const QString&)>(&XrayFileSystemWatcherWidget::updatePcaFileInfoLayout));
	connect(p_fileSysTree, &XrayFileSystemWatcherTreeWidget::onClicked_onPcaFileItem, this, static_cast<void (XrayFileSystemWatcherWidget::*)(const QString&)>(&XrayFileSystemWatcherWidget::updateDockWidgetTitle));
	p_searchLineEdit = new QLineEdit("");
	connect(p_searchLineEdit, &QLineEdit::textEdited, p_fileSysTree, [this](const QString& _text) { p_fileSysTree->searchAndSelectionItem(_text, 0); });

	auto fileSystemTreeContainerLayout = new QVBoxLayout;
	fileSystemTreeContainerLayout->setContentsMargins(8, 8, 8, 8);
	fileSystemTreeContainerLayout->addWidget(p_fileSysTree);
	fileSystemTreeContainerLayout->addWidget(p_searchLineEdit);
	auto fileSystemTreeContainer = new QWidget(p_centeralTabWidget);
	fileSystemTreeContainer->setLayout(fileSystemTreeContainerLayout);

	p_centeralTabWidget->addTab(fileSystemTreeContainer, "Projects");

	// setup of CT machines status tab
	//auto machinesContainer = new XrayCTMachinesStatusWidget(p_centeralTabWidget);
	//p_centeralTabWidget->addTab(machinesContainer, "CT Machines");


	auto pcaFileInfoGroupBox = new QGroupBox(QApplication::translate("XrayFileSystemWatcherWidget", "PCA Info"), dockWidget);
	pcaFileInfoGroupBox->setStyleSheet("QWidget { border: 1px solid rgb(80, 80, 80); }");
	auto pcaFileInfoLayoutWidget = new QWidget;
	pcaFileInfoLayoutWidget->setStyleSheet("QWidget { border: 0px }");
	pcaFileInfoLayout = new QGridLayout;
	pcaFileInfoLayout->setContentsMargins(8, 8, 8, 8);
	updatePcaFileInfoLayout("CT_Filterdeckel_02.pca", pcaFileInfoLayout);
	pcaFileInfoLayoutWidget->setLayout(pcaFileInfoLayout);
	auto scrollArea = new QScrollArea;
	scrollArea->setWidgetResizable(true);
	scrollArea->setWidget(pcaFileInfoLayoutWidget);
	scrollArea->setStyleSheet("QScrollArea { border: 0px; } QScrollBar { border: 0px; }");
	auto scrollAreaLayout = new QGridLayout;
	scrollAreaLayout->setMargin(1);
	scrollAreaLayout->addWidget(scrollArea);
	pcaFileInfoGroupBox->setLayout(scrollAreaLayout);

	auto vLayout = new QVBoxLayout;
	vLayout->setContentsMargins(8, 8, 8, 8);
	auto widget = new QWidget(dockWidget);
	widget->setStyleSheet("QWidget { border: 1px solid rgb(128, 85, 0); }");

	// add slide images.
	auto slideShowWidgetGroupBox = new QGroupBox(QApplication::translate("XrayFileSystemWatcherWidget", "Image Box"), dockWidget);
	slideShowWidgetGroupBox->setStyleSheet("QWidget { border: 1px solid rgb(80, 80, 80); }");
	p_slideShowWidget = new XrayImageSlideShowWidget({":/res/images/.png"}, widget);
	p_slideShowWidget->setTransitionType(XrayImageSlideShowWidget::None);
	p_slideShowWidget->setScaledContents(false);
	p_slideShowWidget->setFixedHeight(300);
	p_slideShowWidget->setStyleSheet("QWidget { border: 0px solid rgb(128, 85, 0); }");
	p_slideShowWidget->getStatusBar().setHidden(false);
	connect(p_fileSysTree, &XrayFileSystemWatcherTreeWidget::onClicked_on2DItem, p_slideShowWidget, &XrayImageSlideShowWidget::setImageFiles);
	connect(p_fileSysTree, &XrayFileSystemWatcherTreeWidget::onClicked_on2DItem, this, static_cast<void (XrayFileSystemWatcherWidget::*)(const QStringList&)>(&XrayFileSystemWatcherWidget::updateDockWidgetTitle));
	auto slideShowWidgetLayout = new QGridLayout;
	slideShowWidgetLayout->setMargin(1);
	slideShowWidgetLayout->addWidget(p_slideShowWidget);
	slideShowWidgetGroupBox->setLayout(slideShowWidgetLayout);

	vLayout->addWidget(pcaFileInfoGroupBox);
	vLayout->addSpacerItem(new QSpacerItem(25, 25, QSizePolicy::Preferred, QSizePolicy::Expanding));
	vLayout->addWidget(slideShowWidgetGroupBox);
	widget->setLayout(vLayout);

	dockWidget->setWidget(widget);
	addDockWidget(Qt::RightDockWidgetArea, dockWidget);

	p_statusBar = new XrayStatusBar(this);
	p_statusBar->setSizeGripEnabled(false);
	p_statusBar->setContentsMargins(5, 0, 5, 0);
	p_statusBar->setStyleSheet("QStatusBar { font-size: 11px; font-weight: normal; color: rgb(200, 200, 200); } QLabel { font-size: 11px; font-weight: normal; color: rgb(200, 200, 200); }");
	p_statusBar->addPermanentWidget(p_progressWidget);
	connect(p_fileSysTree, &XrayFileSystemWatcherTreeWidget::onFinished, [this]()
	{
		p_statusBar->showMessage(tr("Last updated: ") + QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss"));
	});
	setStatusBar(p_statusBar);
}
XrayFileSystemWatcherWidget::~XrayFileSystemWatcherWidget()
{
}

struct PcaFileInfo
{
	QString FDD = "n/a";
	QString FOD = "n/a";
	QString Magnification = "n/a";
	QString VoxelSizeX = "n/a";
	QString NumberImages = "n/a";
	QString RotationSector = "n/a";
	QString Active = "n/a";
	QString TimingVal = "n/a";
	QString Avg = "n/a";
	QString Skip = "n/a";
	QString Binning = "n/a";
	QString Name = "n/a";
	QString ID = "n/a";
	QString Voltage = "n/a";
	QString Current = "n/a";
	QString Mode = "n/a";
	QString Filter = "n/a";
	QString YSample = "n/a";
	QString BHC_Param = "n/a";
};
PcaFileInfo readPcaFileInfo(QSettings& _setting)
{
	PcaFileInfo pca;

	pca.FDD = _setting.value("Geometry/FDD", pca.FDD).toString();
	pca.FOD = _setting.value("Geometry/FOD", pca.FOD).toString();
	pca.Magnification = _setting.value("Geometry/Magnification", pca.Magnification).toString();
	pca.VoxelSizeX = _setting.value("Geometry/VoxelSizeX", pca.VoxelSizeX).toString();
	pca.NumberImages = _setting.value("CT/NumberImages", pca.NumberImages).toString();
	pca.RotationSector = _setting.value("CT/RotationSector", pca.RotationSector).toString();
	pca.Active = _setting.value("Multiscan/Active", pca.Active).toString();
	pca.TimingVal = _setting.value("Detector/TimingVal", pca.TimingVal).toString();
	pca.Avg = _setting.value("Detector/Avg", pca.Avg).toString();
	pca.Skip = _setting.value("Detector/Skip", pca.Skip).toString();
	pca.Binning = _setting.value("Detector/Binning", pca.Binning).toString();
	pca.Name = _setting.value("Xray/Name", pca.Name).toString();
	pca.ID = _setting.value("Xray/ID", pca.ID).toString();
	pca.Voltage = _setting.value("Xray/Voltage", pca.Voltage).toString();
	pca.Current = _setting.value("Xray/Current", pca.Current).toString();
	pca.Mode = _setting.value("Xray/Mode", pca.Mode).toString();
	pca.Filter = _setting.value("Xray/Filter", pca.Filter).toString();
	pca.YSample = _setting.value("Axis/YSample", pca.YSample).toString();
	pca.BHC_Param = _setting.value("BHC_Values/BHC_Param", pca.BHC_Param).toString();

	return pca;
}
void updatePcaFileInfoLabels(const PcaFileInfo& _pca, QList<QPair<QLabel*, QLabel*> >& _labels)
{
	auto n = 0;
	_labels[n].first->setText("FDD");
	_labels[n++].second->setText(_pca.FDD);

	_labels[n].first->setText("FOD");
	_labels[n++].second->setText(_pca.FOD);

	_labels[n].first->setText("Magnification");
	_labels[n++].second->setText(_pca.Magnification);

	_labels[n].first->setText("VoxelSizeX");
	_labels[n++].second->setText(_pca.VoxelSizeX);

	_labels[n].first->setText("NumberImages");
	_labels[n++].second->setText(_pca.NumberImages);

	_labels[n].first->setText("RotationSector");
	_labels[n++].second->setText(_pca.RotationSector);

	_labels[n].first->setText("Active");
	_labels[n++].second->setText(_pca.Active);

	_labels[n].first->setText("TimingVal");
	_labels[n++].second->setText(_pca.TimingVal);

	_labels[n].first->setText("Avg");
	_labels[n++].second->setText(_pca.Avg);

	_labels[n].first->setText("Skip");
	_labels[n++].second->setText(_pca.Skip);

	_labels[n].first->setText("Binning");
	_labels[n++].second->setText(_pca.Binning);

	_labels[n].first->setText("Name");
	_labels[n++].second->setText(_pca.Name);

	_labels[n].first->setText("ID");
	_labels[n++].second->setText(_pca.ID);

	_labels[n].first->setText("Voltage");
	_labels[n++].second->setText(_pca.Voltage);

	_labels[n].first->setText("Current");
	_labels[n++].second->setText(_pca.Current);

	_labels[n].first->setText("Mode");
	_labels[n++].second->setText(_pca.Mode);

	_labels[n].first->setText("Filter");
	_labels[n++].second->setText(_pca.Filter);

	_labels[n].first->setText("YSample");
	_labels[n++].second->setText(_pca.YSample);

	_labels[n].first->setText("BHC_Param");
	_labels[n++].second->setText(_pca.BHC_Param);
}
void XrayFileSystemWatcherWidget::updatePcaFileInfoLayout(const QString& _pcaFileName, QGridLayout* _layout)
{
	XrayQtUtil::deleteChildLabels(_layout);

	QList<QPair<QLabel*, QLabel*> > _labels;

	auto pcaInfoN = 0;
	for (auto i = 0; i < 19; i++)
	{
		auto pcaInfoLabel = new QLabel("");
		pcaInfoLabel->setStyleSheet("QLabel { font-size: 12px; font-weight: bold; color: rgb(159,207,249); }");
		auto pcaInfoValue = new QLabel("");
		pcaInfoValue->setStyleSheet("QLabel { font-size: 11px; font-weight: normal; color: rgb(159,207,249); }");
		_layout->addWidget(pcaInfoLabel, pcaInfoN, 0);
		_layout->addWidget(pcaInfoValue, pcaInfoN++, 1);
		_labels.append(QPair<QLabel*, QLabel*>(pcaInfoLabel, pcaInfoValue));
	}

	QSettings settings(_pcaFileName, QSettings::IniFormat);
	updatePcaFileInfoLabels(readPcaFileInfo(settings), _labels);
}
void XrayFileSystemWatcherWidget::updatePcaFileInfoLayout(const QString& _pcaFileName)
{
	if (!QFile::exists(_pcaFileName))
	{
		p_statusBar->showMessage("File doesn't exists anymore!", 10000);
		return;
	}
	updatePcaFileInfoLayout(_pcaFileName, pcaFileInfoLayout);
}
void XrayFileSystemWatcherWidget::updateDockWidgetTitle(const QString& _path)
{
	if (_path.size() < 2)
		return;

	dockWidget->setWindowTitle("Project: " + _path.split('/').at(1));	// project name
}
void XrayFileSystemWatcherWidget::updateDockWidgetTitle(const QStringList& _paths)
{
	if (_paths.empty())
		return;

	updateDockWidgetTitle(_paths.front());
}

void XrayFileSystemWatcherWidget::closeEvent(QCloseEvent* _event)
{
	//if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayMainWindow", "Are you sure you want to quit?"), QMessageBox::Yes | QMessageBox::No).exec())
	//{
		_event->accept();
		return;
	//}
	_event->ignore();
}