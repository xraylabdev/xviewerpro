/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/06
** filename: 	XrayFileSystemStructure.cpp
** file base:	XrayFileSystemStructure
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the main window of the application.
****************************************************************************/

#include "XrayFileSystemStructure.h"
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cctype>

XRAYLAB_USING_NAMESPACE

/****************************************************************************
	Base class for handling directory and files of the file system
****************************************************************************/
XraySysBase::XraySysBase() :
	m_parent(nullptr)
{
}
XraySysBase::XraySysBase(const std::string& name, XraySysDirectory* parent) :
	m_name(name),
	m_parent(parent)
{
}
XraySysBase::~XraySysBase()
{
}

bool XraySysBase::remove()
{
	if (!m_parent)
		return false;
	return m_parent->remove(this);
}

/****************************************************************************
	Class for directory files
****************************************************************************/
XraySysFile::XraySysFile(const std::string& name, XraySysDirectory* parent, std::size_t size) :
	XraySysBase(name, parent),
	m_size(size)
{
}
XraySysFile::~XraySysFile()
{
}


/****************************************************************************
	Class for directories
****************************************************************************/
XraySysDirectory::XraySysDirectory()
{
}
XraySysDirectory::XraySysDirectory(const std::string& name, XraySysDirectory* parent) :
	XraySysBase(name, parent)
{
}
XraySysDirectory::~XraySysDirectory()
{
	clear();
}

void XraySysDirectory::clear()
{
	for (auto& parent : m_contents)
	{
		if (auto dir = dynamic_cast<XraySysDirectory*>(parent))
		{
			auto& list = dir->getContents();
			for (auto& f : list)
			{
				if (f)
				{
					//std::cout << "Deleting File..." << f->getName() << "\n";
					delete f;
					f = nullptr;
				}
			}

			//std::cout << "Deleting Dir..." << dir->getName() << "\n";
			delete dir;
			dir = nullptr;
		}
	}
	m_contents.clear();
}

std::size_t XraySysDirectory::getSize() const
{
	std::size_t size = 0;
	for (const auto& i : m_contents)
		size += i->getSize();

	return size;
}

int XraySysDirectory::getNumberOfChilds()
{
	int count = 0;
	for (const auto& i : m_contents)
	{
		if (auto dir = dynamic_cast<XraySysDirectory*>(i))
			count += dir->getNumberOfChilds();
		else if (auto file = dynamic_cast<XraySysFile*>(i))
			count++;
	}
	return count;
}

std::vector<XraySysDirectory> XraySysDirectory::getDirectories()
{
	std::vector<XraySysDirectory> files;
	files.reserve(m_contents.size());
	for (const auto& i : m_contents)
	{
		if (auto file = dynamic_cast<XraySysDirectory*>(i))
			files.emplace_back(*file);
	}
	return files;
}

std::vector<XraySysFile> XraySysDirectory::getFiles()
{
	std::vector<XraySysFile> files;
	files.reserve(m_contents.size());
	for (const auto& i : m_contents)
	{
		if (auto file = dynamic_cast<XraySysFile*>(i))
			files.emplace_back(*file);
	}
	return files;
}

void XraySysDirectory::print()
{
	for (const auto& i : m_contents)
		print(i);
}

void XraySysDirectory::print(XraySysBase* parent)
{
	if (auto dir = dynamic_cast<XraySysDirectory*>(parent))
	{
		std::cout << dir->getName() << "\n";
		auto files = dir->getFiles();
		for (const auto& i : files)
			std::cout << std::setw(5) << "" << i.getName() << "\n";
	}
}


XraySysBase* XraySysDirectory::get(const std::string& name)
{
	for (const auto& i : m_contents)
	{
		if (i->getName() == name)
			return i;
	}
	return nullptr;
}
XraySysBase* XraySysDirectory::get(std::size_t index)
{
	if (index < 0 || index >= m_contents.size())
		return nullptr;
	return m_contents[index];
}

void XraySysDirectory::add(XraySysBase* entry)
{
	m_contents.push_back(entry);
}

bool XraySysDirectory::remove(XraySysBase* entry)
{
	if (!entry)
		return false;

	// first remove from the list
	m_contents.erase(std::remove(m_contents.begin(), m_contents.end(), entry), m_contents.end());

	// now delete it's childes and itself
	if (auto dir = dynamic_cast<XraySysDirectory*>(entry))
	{
		auto& list = dir->getContents();
		for (auto& f : list)
		{
			if (f)
			{
				//std::cout << "Deleting File..." << f->getName() << "\n";
				delete f;
				f = nullptr;
			}
		}

		// delete itself
		//std::cout << "Deleting Dir..." << dir->getName() << "\n";
		delete dir;
		dir = nullptr;

		return true;
	}
	return false;
}

/****************************************************************************
	Class for file system structure
****************************************************************************/
XrayFileSystemStructure::XrayFileSystemStructure(const std::vector<std::string>& fileExtFilters, const std::vector<std::string>& fileNameContainFilters) :
	m_fileExtFilters(fileExtFilters),
	m_fileContainFilters(fileNameContainFilters)
{
	// make extension to lower case
	for (auto& ext : m_fileExtFilters)
		std::transform(ext.begin(), ext.end(), ext.begin(), [](unsigned char c) { return std::tolower(c); });

	for (auto& name : m_fileContainFilters)
		std::transform(name.begin(), name.end(), name.begin(), [](unsigned char c) { return std::tolower(c); });
}
XrayFileSystemStructure::~XrayFileSystemStructure()
{
}
void XrayFileSystemStructure::clear()
{
	m_sysDir.clear();
}

void XrayFileSystemStructure::setFileExtFilters(const std::vector<std::string>& filters)
{ 
	m_fileExtFilters = filters;
	// make extensions to lower case
	for (auto& ext : m_fileExtFilters)
		std::transform(ext.begin(), ext.end(), ext.begin(), [](unsigned char c) { return std::tolower(c); });
}
void XrayFileSystemStructure::setFileContainFilters(const std::vector<std::string>& filters)
{
	m_fileContainFilters = filters;
	// make extensions to lower case
	for (auto& name : m_fileContainFilters)
		std::transform(name.begin(), name.end(), name.begin(), [](unsigned char c) { return std::tolower(c); });
}

static bool hasExtension(std::string ext, const std::vector<std::string>& extensions)
{
	if (extensions.empty() || ext.empty())
		return false;

	std::transform(ext.begin(), ext.end(), ext.begin(), [](unsigned char c) { return std::tolower(c); });

	for (const auto& i : extensions)
	{
		if (i == ext)
			return true;
	}
	return false;
}

static bool containName(std::string name, const std::vector<std::string>& names)
{
	if (names.empty() || name.empty())
		return false;

	std::transform(name.begin(), name.end(), name.begin(), [](unsigned char c) { return std::tolower(c); });

	for (const auto& i : names)
	{
		if (name.find(i) != std::string::npos)
			return true;
	}
	return false;
}

/*!
 * Description:
 * This function takes the root directory path and iterate all sub-directories and files.
 * It checks if the next entry is directory or a file. If its a directory then start 
   recursive interaction to check its files.
   If it finds a file then it checks filters and add filtered file to the directory.
*/
void XrayFileSystemStructure::scan(const std::filesystem::path& rootPath)
{
	for (const auto& entry : std::filesystem::directory_iterator(rootPath))
	{
		const auto fileName = entry.path().filename().string();
		const auto absolutePath = entry.path().string();

		if (entry.is_directory())
		{
			auto dir = new XraySysDirectory(absolutePath, nullptr);
			dir->setBaseName(fileName);
			dir->setLastWriteTime(entry.last_write_time());
			m_sysDir.add(dir);

			scan(entry);
		}
		else if (entry.is_regular_file())
		{
			auto dir = entry.path().parent_path().string();
			auto stem = entry.path().stem().string();		// returns file name without extension.
			auto parent = dynamic_cast<XraySysDirectory*>(m_sysDir.get(dir));
			if (parent)
			{
				auto ext = entry.path().extension().string();	// returns extension with dot (.) as a first char.
				
				// case 1: if both filters are empty
				if (m_fileExtFilters.empty() && m_fileContainFilters.empty())
				{
					auto file = new XraySysFile(absolutePath, parent, entry.file_size());
					file->setStem(stem);
					file->setBaseName(fileName);
					file->setLastWriteTime(entry.last_write_time());
					parent->add(file);
				}
				else
				{
					// case 2: if file extensions filters are empty, then we have to check if contain filters are specified
					if (m_fileExtFilters.empty())
					{
						if (!containName(stem, m_fileContainFilters))
						{
							auto file = new XraySysFile(absolutePath, parent, entry.file_size());
							file->setStem(stem);
							file->setBaseName(fileName);
							file->setLastWriteTime(entry.last_write_time());
							parent->add(file);
						}
					}
					// case 3: if contain filters are empty, then we have to check if files are with same extensions
					else if (m_fileContainFilters.empty())
					{
						if (hasExtension(ext, m_fileExtFilters))
						{
							auto file = new XraySysFile(absolutePath, parent, entry.file_size());
							file->setStem(stem);
							file->setBaseName(fileName);
							file->setLastWriteTime(entry.last_write_time());
							parent->add(file);
						}
					}
					// case 4: if both filters are not empty, then we must check file extension and contain filters.
					else if (!m_fileExtFilters.empty() && !m_fileContainFilters.empty())
					{
						if (hasExtension(ext, m_fileExtFilters))
						{
							if (!containName(stem, m_fileContainFilters))
							{
								auto file = new XraySysFile(absolutePath, parent, entry.file_size());
								file->setStem(stem);
								file->setBaseName(fileName);
								file->setLastWriteTime(entry.last_write_time());
								parent->add(file);
							}
						}
					}
				}
			}
		}
	}
}

void XrayFileSystemStructure::scan()
{
	m_sysDir.clear();
	scan(m_root);
}

int XrayFileSystemStructure::getNumberOfChilds()
{
	return m_sysDir.getNumberOfChilds();
}

std::vector<XraySysDirectory> XrayFileSystemStructure::getDirectories()
{
	return m_sysDir.getDirectories();
}

std::vector<XraySysFile> XrayFileSystemStructure::getFiles()
{
	return m_sysDir.getFiles();
}

void XrayFileSystemStructure::print()
{
	m_sysDir.print();
}

void XrayFileSystemStructure::print(XraySysBase* parent)
{
	m_sysDir.print(parent);
}

std::size_t XrayFileSystemStructure::getSize() const
{
	return m_sysDir.getSize();
}

XraySysBase* XrayFileSystemStructure::get(const std::string& name)
{
	return m_sysDir.get(name);
}

XraySysBase* XrayFileSystemStructure::get(std::size_t index)
{
	return m_sysDir.get(index);
}

void XrayFileSystemStructure::add(XraySysBase* entry)
{
	m_sysDir.add(entry);
}

bool XrayFileSystemStructure::remove(XraySysBase* entry)
{
	return m_sysDir.remove(entry);
}

std::vector<XraySysBase*>& XrayFileSystemStructure::getContents()
{ 
	return m_sysDir.getContents();
}
