﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/02
** filename: 	XrayFileSystemWatcherWindow.cpp
** file base:	XrayFileSystemWatcherWindow
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the file system monitoring window for
				specified folders.
****************************************************************************/

#include "XrayFileSystemWatcherWindow.h"
#include "ui_XrayAboutDialog.h"
#include "XrayIconPushButton.h"
#include "XrayGlobal.h"
#include "XrayQtUtil.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QDesktopServices>
#include <QDateTime>
#include <QLayout>
#include <QSettings>
#include <QMenu>
#include <QActionGroup>
#include <QDir>
#include <QDebug>
#include <QPushButton>
#include <QMessageBox>
#include <QSettings>
#include <QInputDialog>
#include <QSpinBox>
#include <QScrollArea>

XRAYLAB_USING_NAMESPACE

XrayFileSystemWatcherWindow::XrayFileSystemWatcherWindow(const QString& _title, QWidget* _parent) :
	XrayFramelessWindowWin32(_title, _parent),
	p_fileSysWatcher(new XrayFileSystemWatcherWidget)
{
	setWindowIcon(QApplication::windowIcon());

	p_menu = createMenu();
	menuButton()->setMenu(p_menu);
	menuButton()->setVisible(true);

	addCentralWidget(p_fileSysWatcher);

	const auto geometry = QApplication::desktop()->screenGeometry(this);
	resize(geometry.width() / 1.1, geometry.height() / 1.1);
	move((geometry.width() / 1.1 - width()) / 2, (geometry.height() / 1.1 - height()) / 2);
	restoreSettings();

	QSettings settings(QApplication::organizationName(), windowTitle());
	p_fileSysWatcher->getFileSystemWatcher()->restoreSettings(settings);
}
XrayFileSystemWatcherWindow::~XrayFileSystemWatcherWindow()
{
}
void XrayFileSystemWatcherWindow::closeEvent(QCloseEvent* _event)
{
	QSettings settings(QApplication::organizationName(), windowTitle());
	p_fileSysWatcher->getFileSystemWatcher()->saveSettings(settings);
	saveSettings();
	_event->accept();
}

QMenu* XrayFileSystemWatcherWindow::createMenu()
{
	auto menu = new QMenu;

	p_actionViewHelp = new QAction(QIcon(":/res/images/help_blue_icon.png"), QApplication::translate("XrayFileSystemWatcherWindow", "View Help"), this);
	connect(p_actionViewHelp, &QAction::triggered, [this]() { });
	//menu->addAction(p_actionViewHelp);

	p_actionWebsite = new QAction(QIcon(":/res/images/website_blue_icon.png"), QApplication::translate("XrayFileSystemWatcherWindow", "Website"), this);
	connect(p_actionWebsite, &QAction::triggered, [this]() { QDesktopServices::openUrl(QUrl("https://xray-lab.com/")); });
	//menu->addAction(p_actionWebsite);

	p_actionAbout = new QAction(QIcon(":/res/images/about_blue_icon.png"), QApplication::translate("XrayFileSystemWatcherWindow", "About"), this);
	connect(p_actionAbout, &QAction::triggered, [this]()
	{
		QDialog dialog;
		dialog.setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint);
		dialog.setWindowIcon(QApplication::windowIcon());
		dialog.setFixedSize(650, 480);
		Ui::XrayAboutDialog d;
		d.setupUi(&dialog);
		d.applicationTitle->setText(QApplication::applicationName() + " v" + QApplication::applicationVersion() + " (64 bit)");
		d.releaseDate->setText("09.04.2019");
		d.moduleName->setText(windowTitle());
		dialog.exec();
	});
	//menu->addAction(p_actionAbout);
	//menu->addSeparator();

	p_actionRestoreLastTree = new QAction(QIcon(":/paint/images/redo_blue_icon.png"), QApplication::translate("XrayFileSystemWatcherWindow", "Restore Last Projects"), this);
	connect(p_actionRestoreLastTree, &QAction::triggered, [this]()
	{
		QSettings settings(QApplication::organizationName(), windowTitle());
		p_fileSysWatcher->getFileSystemWatcher()->restoreSettings(settings);
		p_fileSysWatcher->getFileSystemWatcher()->buildTree();
		p_fileSysWatcher->getFileSystemWatcher()->restart();
	});
	menu->addAction(p_actionRestoreLastTree);

	menu->addSeparator();
	p_actionRootPath = new QAction(QIcon(":/res/images/settings_blue_icon.png"), QApplication::translate("XrayFileSystemWatcherWindow", "Set Root Path..."), this);
	connect(p_actionRootPath, &QAction::triggered, [this]()
	{
		auto dialog = new QDialog(this);
		dialog->setWindowIcon(QApplication::windowIcon());
		dialog->setWindowFlags(dialog->windowFlags() & (~Qt::WindowContextHelpButtonHint | Qt::MSWindowsFixedSizeDialogHint));
		auto l = new QGridLayout;
		l->setContentsMargins(8, 8, 8, 8);
		auto label = new QLabel(QApplication::translate("XrayFileSystemWatcherWindow", "Root Drive:"), dialog);
		auto lineEdit = new QLineEdit(p_fileSysWatcher->getFileSystemWatcher()->getRootPath(), dialog);
		l->addWidget(label, 0, 0);
		l->addWidget(lineEdit, 0, 1);

		label = new QLabel(QApplication::translate("XrayFileSystemWatcherWindow", "Update Interval: "), dialog);
		auto spinBox = new QSpinBox(dialog);
		spinBox->setMinimum(0);
		spinBox->setMaximum(999999999);
		spinBox->setValue(p_fileSysWatcher->getFileSystemWatcher()->getUpdateInterval() / 1000);	// milliseconds to sec
		spinBox->setSuffix(" seconds");
		l->addWidget(label, 1, 0);
		l->addWidget(spinBox, 1, 1);
		l->addWidget(new QWidget());

		auto buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
		connect(buttonBox, &QDialogButtonBox::accepted, dialog, &QDialog::accept);
		connect(buttonBox, &QDialogButtonBox::rejected, dialog, &QDialog::reject);
		l->addWidget(buttonBox, 2, 1);

		dialog->setLayout(l);
		dialog->setFixedSize(300, 160);

		if(dialog->exec())
		{
			auto bl = lineEdit->blockSignals(true);
			auto bs = spinBox->blockSignals(true);
			if (QFile::exists(lineEdit->text()))
			{
				p_fileSysWatcher->getFileSystemWatcher()->setRootPath(lineEdit->text());
				p_fileSysWatcher->getFileSystemWatcher()->setUpdateInterval(spinBox->value() * 1000);	// sec to milliseconds
				p_fileSysWatcher->getFileSystemWatcher()->restart();
			}
			else
			{
				QMessageBox(QMessageBox::Critical, windowTitle(), QApplication::translate("XrayFileSystemWatcherWindow", "Please enter a valid drive path!")).exec();
			}
			spinBox->blockSignals(bs);
			lineEdit->blockSignals(bl);
		}
	});
	menu->addAction(p_actionRootPath);
	p_actionUpdate = new QAction(QIcon(":/res/images/refresh_blue_icon.png"), QApplication::translate("XrayFileSystemWatcherWindow", "Update"), this);
	connect(p_actionUpdate, &QAction::triggered, [this]()
	{
		p_fileSysWatcher->getFileSystemWatcher()->restart();
	});
	menu->addAction(p_actionUpdate);

	menu->addSeparator();
	p_actionDockVisibility = new QAction(QApplication::translate("XrayFileSystemWatcherWindow", "Hide Dock"), this);
	p_actionDockVisibility->setCheckable(true);
	p_actionDockVisibility->setChecked(true);
	connect(p_actionDockVisibility, &QAction::toggled, p_fileSysWatcher, &XrayFileSystemWatcherWidget::setDockVisible);
	connect(p_fileSysWatcher, &XrayFileSystemWatcherWidget::dockVisibilityChanged, [this](bool _visibility)
	{
		auto b = p_actionDockVisibility->blockSignals(true); // if we don't block the signals here, application crash occurs.
		p_actionDockVisibility->setChecked(_visibility);
		if(_visibility)
			p_actionDockVisibility->setText(QApplication::translate("XrayFileSystemWatcherWindow", "Hide Dock"));
		else
			p_actionDockVisibility->setText(QApplication::translate("XrayFileSystemWatcherWindow", "Show Dock"));
		p_actionDockVisibility->blockSignals(b);
	});
	menu->addAction(p_actionDockVisibility);

	return menu;
}