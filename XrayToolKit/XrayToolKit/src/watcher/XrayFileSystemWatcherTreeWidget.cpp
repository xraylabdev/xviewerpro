/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayFileSystemWatcher.cpp
** file base:	XrayFileSystemWatcher
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a tree widget with file system watcher.
****************************************************************************/

#include "XrayFileSystemWatcherTreeWidget.h"

#include <QApplication>
#include <QClipboard>
#include <QDesktopServices>
#include <QUrl>
#include <QGridLayout>
#include <QPushButton>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QFileIconProvider>
#include <QDateTime>
#include <QLabel>
#include <QSettings>
#include <QStack>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayFileSystemWatcherTreeWidgetItem* XrayFileSystemWatcherTreeWidgetItem::clone2() const
{
	XrayFileSystemWatcherTreeWidgetItem *copy = 0;

	QStack<const XrayFileSystemWatcherTreeWidgetItem*> stack;
	QStack<XrayFileSystemWatcherTreeWidgetItem*> parentStack;
	stack.push(this);
	parentStack.push(0);

	XrayFileSystemWatcherTreeWidgetItem *root = 0;
	const XrayFileSystemWatcherTreeWidgetItem *item = 0;
	XrayFileSystemWatcherTreeWidgetItem *parent = 0;
	while (!stack.isEmpty())
	{
		// get current item, and copied parent
		item = stack.pop();
		parent = parentStack.pop();

		// copy item
		copy = new XrayFileSystemWatcherTreeWidgetItem(*item);
		copy->setPath(item->getPath());
		if (!root)
			root = copy;

		// set parent and add to parents children list
		if (parent)
		{
			copy->p_par = parent;
			parent->insertChild(0, copy);
		}

		for (int i = 0; i < item->childCount(); ++i)
		{
			auto it = dynamic_cast<XrayFileSystemWatcherTreeWidgetItem*>(item->child(i));
			stack.push(it);
			parentStack.push(copy);
		}
	}
	return root;
}
QTreeWidgetItem* XrayFileSystemWatcherTreeWidgetItem::searchItem(QTreeWidget* _tree, const QString& _path, QTreeWidgetItemIterator::IteratorFlags _flags)
{
	if (_path.isEmpty())
		return nullptr;

	QTreeWidgetItemIterator it(_tree, _flags);
	while (*it)
	{
		const auto item = dynamic_cast<XrayFileSystemWatcherTreeWidgetItem*>(*it);
		if (item)
		{
			if (item->getPath() == _path)
				return item;
		}
		++it;
	}

	return nullptr;
}
bool XrayFileSystemWatcherTreeWidgetItem::operator < (const QTreeWidgetItem& other) const
{
	auto column = treeWidget()->sortColumn();
	if (column == 6)
	{
		const auto right = dynamic_cast<const XrayFileSystemWatcherTreeWidgetItem*>(&other);
		if (right) return m_dateTime < right->getDateTime();
	}
	return text(column).toLower() < other.text(column).toLower();
}

static QColor subItemLabelColor = QColor(33, 197, 199);
static QColor subItemChildLabelColor = QColor(3, 147, 149);
static QColor subItemChildChildLabelColor = QColor(0, 127, 129);

static QColor subItemTitleRowColor = QColor(57, 158, 244);
static QColor subItemValueRowColor = QColor(159, 207, 249);


/************************************************************************/
/*                                                                      */
/************************************************************************/
XrayFileSystemWatcherTreeWidget::XrayFileSystemWatcherTreeWidget(QWidget* parent) :
	QTreeWidget(parent),
	p_watcher(nullptr)
{
	QString style = "QHeaderView::section {"
		//"background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,"
		//"stop:0 #616161, stop: 0.5 #505050,"
		//"stop: 0.6 #434343, stop:1 #656565);"
		"color: rgb(57, 158, 244);"
		"padding: 10px;"
		"padding-right: 30px;"
		"border-left: 1px solid rgb(128, 85, 0);"
		"border-top: 1px solid rgb(128, 85, 0);"
		"border-bottom: 1px solid rgb(128, 85, 0);"
		"font-size: 16px;"
		"font-weight: bold;"
		"}";
	style += "QTreeWidget::item { padding:10px; border-left: 1px solid rgb(128, 85, 0); border-bottom: 1px solid rgb(128, 85, 0); }";
	////style += "QTreeWidget::item:alternate { background: rgb(128, 128, 128); }";
	//style += "QTreeWidget::item:has-children { color: rgb(255, 255, 255); border-right: 1px solid rgb(128, 85, 0, 255); }";
	style += "QTreeWidget::item:hover,QTreeWidget::item:hover:selected { background-color: rgb(128, 85, 0); }";
	style += "QTreeWidget::item:selected, QTreeWidget::item:selected:active, QTreeWidget::item:selected:!active { background-color: rgb(47, 148, 234); }";
	setStyleSheet(style);

	//setRootIsDecorated(false);							// It will remove the root item as childless.
	//setSelectionMode(QAbstractItemView::MultiSelection);	// For enabling multiple items selection.

	setAlternatingRowColors(true);
	setAnimated(true);
	setSortingEnabled(true);
	sortItems(6, Qt::DescendingOrder);
	setEditTriggers(NoEditTriggers);

	QStringList ColumnNames;
	ColumnNames << "Project" << "Customer name" << "Customer location" << "Component" << "Quantity" << "Contact person" << "Date modified" << "";
	setHeaderLabels(ColumnNames);
	header()->setStretchLastSection(true);
	header()->setSectionResizeMode(QHeaderView::Interactive);
	//header()->setSectionResizeMode(0, QHeaderView::Fixed);

	setIndentation(14);	// 0 will make the whole tree items expanded by double click without any down and up arrows.
	setColumnCount(7);

	connect(this, &QTreeWidget::itemClicked, this, &XrayFileSystemWatcherTreeWidget::onItemClicked);
	connect(this, &QTreeWidget::itemDoubleClicked, this, &XrayFileSystemWatcherTreeWidget::onItemDoubleClicked);
	connect(this, &QTreeWidget::itemActivated, this, &XrayFileSystemWatcherTreeWidget::onItemActivated);
	connect(this, &QTreeWidget::itemEntered, this, &XrayFileSystemWatcherTreeWidget::onItemEntered);

	createCustomMenu();
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QTreeWidget::customContextMenuRequested, this, &XrayFileSystemWatcherTreeWidget::showContextMenu);
}
XrayFileSystemWatcherTreeWidget::~XrayFileSystemWatcherTreeWidget()
{
	// Note: very important to terminate the thread and delete the object.
	if (p_watcher)
	{
		if (p_watcher->isRunning())
		{
			p_watcher->stop();
			p_watcher->quit();
			p_watcher->terminate();
		}
		delete p_watcher;
		p_watcher = nullptr;
	}
}
void XrayFileSystemWatcherTreeWidget::setRootPath(const QString& _path)
{
	m_rootPath = _path;
}
QString XrayFileSystemWatcherTreeWidget::getRootPath() const
{
	return m_rootPath;
}
void XrayFileSystemWatcherTreeWidget::setUpdateInterval(int _updateInterval)
{
	m_delay = _updateInterval;
}
int XrayFileSystemWatcherTreeWidget::getUpdateInterval() const
{
	return m_delay;
}
void XrayFileSystemWatcherTreeWidget::restart()
{
	if (p_watcher)
	{
		p_watcher->stop();
		p_watcher->quit();
		QThread::currentThread()->msleep(500);
		p_watcher->terminate();
		p_watcher->deleteLater();
		p_watcher = nullptr;
		QThread::currentThread()->msleep(200);
	}

	p_watcher = new XrayThreadedFileSystemWatcher(this, m_rootPath, m_delay);
	connect(p_watcher, &XrayThreadedFileSystemWatcher::onStart, [this]() { emit onStart(); });
	connect(p_watcher, &XrayThreadedFileSystemWatcher::onProgress, [this](double _value) { emit onProgress(_value); });
	connect(p_watcher, &XrayThreadedFileSystemWatcher::onFinished, this, &XrayFileSystemWatcherTreeWidget::updateTree);

	emit onProgress(0);
	p_watcher->start(QThread::HighestPriority);
}

void XrayFileSystemWatcherTreeWidget::updateTree()
{
	if (!p_watcher)
		return;

	const auto& paths = p_watcher->getTreePaths();
	if (paths.empty())
		return;

	m_paths.clear();
	m_paths = paths;

	buildTree(paths);
}
void XrayFileSystemWatcherTreeWidget::buildTree()
{
	buildTree(m_paths);
}
void XrayFileSystemWatcherTreeWidget::buildTree(const QStringList& _paths)
{
	if (_paths.empty())
		return;

	auto resizeMode = header()->sectionResizeMode(0);
	auto editTrigger = editTriggers();
	auto rowColors = alternatingRowColors();
	auto anim = isAnimated();
	auto sort = isSortingEnabled();

	// try to disable features that could effect on 
	// performance while top level items adding to the tree.
	setUpdatesEnabled(false);

	header()->setSectionResizeMode(QHeaderView::Fixed);
	setEditTriggers(QTreeWidget::NoEditTriggers);
	setSortingEnabled(false);
	setAnimated(false);
	setAlternatingRowColors(false);

	clear();	// remove all previous tree items first.
	emit onProgress(0);

	auto n = 0;
	for (const auto& i : _paths)
	{
		auto l = i.split("/");
		l.removeAt(0);	// remove the root from the tree.
		buildTree(l, i);

		// emit a progress bar signal with value.
		if (n % 2 == 0)
			emit onProgress(n / static_cast<double>(_paths.size()));
		n++;
		// end of progress bar.
	}

	setAlternatingRowColors(rowColors);
	setAnimated(anim);
	setSortingEnabled(sort);
	if (sort) sortItems(6, Qt::DescendingOrder);
	setEditTriggers(editTrigger);
	header()->setSectionResizeMode(resizeMode);

	setUpdatesEnabled(true);

	emit onFinished();
}
void XrayFileSystemWatcherTreeWidget::buildTree(const QStringList& _list, const QString& _path)
{
	auto dateTime = QFileInfo(_path).lastModified();

	auto topLevelTexts = [&](const QString& _fileName)
	{
		auto projectName = _fileName.split('/').back();
		auto projectFileName = _fileName + "/" + projectName + ".ini";
		auto time = dateTime.toString("dd.MM.yyyy HH:mm:ss");

		QStringList rowItems;

		if (QFile::exists(projectFileName))
		{
			QSettings settings(projectFileName, QSettings::IniFormat);
			auto customerName = settings.value("CustomerInfo/name", "n/a").toString();
			auto customerLocation = settings.value("CustomerInfo/address1", "n/a").toString() + ", " + settings.value("CustomerInfo/address2", "n/a").toString() + ", " + settings.value("CustomerInfo/country", "n/a").toString();
			auto component = settings.value("Project/component", "n/a").toString();
			auto quantity = settings.value("Project/quantity", "n/a").toString();
			auto customerContactPersonName = settings.value("CustomerContactPerson/name", "n/a").toString();
			rowItems << projectName << customerName << customerLocation << component << quantity << customerContactPersonName << time;
		}
		else
		{
			rowItems << projectName << "" << "" << "" << "" << "" << time;
		}

		return rowItems;
	};

	auto parent = invisibleRootItem();
	QFileIconProvider provider;

	// i is the depth of the folder tree. Like in this "D:/ABC/DEF/GHE/M.jpg" path.
	for (int i = 0; i < _list.size(); i++)
	{
		auto item = searchChildItem(parent, 0, _list[i]);
		if (item)
		{
			parent = item;
			continue;
		}

		// i == 0 belongs to the main project items that will always contains numerical name.
		if (i == 0)
		{
			item = new XrayFileSystemWatcherTreeWidgetItem(parent, topLevelTexts(_path), _path, dateTime);
			item->setExpanded(false);
			item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

			auto font = parent->font(0);
			font.setBold(true);
			font.setPointSize(9);
			item->setFont(0, font);

			item->setTextColor(0, QColor(255, 255, 255));
			for (auto column = 1; column < parent->columnCount(); column++)
				item->setTextColor(column, QColor(220, 220, 220));
			
			addTopLevelItem(parent);
		}
		// i == 1 belongs to the "Bilder" and "CT" folders.
		else if (i == 1)
		{
			item = new XrayFileSystemWatcherTreeWidgetItem(parent, { _list[i] }, _path);
			item->setTextColor(0, subItemLabelColor);
		}
		// i == 2 belongs to the sub folders of "Bilder" and "CT" which are "2D", "3D", and "CT_Test_01" folders.
		else if (i == 2)
		{
			item = new XrayFileSystemWatcherTreeWidgetItem(parent, { _list[i] }, _path);
			item->setTextColor(0, subItemTitleRowColor);
		}
		// We are only interested are in JPG and PCA files, so there items belong JPG and PCA files.
		else
		{
			item = new XrayFileSystemWatcherTreeWidgetItem(parent, { _list[i] }, _path);
			item->setTextColor(0, subItemChildChildLabelColor);

			// check if we are adding an image file.
			if (_list[i].endsWith(".jpg", Qt::CaseInsensitive) || _list[i].endsWith(".jpeg", Qt::CaseInsensitive))
			{
				item->setIcon(0, QIcon(":/paint/images/picture_dir_blue_icon.png"));
				item->setFirstColumnSpanned(true);	// this function must be called after adding this item to the parent.

				// set total number of images in the second column of the parent.
				parent->setText(1, QString::number(parent->childCount()));
				parent->setTextColor(1, subItemValueRowColor);
			}
			// check if we are adding a pca file.
			else if (_list[i].endsWith(".pca"))
			{
				item->setIcon(0, provider.icon(QFileIconProvider::File));
				item->setFirstColumnSpanned(true);	// this function must be called after adding this item to the parent.

				// load the PCA file and set PCA values to the parent columns.
				auto cols = 1;
				QSettings settings(_path, QSettings::IniFormat);
				parent->setText(cols++, settings.value("Xray/ID").toString());
				parent->setText(cols++, settings.value("Xray/Voltage").toString());
				parent->setText(cols++, settings.value("Xray/Current").toString());
				parent->setText(cols++, settings.value("CT/NumberImages").toString());
				parent->setText(cols++, settings.value("Detector/TimingVal").toString());
				parent->setText(cols++, settings.value("Detector/Avg").toString());

				// colorize the newly added column.
				for (auto column = 1; column < parent->columnCount(); column++)
					parent->setTextColor(column, subItemTitleRowColor);

				// if this item is the branch item of the root CT, then add new columns to the parent
				// of the parent of this item, that contains the following properties of the PCA file.
				if (parent->parent())
				{
					parent->parent()->setText(1, "ID");
					parent->parent()->setText(2, "Voltage");
					parent->parent()->setText(3, "Current");
					parent->parent()->setText(4, "Images");
					parent->parent()->setText(5, "TimingVal");
					parent->parent()->setText(6, "Avg");
					// colorize the newly added column to the parent of the parent.
					for (auto column = 1; column < parent->parent()->columnCount(); column++)
						parent->parent()->setTextColor(column, subItemTitleRowColor);
				}
			}
			else
			{
				// if the parent of this item is 2D or 3D, then add a new column to the parent
				// that contains the total number of children items, which are the JPG images.
				if (parent->text(0) == "2D" || parent->text(0) == "3D")
				{
					parent->setText(1, QString::number(parent->childCount()));
					parent->setTextColor(1, subItemValueRowColor);
				}
			}
		}

		parent = item;
	}
}

void XrayFileSystemWatcherTreeWidget::onClicked_ImageItems(QTreeWidgetItem *item, int column)
{
	auto it = dynamic_cast<XrayFileSystemWatcherTreeWidgetItem*>(item);
	if (!it) return;

	column = 0;	// currently we are only considering the first column.

	if (it->text(column).endsWith(".jpg", Qt::CaseInsensitive) || it->text(column).endsWith(".jpeg", Qt::CaseInsensitive))
	{
		emit onClicked_on2DItem({ it->getPath() });
		return;
	}

	if (it->text(column).endsWith(".pca", Qt::CaseInsensitive))
	{
		emit onClicked_onPcaFileItem(it->getPath());
		return;
	}

	if (item->parent())
	{
		for (auto i = 0; i < it->childCount(); i++)
		{
			const auto child = dynamic_cast<XrayFileSystemWatcherTreeWidgetItem*>(it->child(i));
			if (child)
			{
				if (child->text(column).endsWith(".pca", Qt::CaseInsensitive))
				{
					emit onClicked_onPcaFileItem(child->getPath());
					return;
				}
			}
		}
	}

	if (item->parent())
	{
		QStringList files;
		files.reserve(it->childCount());

		for (auto i = 0; i < it->childCount(); i++)
		{
			const auto child = dynamic_cast<XrayFileSystemWatcherTreeWidgetItem*>(it->child(i));
			if (child)
			{
				if (child->text(column).endsWith(".jpg", Qt::CaseInsensitive) || child->text(column).endsWith(".jpeg", Qt::CaseInsensitive))
					files.append(child->getPath());
			}
		}

		if (!files.empty())
		{
			emit onClicked_on2DItem(files);
			return;
		}
	}
}
void XrayFileSystemWatcherTreeWidget::onItemPressed(QTreeWidgetItem *item, int column)
{
	Q_UNUSED(item);
	Q_UNUSED(column);
	//onClicked_ImageItems(item, column);
}
void XrayFileSystemWatcherTreeWidget::onItemClicked(QTreeWidgetItem* item, int column)
{
	Q_UNUSED(item);
	Q_UNUSED(column);
	//onClicked_ImageItems(item, column);
}
void XrayFileSystemWatcherTreeWidget::onItemDoubleClicked(QTreeWidgetItem *item, int column)
{
	Q_UNUSED(item);
	Q_UNUSED(column);
	//onClicked_ImageItems(item, column);
}
void XrayFileSystemWatcherTreeWidget::onItemActivated(QTreeWidgetItem *item, int column)
{
	onClicked_ImageItems(item, column);
}
void XrayFileSystemWatcherTreeWidget::onItemEntered(QTreeWidgetItem *item, int column)
{
	Q_UNUSED(item);
	Q_UNUSED(column);
	//qDebug() << "Entered: " << item->text(0) << "    " << column;
}
void XrayFileSystemWatcherTreeWidget::searchAndSelectionItem(const QString& _text, int _column)
{
	auto item = searchItem(_column, _text);
	if (item)
	{
		item->setSelected(true);
		setItemSelected(item, true);
		setCurrentItem(item);
	}
}
void XrayFileSystemWatcherTreeWidget::showContextMenu(const QPoint& _point)
{
	if (!itemAt(_point))
		return;

	m_currentPoint = _point;
	p_contextMenu->exec(QCursor::pos());
}
void XrayFileSystemWatcherTreeWidget::createCustomMenu()
{
	p_contextMenu = new QMenu(this);

	auto open = new QAction(QIcon(":/paint/images/open_blue_icon.png"), QApplication::translate("XrayFileSystemWatcherTreeWidget", "Open in file explorer..."), this);
	connect(open, &QAction::triggered, this, [this]()
	{
		QDesktopServices::openUrl(QUrl::fromLocalFile(getItemAbsolutePath(itemAt(m_currentPoint), 0)));	// QUrl::fromLocalFile enables working with names with spaces. Otherwise if name has spaces, QUrl fails to open that path.
	});

	auto copy = new QAction(QIcon(":/paint/images/copy_blue_icon.png"), QApplication::translate("XrayFileSystemWatcherTreeWidget", "Copy path"), this);
	connect(copy, &QAction::triggered, this, [this]()
	{
		QApplication::clipboard()->setText(getItemAbsolutePath(itemAt(m_currentPoint), 0));
	});

	p_contextMenu->addAction(open);
	p_contextMenu->addSeparator();
	p_contextMenu->addAction(copy);
}
QString XrayFileSystemWatcherTreeWidget::getItemAbsolutePath(QTreeWidgetItem* _item, int _column)
{
	QString path;
	if (_item)
	{
		if (!_item->parent())
		{
			path = m_rootPath + _item->text(_column);
		}
		else if (_item->parent())
		{
			path = _item->text(_column);
			while (_item->parent())
			{
				_item = _item->parent();
				path.prepend(_item->text(_column) + "/");

			}

			path.prepend(m_rootPath);
		}
	}
	return path;
}
QTreeWidgetItem* XrayFileSystemWatcherTreeWidget::searchItem(int _column, const QString& _text, QTreeWidgetItemIterator::IteratorFlags _flags)
{
	if (_text.isEmpty())
		return nullptr;

	QTreeWidgetItemIterator it(this, _flags);
	while (*it)
	{
		if ((*it)->text(_column) == _text)
			return *it;
		++it;
	}

	return nullptr;
}
QTreeWidgetItem* XrayFileSystemWatcherTreeWidget::searchItem(QTreeWidgetItem* _parent, int _column, const QString& _text, QTreeWidgetItemIterator::IteratorFlags _flags)
{
	if (_text.isEmpty() || !_parent)
		return nullptr;

	QTreeWidgetItemIterator it(_parent, _flags);
	while (*it)
	{
		if ((*it)->text(_column) == _text)
			return *it;
		++it;
	}

	return nullptr;
}
QTreeWidgetItem* XrayFileSystemWatcherTreeWidget::searchChildItem(QTreeWidgetItem* _item, int _column, const QString& _text)
{
	for (auto i = 0; i < _item->childCount(); i++)
	{
		if (_item->child(i)->text(_column) == _text)
			return _item->child(i);
	}

	return nullptr;
}

void XrayFileSystemWatcherTreeWidget::saveSettings(QSettings& _settings)
{
	_settings.beginGroup("FileSystemWatcherTree");
	_settings.setValue("root_path", m_rootPath);
	_settings.setValue("update_interval", m_delay);
	_settings.setValue("paths", m_paths);
	_settings.endGroup();
}
void XrayFileSystemWatcherTreeWidget::restoreSettings(QSettings& _settings)
{
	_settings.beginGroup("FileSystemWatcherTree");
	m_rootPath = _settings.value("root_path", "Q:\\").toString();
	m_delay = _settings.value("update_interval", 3600).toInt();
	m_paths = _settings.value("paths").toStringList();
	_settings.endGroup();
}

QFrame* createTimeSheet(const QString _startTime, const QString _endTime)
{
	auto widget = new QFrame;
	widget->setFrameShape(QFrame::Shape::StyledPanel);
	widget->setLineWidth(1);
	//widget->setStyleSheet("QWidget { padding: 10px; outline: 1px solid rgb(21, 142, 21);}");
	auto layout = new QGridLayout;
	layout->addWidget(new QLabel(QApplication::translate("XrayFileSystemWatcherTreeWidget", "Start time: ")), 0, 0);
	layout->addWidget(new QLabel(_startTime), 0, 1);

	layout->addWidget(new QLabel(QApplication::translate("XrayFileSystemWatcherTreeWidget", "End time: ")), 1, 0);
	layout->addWidget(new QLabel(_endTime), 1, 1);
	widget->setLayout(layout);
	return widget;
}
//while (item->childCount())
//{
//	item->removeChild(item->child(0));
//}