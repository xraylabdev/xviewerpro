/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/22
** filename: 	XrayMachineLogFileDictionary.cpp
** file base:	XrayMachineLogFileDictionary
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that reads the log files of XRAY-LAB CT machines.
****************************************************************************/

#include "XrayMachineLogFileDictionary.h"
#include "XrayLogger.h"
#include "XrayQtUtil.h"
#include "XrayThreadedFileSystemWatcher.h"
#include <QCollator>
#include <QDateTime>
#include <QTime>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayMachineLogFileDictionary::XrayMachineLogFileDictionary()
{
	m_syncmove_reader.setNumberOfLines(10);
	m_syncmove_reader.setCharactersPerLine(256);

	m_calibration_reader.setNumberOfLines(1);
	m_calibration_reader.setCharactersPerLine(256);

	m_dtxae_reader.setNumberOfLines(1000);
	m_dtxae_reader.setCharactersPerLine(256);

	m_xs_control_1886_reader.setNumberOfLines(-1);	// -1 to read all lines in the file.
	m_xs_control_1886_reader.setCharactersPerLine(256);

	m_xs_xray_1886_reader.setNumberOfLines(100);
	m_xs_xray_1886_reader.setCharactersPerLine(256);
}

void XrayMachineLogFileDictionary::read()
{
	m_map.clear();
	parse_syncmove();
	parse_calibration();
	parse_dtxa();
	parse_xs_control_1886();
	parse_xs_xray_1886();
	parse_xs_control_0000();
	parse_ct_data_dir();
}

void XrayMachineLogFileDictionary::set_syncmove_file(const QString& _fileName)
{
	m_syncmove_reader.setFileName(_fileName);
}
void XrayMachineLogFileDictionary::set_calibration_file(const QString& _fileName)
{
	m_calibration_reader.setFileName(_fileName);
}
void XrayMachineLogFileDictionary::set_dtxa_file(const QString& _fileName)
{
	m_dtxae_reader.setFileName(_fileName);
}
void XrayMachineLogFileDictionary::set_xs_control_1886_file(const QString& _fileName)
{
	m_xs_control_1886_reader.setFileName(_fileName);
}
void XrayMachineLogFileDictionary::set_xs_xray_1886_file(const QString& _fileName)
{
	m_xs_xray_1886_reader.setFileName(_fileName);
}
void XrayMachineLogFileDictionary::set_xs_control_0000_file(const QString& _fileName)
{
	m_xs_control_0000_reader.setFileName(_fileName);
}
void XrayMachineLogFileDictionary::set_ct_data_dir_path(const QString& _path)
{
	m_ct_data_dir_path = _path;
}

void XrayMachineLogFileDictionary::parse_syncmove()
{
	auto lines = m_syncmove_reader.getLines();
	if (lines.empty())
		return;

	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains("Computer:"))
		{
			auto words = lines[i].remove(' ').split("Computer:");
			if (words.size() > 1)
			{
				auto comp = words[1].split("User:");
				if (comp.size() > 1)
				{
					m_map.insert("sm_computer", comp[0]);
					m_map.insert("sm_user", comp[1]);
					break;
				}
			}
		}
	}
}

void XrayMachineLogFileDictionary::parse_calibration()
{
	auto lines = m_calibration_reader.getLines();
	if (lines.empty())
		return;

	for (int i = lines.size() - 1; i >= 0; i--)
	{
		auto words = lines[i].split(QRegExp("\\s+"));
		if (words.size() > 7)
		{
			auto d = words[0].split('/');
			m_map.insert("cal_date", QDate(d[2].toInt() + 2000, d[0].toInt(), d[1].toInt()));

			auto t = words[1].split(':');
			m_map.insert("cal_time", QTime(t[0].toInt(), t[1].toInt(), t[2].toInt()));

			m_map.insert("cal_actual", words[2]);
			m_map.insert("cal_nominal", words[3]);
			m_map.insert("cal_fod", words[4]);
			m_map.insert("cal_fod_delta", words[5]);
			m_map.insert("cal_fdd", words[6]);
			m_map.insert("cal_user", words[7]);
			break;
		}
	}
}

void XrayMachineLogFileDictionary::parse_dtxa()
{
	auto lines = m_dtxae_reader.getLines();
	if (lines.empty())
		return;

	// pca file location
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains("Set pca path | "))
		{
			auto words = lines[i].split("Set pca path | ");
			if (words.size() > 1)
			{
				m_map.insert("dt_pca_location", words[1]);
				parse_pca_file(words[1]);
				break;
			}
		}
	}

	// mean, standard deviation, median
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains("Acquire offset | OffsetInfo: mean:"))
		{
			auto words = lines[i].split("Acquire offset | OffsetInfo: mean:");
			if (words.size() > 1)
			{
				auto comp = words[1].remove(',').split(":");
				if (comp.size() > 2)
				{
					auto mean = comp[0].split(' ');
					if (!mean.empty())
						m_map.insert("dt_mean", mean[1]);

					auto sd = comp[1].split(' ');
					if (!sd.empty())
						m_map.insert("dt_standard_deviation", sd[1]);

					auto median = comp[2].split(' ');
					if (!median.empty())
						m_map.insert("dt_median", median[1]);

					break;
				}
			}
		}
	}

	// GainPoints
	auto gainCount = 0;
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains("GainPoints: "))
		{
			auto words = lines[i].split("GainPoints: ");
			if (words.size() > 1)
			{
				m_map.insert(QString("dt_gain_points_%1").arg(gainCount), words[1]);
				gainCount++;
				if (gainCount > 2)	// how many values do we need for the gain points. We need 3.
					break;
			}
		}
	}

	// Start of calibration
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains(" I: Acquire calibration image"))
		{
			auto words = lines[i].split(" I: Acquire calibration image");
			if (words.size() > 1)
			{
				auto dateTime = words[0].split(' ');
				if (dateTime.size() > 1)
				{
					auto date = dateTime[0].split('/');	// format: 03/15/2019
					if (!date.empty())
					{
						m_map.insert("dt_calibration_start_date", QDate(date[2].toInt(), date[0].toInt(), date[1].toInt()));
					}

					auto time = dateTime[1].split('_');	// format: 16:16:05_323
					if (!time.empty())
					{
						auto t = time[0].split(':');
						m_map.insert("dt_calibration_start_time", QTime(t[0].toInt(), t[1].toInt(), t[2].toInt()));
					}

					break;
				}
			}
		}
	}

	// success of calibration
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains(" I: MGain PreProcess"))
		{
			auto words = lines[i].split(" I: MGain PreProcess");
			if (words.size() > 1)
			{
				auto dateTime = words[0].split(' ');
				if (dateTime.size() > 1)
				{
					auto date = dateTime[0].split('/');	// format: 03/15/2019
					if (!date.empty())
					{
						m_map.insert("dt_calibration_success_date", QDate(date[2].toInt(), date[0].toInt(), date[1].toInt()));
					}

					auto time = dateTime[1].split('_');	// format: 16:16:05_323
					if (!time.empty())
					{
						auto t = time[0].split(':');
						m_map.insert("dt_calibration_success_time", QTime(t[0].toInt(), t[1].toInt(), t[2].toInt()));
					}

					if (lines[i].contains("OK"))
						m_map.insert("dt_calibration_success_status", true);
					else
						m_map.insert("dt_calibration_success_status", false);

					break;
				}
			}
		}
	}

	// starting scan time
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains(" I: CT mode | Started"))
		{
			auto words = lines[i].split(" I: CT mode | Started");
			if (words.size() > 1)
			{
				auto dateTime = words[0].split(' ');
				if (dateTime.size() > 1)
				{
					auto date = dateTime[0].split('/');	// format: 03/15/2019
					if (!date.empty())
					{
						m_map.insert("dt_starting_scan_date", QDate(date[2].toInt(), date[0].toInt(), date[1].toInt()));
					}

					auto time = dateTime[1].split('_');	// format: 16:16:05_323
					if (!time.empty())
					{
						auto t = time[0].split(':');
						m_map.insert("dt_starting_scan_time", QTime(t[0].toInt(), t[1].toInt(), t[2].toInt()));
					}

					break;
				}
			}
		}
	}
}

void XrayMachineLogFileDictionary::parse_xs_control_1886()
{
	auto lines = m_xs_control_1886_reader.getLines();
	if (lines.empty())
		return;

	// system id - start from top, usually the first line contains the system id.
	for (int i = 0; i < lines.size(); i++)
	{
		if (lines[i].contains("INFO : System ID : "))
		{
			auto words = lines[i].split("INFO : System ID : ");
			if (words.size() > 1)
			{
				auto id = words[1].remove(' ');
				if (!id.isEmpty())
				{
					m_map.insert("xsc_system_id", id);
					break;
				}
			}
		}
	}

	// voltage and current
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains("INFO : X-Ray runup to "))
		{
			auto words = lines[i].split("INFO : X-Ray runup to ");
			if (words.size() > 1)
			{
				auto volts = words[1].split(" @ ");
				if (volts.size() > 1)
				{
					m_map.insert("xsc_voltage", volts[0].remove("kV"));
					m_map.insert("xsc_current", volts[1].remove("�A"));
					break;
				}
			}
		}
	}

	// On-Time source in seconds
	auto onTimeSource = 0;
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains("INFO : X-Ray Off after "))
		{
			auto words = lines[i].split("INFO : X-Ray Off after ");
			if (words.size() > 1)
			{
				auto sec = words[1].split(' ');
				if (sec.size() > 0)
				{
					onTimeSource += sec[0].toInt();
				}
			}
		}
	}
	m_map.insert("xsc_on_time_source", QString::number(onTimeSource));

	// Run up time
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains(" || INFO : X-Ray stable "))
		{
			auto words = lines[i].split(" || INFO : X-Ray stable ");
			if (words.size() > 1)
			{
				auto dateTime = words[0].split(' ');
				if (dateTime.size() > 1)
				{
					auto date = dateTime[0].split('.');
					if (!date.empty())
					{
						m_map.insert("xsc_runup_date", QDate(date[2].toInt(), date[1].toInt(), date[0].toInt()));
					}

					auto time = dateTime[1].split(':');
					if (!time.empty())
					{
						m_map.insert("xsc_runup_time", QTime(time[0].toInt(), time[1].toInt(), time[2].toInt()));
					}

					break;
				}
			}
		}
	}

	// Filament adjustments
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains("Filamentadjust : "))
		{
			auto words = lines[i].split("Filamentadjust : ");
			if (words.size() > 1)
			{
				auto comp = words[1].remove(',').split(":");
				if (comp.size() > 2)
				{
					auto filament = comp[0].split("% ");
					if (!filament.empty())
						m_map.insert("xsc_filament_adjust", filament[0]);

					auto ep = comp[1].split("mV ");
					if (!ep.empty())
						m_map.insert("xsc_filament_adjust_ep", ep[0]);

					auto moni = comp[2].remove("mV");
					m_map.insert("xsc_filament_adjust_moni", moni);

					break;
				}
			}
		}
	}

	// Filament resistance : Value in mOhm at voltage
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains("Filament cold "))
		{
			auto words = lines[i].split("Filament cold ");
			if (words.size() > 1)
			{
				auto comp = words[1].split(' ');
				if (comp.size() > 3)
				{
					auto filament = comp[0];
					if (!filament.isEmpty())
						m_map.insert("xsc_filament_resistance", filament);

					auto volts = comp[3].remove("mV)");
					if (!volts.isEmpty())
						m_map.insert("xsc_filament_resistance_voltage", volts);

					break;
				}
			}
		}
	}

	// Gridload : in kOhm at voltage
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains("Gridload "))
		{
			auto words = lines[i].split("Gridload ");
			if (words.size() > 1)
			{
				auto comp = words[1].split(' ');
				if (comp.size() > 3)
				{
					auto filament = comp[0];
					if (!filament.isEmpty())
						m_map.insert("xsc_gridload_resistance", filament);

					auto volts = comp[3].remove("mV)");
					if (!volts.isEmpty())
						m_map.insert("xsc_gridload_voltage", volts);

					break;
				}
			}
		}
	}

	// Time of Filament Adjustment started
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains(" || INFO : Filament Adjustment started"))
		{
			auto words = lines[i].split(" || INFO : Filament Adjustment started");
			if (words.size() > 1)
			{
				auto dateTime = words[0].split(' ');
				if (dateTime.size() > 1)
				{
					auto date = dateTime[0].split('.');
					if (!date.empty())
					{
						m_map.insert("xsc_filament_adjust_started_date", QDate(date[2].toInt(), date[1].toInt(), date[0].toInt()));
					}

					auto time = dateTime[1].split(':');
					if (!time.empty())
					{
						m_map.insert("xsc_filament_adjust_started_time", QTime(time[0].toInt(), time[1].toInt(), time[2].toInt()));
					}

					break;
				}
			}
		}
	}

	// Time of Filament Adjustment finished
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains(" || INFO : Filament Adjustment finished"))
		{
			auto words = lines[i].split(" || INFO : Filament Adjustment finished");
			if (words.size() > 1)
			{
				auto dateTime = words[0].split(' ');
				if (dateTime.size() > 1)
				{
					auto date = dateTime[0].split('.');
					if (!date.empty())
					{
						m_map.insert("xsc_filament_adjust_finished_date", QDate(date[2].toInt(), date[1].toInt(), date[0].toInt()));
					}

					auto time = dateTime[1].split(':');
					if (!time.empty())
					{
						m_map.insert("xsc_filament_adjust_finished_time", QTime(time[0].toInt(), time[1].toInt(), time[2].toInt()));
					}

					break;
				}
			}
		}
	}
}

void XrayMachineLogFileDictionary::parse_xs_xray_1886()
{
	auto lines = m_xs_xray_1886_reader.getLines();
	if (lines.empty())
		return;

	// voltage and current
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains("INFO : X-Ray runup to "))
		{
			auto words = lines[i].split("INFO : X-Ray runup to ");
			if (words.size() > 1)
			{
				auto volts = words[1].split(" @ ");
				if (volts.size() > 1)
				{
					m_map.insert("xsx_voltage", volts[0].remove("kV"));
					m_map.insert("xsx_current", volts[1].remove("�A"));
					break;
				}
			}
		}
	}

	// On-Time source in seconds
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains("INFO : X-Ray Off after "))
		{
			auto words = lines[i].split("INFO : X-Ray Off after ");
			if (words.size() > 1)
			{
				auto sec = words[1].split(' ');
				if (sec.size() > 0)
				{
					m_map.insert("xsx_on_time_source", sec[0]);
					break;
				}
			}
		}
	}

	// Run up time
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains(" || INFO : X-Ray stable "))
		{
			auto words = lines[i].split(" || INFO : X-Ray stable ");
			if (words.size() > 1)
			{
				auto dateTime = words[0].split(' ');
				if (dateTime.size() > 1)
				{
					auto date = dateTime[0].split('.');
					auto time = dateTime[1].split(':');
					if (time.size() > 2 && date.size() > 2)
					{
						QDate d(date[2].toInt(), date[1].toInt(), date[0].toInt());
						QTime t(time[0].toInt(), time[1].toInt(), time[2].toInt());

						auto startedTime = QDateTime(d, t);
						auto currentTime = QDateTime::currentDateTime();
						auto uptime = qAbs(startedTime.time().secsTo(currentTime.time())) + qAbs(startedTime.date().daysTo(currentTime.date())) * 60 * 60 * 24; // seconds + days to seconds. 1 days = 86400 sec.

						m_map.insert("xsx_runup_date", d);
						m_map.insert("xsx_runup_time", t);
						m_map.insert("xsx_runup_up_time_sec", QString::number(uptime));
					}

					break;
				}
			}
		}
	}
}

void XrayMachineLogFileDictionary::parse_xs_control_0000()
{
	auto lines = m_xs_control_0000_reader.getLines();
	if (lines.empty())
		return;

	// voltage and current
	for (int i = lines.size() - 1; i >= 0; i--)
	{
		if (lines[i].contains(" || INFO : xs-control started"))
		{
			auto words = lines[i].split(" || INFO : xs-control started");
			if (words.size() > 1)
			{
				auto dateTime = words[0].split(' ');
				if (dateTime.size() > 1)
				{
					auto date = dateTime[0].split('.');
					auto time = dateTime[1].split(':');
					if (time.size() > 2 && date.size() > 2)
					{
						QDate d(date[2].toInt(), date[1].toInt(), date[0].toInt());
						QTime t(time[0].toInt(), time[1].toInt(), time[2].toInt());

						auto currentTime = QDateTime::currentDateTime();
						auto startedTime = QDateTime(d, t);
						auto uptime = qAbs(startedTime.time().secsTo(currentTime.time())) + qAbs(startedTime.date().daysTo(currentTime.date())) * 60 * 60 * 24; // seconds + days to seconds. 1 days = 86400 sec.
						
						m_map.insert("xsc0_started_date", d);
						m_map.insert("xsc0_started_time", t);
						m_map.insert("xsc0_up_time_sec", QString::number(uptime));
					}

					break;
				}
			}
		}
	}
}

void XrayMachineLogFileDictionary::parse_pca_file(const QString& _fileName)
{
	if (!QFile(_fileName).exists())
	{
		xAppError("This PCA file {} doesn't exists.", _fileName.toStdString());
		return;
	}

	QSettings settings(_fileName, QSettings::IniFormat);
	parse_pca_file(settings);
}
void XrayMachineLogFileDictionary::parse_pca_file(QSettings& settings)
{
	m_map.insert("pca_fdd", settings.value("Geometry/FDD", "n/a").toString());
	m_map.insert("pca_fod", settings.value("Geometry/FOD", "n/a").toString());
	m_map.insert("pca_magnification", settings.value("Geometry/Magnification", "n/a").toString());
	m_map.insert("pca_voxel_size_x", settings.value("Geometry/VoxelSizeX", "n/a").toString());
	m_map.insert("pca_number_images", settings.value("CT/NumberImages", "n/a").toString());
	m_map.insert("pca_rotation_sector", settings.value("CT/RotationSector", "n/a").toString());
	m_map.insert("pca_active", settings.value("Multiscan/Active", "n/a").toString());
	m_map.insert("pca_timing_val", settings.value("Detector/TimingVal", "n/a").toString());
	m_map.insert("pca_avg", settings.value("Detector/Avg", "n/a").toString());
	m_map.insert("pca_skip", settings.value("Detector/Skip", "n/a").toString());
	m_map.insert("pca_binning", settings.value("Detector/Binning", "n/a").toString());
	m_map.insert("pca_name", settings.value("Xray/Name", "n/a").toString());
	m_map.insert("pca_id", settings.value("Xray/ID", "n/a").toString());
	m_map.insert("pca_voltage", settings.value("Xray/Voltage", "n/a").toString());
	m_map.insert("pca_current", settings.value("Xray/Current", "n/a").toString());
	m_map.insert("pca_mode", settings.value("Xray/Mode", "n/a").toString());
	m_map.insert("pca_filter", settings.value("Xray/Filter", "n/a").toString());
	m_map.insert("pca_x_sample", settings.value("Axis/XSample", "n/a").toString());
	m_map.insert("pca_y_sample", settings.value("Axis/YSample", "n/a").toString());
	m_map.insert("pca_z_sample", settings.value("Axis/ZSample", "n/a").toString());
	m_map.insert("pca_bhc_param", settings.value("BHC_Values/BHC_Param", "n/a").toString());
}

void XrayMachineLogFileDictionary::parse_ct_data_dir()
{
	if (!QFile::exists(m_ct_data_dir_path))
		return;

	QDir dir(m_ct_data_dir_path);
	dir.setFilter(QDir::Files | QDir::NoSymLinks);
	dir.setSorting(QDir::NoSort);  // will sort manually with std::sort

	auto entryList = dir.entryList();
	if (entryList.empty())
		return;

	QCollator collator;
	collator.setNumericMode(true);

	std::sort(entryList.begin(), entryList.end(),
		[&collator](const QString &file1, const QString &file2)
	{
		return collator.compare(file1, file2) < 0;
	});

	// extract file number from the file name.
	QStringList files;
	for (const auto& i : entryList)
	{
		auto s = QFileInfo(i).baseName().split("_0");
		if (s.size() > 1)
			files.push_back(s[1]);
	}

	m_map.insert("ct_data_project_total_images", QString::number(files.size()));

	if (!files.empty())
	{
		auto fm = QFileInfo(m_ct_data_dir_path + "/" + entryList.front()).lastModified();
		auto lm = QFileInfo(m_ct_data_dir_path + "/" + entryList.back()).lastModified();
		auto diff = fm.time().msecsTo(lm.time());	// in milliseconds.

		m_map.insert("ct_data_project_first_image_modified", fm);
		m_map.insert("ct_data_project_last_image_modified", lm);
		m_map.insert("ct_data_project_first_last_image_diff", QString::number(qAbs(diff)));
		m_map.insert("ct_data_project_last_image_number", files.back());
	}
}