/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/06
** filename: 	XrayFileSystemWatcher.cpp
** file base:	XrayFileSystemWatcher
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a file system watcher with a time
				interval feature.
****************************************************************************/

#include "XrayThreadedFileSystemWatcher.h"
#include "XrayFileSystemWatcherTreeWidget.h"
#include "XrayQFileUtil.h"

#include <QApplication>
#include <QDirIterator>
#include <QDateTime>
#include <QGridLayout>
#include <QSettings>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayThreadedFileSystemWatcher::XrayThreadedFileSystemWatcher(QObject* parent, const QString& _rootPath, int _updateInterval) :
	XrayQThread(parent)
{
	connect(&m_watcher, &QFileSystemWatcher::directoryChanged, this, &XrayThreadedFileSystemWatcher::directoryChanged);
	connect(&m_watcher, &QFileSystemWatcher::fileChanged, this, &XrayThreadedFileSystemWatcher::fileChanged);

	m_dir.setPath(_rootPath);
	m_dir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot | QDir::Modified);
	m_dir.setNameFilters({ "*" });

	setDelay(_updateInterval);
}
XrayThreadedFileSystemWatcher::~XrayThreadedFileSystemWatcher()
{
	if (isRunning())
	{
		stop();
		quit();
		terminate();
	}
}

QStringList XrayThreadedFileSystemWatcher::getFiles()
{
	return m_watcher.files();
}
void XrayThreadedFileSystemWatcher::setPaths(const QStringList& _paths)
{
	if (_paths.empty())
		return;

	// remove all previous directories and add the new paths.
	if (!m_watcher.directories().isEmpty())
		m_watcher.removePaths(m_watcher.directories());
	m_watcher.addPaths(_paths);
}
QStringList XrayThreadedFileSystemWatcher::getDirectories()
{
	return m_watcher.directories();
}

void XrayThreadedFileSystemWatcher::run()
{
	m_isstop = false;

	while (true)
	{
		// stop this thread
		m_stopmutex.lock();
		if (m_isstop)
		{
			m_isstop = false;
			m_stopmutex.unlock();
			break;
		}
		m_stopmutex.unlock();

		// build a tree structure and add specific directories to the watcher.
		addPaths();

		// notify to GUI to update the tree widget.
		emit onFinished();

		// pause the thread after building a tree structure and notifying to GUI.
		// The purpose to pause the thread here, is that if there a change in the watched folders and files,
		// only then thread should run otherwise there is no need to rebuild the tree structure.
		// directoryChanged() and fileChanged() function will resume the thread in case of any change.
		pause();

		// pause this thread
		m_pausemutex.lock();
		if (m_ispause)
			m_pausecond.wait(&m_pausemutex);	// here the thread will stop to execute until someone calls resume.
		m_pausemutex.unlock();

		// even the thread is resumed, still it should sleep for the specified interval.
		msleep(m_delay);
	}
}

void XrayThreadedFileSystemWatcher::addPaths()
{
	const auto path = m_dir.absolutePath();
	if (!QFile::exists(path))
		return;

	emit onStart();

	// extract directories with only numerical character name like "343523".
	auto projectDirs = XrayQFileUtil::getSubNumericalDirs(path);	// 23456, 23456, 23456, 

	m_paths.clear();
	m_paths.reserve(projectDirs.size() * 100);
	m_paths += projectDirs;

	QStringList paths;
	paths.reserve(projectDirs.size() * 100);
	paths.append(path);
	paths += projectDirs;

	auto n = 0;

	// okay final remarks after testing many ways to get the maximum speed.
	// QDirIterator::Subdirectories is really slow, on the other hand QDirIterator::NoIteratorFlags
	// gives fast results, so i should stick with it. 
	// Only directories from 2D, 3D, and CT are unknown, other paths are fixes such as "/Bilder/2D", "/Bilder/3D" etc.,
	// so use the hard-codded fixed paths to get the expected directories and files.

	for (const auto& i : projectDirs)
	{
		auto CT = i + "/CT";
		if (QFile::exists(CT))
		{
			paths.push_back(CT);

			auto dirs = XrayQFileUtil::getSubDirs(CT, {}, QDir::Dirs | QDir::NoDotAndDotDot, QDirIterator::NoIteratorFlags); // 01_CT_10, 01_CT_11, 01_CT_12, ...
			paths += dirs;
			m_paths += dirs;

			for (const auto& j : dirs)
			{
				auto pcaFiles = XrayQFileUtil::getSubDirs(j, { "*.pca" }, QDir::Files, QDirIterator::NoIteratorFlags); // *.pca
				m_paths += pcaFiles;
			}
		}

		auto Bilder = i + "/Bilder";
		if (QFile::exists(Bilder))
		{
			paths.push_back(Bilder);

			auto B2D = i + "/Bilder/2D";
			if (QFile::exists(B2D))
			{
				paths.push_back(B2D);

				auto dirs = XrayQFileUtil::getSubDirFiles(B2D, { "*.jpg", "*.jpeg" }, QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot, QDirIterator::NoIteratorFlags); 	// 398485, *.jpg

				for (const auto& j : dirs)
				{
					QFileInfo fi(j);
					if (fi.isDir())
					{
						paths.push_back(j);

						auto jpgFiles = XrayQFileUtil::getSubDirs(j, { "*.jpg", "*.jpeg" }, QDir::Files, QDirIterator::NoIteratorFlags); 	// *.jpg
						m_paths += jpgFiles;
					}
					else
					{
						auto ext = fi.suffix();
						if (ext == "jpg" || ext == "jpeg")
							m_paths.push_back(j);
					}
				}
			}

			auto B3D = i + "/Bilder/3D";
			if (QFile::exists(B3D))
			{
				paths.push_back(B3D);

				auto dirs = XrayQFileUtil::getSubDirFiles(B3D, { "*.jpg", "*.jpeg" }, QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot, QDirIterator::NoIteratorFlags); 	// 293749, *.jpg

				for (const auto& j : dirs)
				{
					QFileInfo fi(j);
					if (fi.isDir())
					{
						paths.push_back(j);

						auto jpgFiles = XrayQFileUtil::getSubDirs(j, { "*.jpg", "*.jpeg" }, QDir::Files, QDirIterator::NoIteratorFlags); 	// *.jpg
						m_paths += jpgFiles;
					}
					else
					{
						auto ext = fi.suffix();
						if (ext == "jpg" || ext == "jpeg")
							m_paths.push_back(j);
					}
				}
			}
		}

		// emit a progress bar signal with value.
		if (n % 2 == 0)
			emit onProgress(n / static_cast<double>(projectDirs.size()));
		n++;
		// end of progress bar.
	}

	setPaths(paths);
}

void XrayThreadedFileSystemWatcher::setRootPath(const QString& _path)
{
	if (!m_watcher.addPath(_path))
		qWarning() << "Couldn't add the specified path: " << _path;
	else
		m_dir.setPath(_path);
}
void XrayThreadedFileSystemWatcher::directoryChanged(const QString& _path)
{
	Q_UNUSED(_path);
	if (!isRunning())
		return;

	resume();
}
void XrayThreadedFileSystemWatcher::fileChanged(const QString& _path)
{
	Q_UNUSED(_path);
	if (!isRunning())
		return;

	resume();
}
void XrayThreadedFileSystemWatcher::clear()
{
	auto dirs = m_watcher.directories();
	if (!dirs.isEmpty())
		m_watcher.removePaths(dirs);
}
void XrayThreadedFileSystemWatcher::remove(const QString& _path)
{
	for (const auto& dir : m_watcher.directories())
		if (dir.startsWith(_path, Qt::CaseInsensitive))
			m_watcher.removePath(dir);
}


void XrayThreadedFileSystemWatcher::setTreePaths(const QStringList& _paths)
{
	m_paths = _paths;
}
QStringList XrayThreadedFileSystemWatcher::getTreePaths()
{
	return m_paths;
}

void XrayThreadedFileSystemWatcher::saveSettings(QSettings& _settings)
{
	_settings.beginGroup("FileSystemWatcher");
	_settings.setValue("Paths/Watched", m_watcher.directories());
	_settings.setValue("Paths/Projects", m_paths);
	_settings.endGroup();
}
void XrayThreadedFileSystemWatcher::restoreSettings(QSettings& _settings)
{
	_settings.beginGroup("FileSystemWatcher");
	setPaths(_settings.value("Paths/Watched").toStringList());
	setTreePaths(_settings.value("Paths/Projects").toStringList());
	_settings.endGroup();
}