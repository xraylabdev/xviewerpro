/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/06
** filename: 	XrayDeleteProjectFilesWidget.cpp
** file base:	XrayDeleteProjectFilesWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the main window of the application.
****************************************************************************/

#include "XrayDeleteProjectFilesWidget.h"
#include "ui_XrayDeleteProjectFilesWidget.h"

#include "XrayGlobal.h"
#include "XrayLicenseGlobals.h"
#include "XrayQFileUtil.h"

#include <QThread>
#include <QTimer>
#include <QSortFilterProxyModel>
#include <QDesktopServices>
#include <QMenu>
#include <QDialog>
#include <QDialogButtonBox>
#include <QListView>
#include <QStringListModel>
#include <QHeaderView>
#include <QFileDialog>
#include <QCollator>
#include <QDir>
#include <QDirIterator>
#include <QMessageBox>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

static QStringList collectScannedFiles(const std::vector<XraySysFile>& files, bool checkLastDigit)
{
	if (files.empty())
		return {};

	QStringList scannedFiles;
	scannedFiles.reserve(static_cast<int>(files.size()));

	for (const auto& f : files)
	{
		if (checkLastDigit)
		{
			// check if the last character of the file name (stem) is a digit, we are only interested in those files.
			auto s = QString::fromStdString(f.getStem());
			if (s.back().isDigit())
				scannedFiles.push_back(QString::fromStdString(f.getName()));
		}
		else
		{
			scannedFiles.push_back(QString::fromStdString(f.getName()));
		}
	}

	return scannedFiles;
}

static QStringList collectSortedFiles(const std::vector<XraySysFile>& files, bool checkLastDigit, int skipFromStart, int skipFromEnd)
{
	auto scannedFiles = collectScannedFiles(files, checkLastDigit);
	if (scannedFiles.empty())
		return {};

	QStringList sortedFiles;
	sortedFiles.reserve(scannedFiles.size() - skipFromStart - skipFromEnd);

	// how many files to be skipped from the start and end of the list.
	// total files must be more than skipped files.
	if (scannedFiles.size() > (skipFromStart + skipFromEnd))
	{
		// sort numerically by considering the last digits of the string
		if (checkLastDigit)
		{
			QCollator collator;
			collator.setNumericMode(true);
			std::sort(scannedFiles.begin(), scannedFiles.end(), collator);
		}

		for (auto f = skipFromStart; f < (scannedFiles.size() - skipFromEnd); f++)
		{
			// file that need to be deleted
			sortedFiles.push_back(scannedFiles.at(f));
			//qDebug() << "File..." << scannedFiles.at(f) << "\n";
		}
	}

	return sortedFiles;
}

static std::vector<std::string> getFileNameFilters(const QString& text)
{
	std::vector<std::string> fileNameFilters;
	if (text.isEmpty())
		return fileNameFilters;

	auto list = text.split(' ');

	fileNameFilters.reserve(list.size());
	for (const auto& s : list)
		fileNameFilters.emplace_back(s.toStdString());
	return fileNameFilters;
}

XrayProjectCleanerScanWorker::XrayProjectCleanerScanWorker(const QStringList& projDir, const QString& subDir, const QString& extFilters, const QString& containFilters, bool isNumericSort, int skipStart, int skipEnd, QObject *parent) :
	QObject(parent),
	m_projectDir(projDir),
	m_subDir(subDir),
	m_extFilters(extFilters),
	m_containFilters(containFilters),
	m_isNumericSort(isNumericSort),
	m_skipStart(skipStart),
	m_skipEnd(skipEnd)
{
	qRegisterMetaType<QVector< QVector<QPair<QString, QStringList> > > >("QVector< QVector<QPair<QString, QStringList> > >");
	qRegisterMetaType<QVector<QVector<QString> > >("QVector<QVector<QString> >");
	qRegisterMetaType<std::size_t>("std::size_t");
}

void XrayProjectCleanerScanWorker::scan()
{
	if (m_projectDir.empty())
		return;

	QVector<QVector<QString> > tableRows;
	tableRows.reserve(m_projectDir.size());

	QVector< QVector<QPair<QString, QStringList> > > scannedDir;
	scannedDir.reserve(m_projectDir.size());

	std::size_t totalFiles = 0;
	std::size_t totalSize = 0;

	emit onStart();
	auto n = 0;
	emit onProgress(n);

	for (const auto& p : m_projectDir)
	{
		QVector<QPair<QString, QStringList> > scannedFiles;

		auto cleanedDir = p + '/' + m_subDir + '/';	// Q:/25746/CT/
		if (QDir().exists(cleanedDir))
		{
			XrayFileSystemStructure fileSys(getFileNameFilters(m_extFilters), getFileNameFilters(m_containFilters));
			fileSys.setRoot(cleanedDir.toStdString());
			fileSys.scan();

			auto& contents = fileSys.getContents();

			scannedFiles.reserve(static_cast<int>(contents.size()));

			std::size_t nfiles = 0;
			std::size_t filesSize = 0;

			for (const auto& i : contents)
			{
				if (auto dir = dynamic_cast<XraySysDirectory*>(i))
				{
					auto sortedFiles = collectSortedFiles(dir->getFiles(), m_isNumericSort, m_skipStart, m_skipEnd);
					scannedFiles.push_back({ QString::fromStdString(dir->getName()), sortedFiles });
					totalFiles += sortedFiles.size();
					totalSize += dir->getSize();

					nfiles += sortedFiles.size();
					filesSize += dir->getSize();
				}
			}

			tableRows.push_back({ p.split('/').back() + '/' + m_subDir + '/', QString::number(nfiles), XrayQFileUtil::getHumanReadableSize(filesSize) });

			if (n % 2 == 0)
			{
				emit onProgress(n / static_cast<double>(m_projectDir.size())); // emit a progress bar signal with value.
				qApp->processEvents();
			}
			n++;
		}
		else
		{
			tableRows.push_back({ p.split('/').back(), "", "" });
		}

		scannedDir.push_back(scannedFiles);
	}

	emit result(tableRows, scannedDir, totalFiles, totalSize);
}

static QVector<QPair<QString, QStringList> > getScannedFiles(const QString& dirName, const QVector<QVector<QString> >& tableRows, const QVector< QVector<QPair<QString, QStringList> > >& scannedDir)
{
	// The array size of tableRows and scannedDir is always the same.
	for (auto i = 0; i < tableRows.size(); i++)
	{
		// look for the first column text if matches then returns the directory structure of that index.
		if (tableRows[i][0] == dirName)
			return scannedDir[i];
	}

	return {};
}

XrayProjectCleanerScanController::XrayProjectCleanerScanController(const QStringList& projDir, const QString& subDir, const QString& extFilters, const QString& containFilters, bool isNumericSort, int skipStart, int skipEnd, int _updateInterval, QObject *parent) :
	QObject(parent),
	p_thread(new QThread(this)),
	p_timer(new QTimer)
{
	p_timer->setInterval(_updateInterval);
	p_timer->moveToThread(p_thread);

	auto worker = new XrayProjectCleanerScanWorker(projDir, subDir, extFilters, containFilters, isNumericSort, skipStart, skipEnd);
	worker->moveToThread(p_thread);
	connect(p_thread, &QThread::started, p_timer, static_cast<void (QTimer::*)()>(&QTimer::start));
	connect(p_thread, &QThread::started, worker, &XrayProjectCleanerScanWorker::scan);
	connect(p_timer, &QTimer::timeout, worker, &XrayProjectCleanerScanWorker::scan);

	connect(worker, &XrayProjectCleanerScanWorker::onStart, this, &XrayProjectCleanerScanController::onStart);
	connect(worker, &XrayProjectCleanerScanWorker::onProgress, this, &XrayProjectCleanerScanController::onProgress);
	connect(worker, &XrayProjectCleanerScanWorker::result, this, &XrayProjectCleanerScanController::result);

	connect(p_thread, &QThread::finished, p_timer, &QTimer::deleteLater);
	connect(p_thread, &QThread::finished, worker, &XrayProjectCleanerScanWorker::deleteLater);
}
XrayProjectCleanerScanController::~XrayProjectCleanerScanController()
{
	if (p_thread->isRunning())
	{
		p_thread->quit();
		p_thread->wait();
	}
}
void XrayProjectCleanerScanController::setInterval(int _msec)
{
	p_timer->setInterval(_msec);
}
int XrayProjectCleanerScanController::interval() const
{
	return p_timer->interval();
}
void XrayProjectCleanerScanController::start()
{
	p_thread->start();
}

/*
*/
XrayDeleteProjectFilesWidget::XrayDeleteProjectFilesWidget(QWidget* parent) :
	QWidget(parent),
	ui(new Ui::XrayDeleteProjectFilesWidget),
	p_scanController(nullptr),
	m_demoVersion(false)
{
	ui->setupUi(this);
	setWindowTitle("XProject Files Cleaner");
	
	connect(ui->rootPathBtn, &QPushButton::clicked, this, &XrayDeleteProjectFilesWidget::browseRoot);
	connect(ui->scanBtn, &QPushButton::clicked, this, &XrayDeleteProjectFilesWidget::startScan);
	connect(ui->cancelBtn, &QPushButton::clicked, this, &XrayDeleteProjectFilesWidget::cancelScan);
	connect(ui->updateBtn, &QPushButton::clicked, this, &XrayDeleteProjectFilesWidget::updateStats);
	connect(ui->startCleaningBtn, &QPushButton::clicked, this, &XrayDeleteProjectFilesWidget::runCleaning);

	p_tableModel = nullptr;
	ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
	ui->tableView->setSelectionMode(QAbstractItemView::ExtendedSelection);
	ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui->tableView->setAlternatingRowColors(true);
	ui->tableView->setSortingEnabled(true);
	ui->tableView->setStyleSheet("QTableView::item { padding:2px; }");
	connect(ui->tableView, &QTableView::doubleClicked, this, &XrayDeleteProjectFilesWidget::doubleClickedItem);
}
XrayDeleteProjectFilesWidget::~XrayDeleteProjectFilesWidget()
{
	if (p_scanController)
		delete p_scanController;
	p_scanController = nullptr;
}

void XrayDeleteProjectFilesWidget::writeSettings(QSettings& settings)
{
	settings.beginGroup("XFilesCleaner");
	settings.setValue("RootDir/RootPath", ui->rootPath->text());
	settings.setValue("RootDir/RootPathFilter", ui->rootPathFiltersEdit->text());
	settings.setValue("RootDir/SubDir", ui->subDirectory->text());
	settings.setValue("RootDir/FileExtFilters", ui->fileNameFilters->text());
	settings.setValue("RootDir/NameContainFilters", ui->fileNameContainFilters->text());
	settings.setValue("RootDir/SortNumeric", ui->sortNumericCBox->isChecked());
	settings.setValue("RootDir/SkipStart", ui->skipFromStartSpin->value());
	settings.setValue("RootDir/SkipEnd", ui->skipFromEndSpin->value());
	settings.setValue("RootDir/UpdateInterval", ui->updateIntervalSpin->value());
	settings.setValue("RootDir/StartupScan", ui->startupScanCBtn->isChecked());
	settings.endGroup();
}
void XrayDeleteProjectFilesWidget::readSettings(QSettings& settings)
{
	settings.beginGroup("XFilesCleaner");
	ui->rootPath->setText(settings.value("RootDir/RootPath", ui->rootPath->text()).toString());
	ui->rootPathFiltersEdit->setText(settings.value("RootDir/RootPathFilter", ui->rootPathFiltersEdit->text()).toString());
	ui->subDirectory->setText(settings.value("RootDir/SubDir", ui->subDirectory->text()).toString());
	ui->fileNameFilters->setText(settings.value("RootDir/FileExtFilters", ui->fileNameFilters->text()).toString());
	ui->fileNameContainFilters->setText(settings.value("RootDir/NameContainFilters", ui->fileNameContainFilters->text()).toString());
	ui->sortNumericCBox->setChecked(settings.value("RootDir/SortNumeric", ui->sortNumericCBox->isChecked()).toBool());
	ui->skipFromStartSpin->setValue(settings.value("RootDir/SkipStart", ui->skipFromStartSpin->value()).toInt());
	ui->skipFromEndSpin->setValue(settings.value("RootDir/SkipEnd", ui->skipFromEndSpin->value()).toInt());
	ui->updateIntervalSpin->setValue(settings.value("RootDir/UpdateInterval", ui->updateIntervalSpin->value()).toInt());
	ui->startupScanCBtn->setChecked(settings.value("RootDir/StartupScan", ui->startupScanCBtn->isChecked()).toBool());
	settings.endGroup();
}
void XrayDeleteProjectFilesWidget::scanOnStartup()
{
	if (ui->startupScanCBtn->isChecked())
		startScan();
}

void XrayDeleteProjectFilesWidget::browseRoot()
{
	auto dir = QFileDialog::getExistingDirectory(this, QApplication::translate("XrayDeleteProjectFilesWidget", "Select Root Directory"), XrayGlobal::getLastFileOpenPath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	if (dir.isEmpty())
		return;

	ui->rootPath->setText(dir);
	XrayGlobal::setLastFileOpenPath(dir);
}

void XrayDeleteProjectFilesWidget::updateProperties(std::size_t files, std::size_t size)
{
	ui->totalSize->setText(XrayQFileUtil::getHumanReadableSize(size));
	ui->totalFiles->setText(QString::number(files));
}

void XrayDeleteProjectFilesWidget::startScan()
{
	// first stop the thread then proceed
	cancelScan();

	// scan directories for sub-directories
	auto projects = getSubDirs();
	if (projects.empty())
	{
		ui->cleaningLabel->setText(QApplication::translate("XrayDeleteProjectFilesWidget", "Couldn't find any project directory!"));
		return;
	}

	auto subDir = ui->subDirectory->text();
	auto extFilters = ui->fileNameFilters->text();
	auto containFilters = ui->fileNameContainFilters->text();
	auto isNumericSort = ui->sortNumericCBox->isChecked();
	auto skipStart = ui->skipFromStartSpin->value();
	auto skipEnd = ui->skipFromEndSpin->value();
	auto updateInterval = ui->updateIntervalSpin->value();

	p_scanController = new XrayProjectCleanerScanController(projects, subDir, extFilters, containFilters, isNumericSort, skipStart, skipEnd, updateInterval * 1000, this);
	connect(p_scanController, &XrayProjectCleanerScanController::onStart, this, &XrayDeleteProjectFilesWidget::onStart);
	connect(p_scanController, &XrayProjectCleanerScanController::onProgress, this, &XrayDeleteProjectFilesWidget::onProgress);
	connect(p_scanController, &XrayProjectCleanerScanController::result, [this](const QVector<QVector<QString> >& tableRows, const QVector< QVector<QPair<QString, QStringList> > >& dirStructure, std::size_t totalFiles, std::size_t totalSize)
	{
		// Note: there is no way to reset the table model, clearing the table view works only by creating a new table model and set to table view.
		if (p_tableModel)
			p_tableModel->deleteLater();
		p_tableModel = new XrayTableModel;
		p_tableModel->setDataHasVerticalHeaders(false);

		// proxy model enables sorting by clicking on the headers
		auto proxyModel = new QSortFilterProxyModel(this);
		proxyModel->setSourceModel(p_tableModel);

		p_tableModel->setObjectName("ProjectDirTable");
		p_tableModel->createTable({"Projects", "Files", "Size"}, tableRows);
		ui->tableView->setModel(proxyModel);

		m_tableRows = tableRows;
		m_scannedFiles = dirStructure;

		updateProperties(totalFiles, totalSize);
		ui->cleaningLabel->setText(QApplication::translate("XrayDeleteProjectFilesWidget", "Ready"));

		emit onFinished();
	});

	p_scanController->start();
}

void XrayDeleteProjectFilesWidget::cancelScan()
{
	if (p_scanController)
		delete p_scanController;
	p_scanController = nullptr;
	emit onFinished();
}

void XrayDeleteProjectFilesWidget::updateStats()
{
	std::size_t totalFiles = 0;
	std::size_t totalSize = 0;

	auto selectedRows = ui->tableView->selectionModel()->selectedRows();
	for (const auto& index : selectedRows)
	{
		if (!index.isValid())
			continue;

		if (index.row() < 0 || index.row() >= m_scannedFiles.size())
			continue;

		auto scannedFiles = getScannedFiles(index.data(Qt::DisplayRole).toString(), m_tableRows, m_scannedFiles);

		for (const auto& sf : scannedFiles)
		{
			//qDebug() << sf.first;
			totalFiles += sf.second.size();
			for (const auto& f : sf.second)
			{
				//qDebug() << "\t" << f;
				totalSize += QFileInfo(f).size();
			}
		}
	}

	updateProperties(totalFiles, totalSize);
}

QStringList XrayDeleteProjectFilesWidget::getFilesThatMustBeDeleted()
{
	auto selectedRows = ui->tableView->selectionModel()->selectedRows();
	if (selectedRows.empty())
	{
		ui->cleaningLabel->setText(QApplication::translate("XrayDeleteProjectFilesWidget", "Please select directories that you want to clean!"));
		return {};
	}

	QStringList filesList;
	filesList.reserve(100);

	emit onStart();
	auto n = 0;
	emit onProgress(n);

	for (const auto& index : selectedRows)
	{
		if (!index.isValid())
			continue;

		if (index.row() < 0 || index.row() >= m_scannedFiles.size())
			continue;


		auto scannedFiles = getScannedFiles(index.data(Qt::DisplayRole).toString(), m_tableRows, m_scannedFiles);
		for (const auto& sf : scannedFiles)
		{
			n = 0;
			for (const auto& f : sf.second)
			{
				filesList.push_back(f);

				if (n % 2 == 0)
				{
					emit onProgress(n / static_cast<double>(sf.second.size()));	// emit a progress bar signal with value.
					qApp->processEvents();
				}
				n++;
			}
		}
	}

	emit onFinished();

	return filesList;
}

void XrayDeleteProjectFilesWidget::runCleaning()
{
	ui->cleaningLabel->setText(QApplication::translate("XrayDeleteProjectFilesWidget", "Ready"));

	auto filesList = getFilesThatMustBeDeleted();
	if (filesList.empty())
	{
		ui->cleaningLabel->setText(QApplication::translate("XrayDeleteProjectFilesWidget", "Couldn't find any cleanable file in the selected directory!"));
		return;
	}

	if (filesList.size() > 50000)
	{
		QMessageBox msgBox;
		const auto clean = msgBox.addButton(QApplication::translate("XrayDeleteProjectFilesWidget", "Clean"), QMessageBox::AcceptRole);
		const auto showList = msgBox.addButton(QApplication::translate("XrayDeleteProjectFilesWidget", "Show me the list"), QMessageBox::DestructiveRole);
		msgBox.addButton(QApplication::translate("XrayDeleteProjectFilesWidget", "Cancel"), QMessageBox::RejectRole);
		msgBox.setText(QApplication::translate("XrayDeleteProjectFilesWidget", "The detected files are more than 50 thousands. Do you want to see the list of cleanable files?\n\nPress 'Clean' if you do not want to see the list of cleanable files."));
		msgBox.setIcon(QMessageBox::Question);
		msgBox.exec();
		if (msgBox.clickedButton() == clean)
		{
			deleteFiles(filesList);
			return;
		}
		else if (msgBox.clickedButton() == showList)
		{
			// continue
		}
		else
		{
			return;
		}
	}

	auto dialog = new QDialog;
	dialog->setWindowTitle(windowTitle());
	dialog->setWindowModality(Qt::ApplicationModal);
	dialog->setAttribute(Qt::WA_DeleteOnClose);

	auto listView = new QListView;
	listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
	listView->setSelectionMode(QAbstractItemView::ExtendedSelection);
	listView->setSelectionBehavior(QAbstractItemView::SelectRows);
	listView->setAlternatingRowColors(true);
	listView->setStyleSheet("QTableView::item { padding:2px; }");

	auto listViewModel = new QStringListModel;
	listViewModel->setStringList(filesList);

	listView->setModel(listViewModel);
	listView->setContextMenuPolicy(Qt::CustomContextMenu);
	auto menu = new QMenu(listView);
	connect(listView, &QTableView::customContextMenuRequested, [listView, menu](const QPoint& _point)
	{
		if (!listView->indexAt(_point).isValid())
			return;
		menu->exec(listView->viewport()->mapToGlobal(_point));
	});
	auto action = new QAction(QIcon(":/paint/images/delete_small_icon.png"), QApplication::translate("XrayDeleteProjectFilesWidget", "Remove Selected"), this);
	connect(action, &QAction::triggered, [listView]()
	{
		auto indexes = listView->selectionModel()->selectedRows();
		if (indexes.empty())
			return;

		if (indexes.count() == 1)
		{
			listView->model()->removeRow(indexes.back().row(), QModelIndex());
		}
		else
		{
			std::vector<int> rows;
			rows.resize(indexes.count());
			for (auto i = 0; i < indexes.count(); i++)
				rows[i] = indexes[i].row();
			std::sort(rows.begin(), rows.end());
			std::reverse(rows.begin(), rows.end());	// important to reverse the container.

			for (const auto& i : rows)
				listView->model()->removeRow(i, QModelIndex());
		}
	});
	menu->addAction(action);

	auto buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	buttonBox->button(QDialogButtonBox::Ok)->setText(QApplication::translate("XrayDeleteProjectFilesWidget", "Clean"));
	connect(buttonBox, &QDialogButtonBox::accepted, dialog, &QDialog::accept);
	connect(buttonBox, &QDialogButtonBox::rejected, dialog, &QDialog::reject);

	auto layout = new QGridLayout;
	layout->addWidget(new QLabel(QApplication::translate("XrayDeleteProjectFilesWidget", "Following %1 files are about to be deleted.\nYou can remove files from the list that shouldn't be deleted.\n(Right click to see the menu actions.)\n").arg(QString::number(filesList.size())), 0, 0));
	layout->addWidget(listView, 1, 0);
	layout->addWidget(buttonBox, 2, 0);

	dialog->setLayout(layout);

	dialog->resize(600, 500);
	if (dialog->exec() == QDialog::Accepted)
	{
		auto files = listViewModel->stringList();
		if (files.empty())
			return;

		if (QMessageBox::Yes == QMessageBox(QMessageBox::Question, windowTitle(), QApplication::translate("XrayDeleteProjectFilesWidget", "Are you sure you want to clean?"), QMessageBox::Yes | QMessageBox::No).exec())
			deleteFiles(files);
	}
}

void XrayDeleteProjectFilesWidget::deleteFiles(const QStringList& files)
{
	auto demoCounter = 0;
	for (const auto& f : files)
	{
		if (QFile::remove(f))
		{
			// check the demo version limitations.
			if (m_demoVersion)
			{
				if (demoCounter > 3)
				{
					ui->cleaningLabel->setText(QApplication::translate("XrayDeleteProjectFilesWidget", "You are using a demo version which has limited features."));
					emit onFinished();
					return;
				}
			}

			demoCounter++;
		}
	}

	if (demoCounter > 0)
		ui->cleaningLabel->setText(QApplication::translate("XrayDeleteProjectFilesWidget", "Files in the selected directories have been cleaned successfully!"));
}

void XrayDeleteProjectFilesWidget::enableDemoVersion(bool b)
{
	m_demoVersion = b;
}

QStringList XrayDeleteProjectFilesWidget::getSubDirs()
{
	if (!QDir().exists(ui->rootPath->text()))
		return {};

	emit onStart();
	auto n = 0;
	emit onProgress(n);

	QRegExp re(ui->rootPathFiltersEdit->text());  // a digit (\d), zero or more times (*)
	QStringList paths;
	paths.reserve(100);

	QDirIterator it(ui->rootPath->text(), QDir::Dirs | QDir::NoDotAndDotDot, QDirIterator::NoIteratorFlags);
	while (it.hasNext())
	{
		auto p = it.next();
		if (!re.isEmpty())
		{
			if (re.exactMatch(p.split('/').back()))
				paths.push_back(p);
		}
		else
		{
			paths.push_back(p);
		}

		if (n % 2 == 0)
		{
			emit onProgress(n / static_cast<double>(100));	// emit a progress bar signal with value. We do not know how many directories in the root, so just consider 100.
			qApp->processEvents();
		}
		n++;
	}

	emit onFinished();

	return paths;
}

void XrayDeleteProjectFilesWidget::closeEvent(QCloseEvent* _event)
{
	QWidget::closeEvent(_event);
}

void XrayDeleteProjectFilesWidget::doubleClickedItem(const QModelIndex& index)
{
	if (!index.isValid())
		return;

	if (index.row() < 0 || index.row() >= m_scannedFiles.size())
		return;
	
	auto dirPath = ui->rootPath->text() + '/' + index.sibling(index.row(), 0).data(Qt::DisplayRole).toString().split('/').first() + '/' + ui->subDirectory->text();	// index.sibling(index.row(), 0) => always get the first column of the selected row
	if (!QDesktopServices::openUrl(QUrl::fromLocalFile(dirPath)))	// QUrl::fromLocalFile enable working with network directories, without fromLocalFile, network drives and directories do not open. 
	{
		qDebug() << ui->rootPath->text() + '/' + index.sibling(index.row(), 0).data(Qt::DisplayRole).toString();
		if (!QDesktopServices::openUrl(QUrl::fromLocalFile(ui->rootPath->text() + '/' + index.sibling(index.row(), 0).data(Qt::DisplayRole).toString())))
		{
			if (QDesktopServices::openUrl(QUrl::fromLocalFile(ui->rootPath->text())))
			{

			}
		}
	}
}