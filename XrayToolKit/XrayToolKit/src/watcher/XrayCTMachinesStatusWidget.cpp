/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/03/22
** filename: 	XrayCTMachinesStatusWidget.cpp
** file base:	XrayCTMachinesStatusWidget
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides a user registration widget with SQLITE
*				database connection.
****************************************************************************/

#include "XrayCTMachinesStatusWidget.h"
#include "ui_XrayCTMachinesStatusWidget.h"
#include "XrayGlobal.h"
#include "XrayQtUtil.h"
#include "XrayCollapsibleFrame.h"

#include <QCoreApplication>
#include <QHeaderView>
#include <QStatusBar>
#include <QSettings>
#include <QTimer>
#include <QThread>
#include <QTime>
#include <QDateTime>
#include <QDebug>

XRAYLAB_USING_NAMESPACE

XrayMachineLogFileDictionaryLoaderWorker::XrayMachineLogFileDictionaryLoaderWorker(const QStringList& _files, QObject *parent) :
	QObject(parent),
	m_projectDir(_files)
{
}
void XrayMachineLogFileDictionaryLoaderWorker::read()
{
	if (m_projectDir.size() < 7)
		return;

	XrayMachineLogFileDictionary m_dict;
	m_dict.set_syncmove_file(m_projectDir[0]);
	m_dict.set_calibration_file(m_projectDir[1]);
	m_dict.set_dtxa_file(m_projectDir[2]);
	m_dict.set_xs_control_1886_file(m_projectDir[3]);
	m_dict.set_xs_xray_1886_file(m_projectDir[4]);
	m_dict.set_xs_control_0000_file(m_projectDir[5]);
	m_dict.set_ct_data_dir_path(m_projectDir[6]);
	m_dict.read();

	emit result(m_dict.map());
}

XrayMachineLogFileDictionaryLoaderController::XrayMachineLogFileDictionaryLoaderController(const QStringList& _files, int _updateInterval, QObject *parent) :
	QObject(parent),
	p_thread(new QThread(this)),
	p_timer(new QTimer)
{
	p_timer->setInterval(_updateInterval);
	p_timer->moveToThread(p_thread);

	auto worker = new XrayMachineLogFileDictionaryLoaderWorker(_files);
	worker->moveToThread(p_thread);
	connect(p_thread, &QThread::started, p_timer, static_cast<void (QTimer::*)()>(&QTimer::start));
	connect(p_timer, &QTimer::timeout, worker, &XrayMachineLogFileDictionaryLoaderWorker::read);
	connect(worker, &XrayMachineLogFileDictionaryLoaderWorker::result, this, &XrayMachineLogFileDictionaryLoaderController::result);
	connect(p_thread, &QThread::finished, p_timer, &QTimer::deleteLater);
	connect(p_thread, &QThread::finished, worker, &XrayMachineLogFileDictionaryLoaderWorker::deleteLater);
}
void XrayMachineLogFileDictionaryLoaderController::setInterval(int _msec)
{
	p_timer->setInterval(_msec);
}
int XrayMachineLogFileDictionaryLoaderController::interval() const
{
	return p_timer->interval();
}
void XrayMachineLogFileDictionaryLoaderController::start()
{
	p_thread->start();
}
XrayMachineLogFileDictionaryLoaderController::~XrayMachineLogFileDictionaryLoaderController()
{
	if (p_thread->isRunning())
	{
		p_thread->quit();
		p_thread->wait();

		qDebug() << "Thread stopped.";
	}
}

XrayCTMachineStatusWidget::XrayCTMachineStatusWidget(const QStringList& _files, int _updateInterval, QWidget* _parent) :
	QWidget(_parent),
	p_logsLoader(new XrayMachineLogFileDictionaryLoaderController(_files, _updateInterval, this))
{
	connect(p_logsLoader, &XrayMachineLogFileDictionaryLoaderController::result, this, &XrayCTMachineStatusWidget::updateWidgets);

	groupBox = new QGroupBox(this);

	auto vLayout = new QVBoxLayout;
	auto gLayout = new QGridLayout;

	auto row = 0;
	computerLabel = new QLabel("Computer");
	computerValue = new QLabel("");
	gLayout->addWidget(computerLabel, row, 0);
	gLayout->addWidget(computerValue, row++, 1);

	userLabel = new QLabel("User");
	userValue = new QLabel("");
	gLayout->addWidget(userLabel, row, 0);
	gLayout->addWidget(userValue, row++, 1);

	powerLabel = new QLabel("Power");
	powerValue = new QLabel("");
	gLayout->addWidget(powerLabel, row, 0);
	gLayout->addWidget(powerValue, row++, 1);

	systemRunningStatusLabel = new QLabel("Running status");
	systemRunningStatusValue = new XrayQLedBlink(this);
	systemRunningStatusValue->setOnColor(XrayQLed::ledColor::Green);
	systemRunningStatusValue->setOffColor(XrayQLed::ledColor::Grey);
	systemRunningStatusValue->setValue(true);
	systemRunningStatusValue->setInterval(1000);
	systemRunningStatusValue->setFixedSize(22, 22);
	gLayout->addWidget(systemRunningStatusLabel, row, 0);
	gLayout->addWidget(systemRunningStatusValue, row++, 1);
	systemRunningStatusValue->start();

	xsampleStatusLabel = new QLabel("CNC X status");
	xsampleStatusValue = new XrayQLedBlink(this);
	xsampleStatusValue->setOnColor(XrayQLed::ledColor::Green);
	xsampleStatusValue->setOffColor(XrayQLed::ledColor::Grey);
	xsampleStatusValue->setValue(true);
	xsampleStatusValue->setInterval(1000);
	xsampleStatusValue->setFixedSize(22, 22);
	gLayout->addWidget(xsampleStatusLabel, row, 0);
	gLayout->addWidget(xsampleStatusValue, row++, 1);
	xsampleStatusValue->start();

	projectRunningStatusLabel = new QLabel("Project running status");
	projectRunningStatusValue = new XrayQLedBlink(this);
	projectRunningStatusValue->setOnColor(XrayQLed::ledColor::Green);
	projectRunningStatusValue->setOffColor(XrayQLed::ledColor::Yellow);
	projectRunningStatusValue->setValue(false);
	projectRunningStatusValue->setInterval(1000);
	projectRunningStatusValue->setFixedSize(22, 22);
	gLayout->addWidget(projectRunningStatusLabel, row, 0);
	gLayout->addWidget(projectRunningStatusValue, row++, 1);
	projectRunningStatusValue->start();

	controlStartedLabel = new QLabel("Control start time");
	controlStartedValue = new QLabel("");
	gLayout->addWidget(controlStartedLabel, row, 0);
	gLayout->addWidget(controlStartedValue, row++, 1);

	controlUpTimeLabel = new QLabel("Control up time");
	controlUpTimeValue = new QLabel("");
	gLayout->addWidget(controlUpTimeLabel, row, 0);
	gLayout->addWidget(controlUpTimeValue, row++, 1);

	xrayRunupTimeLabel = new QLabel("Xray stable time");
	xrayRunupTimeValue = new QLabel("");
	gLayout->addWidget(xrayRunupTimeLabel, row, 0);
	gLayout->addWidget(xrayRunupTimeValue, row++, 1);

	xrayRunupUpTimeLabel = new QLabel("Xray up time");
	xrayRunupUpTimeValue = new QLabel("");
	gLayout->addWidget(xrayRunupUpTimeLabel, row, 0);
	gLayout->addWidget(xrayRunupUpTimeValue, row++, 1);

	onTimeSrcLabel = new QLabel("On-time source");
	onTimeSrcValue = new QLabel("");
	gLayout->addWidget(onTimeSrcLabel, row, 0);
	gLayout->addWidget(onTimeSrcValue, row++, 1);

	totalScannedImagesLabel = new QLabel("Total scanned images");
	totalScannedImagesValue = new QLabel("");
	gLayout->addWidget(totalScannedImagesLabel, row, 0);
	gLayout->addWidget(totalScannedImagesValue, row++, 1);
	m_lastTotalScannedImages = 0;

	lastScannedImageNumberLabel = new QLabel("Last scanned image");
	lastScannedImageNumberValue = new QLabel("");
	gLayout->addWidget(lastScannedImageNumberLabel, row, 0);
	gLayout->addWidget(lastScannedImageNumberValue, row++, 1);

	scannedImagesDiffLabel = new QLabel("First-last image time diff");
	scannedImagesDiffValue = new QLabel("");
	gLayout->addWidget(scannedImagesDiffLabel, row, 0);
	gLayout->addWidget(scannedImagesDiffValue, row++, 1);

	vLayout->addLayout(gLayout);

	auto moreParameters = new XrayCollapsibleFrame(this);
	p_moreParamTree = new QTreeWidget(moreParameters);
	QString style = "QHeaderView::section {"
		//"background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,"
		//"stop:0 #616161, stop: 0.5 #505050,"
		//"stop: 0.6 #434343, stop:1 #656565);"
		"color: rgb(57, 158, 244);"
		"padding: 10px;"
		"padding-right: 30px;"
		"border-left: 1px solid rgb(128, 85, 0);"
		"border-right: 1px solid rgb(128, 85, 0);"
		"border-top: 1px solid rgb(128, 85, 0);"
		"border-bottom: 1px solid rgb(128, 85, 0);"
		"font-size: 12px;"
		"font-weight: bold;"
		"}";
	style += "QTreeWidget::item { padding:10px; border-left: 1px solid rgb(128, 85, 0); border-right: 1px solid rgb(128, 85, 0); border-bottom: 1px solid rgb(128, 85, 0); }";
	////style += "QTreeWidget::item:alternate { background: rgb(128, 128, 128); }";
	//style += "QTreeWidget::item:has-children { color: rgb(255, 255, 255); border-right: 1px solid rgb(128, 85, 0, 255); }";
	style += "QTreeWidget::item:hover,QTreeWidget::item:hover:selected { background-color: rgb(128, 85, 0); }";
	style += "QTreeWidget::item:selected, QTreeWidget::item:selected:active, QTreeWidget::item:selected:!active { background-color: rgb(47, 148, 234); }";
	p_moreParamTree->setStyleSheet(style);

	//setRootIsDecorated(false);							// It will remove the root item as childless.
	//setSelectionMode(QAbstractItemView::MultiSelection);	// For enabling multiple items selection.

	p_moreParamTree->setAlternatingRowColors(true);
	p_moreParamTree->setAnimated(false);
	p_moreParamTree->setSortingEnabled(false);
	p_moreParamTree->sortItems(1, Qt::DescendingOrder);
	p_moreParamTree->setEditTriggers(QTreeWidget::NoEditTriggers);

	QStringList columnNames;
	columnNames << "Parameter" << "Value" << "";
	p_moreParamTree->setHeaderLabels(columnNames);
	p_moreParamTree->header()->setStretchLastSection(true);
	p_moreParamTree->header()->setSectionResizeMode(QHeaderView::Interactive);
	//header()->setSectionResizeMode(0, QHeaderView::Fixed);

	p_moreParamTree->setIndentation(0);	// 0 will make the whole tree items expanded by double click without any down and up arrows.
	p_moreParamTree->setColumnCount(2);

	moreParameters->setWidget(p_moreParamTree);

	vLayout->addWidget(moreParameters);
	groupBox->setLayout(vLayout);

	auto layout = new QGridLayout;
	layout->addWidget(groupBox);
	setLayout(layout);

	setStyleSheet(QString::fromUtf8("QLabel { font-size: 11px; font-weight: normal; color: rgb(159,207,249); }"));
}
void XrayCTMachineStatusWidget::start()
{
	p_logsLoader->start();
}
XrayCTMachineStatusWidget::~XrayCTMachineStatusWidget()
{
	if (p_logsLoader)
	{
		delete p_logsLoader;
		p_logsLoader = nullptr;
	}
}
void XrayCTMachineStatusWidget::updateWidgets(const QMap<QString, QVariant>& _map)
{
	qDebug() << _map.value("sm_computer");

	groupBox->setTitle("Machine: " + _map["xsc_system_id"].toString());
	computerValue->setText(_map["sm_computer"].toString());
	userValue->setText(_map["sm_user"].toString());

	powerValue->setText(QString::number((_map["xsc_voltage"].toInt() * 1e3) * (_map["xsc_current"].toInt() * 1e-6)) + "Watts (W)");

	controlStartedValue->setText(_map["xsc0_started_date"].toDate().toString("dd.MM.yyyy") + " " + _map["xsc0_started_time"].toTime().toString("hh:mm:ss"));
	controlUpTimeValue->setText(XrayQtUtil::convertSecToTime(_map["xsc0_up_time_sec"].toInt(), true));

	xrayRunupTimeValue->setText(_map["xsc_runup_date"].toDate().toString("dd.MM.yyyy") + " " + _map["xsc_runup_time"].toTime().toString("hh:mm:ss"));
	xrayRunupUpTimeValue->setText(XrayQtUtil::convertSecToTime(_map["xsx_runup_up_time_sec"].toInt(), true));

	onTimeSrcValue->setText(XrayQtUtil::convertSecToTime(_map["xsc_on_time_source"].toInt(), false));

	auto totalImages = _map["ct_data_project_total_images"];
	totalScannedImagesValue->setText(totalImages.toString());
	lastScannedImageNumberValue->setText(_map["ct_data_project_last_image_number"].toString());
	scannedImagesDiffValue->setText(_map["ct_data_project_first_last_image_diff"].toString() + " msec");

	if (1)
	{
		systemRunningStatusValue->setToolTip("Running! system has been started!");
		systemRunningStatusValue->stop();
		systemRunningStatusValue->setOnColor(XrayQLed::ledColor::Green);
	}
	else
	{
		systemRunningStatusValue->setToolTip("System is not running!");
		systemRunningStatusValue->setOnColor(XrayQLed::ledColor::Red);
		systemRunningStatusValue->setInterval(300);
		systemRunningStatusValue->start();
	}

	auto xsample = _map["pca_x_sample"];
	if (xsample == 0)
	{
		xsampleStatusValue->setToolTip("Ok");
		xsampleStatusValue->stop();
		xsampleStatusValue->setOnColor(XrayQLed::ledColor::Green);
	}
	else
	{
		xsampleStatusValue->setToolTip("Error: XSample is not 0!");
		xsampleStatusValue->setOnColor(XrayQLed::ledColor::Red);
		xsampleStatusValue->setInterval(300);
		xsampleStatusValue->start();
	}

	if (m_lastTotalScannedImages - totalImages.toInt() == 0)
	{
		projectRunningStatusValue->setToolTip("Not running!");
		projectRunningStatusValue->stop();
		projectRunningStatusValue->setOnColor(XrayQLed::ledColor::Yellow);
	}
	else
	{
		projectRunningStatusValue->setToolTip("Running! Images are being captured by the machine.");
		projectRunningStatusValue->setOnColor(XrayQLed::ledColor::Green);
		projectRunningStatusValue->setInterval(300);
		projectRunningStatusValue->start();
	}
	m_lastTotalScannedImages = totalImages.toInt();

	updateMoreParamTree(_map);
}
void XrayCTMachineStatusWidget::updateMoreParamTree(const QMap<QString, QVariant>& _map)
{
	// try to disable features that could effect on 
	// performance while top level items adding to the tree.
	p_moreParamTree->setUpdatesEnabled(false);
	p_moreParamTree->header()->setSectionResizeMode(QHeaderView::Fixed);
	p_moreParamTree->setEditTriggers(QTreeWidget::NoEditTriggers);
	p_moreParamTree->setSortingEnabled(false);
	p_moreParamTree->setAnimated(false);
	p_moreParamTree->setAlternatingRowColors(false);

	p_moreParamTree->clear();
	QMapIterator<QString, QVariant> i(_map);
	while (i.hasNext())
	{
		i.next();

		QString str;
		auto value = i.value();
		if (value.type() == QVariant::Type::Date)
			str = value.toDate().toString("dd.MM.yyyy");
		else if (value.type() == QVariant::Type::Time)
			str = value.toTime().toString("hh:mm:ss");
		else if (value.type() == QVariant::Type::DateTime)
			str = value.toDateTime().toString("dd.MM.yyyy hh:mm:ss");
		else if (value.type() == QVariant::Type::Bool)
			str = value.toBool() ? "Ok" : "Failed";
		else if (value.type() == QVariant::Type::String)
			str = value.toString();

		auto parent = new QTreeWidgetItem(p_moreParamTree, { i.key(), str });
		parent->setExpanded(false);
		parent->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		auto font = parent->font(0);
		font.setBold(true);
		font.setPointSize(9);
		parent->setFont(0, font);
		parent->setTextColor(0, QColor(159, 207, 249));

		font.setBold(false);
		parent->setFont(1, font);
		parent->setTextColor(1, QColor(159, 207, 249));

		p_moreParamTree->addTopLevelItem(parent);
	}
	p_moreParamTree->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
	p_moreParamTree->setSortingEnabled(true);
	p_moreParamTree->setAnimated(true);
	p_moreParamTree->setAlternatingRowColors(true);
	p_moreParamTree->setUpdatesEnabled(true);
}

XrayCTMachinesStatusWidget::XrayCTMachinesStatusWidget(QWidget* _parent) :
	QWidget(_parent),
	ui(new Ui::XrayCTMachinesStatusWidget)
{
	ui->setupUi(this);

	QStringList files;
	files.push_back("log_files\\syncmove.log");
	files.push_back("log_files\\calibration.log");
	files.push_back("log_files\\dtxa.log");
	files.push_back("log_files\\xs_control-1886.log");
	files.push_back("log_files\\xs_xray-1886.log");
	files.push_back("log_files\\xs_control-0000.log");
	files.push_back("C:/Users/ful/Pictures/images");
	auto widget = new XrayCTMachineStatusWidget(files, 2000, this);

	ui->gridLayout_2->addWidget(widget, 0, 0);

	widget->start();
}
XrayCTMachinesStatusWidget::~XrayCTMachinesStatusWidget()
{
	delete ui;
}