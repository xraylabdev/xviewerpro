
TEMPLATE    	= lib
TARGET      	= XrayToolKit
VERSION			= 1.1.3
CONFIG 			+= staticlib
CONFIG 			+= c++17
QMAKE_CXXFLAGS 	+= /std:c++17

# very important to create a same directory structure when loaded in visual studio.
CONFIG 		-= flat
QT       	+= core gui sql xml widgets printsupport svg network

DEFINES += QT_DEPRECATED_WARNINGS

isEmpty(PREFIX) {
    PREFIX = XrayToolKitLib
}

CONFIG(debug, debug|release) {

	LIBS += -L"../XrayLicensingApi/XrayLicensingApi/lib/x64/Release/" -lXrayLicensingApi

	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lade
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lIlmImfd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lippicvmt
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lippiwd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -littnotifyd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibjasperd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibjpeg-turbod
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibpngd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibprotobufd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibtiffd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibwebpd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lopencv_img_hash430d
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lopencv_world430d
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lquircd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lzlibd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmcharlsd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmCommond
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmDICTd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmDSEDd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmexpatd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmgetoptd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmIODd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmjpeg12d
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmjpeg16d
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmjpeg8d
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmMEXDd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmMSFFd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmopenjp2d
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmzlibd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lsocketxxd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lcomctl32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lws2_32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lvfw32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -ladvapi32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lopengl32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lglu32
	
	LIBS += -L"../../thirdparty/libs/x64/KDReports/lib/" -lkdreports1d

	MOC_DIR = XrayToolKit/moc/Debug/
	RCC_DIR = XrayToolKit/rcc/Debug/
	UI_DIR = XrayToolKit/include/ui/
	
	contains(QT_ARCH, i386) {
		OBJECTS_DIR = obj/x86/Debug/
		DESTDIR = XrayToolKit/lib/x86/Debug/
		LIB_TARGET = $$PREFIX/lib/x86/Debug/XrayToolKit.lib
		target.path = $$PREFIX/lib/x86/Debug/lib/
	} else {
		OBJECTS_DIR = obj/x64/Debug/
		DESTDIR = XrayToolKit/lib/x64/Debug/
		LIB_TARGET = $$PREFIX/lib/x64/Debug/XrayToolKit.lib
		target.path = $$PREFIX/lib/x64/Debug/lib/
	}
	
} else {
	
	LIBS += -L"../XrayLicensingApi/XrayLicensingApi/lib/x64/Debug/" -lXrayLicensingApi
	
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lade
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lIlmImf
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lippicvmt
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lippiw
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -littnotify
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibjasper
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibjpeg-turbo
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibpng
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibprotobuf
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibtiff
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibwebp
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lopencv_img_hash410
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lopencv_world410
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lquirc
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lzlib
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmcharls
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmCommon
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmDICT
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmDSED
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmexpat
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmgetopt
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmIOD
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmjpeg12
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmjpeg16
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmjpeg8
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmMEXD
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmMSFF
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmopenjp2
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmzlib
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lsocketxx
	
	LIBS += -L"../../thirdparty/libs/x64/KDReports/lib/" -lkdreports

	MOC_DIR = XrayToolKit/moc/Release/
	RCC_DIR = XrayToolKit/rcc/Release/
	UI_DIR = XrayToolKit/include/ui/
	
	contains(QT_ARCH, i386) {
		OBJECTS_DIR = obj/x86/Release/
		DESTDIR = XrayToolKit/lib/x86/Release/
		LIB_TARGET = $$PREFIX/lib/x86/Release/XrayToolKit.lib
		target.path = $$PREFIX/lib/x86/Release/lib/
	} else {
		OBJECTS_DIR = obj/x64/Release/
		DESTDIR = XrayToolKit/lib/x64/Release/
		LIB_TARGET = $$PREFIX/lib/x64/Release/XrayToolKit.lib
		target.path = $$PREFIX/lib/x64/Release/lib/
	}
}

	
INCLUDEPATH +=	XrayToolKit/ \
				XrayToolKit/include/ \
				XrayToolKit/include/globals/ \
				XrayToolKit/include/widgets/ \
				XrayToolKit/include/paint/ \
				XrayToolKit/include/watcher/ \
				XrayToolKit/include/report/ \
				XrayToolKit/include/sql/ \
				XrayToolKit/include/filehandler/ \
				XrayToolKit/include/network/ \
				XrayToolKit/include/license/ \
				XrayToolKit/include/cv/ \
				XrayToolKit/include/utilities/ \
				XrayToolKit/include/math/ \
				XrayToolKit/include/res/ \
				XrayToolKit/include/ui/ \
				XrayToolKit/src/xlsx/ \
				../XrayLicensingApi/XrayLicensingApi/include/ \
				../../thirdparty/libs/x64/opencv/include/ \
				../../thirdparty/libs/x64/KDReports/include/KDReports/ \
				../../thirdparty/libs/src/spdlog-1.x/include/ \
				
include(XrayToolKit/src/xlsx/qtxlsx.pri)

SOURCES     += \
			   XrayToolKit/src/widgets/XrayQApplication.cpp \
			   XrayToolKit/src/widgets/XraySingletonQApplication.cpp \
			   XrayToolKit/src/widgets/XrayQAppLauncher.cpp \
			   XrayToolKit/src/widgets/XrayColorPushButton.cpp \
			   XrayToolKit/src/widgets/XrayFontSelectorWidget.cpp \
			   XrayToolKit/src/widgets/XrayButtonGroupBox.cpp \
			   XrayToolKit/src/widgets/XrayAbstractButtonListModel.cpp \
			   XrayToolKit/src/widgets/XrayUpDownButton.cpp \
			   XrayToolKit/src/widgets/XrayCollapsibleFrame.cpp \
			   XrayToolKit/src/widgets/XrayPixmapStatusWidget.cpp \
			   XrayToolKit/src/widgets/XrayQThread.cpp \
			   XrayToolKit/src/widgets/XrayThreadedTimer.cpp \
			   XrayToolKit/src/widgets/XrayProgressChangeEvent.cpp \
			   XrayToolKit/src/widgets/XrayProgressWidget.cpp \
			   XrayToolKit/src/widgets/XrayProgressBarWidget.cpp \
			   XrayToolKit/src/widgets/XrayFileSystemWatcher.cpp \
			   XrayToolKit/src/widgets/XrayToolButtonGridLayoutWidget.cpp \
               XrayToolKit/src/widgets/XrayHoverEventPushButton.cpp \
               XrayToolKit/src/widgets/XrayImageToolButton.cpp \
               XrayToolKit/src/widgets/XrayImageSlideShowWidget.cpp \
			   XrayToolKit/src/widgets/XrayIconPushButton.cpp \
			   XrayToolKit/src/widgets/XrayResizeDialog.cpp \
			   XrayToolKit/src/widgets/XrayStatusBar.cpp \
			   XrayToolKit/src/widgets/XrayWizardWidget.cpp \
			   XrayToolKit/src/widgets/XrayTableModel.cpp \
			   XrayToolKit/src/widgets/XrayFindFilesWidget.cpp \
			   XrayToolKit/src/widgets/XrayFramelessWindowWin32.cpp \
			   XrayToolKit/src/widgets/XrayStackedFramelessWindowWin32.cpp \
			   XrayToolKit/src/widgets/XrayFramelessWindow.cpp \
			   XrayToolKit/src/widgets/XrayFramelessWindowDragger.cpp \
			   XrayToolKit/src/widgets/XrayQDarkStyle.cpp \
			   XrayToolKit/src/widgets/XrayQDigitalNumbers.cpp \
			   XrayToolKit/src/widgets/XrayQVerticalBarMeter.cpp \
			   XrayToolKit/src/widgets/XrayQToleranceBar.cpp \
			   XrayToolKit/src/widgets/XrayQRecentFilesMenu.cpp \
			   XrayToolKit/src/widgets/XrayQListWidget.cpp \
			   XrayToolKit/src/widgets/XrayQListWidgetGroupBoxWithAddFile.cpp \
			   XrayToolKit/src/widgets/XrayQListWidgetGroupBoxWithAddText.cpp \
			   XrayToolKit/src/widgets/XrayQTreeWidget.cpp \
			   XrayToolKit/src/widgets/XrayQTableView.cpp \
			   XrayToolKit/src/widgets/XrayQCalendarWidget.cpp \
			   XrayToolKit/src/widgets/XrayQCalendarDialog.cpp \
			   XrayToolKit/src/widgets/XrayQDoubleSlider.cpp \
			   XrayToolKit/src/widgets/XrayQDoubleSliderEditor.cpp \
			   XrayToolKit/src/widgets/XrayQLed.cpp \
			   XrayToolKit/src/widgets/XrayTextEditor.cpp \
			   XrayToolKit/src/widgets/XrayQTabWidget.cpp \
			   XrayToolKit/src/widgets/XraySortFilterProxyModel.cpp \
			   XrayToolKit/src/widgets/XrayStandardModelTreeView.cpp \
			   XrayToolKit/src/widgets/XrayStandardModelCompleterTreeViewWidget.cpp \
			   XrayToolKit/src/widgets/XrayStandardModelCompleterTreeViewComboBox.cpp \
			   XrayToolKit/src/widgets/XrayHistogramViewWidget.cpp \
			   XrayToolKit/src/widgets/XraySplashScreen.cpp \
			   XrayToolKit/src/widgets/XrayAboutDialog.cpp \
			   \
			   XrayToolKit/src/paint/XrayMainPaintWidget.cpp \
			   XrayToolKit/src/paint/XrayPaintMainWindow.cpp \
			   XrayToolKit/src/paint/XrayPaintFramelessMainWindow.cpp \
			   XrayToolKit/src/paint/XrayPaintShapeFactory.cpp \
			   XrayToolKit/src/paint/XrayPaintShapeListModel.cpp \
			   XrayToolKit/src/paint/XrayPaintShapeListView.cpp \
			   XrayToolKit/src/paint/XrayPaintShapes.cpp \
			   XrayToolKit/src/paint/XrayPaintUndoCommands.cpp \
			   XrayToolKit/src/paint/XrayFlashFilter.cpp \
			   XrayToolKit/src/paint/XrayBGAInspectionFilter.cpp \
			   XrayToolKit/src/paint/XrayImageFilters.cpp \
			   XrayToolKit/src/paint/XrayVoidAnalysisFilter.cpp \
			   XrayToolKit/src/paint/XrayImageStitchingWidget.cpp \
			   XrayToolKit/src/paint/XrayImageCropWidget.cpp \
			   XrayToolKit/src/paint/XrayImageStitchingWindow.cpp \
			   XrayToolKit/src/paint/XraySaveReportAsExcelDialog.cpp \
			   \
			   XrayToolKit/src/watcher/XrayThreadedFileSystemWatcher.cpp \
			   XrayToolKit/src/watcher/XrayFileSystemWatcherTreeWidget.cpp \
			   XrayToolKit/src/watcher/XrayFileSystemWatcherWindow.cpp \
			   XrayToolKit/src/watcher/XrayFileSystemWatcherWidget.cpp \
			   XrayToolKit/src/watcher/XrayMachineLogFileDictionary.cpp \
			   XrayToolKit/src/watcher/XrayCTMachinesStatusWidget.cpp \
			   XrayToolKit/src/watcher/XrayDeleteProjectFilesWindow.cpp \
			   XrayToolKit/src/watcher/XrayDeleteProjectFilesWidget.cpp \
			   XrayToolKit/src/watcher/XrayFileSystemStructure.cpp \
			   \
			   XrayToolKit/src/report/XrayReportWizardPages.cpp \
			   XrayToolKit/src/report/XrayReportWizardWidget.cpp \
			   XrayToolKit/src/report/XrayReportWizardWindow.cpp \
			   XrayToolKit/src/report/XrayQuickReportGenerator.cpp \
			   XrayToolKit/src/report/XrayQuickReportGeneratorWidget.cpp \
			   XrayToolKit/src/report/XrayQuickReportGeneratorWindow.cpp \
			   XrayToolKit/src/report/XrayBatchWaterMarkerWidget.cpp \
			   XrayToolKit/src/report/XrayBatchWaterMarkerWindow.cpp \
			   \
			   XrayToolKit/src/sql/XraySqlUserManagementWidget.cpp \
			   XrayToolKit/src/sql/XraySqlDatabase.cpp \
			   \
			   XrayToolKit/src/filehandler/XrayCSVHandler.cpp \
			   XrayToolKit/src/filehandler/XrayXmlSettings.cpp \
			   XrayToolKit/src/filehandler/XrayQFileReader.cpp \
			   \
			   XrayToolKit/src/network/XrayQFileDownloader.cpp \
			   XrayToolKit/src/network/XrayCheckForUpdate.cpp \
			   XrayToolKit/src/network/XrayQFileDownloaderWidget.cpp \
			   XrayToolKit/src/network/XrayTcpServerThread.cpp \
			   XrayToolKit/src/network/XrayTcpServer.cpp \
			   XrayToolKit/src/network/XrayTcpClientThread.cpp \
			   XrayToolKit/src/network/XrayTcpClientMainWidget.cpp \
			   XrayToolKit/src/network/XrayTcpServerMainWidget.cpp \
			   XrayToolKit/src/network/XrayTcpClientServerMainWindow.cpp \
			   \
			   XrayToolKit/src/license/XrayLicenseFormWidget.cpp \
			   \
			   XrayToolKit/src/cv/XrayCVUtil.cpp \	
			   XrayToolKit/src/cv/fvkContrastEnhancerUtil.cpp \	
			   XrayToolKit/src/cv/fvkSimpleBlobDetector.cpp \
			   \
			   XrayToolKit/src/utilities/XrayLogger.cpp \
			   XrayToolKit/src/utilities/XrayQFileUtil.cpp \
			   XrayToolKit/src/utilities/XrayQDateTime.cpp \
			   XrayToolKit/src/utilities/XrayQSimpleCrypt.cpp \
			   XrayToolKit/src/utilities/XrayQtUtil.cpp \
			   \
			   XrayToolKit/src/math/XrayStdDev.cpp \
			   \
			   XrayToolKit/src/globals/XrayGlobal.cpp \
			   XrayToolKit/src/globals/XrayLicenseGlobals.cpp \
			   \
			   XrayToolKit/src/res/XrayGlobalResources.cpp
			   
			   
HEADERS     += \
			   XrayToolKit/include/widgets/XrayQApplication.h \
			   XrayToolKit/include/widgets/XraySingletonQApplication.h \
			   XrayToolKit/include/widgets/XrayQAppLauncher.h \
			   XrayToolKit/include/widgets/XrayColorPushButton.h \
			   XrayToolKit/include/widgets/XrayFontSelectorWidget.h \
			   XrayToolKit/include/widgets/XrayButtonGroupBox.h \
			   XrayToolKit/include/widgets/XrayAbstractButtonListModel.h \
			   XrayToolKit/include/widgets/XrayUpDownButton.h \
			   XrayToolKit/include/widgets/XrayCollapsibleFrame.h \
			   XrayToolKit/include/widgets/XrayPixmapStatusWidget.h \
			   XrayToolKit/include/widgets/XrayQThread.h \
			   XrayToolKit/include/widgets/XrayThreadedTimer.h \
			   XrayToolKit/include/widgets/XrayProgressChangeEvent.h \
			   XrayToolKit/include/widgets/XrayProgressWidget.h \
			   XrayToolKit/include/widgets/XrayProgressBarWidget.h \
			   XrayToolKit/include/widgets/XrayFileSystemWatcher.h \
			   XrayToolKit/include/widgets/XrayToolButtonGridLayoutWidget.h \
               XrayToolKit/include/widgets/XrayHoverEventPushButton.h \
               XrayToolKit/include/widgets/XrayImageToolButton.h \
               XrayToolKit/include/widgets/XrayImageSlideShowWidget.h \
			   XrayToolKit/include/widgets/XrayIconPushButton.h \
			   XrayToolKit/include/widgets/XrayResizeDialog.h \
			   XrayToolKit/include/widgets/XrayStatusBar.h \
			   XrayToolKit/include/widgets/XrayWizardWidget.h \
			   XrayToolKit/include/widgets/XrayTableModel.h \
			   XrayToolKit/include/widgets/XrayFindFilesWidget.h \
			   XrayToolKit/include/widgets/XrayFramelessWindowWin32.h \
			   XrayToolKit/include/widgets/XrayStackedFramelessWindowWin32.h \
			   XrayToolKit/include/widgets/XrayFramelessWindow.h \
			   XrayToolKit/include/widgets/XrayFramelessWindowDragger.h \
			   XrayToolKit/include/widgets/XrayQDigitalNumbers.h \
			   XrayToolKit/include/widgets/XrayQVerticalBarMeter.h \
			   XrayToolKit/include/widgets/XrayQToleranceBar.h \
			   XrayToolKit/include/widgets/XrayQRecentFilesMenu.h \
			   XrayToolKit/include/widgets/XrayQDarkStyle.h \
			   XrayToolKit/include/widgets/XrayQListWidget.h \
			   XrayToolKit/include/widgets/XrayQListWidgetGroupBoxWithAddFile.h \
			   XrayToolKit/include/widgets/XrayQListWidgetGroupBoxWithAddText.h \
			   XrayToolKit/include/widgets/XrayQTreeWidget.h \
			   XrayToolKit/include/widgets/XrayQTableView.h \
			   XrayToolKit/include/widgets/XrayQCalendarWidget.h \
			   XrayToolKit/include/widgets/XrayQCalendarDialog.h \
			   XrayToolKit/include/widgets/XrayQDoubleSlider.h \
			   XrayToolKit/include/widgets/XrayQDoubleSliderEditor.h \
			   XrayToolKit/include/widgets/XrayQLed.h \
			   XrayToolKit/include/widgets/XrayTextEditor.h \
			   XrayToolKit/include/widgets/XrayQTabWidget.h \
			   XrayToolKit/include/widgets/XraySortFilterProxyModel.h \
			   XrayToolKit/include/widgets/XrayStandardModelTreeView.h \
			   XrayToolKit/include/widgets/XrayStandardModelCompleterTreeViewWidget.h \
			   XrayToolKit/include/widgets/XrayStandardModelCompleterTreeViewComboBox.h \
			   XrayToolKit/include/widgets/XrayHistogramViewWidget.h \
			   XrayToolKit/include/widgets/XraySplashScreen.h \
			   XrayToolKit/include/widgets/XrayAboutDialog.h \
			   \
			   XrayToolKit/include/watcher/XrayThreadedFileSystemWatcher.h \
			   XrayToolKit/include/watcher/XrayFileSystemWatcherTreeWidget.h \
			   XrayToolKit/include/watcher/XrayFileSystemWatcherWindow.h \
			   XrayToolKit/include/watcher/XrayFileSystemWatcherWidget.h \
			   XrayToolKit/include/watcher/XrayMachineLogFileDictionary.h \
			   XrayToolKit/include/watcher/XrayCTMachinesStatusWidget.h \
			   XrayToolKit/include/watcher/XrayDeleteProjectFilesWindow.h \
			   XrayToolKit/include/watcher/XrayDeleteProjectFilesWidget.h \
			   XrayToolKit/include/watcher/XrayFileSystemStructure.h \
			   \
			   XrayToolKit/include/report/XrayReportWizardPages.h \
			   XrayToolKit/include/report/XrayReportWizardWidget.h \
			   XrayToolKit/include/report/XrayReportWizardWindow.h \
			   XrayToolKit/include/report/XrayQuickReportGenerator.h \
			   XrayToolKit/include/report/XrayQuickReportGeneratorWidget.h \
			   XrayToolKit/include/report/XrayQuickReportGeneratorWindow.h \
			   XrayToolKit/include/report/XrayBatchWaterMarkerWidget.h \
			   XrayToolKit/include/report/XrayBatchWaterMarkerWindow.h \
			   \
			   XrayToolKit/include/sql/XraySqlUserManagementWidget.h \
			   XrayToolKit/include/sql/XraySqlDatabase.h \
			   \
			   XrayToolKit/include/filehandler/XrayCSVHandler.h \
			   XrayToolKit/include/filehandler/XrayXmlSettings.h \
			   XrayToolKit/include/filehandler/XrayQFileReader.h \
			   \		   
			   XrayToolKit/include/paint/XrayMainPaintWidget.h \
			   XrayToolKit/include/paint/XrayPaintMainWindow.h \
			   XrayToolKit/include/paint/XrayPaintFramelessMainWindow.h \
			   XrayToolKit/include/paint/XrayPaintShapeFactory.h \
			   XrayToolKit/include/paint/XrayPaintShapeListModel.h \
			   XrayToolKit/include/paint/XrayPaintShapeListView.h \
			   XrayToolKit/include/paint/XrayPaintShapes.h \
			   XrayToolKit/include/paint/XrayPaintUndoCommands.h \
			   XrayToolKit/include/paint/XrayFlashFilter.h \
			   XrayToolKit/include/paint/XrayBGAInspectionFilter.h \	
			   XrayToolKit/include/paint/XrayImageFilters.h \
			   XrayToolKit/include/paint/XrayVoidAnalysisFilter.h \
			   XrayToolKit/include/paint/XrayImageStitchingWidget.h \
			   XrayToolKit/include/paint/XrayImageCropWidget.h \
			   XrayToolKit/include/paint/XrayImageStitchingWindow.h \
			   XrayToolKit/include/paint/XraySaveReportAsExcelDialog.h \
			   \
			   XrayToolKit/include/network/XrayQFileDownloader.h \
			   XrayToolKit/include/network/XrayCheckForUpdate.h \
			   XrayToolKit/include/network/XrayQFileDownloaderWidget.h \
			   XrayToolKit/include/network/XrayTcpServerThread.h \
			   XrayToolKit/include/network/XrayTcpServer.h \
			   XrayToolKit/include/network/XrayTcpClientThread.h \
			   XrayToolKit/include/network/XrayTcpClientMainWidget.h \
			   XrayToolKit/include/network/XrayTcpServerMainWidget.h \
			   XrayToolKit/include/network/XrayTcpClientServerMainWindow.h \
			   \			   
			   XrayToolKit/include/license/XrayLicenseFormWidget.h \			   
			   \
			   XrayToolKit/include/cv/XrayCVUtil.h \
			   XrayToolKit/include/cv/fvkContrastEnhancerUtil.h \
			   XrayToolKit/include/cv/fvkSimpleBlobDetector.h \
			   \			   
			   XrayToolKit/include/utilities/XrayLogger.h \
			   XrayToolKit/include/utilities/XrayQFileUtil.h \
			   XrayToolKit/include/utilities/XrayQDateTime.h \
			   XrayToolKit/include/utilities/XrayQSimpleCrypt.h \
			   XrayToolKit/include/utilities/XrayQtUtil.h \
			   XrayToolKit/include/utilities/XrayQtApiExport.h \
			   \
			   XrayToolKit/include/math/XrayStdDev.h \
			   \
			   XrayToolKit/include/globals/XrayGlobal.h \
			   XrayToolKit/include/globals/XrayLicenseGlobals.h \
			   \
			   XrayToolKit/include/res/XrayGlobalResources.h

FORMS       += XrayToolKit/src/widgets/XrayFramelessWindow.ui \
			   XrayToolKit/src/widgets/XrayQDoubleSliderEditor.ui \
			   XrayToolKit/src/widgets/XrayAboutDialog.ui \
			   XrayToolKit/src/sql/XraySqlUserManagementWidget.ui \
			   XrayToolKit/src/report/XrayQuickReportGeneratorWidget.ui \
			   XrayToolKit/src/report/XrayBatchWaterMarkerWidget.ui \
			   XrayToolKit/src/paint/XrayFlashFilter.ui \
			   XrayToolKit/src/paint/XrayBGAInspectionFilter.ui \
			   XrayToolKit/src/paint/XrayImageFilters.ui \
			   XrayToolKit/src/paint/XrayVoidAnalysisFilter.ui \
			   XrayToolKit/src/paint/XrayImageCropWidget.ui \
			   XrayToolKit/src/paint/XrayImageStitchingWidget.ui \
			   XrayToolKit/src/paint/XraySaveReportAsExcelDialog.ui \
			   XrayToolKit/src/license/XrayLicenseFormWidget.ui \
			   \
			   XrayToolKit/src/watcher/XrayCTMachinesStatusWidget.ui \
			   XrayToolKit/src/watcher/XrayDeleteProjectFilesWidget.ui


RESOURCES   += XrayToolKit/src/res/darkstyle.qrc \
               XrayToolKit/src/res/images.qrc \
			   XrayToolKit/src/res/leds.qrc \
			   XrayToolKit/src/res/languages.qrc
			   
TRANSLATIONS += XrayToolKit/src/res/language/xraytoolkit_en.ts \
				XrayToolKit/src/res/language/xraytoolkit_de.ts



headers.path = $$PREFIX/include
headers.files = $$HEADERS

INSTALLS += target headers