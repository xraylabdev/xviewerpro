
TEMPLATE    	=  app
TARGET      	=  XPaintPro
QT       		+= core gui sql xml widgets printsupport svg network
CONFIG 			+= c++17
QMAKE_CXXFLAGS 	+= /std:c++17
CONFIG 			-= flat

DEFINES += QT_DEPRECATED_WARNINGS


CONFIG(debug, debug|release) {
	CONFIG += console qt

	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lade
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lIlmImfd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lippicvmt
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lippiwd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -littnotifyd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibjasperd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibjpeg-turbod
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibpngd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibprotobufd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibtiffd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibwebpd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lopencv_img_hash430d
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lopencv_world430d
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lquircd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lzlibd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmcharlsd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmCommond
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmDICTd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmDSEDd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmexpatd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmgetoptd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmIODd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmjpeg12d
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmjpeg16d
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmjpeg8d
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmMEXDd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmMSFFd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmopenjp2d
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmzlibd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lsocketxxd
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lopengl32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lglu32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lcomctl32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lcomdlg32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdi32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lws2_32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lvfw32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -ladvapi32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -luser32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lshell32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lole32
	
	LIBS += -L"../XrayLicensingApi/XrayLicensingApi/lib/x64/Debug/" -lXrayLicensingApi
	LIBS += -L"../XrayToolKit/XrayToolKit/lib/x64/Debug/" -lXrayToolKit

	MOC_DIR = XPaintPro/moc/Debug/
	RCC_DIR = XPaintPro/rcc/Debug/
	UI_DIR = XPaintPro/include/ui/
	OBJECTS_DIR = x64/Debug/
	DESTDIR = XPaintPro/x64/Debug/
	
} else {
	CONFIG += qt
	
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lade
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lIlmImf
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lippicvmt
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lippiw
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -littnotify
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibjasper
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibjpeg-turbo
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibpng
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibprotobuf
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibtiff
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -llibwebp
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lopencv_img_hash430
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lopencv_world430
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lquirc
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lzlib
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmcharls
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmCommon
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmDICT
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmDSED
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmexpat
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmgetopt
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmIOD
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmjpeg12
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmjpeg16
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmjpeg8
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmMEXD
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmMSFF
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmopenjp2
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdcmzlib
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lsocketxx
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lopengl32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lglu32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lcomctl32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lcomdlg32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lgdi32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lws2_32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lvfw32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -ladvapi32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -luser32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lshell32
	LIBS += -L"../../thirdparty/libs/x64/opencv/x64/vc15/staticlib/" -lole32
	
	LIBS += -L"../XrayLicensingApi/XrayLicensingApi/lib/x64/Release/" -lXrayLicensingApi
	LIBS += -L"../XrayToolKit/XrayToolKit/lib/x64/Release/" -lXrayToolKit

	MOC_DIR = XPaintPro/moc/Release/
	RCC_DIR = XPaintPro/rcc/Release/
	UI_DIR = XPaintPro/include/ui/
	OBJECTS_DIR = x64/Release/
	DESTDIR = XPaintPro/x64/Release/
}

	
INCLUDEPATH +=	XPaintPro/include/app/ \
				XPaintPro/include/ui/ \
				../XrayLicensingApi/XrayLicensingApi/include/ \
				../XrayToolKit/XrayToolKit/include/widgets/ \
				../XrayToolKit/XrayToolKit/include/paint/ \
				../XrayToolKit/XrayToolKit/include/watcher/ \
				../XrayToolKit/XrayToolKit/include/report/ \
				../XrayToolKit/XrayToolKit/include/sql/ \
				../XrayToolKit/XrayToolKit/include/filehandler/ \
				../XrayToolKit/XrayToolKit/include/cv/ \
				../XrayToolKit/XrayToolKit/include/network/ \
				../XrayToolKit/XrayToolKit/include/license/ \
				../XrayToolKit/XrayToolKit/include/utilities/ \
				../XrayToolKit/XrayToolKit/include/math/ \
				../XrayToolKit/XrayToolKit/include/globals/ \
				../XrayToolKit/XrayToolKit/include/res/ \
				../XrayToolKit/XrayToolKit/include/ui/ \
				../../thirdparty/libs/x64/opencv/include/ \
				../../thirdparty/libs/src/spdlog-1.x/include/


SOURCES     += \
			   XPaintPro/src/app/XrayMainWindow.cpp \
			   XPaintPro/main.cpp
			   

HEADERS     += \
			   XPaintPro/include/app/XrayMainWindow.h


FORMS       += \



RESOURCES   += \
               XPaintPro/src/res/app_images.qrc \
			   XPaintPro/src/res/app_languages.qrc


TRANSLATIONS += XPaintPro/src/res/language/xpaintpro_en.ts \
				XPaintPro/src/res/language/xpaintpro_de.ts

win32:RC_ICONS += XPaintPro/src/res/images/xpaintpro_icon.ico
RC_FILE = XPaintPro_resource.rc