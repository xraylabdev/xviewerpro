/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/01/04
** filename: 	main.cpp
** file base:	main
** file ext:	cpp
** developer:	Muti Ur Rehman (mre@xray-lab.com)
** purpose:		main function of XViewerPro application.
****************************************************************************/

#include "XrayQApplication.h"
#include "XrayGlobalResources.h"
#include "XrayGlobal.h"
#include "XraySplashScreen.h"
#include "XrayMainWindow.h"

#include <QDebug>

XRAYLAB_USING_NAMESPACE

using namespace cv;

int main(int argc, char *argv[])
{
#ifdef _WIN32
	// Qt windows defaults to the PROCESS_PER_MONITOR_DPI_AWARE for DPI display
	// on windows. Unfortunately, this doesn't work well on multi-screens setups.
	// By calling SetProcessDPIAware(), we force the value to
	// PROCESS_SYSTEM_DPI_AWARE instead which fixes those issues.
	SetProcessDPIAware();
#endif

	XrayLogger::init();
	xAppInfo("App initialized!");

	XrayGlobalResources::initResources();		// must put here before creating a QApplication object.
	XrayQApplication::installMessageHandler();
	XrayQApplication::setGlobalAttributes();	// must put here before creating a QApplication object.
	XrayQApplication app(argc, argv);
	app.setOrganizationName("XRAY-LAB");
	app.setOrganizationDomain("https://xray-lab.com");
	app.setApplicationName("XViewer Pro");
	app.setApplicationDisplayName("XViewer Pro");
	app.setApplicationVersion("1.0.098");
	app.setApplicationBuildDate("2021-04-12");
	app.setWindowIcon(QIcon(":/res/images/xpaintpro_icon.png"));
	app.loadLanguage(XrayGlobal::getCurrentAppLanguage());

	auto isSplash = true;
	XraySplashScreen* splash = nullptr;
	if (isSplash)
	{
		QFont appNameFont("Arial", 20, QFont::Bold, false);
		QFont appVerFont("Arial", 9, QFont::Bold, false);
		QFont copyrightFont("Arial", 8, QFont::Bold, false);

		QFontMetrics fmn(appNameFont);
		auto appVer = "Version: " + app.applicationVersion();
		QFontMetrics fmv(appVerFont);
		auto copyright = QString::fromLatin1("COPYRIGHT � 2020 XRAY-LAB GmbH & CO. KG. ALL RIGHTS RESERVED");
		QFontMetrics fmc(copyrightFont);

		splash = new XraySplashScreen(QPixmap(":/res/images/Splash.png"), 8, Qt::WindowStaysOnTopHint | Qt::SplashScreen);
		splash->setBarColor(QColor(120, 214, 75));
		splash->setBarROI(QRect(5, splash->height() - 21, splash->width() - 10, 16));
		splash->addText({ app.applicationName(), appNameFont, QColor(228, 228, 228),  QRect(0, 160, splash->width(), fmn.boundingRect(app.applicationName()).height()), Qt::AlignCenter });
		splash->addText({ appVer, appVerFont, QColor(218, 218, 218),  QRect(0, 200, splash->width(), fmv.boundingRect(appVer).height()), Qt::AlignCenter });
		splash->addText({ copyright, copyrightFont, QColor(218, 218, 218),  QRect(0, 348, splash->width(), fmc.boundingRect(copyright).height()), Qt::AlignCenter });
		splash->start();
	}

	XrayMainWindow win;

	if (isSplash && splash)
	{
		splash->close();
		delete splash;
	}

	win.show();
	win.raise();

	// open project from command line.
	if (argc == 2)
	{
		auto fileName = QString(argv[1]);
		QTimer::singleShot(100, &app, [&win, fileName]() { win.openProject(fileName); }); // wait until app.exec() executes.
	}

	xAppInfo("Loading app with size '{}x{}' and {}!", win.width(), win.height(), QSslSocket::sslLibraryBuildVersionString());

	auto r = app.exec();
	xAppInfo("App exiting with {}", r);
	return r;
}

// qt deployment command: 
// C:\Qt\Qt5.14.1\5.14.1\msvc2017_64\bin\windeployqt --printsupport --xml C:\Users\Rehman\Projects\XViewerPro\XPaintPro\XPaintPro\x64\Release\XViewerPro.exe --release
// C:\Qt\Qt5.14.1\5.14.1\msvc2017_64\bin\windeployqt --printsupport --xml C:\Users\ful\Documents\codes\apps\XPaintPro\XPaintPro\x64\Debug\XPaintPro.exe --debug

// to update the qt translation file
// C:\Qt\Qt5.14.1\5.14.1\msvc2017_64\bin\lupdate C:\Users\ful\Documents\codes\apps\XPaintPro\XPaintPro.pro
// C:\Qt\Qt5.14.1\5.14.1\msvc2017_64\bin\lupdate C:\Users\ful\Documents\codes\apps\XrayToolKit\XrayToolKit.pro

// to create a *.qm translation file
// C:\Qt\Qt5.14.1\5.14.1\msvc2017_64\bin\lrelease C:\Users\ful\Documents\codes\apps\XPaintPro\XPaintPro.pro
// C:\Qt\Qt5.14.1\5.14.1\msvc2017_64\bin\lrelease C:\Users\ful\Documents\codes\apps\XrayToolKit\XrayToolKit.pro
