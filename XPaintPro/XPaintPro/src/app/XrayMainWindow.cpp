﻿/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/06
** filename: 	XrayMainWindow.cpp
** file base:	XrayMainWindow
** file ext:	cpp
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the main window of the application.
****************************************************************************/

#include "XrayMainWindow.h"
#include "XrayAboutDialog.h"

#include "XrayQApplication.h"
#include "XrayQDarkStyle.h"
#include "XrayGlobal.h"
#include "XrayLicenseGlobals.h"
#include "XrayQFileDownloader.h"
#include "XrayQFileUtil.h"
#include "XrayXmlSettings.h"

#include <QApplication>
#include <QProcess>
#include <QDesktopWidget>
#include <QDesktopServices>
#include <QStatusBar>
#include <QMenu>
#include <QStyle>
#include <QActionGroup>
#include <QWidgetAction>
#include <QTimer>
#include <QDir>
#include <QDebug>
#include <QMessageBox>
#include <QStyleFactory>
#include <QLibraryInfo>

XRAYLAB_USING_NAMESPACE

XrayMainWindow::XrayMainWindow(QWidget* parent) :
	XrayFramelessWindowWin32(QApplication::applicationName(), parent),
	p_mainWindow(new XrayPaintMainWindow)
{
	setObjectName("AppMainWindow");

	// set window icon as the application icon.
	setWindowIcon(QApplication::windowIcon());

	// load language translation files.
	loadLanguage(XrayGlobal::getCurrentAppLanguage());

	// create top right drop down menu.
	createMenu();

	// setup the license management for checking the license at startup.
	//setupLicenseManager();

	// set the central widget.
	addCentralWidget(p_mainWindow);

	// apply theme style at start, by default Dark style is enabled.
	setThemeStyle(p_settings->value("Window/ThemeStyle", "Dark").toString());

	// restore paint module settings
	p_mainWindow->readSettings(*p_settings);

	// restore window settings
	restoreSettings();

	// check for update at startup.
	p_updater = new XrayCheckForUpdate(QUrl("https://xray-lab.com/xviewer/download/files/xviewer_updater.xml"));
}
XrayMainWindow::~XrayMainWindow()
{
}
void XrayMainWindow::closeEvent(QCloseEvent* _event)
{
	_event->ignore();
	if (QMessageBox::Yes == QMessageBox(QMessageBox::Question, windowTitle(), QApplication::translate("XrayMainWindow", "Are you sure you want to quit?"), QMessageBox::Yes | QMessageBox::No).exec())	// use this as parent to apply the style-sheet of this window.
	{
		//p_license_timer->deleteLater();
		//p_licenseManager->close();
		//p_licenseManager->deleteLater();
		p_updater->close();	// close the updater window which is not a child of main window.
		p_updater->deleteLater();

		p_settings->setValue("Window/ThemeStyle", XrayGlobal::getCurrentThemeStyle());

		p_mainWindow->writeSettings(*p_settings);

		saveSettings();

		QApplication::closeAllWindows();
		QApplication::exit();
	}
}

QMenu* XrayMainWindow::createMenu()
{
	auto menu = new QMenu;

	//p_actionLicense = new QAction(QIcon(":/res/images/license_blue_icon.png"), QApplication::translate("XrayMainWindow", "Register Product..."), this);
	//connect(p_actionLicense, &QAction::triggered, [this]()
	//{
	//	if (p_licenseManager->isHidden())
	//		p_licenseManager->show();
	//});
	//menu->addAction(p_actionLicense);

	p_actionUpdate = new QAction(QIcon(":/res/images/refresh_blue_icon.png"), QApplication::translate("XrayMainWindow", "Check for Updates"), this);
	connect(p_actionUpdate, &QAction::triggered, [this]() { p_updater->start(); });
	menu->addAction(p_actionUpdate);
	menu->addSeparator();

	p_actionViewHelp = new QAction(QIcon(":/res/images/help_blue_icon.png"), QApplication::translate("XrayMainWindow", "View Help"), this);
	connect(p_actionViewHelp, &QAction::triggered, [this]() { QDesktopServices::openUrl(QUrl::fromLocalFile(QApplication::applicationDirPath() + "/docs/XPaintPro_" + XrayGlobal::getCurrentAppLanguage() + ".pdf")); });
	menu->addAction(p_actionViewHelp);

	p_actionWebsite = new QAction(QIcon(":/res/images/website_blue_icon.png"), QApplication::translate("XrayMainWindow", "Website"), this);
	connect(p_actionWebsite, &QAction::triggered, [this]() { QDesktopServices::openUrl(QUrl("https://xray-lab.com/")); });
	menu->addAction(p_actionWebsite);
	menu->addSeparator();

	p_actionAbout = new QAction(QIcon(":/res/images/about_blue_icon.png"), QApplication::translate("XrayMainWindow", "About"), this);
	connect(p_actionAbout, &QAction::triggered, [this]()
	{
		XrayAboutDialog dialog;
		dialog.setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint);
		dialog.setWindowIcon(QApplication::windowIcon());
		dialog.setFixedSize(650, 480);

		dialog.setImage(QPixmap(QString::fromUtf8(":/res/images/XrayLab_Logo-green-white_SloganEN_RGB.png")));
#ifdef _WIN64
		dialog.setTitle(QApplication::applicationName() + " (64 bit)");
#else
		dialog.setTitle(QApplication::applicationName() + " (32 bit)");
#endif
		dialog.setVersion(QApplication::applicationVersion());
		dialog.setReleaseDate(XrayQApplication::applicationBuildDate());
		dialog.setModule(XrayLicenseGlobals::getModules());
		if (XrayLicenseGlobals::isForCommercialUse())
			dialog.setLicenseType(QApplication::translate("XrayMainWindow", "Licensed for commercial use <a href=\"https://xray-lab.com/us/eula-english\">(License terms)</a>"));
		else
			dialog.setLicenseType(QApplication::translate("XrayMainWindow", "Unlicensed for commercial use <a href=\"https://xray-lab.com/us/eula-english\">(License terms)</a>"));
		dialog.setCopyRight("2020 XRAY-LAB GmbH & Co. KG");
		dialog.setWebsite("<html><head/><body><p><a href=\"https://xray-lab.com\"><span style=\" color:#55aaff;\">www.xray-lab.com</span></a></span></a></p></body></html>");

		dialog.setStyleSheet(QString::fromUtf8("QDialog { background: rgb(70, 70, 70); border:1px solid rgb(21, 142, 21); } QLabel { color: rgb(240, 240, 240); }"));
		dialog.exec();
	});
	menu->addAction(p_actionAbout);

	menu->addSeparator();
	auto langGroup = new QActionGroup(this);
	langGroup->setExclusive(true);
	p_actionEnglish = new QAction(QApplication::translate("XrayMainWindow", "English"), this);
	langGroup->addAction(p_actionEnglish);
	p_actionGerman = new QAction(QApplication::translate("XrayMainWindow", "German"), this);
	langGroup->addAction(p_actionGerman);
	connect(langGroup, &QActionGroup::triggered, this, &XrayMainWindow::slotLanguageChanged);
	auto language = menu->addMenu(QIcon(":/res/images/language_blue_icon.png"), QApplication::translate("XrayMainWindow", "Language"));
	foreach(QAction* a, langGroup->actions()) language->addAction(a);

	menu->addSeparator();
	auto styleMenu = menu->addMenu(QIcon(":/res/images/theme_blue_icon.png"), QApplication::translate("XrayMainWindow", "Style"));
	auto p_actionDarkStyle = new QAction(QIcon(""), QApplication::translate("XrayMainWindow", "Dark"), this);
	connect(p_actionDarkStyle, &QAction::triggered, [this]() { setThemeStyle("Dark"); });
	styleMenu->addAction(p_actionDarkStyle);
	auto p_actionLightStyle = new QAction(QIcon(""), QApplication::translate("XrayMainWindow", "Light"), this);
	connect(p_actionLightStyle, &QAction::triggered, [this]() { setThemeStyle("Light"); });
	styleMenu->addAction(p_actionLightStyle);

	menuButton()->setMenu(menu);
	menuButton()->setVisible(true);
	setMenuBar(p_mainWindow->menuBar());
	p_menu = menu;

	return menu;
}

void XrayMainWindow::openProject(const QString& fileName)
{
	p_mainWindow->openProject(fileName);
}
void XrayMainWindow::slotLanguageChanged(QAction* action)
{
	if (!action)
		return;

	QString currentLang;
	if (action->text() == "German" || action->text() == "Deutsch")
		currentLang = "de";
	else
		currentLang = "en";

	if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, windowTitle(), QApplication::translate("XrayMainWindow", "Please restart the application to see the change."), QMessageBox::Yes | QMessageBox::No).exec())
	{
		XrayGlobal::setCurrentAppLanguage(currentLang);

		p_mainWindow->writeSettings(*p_settings);
		saveSettings();

		qApp->quit();
		QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
	}
}
void XrayMainWindow::loadLanguage(const QString& _language)
{
	const auto filename = ":/language/" + QString("xpaintpro_%1.qm").arg(_language);

	QApplication::removeTranslator(&m_appTranslator);	// remove the old translator first
	if (m_appTranslator.load(filename))
	{
		QApplication::installTranslator(&m_appTranslator);
		xAppInfo("Loading app with '{}' language!", _language);
	}
	else
	{
		xAppInfo("Couldn't load {}", filename);
	}
}

void XrayMainWindow::setThemeStyle(const QString& _style)
{
	if (_style == "Dark")
	{
		qApp->setStyle(new XrayQDarkStyle(QStyleFactory::create(QStringLiteral("Fusion"))));
		setDarkThemeEnabled(true);
		p_mainWindow->statusBar()->setStyleSheet("QStatusBar { color: rgb(170, 170, 170); } QLabel { color: rgb(170, 170, 170); }");
	}
	else if (_style == "Light")
	{
		qApp->setStyle(QStyleFactory::create(QStringLiteral("Fusion")));
		qApp->setStyleSheet(QString());
		setDarkThemeEnabled(false);
		p_mainWindow->statusBar()->setStyleSheet("QStatusBar { color: rgb(0, 0, 0); } QLabel { color: rgb(0, 0, 0); }");
	}

	p_settings->setValue("Window/ThemeStyle", _style);
	XrayGlobal::setCurrentThemeStyle(_style);
	xAppInfo("Loading app with '{}' style!", _style);
}

void XrayMainWindow::setupLicenseManager()
{
	p_licenseManager = new XrayLicenseFormWidget("XLicense Manager", "request@xray-lab.com", "2020 by XRAY-LAB GmbH &amp; Co. Kg", "XRAY");
	p_licenseManager->setWindowModality(Qt::WindowModality::ApplicationModal);
	connect(p_licenseManager, &XrayLicenseFormWidget::decodingFinished, [this]()	// must check whenever the new license file is specified.
	{
		p_mainWindow->enableDemoVersion(XrayLicenseGlobals::getModules(), XrayLicenseGlobals::isDemoVersion());
	});

	p_license_timer = new QTimer(this);
	p_license_timer->setInterval(1000 * 60 * 60 * 1);	// 1 hour
	connect(p_license_timer, &QTimer::timeout, this, [this]()	// must check the license after everyone hour.
	{
		p_mainWindow->enableDemoVersion(XrayLicenseGlobals::getModules(), XrayLicenseGlobals::isDemoVersion());
	});
	p_license_timer->start();

	// must check at the start of the application.
	p_mainWindow->enableDemoVersion(XrayLicenseGlobals::getModules(), XrayLicenseGlobals::isDemoVersion());
}
