<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>XrayMainWindow</name>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="94"/>
        <source>Are you sure you want to quit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="117"/>
        <source>Register Product...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="125"/>
        <source>Check for Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="130"/>
        <source>View Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="134"/>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="139"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="157"/>
        <source>Licensed for commercial use &lt;a href=&quot;https://xray-lab.com/us/eula-english&quot;&gt;(License terms)&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="159"/>
        <source>Unlicensed for commercial use &lt;a href=&quot;https://xray-lab.com/us/eula-english&quot;&gt;(License terms)&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="171"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="173"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="176"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="180"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="181"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="184"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="211"/>
        <source>Please restart the application to see the change.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
