<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>XrayMainWindow</name>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="94"/>
        <source>Are you sure you want to quit?</source>
        <translation>Sind Sie scher, dass Sie die Anwendung beenden wollen?</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="117"/>
        <source>Register Product...</source>
        <translation>Produkt registrieren...</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="125"/>
        <source>Check for Updates</source>
        <translation>Nach Update suchen</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="130"/>
        <source>View Help</source>
        <translation>Hilfe anzeigen</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="134"/>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="139"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>Licensed for commercial use &lt;a href=&quot;https://xray-lab.com&quot;&gt;(License terms)&lt;/a&gt;</source>
        <translation type="vanished">Lizensiert für die kommerzielle Benutzung &lt;a href=&quot;https://xray-lab.com&quot;&gt;(Lizenzbedingungen)&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Unlicensed for commercial use &lt;a href=&quot;https://xray-lab.com&quot;&gt;(License terms)&lt;/a&gt;</source>
        <translation type="vanished">Nicht lizensiert für die kommerzielle Benutzung &lt;a href=&quot;https://xray-lab.com&quot;&gt;(Lizenzbedingungen)&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="157"/>
        <source>Licensed for commercial use &lt;a href=&quot;https://xray-lab.com/us/eula-english&quot;&gt;(License terms)&lt;/a&gt;</source>
        <translation>Lizensiert für kommerzielle Nutzung &lt;a href=&quot;https://xray-lab.com/us/eula-english&quot;&gt;(License terms)&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="159"/>
        <source>Unlicensed for commercial use &lt;a href=&quot;https://xray-lab.com/us/eula-english&quot;&gt;(License terms)&lt;/a&gt;</source>
        <translation>Nicht für kommerzielle NUtzung lizensiert &lt;a href=&quot;https://xray-lab.com/us/eula-english&quot;&gt;(License terms)&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="171"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="173"/>
        <source>German</source>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="176"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="180"/>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="181"/>
        <source>Dark</source>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="184"/>
        <source>Light</source>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../../app/XrayMainWindow.cpp" line="211"/>
        <source>Please restart the application to see the change.</source>
        <translation>Bitte starte das Programm neu, um die Änderung zu sehen.</translation>
    </message>
</context>
</TS>
