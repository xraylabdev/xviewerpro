/****************************************************************************
**
** Copyright (C) 2019 XRay-Lab GmbH & Co. KG
** Contact: https://www.xray-lab.com/
**
** This file is part of the XrayWidgets module of the Xray Toolkit.
**
** $XRAY_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial X-Ray Lab licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The X-Ray Lab Company. For licensing terms
** and conditions see https://www.xray-lab.com/terms-conditions. For further
** information use the contact form at https://www.xray-lab.com/contact-us.
** $XRAY_END_LICENSE$
**
** created:		2019/02/06
** filename: 	XrayMainWindow.h
** file base:	XrayMainWindow
** file ext:	h
** developer:	Dr. Furqan Ullah (ful@xray-lab.com)
** purpose:		class that provides the main window of the application.
****************************************************************************/

#pragma once
#ifndef XrayMainWindow_h__
#define XrayMainWindow_h__

#include "XrayQtApiExport.h"
#include "XrayFramelessWindowWin32.h"
#include "XrayPaintMainWindow.h"
#include "XrayLicenseFormWidget.h"
#include "XrayCheckForUpdate.h"

#include <QMenu>
#include <QAction>
#include <QTranslator>

XRAYLAB_BEGIN_NAMESPACE

class XRAY_QT_API_EXPORT XrayMainWindow : public XrayFramelessWindowWin32
{
	Q_OBJECT

public:
	XrayMainWindow(QWidget* parent = nullptr);
	~XrayMainWindow();

protected:
	void closeEvent(QCloseEvent* _event) override;

public slots:
	/*!
		Description:
		Function that loads a project into this main widget.
	*/	
	void openProject(const QString& fileName);

	/*!
		Description:
		Signal that is triggered when the lanuage is changed.
		/param action - pointer to menu item action of the language.
	*/
	void slotLanguageChanged(QAction* action);	// this slot is called by the language menu actions

private:
	/*!
		Description:
		Function to create a top right menu.
	*/
	QMenu* createMenu();

	/*!
		Description:
		Function to load the language into the translator.
	*/
	void loadLanguage(const QString& _language);	// loads a language by the given language shortcur (e.g. de, en)

	/*!
		Description:
		Function that sets the theme style of the application.
		/param _style - current Dark and Light themes are supported.
	*/
	void setThemeStyle(const QString& _style);

	/*!
		Description:
		Function to setup the license manager.
	*/
	void setupLicenseManager();

	QMenu* p_menu;
	QAction* p_actionLicense;
	QAction* p_actionUpdate;
	QAction* p_actionViewHelp;
	QAction* p_actionWebsite;
	QAction* p_actionAbout;
	QAction* p_actionEnglish;
	QAction* p_actionGerman;

	XrayPaintMainWindow* p_mainWindow;
	XrayLicenseFormWidget* p_licenseManager;
	QTimer* p_license_timer;
	XrayCheckForUpdate* p_updater;

	QTranslator m_appTranslator;	// contains the translations for this application
};

XRAYLAB_END_NAMESPACE

#endif // XrayMainWindow_h__                                                                                                                                                                                                                                                                                   
