/********************************************************************************
** Form generated from reading UI file 'XrayFlashFilter.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_XRAYFLASHFILTER_H
#define UI_XRAYFLASHFILTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_XrayFlashFilter
{
public:
    QGridLayout *gridLayout_3;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *label;
    QSlider *deltaSlider;
    QSpinBox *deltaSpinBox;
    QLabel *label_2;
    QSlider *sigmaSlider;
    QSpinBox *sigmaSpinBox;
    QLabel *label_3;
    QSlider *clippingSlider;
    QSpinBox *clippingSpinBox;
    QLabel *label_4;
    QSlider *exposureSlider;
    QSpinBox *exposureSpinBox;
    QLabel *label_5;
    QSlider *gammaSlider;
    QSpinBox *gammaSpinBox;
    QLabel *label_6;
    QCheckBox *optimizeCheckBox;
    QHBoxLayout *horizontalLayout;
    QPushButton *resetBtn;
    QPushButton *previewBtn;
    QPushButton *applyBtn;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_7;
    QLineEdit *readLineEdit;
    QPushButton *readBrowse;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_8;
    QLineEdit *writeLineEdit;
    QPushButton *writeBrowse;
    QPushButton *processBtn;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *XrayFlashFilter)
    {
        if (XrayFlashFilter->objectName().isEmpty())
            XrayFlashFilter->setObjectName(QString::fromUtf8("XrayFlashFilter"));
        XrayFlashFilter->resize(274, 510);
        gridLayout_3 = new QGridLayout(XrayFlashFilter);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(XrayFlashFilter);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        deltaSlider = new QSlider(groupBox);
        deltaSlider->setObjectName(QString::fromUtf8("deltaSlider"));
        deltaSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(deltaSlider, 0, 1, 1, 1);

        deltaSpinBox = new QSpinBox(groupBox);
        deltaSpinBox->setObjectName(QString::fromUtf8("deltaSpinBox"));

        gridLayout->addWidget(deltaSpinBox, 0, 2, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        sigmaSlider = new QSlider(groupBox);
        sigmaSlider->setObjectName(QString::fromUtf8("sigmaSlider"));
        sigmaSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(sigmaSlider, 1, 1, 1, 1);

        sigmaSpinBox = new QSpinBox(groupBox);
        sigmaSpinBox->setObjectName(QString::fromUtf8("sigmaSpinBox"));

        gridLayout->addWidget(sigmaSpinBox, 1, 2, 1, 1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        clippingSlider = new QSlider(groupBox);
        clippingSlider->setObjectName(QString::fromUtf8("clippingSlider"));
        clippingSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(clippingSlider, 2, 1, 1, 1);

        clippingSpinBox = new QSpinBox(groupBox);
        clippingSpinBox->setObjectName(QString::fromUtf8("clippingSpinBox"));

        gridLayout->addWidget(clippingSpinBox, 2, 2, 1, 1);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        exposureSlider = new QSlider(groupBox);
        exposureSlider->setObjectName(QString::fromUtf8("exposureSlider"));
        exposureSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(exposureSlider, 3, 1, 1, 1);

        exposureSpinBox = new QSpinBox(groupBox);
        exposureSpinBox->setObjectName(QString::fromUtf8("exposureSpinBox"));

        gridLayout->addWidget(exposureSpinBox, 3, 2, 1, 1);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 4, 0, 1, 1);

        gammaSlider = new QSlider(groupBox);
        gammaSlider->setObjectName(QString::fromUtf8("gammaSlider"));
        gammaSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(gammaSlider, 4, 1, 1, 1);

        gammaSpinBox = new QSpinBox(groupBox);
        gammaSpinBox->setObjectName(QString::fromUtf8("gammaSpinBox"));

        gridLayout->addWidget(gammaSpinBox, 4, 2, 1, 1);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 5, 0, 1, 1);

        optimizeCheckBox = new QCheckBox(groupBox);
        optimizeCheckBox->setObjectName(QString::fromUtf8("optimizeCheckBox"));

        gridLayout->addWidget(optimizeCheckBox, 5, 1, 1, 1);


        verticalLayout->addWidget(groupBox);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        resetBtn = new QPushButton(XrayFlashFilter);
        resetBtn->setObjectName(QString::fromUtf8("resetBtn"));

        horizontalLayout->addWidget(resetBtn);

        previewBtn = new QPushButton(XrayFlashFilter);
        previewBtn->setObjectName(QString::fromUtf8("previewBtn"));

        horizontalLayout->addWidget(previewBtn);

        applyBtn = new QPushButton(XrayFlashFilter);
        applyBtn->setObjectName(QString::fromUtf8("applyBtn"));

        horizontalLayout->addWidget(applyBtn);


        verticalLayout->addLayout(horizontalLayout);

        groupBox_2 = new QGroupBox(XrayFlashFilter);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_7 = new QLabel(groupBox_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_2->addWidget(label_7);

        readLineEdit = new QLineEdit(groupBox_2);
        readLineEdit->setObjectName(QString::fromUtf8("readLineEdit"));

        horizontalLayout_2->addWidget(readLineEdit);

        readBrowse = new QPushButton(groupBox_2);
        readBrowse->setObjectName(QString::fromUtf8("readBrowse"));

        horizontalLayout_2->addWidget(readBrowse);


        gridLayout_2->addLayout(horizontalLayout_2, 0, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        horizontalLayout_3->addWidget(label_8);

        writeLineEdit = new QLineEdit(groupBox_2);
        writeLineEdit->setObjectName(QString::fromUtf8("writeLineEdit"));

        horizontalLayout_3->addWidget(writeLineEdit);

        writeBrowse = new QPushButton(groupBox_2);
        writeBrowse->setObjectName(QString::fromUtf8("writeBrowse"));

        horizontalLayout_3->addWidget(writeBrowse);


        gridLayout_2->addLayout(horizontalLayout_3, 1, 0, 1, 1);

        processBtn = new QPushButton(groupBox_2);
        processBtn->setObjectName(QString::fromUtf8("processBtn"));

        gridLayout_2->addWidget(processBtn, 2, 0, 1, 1);


        verticalLayout->addWidget(groupBox_2);


        gridLayout_3->addLayout(verticalLayout, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 121, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer, 1, 0, 1, 1);


        retranslateUi(XrayFlashFilter);
        QObject::connect(deltaSlider, SIGNAL(valueChanged(int)), deltaSpinBox, SLOT(setValue(int)));
        QObject::connect(deltaSpinBox, SIGNAL(valueChanged(int)), deltaSlider, SLOT(setValue(int)));
        QObject::connect(sigmaSlider, SIGNAL(valueChanged(int)), sigmaSpinBox, SLOT(setValue(int)));
        QObject::connect(sigmaSpinBox, SIGNAL(valueChanged(int)), sigmaSlider, SLOT(setValue(int)));
        QObject::connect(clippingSlider, SIGNAL(valueChanged(int)), clippingSpinBox, SLOT(setValue(int)));
        QObject::connect(clippingSpinBox, SIGNAL(valueChanged(int)), clippingSlider, SLOT(setValue(int)));
        QObject::connect(exposureSlider, SIGNAL(valueChanged(int)), exposureSpinBox, SLOT(setValue(int)));
        QObject::connect(exposureSpinBox, SIGNAL(valueChanged(int)), exposureSlider, SLOT(setValue(int)));
        QObject::connect(gammaSlider, SIGNAL(valueChanged(int)), gammaSpinBox, SLOT(setValue(int)));
        QObject::connect(gammaSpinBox, SIGNAL(valueChanged(int)), gammaSlider, SLOT(setValue(int)));

        QMetaObject::connectSlotsByName(XrayFlashFilter);
    } // setupUi

    void retranslateUi(QWidget *XrayFlashFilter)
    {
        XrayFlashFilter->setWindowTitle(QApplication::translate("XrayFlashFilter", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("XrayFlashFilter", "Parameters", nullptr));
        label->setText(QApplication::translate("XrayFlashFilter", "Delta", nullptr));
        label_2->setText(QApplication::translate("XrayFlashFilter", "Sigma", nullptr));
        label_3->setText(QApplication::translate("XrayFlashFilter", "Clipping", nullptr));
        label_4->setText(QApplication::translate("XrayFlashFilter", "Exposure", nullptr));
        label_5->setText(QApplication::translate("XrayFlashFilter", "Gamma", nullptr));
        label_6->setText(QApplication::translate("XrayFlashFilter", "Optimize", nullptr));
        optimizeCheckBox->setText(QString());
        resetBtn->setText(QApplication::translate("XrayFlashFilter", "Reset", nullptr));
        previewBtn->setText(QApplication::translate("XrayFlashFilter", "Preview", nullptr));
        applyBtn->setText(QApplication::translate("XrayFlashFilter", "Apply", nullptr));
        groupBox_2->setTitle(QApplication::translate("XrayFlashFilter", "Directories", nullptr));
        label_7->setText(QApplication::translate("XrayFlashFilter", "Read", nullptr));
        readBrowse->setText(QApplication::translate("XrayFlashFilter", "Browse...", nullptr));
        label_8->setText(QApplication::translate("XrayFlashFilter", "Write", nullptr));
        writeBrowse->setText(QApplication::translate("XrayFlashFilter", "Browse...", nullptr));
        processBtn->setText(QApplication::translate("XrayFlashFilter", "Process", nullptr));
    } // retranslateUi

};

namespace Ui {
    class XrayFlashFilter: public Ui_XrayFlashFilter {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_XRAYFLASHFILTER_H
