Add following to PostBuildEvent:

1. to run application at the end of the setup.
2. in case of 4 segments versioning.

cscript.exe "$(ProjectDir)EnableLaunchApplication.js" "$(BuiltOuputPath)"
"$(ProjectDir)wirunsql.vbs" "$(BuiltOuputPath)" "UPDATE `Property` SET `Property`.`Value` ='2.6.2.4' WHERE `Property`.`Property` ='ProductVersion'"