How to add a Uninstall option in Visual Studio Setup project without writing code
Posted on September 5, 2008Categories DevelopmentTags .NET Views: 104139
Using Visual Studio 2005/2008, you don�t need to write any code to add a uninstall option for a Setup project (Yes I know some people can write code to do it)

1) In the Setup Project �> File System windows �> Right Click �File System on Target machine� �> add a Special Folder, select System Folder;

2) Into this system folder Add a file. Browse for msiexec.exe from local System32 folder and add it. Override default properties of this file as follows:

Read Also
A DCOM error occurred trying to contact the remote computer. Access is denied.
Retrieving the COM class factory for component with CLSID
About Memory Management in .NET
Condition:=Not Installed (make sure you put �Not Installed� exactly like that, same case and everything), 
Permanent:=True, 
System:=True, 
Transitive:=True, 
Vital:=False.

3) Create a new shortcut under the �Users Program Menu�, Set Target to the System Folder which you created in the step 1. and point it�s at the msiexec.exe. Rename the shortcut to �Uninstall Your Application�. Set the Arguments property to /x{space}[ProductCode].

5) Build the project, ignore warning about the fact that msiexec should be excluded, DONT exclude it or the setup project wont build.

The �Not Installed� condition and Permananet:=True ensure that the msiexec.exe is only placed into the system folder as part of the install IF it doesn�t aready exist, and it is not removed on an uninstall � therefore it;s pretty safe to ignore that warning and just go for it.

(Based on the description from SlapHead)