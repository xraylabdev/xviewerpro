var newSequence = 1501;

var msiOpenDatabaseModeTransact = 1;

var msiViewModifyReplace = 4

if (WScript.Arguments.Length != 1) 
{ 
    WScript.StdErr.WriteLine(WScript.ScriptName + " file"); 
    WScript.Quit(1); 
}

var filespec = WScript.Arguments(0); 
var installer = WScript.CreateObject("WindowsInstaller.Installer"); 
var database = installer.OpenDatabase(filespec, msiOpenDatabaseModeTransact);

var sql 
var view 
var record

try 
{ 
    WScript.Echo("Updating the InstallExecuteSequence table...");

    sql = "SELECT `Action`, `Sequence` FROM `InstallExecuteSequence` WHERE `Action`='RemoveExistingProducts'"; 
    view = database.OpenView(sql); 
    view.Execute(); 
    record = view.Fetch(); 
        if (record.IntegerData(2) == newSequence) 
        throw "REP sequence doesn't match expected value - this database appears to have been already modified by this script!"; 
        if (record.IntegerData(2) != 6550) 
        throw "REP sequence doesn't match expected value - this database was either already modified by something else after build, or was not produced by Visual Studio 2010!"; 
    record.IntegerData(2) = newSequence; 
    view.Modify(msiViewModifyReplace, record); 
    view.Close();

    database.Commit(); 
} 
catch(e) 
{ 
    WScript.StdErr.WriteLine(e); 
    WScript.Quit(1); 
}