
TEMPLATE    = lib
TARGET      = XrayLicensingApi
VERSION		= 1.0.0
CONFIG 		+= staticlib
CONFIG 		+= c++14
QT       	+= core gui sql xml widgets printsupport svg network

DEFINES += QT_DEPRECATED_WARNINGS

isEmpty(PREFIX) {
    PREFIX = XrayLicensingApiLib
}

CONFIG(debug, debug|release) {

	MOC_DIR = XrayLicensingApi/moc/Debug/
	RCC_DIR = XrayLicensingApi/rcc/Debug/
	UI_DIR = XrayLicensingApi/include/ui/
	
	contains(QT_ARCH, i386) {
		OBJECTS_DIR = obj/x86/Debug/
		DESTDIR = XrayLicensingApi/lib/x86/Debug/
		LIB_TARGET = $$PREFIX/lib/x86/Debug/XrayLicensingApi.lib
		target.path = $$PREFIX/lib/x86/Debug/lib/
	} else {
		OBJECTS_DIR = obj/x64/Debug/
		DESTDIR = XrayLicensingApi/lib/x64/Debug/
		LIB_TARGET = $$PREFIX/lib/x64/Debug/XrayLicensingApi.lib
		target.path = $$PREFIX/lib/x64/Debug/lib/
	}
	
} else {

	MOC_DIR = XrayLicensingApi/moc/Release/
	RCC_DIR = XrayLicensingApi/rcc/Release/
	UI_DIR = XrayLicensingApi/include/ui/
	
	contains(QT_ARCH, i386) {
		OBJECTS_DIR = obj/x86/Release/
		DESTDIR = XrayLicensingApi/lib/x86/Release/
		LIB_TARGET = $$PREFIX/lib/x86/Release/XrayLicensingApi.lib
		target.path = $$PREFIX/lib/x86/Release/lib/
	} else {
		OBJECTS_DIR = obj/x64/Release/
		DESTDIR = XrayLicensingApi/lib/x64/Release/
		LIB_TARGET = $$PREFIX/lib/x64/Release/XrayLicensingApi.lib
		target.path = $$PREFIX/lib/x64/Release/lib/
	}
}

	
INCLUDEPATH +=	XrayLicensingApi/ \
				XrayLicensingApi/include/


SOURCES     += \
			   XrayLicensingApi/src/fvkBase64.cpp \  
			   XrayLicensingApi/src/fvkQSimpleCrypt.cpp \
			   XrayLicensingApi/src/fvkLicenseDecoderClient.cpp \
			   XrayLicensingApi/src/fvkLicenseEncoderClient.cpp \
			   XrayLicensingApi/src/fvkLicenseDecoderServer.cpp \
			   XrayLicensingApi/src/fvkLicenseEncoderServer.cpp \			   
			   XrayLicensingApi/src/fvkLicenseExecutor.cpp \
			   XrayLicensingApi/src/fvkLicenseManager.cpp \
			   XrayLicensingApi/src/fvkWinUtil.cpp \
			   XrayLicensingApi/src/tiny_aes.cpp

HEADERS     += \
			   XrayLicensingApi/include/fvkBase64.h \   
			   XrayLicensingApi/include/fvkQSimpleCrypt.h \
			   XrayLicensingApi/include/fvkLicenseDecoderClient.h \
			   XrayLicensingApi/include/fvkLicenseEncoderClient.h \
			   XrayLicensingApi/include/fvkLicenseDecoderServer.h \
			   XrayLicensingApi/include/fvkLicenseEncoderServer.h \			   
			   XrayLicensingApi/include/fvkLicenseExecutor.h \
			   XrayLicensingApi/include/fvkLicenseManager.h \
			   XrayLicensingApi/include/fvkWinUtil.h \
			   XrayLicensingApi/include/fvkExport.h \
			   XrayLicensingApi/include/tiny_aes.h


FORMS       += \



RESOURCES   += \

			   
TRANSLATIONS += \


headers.path = $$PREFIX/include
headers.files = $$HEADERS

INSTALLS += target headers