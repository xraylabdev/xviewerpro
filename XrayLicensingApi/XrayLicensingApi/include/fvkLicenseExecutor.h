#pragma once
#ifndef fvkLicenseExecutor_h__
#define fvkLicenseExecutor_h__

/*********************************************************************************
created:	2018/04/03   11:36PM
filename: 	fvkLicenseExecutor.cpp
file base:	fvkLicenseExecutor
file ext:	cpp
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class to execute the PRO version by checking the current hardware.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2018 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkLicenseManager.h"
#include <string>
#include <vector>

namespace R3D
{

class LICENSE_MANAGER_EXPORT fvkLicenseExecutor : public fvkLicenseManager
{
public:
	// Description:
	// Default constructor to initiate the default values.
	fvkLicenseExecutor();

	// Description:
	// Function to set the license key sent by the client for matching with the current system.
	void setClientKey(const std::string& _key) { m_client_key = _key; }
	// Description:
	// Function to get the license key sent by the client.
	auto getClientKey() const { return m_client_key; }

	// Description:
	// Function to set the command line arguments that will be send to the demo executable.
	void setCommandLineArguments(const std::string& _args) { m_args = _args; }
	// Description:
	// Function to get the command line arguments that will be send to the demo executable.
	auto getCommandLineArguments() const { return m_args; }

	// Description:
	// Function to set a mutex name for executing the client sided executable.
	// Used in execute() function.
	void setMutexName(const std::string& _b) { m_mutex_name = _b; }
	// Description:
	// Function to get a mutex name that was sent to the client sided executable.
	auto getMutexName() const { return m_mutex_name; }

	// Description:
	// Function to set demo executable name that should be opened if the current 
	// hardware matches with the provided key.
	void setExecutableName(const std::string& _name) { m_exe_name = _name; }
	// Description:
	// Function to get demo executable name.
	auto getExecutableName() const { return m_exe_name; }

	// Description:
	// Function that checks the assigned client key with the current hardware and 
	// if both are same then create a mutex and open the demo executable.
	// Return values:
	// -1 => couldn't create a mutex! something went wrong.
	//  0 => client key does not match with the current hardware.
	//  1 => everything works well and the demo application is opened as PRO version.
	int execute();

private:
	std::string m_client_key;
	std::string m_mutex_name;
	std::string m_exe_name;
	std::string m_args;
};

}

#endif // fvkLicenseExecutor_h__