#pragma once
#ifndef fvkLicenseEncoderServer_h__
#define fvkLicenseEncoderServer_h__

/*********************************************************************************
created:	2017/04/03   11:36PM
filename: 	fvkLicenseEncoderServer.h
file base:	fvkLicenseEncoderServer
file ext:	h
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class to encode the hardware information to the license key.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2017-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkLicenseManager.h"

namespace R3D
{

class LICENSE_MANAGER_EXPORT fvkLicenseEncoderServer : public fvkLicenseManager
{
public:
	// Description:
	// Default constructor to initiate the default values.
	fvkLicenseEncoderServer();

	// Description:
	// Function to register the client info in the license.
	// The input string must be in the same order as follows.
	// setAppInfo("client.company name.email.phone") => setClientInfo("furqan.XRAY-LAB.ful@xray-lab.com.+491732171885");

	void setLicenseId(const QString& _s);
	void setSerial(const QString& _s);
	void setExpireDate(const QString& _s);
	void setIssueDate(const QString& _s);
	void setLicenseType(const QString& _s);
	void setModules(const QString& _s);

	// Description:
	// Function that encodes the specified information into a license key.
	// Return value:
	// -2 => couldn't encrypt to cryptographic hash.
	// -1 => couldn't encrypt to Base64.
	//  0 => specified key is less than 7 character.
	//  1 => encrypted successfully.
	int encode();

	// Description:
	// Function to print out the values to the console.
	void print();

private:
	QString m_license_id;
	QString m_serial;
	QString m_expire_date;
	QString m_issue_date;
	QString m_license_type;
	QString m_modules;
};

}

#endif // fvkLicenseEncoderServer_h__