#pragma once
#ifndef fvkLicenseDecoder_h__
#define fvkLicenseDecoder_h__

/*********************************************************************************
created:	2017/04/03   11:36PM
filename: 	fvkLicenseDecoderClient.h
file base:	fvkLicenseDecoderClient
file ext:	h
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class to decode the license key.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2017-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkLicenseManager.h"

namespace R3D
{

class LICENSE_MANAGER_EXPORT fvkLicenseDecoderClient : public fvkLicenseManager
{
public:
	// Description:
	// Default constructor to initiate the default values.
	fvkLicenseDecoderClient();

	// Description:
	// Function to get the registered client info.
	// Returned string can be further split into 4 values.
	// client = getClientInfo().at(0).split('.')[0]
	// company_name = getClientInfo().at(0).split('.')[1]
	// email = getClientInfo().at(0).split('.')[2]
	// phone = getClientInfo().at(0).split('.')[3]
	QString getClientName() const;

	QString getOrderDate() const;

	QString getClientCompany() const;

	QString getClientAddress() const;

	QString getClientEmail() const;

	QString getClientPhone() const;

	QString getClientComments() const;


	QString getAppName() const;

	QString getAppVersion() const;

	QString getHardwareKey() const;

	// Description:
	// Function that decodes the license key.
	// Return value:
	// -2 => couldn't decrypt from cryptographic hash.
	// -1 => couldn't decrypt to Base64.
	//  0 => specified key is less than 7 character.
	//  1 => decrypted successfully.
	int decode();

	// Description:
	// Function to print out the values to the console.
	void print();

private:
	void parse(const QString& _keyText);

	QString m_client_name;
	QString m_order_date;
	QString m_client_company;
	QString m_client_address;
	QString m_client_email;
	QString m_client_phone;
	QString m_client_comments;

	QString m_app_name;
	QString m_app_version;
	
	QString m_hardware_key;
};

}

#endif // fvkLicenseDecoder_h__