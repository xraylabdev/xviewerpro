#pragma once
#ifndef fvkLicenseEncoder_h__
#define fvkLicenseEncoder_h__

/*********************************************************************************
created:	2017/04/03   11:36PM
filename: 	fvkLicenseEncoderClient.h
file base:	fvkLicenseEncoderClient
file ext:	h
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class to encode the hardware information to the license key.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2017-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkLicenseManager.h"

namespace R3D
{

class LICENSE_MANAGER_EXPORT fvkLicenseEncoderClient : public fvkLicenseManager
{
public:
	// Description:
	// Default constructor to initiate the default values.
	fvkLicenseEncoderClient();

	// Description:
	// Function to register the client info in the license.
	// The input string must be in the same order as follows.
	// setAppInfo("client.company name.email.phone") => setClientInfo("furqan.XRAY-LAB.ful@xray-lab.com.+491732171885");
	void setClientName(const QString& _info);

	void setOrderDate(const QString& _info);

	void setClientCompany(const QString& _info);

	void setClientAddress(const QString& _info);

	void setClientEmail(const QString& _info);

	void setClientPhone(const QString& _info);

	void setClientComments(const QString& _info);


	void setAppName(const QString& _info);

	void setAppVersion(const QString& _info);

	// Description:
	// Function that encodes the specified information into a license key.
	// Return value:
	// -2 => couldn't encrypt to cryptographic hash.
	// -1 => couldn't encrypt to Base64.
	//  0 => specified key is less than 7 character.
	//  1 => encrypted successfully.
	int encode();

	// Description:
	// Function to print out the values to the console.
	void print();

private:
	QString m_client_name;
	QString m_order_date;
	QString m_client_company;
	QString m_client_address;
	QString m_client_email;
	QString m_client_phone;
	QString m_client_comments;

	QString m_app_name;
	QString m_app_version;
};

}

#endif // fvkLicenseEncoder_h__