#pragma once
#ifndef Base64_h__
#define Base64_h__

/*********************************************************************************
created:	2018/04/02   11:36PM
filename: 	Base64.cpp
file base:	Base64
file ext:	cpp
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	Base64 algo to encode and decode a string.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2018 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include <string>

namespace R3D
{

class Base64
{
public:
	static bool encode(const std::string& _in, std::string* _out);

	static bool encode(const char* _input, std::size_t _input_length, char *out, std::size_t _out_length);

	static bool decode(const std::string& _in, std::string* _out);

	static bool decode(const char* _input, std::size_t _input_length, char* _out, std::size_t _out_length);

	static int decodedLength(const char* _in, std::size_t _in_length);

	static int decodedLength(const std::string& _in);

	static int encodedLength(std::size_t length);

	static int encodedLength(const std::string& _in);

	static void stripPadding(std::string* _in);

private:
	static void a3_to_a4(unsigned char* a4, unsigned char* a3);
	static void a4_to_a3(unsigned char* a3, unsigned char* a4);
	static unsigned char b64_lookup(unsigned char c);
};

}

#endif // Base64_h__