#pragma once
#ifndef Export_h__
#define Export_h__

/********************************************************************
created:	2018/04/02   11:36PM
filename: 	Export.h
file base:	Export
file ext:	h
author:		Furqan Ullah (Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	DLL and Lib export support

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2017-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#  if defined(LICENSE_MANAGER_DLL)
#    ifdef LICENSE_MANAGER_LIBRARY
#      define LICENSE_MANAGER_EXPORT	__declspec(dllexport)
#    else
#      define LICENSE_MANAGER_EXPORT	__declspec(dllimport)
#    endif /* LICENSE_MANAGER_LIBRARY */
#  else
#    define LICENSE_MANAGER_EXPORT
#  endif /* LICENSE_MANAGER_DLL */

#endif // Export_h__