#pragma once
#ifndef fvkLicenseManager_h__
#define fvkLicenseManager_h__

/*********************************************************************************
created:	2017/04/02   11:36PM
filename: 	fvkLicenseManager.h
file base:	fvkLicenseManager
file ext:	h
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class to manage the hardware license key.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2017-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkExport.h"
#include "fvkQSimpleCrypt.h"

#include <string>
#include <vector>

namespace R3D
{

enum HardwareIdentifier
{
	ProcessorName = 1,
	ProcessorId = 2,
	ProcessorSerialNumber = 8,
	BaseBoardProduct = 16,
	BaseBoardSerialNumber = 32,
	BaseBoardManufacturer = 64,
	DiskDriveSerialNumber = 128,
	DiskDriveCaption = 256
};

class LICENSE_MANAGER_EXPORT fvkLicenseManager
{
public:
	// Description:
	// Default constructor to initiate the default values.
	fvkLicenseManager();

	// Description:
	// Function to set hardware devices or identifiers to be considered for the license key.
	void setIdentifier(int _id);
	// Description:
	// Function to get hardware devices or identifiers those were considered for the license key.
	int getIdentifier() const;

	// Description:
	// Function to set an encryption key.
	void setKey(const quint64 _key);
	// Description:
	// Function to get an encryption key.
	quint64 getKey() const;
	// Description:
	// Function to get an encryption key.
	bool hasKey() const;

	// Description:
	// Function to get an encryption key.
	QString getKeyText() const;

	// Description:
	// Function to set the path of the license file including name and extension.
	void setInputFileLocation(const QString& _filepath);
	// Description:
	// Function to get the path of the license file including name and extension.
	QString getInputFileLocation() const;
	// Description:
	// Function to set the output path of the decoded license file including name and extension.
	void setOutputFileLocation(const QString& _filepath);
	// Description:
	// Function to get the output path of the decoded license file including name and extension.
	QString getOutputFileLocation() const;

	// Description:
	// Function that collects the hardware identifiers.
	std::string getDevices();

	// Description:
	// Function that writes the encrypted key to the disk.
	// It returns true in success.
	bool read();
	// Description:
	// Function that writes the encrypted key to the disk.
	// It returns true in success.
	bool write();

	// Description:
	// Function to encode the given text with the given key.
	static QString encodeText(const QString& _rawText, const QString& _key);
	// Description:
	// Function to decode the given text with the given key.
	static QString decodeText(const QString& _hexEncodedText, const QString& _key);

protected:
	int m_identifier;
	fvkQSimpleCrypt m_crypt;
	QString m_key_text;
	QString m_input_file;
	QString m_output_file;
};

}

#endif // fvkLicenseManager_h__