/*********************************************************************************
created:	2018/04/02   11:36PM
filename: 	Base64.cpp
file base:	Base64
file ext:	cpp
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	Base64 algo to encode and decode a string.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2018 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkBase64.h"

using namespace R3D;

const char kBase64Alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";

bool R3D::Base64::encode(const char* _input, std::size_t _input_length, char *out, std::size_t _out_length)
{
	int i = 0, j = 0;
	char *out_begin = out;
	unsigned char a3[3];
	unsigned char a4[4];

	std::size_t encoded_length = encodedLength(_input_length);

	if (_out_length < encoded_length) return false;

	while (_input_length--)
	{
		a3[i++] = *_input++;
		if (i == 3)
		{
			a3_to_a4(a4, a3);

			for (i = 0; i < 4; i++)
			{
				*out++ = kBase64Alphabet[a4[i]];
			}

			i = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 3; j++)
		{
			a3[j] = '\0';
		}

		a3_to_a4(a4, a3);

		for (j = 0; j < i + 1; j++)
		{
			*out++ = kBase64Alphabet[a4[j]];
		}

		while ((i++ < 3)) {
			*out++ = '=';
		}
	}

	return (out == (out_begin + encoded_length));
}

bool R3D::Base64::encode(const std::string& _in, std::string* _out)
{
	int i = 0, j = 0;
	size_t enc_len = 0;
	unsigned char a3[3];
	unsigned char a4[4];

	_out->resize(encodedLength(_in));

	std::size_t input_len = _in.size();
	std::string::const_iterator input = _in.begin();

	while (input_len--)
	{
		a3[i++] = *(input++);
		if (i == 3) {
			a3_to_a4(a4, a3);

			for (i = 0; i < 4; i++)
			{
				(*_out)[enc_len++] = kBase64Alphabet[a4[i]];
			}

			i = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 3; j++)
		{
			a3[j] = '\0';
		}

		a3_to_a4(a4, a3);

		for (j = 0; j < i + 1; j++)
		{
			(*_out)[enc_len++] = kBase64Alphabet[a4[j]];
		}

		while ((i++ < 3))
		{
			(*_out)[enc_len++] = '=';
		}
	}

	return (enc_len == _out->size());
}

bool R3D::Base64::decode(const char* _input, std::size_t _input_length, char* _out, std::size_t _out_length)
{
	int i = 0, j = 0;
	char *out_begin = _out;
	unsigned char a3[3];
	unsigned char a4[4];

	std::size_t decoded_length = decodedLength(_input, _input_length);

	if (_out_length < decoded_length) return false;

	while (_input_length--)
	{
		if (*_input == '=')
		{
			break;
		}

		a4[i++] = *(_input++);
		if (i == 4)
		{
			for (i = 0; i < 4; i++)
			{
				a4[i] = b64_lookup(a4[i]);
			}

			a4_to_a3(a3, a4);

			for (i = 0; i < 3; i++)
			{
				*_out++ = a3[i];
			}

			i = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 4; j++)
		{
			a4[j] = '\0';
		}

		for (j = 0; j < 4; j++)
		{
			a4[j] = b64_lookup(a4[j]);
		}

		a4_to_a3(a3, a4);

		for (j = 0; j < i - 1; j++)
		{
			*_out++ = a3[j];
		}
	}

	return (_out == (out_begin + decoded_length));
}

bool R3D::Base64::decode(const std::string& _in, std::string* _out)
{
	int i = 0, j = 0;
	std::size_t dec_len = 0;
	unsigned char a3[3];
	unsigned char a4[4];

	std::size_t input_len = _in.size();
	std::string::const_iterator input = _in.begin();

	_out->resize(decodedLength(_in));

	while (input_len--)
	{
		if (*input == '=')
		{
			break;
		}

		a4[i++] = *(input++);
		if (i == 4)
		{
			for (i = 0; i < 4; i++)
			{
				a4[i] = b64_lookup(a4[i]);
			}

			a4_to_a3(a3, a4);

			for (i = 0; i < 3; i++)
			{
				(*_out)[dec_len++] = a3[i];
			}

			i = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 4; j++)
		{
			a4[j] = '\0';
		}

		for (j = 0; j < 4; j++)
		{
			a4[j] = b64_lookup(a4[j]);
		}

		a4_to_a3(a3, a4);

		for (j = 0; j < i - 1; j++)
		{
			(*_out)[dec_len++] = a3[j];
		}
	}

	return (dec_len == _out->size());
}

int R3D::Base64::decodedLength(const std::string& _in)
{
	int numEq = 0;
	std::size_t n = _in.size();

	for (std::string::const_reverse_iterator it = _in.rbegin(); *it == '='; ++it)
	{
		++numEq;
	}

	return static_cast<int>(((6 * n) / 8) - numEq);
}

int R3D::Base64::decodedLength(const char* _in, std::size_t _in_length)
{
	int numEq = 0;

	const char *in_end = _in + _in_length;
	while (*--in_end == '=') ++numEq;

	return static_cast<int>(((6 * _in_length) / 8) - numEq);
}

int R3D::Base64::encodedLength(const std::string& _in)
{
	return encodedLength(_in.length());
}

int R3D::Base64::encodedLength(std::size_t length)
{
	return static_cast<int>((length + 2 - ((length + 2) % 3)) / 3 * 4);
}

void R3D::Base64::stripPadding(std::string* _in)
{
	while (!_in->empty() && *(_in->rbegin()) == '=') _in->resize(_in->size() - 1);
}

void R3D::Base64::a3_to_a4(unsigned char* a4, unsigned char* a3)
{
	a4[0] = (a3[0] & 0xfc) >> 2;
	a4[1] = ((a3[0] & 0x03) << 4) + ((a3[1] & 0xf0) >> 4);
	a4[2] = ((a3[1] & 0x0f) << 2) + ((a3[2] & 0xc0) >> 6);
	a4[3] = (a3[2] & 0x3f);
}

void R3D::Base64::a4_to_a3(unsigned char* a3, unsigned char* a4)
{
	a3[0] = (a4[0] << 2) + ((a4[1] & 0x30) >> 4);
	a3[1] = ((a4[1] & 0xf) << 4) + ((a4[2] & 0x3c) >> 2);
	a3[2] = ((a4[2] & 0x3) << 6) + a4[3];
}

unsigned char R3D::Base64::b64_lookup(unsigned char c)
{
	if (c >= 'A' && c <= 'Z') return c - 'A';
	if (c >= 'a' && c <= 'z') return c - 71;
	if (c >= '0' && c <= '9') return c + 4;
	if (c == '+') return 62;
	if (c == '/') return 63;
	return 255;
}
