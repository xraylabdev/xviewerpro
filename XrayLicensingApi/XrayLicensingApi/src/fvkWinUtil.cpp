/*********************************************************************************
created:	2017/02/25   01:58AM
filename: 	fvkWinUtil.h
file base:	fvkWinUtil
file ext:	h
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	This class contains some important Windows utility functions.

/**********************************************************************************
FL-ESSENTIALS (FLE) - FLTK Utility Widgets
Copyright (C) 2014-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkWinUtil.h"

#ifdef _WIN32

#define _WIN32_WINNT 0x0501 // needed for AttachConsole
#include <windows.h>
#include <wincon.h> 	// AttachConsole()
#include <direct.h>
#include <shlobj.h>
#include <Shellapi.h>	// SHELLEXECUTEINFO
#include <process.h>	// _getpid()
#include <stdio.h>
#include <lmcons.h>

#define _WIN32_DCOM
#include <comdef.h>
#include <Wbemidl.h>
#pragma comment(lib, "wbemuuid.lib")
#include <iphlpapi.h>
#pragma comment(lib, "IPHLPAPI.lib")

#endif // _WIN32
#include <iostream>
#include <sstream>
#include <sys/types.h> 	// required for stat.h
#include <sys/stat.h> 	// no clue why required -- man pages say so

using namespace R3D;

void fvkWinUtil::enableDosConsoleInReleaseMode()
{
#ifdef _WIN32
	AllocConsole();
	AttachConsole(GetCurrentProcessId());
	freopen("CON", "w", stdout);
	freopen("CON", "w", stderr);
#endif // _WIN32
}

bool fvkWinUtil::setWindowOption(const char* _window_title, Options _option)
{
#ifdef _WIN32
	HWND hWnd = FindWindowA(nullptr, _window_title);
	if (hWnd)
	{
		ShowWindow(hWnd, static_cast<int>(_option));
		ShowWindow(hWnd, static_cast<int>(_option));
		// for multi-threaded applications
		//ShowWindowAsync(hWnd, static_cast<int>(_option));
		//ShowWindowAsync(hWnd, static_cast<int>(_option));
		return true;
	}
#endif // _WIN32
	return false;
}

std::wstring fvkWinUtil::string_to_wstring(const std::string& _str)
{
	std::wstring temp;
#ifdef _WIN32
	int slength = static_cast<int>(_str.length() + 1);
	int len = MultiByteToWideChar(CP_ACP, 0, _str.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, _str.c_str(), slength, buf, len);
	temp = buf;
	delete[] buf;
#endif // _WIN32
	return temp;
}

std::string fvkWinUtil::wstring_to_string(const std::wstring& _wstr)
{
	std::string temp;
#ifdef _WIN32
	int slength = static_cast<int>(_wstr.length() + 1);
	int len = WideCharToMultiByte(CP_ACP, 0, _wstr.c_str(), slength, 0, 0, 0, 0);
	char* buf = new char[len];
	WideCharToMultiByte(CP_ACP, 0, _wstr.c_str(), slength, buf, len, 0, 0);
	temp = buf;
	delete[] buf;
#endif // _WIN32
	return temp;
}

#ifdef _UNICODE
static std::wstring m_wstring_file;
static std::wstring m_wstring_verb;
#else
static std::string m_wstring_file;
static std::string m_wstring_verb;
#endif // _UNICODE

void fvkWinUtil::shellExecute(const std::string& _filename, const std::string& _command)
{
	if (_filename.empty()) return;
#ifdef _WIN32
#ifdef _UNICODE
	m_wstring_file = string_to_wstring(_filename).c_str();
	m_wstring_verb = string_to_wstring(_command).c_str();
#else
	m_wstring_file = _filename;
	m_wstring_verb = _command;
#endif // _UNICODE

	SHELLEXECUTEINFO info = { 0 };
	info.cbSize = sizeof info;
	info.lpFile = m_wstring_file.c_str();
	info.nShow = SW_SHOW;
	info.fMask = SEE_MASK_INVOKEIDLIST;
	info.lpVerb = m_wstring_verb.c_str();
	ShellExecuteEx(&info);
#endif // _WIN32
}

bool fvkWinUtil::create_directory(const std::string& _dir_name)
{
#if defined(_WIN32)
	if (_mkdir(_dir_name.c_str()) == 0)
		return true;
#else 
	mode_t nMode = 0733; // UNIX style permissions
	if(mkdir(_dir_name.c_str(), nMode) == 0) // can be used on non-Windows
		return true;
#endif
	return false;
}

std::string fvkWinUtil::getSystemFolderPath(int _csidl)
{
#if defined(_WIN32)
	wchar_t folder[1024];
	HRESULT hr = SHGetFolderPathW(0, _csidl, 0, 0, folder);
	if (SUCCEEDED(hr))
	{
		char str[1024];
		wcstombs(str, folder, 1023);
		return str;
	}
#endif

	return "";
}
std::string fvkWinUtil::getSystemFolderPath(fvkWinUtil::SystemFolder _folder)
{
#if defined(_WIN32)
	if (_folder == fvkWinUtil::SystemFolder::Documents)
		return getSystemFolderPath(CSIDL_MYDOCUMENTS);
	else if (_folder == fvkWinUtil::SystemFolder::Pictures)
		return getSystemFolderPath(CSIDL_MYPICTURES);
	else if (_folder == fvkWinUtil::SystemFolder::Videos)
		return getSystemFolderPath(CSIDL_MYVIDEO);
	else if (_folder == fvkWinUtil::SystemFolder::Music)
		return getSystemFolderPath(CSIDL_MYMUSIC);
	else if (_folder == fvkWinUtil::SystemFolder::Desktop)
		return getSystemFolderPath(CSIDL_DESKTOP);
	else if (_folder == fvkWinUtil::SystemFolder::Windows)
		return getSystemFolderPath(CSIDL_WINDOWS);
	else if (_folder == fvkWinUtil::SystemFolder::ProgramFiles)
		return getSystemFolderPath(CSIDL_PROGRAM_FILES);
	else if (_folder == fvkWinUtil::SystemFolder::CommonAppData)
		return getSystemFolderPath(CSIDL_COMMON_APPDATA);
	else if (_folder == fvkWinUtil::SystemFolder::StartMenu)
		return getSystemFolderPath(CSIDL_STARTMENU);
#endif

	return "";
}

std::string fvkWinUtil::getCurrentDirectory()
{
	const unsigned long n = 1024;
#if defined(_WIN32)
		char cd[n];
#ifdef _UNICODE
			GetCurrentDirectoryA(n, cd);
#else
			GetCurrentDirectory(n, cd);
#endif
			return std::string(cd);
#else
return "";
	//char result[n];
	//ssize_t count = readlink("/proc/self/exe", result, n);
	//return std::string(result, (count > 0) ? count : 0);
#endif
}

std::string fvkWinUtil::getMsvcVersionString(int _msc_ver)
{
	std::string version;
	switch (_msc_ver)
	{
	case 1310:
		version = "MSVC++ 7.1 (Visual Studio 2003)";
		break;
	case 1400:
		version = "MSVC++ 8.0 (Visual Studio 2005)";
		break;
	case 1500:
		version = "MSVC++ 9.0 (Visual Studio 2008)";
		break;
	case 1600:
		version = "MSVC++ 10.0 (Visual Studio 2010)";
		break;
	case 1700:
		version = "MSVC++ 11.0 (Visual Studio 2012)";
		break;
	case 1800:
		version = "MSVC++ 12.0 (Visual Studio 2013)";
		break;
	case 1900:
		version = "MSVC++ 14.0 (Visual Studio 2015)";
		break;
	case 1910:
		version = "MSVC++ 14.1 (Visual Studio 2017)";
		break;
	case 1912:
		version = "MSVC++ 14.1 (Visual Studio 2017)";
		break;
	default:
		version = "unknown MSVC++ version";
	}
	return version;
}

/************************************************************************/
/* Make Window Always on Top                                            */
/************************************************************************/
#if defined(_WIN32)
static BOOL CALLBACK EnumWindowsAlwaysOnTop(HWND _window_handle, LPARAM _l_param)
{
	DWORD searchedProcessId = static_cast<DWORD>(_l_param);  // This is the process ID we search for (passed from BringToForeground as lParam)
	DWORD windowProcessId = 0;
	GetWindowThreadProcessId(_window_handle, &windowProcessId); // Get process ID of the window we just found
	if (searchedProcessId == windowProcessId)
	{
		SetWindowPos(_window_handle, HWND_TOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
		return 0;	// Stop enumerating windows
	}
	return 1;		// Continue enumerating
}
static BOOL CALLBACK EnumWindowsNotAlwaysOnTop(HWND _window_handle, LPARAM _l_param)
{
	unsigned long searchedProcessId = static_cast<unsigned long>(_l_param);  // This is the process ID we search for (passed from BringToForeground as lParam)
	unsigned long windowProcessId = 0;
	GetWindowThreadProcessId(_window_handle, &windowProcessId); // Get process ID of the window we just found
	if (searchedProcessId == windowProcessId)
	{
		SetWindowPos(_window_handle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
		return 0;	// Stop enumerating windows
	}
	return 1;		// Continue enumerating
}
#endif // _WIN32

void fvkWinUtil::makeWindowAlwaysOnTop(unsigned long _process_id, bool _isontop)
{
#if defined(_WIN32)
	if (_isontop)
		EnumWindows(&EnumWindowsAlwaysOnTop, (LPARAM)_process_id);
	else
		EnumWindows(&EnumWindowsNotAlwaysOnTop, (LPARAM)_process_id);
#endif // _WIN32
}

void fvkWinUtil::makeWindowAlwaysOnTop(bool _isontop)
{
#if defined(_WIN32)
	makeWindowAlwaysOnTop(_getpid(), _isontop);
#endif // _WIN32
}

void* fvkWinUtil::createMutex(const char* _name)
{
	void* h = nullptr;
#if defined(_WIN32)
#ifdef _UNICODE
	h = CreateMutex(nullptr, TRUE, string_to_wstring(_name).c_str());
#else
	h = CreateMutex(nullptr, TRUE, _name);
#endif // _UNICODE
	const unsigned long e = GetLastError();
	if (e == ERROR_ALREADY_EXISTS)
	{
		CloseHandle(h);
		return nullptr;
	}
	else if (e == ERROR_ACCESS_DENIED)
	{
		CloseHandle(h);
		return nullptr;
	}
#endif // _WIN32
	return h;
}

bool fvkWinUtil::mutexAlreadyExists(const char* _name)
{
	void* h = nullptr;
#if defined(_WIN32)
#ifdef _UNICODE
	h = CreateMutex(nullptr, TRUE, string_to_wstring(_name).c_str());
#else
	h = CreateMutex(nullptr, TRUE, _name);
#endif // _UNICODE
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		CloseHandle(h);
		return true;
	}
	CloseHandle(h);
	return false;
#endif // _WIN32
	return false;
}

/*-------------------------------------------------------------------------
https://support.microsoft.com/en-us/help/118626/how-to-determine-whether-a-thread-is-running-in-user-context-of-local
This function checks the token of the calling thread to see if the caller
belongs to the Administrators group.

Return Value:
TRUE if the caller is an administrator on the local machine.
Otherwise, FALSE.
--------------------------------------------------------------------------*/
int fvkWinUtil::isCurrentUserLocalAdministrator()
{
#if defined(_WIN32)

	BOOL   fReturn = FALSE;
	DWORD  dwStatus;
	DWORD  dwAccessMask;
	DWORD  dwAccessDesired;
	DWORD  dwACLSize;
	DWORD  dwStructureSize = sizeof(PRIVILEGE_SET);
	PACL   pACL = NULL;
	PSID   psidAdmin = NULL;

	HANDLE hToken = NULL;
	HANDLE hImpersonationToken = NULL;

	PRIVILEGE_SET   ps;
	GENERIC_MAPPING GenericMapping;

	PSECURITY_DESCRIPTOR     psdAdmin = NULL;
	SID_IDENTIFIER_AUTHORITY SystemSidAuthority = SECURITY_NT_AUTHORITY;

	const DWORD ACCESS_READ = 1;
	const DWORD ACCESS_WRITE = 2;

	__try
	{

		if (!OpenThreadToken(GetCurrentThread(), TOKEN_DUPLICATE | TOKEN_QUERY, TRUE, &hToken))
		{
			if (GetLastError() != ERROR_NO_TOKEN)
				__leave;

			if (!OpenProcessToken(GetCurrentProcess(), TOKEN_DUPLICATE | TOKEN_QUERY, &hToken))
				__leave;
		}

		if (!DuplicateToken(hToken, SecurityImpersonation, &hImpersonationToken))
			__leave;

		if (!AllocateAndInitializeSid(&SystemSidAuthority, 2,
			SECURITY_BUILTIN_DOMAIN_RID,
			DOMAIN_ALIAS_RID_ADMINS,
			0, 0, 0, 0, 0, 0, &psidAdmin))
			__leave;

		psdAdmin = LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH);
		if (psdAdmin == NULL)
			__leave;

		if (!InitializeSecurityDescriptor(psdAdmin, SECURITY_DESCRIPTOR_REVISION))
			__leave;

		// Compute size needed for the ACL.
		dwACLSize = sizeof(ACL) + sizeof(ACCESS_ALLOWED_ACE) + GetLengthSid(psidAdmin) - sizeof(DWORD);

		pACL = (PACL)LocalAlloc(LPTR, dwACLSize);
		if (pACL == NULL)
			__leave;

		if (!InitializeAcl(pACL, dwACLSize, ACL_REVISION2))
			__leave;

		dwAccessMask = ACCESS_READ | ACCESS_WRITE;

		if (!AddAccessAllowedAce(pACL, ACL_REVISION2, dwAccessMask, psidAdmin))
			__leave;

		if (!SetSecurityDescriptorDacl(psdAdmin, TRUE, pACL, FALSE))
			__leave;

		/*
		   AccessCheck validates a security descriptor somewhat; set the group
			 and owner so that enough of the security descriptor is filled out to
			 make AccessCheck happy.
		 */
		SetSecurityDescriptorGroup(psdAdmin, psidAdmin, FALSE);
		SetSecurityDescriptorOwner(psdAdmin, psidAdmin, FALSE);

		if (!IsValidSecurityDescriptor(psdAdmin))
			__leave;

		dwAccessDesired = ACCESS_READ;

		/*
		   Initialize GenericMapping structure even though you
		   do not use generic rights.
		*/
		GenericMapping.GenericRead = ACCESS_READ;
		GenericMapping.GenericWrite = ACCESS_WRITE;
		GenericMapping.GenericExecute = 0;
		GenericMapping.GenericAll = ACCESS_READ | ACCESS_WRITE;

		if (!AccessCheck(psdAdmin, hImpersonationToken, dwAccessDesired,
			&GenericMapping, &ps, &dwStructureSize, &dwStatus,
			&fReturn))
		{
			fReturn = FALSE;
			__leave;
		}
	}
	__finally
	{
		// Clean up.
		if (pACL) LocalFree(pACL);
		if (psdAdmin) LocalFree(psdAdmin);
		if (psidAdmin) FreeSid(psidAdmin);
		if (hImpersonationToken) CloseHandle(hImpersonationToken);
		if (hToken) CloseHandle(hToken);
	}

	return int(fReturn);
#endif // _WIN32

	return 0;
}

std::vector<std::string> fvkWinUtil::getDevices(const char* _device_name, const char* _device_value)
{
	std::vector<std::string> devices;

#if defined(_WIN32)

	auto devicename = string_to_wstring(_device_name);
	auto devicevalue = string_to_wstring(_device_value);

	if (devicename.empty() || devicevalue.empty())
		return devices;

	// Step 1: --------------------------------------------------
	// Initialize COM. ------------------------------------------

	auto hres = ::CoInitialize(nullptr);	// don't use CoInitializeEX
	// If you are running on NT 4.0 or higher you can use the following call instead to 
	// make the EXE free threaded. This means that calls come in on a random RPC thread.
	// auto hres = ::CoInitializeEx(nullptr, COINIT_MULTITHREADED);
	if (FAILED(hres))
	{
		if (hres != RPC_E_CHANGED_MODE)
		{
			std::cout << "Failed to initialize COM library. Error code = 0x" << std::hex << hres << std::endl;
			return devices;
		}
	}

	// Step 2: --------------------------------------------------
	// Set general COM security levels --------------------------
	// Note: If you are using Windows 2000, you need to specify -
	// the default authentication credentials for a user by using
	// a SOLE_AUTHENTICATION_LIST structure in the pAuthList ----
	// parameter of CoInitializeSecurity ------------------------

	hres = ::CoInitializeSecurity(
		nullptr,
		-1,                          // COM authentication
		nullptr,                        // Authentication services
		nullptr,                        // Reserved
		RPC_C_AUTHN_LEVEL_DEFAULT,   // Default authentication 
		RPC_C_IMP_LEVEL_IMPERSONATE, // Default Impersonation  
		nullptr,                        // Authentication info
		EOAC_NONE,                   // Additional capabilities 
		nullptr                         // Reserved
	);

	if (FAILED(hres) && !(hres == RPC_E_TOO_LATE))
	{
		std::cout << "Failed to initialize security. Error code = 0x" << std::hex << hres << std::endl;
		::CoUninitialize();
		return devices;
	}

	// Step 3: ---------------------------------------------------
	// Obtain the initial locator to WMI -------------------------

	IWbemLocator* pLoc = nullptr;

	hres = ::CoCreateInstance(
		CLSID_WbemLocator,
		0,
		CLSCTX_INPROC_SERVER,
		IID_IWbemLocator, reinterpret_cast<void**>(&pLoc));

	if (FAILED(hres))
	{
		std::cout << "Failed to create IWbemLocator object." << " Err code = 0x" << std::hex << hres << std::endl;
		pLoc->Release();
		::CoUninitialize();
		return devices;                 // Program has failed.
	}

	// Step 4: -----------------------------------------------------
	// Connect to WMI through the IWbemLocator::ConnectServer method

	IWbemServices* pSvc = nullptr;

	// Connect to the root\cimv2 namespace with
	// the current user and obtain pointer pSvc
	// to make IWbemServices calls.
	hres = pLoc->ConnectServer(
		/*&string_to_wstring("ROOT\\CIMV2")[0]*/_bstr_t(L"ROOT\\CIMV2"), // Object path of WMI namespace
		nullptr,                 // User name. NULL = current user
		nullptr,                 // User password. NULL = current
		0,						 // Locale. NULL indicates current
		0,						 // Security flags.
		0,                       // Authority (for example, Kerberos)
		0,                       // Context object 
		&pSvc                    // pointer to IWbemServices proxy
	);

	if (FAILED(hres))
	{
		std::cout << "Could not connect to the server. Error code = 0x" << std::hex << hres << std::endl;
		pLoc->Release();
		::CoUninitialize();
		return devices;
	}

	// Step 5: --------------------------------------------------
	// Set security levels on the proxy -------------------------
	hres = ::CoSetProxyBlanket(
		pSvc,                        // Indicates the proxy to set
		RPC_C_AUTHN_WINNT,           // RPC_C_AUTHN_xxx
		RPC_C_AUTHZ_NONE,            // RPC_C_AUTHZ_xxx
		nullptr,                        // Server principal name 
		RPC_C_AUTHN_LEVEL_CALL,      // RPC_C_AUTHN_LEVEL_xxx 
		RPC_C_IMP_LEVEL_IMPERSONATE, // RPC_C_IMP_LEVEL_xxx
		nullptr,                        // client identity
		EOAC_NONE                    // proxy capabilities 
	);

	if (FAILED(hres))
	{
		std::cout << "Could not set proxy blanket. Error code = 0x" << std::hex << hres << std::endl;
		pSvc->Release();
		pLoc->Release();
		::CoUninitialize();
		return devices;
	}

	// Step 6: --------------------------------------------------
	// Use the IWbemServices pointer to make requests of WMI ----

	IEnumWbemClassObject* pEnumerator = nullptr;
	hres = pSvc->ExecQuery(
		bstr_t("WQL"),
		//bstr_t("SELECT * FROM Win32_Processor"),	// For example, get the name of the operating system
		&devicename[0],
		WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
		nullptr,
		&pEnumerator);

	if (FAILED(hres))
	{
		std::cout << "Query for operating system name failed." << " Error code = 0x" << std::hex << hres << std::endl;
		pEnumerator->Release();
		pSvc->Release();
		pLoc->Release();
		::CoUninitialize();
		return devices;               // Program has failed.
	}

	// Step 7: -------------------------------------------------
	// Get the data from the query in step 6 -------------------

	while (pEnumerator)
	{
		IWbemClassObject *pclsObj = nullptr;
		ULONG iter_result = 0;

		pEnumerator->Next(WBEM_INFINITE, 1, &pclsObj, &iter_result);
		if (!iter_result)
			break;

		VARIANT vtProp;

		// Get the value of the Name property
		pclsObj->Get(&devicevalue[0], 0, &vtProp, 0, 0);

		if (vtProp.vt == VT_I1)
			std::cout << "VT_I1: " << vtProp.intVal << std::endl;
		else if (vtProp.vt == VT_I2)
			std::cout << "VT_I2: " << vtProp.intVal << std::endl;
		else if (vtProp.vt == VT_I8)
			std::cout << "VT_I8: " << vtProp.intVal << std::endl;
		else if (vtProp.vt == VT_UI1)
			std::cout << "VT_UI1: " << vtProp.intVal << std::endl;
		else if (vtProp.vt == VT_UI2)
			std::cout << "VT_UI2: " << vtProp.intVal << std::endl;
		else if (vtProp.vt == VT_UI4)
			std::cout << "VT_UI4: " << vtProp.intVal << std::endl;
		else if (vtProp.vt == VT_UI8)
			std::cout << "VT_UI8: " << vtProp.intVal << std::endl;
		else if (vtProp.vt == VT_NULL)
			std::cout << "Null" << std::endl;
		else if (vtProp.vt == VT_EMPTY)
			std::cout << "Empty" << std::endl;
		else if (vtProp.vt == VT_DATE)
			std::cout << "DATE is not available!" << std::endl;
		else if (vtProp.vt == VT_BOOL)
		{
			std::ostringstream sstream;
			sstream << vtProp.boolVal;
			devices.push_back(sstream.str());
		}
		else if (vtProp.vt == VT_I4)	// Note: VT_I4 = uint32
		{
			std::ostringstream sstream;
			sstream << vtProp.uintVal;
			devices.push_back(sstream.str());
		}
		else if (vtProp.vt == VT_BSTR)
		{
			std::wstring result(vtProp.bstrVal);
			std::string str(result.begin(), result.end());

			devices.push_back(str);
		}
		VariantClear(&vtProp);
		pclsObj->Release();
	}

	// Cleanup
	if (pSvc) pSvc->Release();
	if (pLoc) pLoc->Release();
	if (pEnumerator) pEnumerator->Release();
	::CoUninitialize();

#endif // _WIN32

	return devices;
}
