/*********************************************************************************
created:	2017/04/03   11:36PM
filename: 	fvkLicenseDecoderClient.cpp
file base:	fvkLicenseDecoderClient
file ext:	cpp
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class to decode the license key.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2017-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkLicenseDecoderClient.h"

#include <QDebug>

using namespace R3D;

fvkLicenseDecoderClient::fvkLicenseDecoderClient()
{
}

QString fvkLicenseDecoderClient::getClientName() const
{
	return m_client_name;
}
QString fvkLicenseDecoderClient::getOrderDate() const
{
	return m_order_date;
}
QString fvkLicenseDecoderClient::getClientCompany() const
{
	return m_client_company;
}
QString fvkLicenseDecoderClient::getClientAddress() const
{
	return m_client_address;
}
QString fvkLicenseDecoderClient::getClientEmail() const
{
	return m_client_email;
}
QString fvkLicenseDecoderClient::getClientPhone() const
{
	return m_client_phone;
}
QString fvkLicenseDecoderClient::getClientComments() const
{
	return m_client_comments;
}

QString fvkLicenseDecoderClient::getAppName() const
{
	return m_app_name;
}
QString fvkLicenseDecoderClient::getAppVersion() const
{
	return m_app_version;
}

QString fvkLicenseDecoderClient::getHardwareKey() const
{
	return m_hardware_key;
}

void fvkLicenseDecoderClient::parse(const QString& _keyText)
{
	m_client_name = "";
	m_order_date = "";
	m_client_company = "";
	m_client_address = "";
	m_client_email = "";
	m_client_phone = "";
	m_client_comments = "";

	m_app_name = "";
	m_app_version = "";

	auto s = _keyText.split(';');
	if (s.size() == 3)
	{
		if (s[0].contains('^'))		// more than 1 modules were registered.
		{
			auto client = s[0].split('^');
			if (client.size() == 7)
			{
				m_client_name = client[0];
				m_order_date = client[1];
				m_client_company = client[2];
				m_client_address = client[3];
				m_client_email = client[4];
				m_client_phone = client[5];
				m_client_comments = client[6];
			}
		}

		if (s[1].contains('^'))		// more than 1 modules were registered.
		{
			auto app = s[1].split('^');
			if (app.size() == 2)
			{
				m_app_name = app[0];
				m_app_version = app[1];
			}
		}

		m_hardware_key = s[2];
	}
}

int fvkLicenseDecoderClient::decode()
{
	auto strKey = QString::number(getKey());
	if (strKey.size() < 6 || strKey.size() > 14)
		return 0;

	// encrypt from cryptographic hash.
	auto base64 = m_crypt.decryptToString(m_key_text);
	if (base64.isEmpty())
		return -2;

	auto encrypted = QByteArray::fromBase64(base64.toLatin1());
	if (encrypted.isEmpty())
		return -1;

	// decryption with the same key.
	QString decrypted;
	for (auto i = 0; i < encrypted.size(); i++)
		decrypted += static_cast<char>(static_cast<quint64>(encrypted.at(i)) - getKey() - static_cast<quint64>(strKey.at(i % strKey.size()).toLatin1()));

	decrypted = decodeText(decrypted, "3" + strKey + "7");

	m_key_text = decrypted;
	parse(m_key_text);

	return 1;
}

void fvkLicenseDecoderClient::print()
{
	qDebug() << "";
	qDebug().noquote() << "Client Name: " << m_client_name;
	qDebug().noquote() << "Order Date: " << m_order_date;
	qDebug().noquote() << "Client Company: " << m_client_company;
	qDebug().noquote() << "Client Address: " << m_client_address;
	qDebug().noquote() << "Client Email: " << m_client_email;
	qDebug().noquote() << "Client Phone: " << m_client_phone;
	qDebug().noquote() << "Client Comments: " << m_client_comments;
	qDebug() << "";

	qDebug().noquote() << "App Name: " << m_app_name;
	qDebug().noquote() << "App Version: " << m_app_version;

	qDebug() << "";

	qDebug().noquote() << "Key Text: " << m_key_text;
	qDebug() << "";

	qDebug().noquote() << "Input File: " << m_input_file;
	qDebug().noquote() << "Output File: " << m_output_file;
}
