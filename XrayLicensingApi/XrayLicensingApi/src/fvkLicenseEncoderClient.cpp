/*********************************************************************************
created:	2017/04/03   11:36PM
filename: 	fvkLicenseEncoderClient.cpp
file base:	fvkLicenseEncoderClient
file ext:	cpp
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class to encode the hardware information to the license key.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2017-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkLicenseEncoderClient.h"

#include <QDebug>

using namespace R3D;

fvkLicenseEncoderClient::fvkLicenseEncoderClient()
{
}

void fvkLicenseEncoderClient::setClientName(const QString& _info)
{
	m_client_name = _info;
}
void fvkLicenseEncoderClient::setOrderDate(const QString& _info)
{
	m_order_date = _info;
}
void fvkLicenseEncoderClient::setClientCompany(const QString& _info)
{
	m_client_company = _info;
}
void fvkLicenseEncoderClient::setClientAddress(const QString& _info)
{
	m_client_address = _info;
}
void fvkLicenseEncoderClient::setClientEmail(const QString& _info)
{
	m_client_email = _info;
}
void fvkLicenseEncoderClient::setClientPhone(const QString& _info)
{
	m_client_phone = _info;
}
void fvkLicenseEncoderClient::setClientComments(const QString& _info)
{
	m_client_comments = _info;
}

void fvkLicenseEncoderClient::setAppName(const QString& _info)
{
	m_app_name = _info;
}
void fvkLicenseEncoderClient::setAppVersion(const QString& _info)
{
	m_app_version = _info;
}

int fvkLicenseEncoderClient::encode()
{
	auto client = 
		m_client_name + '^' +
		m_order_date + '^' +
		m_client_company + '^' +
		m_client_address + '^' +
		m_client_email + '^' +
		m_client_phone + '^' +
		m_client_comments;

	auto app =
		m_app_name + '^' +
		m_app_version;

	auto s = client + ';' + app + ';' + getDevices().c_str();

	auto strKey = QString::number(getKey());
	if (strKey.size() < 6 || strKey.size() > 14)
		return 0;

	s = encodeText(s, "3" + strKey + "7");

	// encryption with the same key.
	QString encrypted;
	for (auto i = 0; i < s.size(); i++)
		encrypted += static_cast<char>(static_cast<quint64>(s.at(i).toLatin1()) + getKey() + static_cast<quint64>(strKey.at(i % strKey.size()).toLatin1()));

	auto base64 = encrypted.toLatin1().toBase64();
	if(base64.isEmpty())
		return -1;

	// encrypt to cryptographic hash.
	m_key_text = m_crypt.encryptToString(base64);
	if(m_key_text.isEmpty())
		return -2;

	return 1;
}

void fvkLicenseEncoderClient::print()
{
	qDebug() << "";
	qDebug().noquote() << "Client Name: " << m_client_name;
	qDebug().noquote() << "Order Date: " << m_order_date;
	qDebug().noquote() << "Client Company: " << m_client_company;
	qDebug().noquote() << "Client Address: " << m_client_address;
	qDebug().noquote() << "Client Email: " << m_client_email;
	qDebug().noquote() << "Client Phone: " << m_client_phone;
	qDebug().noquote() << "Client Comments: " << m_client_comments;
	qDebug() << "";

	qDebug().noquote() << "App Name: " << m_app_name;
	qDebug().noquote() << "App Version: " << m_app_version;

	qDebug() << "";

	qDebug().noquote() << "Key Text: " << m_key_text;
	qDebug() << "";

	qDebug().noquote() << "Input File: " << m_input_file;
	qDebug().noquote() << "Output File: " << m_output_file;
}
