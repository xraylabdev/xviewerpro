/*********************************************************************************
created:	2017/04/03   11:36PM
filename: 	fvkLicenseEncoderServer.cpp
file base:	fvkLicenseEncoderServer
file ext:	cpp
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class to encode the hardware information to the license key.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2017-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkLicenseEncoderServer.h"
#include "fvkLicenseDecoderClient.h"

#include <QDebug>

using namespace R3D;

fvkLicenseEncoderServer::fvkLicenseEncoderServer()
{
}

void fvkLicenseEncoderServer::setLicenseId(const QString& _s)
{
	m_license_id = _s;
}
void fvkLicenseEncoderServer::setSerial(const QString& _s)
{
	m_serial = _s;
}
void fvkLicenseEncoderServer::setExpireDate(const QString& _s)
{
	m_expire_date = _s;
}
void fvkLicenseEncoderServer::setIssueDate(const QString& _s)
{
	m_issue_date = _s;
}
void fvkLicenseEncoderServer::setLicenseType(const QString& _s)
{
	m_license_type = _s;
}
void fvkLicenseEncoderServer::setModules(const QString& _s)
{
	m_modules = _s;
}

int fvkLicenseEncoderServer::encode()
{
	fvkLicenseDecoderClient decoder;
	decoder.setIdentifier(getIdentifier());
	decoder.setKey(getKey());
	decoder.setInputFileLocation(getInputFileLocation());
	if (!decoder.read())
		return 0;

	if (decoder.decode() != 1)
		return -1;

	if(decoder.getOrderDate().isEmpty())
		return -2;

	auto client = 
		decoder.getClientName() + '^' +
		decoder.getClientCompany() + '^' +
		decoder.getClientEmail() + '^' +
		decoder.getAppName() + '^' +
		decoder.getAppVersion() + '^' +
		m_license_id + '^' +
		m_serial + '^' +
		m_license_type + '^' +
		m_expire_date + '^' +
		m_issue_date + '^' +
		m_modules;

	auto s = client + ';' + decoder.getHardwareKey();

	auto serverSideKey = getKey() - 1;	// do not use the same key.
	auto strKey = QString::number(serverSideKey);
	if (strKey.size() < 6 || strKey.size() > 14)
		return -3;

	s = encodeText(s, "3" + strKey + "7");

	// encryption with the same key.
	QString encrypted;
	for (auto i = 0; i < s.size(); i++)
		encrypted += static_cast<char>(static_cast<quint64>(s.at(i).toLatin1()) + serverSideKey + static_cast<quint64>(strKey.at(i % strKey.size()).toLatin1()));

	auto base64 = encrypted.toLatin1().toBase64();
	if(base64.isEmpty())
		return -4;

	// encrypt to cryptographic hash.
	m_key_text = m_crypt.encryptToString(base64);
	if(m_key_text.isEmpty())
		return -5;

	return 1;
}

void fvkLicenseEncoderServer::print()
{
	qDebug() << "";
	qDebug().noquote() << "License-id: " << m_license_id;
	qDebug().noquote() << "Serial: " << m_serial;
	qDebug().noquote() << "Type: " << m_license_type;
	qDebug().noquote() << "Expire: " << m_expire_date;
	qDebug().noquote() << "Issue: " << m_issue_date;
	qDebug().noquote() << "Modules: " << m_modules;
	qDebug() << "";

	qDebug().noquote() << "Key Text: " << m_key_text;
	qDebug() << "";

	qDebug().noquote() << "Input File: " << m_input_file;
	qDebug().noquote() << "Output File: " << m_output_file;
}
