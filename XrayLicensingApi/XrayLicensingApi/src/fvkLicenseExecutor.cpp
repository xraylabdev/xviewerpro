/*********************************************************************************
created:	2018/04/03   11:36PM
filename: 	fvkLicenseExecutor.cpp
file base:	fvkLicenseExecutor
file ext:	cpp
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class to execute the PRO version by checking the current hardware.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2018 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkLicenseExecutor.h"
#include "fvkWinUtil.h"

#ifdef _WIN32
#include <windows.h>
#endif

using namespace R3D;

fvkLicenseExecutor::fvkLicenseExecutor()
{
}

int fvkLicenseExecutor::execute()
{
	// first match license key with the current hardware.
	auto s = getDevices();
	if (m_client_key != s)
		return 0;

#ifdef _WIN32

	// create mutex to communicate with executables.
	auto mutex = fvkWinUtil::createMutex(m_mutex_name.c_str());
	if (!mutex)
		return -1;

	// execute application by specifying command, file path, and arguments.
#ifdef _UNICODE
	ShellExecuteA(nullptr, (char*)"open", (char*)m_exe_name.c_str(), (char*)m_args.c_str(), nullptr, SW_SHOW);
#else
	ShellExecute(nullptr, (char*)"open", (char*)m_exe_name.c_str(), (char*)m_args.c_str(), nullptr, SW_SHOW);
#endif // _UNICODE

	// very important: wait for a moment to let main application check, whether mutex is already existed or not.
	Sleep(5000);

	// we are all set, close the mutex now.
	ReleaseMutex(mutex);
	CloseHandle(mutex);

#endif // _WIN32

	return 1;
}