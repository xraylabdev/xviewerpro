/*********************************************************************************
created:	2017/04/03   11:36PM
filename: 	fvkLicenseDecoderServer.cpp
file base:	fvkLicenseDecoderServer
file ext:	cpp
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class to decode the license key.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2017-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkLicenseDecoderServer.h"

#include <QDebug>

using namespace R3D;

fvkLicenseDecoderServer::fvkLicenseDecoderServer() :
	m_hardwareIdMatched(false)
{
}

QString fvkLicenseDecoderServer::getClientName() const
{
	return m_client_name;
}
QString fvkLicenseDecoderServer::getClientCompany() const
{
	return m_client_company;
}
QString fvkLicenseDecoderServer::getClientEmail() const
{
	return m_client_email;
}
QString fvkLicenseDecoderServer::getAppName() const
{
	return m_app_name;
}
QString fvkLicenseDecoderServer::getAppVersion() const
{
	return m_app_version;
}

QString fvkLicenseDecoderServer::getLicenseId() const
{
	return m_license_id;
}
QString fvkLicenseDecoderServer::getSerial() const
{
	return m_serial;
}
QString fvkLicenseDecoderServer::getExpireDate() const
{
	return m_expire_date;
}
QString fvkLicenseDecoderServer::getIssueDate() const
{
	return m_issue_date;
}
QString fvkLicenseDecoderServer::getLicenseType() const
{
	return m_license_type;
}
QString fvkLicenseDecoderServer::getModules() const
{
	return m_modules;
}

bool fvkLicenseDecoderServer::isHardwareMatched()
{
	return m_hardwareIdMatched;
}

void fvkLicenseDecoderServer::parse(const QString& _keyText)
{
	m_client_name = "";
	m_client_company = "";
	m_client_email = "";
	m_app_name = "";
	m_app_version = "";
	m_license_id = "";
	m_serial = "";
	m_license_type = "";
	m_expire_date = "";
	m_issue_date = "";
	m_modules = "";
	m_hardwareIdMatched = false;

	auto s = _keyText.split(';');
	if (s.size() == 2)
	{
		if (s[0].contains('^'))
		{
			auto client = s[0].split('^');
			if (client.size() == 11)
			{
				m_client_name = client[0];
				m_client_company = client[1];
				m_client_email = client[2];
				m_app_name = client[3];
				m_app_version = client[4];

				m_license_id = client[5];
				m_serial = client[6];
				m_license_type = client[7];
				m_expire_date = client[8];
				m_issue_date = client[9];
				m_modules = client[10];
			}
		}

		if (s[1] == QString(getDevices().c_str()))
			m_hardwareIdMatched = true;
	}
}

int fvkLicenseDecoderServer::decode()
{
	auto serverSideKey = getKey() - 1;	// do not use the same key.
	auto strKey = QString::number(serverSideKey);
	if (strKey.size() < 6 || strKey.size() > 14)
		return 0;

	// encrypt from cryptographic hash.
	auto base64 = m_crypt.decryptToString(m_key_text);
	if (base64.isEmpty())
		return -2;

	auto encrypted = QByteArray::fromBase64(base64.toLatin1());
	if (encrypted.isEmpty())
		return -1;

	// decryption with the same key.
	QString decrypted;
	for (auto i = 0; i < encrypted.size(); i++)
		decrypted += static_cast<char>(static_cast<quint64>(encrypted.at(i)) - serverSideKey - static_cast<quint64>(strKey.at(i % strKey.size()).toLatin1()));

	decrypted = decodeText(decrypted, + "3" + strKey + "7");

	m_key_text = decrypted;
	parse(m_key_text);

	return 1;
}

void fvkLicenseDecoderServer::print()
{
	qDebug() << "";
	if (m_hardwareIdMatched)
		qDebug().noquote() << "Registered hardware.";
	else
		qDebug().noquote() << "Not registered hardware.";

	qDebug() << "";
	qDebug().noquote() << "Client Name: " << m_client_name;
	qDebug().noquote() << "Client Company: " << m_client_company;
	qDebug().noquote() << "Client Email: " << m_client_email;

	qDebug() << "";
	qDebug().noquote() << "App Name: " << m_app_name;
	qDebug().noquote() << "App Version: " << m_app_version;

	qDebug() << "";
	qDebug().noquote() << "License-id: " << m_license_id;
	qDebug().noquote() << "Serial: " << m_serial;
	qDebug().noquote() << "Type: " << m_license_type;
	qDebug().noquote() << "Expire: " << m_expire_date;
	qDebug().noquote() << "Issue: " << m_issue_date;
	qDebug().noquote() << "Modules: " << m_modules;
	qDebug() << "";

	qDebug().noquote() << "Key Text: " << m_key_text;
	qDebug() << "";

	qDebug().noquote() << "Input File: " << m_input_file;
	qDebug().noquote() << "Output File: " << m_output_file;
}
