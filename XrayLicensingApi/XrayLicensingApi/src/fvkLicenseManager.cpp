/*********************************************************************************
created:	2017/04/02   11:36PM
filename: 	fvkLicenseManager.cpp
file base:	fvkLicenseManager
file ext:	cpp
author:		Furqan Ullah (Post-doc, Ph.D.)
website:    http://real3d.pk
CopyRight:	All Rights Reserved

purpose:	class to manage the hardware license _key.

/**********************************************************************************
REAL3D LICENSE MANAGER
Copyright (C) 2017-2019 REAL3D

This file and its content is protected by a software license.
You should have received a copy of this license with this file.
If not, please contact Dr. Furqan Ullah immediately:
**********************************************************************************/

#include "fvkLicenseManager.h"
#include "fvkWinUtil.h"
#include "tiny_aes.h"

#include <QFile>
#include <QTextStream>
#include <QCryptographicHash>

#include <regex>

using namespace R3D;

fvkLicenseManager::fvkLicenseManager() :
	m_identifier(HardwareIdentifier::ProcessorName | HardwareIdentifier::ProcessorId | HardwareIdentifier::BaseBoardProduct)
{
	m_crypt.setCompressionMode(fvkQSimpleCrypt::CompressionMode::CompressionAlways);
	m_crypt.setIntegrityProtectionMode(fvkQSimpleCrypt::IntegrityProtectionMode::ProtectionHash);
}

void fvkLicenseManager::setIdentifier(int _id)
{ 
	m_identifier = _id; 
}
int fvkLicenseManager::getIdentifier() const
{ 
	return m_identifier; 
}

void fvkLicenseManager::setKey(const quint64 _key)
{
	m_crypt.setKey(_key);
}
quint64 fvkLicenseManager::getKey() const
{
	return m_crypt.getKey();
}
bool fvkLicenseManager::hasKey() const
{
	return m_crypt.hasKey();
}
QString fvkLicenseManager::getKeyText() const
{ 
	return m_key_text;
}

void fvkLicenseManager::setInputFileLocation(const QString & _filepath)
{
	m_input_file = _filepath;
}
QString fvkLicenseManager::getInputFileLocation() const
{
	return m_input_file;
}
void fvkLicenseManager::setOutputFileLocation(const QString & _filepath) 
{ 
	m_output_file = _filepath; 
}
QString fvkLicenseManager::getOutputFileLocation() const 
{
	return m_output_file; 
}

bool fvkLicenseManager::read()
{
	if (!QFile::exists(m_input_file))
		return false;

	QFile file(m_input_file);
	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QTextStream stream(&file);

		QString line;
		while (stream.readLineInto(&line))
			m_key_text.append(line);

		file.close();
		return true;
	}

	return false;
}
bool fvkLicenseManager::write()
{
	if (m_key_text.isEmpty())
		return false;

	QFile file(m_output_file);
	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream stream(&file);

		stream << m_key_text;

		file.close();

		if (QFile::exists(m_output_file))
			return true;
	}

	return false;
}

static std::string merge(const std::vector<std::string>& _v, bool _removeSpaces = true)
{
	std::regex r("\\s+");
	std::string s;
	for (const auto& i : _v)
	{
		if (!i.empty())
		{
			if (_removeSpaces)
				s += std::regex_replace(i, r, "");
			else
				s += i;
		}
	}
	return s;
}

std::string fvkLicenseManager::getDevices()
{
	std::string hardware;
	if (m_identifier & HardwareIdentifier::ProcessorName)
		hardware += merge(fvkWinUtil::getDevices("SELECT * FROM Win32_Processor", "Name"), true) + '^';
	if (m_identifier & HardwareIdentifier::ProcessorId)
		hardware += merge(fvkWinUtil::getDevices("SELECT * FROM Win32_Processor", "ProcessorId"), true) + '^';
	if (m_identifier & HardwareIdentifier::ProcessorSerialNumber)
		hardware += merge(fvkWinUtil::getDevices("SELECT * FROM Win32_Processor", "SerialNumber"), true) + '^';
	if (m_identifier & HardwareIdentifier::BaseBoardProduct)
		hardware += merge(fvkWinUtil::getDevices("SELECT * FROM Win32_BaseBoard", "Product"), true) + '^';
	if (m_identifier & HardwareIdentifier::BaseBoardSerialNumber)
		hardware += merge(fvkWinUtil::getDevices("SELECT * FROM Win32_BaseBoard", "SerialNumber"), true) + '^';
	if (m_identifier & HardwareIdentifier::BaseBoardManufacturer)
		hardware += merge(fvkWinUtil::getDevices("SELECT * FROM Win32_BaseBoard", "Manufacturer"), true) + '^';
	if (m_identifier & HardwareIdentifier::DiskDriveSerialNumber)
		hardware += merge(fvkWinUtil::getDevices("SELECT * FROM Win32_PhysicalMedia", "SerialNumber"), true) + '^';
	if (m_identifier & HardwareIdentifier::DiskDriveCaption)
		hardware += merge(fvkWinUtil::getDevices("SELECT * FROM Win32_DiskDrive", "Caption"), true) + '^';

	return hardware;
}

const uint8_t iv[] = { 0xf0, 0xe1, 0xd2, 0xc3, 0xb4, 0xa5, 0x96, 0x87, 0x78, 0x69, 0x5a, 0x4b, 0x3c, 0x2d, 0x5e, 0xaf };

static int getAlignedSize(int currSize, int alignment)
{
	Q_ASSERT(currSize >= 0);

	int padding = (alignment - currSize % alignment) % alignment;
	return currSize + padding;
}

QString fvkLicenseManager::encodeText(const QString& _rawText, const QString& _key)
{
	QCryptographicHash hash(QCryptographicHash::Md5);
	hash.addData(_key.toUtf8());
	auto keyData = hash.result();

	auto rawData = _rawText.utf16();
	auto rawDataVoid = (void*)rawData;
	auto rawDataChar = static_cast<const char*>(rawDataVoid);
	QByteArray inputData;
	inputData.append(rawDataChar, _rawText.size() * sizeof(QChar) + 1);

	const auto length = inputData.size();
	auto encryptionLength = getAlignedSize(length, 16);
	Q_ASSERT(encryptionLength % 16 == 0 && encryptionLength >= length);

	QByteArray encodingBuffer(encryptionLength, '\0');
	inputData.resize(encryptionLength);
	for (auto i = length; i < encryptionLength; i++)
		inputData[i] = 0;

	AES_CBC_encrypt_buffer((uint8_t*)encodingBuffer.data(), (uint8_t*)inputData.data(), encryptionLength, (const uint8_t*)keyData.data(), iv);

	QByteArray data(encodingBuffer.data(), encryptionLength);
	auto hex = QString::fromLatin1(data.toHex());
	return hex;
}

QString fvkLicenseManager::decodeText(const QString& _hexEncodedText, const QString& _key)
{
	QCryptographicHash hash(QCryptographicHash::Md5);
	hash.addData(_key.toUtf8());
	auto keyData = hash.result();

	const auto length = _hexEncodedText.size();
	auto encryptionLength = getAlignedSize(length, 16);

	QByteArray encodingBuffer(encryptionLength, '\0');

	auto encodedText = QByteArray::fromHex(_hexEncodedText.toLatin1());
	const auto encodedOriginalSize = encodedText.size();
	Q_ASSERT(encodedText.length() <= encryptionLength);
	encodedText.resize(encryptionLength);
	for (auto i = encodedOriginalSize; i < encryptionLength; i++)
		encodedText[i] = 0;

	AES_CBC_decrypt_buffer((uint8_t*)encodingBuffer.data(), (uint8_t*)encodedText.data(), encryptionLength, (const uint8_t*)keyData.data(), iv);

	encodingBuffer.append("\0\0");
	auto data = encodingBuffer.data();
	const ushort* decodedData = reinterpret_cast<ushort*>(data);
	auto result = QString::fromUtf16(decodedData, -1);
	return result;
}