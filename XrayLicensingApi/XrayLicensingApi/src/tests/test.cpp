
#include "fvkLicenseEncoderClient.h"
#include "fvkLicenseDecoderClient.h"
#include "fvkLicenseEncoderServer.h"
#include "fvkLicenseDecoderServer.h"

#include <QDebug>

using namespace R3D;

int main(int argc, char *argv[])
{
	qDebug() << fvkLicenseManager::encodeText("Hello World, please help....", "456456457");

	fvkLicenseEncoderClient encoder;
	encoder.setClientName("furqan");
	encoder.setOrderDate("11:06:2019");
	encoder.setClientCompany("XRAY-LAB GmbH Co. KG.");
	encoder.setClientAddress("SternnFels 2, 898327");
	encoder.setClientEmail("ful@xray-lab.com");
	encoder.setClientPhone("0137438446");
	encoder.setClientComments("All is well.");

	encoder.setAppName("AppManager");
	encoder.setAppVersion("1142");

	encoder.setKey(456456457);
	encoder.setIdentifier(
		HardwareIdentifier::ProcessorName |
		HardwareIdentifier::ProcessorId |
		HardwareIdentifier::BaseBoardProduct |
		HardwareIdentifier::BaseBoardManufacturer);
	encoder.setOutputFileLocation("C:/Users/ful/Documents/test/encoded.txt");
	encoder.encode();
	qDebug().noquote() << "\nencoded:";
	encoder.print();
	encoder.write();

	//--------------------------

	fvkLicenseDecoderClient decoder;
	decoder.setKey(456456457);
	decoder.setIdentifier(
		HardwareIdentifier::ProcessorName |
		HardwareIdentifier::ProcessorId |
		HardwareIdentifier::BaseBoardProduct |
		HardwareIdentifier::BaseBoardManufacturer);
	decoder.setInputFileLocation("C:/Users/ful/Documents/test/encoded.txt");
	decoder.setOutputFileLocation("C:/Users/ful/Documents/test/decoded.txt");
	decoder.read();
	decoder.decode();
	qDebug().noquote() << "decoded: \n";
	decoder.print();
	decoder.write();

	qDebug().noquote() << "server encoded: \n";
	fvkLicenseEncoderServer serverEncoder;
	serverEncoder.setKey(456456457);
	serverEncoder.setIdentifier(
		HardwareIdentifier::ProcessorName |
		HardwareIdentifier::ProcessorId |
		HardwareIdentifier::BaseBoardProduct |
		HardwareIdentifier::BaseBoardManufacturer);
	serverEncoder.setLicenseId("123456789");
	serverEncoder.setSerial("0000000");
	serverEncoder.setExpireDate("09:21:2019");
	serverEncoder.setIssueDate("06:11:2019");
	serverEncoder.setLicenseType("Permanent");
	serverEncoder.setModules("XVOID");
	serverEncoder.setInputFileLocation(decoder.getInputFileLocation());
	serverEncoder.setOutputFileLocation(decoder.getOutputFileLocation());
	if (serverEncoder.encode() == 1)
		serverEncoder.print();
	serverEncoder.write();

	qDebug().noquote() << "server decoded: \n";
	fvkLicenseDecoderServer serverDecoder;
	serverDecoder.setKey(456456457);
	serverDecoder.setIdentifier(
		HardwareIdentifier::ProcessorName |
		HardwareIdentifier::ProcessorId |
		HardwareIdentifier::BaseBoardProduct |
		HardwareIdentifier::BaseBoardManufacturer);
	serverDecoder.setInputFileLocation(decoder.getOutputFileLocation());
	serverDecoder.read();
	if (serverDecoder.decode() == 1)
		serverDecoder.print();

	return 1;
}
